/*
 * Copyright (c) 2012-2013 by Tensilica Inc. ALL RIGHTS RESERVED.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef __PRELOADER_PILSI_CONFIGURE_H__
#define __PRELOADER_PILSI_CONFIGURE_H__

#include "xt_library_loader.h"


#ifdef __cplusplus
extern "C" {
#endif


extern xtlib_packaged_library xa_aac_dec_library;// user muts be extern your lib to here!!!
#ifdef MTK_BT_A2DP_MSBC_USE_PIC
    extern xtlib_packaged_library pisplit_msbc_dec;// user muts be extern your lib to here!!!
    extern xtlib_packaged_library pisplit_msbc_enc;// user muts be extern your lib to here!!!
    #define MSBC_DEC_LIB &pisplit_msbc_dec,
    #define MSBC_ENC_LIB &pisplit_msbc_enc,
#else
    #define MSBC_DEC_LIB
    #define MSBC_ENC_LIB
#endif

extern xtlib_packaged_library pisplit_cvsd_dec;// user muts be extern your lib to here!!!
extern xtlib_packaged_library pisplit_cvsd_enc;// user muts be extern your lib to here!!!
extern xtlib_packaged_library pisplit_cpd;// user muts be extern your lib to here!!!
#ifdef MTK_WWE_USE_PIC

     #ifdef MTK_WWE_AMA_USE_PIC
          extern xtlib_packaged_library pisplit_library_pryon1000;// user muts be extern your lib to here!!!
          #define WWE_LIB_AMA &pisplit_library_pryon1000,
     #else
          #define WWE_LIB_AMA
     #endif
     #ifdef MTK_WWE_GSOUND_USE_PIC
          extern xtlib_packaged_library pisplit_google_hotword_dsp_multi_bank;// user muts be extern your lib to here!!!
          #define WWE_LIB_GSOUND &pisplit_google_hotword_dsp_multi_bank,
     #else
          #define WWE_LIB_GSOUND
     #endif
#else
     #define WWE_LIB_AMA
     #define WWE_LIB_GSOUND
#endif

#ifdef MTK_INEAR_ENHANCEMENT
     extern xtlib_packaged_library pisplit_ecnr_inear;// user muts be extern your lib to here!!!
     #define ECNR_LIB &pisplit_ecnr_inear,
#elif defined(MTK_DUALMIC_INEAR)
     extern xtlib_packaged_library pisplit_ecnr_inear_v2;// user muts be extern your lib to here!!!
     #define ECNR_LIB &pisplit_ecnr_inear_v2,
#elif defined(MTK_3RD_PARTY_NR)
     extern xtlib_packaged_library pisplit_ec_rxnr;// user muts be extern your lib to here!!!
     #define ECNR_LIB &pisplit_ec_rxnr,
#else
     extern xtlib_packaged_library pisplit_ecnr;// user muts be extern your lib to here!!!
     #define ECNR_LIB &pisplit_ecnr,
#endif

extern xtlib_packaged_library pisplit_skew_ctrl;// user muts be extern your lib to here!!!
extern xtlib_packaged_library pisplit_plc_pitch;// user muts be extern your lib to here!!!

#ifdef MTK_VOICE_AGC_ENABLE
     extern xtlib_packaged_library pisplit_agc;// user muts be extern your lib to here!!!
	 #define AGC_LIB &pisplit_agc,
#else
     #define AGC_LIB
#endif

#ifdef MTK_PEQ_ENABLE
extern xtlib_packaged_library pisplit_peq2;// user muts be extern your lib to here!!!
#endif

#ifdef MTK_BT_A2DP_VENDOR_USE_PIC
extern xtlib_packaged_library vendor_dec_library;// user muts be extern your lib to here!!!
#endif
#ifdef MTK_BT_A2DP_AAC_USE_PIC
extern xtlib_packaged_library pisplit_AAC_dec_5x;
#endif
#ifdef MTK_BT_A2DP_SBC_USE_PIC
extern xtlib_packaged_library pisplit_sbc_dec;
#endif

#ifdef MTK_BT_A2DP_VENDOR_USE_PIC
#define VEND_LIB &vendor_dec_library,
#else
#define VEND_LIB
#endif

#ifdef MTK_BT_A2DP_AAC_USE_PIC
#define AAC_LIB &pisplit_AAC_dec_5x,
#else
#define AAC_LIB
#endif

#ifdef MTK_BT_A2DP_SBC_USE_PIC
#define SBC_LIB &pisplit_sbc_dec,
#else
#define SBC_LIB
#endif
// user muts be add your lib to here!!!
#define PIC_LIB_LIST_DEFAULT \
    ECNR_LIB\
	AGC_LIB\
    &pisplit_cpd,\
    &pisplit_skew_ctrl,\
    &pisplit_plc_pitch,\
    WWE_LIB_AMA\
    WWE_LIB_GSOUND\
    VEND_LIB\
	AAC_LIB\
	SBC_LIB\
	MSBC_DEC_LIB\
	MSBC_ENC_LIB\

#ifdef MTK_PEQ_ENABLE
#define PIC_LIB_LIST {PIC_LIB_LIST_DEFAULT &pisplit_peq2,}
#else
#define PIC_LIB_LIST {PIC_LIB_LIST_DEFAULT}
#endif


#ifdef __cplusplus
}
#endif

#endif /* __PRELOADER_PILSI_CONFIGURE_H__ */
