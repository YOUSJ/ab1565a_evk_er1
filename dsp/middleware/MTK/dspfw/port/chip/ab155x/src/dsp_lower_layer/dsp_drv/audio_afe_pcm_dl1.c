/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "sink.h"
#include "transform.h"
#include "stream_audio_setting.h"
#include "hal_nvic.h"
#include "FreeRTOS.h"
#include "stream_audio_driver.h"
#include "dsp_callback.h"
#include "hal_audio_afe_control.h"
#include "hal_audio_afe_define.h"
#include "hal_audio_afe_clock.h"
#include "audio_afe_common.h"


extern afe_stream_channel_t connect_type[2][2];
extern afe_t afe;

static void dl1_global_var_init(SINK sink)
{
    #if 0
    afe_block_t *afe_block = &sink->param.audio.AfeBlkControl;
    memset(afe_block, 0, sizeof(afe_block_t));
    #else
    UNUSED(sink);
    #endif
}

static int32_t pcm_dl1_probe(SINK sink)
{
    dl1_global_var_init(sink);
    return 0;
}

static int32_t pcm_dl1_start(SINK sink)
{
    AUDIO_PARAMETER *runtime = &sink->param.audio;
    afe_block_t *afe_block = &sink->param.audio.AfeBlkControl;
    audio_digital_block_t memory_block;
    uint32_t dl_samplerate = runtime->rate;

    hal_audio_afe_clock_on();

    memory_block =  hal_audio_afe_get_memory_digital_block (sink->param.audio.memory, true);
    DSP_MW_LOG_I("DSP audio pcm_dl_start:%d\r\n", 1, memory_block);

    if (afe_block->u4asrcflag) {
        afe_asrc_config_t asrc_config;
        if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL1) {
            afe_set_asrc_dl1_configuration_parameters(sink, &asrc_config);
            afe_set_asrc_enable(true, AFE_MEM_ASRC_1, &asrc_config);
        } else if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL2) {
        afe_set_asrc_dl2_configuration_parameters(sink, &asrc_config);
            afe_set_asrc_enable(true, AFE_MEM_ASRC_2, &asrc_config);
        }
        dl_samplerate = asrc_config.output_buffer.rate;
        afe_block->u4asrcrate = dl_samplerate;
#if (AFE_REGISTER_ASRC_IRQ)
    } else {
#else
    }
    {
#endif
        /* set irq start */
        if (!sink->param.audio.AfeBlkControl.u4awsflag) {
            if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL2) {
                if(sink->param.audio.aws_sync_request != true){
                    //DSP_MW_LOG_I("[Rdebug2]VP irq enable", 0);
                    afe_enable_audio_irq(afe_irq_request_number(memory_block), runtime->rate, runtime->count);
                }
            } else {
                afe_enable_audio_irq(afe_irq_request_number(memory_block), runtime->rate, runtime->count);
            }
        } else {
            afe_set_irq_samplerate(afe_irq_request_number(memory_block), runtime->rate);
            afe_set_irq_counter(afe_irq_request_number(memory_block), runtime->count);
        }
    }

    afe_set_samplerate(memory_block, dl_samplerate);
    afe_set_channels(memory_block, runtime->channel_num);

    if (sink->param.audio.AfeBlkControl.u4awsflag) {
        afe_bt_sync_enable(true, memory_block);
        afe_26m_xtal_cnt_enable(true);
    }
    uint32_t read_reg = afe_get_bt_sync_monitor(AUDIO_DIGITAL_BLOCK_MEM_DL1);

    if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL2) {
        if(sink->param.audio.aws_sync_request != true){
            //DSP_MW_LOG_I("[Rdebug2]VP memory enable", 0);
            afe_set_memory_path_enable(memory_block, !(sink->param.audio.AfeBlkControl.u4awsflag), true);
        }
    }else{
        afe_set_memory_path_enable(memory_block, !(sink->param.audio.AfeBlkControl.u4awsflag), true);
    }

    if ((memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL1) && ((read_reg != 0)||(afe_get_bt_sync_monitor(AUDIO_DIGITAL_BLOCK_MEM_DL1) != 0)) && (afe_get_bt_sync_monitor_state(AUDIO_DIGITAL_BLOCK_MEM_DL1) != 0))
    {
        DSP_MW_LOG_W("Audio Play En may be set before memory path enabled (%d,%d) state:0x%x", 3,read_reg,afe_get_bt_sync_monitor(AUDIO_DIGITAL_BLOCK_MEM_DL1),afe_get_bt_sync_monitor_state(AUDIO_DIGITAL_BLOCK_MEM_DL1));
    }
    if (afe_block->u4asrcflag) {
        if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL1) {
            afe_mem_asrc_enable(AFE_MEM_ASRC_1, true);
        } else if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL2) {
            afe_mem_asrc_enable(AFE_MEM_ASRC_2, true);
        }
    }

    if (sink->param.audio.hw_gain) {
#if 0
        if (sink->param.audio.memory&HAL_AUDIO_MEM1) {
            afe_audio_set_output_digital_gain(AUDIO_HW_GAIN);
        }
        if (sink->param.audio.memory&HAL_AUDIO_MEM2) {
            afe_audio_set_output_digital_gain(AUDIO_HW_GAIN2);
        }
#else
        if (afe_get_hardware_digital_status(AFE_HW_DIGITAL_GAIN1)) {
            afe_audio_set_output_digital_gain(AUDIO_HW_GAIN);
        }
        if (afe_get_hardware_digital_status(AFE_HW_DIGITAL_GAIN2)) {
            afe_audio_set_output_digital_gain(AUDIO_HW_GAIN2);
        }
#endif
    }

    if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL1) {
        afe.dl1_enable = true;
    } else if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL2) {
        afe.dl2_enable = true;
    }
    vRegResetBit(0xA207022C, 8);
    hal_gpt_delay_us(20);
    vRegSetBit(0xA207022C, 8);
    return 0;
}

static int32_t pcm_dl1_stop(SINK sink)
{
    afe_block_t *afe_block = &sink->param.audio.AfeBlkControl;
    audio_digital_block_t memory_block;

    memory_block =  hal_audio_afe_get_memory_digital_block (sink->param.audio.memory, true);
    DSP_MW_LOG_I("DSP audio pcm_dl_stop:%d\r\n", 1, memory_block);

    if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL1) {
        afe.dl1_enable = false;
    } else if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL2) {
        afe.dl2_enable = false;
    }

    afe_disable_audio_irq(afe_irq_request_number(memory_block));
    if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL2) {
        if(afe_get_memory_path_enable(AUDIO_DIGITAL_BLOCK_MEM_DL2)){
            //DSP_MW_LOG_I("[Rdebug2]VP have start memory & irq disable", 0);
            afe_disable_audio_irq(afe_irq_request_number(memory_block));
            afe_set_memory_path_enable(memory_block, !(sink->param.audio.AfeBlkControl.u4awsflag), false);
        }
    }else{
        afe_set_memory_path_enable(memory_block, !(sink->param.audio.AfeBlkControl.u4awsflag), false);
    }

    if (afe_block->u4asrcflag) {
        if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL1) {
            afe_mem_asrc_enable(AFE_MEM_ASRC_1, false);
            afe_set_asrc_enable(false, AFE_MEM_ASRC_1, NULL);
        } else if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_DL2) {
            afe_mem_asrc_enable(AFE_MEM_ASRC_2, false);
            afe_set_asrc_enable(false, AFE_MEM_ASRC_2, NULL);
        }
    }

    if (sink->param.audio.AfeBlkControl.u4awsflag) {
        afe_bt_sync_enable(false, memory_block);
        afe_26m_xtal_cnt_enable(false);
    }


    hal_audio_afe_clock_off();
    return 0;
}

static int32_t pcm_dl1_hw_params(SINK sink)
{
    UNUSED(sink);
    return 0;
}

static int32_t pcm_dl1_open(SINK sink)
{
    AUDIO_PARAMETER *runtime = &sink->param.audio;
    hal_audio_device_t device = sink->param.audio.audio_device;//hal_audio_get_stream_out_device();
    uint32_t sink_ch;
    uint32_t samplerate;
    DSP_MW_LOG_I("DSP audio pcm_dl_open:%d\r\n", 1, hal_audio_afe_get_memory_digital_block (sink->param.audio.memory, true));

    hal_audio_afe_clock_on();
    #if 0
    uint32_t stream_ch;
    TRANSFORM transform = sink->transform;
    DSP_CALLBACK_PTR callback_ptr = NULL;
    callback_ptr = DSP_Callback_Get(transform->source, sink);
    stream_ch = (callback_ptr->EntryPara.out_channel_num > 2)
                ? 1
                : callback_ptr->EntryPara.out_channel_num - 1;

    sink_ch = (runtime->channel_num > 2)
                    ? 1
                    :runtime->channel_num - 1;
    runtime->connect_channel_type = connect_type[stream_ch][sink_ch];
    #else
    sink_ch = (runtime->channel_num >= 2)
                ? 1
                : 0;

    runtime->connect_channel_type = connect_type[sink_ch][sink_ch];
    #endif

    /*set interconnection*/
    DSP_MW_LOG_I("DSP audio pcm_dl1_open channel_type:%d \r\n", 1, runtime->connect_channel_type);
    hal_audio_afe_set_connection(runtime, false, true);

    afe_audio_device_enable(true, device, runtime->audio_interface, runtime->memory, runtime->format, runtime->rate, runtime->misc_parms);

    if (runtime->hw_gain) {
        if (runtime->memory == HAL_AUDIO_MEM1) {
            afe_set_hardware_digital_gain_mode(AFE_HW_DIGITAL_GAIN1, afe_get_audio_device_samplerate(device, runtime->audio_interface));
            if ((afe.amp_handle->get_dac_rate_change_status == NULL) || (afe.amp_handle->set_hw_gain_id_handler == NULL) ||
                (afe.amp_handle->get_dac_rate_change_status(&samplerate) == false) || (afe.amp_handle->set_hw_gain_id_handler(1) == false)) {
                afe_enable_hardware_digital_gain(AFE_HW_DIGITAL_GAIN1, true);
            }
        } else {
            afe_set_hardware_digital_gain_mode(AFE_HW_DIGITAL_GAIN2, afe_get_audio_device_samplerate(device, runtime->audio_interface));
            if ((afe.amp_handle->get_dac_rate_change_status == NULL) || (afe.amp_handle->set_hw_gain_id_handler == NULL) ||
                (afe.amp_handle->get_dac_rate_change_status(&samplerate) == false) || (afe.amp_handle->set_hw_gain_id_handler(2) == false)) {
                afe_enable_hardware_digital_gain(AFE_HW_DIGITAL_GAIN2, true);
            }
        }
    }

    return 0;
}

static int32_t pcm_dl1_close(SINK sink)
{
    hal_audio_device_t device = sink->param.audio.audio_device;
    DSP_MW_LOG_I("DSP audio pcm_dl_close:%d\r\n", 1, hal_audio_afe_get_memory_digital_block (sink->param.audio.memory, true));

    afe_audio_device_enable(false, device, sink->param.audio.audio_interface, sink->param.audio.memory, sink->param.audio.format, sink->param.audio.rate, sink->param.audio.misc_parms);

    if (sink->param.audio.hw_gain) {

        if (sink->param.audio.memory == HAL_AUDIO_MEM1) {
            afe_enable_hardware_digital_gain(AFE_HW_DIGITAL_GAIN1, false);
        } else {
            afe_enable_hardware_digital_gain(AFE_HW_DIGITAL_GAIN2, false);
        }
    }

    /*clear interconnection*/
    hal_audio_afe_set_connection(&sink->param.audio, false, false);

    hal_audio_afe_clock_off();
    return 0;
}

static int32_t pcm_dl1_trigger(SINK sink, int cmd)
{
    switch (cmd) {
        case AFE_PCM_TRIGGER_START:
            return pcm_dl1_start(sink);
            break;
        case AFE_PCM_TRIGGER_STOP:
            return pcm_dl1_stop(sink);
            break;
        case AFE_PCM_TRIGGER_RESUME:
            return pcm_dl1_open(sink);
            break;
        case AFE_PCM_TRIGGER_SUSPEND:
            return pcm_dl1_close(sink);
            break;
        default:
            break;
    }
    return -1;
}

// src: Source Streambuffer not Sink Streambuffer
ATTR_TEXT_IN_IRAM_LEVEL_2 static int32_t pcm_dl1_copy(SINK sink, void *src, uint32_t count)
{
    // count: w/o channl, unit: bytes
    // copy the src's streambuffer to sink's streambuffer
    if (Sink_Audio_WriteBuffer(sink, src, count) == false) {
        return -1;
    }
    return 0;
}

audio_sink_pcm_ops_t afe_platform_dl1_ops = {
    .probe      = pcm_dl1_probe,
    .open       = pcm_dl1_open,
    .close      = pcm_dl1_close,
    .hw_params  = pcm_dl1_hw_params,
    .trigger    = pcm_dl1_trigger,
    .copy       = pcm_dl1_copy,
};
