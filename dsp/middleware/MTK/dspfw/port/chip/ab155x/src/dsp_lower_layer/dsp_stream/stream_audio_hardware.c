/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

/*!
 *@file   stream_audio_hardware.c
 *@brief  Defines the hardware control for audio stream
 *
 @verbatim
 @endverbatim
 */

//-
#include "types.h"
#include "audio_config.h"
#include "source_inter.h"
#include "sink_inter.h"
#include "stream_audio_setting.h"
#include "dtm.h"
#include "stream_audio_driver.h"
#include "stream_audio_hardware.h"
#include "hal_audio_afe_control.h"
#include "dsp_audio_ctrl.h"
U16  ADC_SOFTSTART;


#define AUDIO_AFE_DL_DEFAULT_FRAME_NUM 4
#define AUDIO_AFE_UL_DEFAULT_FRAME_NUM 4
#define AUDIO_AFE_SOURCE_ASRC_BUFFER_SIZE 4096

VOID Sink_Audio_Get_Default_Parameters(SINK sink)
{
    AUDIO_PARAMETER *pAudPara = &sink->param.audio;
    afe_pcm_format_t format;
    uint32_t media_frame_samples;

    memset(&sink->param.audio.AfeBlkControl, 0, sizeof(afe_block_t));

    format = gAudioCtrl.Afe.AfeDLSetting.format ;
    /* calculate memory size for delay */
    if (format == AFE_PCM_FORMAT_S32_LE ||
        format == AFE_PCM_FORMAT_U32_LE ||
        format == AFE_PCM_FORMAT_S24_LE ||
        format == AFE_PCM_FORMAT_U24_LE)
        pAudPara->format_bytes = 4;
    else
        pAudPara->format_bytes = 2;

    pAudPara->format        = format;

    pAudPara->channel_num = ((sink->param.audio.channel_sel == AUDIO_CHANNEL_A) ||
                             (sink->param.audio.channel_sel == AUDIO_CHANNEL_B) ||
                             (sink->param.audio.channel_sel == AUDIO_CHANNEL_VP))
                                        ? 1 : 2;

    pAudPara->rate          = gAudioCtrl.Afe.AfeDLSetting.rate;
    pAudPara->src_rate      = gAudioCtrl.Afe.AfeDLSetting.src_rate;
    pAudPara->period        = gAudioCtrl.Afe.AfeDLSetting.period;           /* ms, how many period to trigger */

#if 1   // for FPGA early porting
    if (sink->type == SINK_TYPE_VP_AUDIO) {
        media_frame_samples = Audio_setting->Audio_VP.Frame_Size;
    } else {
        media_frame_samples = Audio_setting->Audio_sink.Frame_Size;//Audio_setting->Audio_sink.Frame_Size;//AUDIO_AAC_FRAME_SAMPLES;
    }
#else
    switch (gAudioCtrl.Afe.OperationMode) {
        case AU_AFE_OP_ESCO_VOICE_MODE:
            media_frame_samples = AUDIO_SBC_FRAME_SAMPLES;  // use mSBC (worst case)
            break;
        case AU_AFE_OP_PLAYBACK_MODE:
            media_frame_samples = AUDIO_AAC_FRAME_SAMPLES; // TODO:
            break;
        default:
            media_frame_samples = AUDIO_SBC_FRAME_SAMPLES;
            break;
    }
#endif

    pAudPara->buffer_size   = AUDIO_AFE_DL_DEFAULT_FRAME_NUM * media_frame_samples *
                              pAudPara->channel_num * pAudPara->format_bytes;

    pAudPara->AfeBlkControl.u4asrc_buffer_size = pAudPara->buffer_size;
    pAudPara->count         = (pAudPara->rate * pAudPara->period) / 1000;

    if (pAudPara->count >= pAudPara->buffer_size) {
        pAudPara->count = pAudPara->buffer_size >> 2;
    }
    if (pAudPara->period == 0) {
        pAudPara->count = media_frame_samples;
        pAudPara->period = media_frame_samples / (pAudPara->rate /1000);
    }
    pAudPara->audio_device                   = gAudioCtrl.Afe.AfeDLSetting.audio_device;
#ifdef ENABLE_2A2D_TEST
    pAudPara->audio_device1                  = HAL_AUDIO_DEVICE_NONE;
    pAudPara->audio_device2                  = HAL_AUDIO_DEVICE_NONE;
    pAudPara->audio_device3                  = HAL_AUDIO_DEVICE_NONE;
#endif
    pAudPara->stream_channel                 = gAudioCtrl.Afe.AfeDLSetting.stream_channel;
    pAudPara->memory                         = gAudioCtrl.Afe.AfeDLSetting.memory;
    pAudPara->audio_interface                = gAudioCtrl.Afe.AfeDLSetting.audio_interface;
#ifdef ENABLE_2A2D_TEST
    pAudPara->audio_interface1               = HAL_AUDIO_INTERFACE_NONE;
    pAudPara->audio_interface2               = HAL_AUDIO_INTERFACE_NONE;
    pAudPara->audio_interface3               = HAL_AUDIO_INTERFACE_NONE;
#endif
    pAudPara->hw_gain                        = gAudioCtrl.Afe.AfeDLSetting.hw_gain;
    pAudPara->echo_reference                 = gAudioCtrl.Afe.AfeDLSetting.echo_reference;
    pAudPara->misc_parms                     = gAudioCtrl.Afe.AfeDLSetting.misc_parms;

    DSP_MW_LOG_I("audio sink default buffer_size:%d, count:%d\r\n", 2, pAudPara->buffer_size, pAudPara->count);
    DSP_MW_LOG_I("audio sink default device:%d, channel:%d, memory:%d, interface:%d\r\n", 4, pAudPara->audio_device,
                                                                                    pAudPara->stream_channel,
                                                                                    pAudPara->memory,
                                                                                    pAudPara->audio_interface);
}

VOID Source_Audio_Get_Default_Parameters(SOURCE source)
{
    AUDIO_PARAMETER *pAudPara = &source->param.audio;
    afe_pcm_format_t format;
    uint32_t media_frame_samples;
#ifdef ENABLE_2A2D_TEST
    U8  channel_num;
#endif

    memset(&source->param.audio.AfeBlkControl, 0, sizeof(afe_block_t));

    format = gAudioCtrl.Afe.AfeULSetting.format ;
    /* calculate memory size for delay */
    if (format == AFE_PCM_FORMAT_S32_LE ||
        format == AFE_PCM_FORMAT_U32_LE ||
        format == AFE_PCM_FORMAT_S24_LE ||
        format == AFE_PCM_FORMAT_U24_LE)
        pAudPara->format_bytes = 4;
    else
        pAudPara->format_bytes = 2;

    pAudPara->format        = format;


    if((pAudPara->channel_sel == AUDIO_CHANNEL_A) || (pAudPara->channel_sel == AUDIO_CHANNEL_B))
        pAudPara->channel_num = 1;
    else if (pAudPara->channel_sel == AUDIO_CHANNEL_A_AND_B)
        pAudPara->channel_num = 2;
    else if (pAudPara->channel_sel == AUDIO_CHANNEL_3ch)
        pAudPara->channel_num = 3;
    else if (pAudPara->channel_sel == AUDIO_CHANNEL_4ch)
        pAudPara->channel_num = 4;

    //printf("[source defualt] channel_num: %d", pAudPara->channel_num);

    /*pAudPara->channel_num = ((pAudPara->channel_sel == AUDIO_CHANNEL_A) ||
                             (pAudPara->channel_sel == AUDIO_CHANNEL_B))
                                             ? 1 : 2 ;*/

   // pAudPara->channel_num   = 1;
    pAudPara->rate          = gAudioCtrl.Afe.AfeULSetting.rate;
    pAudPara->src_rate      = gAudioCtrl.Afe.AfeULSetting.src_rate;
    pAudPara->period        = gAudioCtrl.Afe.AfeULSetting.period;

    // for early porting
    media_frame_samples = Audio_setting->Audio_source.Frame_Size;//AUDIO_AAC_FRAME_SAMPLES;
#ifdef ENABLE_2A2D_TEST
    channel_num = (pAudPara->channel_num>=2) ? 2 : 1;

    pAudPara->buffer_size   = media_frame_samples*Audio_setting->Audio_source.Buffer_Frame_Num*channel_num*pAudPara->format_bytes;

#else
    pAudPara->buffer_size   = media_frame_samples*Audio_setting->Audio_source.Buffer_Frame_Num*pAudPara->channel_num*pAudPara->format_bytes;

#endif

    pAudPara->AfeBlkControl.u4asrc_buffer_size = AUDIO_AFE_SOURCE_ASRC_BUFFER_SIZE;//AUDIO_AFE_BUFFER_SIZE;//pAudPara->buffer_size;
    //pAudPara->buffer_size   = AUDIO_SOURCE_DEFAULT_FRAME_NUM * media_frame_samples *
    //                          pAudPara->channel_num * pAudPara->format_bytes;
// printf("===> %d %d %d %d = %d\r\n", AUDIO_SOURCE_DEFAULT_FRAME_NUM, media_frame_samples, pAudPara->channel_num,pAudPara->format_bytes,   pAudPara->buffer_size);
    pAudPara->count         = (pAudPara->rate * pAudPara->period) / 1000;
    if (pAudPara->count >= pAudPara->buffer_size) {
        pAudPara->count = pAudPara->buffer_size >> 2;
    }
    if (pAudPara->period == 0) {
        pAudPara->count = media_frame_samples;
        pAudPara->period = media_frame_samples / (pAudPara->rate /1000);
    }
    pAudPara->audio_device                   = gAudioCtrl.Afe.AfeULSetting.audio_device;
#ifdef ENABLE_2A2D_TEST
    pAudPara->audio_device1                  = gAudioCtrl.Afe.AfeULSetting.audio_device1;
    pAudPara->audio_device2                  = gAudioCtrl.Afe.AfeULSetting.audio_device2;
    pAudPara->audio_device3                  = gAudioCtrl.Afe.AfeULSetting.audio_device3;
#endif
    pAudPara->stream_channel                 = gAudioCtrl.Afe.AfeULSetting.stream_channel;
    pAudPara->memory                         = gAudioCtrl.Afe.AfeULSetting.memory;
    pAudPara->audio_interface                = gAudioCtrl.Afe.AfeULSetting.audio_interface;
#ifdef ENABLE_2A2D_TEST
    pAudPara->audio_interface1               = gAudioCtrl.Afe.AfeULSetting.audio_interface1;
    pAudPara->audio_interface2               = gAudioCtrl.Afe.AfeULSetting.audio_interface2;
    pAudPara->audio_interface3               = gAudioCtrl.Afe.AfeULSetting.audio_interface3;
#endif
    pAudPara->hw_gain                        = gAudioCtrl.Afe.AfeULSetting.hw_gain;
    pAudPara->echo_reference                 = gAudioCtrl.Afe.AfeULSetting.echo_reference;
    pAudPara->misc_parms                     = gAudioCtrl.Afe.AfeULSetting.misc_parms;

    DSP_MW_LOG_I("audio source default buffer_size:%d, count:%d\r\n", 2, pAudPara->buffer_size, pAudPara->count);
    DSP_MW_LOG_I("audio source default device:%d, channel_num:%d, memory:%d, interface:%d\r\n", 4, pAudPara->audio_device,
                                                                                      pAudPara->channel_num,
                                                                                      pAudPara->memory,
                                                                                      pAudPara->audio_interface);
}


VOID Sink_Audio_HW_Init_AFE(SINK sink)
{
    // do .hw_params()
    // according to sink to init the choosen AFE IO block
    // 1) hw_type
    // 2) channel
    // 3) mem allocate

    // TODO: AFE Clock init here <----

    if (audio_ops_probe(sink)) {
        DSP_MW_LOG_E("audio sink type : %d probe error\r\n", 1, sink->type);
    }
    if (audio_ops_hw_params(sink)) {
        DSP_MW_LOG_E("audio sink type : %d setting hw_params error\r\n", 1, sink->type);
    }

    switch (sink->param.audio.HW_type)
    {
        case  AUDIO_HARDWARE_PCM :
            //printf_james("Sink_Audio_HW_Init\r\n");
            break;
        case AUDIO_HARDWARE_I2S_M ://I2S master
            //#warning "To do I2S master interface later"
            break;
        case AUDIO_HARDWARE_I2S_S ://I2S slave
            //#warning "To do I2S slave interface later"
            break;
        default:
            configASSERT(0);
            break;
    }
}

VOID Sink_Audio_HW_Init(SINK sink)
{
    UNUSED(sink);
    // DSP_DRV_oDFE_CLK_INIT();
    // switch (sink->param.audio.HW_type)
    // {
        // case  AUDIO_HARDWARE_PCM :
            // AUDIO_DFE.OUT_SET0.field.ODFE_THRP_RATE_SEL = ODFE_RATE_ALIGN_DAC;
            // Audio_setting->Rate.Sink_Output_Sampling_Rate = FS_RATE_96K;
            // break;
        // case AUDIO_HARDWARE_I2S_M ://I2S master
            // AUDIO_DFE.OUT_SET0.field.ODFE_THRP_RATE_SEL = ODFE_RATE_ALIGN_I2S_MASTER;
            // AUDIO_DFE.OUT_SET0.field.I2S_MASTER_SDO_MUX = 0;
            // break;
        // case AUDIO_HARDWARE_I2S_S ://I2S slave
            // AUDIO_DFE.OUT_SET0.field.ODFE_THRP_RATE_SEL = ODFE_RATE_ALIGN_I2S_SLAVE;
            // AUDIO_DFE.OUT_SET0.field.I2S_SLAVE_SDO_MUX = 0;
            // break;
        // case AUDIO_HARDWARE_SPDIF :
            // AUDIO_DFE.OUT_SET0.field.ODFE_THRP_RATE_SEL = ODFE_RATE_ALIGN_SPDIF_TX;
            // AUDIO_DFE.OUT_SET0.field.SPDIF_TX_MUX = 0;
            // break;
    // }
}


VOID Source_Audio_HW_Init(SOURCE source)
{
    // TODO: AFE Clock init here <----
    if (audio_ops_probe(source))
        DSP_MW_LOG_E("audio source type : %d probe error\r\n", 1, source->type);

    if (audio_ops_hw_params(source))
        DSP_MW_LOG_E("audio source type : %d setting hw_params error\r\n", 1, source->type);

}


VOID AudioSourceHW_Ctrl (SOURCE source, BOOL IsEnabled)
{
    UNUSED(source);
    UNUSED(IsEnabled);
    /*U8 I2S_ModeSel,I2S_SampleRate;
    audio_hardware HW_type = (audio_hardware)source->param.audio.HW_type;

    I2S_ModeSel = (U8)I2S_RX_MODE;
    if (Sink_blks[SINK_TYPE_AUDIO] != NULL)
    {
        I2S_ModeSel = (Sink_blks[SINK_TYPE_AUDIO]->param.audio.HW_type == HW_type)? (U8)I2S_TRX_MODE : (U8)I2S_RX_MODE;
    }
    if (IsEnabled)
    {
        //Enable WADMA
        DSP_DRV_SOURCE_WADMA(source, IsEnabled);

        //Enable iDFE
        if(source->type == SOURCE_TYPE_AUDIO)
        {
            DSP_DRV_iDFE_DEC3_INIT( Audio_setting->Rate.Source_DownSampling_Ratio, source->param.audio.channel_num, Audio_setting->resolution.AudioInRes);
        }

        //Enable audio Interface
        switch (HW_type)
        {
            case AUDIO_HARDWARE_PCM:
                DSP_AD_IN_INIT();
                break;

            case AUDIO_HARDWARE_I2S_M:
                if (Audio_setting->Rate.Source_Input_Sampling_Rate > FS_RATE_48K)
                {
                    I2S_SampleRate = I2S_FS_RATE_96K;
                    Audio_setting->Rate.Source_Input_Sampling_Rate = FS_RATE_96K;
                }
                else
                {
                    I2S_SampleRate = I2S_FS_RATE_48K;
                    Audio_setting->Rate.Source_Input_Sampling_Rate = FS_RATE_48K;
                }
                DSP_DRV_I2S_MS_INIT(I2S_SampleRate ,I2S_WORD_LEN_32BIT,I2S_WORD_LEN_32BIT, I2S_WORD_LEN_32BIT ,I2S_ModeSel);
                break;

            case AUDIO_HARDWARE_I2S_S:
                DSP_DRV_I2S_SL_INIT(I2S_WORD_LEN_32BIT,I2S_WORD_LEN_32BIT,I2S_WORD_LEN_32BIT,I2S_ModeSel);
                break;
            case AUDIO_HARDWARE_SPDIF:
                DSP_DRV_SPDIF_RX_INIT();
                break;
            default:
                break;
        }
    }
    else
    {
        //Disable audio Interface
        switch (HW_type)
        {
            case AUDIO_HARDWARE_PCM:
                DSP_AD_IN_END();
                break;

            case AUDIO_HARDWARE_I2S_M :
                if (I2S.SET0.field.I2S_TR_MODE_CTL == I2S_RX_MODE)
                {
                    DSP_DRV_I2S_MS_END();
                }
                else
                {
                    I2S.SET0.field.I2S_TR_MODE_CTL = I2S_TX_MODE;
                }
                break;

            case AUDIO_HARDWARE_I2S_S :
                if (I2S.SET1.field.I2S_TR_MODE_CTL == I2S_RX_MODE)
                {
                    DSP_DRV_I2S_SL_END();
                }
                else
                {
                    I2S.SET1.field.I2S_TR_MODE_CTL = I2S_TX_MODE;
                }
                break;
            case AUDIO_HARDWARE_SPDIF:
                DSP_DRV_SPDIF_RX_END();
                break;
            default:
                break;
        }

        //Disable iDFE
        if(source->type == SOURCE_TYPE_AUDIO)
        {
            DSP_DRV_iDFE_DEC3_END();
        }

        //Disable WADMA
        DSP_DRV_SOURCE_WADMA(source, IsEnabled);
    }*/
}


VOID AudioSinkHW_Ctrl (SINK sink, BOOL IsEnabled)
{
    UNUSED(sink);
    UNUSED(IsEnabled);
    /*audio_hardware HW_type = sink->param.audio.HW_type;
    U8 I2S_ModeSel,samplingRate;

    I2S_ModeSel = (U8)I2S_TX_MODE;
    if (Source_blks[SOURCE_TYPE_AUDIO] != NULL)
    {
        I2S_ModeSel = (Source_blks[SOURCE_TYPE_AUDIO]->param.audio.HW_type == HW_type)? (U8)I2S_TRX_MODE : (U8)I2S_TX_MODE;
    }

    if (IsEnabled)
    {
        //Enable RADMA
        DSP_DRV_SINK_RADMA(sink, IsEnabled);

        //Enable SRC
        if (sink->type == SINK_TYPE_AUDIO && Audio_setting->Audio_sink.SRC_Out_Enable)
        {
            DSP_DRV_SRC_A_INIT (DSP_FsChange2SRCInRate(Audio_setting->Rate.SRC_Sampling_Rate),
                                DSP_FsChange2SRCOutRate(AudioSinkSamplingRate_Get()),
                                Audio_setting->resolution.SRCInRes,
                                Audio_setting->resolution.AudioOutRes);
        }

        //Enable oDFE
        if(sink->type == SINK_TYPE_AUDIO)
        {
            DSP_DRV_oDFE_INT4_INIT(Audio_setting->Rate.Sink_UpSampling_Ratio,
                                   sink->param.audio.channel_num,
                                   (Audio_setting->Audio_sink.SRC_Out_Enable)^1,
                                   Audio_setting->Audio_sink.CIC_Filter_Enable,
                                   Audio_setting->resolution.AudioOutRes);
        }
        else if (sink->type == SINK_TYPE_VP_AUDIO)
        {

            DSP_DRV_oDFE_INT6_INIT(Audio_setting->Rate.VP_UpSampling_Ratio,
                                   WITH_CIC,
                                   Audio_setting->resolution.AudioOutRes); //Shall able to adjust sampling rate with audio sink
        }

        //Enable audio Interface
        switch (HW_type)
        {
            case AUDIO_HARDWARE_PCM:
                if (AUDIO_CODEC.CTL0.field.EN_AU_DAC_DSM == 0)
                {
                    DSP_DA_OUT_INIT(sink->param.audio.channel_num);
                }
                break;

            case AUDIO_HARDWARE_I2S_M :
                if (Audio_setting->Rate.Sink_Output_Sampling_Rate > FS_RATE_48K)
                {
                    samplingRate = I2S_FS_RATE_96K;
                    Audio_setting->Rate.Sink_Output_Sampling_Rate = FS_RATE_96K;
                }
                else
                {
                    samplingRate = I2S_FS_RATE_48K;
                    Audio_setting->Rate.Sink_Output_Sampling_Rate = FS_RATE_48K;
                }
                DSP_DRV_I2S_MS_INIT(samplingRate ,I2S_WORD_LEN_32BIT,I2S_WORD_LEN_32BIT, I2S_WORD_LEN_32BIT ,I2S_ModeSel);
                break;

            case AUDIO_HARDWARE_I2S_S :
                DSP_DRV_I2S_SL_INIT(I2S_WORD_LEN_32BIT,I2S_WORD_LEN_32BIT,I2S_WORD_LEN_32BIT,I2S_ModeSel);
                break;
            case AUDIO_HARDWARE_SPDIF :
                samplingRate = DSP_ChangeFs2SpdifRate(Audio_setting->Rate.Sink_Output_Sampling_Rate);
                DSP_DRV_SPDIF_TX_INIT(samplingRate);
                break;
            default:
                break;
        }
    }
    else
    {
        //Disable audio Interface
        switch (HW_type)
        {
            case AUDIO_HARDWARE_PCM:
                DSP_DA_OUT_END();
                break;

            case AUDIO_HARDWARE_I2S_M :
                if (I2S.SET0.field.I2S_TR_MODE_CTL == I2S_TX_MODE)
                {
                    DSP_DRV_I2S_MS_END();
                }
                else
                {
                    I2S.SET0.field.I2S_TR_MODE_CTL = I2S_RX_MODE;
                }
                break;

            case AUDIO_HARDWARE_I2S_S :
                if (I2S.SET1.field.I2S_TR_MODE_CTL == I2S_TX_MODE)
                {
                    DSP_DRV_I2S_SL_END();
                }
                else
                {
                    I2S.SET1.field.I2S_TR_MODE_CTL = I2S_RX_MODE;
                }
                break;
            case AUDIO_HARDWARE_SPDIF:
                DSP_DRV_SPDIF_TX_END();
                break;
            default:
                break;
        }

        //Disable oDFE
        if(sink->type == SINK_TYPE_AUDIO)
        {
            DSP_DRV_oDFE_INT4_END();
        }
        else if (sink->type == SINK_TYPE_VP_AUDIO)
        {
            DSP_DRV_oDFE_INT6_END();
        }


        //Disable SRC
        if (sink->type == SINK_TYPE_AUDIO && Audio_setting->Audio_sink.SRC_Out_Enable)
        {
            DSP_DRV_SRC_A_END();
        }

        //Disable RADMA
        DSP_DRV_SINK_RADMA(sink, IsEnabled);
    }

    if (sink->type == SINK_TYPE_AUDIO)
        Audio_setting->Audio_sink.Output_Enable = IsEnabled;
    else if (sink->type == SINK_TYPE_VP_AUDIO)
        Audio_setting->Audio_VP.Output_Enable = IsEnabled;

    #if 0
    if ((Audio_setting->Audio_sink.Output_Enable==FALSE) &&
        (Audio_setting->Audio_VP.Output_Enable==FALSE))
        DSP_DRV_DisableAudioDfeClock();
    #endif*/
}

