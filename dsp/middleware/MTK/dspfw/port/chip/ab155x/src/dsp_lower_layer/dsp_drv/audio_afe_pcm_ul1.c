/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "source.h"
#include "stream_audio_setting.h"
#include "hal_nvic.h"
#include "FreeRTOS.h"
#include "stream_audio_driver.h"
#include "dsp_callback.h"
#include "dsp_audio_ctrl.h"
#include "hal_audio_afe_control.h"
#include "hal_audio_afe_define.h"
#include "hal_audio_afe_clock.h"
#include "audio_afe_common.h"

extern afe_stream_channel_t connect_type[2][2];
extern afe_t afe;
extern audio_source_pcm_ops_t afe_platform_awb_ops;

static void ul1_global_var_init(SOURCE source)
{
    #if 0
    afe_block_t *afe_block = &source->param.audio.AfeBlkControl;
    memset(afe_block, 0, sizeof(afe_block_t));
    #else
    UNUSED(source);
    #endif
}

static int32_t pcm_ul1_probe(SOURCE source)
{
    ul1_global_var_init(source);
    return 0;
}

static int32_t pcm_ul1_start(SOURCE source)
{
    AUDIO_PARAMETER *runtime = &source->param.audio;
    audio_digital_block_t memory_block;
    DSP_MW_LOG_I("DSP audio pcm_ul1_start\r\n", 0);
    hal_audio_afe_clock_on();

    memory_block =  hal_audio_afe_get_memory_digital_block (source->param.audio.memory, false);

    /* set irq start */
    if(!source->param.audio.AfeBlkControl.u4awsflag) {
        afe_enable_audio_irq(afe_irq_request_number(memory_block), runtime->rate, runtime->count);
    }

#ifdef ENABLE_2A2D_TEST
    //1A1D use VUL1
    if(runtime->audio_device1 != HAL_AUDIO_DEVICE_NONE) {
        memory_block = AUDIO_DIGITAL_BLOCK_MEM_VUL1;
    }
#endif
    afe_set_samplerate(memory_block, runtime->rate);
    afe_set_channels(memory_block, runtime->channel_num);

#ifdef ENABLE_2A2D_TEST
    //2A1D or 2A2D use VUL2
    if(runtime->audio_device2 != HAL_AUDIO_DEVICE_NONE) {
        afe_set_samplerate(AUDIO_DIGITAL_BLOCK_MEM_VUL2, runtime->rate);
        afe_set_channels(AUDIO_DIGITAL_BLOCK_MEM_VUL2, runtime->channel_num);
    }
#endif

    if (!source->param.audio.AfeBlkControl.u4awsflag) {
        afe_set_memory_path_enable(memory_block, true, true);
#ifdef ENABLE_2A2D_TEST
        if(runtime->audio_device2 != HAL_AUDIO_DEVICE_NONE) {
            afe_set_memory_path_enable(AUDIO_DIGITAL_BLOCK_MEM_VUL2, true, true);
        }
#endif
    }


    if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_VUL1) {
        afe.ul1_enable = true;
    }
    return 0;
}

static int32_t pcm_ul1_stop(SOURCE source)
{
    afe_block_t *afe_block = &source->param.audio.AfeBlkControl;
    audio_digital_block_t memory_block;
    DSP_MW_LOG_I("DSP audio pcm_ul1_stop\r\n", 0);
    memory_block =  hal_audio_afe_get_memory_digital_block (source->param.audio.memory, false);

    afe_disable_audio_irq(afe_irq_request_number(memory_block));

#ifdef ENABLE_2A2D_TEST
    //1A1D use VUL1
    if(source->param.audio.audio_device1 != HAL_AUDIO_DEVICE_NONE) {
        memory_block = AUDIO_DIGITAL_BLOCK_MEM_VUL1;
    }
#endif

    afe_set_memory_path_enable(memory_block, true, false);

#ifdef ENABLE_2A2D_TEST
    //2A1D or 2A2D use VUL2
    if(source->param.audio.audio_device2 != HAL_AUDIO_DEVICE_NONE) {
        afe_set_memory_path_enable(AUDIO_DIGITAL_BLOCK_MEM_VUL2, true, false);
    }
#endif

    afe_clear_memory_block(afe_block, memory_block);

    if (memory_block == AUDIO_DIGITAL_BLOCK_MEM_VUL1) {
        afe.ul1_enable = false;
    }
    hal_audio_afe_clock_off();
    return 0;
}

static int32_t pcm_ul1_hw_params(SOURCE source)
{
    UNUSED(source);
    return 0;
}

static int32_t pcm_ul1_open(SOURCE source)
{
    uint32_t source_ch;
    AUDIO_PARAMETER *runtime = &source->param.audio;
    hal_audio_device_t device = source->param.audio.audio_device;//hal_audio_get_stream_in_device();
#ifdef ENABLE_2A2D_TEST
    hal_audio_device_t device1 = source->param.audio.audio_device1;
    hal_audio_device_t device2 = source->param.audio.audio_device2;
    hal_audio_device_t device3 = source->param.audio.audio_device3;
#endif
    int32_t ret = 0;
    DSP_MW_LOG_I("DSP audio pcm_ul1_open\r\n", 0);

    hal_audio_afe_clock_on();
    #if 0
    TRANSFORM transform = source->transform;
    DSP_CALLBACK_PTR callback_ptr = NULL;
    audio_afe_io_block_t audio_block_in, audio_block_out;
    uint32_t stream_ch, source_ch;
    callback_ptr = DSP_Callback_Get(source, transform->sink);
    stream_ch = (callback_ptr->EntryPara.in_channel_num >2)
                  ? 1
                  : callback_ptr->EntryPara.in_channel_num - 1;
    source_ch = (runtime->channel_num > 2)
                  ? 1
                  : runtime->channel_num - 1;
    runtime->connect_channel_type = connect_type[stream_ch][source_ch];
    #else
    source_ch = (runtime->channel_num > 2)
                  ? 1
                  : runtime->channel_num - 1;
    runtime->connect_channel_type = connect_type[source_ch][source_ch];
    #endif
    /*set interconnection*/
    DSP_MW_LOG_I("DSP audio pcm_ul1_open channel_type:%d \r\n", 1, runtime->connect_channel_type);
    hal_audio_afe_set_connection(runtime, true, true);

    #if 0
    //UpLink use software gain control
    if (runtime->hw_gain) {
        afe_set_hardware_digital_gain_mode(AFE_HW_DIGITAL_GAIN2, runtime->rate, 20);
        afe_enable_hardware_digital_gain(AFE_HW_DIGITAL_GAIN2, true);
    }
    #endif


    afe_audio_device_enable(true, device, runtime->audio_interface, runtime->memory, runtime->format, runtime->rate, runtime->misc_parms);

#ifdef ENABLE_2A2D_TEST
    if (device1 != HAL_AUDIO_DEVICE_NONE) {
        afe_audio_device_enable(true, device1, runtime->audio_interface1, runtime->memory, runtime->format, runtime->rate, runtime->misc_parms);
    }
    if (device2 != HAL_AUDIO_DEVICE_NONE) {
        afe_audio_device_enable(true, device2, runtime->audio_interface2, runtime->memory, runtime->format, runtime->rate, runtime->misc_parms);
    }
    if (device3 != HAL_AUDIO_DEVICE_NONE) {
        afe_audio_device_enable(true, device3, runtime->audio_interface3, runtime->memory, runtime->format, runtime->rate, runtime->misc_parms);
    }
#endif
    if (source->param.audio.echo_reference == true) {
        ret = afe_platform_awb_ops.open(source);
    }

    return ret;
}

static int32_t pcm_ul1_close(SOURCE source)
{
    int32_t ret = 0;
    hal_audio_device_t device = source->param.audio.audio_device;
#ifdef ENABLE_2A2D_TEST
    hal_audio_device_t device1 = source->param.audio.audio_device1;
    hal_audio_device_t device2 = source->param.audio.audio_device2;
    hal_audio_device_t device3 = source->param.audio.audio_device3;
#endif
    DSP_MW_LOG_I("DSP audio pcm_ul1_close\r\n", 0);

    afe_audio_device_enable(false, device, source->param.audio.audio_interface, source->param.audio.memory, source->param.audio.format, source->param.audio.rate, source->param.audio.misc_parms);
#ifdef ENABLE_2A2D_TEST
    if (device1 != HAL_AUDIO_DEVICE_NONE) {
        afe_audio_device_enable(false, device1, source->param.audio.audio_interface1, source->param.audio.memory, source->param.audio.format, source->param.audio.rate, source->param.audio.misc_parms);
    }
    if (device2 != HAL_AUDIO_DEVICE_NONE) {
        afe_audio_device_enable(false, device2, source->param.audio.audio_interface2, source->param.audio.memory, source->param.audio.format, source->param.audio.rate, source->param.audio.misc_parms);
    }
    if (device3 != HAL_AUDIO_DEVICE_NONE) {
        afe_audio_device_enable(false, device3, source->param.audio.audio_interface3, source->param.audio.memory, source->param.audio.format, source->param.audio.rate, source->param.audio.misc_parms);
    }
#endif
    #if 0
    //UpLink use software gain control
    if (source->param.audio.hw_gain) {
        afe_enable_hardware_digital_gain(AFE_HW_DIGITAL_GAIN2, false);
    }
    #endif

    /*clear interconnection*/
    hal_audio_afe_set_connection(&source->param.audio, true, false);



    if (source->param.audio.echo_reference == true) {
        ret = afe_platform_awb_ops.close(source);
    }

    hal_audio_afe_clock_off();
    return ret;
}

static int32_t pcm_ul1_trigger(SOURCE source, int cmd)
{
    int32_t ret = 0;

    switch (cmd) {
        case AFE_PCM_TRIGGER_START:
            ret = pcm_ul1_start(source);
            if ((ret == 0)&& (source->param.audio.echo_reference == true)) {
                ret = afe_platform_awb_ops.trigger(source, cmd);
            }
            break;
        case AFE_PCM_TRIGGER_STOP:
            if (source->param.audio.echo_reference == true) {
                ret = afe_platform_awb_ops.trigger(source, cmd);
            }
            if (ret == 0) {
                ret = pcm_ul1_stop(source);
            }
            break;
        case AFE_PCM_TRIGGER_RESUME:
            ret = pcm_ul1_open(source);
            if ((ret == 0)&& (source->param.audio.echo_reference == true)) {
                ret = afe_platform_awb_ops.trigger(source, cmd);
            }
            break;
        case AFE_PCM_TRIGGER_SUSPEND:
            if (source->param.audio.echo_reference == true) {
                ret = afe_platform_awb_ops.trigger(source, cmd);
            }
            if (ret == 0) {
                ret = pcm_ul1_close(source);
            }
            break;
        default:
            ret = -1;
            break;
    }
    if (ret != 0) {
        DSP_MW_LOG_I("pcm_ul1_trigger %d, error:%d\r\n", 2, cmd, ret);
    }
    return ret;
}

ATTR_TEXT_IN_IRAM_LEVEL_2 static int32_t pcm_ul1_copy(SOURCE source, void *dst, uint32_t count)
{
    //copy the AFE src streambuffer to sink streambuffer
    if (Source_Audio_ReadAudioBuffer(source, dst , count) == false) {
        return -1;
    }
    return 0;
}

audio_source_pcm_ops_t afe_platform_ul1_ops = {
    .probe      = pcm_ul1_probe,
    .open       = pcm_ul1_open,
    .close      = pcm_ul1_close,
    .hw_params  = pcm_ul1_hw_params,
    .trigger    = pcm_ul1_trigger,
    .copy       = pcm_ul1_copy,
};
