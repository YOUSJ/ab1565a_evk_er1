/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

//-
#include <string.h>

#include "dsp_sdk.h"

#include "dsp_feature_interface.h"
#include "dsp_stream_connect.h"
#include "dsp_audio_process.h"
#include "cvsd_enc_interface.h"
#include "msbc_enc_interface.h"
#include "dprt_rt.h"
#include "msbc_dec_interface.h"
#ifdef MTK_RECORD_OPUS_ENABLE
#include "opus_encoder_interface.h"
#endif
#include "cvsd_dec_interface.h"
#include "sbc_interface.h"
#include "aac_dec_interface.h"
#include "compander_interface.h"
#ifdef MTK_INHOUSE_ECNR_ENABLE
#include "aec_nr_interface.h"
#endif
#include "clk_skew.h"
#if defined(MTK_PEQ_ENABLE) || defined(MTK_LINEIN_PEQ_ENABLE)
#include "peq_interface.h"
#endif
#ifdef MTK_AUDIO_PLC_ENABLE
#include "audio_plc_interface.h"
#endif
#ifdef MTK_LINEIN_INS_ENABLE
#include "ins_interface.h"
#endif

#include "dsp_vendor_codec_sdk.h"

#include "ch_select_interface.h"
#include "mute_smooth_interface.h"
#ifdef MTK_LEAKAGE_DETECTION_ENABLE
#include "leakage_compensation.h"
#endif
#ifdef MTK_USER_TRIGGER_FF_ENABLE
#include "user_trigger_ff.h"
#endif


////////////////////////////////////////////////////////////////////////////////
// TYPE DEFINITIONS ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
typedef struct stream_codec_sample_instance_u
{
    uint8_t scratch_memory[128];
    uint8_t output[512];
    bool memory_check;
    bool reset_check;
} stream_codec_sample_instance_t, *stream_codec_sample_instance_ptr_t;

typedef struct stream_function_sample_instance_u
{
    uint32_t coefficient_size;
    uint8_t filter[32];
    int16_t buffer_l[512];
    int16_t buffer_r[512];
} stream_function_sample_instance_t, *stream_function_sample_instance_ptr_t;


////////////////////////////////////////////////////////////////////////////////
// Constant Definitions ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#define CODEC_SAMPLE_MEMORY_SIZE        sizeof(stream_codec_sample_instance_t)
#define CODEC_OUTPUT_SIZE               1024

#define FUNCTION_SAMPLE_MEMORY_SIZE     sizeof(stream_function_sample_instance_t)




////////////////////////////////////////////////////////////////////////////////
// Function Prototypes /////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
bool stream_codec_decoder_sample_initialize(void *para);
bool stream_codec_decoder_sample_process(void *para);
bool stream_function_sample_initialize(void *para);
bool stream_function_sample_process(void *para);

uint32_t codec_decoder_sample_api(uint8_t *pattern_pointer, int16_t *l_pointer, int16_t *r_pointer, uint32_t input_length);
uint32_t function_sample_api(stream_function_sample_instance_ptr_t instance_ptr, int16_t *input_l_pointer, int16_t *input_r_pointer, int16_t *output_l_pointer, int16_t *output_r_pointer, uint32_t length);


////////////////////////////////////////////////////////////////////////////////
// Global Variables ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/*===========================================================================================*/
// Codec blocks
// CodecOutSize must have value
stream_feature_table_t stream_feature_table[DSP_FEATURE_MAX_NUM] =
{
    /*====================================================================================    stream feature codec    ====================================================================================*/
    /*feature_type,                                              memory_size,          codec_output_size,                               *initialize_entry,                                  *process_entry*/
    {CODEC_PCM_COPY,                                                       0,                       2048,                      stream_pcm_copy_initialize,                         stream_pcm_copy_process},/*0x00 CODEC_PCM_COPY               */ /* Only copy in_ptr memory to out_ptr memory */
    {CODEC_DECODER_CVSD,                            DSP_CVSD_DECODER_MEMSIZE,  960+DSP_SIZE_FOR_CLK_SKEW,            stream_codec_decoder_cvsd_initialize,               stream_codec_decoder_cvsd_process},/*0x01 CVSD_Decoder,    */   // 120(NB), 240(WB)
    {CODEC_DECODER_MSBC,                            DSP_MSBC_DECODER_MEMSIZE,  960+DSP_SIZE_FOR_CLK_SKEW,            stream_codec_decoder_msbc_initialize,               stream_codec_decoder_msbc_process},/*0x02 CODEC_DECODER_MSBC,      */
    {CODEC_DECODER_SBC,                                DSP_SBC_CODEC_MEMSIZE, 4096+DSP_SIZE_FOR_CLK_SKEW,             stream_codec_decoder_sbc_initialize,                stream_codec_decoder_sbc_process},/*0x03 CODEC_DECODER_SBC,       */
    {CODEC_DECODER_AAC,                              DSP_AAC_DECODER_MEMSIZE, 4096+DSP_SIZE_FOR_CLK_SKEW,             stream_codec_decoder_aac_initialize,                stream_codec_decoder_aac_process},/*0x04 CODEC_DECODER_AAC,       */
    {CODEC_DECODER_MP3,                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x05 _Reserved,       */
    {CODEC_DECODER_EC,                                                     0,                       0xFF,                                            NULL,                                            NULL},/*0x06 _Reserved,       */
    {CODEC_DECODER_UART,                                                   0,                       0xFF,                                            NULL,                                            NULL},/*0x07 _Reserved,       */
    {CODEC_DECODER_UART16BIT,                                              0,                       0xFF,                                            NULL,                                            NULL},/*0x08 _Reserved,       */
    {CODEC_DECODER_CELT_HD,                                                0,                       0xFF,                                            NULL,                                            NULL},/*0x09 _Reserved,       */
    {CODEC_DECODER_VENDOR,                                VENDOR_HANDLE_SIZE, VENDOR_CODEC_OUT_SIZE+DSP_SIZE_FOR_CLK_SKEW,          STREAM_CODEC_DECODER_VENDOR_INITIALIZE,             STREAM_CODEC_DECODER_VENDOR_PROCESS},/*0x0A _CODEC_DECODER_VENDOR,       */
    {CODEC_DECODER_VENDOR_1,                              VENDOR_1_HANDLE_SIZE, VENDOR_1_CODEC_OUT_SIZE+DSP_SIZE_FOR_CLK_SKEW,      STREAM_CODEC_DECODER_VENDOR_1_INITIALIZE,           STREAM_CODEC_DECODER_VENDOR_1_PROCESS},/*0x0B _CODEC_DECODER_VENDOR,       */
    {CODEC_DECODER_VENDOR_2,                              VENDOR_2_HANDLE_SIZE, VENDOR_2_CODEC_OUT_SIZE+DSP_SIZE_FOR_CLK_SKEW,      STREAM_CODEC_DECODER_VENDOR_2_INITIALIZE,           STREAM_CODEC_DECODER_VENDOR_2_PROCESS},/*0x0C _CODEC_DECODER_VENDOR,       */
    {CODEC_DECODER_VENDOR_3,                              VENDOR_3_HANDLE_SIZE, VENDOR_3_CODEC_OUT_SIZE+DSP_SIZE_FOR_CLK_SKEW,      STREAM_CODEC_DECODER_VENDOR_3_INITIALIZE,           STREAM_CODEC_DECODER_VENDOR_3_PROCESS},/*0x0D _CODEC_DECODER_VENDOR,       */
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x0E _Reserved,       */
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x0F _Reserved,       */
    {CODEC_ENCODER_CVSD,                            DSP_CVSD_ENCODER_MEMSIZE,                        240,            stream_codec_encoder_cvsd_initialize,               stream_codec_encoder_cvsd_process},/*0x10 CVSD_Encoder,    */   // 60(NB), 120(WB)
    {CODEC_ENCODER_MSBC,                            DSP_MSBC_ENCODER_MEMSIZE,                        240,            stream_codec_encoder_msbc_initialize,               stream_codec_encoder_msbc_process},/*0x11 CODEC_ENCODER_MSBC,      */
#ifdef MTK_RECORD_OPUS_ENABLE
    {CODEC_ENCODER_OPUS,                                                   0,                        168,                               OPUS_Encoder_Init,                                    OPUS_Encoder},/*0x12  CODEC_ENCODER_OPUS,      */
#else
    {CODEC_ENCODER_OPUS,                                                   0,                       0xFF,                                            NULL,                                            NULL},/*0x12  CODEC_ENCODER_OPUS,      */
#endif
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x13 _Reserved,       */
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x14 _Reserved,       */
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x15 _Reserved,       */
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x16 _Reserved,       */
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x17 _Reserved,       */
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x18 _Reserved,       */
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x19 _Reserved,       */
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x1A _Reserved,       */
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x1B _Reserved,       */
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x1C _Reserved,       */
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x1D _Reserved,       */
    {0,                                                                    0,                       0xFF,                                            NULL,                                            NULL},/*0x1E _Reserved,       */
    /*====================================================================================    stream feature function    ====================================================================================*/
    /*feature_type,                                              memory_size,      0(Must equal to zero),                               *initialize_entry,                                  *process_entry*/
    {DSP_SRC,                                                DSP_SRC_MEMSIZE,                          0,                  stream_function_src_initialize,                     stream_function_src_process},/*0x1F  DSP_SRC,       */
    {FUNC_END,                                                             0,                          0,                  stream_function_end_initialize,                     stream_function_end_process},/*0x20 FUNC_END,                */

    {FUNC_RX_WB_DRC,                                                       0,                          0,      stream_function_drc_voice_rx_wb_initialize,            stream_function_drc_voice_rx_process},/*0x21 FUNC_RX_WB_DRC,          */
    {FUNC_RX_NB_DRC,                                                       0,                          0,      stream_function_drc_voice_rx_nb_initialize,            stream_function_drc_voice_rx_process},/*0x22 FUNC_RX_NB_DRC,          */
    {FUNC_TX_WB_DRC,                                                       0,                          0,      stream_function_drc_voice_tx_wb_initialize,            stream_function_drc_voice_tx_process},/*0x23 FUNC_TX_WB_DRC,       */
    {FUNC_TX_NB_DRC,                                                       0,                          0,      stream_function_drc_voice_tx_nb_initialize,            stream_function_drc_voice_tx_process},/*0x24 FUNC_TX_NB_DRC,       */
#ifdef MTK_INHOUSE_ECNR_ENABLE
    {FUNC_RX_NR,                                                           0,                          0,               stream_function_aec_nr_initialize,                      stream_function_nr_process},/*0x25 FUNC_RX_NR,                */
    {FUNC_TX_NR,                                                           0,                          0,               stream_function_aec_nr_initialize,                     stream_function_aec_process},/*0x26 FUNC_TX_NR,                */
#else
    {FUNC_RX_NR,                                                           0,                          0,                                            NULL,                                            NULL},/*0x25 FUNC_RX_NR,                */
    {FUNC_TX_NR,                                                           0,                          0,                                            NULL,                                            NULL},/*0x26 FUNC_TX_NR,                */
#endif
#ifdef ENABLE_2A2D_TEST
    {FUNC_CLK_SKEW_UL,     DSP_CLK_SKEW_MEMSIZE*5+DSP_CLK_SKEW_TEMP_BUF_SIZE,                          0,    stream_function_clock_skew_uplink_initialize,       stream_function_clock_skew_uplink_process},/*0x27 FUNC_CLK_SKEW_UL,        */
#else
    {FUNC_CLK_SKEW_UL,     DSP_CLK_SKEW_MEMSIZE*3+DSP_CLK_SKEW_TEMP_BUF_SIZE,                          0,    stream_function_clock_skew_uplink_initialize,       stream_function_clock_skew_uplink_process},/*0x27 FUNC_CLK_SKEW_UL,        */
#endif
    {FUNC_CLK_SKEW_DL,     DSP_CLK_SKEW_MEMSIZE*4+DSP_CLK_SKEW_TEMP_BUF_SIZE,                          0,  stream_function_clock_skew_downlink_initialize,     stream_function_clock_skew_downlink_process},/*0x28 FUNC_CLK_SKEW_DL,        */
    {FUNC_MIC_SW_GAIN,                                                     0,                          0,                 stream_function_gain_initialize,                    stream_function_gain_process},/*0x29 FUNC_MIC_SW_GAIN,        */
    {FUNC_PLC,                                         DSP_VOICE_PLC_MEMSIZE,                          0,                  stream_function_plc_initialize,                     stream_function_plc_process},/*0x2A FUNC_PLC,                */
    {FUNC_CLK_SKEW_HFP_DL, DSP_CLK_SKEW_MEMSIZE*2+DSP_CLK_SKEW_TEMP_BUF_SIZE,                          0,  stream_function_clock_skew_downlink_initialize, stream_function_clock_skew_hfp_downlink_process},/*0x2B FUNC_CLK_SKEW_DL,   */
    {FUNC_PROC_SIZE_CONV,                                                  0,                          0,       stream_function_size_converter_initialize,          stream_function_size_converter_process},/*0x2C FUNC_PROC_SIZE_CONV     */ /* Convert Processing size to give fixed given size*/
    {FUNC_JOINT,                                                           0,                          0,                                            NULL,                                            NULL},/*0x2D FUNC_JOINT,       */
    {FUNC_BRANCH,                                                          0,                          0,                                            NULL,                                            NULL},/*0x2E FUNC_BRANCH,       */
    {FUNC_MUTE,                                                            0,                          0,                                            NULL,                                            NULL},/*0x2F FUNC_MUTE,       */
    {FUNC_DRC,                                                             0,                          0,            stream_function_drc_audio_initialize,               stream_function_drc_audio_process},/*0x30 FUNC_DRC,       */
#if defined(MTK_PEQ_ENABLE) || defined(MTK_LINEIN_PEQ_ENABLE)
    {FUNC_PEQ,                                               DSP_PEQ_MEMSIZE,                          0,                  stream_function_peq_initialize,                     stream_function_peq_process},/*0x31 FUNC_PEQ,       */
#else
    {FUNC_PEQ,                                                             0,                          0,                                            NULL,                                            NULL},/*0x31 FUNC_PEQ,       */
#endif
    {FUNC_LPF,                                                             0,                          0,                                            NULL,                                            NULL},/*0x32 FUNC_LPF,       */
    {FUNC_CH_SEL,                                                          0,                          0,stream_function_channel_selector_initialize_a2dp,   stream_function_channel_selector_process_a2dp},/*0x33 FUNC_CH_SEL,       */
    {FUNC_MUTE_SMOOTHER,                                                   0,                          0,          stream_function_mute_smooth_initialize,             stream_function_mute_smooth_process},/*0x34 FUNC_MUTE_SMOOTHER,       */
#ifdef MTK_PEQ_ENABLE
    {FUNC_PEQ2,                                              DSP_PEQ_MEMSIZE,                          0,                 stream_function_peq2_initialize,                    stream_function_peq2_process},/*0x35 FUNC_PEQ2,     */
#else
    {FUNC_PEQ2,                                                            0,                          0,                                            NULL,                                            NULL},/*0x35 FUNC_PEQ2,     */
#endif
    {FUNC_DRC2,                                                            0,                          0,           stream_function_drc_audio2_initialize,              stream_function_drc_audio2_process},/*0x36 FUNC_DRC2,       */
    {FUNC_CH_SEL_HFP,                                                      0,                          0, stream_function_channel_selector_initialize_hfp,    stream_function_channel_selector_process_hfp},/*0x37 FUNC_CH_SEL_HFP,       */
#if (defined(MTK_PEQ_ENABLE) && defined(MTK_DEQ_ENABLE))
    {FUNC_DEQ,                                               DSP_DEQ_MEMSIZE,                          0,                  stream_function_deq_initialize,                     stream_function_deq_process},/*0x38 FUNC_DEQ,     */
    {FUNC_DEQ_MUTE,                                                        0,                          0,             stream_function_deq_mute_initialize,                stream_function_deq_mute_process},/*0x39 FUNC_DEQ_MUTE,     */
#else
    {FUNC_DEQ,                                                             0,                          0,                                            NULL,                                            NULL},/*0x38 FUNC_DEQ,     */
    {FUNC_DEQ_MUTE,                                                        0,                          0,                                            NULL,                                            NULL},/*0x39 FUNC_DEQ_MUTE,     */
#endif
#ifdef MTK_LEAKAGE_DETECTION_ENABLE
    {FUNC_LEAKAGE_COMPENSATION,              DSP_LEAKAGE_COMPENSATION_MEMSIZE,                          0, stream_function_leakage_compensation_initialize,    stream_function_leakage_compensation_process},/*0x3A  FUNC_LEAKAGE_COMPENSATION,       */
#else
    {FUNC_LEAKAGE_COMPENSATION,                                             0,                          0,                                            NULL,                                            NULL},/*0x3A  FUNC_LEAKAGE_COMPENSATION,       */
#endif
#ifdef MTK_PEQ_ENABLE
      {FUNC_PEQ_INSTANT,                                              DSP_PEQ_MEMSIZE,                   0,                  stream_function_peq_initialize,             stream_function_instant_peq_process},/*0x3B FUNC_PEQ_INSTANT,     */
#else
      {FUNC_PEQ_INSTANT,                                                     0,                          0,                                            NULL,                                            NULL},/*0x3B FUNC_PEQ_INSTANT,     */
#endif
#ifdef MTK_AUDIO_PLC_ENABLE
     {FUNC_AUDIO_PLC,                                       AUDIO_PLC_MEMSIZE,                         0,                                   Audio_PLC_Init,                               Audio_PLC_Process},/*0x34 FUNC_AUDIO_PLC,       */
#else
     {FUNC_AUDIO_PLC,                                                       0,                      0xFF,                                             NULL,                                            NULL},/*0x34 FUNC_AUDIO_PLC,       */
#endif
#ifdef MTK_USER_TRIGGER_FF_ENABLE
     {FUNC_USER_TRIGGER_FF,                                                0,                          0,       stream_function_user_trigger_ff_initialize,        stream_function_user_trigger_ff_process},/*0x3D  FUNC_USER_TRIGGER_FF,       */
#else
     {FUNC_USER_TRIGGER_FF,                                                0,                          0,                                             NULL,                                           NULL},/*0x3D  FUNC_USER_TRIGGER_FF,       */
#endif
#ifdef MTK_LINEIN_INS_ENABLE
    {FUNC_INS,                                                             0,                          0,            stream_function_ins_audio_initialize,               stream_function_ins_audio_process},/*0x3E FUNC_INS,       */
#else
    {FUNC_INS,                                                             0,                          0,                                            NULL,                                            NULL},/*0x3E FUNC_INS,       */
#endif
 };
 /*==========================================================================================================================================================================================================*/


                                                         /*             feature_type,                         memory_size,     codec_output_size,                           initialize_entry,                      process_entry*/
stream_feature_codec_t     stream_feature_sample_codec    = {   CODEC_DECODER_SAMPLE,            CODEC_SAMPLE_MEMORY_SIZE,     CODEC_OUTPUT_SIZE,     stream_codec_decoder_sample_initialize, stream_codec_decoder_sample_process};

                                                          /*            feature_type,                         memory_size, 0(Must equal to zero),                           initialize_entry,                  process_entry*/
stream_feature_function_t  stream_feature_sample_function = {            FUNC_SAMPLE,         FUNCTION_SAMPLE_MEMORY_SIZE,                     0,          stream_function_sample_initialize, stream_function_sample_process};



stream_feature_list_t stream_feature_list_hfp_uplink[] =
{
    CODEC_PCM_COPY,
    FUNC_CH_SEL_HFP,
    FUNC_MIC_SW_GAIN,
    FUNC_CLK_SKEW_UL,
#ifdef MTK_INHOUSE_ECNR_ENABLE
    FUNC_TX_NR,
#endif
    FUNC_TX_WB_DRC,
    CODEC_ENCODER_MSBC,
    FUNC_END,
};

stream_feature_list_t stream_feature_list_hfp_downlink[] =
{
    CODEC_DECODER_MSBC,
    FUNC_PLC,
#ifdef MTK_INHOUSE_ECNR_ENABLE
    FUNC_RX_NR,
#endif
    FUNC_RX_WB_DRC,
#if defined(MTK_PEQ_ENABLE) && defined(MTK_DEQ_ENABLE)
    FUNC_DEQ_MUTE,
#endif
    FUNC_CLK_SKEW_HFP_DL,
    FUNC_END,
};

stream_feature_list_t stream_feature_list_a2dp[] =
{
    CODEC_DECODER_SBC,
    FUNC_CH_SEL,
    //FUNC_MUTE_SMOOTHER,
    //FUNC_PROC_SIZE_CONV,
#ifdef MTK_AUDIO_PLC_ENABLE
    FUNC_AUDIO_PLC,
#else
    FUNC_MUTE_SMOOTHER,
#endif
#ifdef MTK_PEQ_ENABLE
    FUNC_PEQ,
    FUNC_DRC,
    FUNC_PEQ2, //for hybrid ANC L/R ch
#ifndef MTK_ANC_ENABLE
    FUNC_DRC2,
#elif (defined(MTK_PEQ_ENABLE) && defined(MTK_ANC_ENABLE) && defined(MTK_DEQ_ENABLE))
    FUNC_DEQ,  //for hybrid ANC R ch
#endif
#endif
    FUNC_CLK_SKEW_DL,
    FUNC_END,

};

stream_feature_list_t stream_feature_list_playback[] =
{
    CODEC_PCM_COPY,
#ifdef MTK_PEQ_ENABLE
    FUNC_PEQ_INSTANT,
    FUNC_DRC,
    FUNC_PEQ2, //for hybrid ANC L/R ch
#ifndef MTK_ANC_ENABLE
    FUNC_DRC2,
#elif (defined(MTK_PEQ_ENABLE) && defined(MTK_ANC_ENABLE) && defined(MTK_DEQ_ENABLE))
    FUNC_DEQ,  //for hybrid ANC R ch
#endif
#endif
    FUNC_END,
};

#ifdef MTK_BT_A2DP_VENDOR_BC_ENABLE
stream_feature_list_t stream_feature_list_vend_a2dp[] =
{
    CODEC_DECODER_VENDOR,
    FUNC_CH_SEL,
    FUNC_END,
};
#endif
stream_feature_list_t stream_feature_list_mic_record[] =
{
    CODEC_PCM_COPY,
    FUNC_MIC_SW_GAIN,
    FUNC_END,
};

stream_feature_list_t stream_feature_list_mic_record_airdump[] =
{
    CODEC_PCM_COPY,
    FUNC_END,
};

#ifdef MTK_RECORD_OPUS_ENABLE
stream_feature_list_t stream_feature_list_opus_mic_record[] =
{
    CODEC_PCM_COPY,
    FUNC_MIC_SW_GAIN,
    CODEC_ENCODER_OPUS,
    FUNC_END,
};
#endif

stream_feature_list_t stream_feature_list_prompt[] =
{
    CODEC_PCM_COPY,
#if defined(MTK_PEQ_ENABLE) && defined(MTK_DEQ_ENABLE)
    FUNC_DEQ_MUTE,
#endif
    FUNC_END,
};

stream_feature_list_t stream_feature_list_linein[] =
{
    CODEC_PCM_COPY,
    FUNC_MIC_SW_GAIN,
#ifdef MTK_LINEIN_INS_ENABLE
    FUNC_INS,
#endif
#ifdef MTK_LINEIN_PEQ_ENABLE
    FUNC_PEQ,
    FUNC_DRC,
#endif
    FUNC_END,
};

#ifdef MTK_LEAKAGE_DETECTION_ENABLE
stream_feature_list_t stream_feature_list_leakage_compensation[] =
{
    CODEC_PCM_COPY,
    FUNC_LEAKAGE_COMPENSATION,
    FUNC_END,
};
#endif

#ifdef MTK_USER_TRIGGER_FF_ENABLE
stream_feature_list_t stream_feature_list_user_trigger_ff[4] =
{
    CODEC_PCM_COPY,
    FUNC_USER_TRIGGER_FF,
    FUNC_END,
    #if MTK_HWSRC_IN_STREAM
    0,
    #endif
};

#endif

static const uint16_t coefficient_table_16k[13] = { //13-tap
    0x0127, 0x027A, 0x0278, 0x0227,
    0xFFD5, 0xFD22, 0xFABF, 0xFAEB,
    0xFE90, 0x05EB, 0x0F47, 0x180A,
    0x1D4E
};

////////////////////////////////////////////////////////////////////////////////
// DSPMEM FUNCTION DECLARATIONS /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/**
 * dsp_sdk_add_feature
 *
 * Add customer's feature to feature table
 *
 *
 *
 */
void dsp_sdk_initialize(void)
{
    dsp_sdk_add_feature_table(&stream_feature_sample_codec);
    dsp_sdk_add_feature_table(&stream_feature_sample_function);
}

/**
 * stream_codec_decoder_sample_process
 *
 *
 */
bool stream_codec_decoder_sample_initialize(void *para)
{
    stream_codec_sample_instance_ptr_t codec_sample_pointer;

    //Get working buffer pointer
    codec_sample_pointer = (stream_codec_sample_instance_ptr_t)stream_codec_get_workingbuffer(para);

    //Do initialize
    codec_sample_pointer->memory_check = true;
    codec_sample_pointer->reset_check = false;
    memset(codec_sample_pointer->scratch_memory, 0xFF, 128);

    //return 0 when successfully initialize
    return 0;
}


/**
 * stream_codec_decoder_sample_process
 *
 *
 */
bool stream_codec_decoder_sample_process(void *para)
{
    stream_codec_sample_instance_ptr_t codec_sample_pointer;
    uint8_t *pattern_input_pointer;
    int16_t *output_l_pointer, *output_r_pointer;
    uint32_t input_length, output_length;

    //Get working buffer pointer, stream buffer pointer, and length
    codec_sample_pointer = (stream_codec_sample_instance_ptr_t)stream_codec_get_workingbuffer(para);
    pattern_input_pointer = stream_codec_get_input_buffer(para, 1);
    output_l_pointer = stream_codec_get_output_buffer(para, 1);
    output_r_pointer = stream_codec_get_output_buffer(para, 2);
    input_length = stream_codec_get_input_size(para);
    output_length = stream_codec_get_output_size(para);


    //Call decoder
    //output sample rate : 16kHz
    //output resolution  : 16-bit
    output_length = codec_decoder_sample_api(pattern_input_pointer, output_l_pointer, output_r_pointer, input_length);

    //Check decoder output
    if (output_length == 0) {
        //Do reinitialize when an error occurs.
        stream_feature_reinitialize(para);
    }

    //Check expected resolution
    if (stream_codec_get_output_resolution(para) == RESOLUTION_32BIT) {
        dsp_converter_16bit_to_32bit((int32_t*)output_l_pointer, (int16_t*)output_l_pointer, output_length/sizeof(int16_t));
        dsp_converter_16bit_to_32bit((int32_t*)output_r_pointer, (int16_t*)output_r_pointer, output_length/sizeof(int16_t));
        output_length = output_length*2;
    }

    //Modify stream buffering format
    stream_codec_modify_output_samplingrate(para, FS_RATE_16K);
    stream_codec_modify_output_size(para, output_length);
    stream_codec_modify_resolution(para, stream_codec_get_output_resolution(para));

    //return 0 when successfully process
    return 0;
}

/**
 * stream_function_sample_initialize
 *
 *
 */
bool stream_function_sample_initialize(void *para)
{
    stream_function_sample_instance_ptr_t function_sample_pointer;

    //Get working buffer pointer
    function_sample_pointer = (stream_function_sample_instance_ptr_t)stream_function_get_working_buffer(para);

    //Do initialize
    memcpy(function_sample_pointer->filter, coefficient_table_16k, 13*sizeof(uint16_t));
    function_sample_pointer->coefficient_size = 13;

    //return 0 when successfully initialize
    return 0;
}

/**
 * stream_function_sample_process
 *
 *
 */
bool stream_function_sample_process(void *para)
{
    int16_t *l_pointer, *r_pointer;
    uint32_t frame_length;
    stream_function_sample_instance_ptr_t function_sample_pointer;

    //Get working buffer pointer, stream buffer pointer, and length
    function_sample_pointer = (stream_function_sample_instance_ptr_t)stream_function_get_working_buffer(para);
    l_pointer = stream_function_get_inout_buffer(para, 1);
    r_pointer = stream_function_get_inout_buffer(para, 2);
    frame_length = stream_function_get_output_size(para);

    //Call function API
    function_sample_api(function_sample_pointer,
                        l_pointer,
                        r_pointer,
                        function_sample_pointer->buffer_l,
                        function_sample_pointer->buffer_r,
                        frame_length);



    //return 0 when successfully process
    return 0;
}



uint32_t codec_decoder_sample_api(uint8_t *pattern_pointer, int16_t *l_pointer, int16_t *r_pointer, uint32_t input_length)
{
    uint32_t codec_output_length = CODEC_OUTPUT_SIZE;
    UNUSED(pattern_pointer);
    UNUSED(l_pointer);
    UNUSED(r_pointer);
    UNUSED(input_length);
    return codec_output_length;
}

uint32_t function_sample_api(stream_function_sample_instance_ptr_t instance_ptr, int16_t *input_l_pointer, int16_t *input_r_pointer, int16_t *output_l_pointer, int16_t *output_r_pointer, uint32_t length)
{
    UNUSED(instance_ptr);
    UNUSED(input_l_pointer);
    UNUSED(input_r_pointer);
    UNUSED(output_l_pointer);
    UNUSED(output_r_pointer);
    UNUSED(length);
    return 0;
}

