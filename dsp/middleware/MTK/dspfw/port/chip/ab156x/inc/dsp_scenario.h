/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef _DSP_SCENARIO_H_
#define _DSP_SCENARIO_H_

#include "types.h"
#include "config.h"
#include "dsp_interface.h"
#include "dtm.h"
#include "audio_config.h"
#include "stream_audio_setting.h"
#include "stream_audio.h"
#include "stream.h"
#include "dsp_sdk.h"
#include "hal_gpt.h"
#include "dsp_audio_msg.h"
#include "hal_resource_assignment.h"

/******************************************************************************
 * External Global Variables
 ******************************************************************************/
typedef struct {
    SOURCE      source;
    SINK        sink;
    TRANSFORM   transform;
    stream_feature_list_ptr_t pfeature_table;
} CONNECTION_IF;

/******************************************************************************
 * External Function Prototypes
 ******************************************************************************/
EXTERN void CB_N9_A2DP_OPEN(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_A2DP_START(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_A2DP_STOP(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_A2DP_CLOSE(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_A2DP_SUSPEND(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_N9_A2DP_RESUME(hal_ccni_message_t msg, hal_ccni_message_t *ack);


EXTERN void CB_N9_SCO_UL_OPEN(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_SCO_UL_START(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_SCO_UL_STOP(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_SCO_UL_CLOSE(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_SCO_UL_PLAY(hal_ccni_message_t , hal_ccni_message_t *);
EXTERN void CB_N9_SCO_UL_SUSPEND(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_N9_SCO_UL_RESUME(hal_ccni_message_t msg, hal_ccni_message_t *ack);

EXTERN void CB_N9_SCO_DL_OPEN(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_SCO_DL_START(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_SCO_DL_STOP(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_SCO_DL_CLOSE(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_SCO_DL_SUSPEND(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_N9_SCO_DL_RESUME(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_SCO_DL_AVC_VOL_UPDATE(hal_ccni_message_t msg, hal_ccni_message_t *ack);


EXTERN void CB_N9_SCO_ULIRQ(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_SCO_DLIRQ(hal_ccni_message_t, hal_ccni_message_t*);
EXTERN void CB_N9_SCO_MICIRQ(hal_ccni_message_t, hal_ccni_message_t*);

EXTERN void CB_CM4_PLAYBACK_OPEN(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_PLAYBACK_START(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_PLAYBACK_STOP(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_PLAYBACK_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_PLAYBACK_DATA_REQ_ACK(void);
EXTERN void CB_CM4_PLAYBACK_SUSPEND(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_PLAYBACK_RESUME(hal_ccni_message_t msg, hal_ccni_message_t *ack);

EXTERN void CB_CM4_LINEIN_PLAYBACK_OPEN(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_LINEIN_PLAYBACK_START(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_LINEIN_PLAYBACK_STOP(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_LINEIN_PLAYBACK_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_LINEIN_PLAYBACK_SUSPEND(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_LINEIN_PLAYBACK_RESUME(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_TRULY_LINEIN_PLAYBACK_OPEN(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_TRULY_LINEIN_PLAYBACK_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack);

EXTERN void CB_CM4_RECORD_OPEN(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_RECORD_START(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_RECORD_STOP(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_RECORD_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_RECORD_SUSPEND(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_RECORD_RESUME(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_RECORD_LC_SET_PARAM_ACK(hal_ccni_message_t msg, hal_ccni_message_t *ack);

EXTERN void dsp_sidetone_start(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void dsp_sidetone_stop(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void dsp_sidetone_set_volume(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void dsp_sidetone_start_volume_set(void);

#ifdef MTK_PEQ_ENABLE
EXTERN void dsp_peq_set_param(hal_ccni_message_t msg, hal_ccni_message_t *ack);
#endif
EXTERN void Ch_Select_Set_Param (hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void AEC_NR_Enable(hal_ccni_message_t msg, hal_ccni_message_t *ack);

EXTERN void dsp_dc_compensation_start(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void dsp_dc_compensation_stop(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void dsp_alc_switch(hal_ccni_message_t msg, hal_ccni_message_t *ack);

EXTERN void dsp_streaming_deinit_all(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void dsp_get_reference_gain(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void dsp_set_sysram_addr(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_AUDIO_DUMP_INIT(hal_ccni_message_t msg, hal_ccni_message_t *ack);
#ifdef MTK_AIRDUMP_EN
EXTERN void CB_CM4_SCO_AIRDUMP_EN(hal_ccni_message_t msg, hal_ccni_message_t *ack);
#endif

#ifdef MTK_PROMPT_SOUND_ENABLE
EXTERN void CB_CM4_VP_PLAYBACK_OPEN(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_VP_PLAYBACK_START(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_VP_PLAYBACK_STOP(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_VP_PLAYBACK_CONFIG(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_VP_PLAYBACK_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_VP_PLAYBACK_TRIGGER(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_CM4_VP_PLAYBACK_DATA_REQ_ACK(void);
#endif

EXTERN void CB_N9_CLK_SKEW_LAG(hal_ccni_message_t msg, hal_ccni_message_t *ack);
EXTERN void CB_N9_CLK_SKEW_LEAD(hal_ccni_message_t msg, hal_ccni_message_t *ack);

void CB_CM4_AUDIO_AMP_FORCE_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack);

#ifdef MTK_SENSOR_SOURCE_ENABLE
void CB_GSENSOR_DETECT_START(hal_ccni_message_t msg, hal_ccni_message_t *ack);
void CB_GSENSOR_DETECT_STOP(hal_ccni_message_t msg, hal_ccni_message_t *ack);
void CB_GSENSOR_DETECT_READ_RG(hal_ccni_message_t msg, hal_ccni_message_t *ack);
void CB_GSENSOR_DETECT_WRITE_RG(hal_ccni_message_t msg, hal_ccni_message_t *ack);
#endif
#ifdef MTK_AUDIO_PLC_ENABLE
void DSP_AUDIO_PLC_CONTROL(hal_ccni_message_t msg, hal_ccni_message_t *ack);
#endif

#ifdef HAL_AUDIO_DSP_SHUTDOWN_SPECIAL_CONTROL_ENABLE
void DSP_DUMMY_SHUTDOWN(void);
#endif



void dsp_application_open(hal_ccni_message_t msg, hal_ccni_message_t *ack);
void dsp_application_start(hal_ccni_message_t msg, hal_ccni_message_t *ack);
void dsp_application_stop(hal_ccni_message_t msg, hal_ccni_message_t *ack);
void dsp_application_close(hal_ccni_message_t msg, hal_ccni_message_t *ack);
void dsp_application_play(hal_ccni_message_t msg, hal_ccni_message_t *ack);
void dsp_application_suspend(hal_ccni_message_t msg, hal_ccni_message_t *ack);
void dsp_application_resume(hal_ccni_message_t msg, hal_ccni_message_t *ack);
#ifdef MTK_SLT_AUDIO_HW
void AUDIO_SLT_START(hal_ccni_message_t msg, hal_ccni_message_t *ack);
#endif


#endif /* _DSP_SCENARIO_H_ */

