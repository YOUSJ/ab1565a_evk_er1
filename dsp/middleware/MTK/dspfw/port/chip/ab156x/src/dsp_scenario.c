/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

/******************************************************************************
* Include
******************************************************************************/
#include "string.h"
#include "dsp_scenario.h"
#include "stream.h"
#include "stream_n9sco.h"
#include "stream_n9_a2dp.h"
#include "hal_audio_afe_define.h"
#include "hal_audio_afe_control.h"
#include "dsp_audio_msg_define.h"
#include "dsp_drv_afe.h"
#include "davt.h"
#include "dsp_audio_ctrl.h"
#include "dsp_share_memory.h"
#include "dsp_temp.h"
#include "audio_nvdm_common.h"
#include "clk_skew.h"
#include "aec_nr_interface.h"
#include "long_term_clk_skew.h"
#ifdef MTK_AIRDUMP_EN
#include "aec_nr_interface.h"
#endif
#ifdef MTK_PEQ_ENABLE
#include "peq_interface.h"
#include "compander_interface.h"
#endif
#include "dsp_dump.h"
#include "sfr_bt.h"
#ifdef MTK_LEAKAGE_DETECTION_ENABLE
#include "leakage_compensation.h"
#endif
#ifdef MTK_WWE_ENABLE
#include "wwe_interface.h"
#endif
#ifdef MTK_SENSOR_SOURCE_ENABLE
#include "bsp_multi_axis_sensor.h"
#endif
#ifdef MTK_AUDIO_PLC_ENABLE
#include "audio_plc_interface.h"
#endif
#include "dsp_audio_msg_define.h"

/******************************************************************************
* Define
******************************************************************************/
#define eSCO_UT_FILL_SHAREBUF
// #define eSCO_UT_WETH_MEMORY


#define eSCO_UT_FILL_SHAREBUF
#define DSP_REMAP_SHARE_INFO(para,type)  ({  \
         para = (type)hal_memview_cm4_to_dsp0((uint32_t)para); \
         /*((SHARE_BUFFER_INFO_PTR) para)->startaddr = hal_memview_cm4_to_dsp0(((SHARE_BUFFER_INFO_PTR) para)->startaddr);*/})
#define abs32(x) ( (x >= 0) ? x : (-x) )

EXTERN audio_hardware afe_get_audio_hardware_by_au_afe_open_param (au_afe_open_param_t afe_open);
EXTERN audio_instance afe_get_audio_instance_by_au_afe_open_param (au_afe_open_param_t afe_open);
EXTERN audio_channel afe_get_audio_channel_by_au_afe_open_param (au_afe_open_param_t afe_open);
EXTERN afe_t afe;

/******************************************************************************
* Global variable
******************************************************************************/
CONNECTION_IF playback_if, n9_a2dp_if, n9_sco_ul_if, n9_sco_dl_if, record_if, linein_playback_if;
#ifdef MTK_PROMPT_SOUND_ENABLE
CONNECTION_IF playback_vp_if;
#endif
#ifdef MTK_SENSOR_SOURCE_ENABLE
CONNECTION_IF sensor_src_if;
uint32_t gsensor_timer_handler = 0;
#endif


void afe_print_reg(void){
     DSP_MW_LOG_I("AUDIO_TOP_CON0=%x\r\n", 1, AFE_READ(AUDIO_TOP_CON0));
     DSP_MW_LOG_I("AUDIO_TOP_CON1=%x\r\n", 1, AFE_READ(AUDIO_TOP_CON1));
     DSP_MW_LOG_I("AFE_DAC_CON0=%x\r\n", 1, AFE_READ(AFE_DAC_CON0));
     DSP_MW_LOG_I("AFE_DAC_CON1=%x\r\n", 1, AFE_READ(AFE_DAC_CON1));
     DSP_MW_LOG_I("AFE_DAC_CON2=%x\r\n", 1, AFE_READ(AFE_DAC_CON2));
     DSP_MW_LOG_I("AFE_ADDA_TOP_CON0=%x\r\n", 1, AFE_READ(AFE_ADDA_TOP_CON0));
     DSP_MW_LOG_I("AFE_ADDA_DL_SRC2_CON0=%x\r\n", 1, AFE_READ(AFE_ADDA_DL_SRC2_CON0));
     DSP_MW_LOG_I("AFE_ADDA_DL_SRC2_CON1=%x\r\n", 1, AFE_READ(AFE_ADDA_DL_SRC2_CON1));
     DSP_MW_LOG_I("AFE_ADDA_UL_DL_CON0=%x\r\n", 1, AFE_READ(AFE_ADDA_UL_DL_CON0));
     DSP_MW_LOG_I("AFE_ADDA_PREDIS_CON0=%x\r\n", 1, AFE_READ(AFE_ADDA_PREDIS_CON0));
     DSP_MW_LOG_I("AFE_ADDA_PREDIS_CON1=%x\r\n", 1, AFE_READ(AFE_ADDA_PREDIS_CON1));
     DSP_MW_LOG_I("AFE_CLASSG_LPSRCH_CFG0=%x\r\n", 1, AFE_READ(AFE_CLASSG_LPSRCH_CFG0));
     DSP_MW_LOG_I("ZCD_CON2=%x\r\n", 1, AFE_READ(ZCD_CON2));
     DSP_MW_LOG_I("ZCD_CON0=%x\r\n", 1, AFE_READ(ZCD_CON0));
     //DSP_MW_LOG_I("ZCD_CON1=%x\r\n", 1, AFE_READ(ZCD_CON1));
     DSP_MW_LOG_I("AUDDEC_ANA_CON0=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON0));
     DSP_MW_LOG_I("AUDDEC_ANA_CON1=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON1));
     DSP_MW_LOG_I("AUDDEC_ANA_CON2=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON2));
     DSP_MW_LOG_I("AUDDEC_ANA_CON3=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON3));
     DSP_MW_LOG_I("AUDDEC_ANA_CON4=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON4));
     DSP_MW_LOG_I("AUDDEC_ANA_CON5=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON5));
     DSP_MW_LOG_I("AUDDEC_ANA_CON6=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON6));
     DSP_MW_LOG_I("AUDDEC_ANA_CON7=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON7));
     DSP_MW_LOG_I("AUDDEC_ANA_CON8=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON8));
     DSP_MW_LOG_I("AUDDEC_ANA_CON9=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON9));
     DSP_MW_LOG_I("AUDDEC_ANA_CON10=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON10));
     DSP_MW_LOG_I("AUDDEC_ANA_CON11=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON11));
     DSP_MW_LOG_I("AUDDEC_ANA_CON12=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON12));
     DSP_MW_LOG_I("AUDDEC_ANA_CON13=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON13));
     DSP_MW_LOG_I("AUDDEC_ANA_CON14=%x\r\n", 1, ANA_READ(AUDDEC_ANA_CON14));
}
bool ScoDlStopFlag;


/******************************************************************************
* Function
******************************************************************************/
U32 dsp_calcuate_number_of_bit (U32 value)
{
    U32 number_of_bit;
    value = value - ((value >> 1) & 0x55555555);
    value = (value & 0x33333333) + ((value >> 2) & 0x33333333);
    number_of_bit = (((value + (value >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
    return number_of_bit;
}

SOURCE dsp_open_stream_in_afe(mcu2dsp_open_param_p open_param)
{
    SOURCE source;
    bool echo_path = false;
    DSP_MW_LOG_I("Stream in afe\r\n", 0);
    DSP_MW_LOG_I("afe in device:%d, channel:%d, memory:%d, interface:%d \r\n", 4, open_param->stream_in_param.afe.audio_device,
                                                                                  open_param->stream_in_param.afe.stream_channel,
                                                                                  open_param->stream_in_param.afe.memory,
                                                                                  open_param->stream_in_param.afe.audio_interface);

    DSP_MW_LOG_I("afe in interface:%d, hw_gain:%d\r\n", 2, open_param->stream_in_param.afe.audio_interface,
                                                  open_param->stream_in_param.afe.hw_gain);
#ifdef ENABLE_2A2D_TEST
	DSP_MW_LOG_I("[2A2D] afe in audio_device: %d, %d, %d, %d\r\n",4,open_param->stream_in_param.afe.audio_device, open_param->stream_in_param.afe.audio_device1, open_param->stream_in_param.afe.audio_device2, open_param->stream_in_param.afe.audio_device3);
    DSP_MW_LOG_I("[2A2D] afe in audio_interface: %d, %d, %d, %d\r\n", 4, open_param->stream_in_param.afe.audio_interface, open_param->stream_in_param.afe.audio_interface1, open_param->stream_in_param.afe.audio_interface2, open_param->stream_in_param.afe.audio_interface3);
#endif

    AudioAfeConfiguration(AUDIO_SOURCE_DATA_FORMAT, open_param->stream_in_param.afe.format);

    if (open_param->stream_in_param.afe.sampling_rate != 0) {
        AudioAfeConfiguration(AUDIO_SOURCE_IRQ_RATE, open_param->stream_in_param.afe.sampling_rate);
    }
    AudioAfeConfiguration(AUDIO_SOURCE_IRQ_PERIOD, open_param->stream_in_param.afe.irq_period);

    /*echo path*/
    echo_path = (open_param->stream_in_param.afe.memory&HAL_AUDIO_MEM3)/* && (dsp_calcuate_number_of_bit(open_param->stream_in_param.afe.audio_interface) == 1)*/;
    if (echo_path) {
        open_param->stream_in_param.afe.memory &= ~HAL_AUDIO_MEM3;//echo path
        DSP_MW_LOG_I("afe in with echo, memory:%d\r\n", 1, open_param->stream_in_param.afe.memory);
    }else{
        DSP_MW_LOG_I("afe in not with echo, memory:%d\r\n", 1, open_param->stream_in_param.afe.memory);
    }


#if 0

    /* 4 A-MIC Verification */
    open_param->stream_in_param.afe.audio_device        = HAL_AUDIO_CONTROL_DEVICE_ANALOG_MIC_L;
    open_param->stream_in_param.afe.audio_device1       = HAL_AUDIO_CONTROL_DEVICE_ANALOG_MIC_R;
    open_param->stream_in_param.afe.audio_device2       = HAL_AUDIO_CONTROL_DEVICE_ANALOG_MIC_L;
    open_param->stream_in_param.afe.audio_device3       = HAL_AUDIO_CONTROL_DEVICE_ANALOG_MIC_R;

    open_param->stream_in_param.afe.audio_interface     = HAL_AUDIO_INTERFACE_1;
    open_param->stream_in_param.afe.audio_interface1    = HAL_AUDIO_INTERFACE_1;
    open_param->stream_in_param.afe.audio_interface2    = HAL_AUDIO_INTERFACE_2;
    open_param->stream_in_param.afe.audio_interface3    = HAL_AUDIO_INTERFACE_2;

#endif

    AudioAfeConfiguration(AUDIO_SOURCE_DEVICE, open_param->stream_in_param.afe.audio_device);
#ifdef ENABLE_2A2D_TEST
    AudioAfeConfiguration(AUDIO_SOURCE_DEVICE1, open_param->stream_in_param.afe.audio_device1);
    AudioAfeConfiguration(AUDIO_SOURCE_DEVICE2, open_param->stream_in_param.afe.audio_device2);
    AudioAfeConfiguration(AUDIO_SOURCE_DEVICE3, open_param->stream_in_param.afe.audio_device3);
#endif
    AudioAfeConfiguration(AUDIO_SOURCE_CHANNEL, open_param->stream_in_param.afe.stream_channel);
    AudioAfeConfiguration(AUDIO_SOURCE_MEMORY, open_param->stream_in_param.afe.memory);
    AudioAfeConfiguration(AUDIO_SOURCE_INTERFACE, open_param->stream_in_param.afe.audio_interface);
#ifdef ENABLE_2A2D_TEST
    AudioAfeConfiguration(AUDIO_SOURCE_INTERFACE1, open_param->stream_in_param.afe.audio_interface1);
    AudioAfeConfiguration(AUDIO_SOURCE_INTERFACE2, open_param->stream_in_param.afe.audio_interface2);
    AudioAfeConfiguration(AUDIO_SOURCE_INTERFACE3, open_param->stream_in_param.afe.audio_interface3);
#endif
    AudioAfeConfiguration(AUDIO_SOURCE_HW_GAIN, open_param->stream_in_param.afe.hw_gain);
    #if AUTO_ERROR_SUPPRESSION
    AudioAfeConfiguration(AUDIO_SOURCE_MISC_PARMS_I2S_CLK, open_param->stream_in_param.afe.misc_parms.I2sClkSourceType);
    AudioAfeConfiguration(AUDIO_SOURCE_MISC_PARMS_MICBIAS, open_param->stream_in_param.afe.misc_parms.MicbiasSourceType);
    #endif
    AudioAfeConfiguration(AUDIO_SOURCE_MISC_PARMS, open_param->stream_in_param.afe.misc_parms);
    AudioAfeConfiguration(AUDIO_SOURCE_ECHO_REFERENCE, echo_path);

    DSP_MW_LOG_I("afe in format:%d, sampling rate:%d, IRQ period:%d mise:0x%x\r\n", 4, open_param->stream_in_param.afe.format,
                                                                    gAudioCtrl.Afe.AfeULSetting.rate,
                                                                    open_param->stream_in_param.afe.irq_period,
                                                                    open_param->stream_in_param.afe.misc_parms);

    //TEMP!! should remove Audio_Default_setting_init
    Audio_setting->Audio_source.Buffer_Frame_Num = open_param->stream_in_param.afe.frame_number;
    Audio_setting->Audio_source.Frame_Size       = open_param->stream_in_param.afe.frame_size;

    if (open_param->stream_in_param.afe.memory&HAL_AUDIO_MEM_SUB) {
        //Sub-Source
        source = StreamAudioAfeSubSource(afe_get_audio_hardware_by_au_afe_open_param(open_param->stream_in_param.afe),
                                         afe_get_audio_instance_by_au_afe_open_param(open_param->stream_in_param.afe),
                                         afe_get_audio_channel_by_au_afe_open_param(open_param->stream_in_param.afe));
    } else {
        source = StreamAudioAfeSource(afe_get_audio_hardware_by_au_afe_open_param(open_param->stream_in_param.afe),
                                      afe_get_audio_instance_by_au_afe_open_param(open_param->stream_in_param.afe),
                                      afe_get_audio_channel_by_au_afe_open_param(open_param->stream_in_param.afe));
    }
    Clock_Skew_UL_Para_Init();

    if (source != NULL) {

    } else {
        DSP_MW_LOG_E("DSP source create fail\r\n", 0);
    }
    return source;

}

SOURCE dsp_open_stream_in_hfp(mcu2dsp_open_param_p open_param)
{
    SOURCE source;
    DSP_MW_LOG_I("Stream in hfp\r\n", 0);
    DSP_MW_LOG_I("hfp in Codec:%d\r\n", 1, open_param->stream_in_param.hfp.codec_type);

    if (open_param->stream_in_param.hfp.codec_type == BT_HFP_CODEC_CVSD) {
        DSP_ALG_UpdateEscoRxMode(CVSD);
        stream_feature_configure_type(stream_feature_list_hfp_downlink, CODEC_DECODER_CVSD, CONFIG_DECODER);
    }
    else if (open_param->stream_in_param.hfp.codec_type  == BT_HFP_CODEC_mSBC) {
        DSP_ALG_UpdateEscoRxMode(mSBC);
        stream_feature_configure_type(stream_feature_list_hfp_downlink, CODEC_DECODER_MSBC, CONFIG_DECODER);
    }
    else {
        //not support codec type
    }
    n9_sco_dl_if.pfeature_table = (stream_feature_list_ptr_t)&stream_feature_list_hfp_downlink;

    DSP_Callback_PreloaderConfig(n9_sco_dl_if.pfeature_table);


    source = StreamN9ScoSource(open_param->stream_in_param.hfp.p_share_info);
    if (source != NULL) {
        source->streamBuffer.AVMBufferInfo.SampleRate = 16000;
        source->param.n9sco.share_info_base_addr->SampleRate = 16000;
        AudioAfeConfiguration(AUDIO_SINK_IRQ_RATE, 16000);
    } else {
        DSP_MW_LOG_E("DSP source create fail\r\n", 0);
    }

    /* rcdc clk skew */
    Clock_Skew_DL_Para_Init();
    #ifndef MTK_BT_HFP_SPE_ALG_V2
    rcdc_clk_info_ptr = (RCDC_BT_CLK_INFO_t *)open_param->stream_in_param.hfp.clk_info_address;
    rcdc_clk_offset_info_ptr = (RCDC_CLK_OFFSET_INFO_t *)open_param->stream_in_param.hfp.bt_inf_address;
    #endif

    #ifdef MTK_AIRDUMP_EN
    rAirDumpCtrl = (AIRDUMPCTRL_t *)open_param->stream_in_param.hfp.p_air_dump_buf;
    #endif

    return source;
}

SOURCE dsp_open_stream_in_a2dp(mcu2dsp_open_param_p open_param)
{
    SOURCE source;
    U32 samplerate = 0, channel = 0;

    DSP_MW_LOG_I("Stream In A2DP\r\n", 0);
    source  = StreamN9A2dpSource(&open_param->stream_in_param.a2dp);

    if (source != NULL) {
        /* parse codec info */
        samplerate  = a2dp_get_samplingrate(source);
        channel     = a2dp_get_channel(source);

        DSP_MW_LOG_I("A2DP codec type: %d, sr: %d, SL: %d\r\n", 3, source->param.n9_a2dp.codec_info.codec_cap.type, samplerate, source->param.n9_a2dp.sink_latency);
        n9_a2dp_if.pfeature_table = stream_feature_list_a2dp;
        if ( source->param.n9_a2dp.codec_info.codec_cap.type == BT_A2DP_CODEC_VENDOR ) {
            DSP_UpdateA2DPCodec(BT_A2DP_CODEC_VENDOR);
            DSP_MW_LOG_I("A2DP codec :%d", 1,source->param.n9_a2dp.codec_info.codec_cap.type);
            #ifndef MTK_BT_A2DP_VENDOR_ENABLE
            DSP_MW_LOG_E("A2DP request unsupported codec", 0);
            configASSERT(0);
            #endif
            #ifdef MTK_BT_A2DP_VENDOR_BC_ENABLE
            if (source->param.n9_a2dp.sink_latency == 0)
            {
                n9_a2dp_if.pfeature_table = stream_feature_list_vend_a2dp;
            }
            #endif
            stream_feature_configure_type(n9_a2dp_if.pfeature_table, CODEC_DECODER_VENDOR, CONFIG_DECODER);
            //not support codec type
        }
        else if ( source->param.n9_a2dp.codec_info.codec_cap.type == BT_A2DP_CODEC_SBC ) {
            DSP_UpdateA2DPCodec(BT_A2DP_CODEC_SBC);
            stream_feature_configure_type(n9_a2dp_if.pfeature_table, CODEC_DECODER_SBC, CONFIG_DECODER);
            #ifdef MTK_AUDIO_PLC_ENABLE
            Audio_PLC_ctrl(open_param->stream_in_param.a2dp.audio_plc);
            #endif
        }
        else if ( source->param.n9_a2dp.codec_info.codec_cap.type == BT_A2DP_CODEC_AAC ) {
            DSP_UpdateA2DPCodec(BT_A2DP_CODEC_AAC);
            stream_feature_configure_type(n9_a2dp_if.pfeature_table, CODEC_DECODER_AAC, CONFIG_DECODER);
            #ifdef MTK_AUDIO_PLC_ENABLE
            Audio_PLC_ctrl(open_param->stream_in_param.a2dp.audio_plc);
            #endif
        }

        source->streamBuffer.AVMBufferInfo.SampleRate = samplerate;
        memcpy(&(((SHARE_BUFFER_INFO_PTR)(source->param.n9_a2dp.share_info_base_addr))->sample_rate), &(source->streamBuffer.AVMBufferInfo.SampleRate), 4);/* update sample_rate */
        AudioAfeConfiguration(AUDIO_SINK_IRQ_RATE, samplerate);

        /* long-term clock skew */
        Clock_Skew_DL_Para_Init();
        lt_clk_skew_reset_info();
        lt_clk_skew_set_sample_rate(samplerate);
        lt_clk_skew_set_asi_buf(source->param.n9_a2dp.asi_buf);
        lt_clk_skew_set_min_gap_buf(source->param.n9_a2dp.min_gap_buf);
        lt_clk_skew_set_sink_latency(source->param.n9_a2dp.sink_latency);

        #ifndef MTK_BT_HFP_SPE_ALG_V2
        /* rcdc clock skew */
        //rcdc_clk_info_ptr = (RCDC_BT_CLK_INFO_t *)open_param->stream_in_param.a2dp.clk_info_address;
        rcdc_clk_offset_info_ptr = (RCDC_CLK_OFFSET_INFO_t *)open_param->stream_in_param.a2dp.bt_inf_address;
        #endif
        source->param.n9_a2dp.readOffset = open_param->stream_in_param.a2dp.p_afe_buf_report;// store afe buffer report instead
        #ifdef MTK_PEQ_ENABLE
        PEQ_Reset_Info();
        #endif


        DSP_Callback_PreloaderConfig((stream_feature_list_ptr_t)n9_a2dp_if.pfeature_table);
    } else {
        DSP_MW_LOG_E("DSP source create fail\r\n", 0);
    }
    return source;
}

SOURCE dsp_open_stream_in_playback(mcu2dsp_open_param_p open_param)
{
    SOURCE source;
    U32 sample_rate;
    DSP_MW_LOG_I("Stream in playback\r\n", 0);

    source = StreamCM4PlaybackSource(&open_param->stream_in_param.playback);

    if (source) {
        sample_rate = source->param.cm4_playback.info.sampling_rate;

        /* Yo: Should switch to VP afe sink later */
        source->streamBuffer.ShareBufferInfo.sample_rate = sample_rate;
        AudioAfeConfiguration(AUDIO_SINK_IRQ_RATE, sample_rate);

        stream_feature_configure_type(stream_feature_list_prompt, CODEC_PCM_COPY, CONFIG_DECODER);
    } else {
        DSP_MW_LOG_E("DSP source create fail\r\n", 0);
    }

    return source;
}

SOURCE dsp_open_stream_in_vp(mcu2dsp_open_param_p open_param)
{
    SOURCE source;
    U32 sample_rate;
    DSP_MW_LOG_I("Stream in vp\r\n", 0);
#ifdef MTK_PROMPT_SOUND_ENABLE
    source = StreamCM4VPPlaybackSource(&open_param->stream_in_param.playback);
#endif
    if (source) {
        sample_rate = source->param.cm4_playback.info.sampling_rate;

        /* Yo: Should switch to VP afe sink later */
        source->streamBuffer.ShareBufferInfo.sample_rate = sample_rate;
        AudioAfeConfiguration(AUDIO_SINK_IRQ_RATE, sample_rate);

        stream_feature_configure_type(stream_feature_list_prompt, CODEC_PCM_COPY, CONFIG_DECODER);
    } else {
        DSP_MW_LOG_E("DSP source create fail\r\n", 0);
    }

    return source;
}

#ifdef MTK_SENSOR_SOURCE_ENABLE
SOURCE dsp_open_stream_in_gsensor(mcu2dsp_open_param_p open_param)
{
    SOURCE source;
    source = StreamGsensorSource();
    return source;
}

SOURCE dsp_open_stream_out_gsensor(mcu2dsp_open_param_p open_param)
{
    SINK sink;
    sink = StreamGsensorSink();
    return sink;
}
#endif

SOURCE dsp_open_stream_in(mcu2dsp_open_param_p open_param)
{
    SOURCE source = NULL;
    if (open_param != NULL) {
        switch (open_param->param.stream_in) {
            case STREAM_IN_AFE:
                source = dsp_open_stream_in_afe(open_param);
                break;
            case STREAM_IN_HFP:
                source = dsp_open_stream_in_hfp(open_param);
                break;
            case STREAM_IN_A2DP:
                source = dsp_open_stream_in_a2dp(open_param);
                break;
            case STREAM_IN_PLAYBACK:
                source = dsp_open_stream_in_playback(open_param);
                break;
            case STREAM_IN_VP:
                source = dsp_open_stream_in_vp(open_param);
                break;
            #ifdef MTK_SENSOR_SOURCE_ENABLE
            case STREAM_IN_GSENSOR:
                source = dsp_open_stream_in_gsensor(open_param);
                break;
            #endif
            default:
                break;
        }
    }
    return source;
}

SINK dsp_open_stream_out_hfp(mcu2dsp_open_param_p open_param)
{
    SINK sink;
    DSP_MW_LOG_I("Stream out hfp\r\n", 0);
    DSP_MW_LOG_I("hfp out Codec:%d\r\n", 1, open_param->stream_out_param.hfp.codec_type);

    if (open_param->stream_out_param.hfp.codec_type == BT_HFP_CODEC_CVSD ) {
        DSP_ALG_UpdateEscoTxMode(CVSD);
        stream_feature_configure_type(stream_feature_list_hfp_uplink, CODEC_ENCODER_CVSD, CONFIG_ENCODER);
    }
    else if (open_param->stream_out_param.hfp.codec_type  == BT_HFP_CODEC_mSBC ) {
        DSP_ALG_UpdateEscoTxMode(mSBC);
        stream_feature_configure_type(stream_feature_list_hfp_uplink, CODEC_ENCODER_MSBC, CONFIG_ENCODER);
    }
    else {
        //not support codec type
    }
    n9_sco_ul_if.pfeature_table = (stream_feature_list_ptr_t)&stream_feature_list_hfp_uplink;

    DSP_Callback_PreloaderConfig(n9_sco_ul_if.pfeature_table);

    sink = StreamN9ScoSink(open_param->stream_out_param.hfp.p_share_info);
    if (sink != NULL) {

    } else {
        DSP_MW_LOG_E("DSP sink create fail\r\n", 0);
    }

    return sink;
}

SINK dsp_open_stream_out_afe(mcu2dsp_open_param_p open_param)
{
    SINK sink = NULL;

    //[TEMP]: Add AT Cmd to switch I2S mode
    if(((*((volatile uint32_t*)(0xA2120B04)) >> 2)&0x01) == 1) {
        open_param->stream_out_param.afe.audio_device = HAL_AUDIO_CONTROL_DEVICE_SPDIF;//HAL_AUDIO_DEVICE_I2S_MASTER;
        open_param->stream_out_param.afe.audio_interface = HAL_AUDIO_INTERFACE_1;
    }

    DSP_MW_LOG_I("Stream out afe\r\n", 1);
    #ifdef ENABLE_HWSRC_CLKSKEW
    DSP_MW_LOG_I("afe out device:%d, channel:%d, memory:%d, interface:%d, hw_gain:%d, adc_mode:%d, performance:%d,clkskew_mode:%d \r\n", 8, open_param->stream_out_param.afe.audio_device,
                                                                         open_param->stream_out_param.afe.stream_channel,
                                                                         open_param->stream_out_param.afe.memory,
                                                                         open_param->stream_out_param.afe.audio_interface,
                                                                         open_param->stream_out_param.afe.hw_gain,
                                                                         open_param->stream_out_param.afe.adc_mode,
                                                                         open_param->stream_out_param.afe.performance,
                                                                         open_param->stream_out_param.afe.clkskew_mode);
    #else
    DSP_MW_LOG_I("afe out device:%d, channel:%d, memory:%d, interface:%d, hw_gain:%d, adc_mode:%d, performance:%d \r\n", 8, open_param->stream_out_param.afe.audio_device,
                                                                         open_param->stream_out_param.afe.stream_channel,
                                                                         open_param->stream_out_param.afe.memory,
                                                                         open_param->stream_out_param.afe.audio_interface,
                                                                         open_param->stream_out_param.afe.hw_gain,
                                                                         open_param->stream_out_param.afe.adc_mode,
                                                                         open_param->stream_out_param.afe.performance);
    #endif

    AudioAfeConfiguration(AUDIO_SINK_DATA_FORMAT, open_param->stream_out_param.afe.format);
    if (open_param->stream_out_param.afe.sampling_rate != 0) {
        AudioAfeConfiguration(AUDIO_SINK_IRQ_RATE, open_param->stream_out_param.afe.sampling_rate);
    }

#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
    AudioAfeConfiguration(AUDIO_SRC_RATE,open_param->stream_out_param.afe.stream_out_sampling_rate);
#endif
    AudioAfeConfiguration(AUDIO_SINK_IRQ_PERIOD, open_param->stream_out_param.afe.irq_period);

    AudioAfeConfiguration(AUDIO_SINK_DEVICE, open_param->stream_out_param.afe.audio_device);
    AudioAfeConfiguration(AUDIO_SINK_CHANNEL, open_param->stream_out_param.afe.stream_channel);
    AudioAfeConfiguration(AUDIO_SINK_MEMORY, open_param->stream_out_param.afe.memory);
    AudioAfeConfiguration(AUDIO_SINK_INTERFACE, open_param->stream_out_param.afe.audio_interface);
    AudioAfeConfiguration(AUDIO_SINK_HW_GAIN, open_param->stream_out_param.afe.hw_gain);
#ifdef ENABLE_HWSRC_CLKSKEW
    if (open_param->stream_out_param.afe.memory == HAL_AUDIO_MEM1) {
        AudioAfeConfiguration(AUDIO_SINK_CLKSKEW_MODE, open_param->stream_out_param.afe.clkskew_mode);
        ClkSkewMode_g = open_param->stream_out_param.afe.clkskew_mode;
        DSP_MW_LOG_I("afe clkskew_mode:%d %d", 2, open_param->stream_out_param.afe.clkskew_mode, ClkSkewMode_g);
    }
#endif
    #ifdef HAL_AUDIO_ENABLE_PATH_MEM_DEVICE
    AudioAfeConfiguration(AUDIO_SINK_ADC_MODE, open_param->stream_out_param.afe.adc_mode);
    #endif
    #if AUTO_ERROR_SUPPRESSION
    AudioAfeConfiguration(AUDIO_SINK_MISC_PARMS_I2S_CLK, open_param->stream_out_param.afe.misc_parms.I2sClkSourceType);
    AudioAfeConfiguration(AUDIO_SINK_MISC_PARMS_MICBIAS, open_param->stream_out_param.afe.misc_parms.MicbiasSourceType);
    #endif
    AudioAfeConfiguration(AUDIO_SINK_ECHO_REFERENCE, false);

    DSP_MW_LOG_I("afe out format:%d, sampling rate:%d, IRQ period:%d\r\n", 3, open_param->stream_out_param.afe.format,
                                                                     gAudioCtrl.Afe.AfeDLSetting.rate,
                                                                     open_param->stream_out_param.afe.irq_period);


    if (open_param->stream_out_param.afe.memory == HAL_AUDIO_MEM1) {
        AudioAfeConfiguration(AUDIO_SINK_SW_CHANNELS, open_param->stream_out_param.afe.sw_channels);
        Audio_setting->Audio_sink.Buffer_Frame_Num = open_param->stream_out_param.afe.frame_number;
        Audio_setting->Audio_sink.Frame_Size       = open_param->stream_out_param.afe.frame_size;
        sink = StreamAudioAfeSink(afe_get_audio_hardware_by_au_afe_open_param(open_param->stream_out_param.afe),
                                  afe_get_audio_instance_by_au_afe_open_param(open_param->stream_out_param.afe),
                                  afe_get_audio_channel_by_au_afe_open_param(open_param->stream_out_param.afe));

    } else if (open_param->stream_out_param.afe.memory == HAL_AUDIO_MEM2) {
        Audio_setting->Audio_VP.Buffer_Frame_Num = open_param->stream_out_param.afe.frame_number;
        Audio_setting->Audio_VP.Frame_Size = open_param->stream_out_param.afe.frame_size;
        // VP/RT memory path DL2_data
        #ifdef MTK_PROMPT_SOUND_ENABLE
        sink = StreamAudioAfe2Sink(afe_get_audio_hardware_by_au_afe_open_param(open_param->stream_out_param.afe),
                                   afe_get_audio_instance_by_au_afe_open_param(open_param->stream_out_param.afe),
                                   afe_get_audio_channel_by_au_afe_open_param(open_param->stream_out_param.afe));
        #endif
    }

    if (sink != NULL) {

    } else {
        DSP_MW_LOG_E("DSP sink create fail\r\n", 0);
    }
    return sink;

}

SINK dsp_open_stream_out_record(mcu2dsp_open_param_p open_param)
{
    SINK sink;
    DSP_MW_LOG_I("Stream out record\r\n", 0);

    sink = StreamCm4RecordSink(&(open_param->stream_out_param.record));
    if (sink != NULL) {

    } else {
        DSP_MW_LOG_E("DSP sink create fail\r\n", 0);
    }
    return sink;
}

SINK dsp_open_stream_out_virtual(mcu2dsp_open_param_p open_param)
{
    SINK sink;
    DSP_MW_LOG_I("Stream out virtual\r\n", 0);
    UNUSED(open_param);
    sink = StreamVirtualSink(NULL, NULL);
    if (sink != NULL) {

    } else {
        DSP_MW_LOG_E("DSP sink create fail\r\n", 0);
    }
    return sink;
}


SINK dsp_open_stream_out(mcu2dsp_open_param_p open_param)
{
    SINK sink = NULL;
    if (open_param != NULL) {
        switch (open_param->param.stream_out) {
            case STREAM_OUT_AFE:
                sink = dsp_open_stream_out_afe(open_param);
                break;
            case STREAM_OUT_HFP:
                sink = dsp_open_stream_out_hfp(open_param);
                break;
            case STREAM_OUT_RECORD:
                sink = dsp_open_stream_out_record(open_param);
                break;
            #ifdef MTK_SENSOR_SOURCE_ENABLE
            case STREAM_OUT_GSENSOR:
                sink = dsp_open_stream_out_gsensor(open_param);
                break;
            #endif
            case STREAM_OUT_VIRTUAL:
                sink = dsp_open_stream_out_virtual(open_param);
                break;
            default:
                break;
        }
    }
    return sink;
}

void dsp_start_stream_in_afe(mcu2dsp_start_param_p start_param, SOURCE source)
{
    source->param.audio.AfeBlkControl.u4awsflag = start_param->stream_in_param.afe.mce_flag;
    DSP_MW_LOG_I("Stream in afe start MCE:%d\r\n", 1, source->param.audio.AfeBlkControl.u4awsflag);
}

void dsp_start_stream_in_a2dp(mcu2dsp_start_param_p start_param, SOURCE source)
{
    source->param.n9_a2dp.cp_exist = FALSE;
    source->param.n9_a2dp.predict_asi = start_param->stream_in_param.a2dp.start_asi;
    source->param.n9_a2dp.latency_monitor = start_param->stream_in_param.a2dp.latency_monitor_enable;
    source->param.n9_a2dp.DspReportStartId = 0xFFFF;
    DSP_MW_LOG_I("DSP Msg CP:%d, ASI:%d, Lm : %d\r\n", 3, source->param.n9_a2dp.cp_exist,
                                                    source->param.n9_a2dp.predict_asi,
                                                    source->param.n9_a2dp.latency_monitor);
    source->param.n9_a2dp.latency_monitor = TRUE;
    U32 bt_clk;
    U16 intra_clk;
    MCE_GetBtClk((BTCLK *)&bt_clk,(BTPHASE *)&intra_clk);
    printf("Get host set asi: 0x%x b:0x%x i:0x%x",start_param->stream_in_param.a2dp.start_asi,start_param->stream_in_param.a2dp.start_bt_clk,start_param->stream_in_param.a2dp.start_bt_intra_clk);
    printf("Get play en  b: 0x%x i:0x%x n:0x%x",bt_clk,intra_clk,rBb->rClkCtl.rNativeClock);
    DSP_MW_LOG_I("DSP Msg CP:%d, ASI:%d, Lm : %d\r\n", 3, source->param.n9_a2dp.cp_exist,
                                                    source->param.n9_a2dp.predict_asi,
                                                    source->param.n9_a2dp.latency_monitor);
    start_param->stream_in_param.a2dp.start_bt_intra_clk = (start_param->stream_in_param.a2dp.start_bt_intra_clk&0xFFFF)<<1;

    if (abs32((S32)(start_param->stream_in_param.a2dp.start_bt_clk - bt_clk)) >  0x2000) // Maximum tolerence time difference = 0x2000*312.5(us) = 2.56(s)
    {
        DSP_MW_LOG_I("Host notify play time abnormal, bt_clk: 0x%x play_clk: 0x%x",bt_clk, start_param->stream_in_param.a2dp.start_bt_clk);
        start_param->stream_in_param.a2dp.start_bt_clk = bt_clk + 0x100;
        //configASSERT(0);
    }
    if ((S32)(start_param->stream_in_param.a2dp.start_bt_clk - bt_clk) > 0x10)  // check play time in time
    {
        printf("Set legal play en");
    }
    else
    {
        DSP_MW_LOG_I("Host notify play time too early, modify play asi : %d", source->param.n9_a2dp.predict_asi);
        U32 temp_start_bt_clk = start_param->stream_in_param.a2dp.start_bt_clk;
        while ((S32)(temp_start_bt_clk - bt_clk) < 0x80)
        {
            source->param.n9_a2dp.predict_asi += 1024;
            temp_start_bt_clk +=  3276800 / (source->streamBuffer.AVMBufferInfo.SampleRate); // roughly calc
        }
        start_param->stream_in_param.a2dp.start_bt_clk +=((((source->param.n9_a2dp.predict_asi - start_param->stream_in_param.a2dp.start_asi)<<5)*100 / (source->streamBuffer.AVMBufferInfo.SampleRate))& 0xFFFFFFC);
        start_param->stream_in_param.a2dp.start_bt_intra_clk += ((((source->param.n9_a2dp.predict_asi - start_param->stream_in_param.a2dp.start_asi))*20000)/(source->streamBuffer.AVMBufferInfo.SampleRate/100))%2500;
    }
    MCE_TransBT2NativeClk(start_param->stream_in_param.a2dp.start_bt_clk,start_param->stream_in_param.a2dp.start_bt_intra_clk,&bt_clk,&intra_clk);
    DSP_MW_LOG_I("Play en  b: 0x%x i:0x%x n:0x%x asi: 0x%x",4,bt_clk,intra_clk,rBb->rClkCtl.rNativeClock,source->param.n9_a2dp.predict_asi);
    hal_audio_afe_set_play_en(bt_clk,intra_clk);
}


void dsp_start_stream_in(mcu2dsp_start_param_p start_param, SOURCE source)
{
    if (start_param != NULL) {
        switch (start_param->param.stream_in) {
            case STREAM_IN_AFE:
                dsp_start_stream_in_afe(start_param, source);
                break;
            case STREAM_IN_HFP:
                break;
            case STREAM_IN_A2DP:
                dsp_start_stream_in_a2dp(start_param, source);
                break;
            case STREAM_IN_PLAYBACK:
                break;
            case STREAM_IN_VP:
                break;
            default:
                break;
        }
    }
}

void dsp_start_stream_out_afe(mcu2dsp_start_param_p start_param, SINK sink)
{
    sink->param.audio.AfeBlkControl.u4awsflag = start_param->stream_out_param.afe.mce_flag;
    //sink->param.audio.AfeBlkControl.u4awsflag = TRUE;// synchornize headset project & MCE
    sink->param.audio.aws_sync_request        = start_param->stream_out_param.afe.aws_sync_request;
    sink->param.audio.aws_sync_time           = start_param->stream_out_param.afe.aws_sync_time;
    DSP_MW_LOG_I("Stream out afe start MCE:%d\r\n", 1, sink->param.audio.AfeBlkControl.u4awsflag);
}



void dsp_start_stream_out(mcu2dsp_start_param_p start_param, SINK sink)
{
    if (start_param != NULL) {
        switch (start_param->param.stream_out) {
            case STREAM_OUT_AFE:
                dsp_start_stream_out_afe(start_param, sink);
                break;
            case STREAM_OUT_HFP:
                break;
            case STREAM_OUT_RECORD:
                break;
            default:
                break;
        }
    }
}


void dsp_trigger_suspend (SOURCE source, SINK sink)
{
    afe_amp_keep_enable_state(TRUE);
    if(source != NULL) {
        audio_ops_trigger(source, AFE_PCM_TRIGGER_SUSPEND);
    }

    if (sink != NULL) {
        audio_ops_trigger(sink, AFE_PCM_TRIGGER_SUSPEND);
    }
}

void dsp_trigger_resume (SOURCE source, SINK sink)
{
    if (source != NULL) {
        if (!audio_ops_trigger(source, AFE_PCM_TRIGGER_RESUME)) {
            source->param.audio.mute_flag = TRUE;
            source->param.audio.pop_noise_pkt_num = 0;
        }
    }

    if (sink != NULL) {
        audio_ops_trigger(sink, AFE_PCM_TRIGGER_RESUME);
    }
    afe_amp_keep_enable_state(FALSE);
}
/* A2DP CCNI callback function */
void CB_N9_A2DP_OPEN(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    printf("A2DP UT start22");
#if 0
    //return;
#else
    UNUSED(ack);
    DSP_MW_LOG_I("A2DP OPEN\r\n", 0);

    /* remap to non-cacheable address */

    mcu2dsp_open_param_p open_param;
    open_param = (mcu2dsp_open_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    DSP_REMAP_SHARE_INFO(open_param->stream_in_param.a2dp.p_share_info, AVM_SHARE_BUF_INFO_PTR);
    DSP_MW_LOG_I("A2DP share info %X\r\n", 1, open_param->stream_in_param.a2dp.p_share_info->StartAddr);
    DSP_REMAP_SHARE_INFO(open_param->stream_in_param.a2dp.p_asi_buf, uint32_t *);
    DSP_REMAP_SHARE_INFO(open_param->stream_in_param.a2dp.p_min_gap_buf, uint32_t *);
    DSP_REMAP_SHARE_INFO(open_param->stream_in_param.a2dp.p_pcdc_anchor_info_buf, uint32_t *);
    DSP_REMAP_SHARE_INFO(open_param->stream_in_param.a2dp.p_current_bit_rate, uint32_t *);
    DSP_REMAP_SHARE_INFO(open_param->stream_in_param.a2dp.bt_inf_address, uint32_t);
    DSP_REMAP_SHARE_INFO(open_param->stream_in_param.a2dp.p_afe_buf_report, uint32_t);

    n9_a2dp_if.source   = dsp_open_stream_in(open_param);
    n9_a2dp_if.sink     = dsp_open_stream_out(open_param);
    n9_a2dp_if.transform = NULL;

    DSP_MW_LOG_I("A2DP OPEN Finish\r\n", 0);
    //PIC
    printf("A2DP UT OPEN done");
#endif
}

void CB_N9_A2DP_START(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
#if 0
    //aud_msg_ack(MSG_MCU2DSP_BT_AUDIO_DL_START | 0x8000, FALSE);
    //return;
#else
    U32 gpt_timer;
    UNUSED(ack);
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &gpt_timer);
    DSP_MW_LOG_I("A2DP START Time:%d\r\n", 1, gpt_timer);


    /* remap to non-cacheable address */
    mcu2dsp_start_param_p start_param;
    start_param = (mcu2dsp_start_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);


    dsp_start_stream_in (start_param, n9_a2dp_if.source);
    dsp_start_stream_out(start_param, n9_a2dp_if.sink);
    n9_a2dp_if.sink->param.audio.AfeBlkControl.u4awsflag = 1;
    DSP_MW_LOG_I("A2DP notify cnt address 0x%x Start Ro :%d", 2, &(n9_a2dp_if.source->streamBuffer.AVMBufferInfo.NotifyCount),n9_a2dp_if.source->streamBuffer.AVMBufferInfo.ReadIndex);
    #ifdef MTK_BT_A2DP_VENDOR_BC_ENABLE
    if((n9_a2dp_if.source->param.n9_a2dp.codec_info.codec_cap.type == BT_A2DP_CODEC_VENDOR) && (n9_a2dp_if.source->param.n9_a2dp.sink_latency == 0))
    {
        n9_a2dp_if.sink->param.audio.AfeBlkControl.u4awsflag = FALSE;
        DSP_MW_LOG_I("vendor BC force aws flag to be false", 0);
    }
    #endif
    //n9_a2dp_if.sink->param.audio.AfeBlkControl.u4awsflag = FALSE;

    n9_a2dp_if.source->param.n9_a2dp.mce_flag = n9_a2dp_if.sink->param.audio.AfeBlkControl.u4awsflag;

    #if ADATIVE_LATENCY_CTRL
    //n9_a2dp_if.source->param.n9_a2dp.alc_monitor = ((n9_a2dp_if.source->param.n9_a2dp.sink_latency < ADATIVE_LATENCY)&&(Audio_setting->Audio_sink.alc_enable == TRUE)&&( n9_a2dp_if.source->param.n9_a2dp.codec_info.codec_cap.type != BT_A2DP_CODEC_VENDOR )) ? TRUE : FALSE;
    n9_a2dp_if.source->param.n9_a2dp.alc_monitor = FALSE;
    #else
    n9_a2dp_if.source->param.n9_a2dp.alc_monitor = FALSE;
    #endif
    DSP_MW_LOG_I("A2DP latency :%d", 1,n9_a2dp_if.source->param.n9_a2dp.sink_latency);
    stream_feature_configure_resolution((stream_feature_list_ptr_t)n9_a2dp_if.pfeature_table, RESOLUTION_32BIT, CONFIG_DECODER);
    n9_a2dp_if.transform = TrasformAudio2Audio(n9_a2dp_if.source, n9_a2dp_if.sink, (stream_feature_list_ptr_t)n9_a2dp_if.pfeature_table);
    n9_a2dp_if.source->param.n9_a2dp.DspReportStartId = msg.ccni_message[0]>>16|0x8000;
    n9_a2dp_if.sink->param.audio.afe_wait_play_en_cnt = 0;
    afe_enable_audio_irq(afe_irq_request_number(AUDIO_DIGITAL_BLOCK_MEM_DL1), n9_a2dp_if.sink->param.audio.rate, n9_a2dp_if.sink->param.audio.count);
    if (n9_a2dp_if.transform == NULL)
    {
        DSP_MW_LOG_E("A2DP START transform failed", 0);
    }
    // *(volatile uint32_t *)0x70000F58 = 0x00000208; //ZCD_CON2
#endif
}

void CB_N9_A2DP_STOP(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("A2DP STOP\r\n", 0);
    if (n9_a2dp_if.transform != NULL)
    {
        StreamDSPClose(n9_a2dp_if.transform->source,n9_a2dp_if.transform->sink,msg.ccni_message[0]>>16|0x8000);
    }
    n9_a2dp_if.transform = NULL;
    DSP_MW_LOG_I("A2DP STOP Finish\r\n", 0);
}

void CB_N9_A2DP_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{

    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("A2DP CLOSE\r\n", 0);
    SourceClose(n9_a2dp_if.source);
    SinkClose(n9_a2dp_if.sink);
    DSP_PIC_FeatureDeinit(n9_a2dp_if.pfeature_table);
    DSP_MW_LOG_I("aac feature PIC unload : %d %d", 2, *(n9_a2dp_if.pfeature_table),*(n9_a2dp_if.pfeature_table));
    memset(&n9_a2dp_if,0,sizeof(CONNECTION_IF));

    DSP_MW_LOG_I("A2DP CLOSE Finish\r\n", 0);
}

void CB_N9_A2DP_CLOCKSKEW(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    //clock skew to adjust hw src
}

void CB_N9_A2DP_SUSPEND(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("A2DP SUSPEND\r\n", 0);

    dsp_trigger_suspend(n9_a2dp_if.source, n9_a2dp_if.sink);


}

void CB_N9_A2DP_RESUME(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("A2DP RESUME\r\n", 0);


    dsp_trigger_resume(n9_a2dp_if.source, n9_a2dp_if.sink);

    afe_amp_keep_enable_state(FALSE);
}

/* End A2DP CCNI callback function */


/* eSCO CCNI callback function */
void CB_N9_SCO_UL_OPEN(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("eSCO UL OPEN\r\n", 0);

    mcu2dsp_open_param_p open_param;
    open_param = (mcu2dsp_open_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    DSP_REMAP_SHARE_INFO(open_param->stream_out_param.hfp.p_share_info, AVM_SHARE_BUF_INFO_PTR);
    DSP_MW_LOG_I("eSCO UL share info->StartAddr: 0x%x, forwarder: 0x%x\r\n", 2, open_param->stream_out_param.hfp.p_share_info->StartAddr, open_param->stream_out_param.hfp.p_share_info->ForwarderAddr);
    n9_sco_ul_if.source   = dsp_open_stream_in(open_param);
    n9_sco_ul_if.sink     = dsp_open_stream_out(open_param);
    n9_sco_ul_if.transform = NULL;

    DSP_MW_LOG_I("eSCO UL OPEN Finish\r\n", 0);
}

void CB_N9_SCO_UL_START(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    U32 gpt_timer;
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &gpt_timer);
    DSP_MW_LOG_I("eSCO UL START gpt %d\r\n", 1,gpt_timer);

    mcu2dsp_start_param_p start_param;
    start_param = (mcu2dsp_start_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);

    dsp_start_stream_in (start_param, n9_sco_ul_if.source);
    dsp_start_stream_out(start_param, n9_sco_ul_if.sink);

#if 1
    n9_sco_ul_if.source->param.audio.AfeBlkControl.u4awsflag = 1;
#else
    n9_sco_ul_if.source->param.audio.AfeBlkControl.u4awsflag = FALSE; // open UL directly
#endif
    n9_sco_ul_if.transform = TrasformAudio2Audio(n9_sco_ul_if.source, n9_sco_ul_if.sink, n9_sco_ul_if.pfeature_table);
    if (n9_sco_ul_if.transform == NULL)
    {
        DSP_MW_LOG_E("SCO UL transform failed", 0);
    }
    DSP_MW_LOG_I("eSCO UL START finish",0);
}

void CB_N9_SCO_UL_STOP(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("eSCO UL STOP\r\n", 0);
    if (n9_sco_ul_if.transform != NULL)
    {
        StreamDSPClose(n9_sco_ul_if.transform->source,n9_sco_ul_if.transform->sink,msg.ccni_message[0]>>16|0x8000);
    }
    n9_sco_ul_if.transform = NULL;
    DSP_MW_LOG_I("eSCO UL STOP Finish\r\n", 0);
}

void CB_N9_SCO_UL_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    SourceClose(n9_sco_ul_if.source);
    SinkClose(n9_sco_ul_if.sink);
    DSP_PIC_FeatureDeinit(n9_sco_ul_if.pfeature_table);
    memset(&n9_sco_ul_if,0,sizeof(CONNECTION_IF));
    DSP_MW_LOG_I("eSCO UL CLOSE Finish\r\n", 0);
}

void CB_N9_SCO_UL_PLAY(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);

    if (n9_sco_ul_if.transform != NULL)
    {
        DSP_MW_LOG_I("eSCO UL PLAY\r\n", 0);
        #if 0//moidfy for ab1568
        afe_set_memory_path_enable(AUDIO_DIGITAL_BLOCK_MEM_VUL1, true, true);
        afe_enable_audio_irq(afe_irq_request_number(AUDIO_DIGITAL_BLOCK_MEM_VUL1), n9_sco_ul_if.source->param.audio.rate, n9_sco_ul_if.source->param.audio.count);

        if (n9_sco_ul_if.source->param.audio.echo_reference) {
            afe_set_memory_path_enable(AUDIO_DIGITAL_BLOCK_MEM_AWB, true, true);
        }
        #else
        hal_audio_trigger_start_parameter_t start_parameter;
        start_parameter.memory_select = n9_sco_ul_if.source->param.audio.mem_handle.memory_select;
        start_parameter.enable = true;
        hal_audio_set_value((hal_audio_set_value_parameter_t *)&start_parameter, HAL_AUDIO_SET_TRIGGER_MEMORY_START);
        #endif
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &(n9_sco_ul_if.sink->param.n9sco.ul_play_gpt));
    }
    else
    {
        DSP_MW_LOG_I("eSCO UL PLAY when tansform not exist\r\n", 0);
    }
}

void CB_N9_SCO_UL_SUSPEND(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("eSCO UL SUSPEND\r\n", 0);
    dsp_trigger_suspend(n9_sco_ul_if.source, n9_sco_ul_if.sink);

}


void CB_N9_SCO_UL_RESUME(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("eSCO UL RESUME\r\n", 0);
    dsp_trigger_resume(n9_sco_ul_if.source, n9_sco_ul_if.sink);
}


void CB_N9_SCO_DL_OPEN(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("eSCO DL OPEN\r\n", 0);

    mcu2dsp_open_param_p open_param;
    open_param = (mcu2dsp_open_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    DSP_REMAP_SHARE_INFO(open_param->stream_in_param.hfp.p_share_info, AVM_SHARE_BUF_INFO_PTR);
    DSP_MW_LOG_I("eSCO DL share info->StartAddr: 0x%x, forwarder: 0x%x\r\n", 2, open_param->stream_in_param.hfp.p_share_info->StartAddr, open_param->stream_in_param.hfp.p_share_info->ForwarderAddr);
    DSP_REMAP_SHARE_INFO(open_param->stream_in_param.hfp.bt_inf_address, uint32_t);
    DSP_REMAP_SHARE_INFO(open_param->stream_in_param.hfp.clk_info_address, uint32_t);
#ifdef MTK_AIRDUMP_EN
    DSP_REMAP_SHARE_INFO(open_param->stream_in_param.hfp.p_air_dump_buf, uint32_t);
#endif

    n9_sco_dl_if.source   = dsp_open_stream_in(open_param);
    n9_sco_dl_if.sink     = dsp_open_stream_out(open_param);
    n9_sco_dl_if.transform = NULL;
    DSP_MW_LOG_I("eSCO DL OPEN Finish\r\n", 0);
}

void CB_N9_SCO_DL_START(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("eSCO DL START\r\n", 0);

    mcu2dsp_start_param_p start_param;
    start_param = (mcu2dsp_start_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);


    dsp_start_stream_in (start_param, n9_sco_dl_if.source);
    dsp_start_stream_out(start_param, n9_sco_dl_if.sink);


    SCO_Rx_Buf_Ctrl(TRUE);
#if 1
    n9_sco_dl_if.sink->param.audio.AfeBlkControl.u4awsflag = TRUE;
#else
    n9_sco_dl_if.sink->param.audio.AfeBlkControl.u4awsflag = FALSE; // Open DL AFE directly
#endif
    n9_sco_dl_if.transform = TrasformAudio2Audio(n9_sco_dl_if.source, n9_sco_dl_if.sink, n9_sco_dl_if.pfeature_table);
    ScoDlStopFlag = FALSE;

    if (n9_sco_dl_if.transform == NULL)
    {
        DSP_MW_LOG_E("SCO DL transform failed", 0);
        aud_msg_ack(MSG_MCU2DSP_BT_VOICE_DL_START|0x8000, false);
    }else{
        aud_msg_ack(MSG_MCU2DSP_BT_VOICE_DL_START|0x8000, false);
        n9_sco_dl_if.source->param.n9sco.rx_forwarder_en = TRUE;
        DSP_MW_LOG_E("SCO DL n9_sco_dl_if.sink->param.audio.rx_forwarder_en: %d ", 1, n9_sco_dl_if.source->param.n9sco.rx_forwarder_en);
        SCO_Rx_Intr_HW_Handler();
        SCO_Rx_Intr_Ctrl(TRUE);
    }
}

void CB_N9_SCO_DL_STOP(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("eSCO DL STOP\r\n", 0);
    ScoDlStopFlag = TRUE;
    if (n9_sco_dl_if.transform != NULL) {
        StreamDSPClose(n9_sco_dl_if.transform->source,n9_sco_dl_if.transform->sink,msg.ccni_message[0]>>16|0x8000);
    }
    n9_sco_dl_if.transform = NULL;
    DSP_MW_LOG_I("eSCO DL STOP Finish\r\n", 0);
}

void CB_N9_SCO_DL_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("eSCO DL CLOSE\r\n", 0);
    SourceClose(n9_sco_dl_if.source);
    SinkClose(n9_sco_dl_if.sink);
    DSP_PIC_FeatureDeinit(n9_sco_dl_if.pfeature_table);
    memset(&n9_sco_dl_if,0,sizeof(CONNECTION_IF));
    DSP_MW_LOG_I("eSCO DL CLOSE Finish\r\n", 0);
}

void CB_N9_SCO_CLOCKSKEW(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    //TBD
}

void CB_N9_SCO_DL_SUSPEND(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("eSCO DL SUSPEND\r\n", 0);

    dsp_trigger_suspend(n9_sco_dl_if.source, n9_sco_dl_if.sink);

}


void CB_N9_SCO_DL_RESUME(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("eSCO DL RESUME\r\n", 0);

    dsp_trigger_resume(n9_sco_dl_if.source, n9_sco_dl_if.sink);
}

void CB_CM4_SCO_DL_AVC_VOL_UPDATE(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("[NDVC][DSP receive] avc_vol: %d, gfgAvcUpdate: %d",2, msg.ccni_message[1], gfgAvcUpdate);
    if(gfgAvcUpdate == false){
       gfgAvcUpdate = true;
       gi2AvcVol = msg.ccni_message[1];
    }
}


void CB_AUDIO_DUMP_INIT(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    uint16_t u2SendID;

    u2SendID = (msg.ccni_message[0] & 0x00ff);
    DSP_MW_LOG_I("[CB_AUDIO_DUMP_INIT] CCNI ID: %x, Dump Mask: %x, SendID: %x", 3, msg.ccni_message[0], msg.ccni_message[1],u2SendID);
    if(u2SendID == 0){
        AudioDumpMask[0] = msg.ccni_message[1];
    }
    else if(u2SendID == 1){
        AudioDumpMask[1] = msg.ccni_message[1];
    }
}

#ifdef MTK_AIRDUMP_EN
void CB_CM4_SCO_AIRDUMP_EN(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    U32 mask;
    hal_nvic_save_and_set_interrupt_mask(&mask);
    if(msg.ccni_message[1] == 1)
    {
        DSP_MW_LOG_I("[AirDump][DSP] AIRDUMP Start\r\n", 0);
        AEC_NR_AirDumpEnable(TRUE);
    }
    else if(msg.ccni_message[1] == 0)
    {
        DSP_MW_LOG_I("[AirDump][DSP] AIRDUMP Stop\r\n", 0);
        AEC_NR_AirDumpEnable(FALSE);
    }
    hal_nvic_restore_interrupt_mask(mask);
}
#endif


void CB_N9_SCO_ULIRQ(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    //printf("CB_N9_SCO_ULIRQ\r\n");
    //n9_sco_ul_if.source->transform->Handler(n9_sco_ul_if.source,n9_sco_ul_if.sink);
    //vTaskResume(DAV_TASK_ID);
}

void CB_N9_SCO_DLIRQ(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    if ((Source_blks[SOURCE_TYPE_N9SCO] == NULL)||(n9_sco_dl_if.transform == NULL))
    {
        DSP_MW_LOG_I("Unexpected N9 DL IRQ", 0);
        return;
    }
    if (Source_blks[SOURCE_TYPE_N9SCO]->param.n9sco.share_info_base_addr->ReadIndex == Source_blks[SOURCE_TYPE_N9SCO]->param.n9sco.share_info_base_addr->WriteIndex)
    {
#ifdef PT_bufferfull
        Source_blks[SOURCE_TYPE_N9SCO]->param.n9sco.share_info_base_addr->bBufferIsFull = TRUE;
#endif
        DSP_MW_LOG_I("SCO DL bufferfull, ro:%d, wo:%d", 2, Source_blks[SOURCE_TYPE_N9SCO]->param.n9sco.share_info_base_addr->ReadIndex, Source_blks[SOURCE_TYPE_N9SCO]->param.n9sco.share_info_base_addr->WriteIndex);
    }
    if (n9_sco_dl_if.source->param.n9sco.IsFirstIRQ)
    {
        n9_sco_dl_if.source->param.n9sco.IsFirstIRQ = FALSE;
        U32 gpt_timer;
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &gpt_timer);
        DSP_MW_LOG_I("CB_N9_SCO_DLIRQ, First Wo:%d, Ro:%d,GPT : %d\r\n", 3, Source_blks[SOURCE_TYPE_N9SCO]->param.n9sco.share_info_base_addr->WriteIndex, Source_blks[SOURCE_TYPE_N9SCO]->param.n9sco.share_info_base_addr->ReadIndex, gpt_timer);
        if ((n9_sco_dl_if.source != NULL)&&(n9_sco_dl_if.source->transform->Handler != NULL))
        {
            n9_sco_dl_if.source->transform->Handler(n9_sco_dl_if.source,n9_sco_dl_if.sink);
            vTaskResume((TaskHandle_t)DAV_TASK_ID);
        }
    }
    if (ScoDlStopFlag == TRUE)
    {
        vTaskResume((TaskHandle_t)DAV_TASK_ID);
    }
}

void CB_N9_SCO_MICIRQ(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    //printf("CB_N9_SCO_MICIRQ\r\n");
    //n9_sco_ul_if.source->transform->Handler(n9_sco_ul_if.source,n9_sco_ul_if.sink);
    //vTaskResume(DAV_TASK_ID);
}
/* End eSCO CCNI callback function */


/* CM4 Playback CCNI callback function */
void CB_CM4_PLAYBACK_OPEN(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("[CM4_PB] Open", 0);


    /* remap to non-cacheable address */
    mcu2dsp_open_param_p open_param;
    open_param = (mcu2dsp_open_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    DSP_REMAP_SHARE_INFO(open_param->stream_in_param.playback.share_info_base_addr, uint32_t);
    memset(&playback_if, 0 ,sizeof(CONNECTION_IF) );
    playback_if.source   = dsp_open_stream_in(open_param);
    playback_if.sink     = dsp_open_stream_out(open_param);
}


void CB_CM4_PLAYBACK_START(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("[CM4_PB] Start", 0);
    mcu2dsp_start_param_p start_param;
    start_param = (mcu2dsp_start_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);

    dsp_start_stream_in (start_param, playback_if.source);
    dsp_start_stream_out(start_param, playback_if.sink);

    playback_if.transform = TrasformAudio2Audio(playback_if.source, playback_if.sink, stream_feature_list_prompt);
    if (playback_if.transform == NULL)
    {
        DSP_MW_LOG_E("CM4 Playback transform failed", 0);
    }
}


void CB_CM4_PLAYBACK_STOP(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("[CM4_PB] Stop", 0);
    if (playback_if.transform != NULL)
    {
        StreamDSPClose(playback_if.transform->source,playback_if.transform->sink,msg.ccni_message[0]>>16|0x8000);
    }
    playback_if.transform = NULL;
}

void CB_CM4_PLAYBACK_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("[CM4_PB] Close", 0);
    SourceClose(playback_if.source);
    SinkClose(playback_if.sink);
    DSP_PIC_FeatureDeinit(playback_if.pfeature_table);
    memset(&playback_if,0,sizeof(CONNECTION_IF));
}

void CB_CM4_PLAYBACK_SUSPEND(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("[CM4_PB]  SUSPEND\r\n", 0);

    dsp_trigger_suspend(playback_if.source, playback_if.sink);
}


void CB_CM4_PLAYBACK_RESUME(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("[CM4_PB]  RESUME\r\n", 0);

    dsp_trigger_resume(playback_if.source, playback_if.sink);

}

/* CM4 Line-in Playback CCNI callback function */
void CB_CM4_LINEIN_PLAYBACK_OPEN(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("LINEIN PLAYBACK OPEN\r\n", 0);


    /* remap to non-cacheable address */
    mcu2dsp_open_param_p open_param;
    open_param = (mcu2dsp_open_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    //DSP_REMAP_SHARE_INFO(open_param->stream_in_param.playback.share_info_base_addr, uint32_t);
    memset(&linein_playback_if, 0 ,sizeof(CONNECTION_IF) );
    linein_playback_if.source   = dsp_open_stream_in(open_param);
    linein_playback_if.sink     = dsp_open_stream_out(open_param);

    linein_playback_if.source->param.audio.linein_scenario_flag = 1;
    linein_playback_if.sink->param.audio.linein_scenario_flag = 1;

    Source_Audio_BufferInfo_Rst(linein_playback_if.source, 0);
    Sink_Audio_BufferInfo_Rst(linein_playback_if.sink, 0);

    DSP_MW_LOG_I("LINEIN PLAYBACK OPEN Finish\r\n", 0);
}

void CB_CM4_LINEIN_PLAYBACK_START(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("LINEIN PLAYBACK START\r\n", 0);
    mcu2dsp_start_param_p start_param;
    start_param = (mcu2dsp_start_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);

    dsp_start_stream_in (start_param, linein_playback_if.source);
    hal_gpt_delay_us(2000);
    dsp_start_stream_out(start_param, linein_playback_if.sink);

    stream_feature_configure_resolution((stream_feature_list_ptr_t)linein_playback_if.pfeature_table, RESOLUTION_32BIT, CONFIG_DECODER);
    linein_playback_if.transform = TrasformAudio2Audio(linein_playback_if.source, linein_playback_if.sink, stream_feature_list_linein);
    if (linein_playback_if.transform == NULL)
    {
        DSP_MW_LOG_E("CM4 Line-in Playback transform failed", 0);
    }
}

void CB_CM4_LINEIN_PLAYBACK_STOP(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    #ifdef ENABLE_AUDIO_WITH_JOINT_MIC
    hal_ccni_message_t joint_msg;
    joint_msg.ccni_message[0]= (MSG_DSP_NULL_REPORT<<16);
    CB_CM4_STREAM_JOINT_STOP(joint_msg, ack);
    #endif

    DSP_MW_LOG_I("LINEIN PLAYBACK STOP\r\n", 0);
    if (linein_playback_if.transform != NULL)
    {
        StreamDSPClose(linein_playback_if.transform->source, linein_playback_if.transform->sink, msg.ccni_message[0]>>16|0x8000);
    }
    linein_playback_if.transform = NULL;
    DSP_MW_LOG_I("LINEIN PLAYBACK STOP Finish\r\n", 0);
}

void CB_CM4_LINEIN_PLAYBACK_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("LINEIN PLAYBACK CLOSE\r\n", 0);
#ifdef ENABLE_AUDIO_WITH_JOINT_MIC
    CB_CM4_STREAM_JOINT_CLOSE(msg, ack);
#endif
    SourceClose(linein_playback_if.source);
    SinkClose(linein_playback_if.sink);
    DSP_PIC_FeatureDeinit(linein_playback_if.pfeature_table);
    //DSP_MW_LOG_I("feature PIC unload : %d",1,*(linein_playback_if.pfeature_table));
    memset(&linein_playback_if,0,sizeof(CONNECTION_IF));

    DSP_MW_LOG_I("LINEIN PLAYBACK CLOSE Finish\r\n", 0);
}

void CB_CM4_LINEIN_PLAYBACK_SUSPEND(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("LINEIN PLAYBACK SUSPEND\r\n", 0);

    dsp_trigger_suspend(linein_playback_if.source, linein_playback_if.sink);
}

void CB_CM4_LINEIN_PLAYBACK_RESUME(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("LINEIN PLAYBACK RESUME\r\n", 0);

    dsp_trigger_resume(linein_playback_if.source, linein_playback_if.sink);
}

afe_loopback_param_t dsp_afe_loopack;
//extern hal_audio_device_parameter_vow_t vow_control;

void CB_CM4_TRULY_LINEIN_PLAYBACK_OPEN(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    DSP_MW_LOG_I("Cm4 Truly Line-in Playback Open", 0);
    mcu2dsp_open_param_p open_param;
    UNUSED(ack);

#ifdef AB1568_BRING_UP_DSP_DEFAULT_HW_LOOPBACK
    dsp_afe_loopack.in_device = HAL_AUDIO_CONTROL_DEVICE_ANALOG_MIC_DUAL;//HAL_AUDIO_CONTROL_DEVICE_LINE_IN_DUAL;
    dsp_afe_loopack.in_interface = HAL_AUDIO_INTERFACE_1;
    //dsp_afe_loopack.in_misc_parms = 259;
    dsp_afe_loopack.out_device = HAL_AUDIO_CONTROL_DEVICE_INTERNAL_DAC_DUAL;//HAL_AUDIO_CONTROL_DEVICE_I2S_MASTER;
    dsp_afe_loopack.out_interface = HAL_AUDIO_INTERFACE_1;
    //dsp_afe_loopack.out_misc_parms = 1;
    dsp_afe_loopack.sample_rate = 48000;
    dsp_afe_loopack.with_hw_gain = true;
    dsp_afe_loopack.stream_channel = HAL_AUDIO_DIRECT;
    dsp_afe_loopack.format = AFE_PCM_FORMAT_S32_LE;
#else
    open_param = (mcu2dsp_open_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);

    dsp_afe_loopack.in_device = open_param->stream_in_param.afe.audio_device;
    dsp_afe_loopack.in_interface = open_param->stream_in_param.afe.audio_interface;
    #if AUTO_ERROR_SUPPRESSION
    dsp_afe_loopack.in_misc_parms = open_param->stream_in_param.afe.misc_parms;
    #endif
    dsp_afe_loopack.out_device = open_param->stream_out_param.afe.audio_device;
    dsp_afe_loopack.out_interface = open_param->stream_out_param.afe.audio_interface;
    #if AUTO_ERROR_SUPPRESSION
    dsp_afe_loopack.out_misc_parms = open_param->stream_out_param.afe.misc_parms;
    #endif
    dsp_afe_loopack.sample_rate = open_param->stream_out_param.afe.sampling_rate;
    dsp_afe_loopack.with_hw_gain = open_param->stream_out_param.afe.hw_gain;
    dsp_afe_loopack.stream_channel = open_param->stream_out_param.afe.stream_channel;
    dsp_afe_loopack.format = open_param->stream_out_param.afe.format;

#endif
#ifdef LINE_IN_PURE_FOR_AMIC_CLASS_G_HQA
        DSP_MW_LOG_I("dsp_afe_loopack.in_device %d, adc_mode %d\r\n",2,dsp_afe_loopack.in_device,open_param->stream_in_param.afe.adc_mode);
        if(dsp_afe_loopack.in_device & HAL_AUDIO_CONTROL_DEVICE_ANALOG_MIC_DUAL){
            dsp_afe_loopack.device_handle_in.analog_mic.adc_parameter.adc_mode = open_param->stream_in_param.afe.adc_mode;
            dsp_afe_loopack.device_handle_in.analog_mic.adc_parameter.performance = open_param->stream_in_param.afe.performance;
        }else if(dsp_afe_loopack.in_device & HAL_AUDIO_CONTROL_DEVICE_DIGITAL_MIC_DUAL){
            dsp_afe_loopack.device_handle_in.digital_mic.dmic_selection = open_param->stream_in_param.afe.adc_mode;
            DSP_MW_LOG_I("dsp_afe_loopack dmic_selection %d adc_mode %d",2,dsp_afe_loopack.device_handle_in.digital_mic.dmic_selection,
                open_param->stream_in_param.afe.adc_mode);
            //printf("dsp_afe_loopack dmic_selection %d adc_mode %d",dsp_afe_loopack.device_handle_in.digital_mic.dmic_selection,
            //    open_param->stream_in_param.afe.adc_mode);
        }

        dsp_afe_loopack.device_handle_out.dac.with_high_performance = open_param->stream_out_param.afe.performance;
        DSP_MW_LOG_I("dsp_afe_loopack in_device %d in_interface %d in_misc_parms %d out_device %d out_interface %d out_misc_parms %d sample_rate %d with_hw_gain %d stream_channel %d format %d adc_mode %d",11,
        dsp_afe_loopack.in_device,dsp_afe_loopack.in_interface,dsp_afe_loopack.in_misc_parms,dsp_afe_loopack.out_device,dsp_afe_loopack.out_interface,
        dsp_afe_loopack.out_misc_parms,dsp_afe_loopack.sample_rate,dsp_afe_loopack.with_hw_gain,dsp_afe_loopack.stream_channel,dsp_afe_loopack.format,
        dsp_afe_loopack.device_handle_in.analog_mic.adc_parameter.adc_mode);
#endif
    afe_set_loopback_enable(true, &dsp_afe_loopack);
#if 0

    memset(&dsp_vow_control,0,sizeof(hal_audio_device_parameter_vow_t));
    dsp_vow_control.audio_device = HAL_AUDIO_CONTROL_DEVICE_VOW;
    dsp_vow_control.dma_irq_threshold = VOW_SRAM_COPY_SIZE;
    dsp_vow_control.snr_threshold = 0x7373;
    dsp_vow_control.alpha_rise = 0x7;
    dsp_vow_control.dmic_selection = HAL_AUDIO_DMIC_GPIO_DMIC0;
    //dsp_vow_control.suspend_mic = true;
    dsp_vow_control.suspend_mic = false;
    dsp_vow_control.input_device = HAL_AUDIO_CONTROL_DEVICE_VOW;
    //dsp_vow_control.mic_selection = HAL_AUDIO_CONTROL_DEVICE_DIGITAL_MIC_DUAL;
    dsp_vow_control.mic_selection = HAL_AUDIO_CONTROL_DEVICE_ANALOG_MIC_DUAL;
    dsp_vow_control.bias_select = HAL_AUDIO_BIAS_SELECT_ALL;
    dsp_vow_control.bias_voltage = HAL_AUDIO_BIAS_VOLTAGE_1_85V;
    dsp_vow_control.mic_interface = HAL_AUDIO_CONTROL_DEVICE_INTERFACE_1;
    dsp_vow_control.memory_select = HAL_AUDIO_MEMORY_UL_VUL1;
    dsp_vow_control.adc_parameter.performance = AFE_PEROFRMANCE_ULTRA_LOW_POWER_MODE;
    dsp_vow_control.rate = 16000;
    dsp_vow_control.vow_detection_done_entry = dsp_vow_isr_handler;
    //dsp_vow_control.vow_mode = AFE_VOW_PHASE0;
    dsp_vow_control.vow_mode = AFE_VOW_PHASE1;
    dsp_vow_control.vow_with_hpf = true;

    vow_sink(NULL,NULL);
    vow_enable(&dsp_vow_control,NULL);


#endif


}

void CB_CM4_TRULY_LINEIN_PLAYBACK_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    DSP_MW_LOG_I("Cm4 Truly Line-in Playback Close", 0);
    UNUSED(msg);
    UNUSED(ack);
    afe_set_loopback_enable(false, &dsp_afe_loopack);
}
/* End CM4 Line-in Playback CCNI callback function */

/* CM4 Record CCNI callback function */
extern bool CM4_Record_air_dump;
extern U8   CM4_Record_air_dump_scenario;
#ifdef MTK_LEAKAGE_DETECTION_ENABLE
extern bool CM4_Record_leakage_enable;
extern hal_audio_device_t leakage_detection_FB_mic;
#endif
#ifdef MTK_WWE_ENABLE
hal_audio_device_t  wwe_audio_device;
#endif
void CB_CM4_RECORD_OPEN(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("Cm4 record Open", 0);

    /* remap to non-cacheable address */
    mcu2dsp_open_param_p open_param;
    open_param = (mcu2dsp_open_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    memset(&record_if, 0 ,sizeof(CONNECTION_IF) );
    audio_dsp_codec_type_t audio_dsp_codec_type;
    audio_dsp_codec_type = msg.ccni_message[0]&0x0fff;
#ifdef MTK_WWE_ENABLE
    g_wwe_mode = ((msg.ccni_message[0]&0xf000) >> 12);
    DSP_MW_LOG_I("Cm4 record Open wwe_mode = %d", 1, g_wwe_mode);
    wwe_audio_device = open_param->stream_in_param.afe.audio_device;
#endif
    DSP_MW_LOG_I("Cm4 record Open msg.ccni_message[0]:0x%x", 1,msg.ccni_message[0]);
    DSP_MW_LOG_I("Cm4 record Open codec type:0x%3x", 1,audio_dsp_codec_type);
    switch(audio_dsp_codec_type){
#ifdef MTK_RECORD_OPUS_ENABLE
        case AUDIO_DSP_CODEC_TYPE_OPUS:{
            record_if.pfeature_table = (stream_feature_list_ptr_t)&stream_feature_list_opus_mic_record;
            break;
        }
#endif
#ifdef MTK_LEAKAGE_DETECTION_ENABLE
        case AUDIO_DSP_CODEC_TYPE_ANC_LC:{
            CM4_Record_leakage_enable = true;
            record_if.pfeature_table = (stream_feature_list_ptr_t)&stream_feature_list_leakage_compensation;
            leakage_detection_FB_mic = open_param->stream_in_param.afe.audio_device1;
            DSP_MW_LOG_I("[Record_LC]leakage_detection_FB_mic:0x%x", 1,leakage_detection_FB_mic);
            break;
        }
#endif
        case AUDIO_DSP_CODEC_TYPE_PCM:{
            record_if.pfeature_table = (stream_feature_list_ptr_t)&stream_feature_list_mic_record;
            break;
        }
        default:
            if((audio_dsp_codec_type & 0xff00) == 0xff00){ /*Record air dump.*/
                CM4_Record_air_dump = true;
                CM4_Record_air_dump_scenario = audio_dsp_codec_type & 0x000f;
                record_if.pfeature_table = (stream_feature_list_ptr_t)&stream_feature_list_mic_record_airdump;
            } else {
                record_if.pfeature_table = (stream_feature_list_ptr_t)&stream_feature_list_mic_record;
            }
        break;
    }
#ifdef MTK_WWE_ENABLE
    if(g_wwe_mode != WWE_MODE_NONE)
    {
        record_if.pfeature_table = (stream_feature_list_ptr_t)&stream_feature_list_wwe_mic_record;
        g_mcu2dsp_vad_param = (mcu2dsp_vad_param_p)open_param->stream_out_param.record.p_share_info->startaddr;
    }
#endif
    record_if.source   = dsp_open_stream_in(open_param);
    record_if.sink     = dsp_open_stream_out(open_param);
#ifdef MTK_WWE_ENABLE
    wwe_processing_init();
#endif
}


void CB_CM4_RECORD_START(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("Cm4 record Start", 0);
    mcu2dsp_start_param_p start_param;
    start_param = (mcu2dsp_start_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);

    dsp_start_stream_in (start_param, record_if.source);
    dsp_start_stream_out(start_param, record_if.sink);

    record_if.transform = TrasformAudio2Audio(record_if.source, record_if.sink, record_if.pfeature_table);
}


void CB_CM4_RECORD_STOP(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("Cm4 record Stop", 0);
#ifdef MTK_WWE_ENABLE
    wwe_processing_deinit();
#endif
    if (record_if.transform != NULL)
    {
        StreamDSPClose(record_if.transform->source,record_if.transform->sink,msg.ccni_message[0]>>16|0x8000);
    }else{
        DSP_MW_LOG_E("Cm4 record not exit, just ack.", 0);
        aud_msg_ack(msg.ccni_message[0]>>16|0x8000, FALSE);
    }
    record_if.transform = NULL;
}

void CB_CM4_RECORD_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("Cm4 record Close", 0);
    SourceClose(record_if.source);
    SinkClose(record_if.sink);
    DSP_PIC_FeatureDeinit(record_if.pfeature_table);
    memset(&record_if,0,sizeof(CONNECTION_IF));
    CM4_Record_air_dump = false;
#ifdef MTK_LEAKAGE_DETECTION_ENABLE
    CM4_Record_leakage_enable = false;
#endif
}

void CB_CM4_RECORD_SUSPEND(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("Cm4 recordSUSPEND\r\n", 0);
    dsp_trigger_suspend(record_if.source, record_if.sink);

}

void CB_CM4_RECORD_RESUME(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("Cm4 record RESUME\r\n", 0);
    dsp_trigger_resume(record_if.source, record_if.sink);
}
/* End Playback CCNI callback function */

#ifdef MTK_LEAKAGE_DETECTION_ENABLE
void CB_CM4_RECORD_LC_SET_PARAM_ACK(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    anc_leakage_compensation_parameters_nvdm_p leakage_compensation_param;
    leakage_compensation_param = (anc_leakage_compensation_parameters_nvdm_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    extern anc_leakage_compensation_parameters_nvdm_t leakage_compensation_thd;
    leakage_compensation_thd.Sz_thd2 = leakage_compensation_param->Sz_thd2;
    leakage_compensation_thd.Sz_thd3 = leakage_compensation_param->Sz_thd3;
    leakage_compensation_thd.Delay_sample = leakage_compensation_param->Delay_sample;
    DSP_MW_LOG_I("[RECORD_LC]CB_CM4_RECORD_LC_SET_PARAM_ACK, thd2:0x%x, thd3:0x%x, Delay_sample:%d\r\n", 3, leakage_compensation_param->Sz_thd2, leakage_compensation_param->Sz_thd3, leakage_compensation_param->Delay_sample);
}
#endif

afe_sidetone_param_t dsp_afe_sidetone;
void dsp_sidetone_start(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);

    U32 gpt_timer;
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &gpt_timer);
    DSP_MW_LOG_I("SideTone Start gpt : %d\r\n", 1,gpt_timer);

    mcu2dsp_sidetone_param_p start_param;
    //mcu2dsp_sidetone_param_t sidetone;


    #if 0
    dsp_afe_sidetone.in_device      = HAL_AUDIO_DEVICE_MAIN_MIC_DUAL;
    dsp_afe_sidetone.in_interface   = HAL_AUDIO_INTERFACE_1;
    dsp_afe_sidetone.out_device     = HAL_AUDIO_DEVICE_DAC_DUAL;
    dsp_afe_sidetone.out_interface  = HAL_AUDIO_INTERFACE_1;
    dsp_afe_sidetone.channel        = HAL_AUDIO_DIRECT;
    dsp_afe_sidetone.gain           = 600;
    #else
    start_param = (mcu2dsp_sidetone_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    memcpy(&dsp_afe_sidetone, start_param, sizeof(mcu2dsp_sidetone_param_t));
    #endif
    //afe_set_sidetone_enable(true, dsp_afe_sidetone);

    afe_set_sidetone_enable_flag(true, dsp_afe_sidetone.gain);

    DSP_MW_LOG_I("SideTone Start gain %d\r\n", 1,dsp_afe_sidetone.gain);
    DTM_enqueue(DTM_EVENT_ID_SIDETONE_START, 0, false);
}

void dsp_sidetone_stop(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("SideTone Stop\r\n", 0);
    #if 0
    mcu2dsp_sidetone_param_p start_param;
    mcu2dsp_sidetone_param_t sidetone;
    start_param = (mcu2dsp_sidetone_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    memcpy(&sidetone, start_param, sizeof(mcu2dsp_sidetone_param_t));
    #endif
    //afe_set_sidetone_enable(false, dsp_afe_sidetone);
    afe_set_sidetone_enable_flag(false, dsp_afe_sidetone.gain);
    #if 0//modify for ab1568
#ifdef ENABLE_SIDETONE_RAMP_TIMER
    fw_sidetone_set_ramp_timer(FW_SIDETONE_MUTE_GAIN);
#else
    DTM_enqueue(DTM_EVENT_ID_SIDETONE_STOP, 0, false);
#endif
    #else
    DTM_enqueue(DTM_EVENT_ID_SIDETONE_STOP_RAMP, 0, false);
    //hal_audio_set_device(&(dsp_afe_sidetone.device_handle_in_side_tone), dsp_afe_sidetone.device_handle_in_side_tone.sidetone.audio_device, HAL_AUDIO_CONTROL_OFF);
    DSP_MW_LOG_I("sidetone device 0x%x off\r\n",1,dsp_afe_sidetone.device_handle_in_side_tone.sidetone.audio_device);
    #endif
}

void dsp_sidetone_set_volume(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("SideTone Set_volume\r\n", 0);
    int32_t sidetone_gain = (int32_t)msg.ccni_message[1];
    afe_set_sidetone_volume(sidetone_gain);
}

void dsp_sidetone_start_volume_set(void)
{
    if (afe_get_sidetone_enable_flag() == true)
    {
        //hal_gpt_delay_ms(200);
        vTaskDelay(pdMS_TO_TICKS(200));
        afe_set_sidetone_volume(afe_get_sidetone_gain());
    }
}


void dsp_dc_compensation_start(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{

    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("DC COMPENSATION Start\r\n", 0);


    #if 0 //modify for ab1568
    afe_amp_enable(true, HAL_AUDIO_DEVICE_DAC_DUAL);
    #endif
    hal_audio_device_parameter_t device_handle;
#if 0
#ifndef CONFIG_TOOL_TEST
#ifdef ANALOG_OUTPUT_CLASSD_ENABLE
    device_handle.analog_mic.audio_device = HAL_AUDIO_CONTROL_DEVICE_ANALOG_MIC_DUAL;
    device_handle.analog_mic.rate = 48000;
    device_handle.analog_mic.mic_interface = HAL_AUDIO_CONTROL_DEVICE_INTERFACE_1;
    device_handle.analog_mic.bias_select = HAL_AUDIO_BIAS_SELECT_ALL;
    device_handle.analog_mic.bias_voltage = HAL_AUDIO_BIAS_VOLTAGE_1_85V;
    device_handle.analog_mic.iir_filter = false;
    device_handle.analog_mic.with_external_bias = false;
    device_handle.analog_mic.with_bias_lowpower = false;
    device_handle.analog_mic.with_phase_inverse = false;
    device_handle.analog_mic.adc_parameter.adc_mode = HAL_AUDIO_ANALOG_INPUT_ACC10K;              //AU_AFE_IN_MODE_t
    device_handle.analog_mic.adc_parameter.performance = AFE_PEROFRMANCE_NORMAL_MODE;  //AU_AFE_POWER_SETTING_t
    device_handle.analog_mic.adc_parameter.is_fifo_sync_clock_ch1 = false;
    device_handle.analog_mic.adc_parameter.with_au_in_swap = false;
    hal_audio_set_device(&device_handle, device_handle.analog_mic.audio_device, HAL_AUDIO_CONTROL_ON);
#else
    device_handle.dac.rate = 48000;
    device_handle.dac.dac_mode= HAL_AUDIO_ANALOG_OUTPUT_CLASSAB;

    device_handle.dac.dc_compensation_value = afe.stream_out.dc_compensation_value;
    device_handle.dac.with_high_performance = false;
    device_handle.dac.with_force_change_rate = false;

    hal_audio_set_device(&device_handle, HAL_AUDIO_CONTROL_DEVICE_INTERNAL_DAC_DUAL, HAL_AUDIO_CONTROL_ON);

    AFE_WRITE(0xC0000EDC, 0x11004089);              // Setting voltage high to 0.9v
    #if 1//modfiy for ab1568
    ANA_SET_REG (0xA207012C, 0x1<<2, 0x1<<2); // Enable trim buffer's reference voltage
    DSP_MW_LOG_I("AUDDEC_ANA_CON11 addr:0x%x val:0x%x\r\n", 2,AUDDEC_ANA_CON11,AFE_READ(AUDDEC_ANA_CON11));
    #else
    ANA_SET_REG (AUDDEC_ANA_CON10, 0x1<<2, 0x1<<2); // Enable trim buffer's reference voltage
    #endif
#endif

#endif
#endif
//#ifdef CONFIG_TOOL_TEST
    hal_audio_set_value_parameter_t set_val_handle;
    set_val_handle.analog_output_mode.dac_mode = msg.ccni_message[0]&0xffff;
    DSP_MW_LOG_I("msg.ccni_message[0] 0x%x\r\n",1,msg.ccni_message[0]);
    set_val_handle.analog_output_mode.gain_select = AFE_HW_ANALOG_GAIN_OUTPUT;
    hal_audio_set_value(&set_val_handle,HAL_AUDIO_SET_VOLUME_OUTPUT_ANALOG_SETTING);
    if(set_val_handle.analog_output_mode.dac_mode == HAL_AUDIO_ANALOG_OUTPUT_CLASSAB){
        device_handle.dac.rate = 48000;
        device_handle.dac.dac_mode= HAL_AUDIO_ANALOG_OUTPUT_CLASSAB;

        device_handle.dac.dc_compensation_value = afe.stream_out.dc_compensation_value;
        device_handle.dac.with_high_performance = false;
        device_handle.dac.with_force_change_rate = false;

        hal_audio_set_device(&device_handle, HAL_AUDIO_CONTROL_DEVICE_INTERNAL_DAC_DUAL, HAL_AUDIO_CONTROL_ON);

        AFE_WRITE(0xC0000EDC, 0x11004089);              // Setting voltage high to 0.9v
        #if 1//modfiy for ab1568
        ANA_SET_REG (0xA207012C, 0x1<<2, 0x1<<2); // Enable trim buffer's reference voltage
        DSP_MW_LOG_I("AUDDEC_ANA_CON11 addr:0x%x val:0x%x\r\n", 2,AUDDEC_ANA_CON11,AFE_READ(AUDDEC_ANA_CON11));
        #endif
    }else{
        device_handle.analog_mic.audio_device = HAL_AUDIO_CONTROL_DEVICE_ANALOG_MIC_DUAL;
        device_handle.analog_mic.rate = 48000;
        device_handle.analog_mic.mic_interface = HAL_AUDIO_CONTROL_DEVICE_INTERFACE_1;
        device_handle.analog_mic.bias_select = HAL_AUDIO_BIAS_SELECT_ALL;
        device_handle.analog_mic.bias_voltage = HAL_AUDIO_BIAS_VOLTAGE_1_85V;
        device_handle.analog_mic.iir_filter = false;
        device_handle.analog_mic.with_external_bias = false;
        device_handle.analog_mic.with_bias_lowpower = false;
        device_handle.analog_mic.with_phase_inverse = false;
        device_handle.analog_mic.adc_parameter.adc_mode = HAL_AUDIO_ANALOG_INPUT_ACC10K;              //AU_AFE_IN_MODE_t
        device_handle.analog_mic.adc_parameter.performance = AFE_PEROFRMANCE_NORMAL_MODE;  //AU_AFE_POWER_SETTING_t
        device_handle.analog_mic.adc_parameter.is_fifo_sync_clock_ch1 = false;
        device_handle.analog_mic.adc_parameter.with_au_in_swap = false;
        hal_audio_set_device(&device_handle, device_handle.analog_mic.audio_device, HAL_AUDIO_CONTROL_ON);
    }
//#endif
}

void dsp_dc_compensation_stop(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("DC COMPENSATION Stop\r\n", 0);
    afe.stream_out.dc_compensation_value = (uint16_t)msg.ccni_message[1];
    bool enable = (bool)(uint32_t)(msg.ccni_message[1] >> 16);
    hal_audio_device_parameter_t device_handle;

    if (enable == true) {
#if 0
#ifndef CONFIG_TOOL_TEST
#ifdef ANALOG_OUTPUT_CLASSD_ENABLE
        hal_audio_analog_mdoe_t dl_analog_mode = HAL_AUDIO_ANALOG_OUTPUT_CLASSD;
        device_handle.analog_mic.audio_device = HAL_AUDIO_CONTROL_DEVICE_ANALOG_MIC_DUAL;
        device_handle.analog_mic.rate = 48000;
        device_handle.analog_mic.mic_interface = HAL_AUDIO_CONTROL_DEVICE_INTERFACE_1;
        device_handle.analog_mic.bias_select = HAL_AUDIO_BIAS_SELECT_ALL;
        device_handle.analog_mic.bias_voltage = HAL_AUDIO_BIAS_VOLTAGE_1_85V;
        device_handle.analog_mic.iir_filter = false;
        device_handle.analog_mic.with_external_bias = false;
        device_handle.analog_mic.with_bias_lowpower = false;
        device_handle.analog_mic.with_phase_inverse = false;
        device_handle.analog_mic.adc_parameter.adc_mode = HAL_AUDIO_ANALOG_INPUT_ACC10K;              //AU_AFE_IN_MODE_t
        device_handle.analog_mic.adc_parameter.performance = AFE_PEROFRMANCE_NORMAL_MODE;//AU_AFE_POWER_SETTING_t
        device_handle.analog_mic.adc_parameter.is_fifo_sync_clock_ch1 = false;
        device_handle.analog_mic.adc_parameter.with_au_in_swap = false;
        afe.stream_out.dc_compensation_value = hal_audio_get_value((hal_audio_get_value_parameter_t *)&dl_analog_mode, HAL_AUDIO_GET_DEVICE_DAC_COMPENSATION_VALUE);
        hal_audio_set_device(&device_handle, device_handle.analog_mic.audio_device, HAL_AUDIO_CONTROL_OFF);
#else
        ANA_SET_REG (AUDDEC_ANA_CON10, 0, 0x1<<2);  // Disable trim buffer's reference voltage
        AFE_WRITE(0xC0000EDC, 0x11004001);          // Setting voltage low to 0.42v
        #if 0 //modify for ab1568
        afe_amp_enable(false, HAL_AUDIO_DEVICE_DAC_DUAL);
        #else
        device_handle.dac.rate = 48000;

        device_handle.dac.dac_mode= HAL_AUDIO_ANALOG_OUTPUT_CLASSAB;

        device_handle.dac.with_high_performance = 0;
        device_handle.dac.dc_compensation_value = afe.stream_out.dc_compensation_value;
        device_handle.dac.with_high_performance = false;
        device_handle.dac.with_force_change_rate = false;
        hal_audio_set_device(&device_handle, HAL_AUDIO_CONTROL_DEVICE_INTERNAL_DAC_DUAL, HAL_AUDIO_CONTROL_OFF);
        #endif
#endif

#endif
#endif
//#ifdef CONFIG_TOOL_TEST
        hal_audio_analog_mdoe_t dac_mode = msg.ccni_message[0]&0xffff;
        DSP_MW_LOG_I("stop msg.ccni_message[0] 0x%x\r\n",1,msg.ccni_message[0]);
        afe.stream_out.dac_mode = dac_mode;
        if(dac_mode == HAL_AUDIO_ANALOG_OUTPUT_CLASSAB){
            ANA_SET_REG (AUDDEC_ANA_CON10, 0, 0x1<<2);  // Disable trim buffer's reference voltage
            AFE_WRITE(0xC0000EDC, 0x11004001);          // Setting voltage low to 0.42v
            #if 0 //modify for ab1568
            afe_amp_enable(false, HAL_AUDIO_DEVICE_DAC_DUAL);
            #else
            device_handle.dac.rate = 48000;

            device_handle.dac.dac_mode= HAL_AUDIO_ANALOG_OUTPUT_CLASSAB;

            device_handle.dac.with_high_performance = 0;
            device_handle.dac.dc_compensation_value = afe.stream_out.dc_compensation_value;
            device_handle.dac.with_high_performance = false;
            device_handle.dac.with_force_change_rate = false;
            hal_audio_set_device(&device_handle, HAL_AUDIO_CONTROL_DEVICE_INTERNAL_DAC_DUAL, HAL_AUDIO_CONTROL_OFF);
            #endif
        }else{
            hal_audio_analog_mdoe_t dl_analog_mode = HAL_AUDIO_ANALOG_OUTPUT_CLASSD;
            device_handle.analog_mic.audio_device = HAL_AUDIO_CONTROL_DEVICE_ANALOG_MIC_DUAL;
            device_handle.analog_mic.rate = 48000;
            device_handle.analog_mic.mic_interface = HAL_AUDIO_CONTROL_DEVICE_INTERFACE_1;
            device_handle.analog_mic.bias_select = HAL_AUDIO_BIAS_SELECT_ALL;
            device_handle.analog_mic.bias_voltage = HAL_AUDIO_BIAS_VOLTAGE_1_85V;
            device_handle.analog_mic.iir_filter = false;
            device_handle.analog_mic.with_external_bias = false;
            device_handle.analog_mic.with_bias_lowpower = false;
            device_handle.analog_mic.with_phase_inverse = false;
            device_handle.analog_mic.adc_parameter.adc_mode = HAL_AUDIO_ANALOG_INPUT_ACC10K;              //AU_AFE_IN_MODE_t
            device_handle.analog_mic.adc_parameter.performance = AFE_PEROFRMANCE_NORMAL_MODE;//AU_AFE_POWER_SETTING_t
            device_handle.analog_mic.adc_parameter.is_fifo_sync_clock_ch1 = false;
            device_handle.analog_mic.adc_parameter.with_au_in_swap = false;
            afe.stream_out.dc_compensation_value = hal_audio_get_value((hal_audio_get_value_parameter_t *)&dl_analog_mode, HAL_AUDIO_GET_DEVICE_DAC_COMPENSATION_VALUE);
            hal_audio_set_device(&device_handle, device_handle.analog_mic.audio_device, HAL_AUDIO_CONTROL_OFF);
        }
//#endif

    }

    DSP_MW_LOG_I("DC COMPENSATION val:0x%x\r\n", 1,afe.stream_out.dc_compensation_value);

}
void dsp_alc_switch(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("a2dp alc switch :%d\r\n", 1,msg.ccni_message[1]);
    Audio_setting->Audio_sink.alc_enable = (bool)(uint32_t)(msg.ccni_message[1]);
}

#ifdef MTK_PEQ_ENABLE
void dsp_peq_set_param(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    mcu2dsp_peq_param_p peq_param;
    peq_param = (mcu2dsp_peq_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    DSP_MW_LOG_I("PEQ set param with phase %d\r\n", 1, peq_param->phase_id);
    PEQ_Set_Param(msg, ack);
    Audio_CPD_Enable(peq_param->drc_enable, peq_param->phase_id, peq_param->drc_force_disable);
}
#endif

void dsp_set_sysram_addr(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("DSP NVDM init\r\n", 0);

    U32 sysram_start_addr;

    sysram_start_addr = hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    dsp0_nvkey_init(sysram_start_addr, 1024*3);// SysRam fixed 3k size
}


void dsp_streaming_deinit_all(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    bool enable = (bool)(uint16_t)msg.ccni_message[1];
    DSP_MW_LOG_I("DSP stream deinit,mute status :%d\r\n",1,enable);
    DAVT_StreeamingDeinitAll();
    //hal_audio_mute_stream_out(enable);
    stream_function_aec_nr_deinitialize();
    #ifdef MTK_BT_HFP_SPE_ALG_V2
    stream_function_cpd_deinitialize();
    #endif
}

void dsp_get_reference_gain(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("DSP get reference gain\r\n", 0);
    S16* Refgain_start_addr;
    Refgain_start_addr = (S16*)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    AEC_NR_GetRefGain(Refgain_start_addr);
}

#ifdef MTK_PROMPT_SOUND_ENABLE
/* CM4 VP Playback CCNI callback function */
volatile uint32_t vp_config_flag = 0;
void CB_CM4_VP_PLAYBACK_OPEN(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("[CM4_VP_PB] Open", 0);
    vp_config_flag = 0;

    /* remap to non-cacheable address */
    mcu2dsp_open_param_p open_param;
    open_param = (mcu2dsp_open_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    DSP_REMAP_SHARE_INFO(open_param->stream_in_param.playback.share_info_base_addr, uint32_t);

    playback_vp_if.source = dsp_open_stream_in(open_param);//StreamCM4VPPlaybackSource(share_info);
    playback_vp_if.sink   = dsp_open_stream_out(open_param);//StreamAudioAfe2Sink(AUDIO_HARDWARE_PCM, INSTANCE_A, AUDIO_CHANNEL_A_AND_B);
    playback_vp_if.transform = NULL;
    //stream_feature_configure_type(stream_feature_list_prompt, CODEC_PCM_COPY, CONFIG_DECODER);

}

void CB_CM4_VP_PLAYBACK_START(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("[CM4_VP_PB] Start", 0);

    mcu2dsp_start_param_p start_param;
    start_param = (mcu2dsp_start_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);

    dsp_start_stream_in (start_param, playback_vp_if.source);
    dsp_start_stream_out(start_param, playback_vp_if.sink);

    playback_vp_if.transform = TrasformAudio2Audio(playback_vp_if.source, playback_vp_if.sink, stream_feature_list_prompt);
    if (playback_vp_if.transform == NULL)
    {
        DSP_MW_LOG_E("[CM4_VP_PB] transform failed", 0);
    }
}


void CB_CM4_VP_PLAYBACK_STOP(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("[CM4_VP_PB] Stop", 0);
    if (playback_vp_if.transform != NULL)
    {
        StreamDSPClose(playback_vp_if.transform->source, playback_vp_if.transform->sink, msg.ccni_message[0]>>16|0x8000);
    }
    playback_vp_if.transform = NULL;
}

void CB_CM4_VP_PLAYBACK_CONFIG(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    /*
      Todo:
      Notify DSP framework to push remained pcm data into AFE.
    */

    /* Set flag to notify APP to stop VP */
    if (playback_vp_if.transform != NULL){
        DSP_MW_LOG_I("[CM4_VP_PB] Config", 0);
        vp_config_flag = 1;
    } else {
        DSP_MW_LOG_I("[CM4_VP_PB] Config duplicate.", 0);
    }
}

void CB_CM4_VP_PLAYBACK_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("[CM4_VP_PB] Close", 0);
    SourceClose(playback_vp_if.source);
    SinkClose(playback_vp_if.sink);
    DSP_PIC_FeatureDeinit(playback_vp_if.pfeature_table);
    memset(&playback_vp_if,0,sizeof(CONNECTION_IF));
}

void CB_CM4_VP_PLAYBACK_TRIGGER(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    DSP_MW_LOG_I("[CM4_VP_PB] Trigger", 0);
    if(afe.dl2_enable){
        //DSP_MW_LOG_I("[CM4_VP_PB] VP trigger memory irq enable", 0);
        #ifndef HAL_AUDIO_ENABLE_PATH_MEM_DEVICE
        audio_digital_block_t memory_block;
        memory_block =  hal_audio_afe_get_memory_digital_block (playback_vp_if.sink->param.audio.memory, true);
        afe_enable_audio_irq(afe_irq_request_number(memory_block), playback_vp_if.sink->param.audio.rate, playback_vp_if.sink->param.audio.count);
        afe_set_memory_path_enable(memory_block, !(playback_vp_if.sink->param.audio.AfeBlkControl.u4awsflag), true);
        #else
        hal_audio_trigger_start_parameter_t sw_trigger_start;
        sw_trigger_start.enable = true;
        sw_trigger_start.memory_select = playback_vp_if.sink->param.audio.mem_handle.memory_select;
        hal_audio_set_value((hal_audio_set_value_parameter_t *)&sw_trigger_start,HAL_AUDIO_SET_TRIGGER_MEMORY_START);
        #endif
        xTaskResumeFromISR((TaskHandle_t)pDPR_TaskHandler);
        portYIELD_FROM_ISR(pdTRUE); // force to do context switch
    }else {
        DSP_MW_LOG_I("[CM4_VP_PB] VP not exit or ready.", 0);
    }
}
#endif

void CB_N9_CLK_SKEW_LAG(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    if (msg.ccni_message[1] == ClkSkewLagsNo) {
        #ifdef DSP_CLK_SKEW_DEBUG_LOG
        DSP_MW_LOG_I("[CLK_SKEW] Lags", 0);
        #endif
        clk_skew_inform_dl_lags_samples(1);
        clk_skew_inform_ul_lags_samples(1);

        ClkSkewLagsNo++;
    }
    else {
        #ifdef DSP_CLK_SKEW_DEBUG_LOG
        DSP_MW_LOG_I("[CLK_SKEW] Lags not from N9, msg:%d", 1, msg.ccni_message[1]);
        #endif
    }

}

void CB_N9_CLK_SKEW_LEAD(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    if (msg.ccni_message[1] == ClkSkewLeadsNo) {
        #ifdef DSP_CLK_SKEW_DEBUG_LOG
        DSP_MW_LOG_I("[CLK_SKEW] Leads", 0);
        #endif
        clk_skew_inform_dl_leads_samples(1);
        clk_skew_inform_ul_leads_samples(1);

        ClkSkewLeadsNo++;
    }
    else {
        #ifdef DSP_CLK_SKEW_DEBUG_LOG
        DSP_MW_LOG_I("[CLK_SKEW] Leads not from N9, msg:%d", 1, msg.ccni_message[1]);
        #endif
    }

}

void CB_CM4_AUDIO_AMP_FORCE_CLOSE(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    UNUSED(msg);
    DSP_MW_LOG_I("[CM4_AUDIO] AMP Force Close", 0);
#ifdef ENABLE_AMP_TIMER
    fw_amp_force_close();
#endif
}

#ifdef MTK_SENSOR_SOURCE_ENABLE
void hal_audio_gsensor_timer_callback(void *user_data)
{
    gsensor_motion_detect_config_read_flg(true);
    volatile SOURCE source = Source_blks[SOURCE_TYPE_GSENSOR];
    AudioCheckTransformHandle(source->transform);
    hal_gpt_sw_start_timer_us(gsensor_timer_handler,40000,hal_audio_gsensor_timer_callback,NULL);
}

void CB_GSENSOR_DETECT_START(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{

    mcu2dsp_open_param_t open_param;
    bsp_multi_axis_config_t acc_cfg = {BMA4_ACCEL_RANGE_4G,
                                       BSP_MULTI_AXIS_ACCEL_NORMAL_AVG4,
                                       BSP_MULTI_AXIS_OUTPUT_DATA_RATE_1600HZ};
    open_param.param.stream_in = STREAM_IN_GSENSOR;
    open_param.param.stream_out = STREAM_OUT_GSENSOR;
    sensor_src_if.source = dsp_open_stream_in(&open_param);
    sensor_src_if.sink   = dsp_open_stream_out(&open_param);
    sensor_src_if.transform = NULL;
    sensor_src_if.pfeature_table = &AudioFeatureList_GSensorMotionDetect;
    DSP_Callback_PreloaderConfig(&AudioFeatureList_GSensorMotionDetect);
    sensor_src_if.transform = TrasformAudio2Audio(sensor_src_if.source, sensor_src_if.sink, AudioFeatureList_GSensorMotionDetect);
#if 1
    if (BSP_MULTI_AXIS_OK != bsp_multi_axis_initialize(BMA456,&acc_cfg)) {
        DSP_MW_LOG_E("[G-sensor] bsp_multi_axis_initialize error!\r\n",0);
        return;
    }

    if (BSP_MULTI_AXIS_OK != bsp_multi_axis_enable(BMA456)) {
        DSP_MW_LOG_E("[G-sensor] bsp_multi_axis_enable error!\r\n",0);
        return;
    }
#endif
    if (gsensor_timer_handler == 0) {
        if (HAL_GPT_STATUS_OK != hal_gpt_sw_get_timer(&gsensor_timer_handler)) {
            DSP_MW_LOG_E("[G-sensor] hal_gpt_sw_get_timer error!\r\n",0);
            return;
        }
    }
    hal_gpt_sw_start_timer_us(gsensor_timer_handler,40000,hal_audio_gsensor_timer_callback,NULL);
}

void CB_GSENSOR_DETECT_STOP(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    bsp_multi_axis_status_t ret;

    hal_gpt_sw_stop_timer_us(gsensor_timer_handler);
#if 1
    while (ret = bsp_multi_axis_deinit(BMA456)) {
        if (ret == BSP_MULTI_AXIS_OK) {
            break;
        }else if (ret == BSP_MULTI_AXIS_RETURN_INIT_VALUE) {
            DSP_MW_LOG_E("[G-sensor] bsp_multi_axis_deinit error!\r\n",0);
            return;
        }
        vTaskDelay(20);
    }

    /*if (BSP_MULTI_AXIS_OK != bsp_multi_axis_deinit(BMA456)) {
        DSP_MW_LOG_E("[G-sensor] bsp_multi_axis_deinit error!\r\n",0);
        return;
    }*/
#endif


    if (sensor_src_if.transform != NULL)
    {
        //StreamDSPClose(sensor_src_if.transform->source,sensor_src_if.transform->sink,msg.ccni_message[0]>>16|0x8000);
    }
    sensor_src_if.transform = NULL;

    //SourceClose(sensor_src_if.source);
    //SinkClose(sensor_src_if.sink);
    //DSP_PIC_FeatureDeinit(sensor_src_if.pfeature_table);
    memset(&sensor_src_if,0,sizeof(CONNECTION_IF));
}

void CB_GSENSOR_DETECT_READ_RG(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    uint8_t data = 0;

    if (bsp_multi_axis_read_register((uint32_t)msg.ccni_message[0]&0xffff,&data) == false) {
        log_hal_msgid_error("[G-sensor] CB_CM4_GSENSOR_READ_RG fail",0);
        return;
    }
    log_hal_msgid_info("[G-sensor]CB_CM4_GSENSOR_READ_RG,addr = 0x%08x,data = 0x%08x",2,msg.ccni_message[0]&0xffff,data);
}

void CB_GSENSOR_DETECT_WRITE_RG(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    uint8_t data = 0;

    data = (uint8_t)msg.ccni_message[1];
    if (bsp_multi_axis_write_register((uint32_t)msg.ccni_message[0]&0xffff,&data) == false) {
        log_hal_msgid_error("[G-sensor] CB_CM4_GSENSOR_WRITE_RG fail",0);
        return;
    }

    log_hal_msgid_info("[G-sensor]CB_CM4_GSENSOR_WRITE_RG,addr = 0x%08x,data = 0x%08x",2,msg.ccni_message[0]&0xffff,data);
}
#endif
#ifdef MTK_AUDIO_PLC_ENABLE
void DSP_AUDIO_PLC_CONTROL(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(ack);
    DSP_MW_LOG_I("DSP_AUDIO_PLC_CONTROL\r\n", 0);

    uint32_t enable = (uint32_t)(msg.ccni_message[1]);
    dsp_audio_plc_ctrl_t audio_plc_ctrl;
    audio_plc_ctrl.enable = enable;
    Audio_PLC_ctrl(audio_plc_ctrl);
    DSP_MW_LOG_I("DSP_AUDIO_PLC_CONTROL ENABLE %d\r\n", 1,enable);
}
#endif

#ifdef MTK_DSP_SHUTDOWN_SPECIAL_CONTROL_ENABLE
void DSP_DUMMY_SHUTDOWN(void)
{
    bool ack = false;
    uint32_t savedmask;
    uint16_t count = 1;
    hal_nvic_save_and_set_interrupt_mask(&savedmask);
    //**Special control.
    //**To avoid DSP task HW semaphore when CM4 disable DSP power by using SPM control.
    hal_ccni_message_t msg;
    memset((void *)&msg, 0, sizeof(hal_ccni_message_t));
    msg.ccni_message[0] = 0x804b << 16;
    while(1){
        count ++;
        if((count % 1000) == 0){
            if(!ack){
                if(hal_ccni_set_event(AUDIO_CM4_TX_EVENT, &msg) == HAL_CCNI_STATUS_OK){ /*Remove all OS API, just call driver.*/
                    ack = true;
                }
            }
        }
    }
}
#endif

#ifdef MTK_MULTI_MIC_STREAM_ENABLE
extern stream_feature_list_t stream_featuremulti_mic_function_a[];
extern stream_feature_list_t stream_featuremulti_mic_function_b[];
extern stream_feature_list_t stream_featuremulti_mic_function_c[];
extern stream_feature_list_t stream_featuremulti_mic_function_f[];

CONNECTION_IF multi_mic_stream[AUDIO_SCENARIO_MULTI_MIC_STREAM_MAX] =
{/*source,      sink,  transform, pfeature_table;*/
    {NULL,      NULL,       NULL,     stream_featuremulti_mic_function_a},  //AUDIO_SCENARIO_MULTI_MIC_STREAM_FUNCTION_A
    {NULL,      NULL,       NULL,     stream_featuremulti_mic_function_b},  //AUDIO_SCENARIO_MULTI_MIC_STREAM_FUNCTION_B
    {NULL,      NULL,       NULL,     stream_featuremulti_mic_function_c},  //AUDIO_SCENARIO_MULTI_MIC_STREAM_FUNCTION_C
    {NULL,      NULL,       NULL,     stream_featuremulti_mic_function_f},  //AUDIO_SCENARIO_MULTI_MIC_STREAM_FUNCTION_F
};
#endif

CONNECTION_IF * dsp_application_get_connection_if(audio_transmitter_receive_scenario_t scenario_id, audio_scenario_sub_id_t sub_id)
{
    CONNECTION_IF *application_ptr = NULL;
    UNUSED(scenario_id);
    UNUSED(sub_id);
    //Get connection if pointer
    if (0) {

#ifdef MTK_MULTI_MIC_STREAM_ENABLE
    } else  if (scenario_id == AUDIO_TRANSMITTER_RX_MULTI_MIC_STREAM) {
        application_ptr = &multi_mic_stream[sub_id];
#endif
    }



    if (!application_ptr) {
        DSP_MW_LOG_E("DSP data get_connection_if failed", 0);
        assert(0);
    }
    return application_ptr;
}

void dsp_application_open(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    CONNECTION_IF *application_ptr;
    mcu2dsp_open_param_p open_param;
    audio_transmitter_receive_scenario_t scenario_id;
    audio_scenario_sub_id_t sub_id;
    UNUSED(ack);
    scenario_id = (msg.ccni_message[0]>>8)&0xFF;
    sub_id = (msg.ccni_message[0]&0xFF);
    DSP_MW_LOG_I("DSP application open scenario:%d, Sub_ID:%d \r\n", 2, scenario_id, sub_id);

    open_param = (mcu2dsp_open_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    application_ptr = dsp_application_get_connection_if(scenario_id, sub_id);

    application_ptr->source     = dsp_open_stream_in(open_param);
    application_ptr->sink       = dsp_open_stream_out(open_param);
    application_ptr->transform = NULL;

    DSP_MW_LOG_I("DSP application open finish\r\n", 0);
}

void dsp_application_start(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{

    CONNECTION_IF *application_ptr;
    mcu2dsp_start_param_p start_param;
    audio_transmitter_receive_scenario_t scenario_id;
    audio_scenario_sub_id_t sub_id;
    uint32_t gpt_timer;
    UNUSED(ack);
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &gpt_timer);
    scenario_id = (msg.ccni_message[0]>>8)&0xFF;
    sub_id = (msg.ccni_message[0]&0xFF);
    DSP_MW_LOG_I("DSP application start:%d scenario:%d, Sub_ID:%d \r\n", 3, gpt_timer, scenario_id, sub_id);

    start_param = (mcu2dsp_start_param_p)hal_memview_cm4_to_dsp0(msg.ccni_message[1]);
    application_ptr = dsp_application_get_connection_if(scenario_id, sub_id);

    dsp_start_stream_in (start_param, application_ptr->source);
    dsp_start_stream_out(start_param, application_ptr->sink);

    application_ptr->transform = TrasformAudio2Audio(application_ptr->source, application_ptr->sink, application_ptr->pfeature_table);
    if (application_ptr->transform == NULL)
    {
        DSP_MW_LOG_E("DSP application transform failed", 0);
        assert(0);
    }
    DSP_MW_LOG_I("DSP application start finish",0);
}

void dsp_application_stop(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    CONNECTION_IF *application_ptr;
    audio_transmitter_receive_scenario_t scenario_id;
    audio_scenario_sub_id_t sub_id;
    UNUSED(ack);
    scenario_id = (msg.ccni_message[0]>>8)&0xFF;
    sub_id = (msg.ccni_message[0]&0xFF);
    DSP_MW_LOG_I("DSP application stop scenario:%d, Sub_ID:%d \r\n", 2, scenario_id, sub_id);

    application_ptr = dsp_application_get_connection_if(scenario_id, sub_id);

    if (application_ptr->transform != NULL)
    {
        StreamDSPClose(application_ptr->transform->source, application_ptr->transform->sink, msg.ccni_message[0]>>16|0x8000);
    }
    application_ptr->transform = NULL;
}

void dsp_application_close(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    CONNECTION_IF *application_ptr;
    audio_transmitter_receive_scenario_t scenario_id;
    audio_scenario_sub_id_t sub_id;
    UNUSED(ack);
    scenario_id = (msg.ccni_message[0]>>8)&0xFF;
    sub_id = (msg.ccni_message[0]&0xFF);
    DSP_MW_LOG_I("DSP application close scenario:%d, Sub_ID:%d \r\n", 2, scenario_id, sub_id);

    application_ptr = dsp_application_get_connection_if(scenario_id, sub_id);

    SourceClose(application_ptr->source);
    application_ptr->source = NULL;
    SinkClose(application_ptr->sink);
    application_ptr->sink = NULL;
    DSP_PIC_FeatureDeinit(application_ptr->pfeature_table);
}

void dsp_application_play(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    CONNECTION_IF *application_ptr;
    audio_transmitter_receive_scenario_t scenario_id;
    audio_scenario_sub_id_t sub_id;
    UNUSED(ack);
    scenario_id = (msg.ccni_message[0]>>8)&0xFF;
    sub_id = (msg.ccni_message[0]&0xFF);
    DSP_MW_LOG_I("DSP application play(SW trigger) scenario:%d, Sub_ID:%d \r\n", 2, scenario_id, sub_id);

    application_ptr = dsp_application_get_connection_if(scenario_id, sub_id);


    hal_audio_trigger_start_parameter_t start_parameter;
    start_parameter.memory_select = 0;
    if (audio_ops_distinguish_audio_source(application_ptr->source) == TRUE)  {
        start_parameter.memory_select |= application_ptr->source->param.audio.mem_handle.memory_select;
    }
    if (audio_ops_distinguish_audio_sink(application_ptr->sink) == TRUE)  {
        start_parameter.memory_select |= application_ptr->sink->param.audio.mem_handle.memory_select;
    }
    start_parameter.enable = true;
    hal_audio_set_value((hal_audio_set_value_parameter_t *)&start_parameter, HAL_AUDIO_SET_TRIGGER_MEMORY_START);

}

void dsp_application_suspend(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    CONNECTION_IF *application_ptr;
    audio_transmitter_receive_scenario_t scenario_id;
    audio_scenario_sub_id_t sub_id;
    UNUSED(ack);
    scenario_id = (msg.ccni_message[0]>>8)&0xFF;
    sub_id = (msg.ccni_message[0]&0xFF);
    DSP_MW_LOG_I("DSP application suspend scenario:%d, Sub_ID:%d \r\n", 2, scenario_id, sub_id);

    application_ptr = dsp_application_get_connection_if(scenario_id, sub_id);
    dsp_trigger_suspend(application_ptr->source, application_ptr->sink);
}

void dsp_application_resume(hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    CONNECTION_IF *application_ptr;
    audio_transmitter_receive_scenario_t scenario_id;
    audio_scenario_sub_id_t sub_id;
    UNUSED(ack);
    scenario_id = (msg.ccni_message[0]>>8)&0xFF;
    sub_id = (msg.ccni_message[0]&0xFF);
    DSP_MW_LOG_I("DSP application resume scenario:%d, Sub_ID:%d \r\n", 2, scenario_id, sub_id);

    application_ptr = dsp_application_get_connection_if(scenario_id, sub_id);
    dsp_trigger_resume(application_ptr->source, application_ptr->sink);
}

#ifdef MTK_SLT_AUDIO_HW
void AUDIO_SLT_START(hal_ccni_message_t msg, hal_ccni_message_t *ack){
    uint32_t result_total;
    uint32_t rate;
    //audio_slt_enable(48000);
    printf("SLT rate= %d",msg.ccni_message[1]);
    rate = msg.ccni_message[1];

    result_total = audio_slt_test_case(rate);

    printf("AUDIO_SLT_START");
#if 1
    //hal_ccni_message_t msg;
    memset((void *)&msg, 0, sizeof(hal_ccni_message_t));
    msg.ccni_message[0] = 0x70 << 16 | 0x80000000;
    printf("result_total 0x%x",result_total);
    msg.ccni_message[1] = result_total;//0x123;
    aud_msg_tx_handler(msg, 0, FALSE);
#endif

}
#endif
