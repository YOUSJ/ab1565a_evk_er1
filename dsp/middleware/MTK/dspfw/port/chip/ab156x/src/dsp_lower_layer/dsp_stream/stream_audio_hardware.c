/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

/*!
 *@file   stream_audio_hardware.c
 *@brief  Defines the hardware control for audio stream
 *
 @verbatim
         * Programmer : SYChiu@airoha.com.tw, Ext.3307
         * Programmer : BrianChen@airoha.com.tw, Ext.2641
         * Programmer : MachiWu@airoha.com.tw, Ext.2673
 @endverbatim
 */

//-
#include "types.h"
#include "audio_config.h"
#include "source_inter.h"
#include "sink_inter.h"
#include "stream_audio_setting.h"
#include "dtm.h"
#include "stream_audio_driver.h"
#include "stream_audio_hardware.h"
#include "hal_audio_afe_control.h"
#include "dsp_audio_ctrl.h"
U16  ADC_SOFTSTART;
EXTERN afe_t afe;


#define AUDIO_AFE_DL_DEFAULT_FRAME_NUM 4
#define AUDIO_AFE_UL_DEFAULT_FRAME_NUM 4
#define AUDIO_AFE_SOURCE_ASRC_BUFFER_SIZE 4096
void afe_dl2_interrupt_handler(void);
void afe_dl1_interrupt_handler(void);
void stream_audio_srcl_interrupt(void);
void stream_audio_src2_interrupt(void);
void Source_device_set_para(hal_audio_device_parameter_t *device_handle);
hal_audio_bias_selection_t micbias_para_convert(MICBIAS_SOURCE_TYPE  MicbiasSourceType);

VOID Sink_Audio_Get_Default_Parameters(SINK sink)
{
    AUDIO_PARAMETER *pAudPara = &sink->param.audio;
    afe_pcm_format_t format;
    uint32_t media_frame_samples;
    //modify for ab1568
    hal_audio_path_parameter_t *path_handle = &sink->param.audio.path_handle;
    hal_audio_memory_parameter_t *mem_handle = &sink->param.audio.mem_handle;
    hal_audio_device_parameter_t *device_handle = &sink->param.audio.device_handle;
    memset(&sink->param.audio.AfeBlkControl, 0, sizeof(afe_block_t));

    format = gAudioCtrl.Afe.AfeDLSetting.format ;
    /* calculate memory size for delay */
    if (format == AFE_PCM_FORMAT_S32_LE ||
        format == AFE_PCM_FORMAT_U32_LE ||
        format == AFE_PCM_FORMAT_S24_LE ||
        format == AFE_PCM_FORMAT_U24_LE)
        pAudPara->format_bytes = 4;
    else
        pAudPara->format_bytes = 2;

    pAudPara->format        = format;

    pAudPara->channel_num = ((sink->param.audio.channel_sel == AUDIO_CHANNEL_A) ||
                             (sink->param.audio.channel_sel == AUDIO_CHANNEL_B) ||
                             (sink->param.audio.channel_sel == AUDIO_CHANNEL_VP))
                                        ? 1 : 2;

    pAudPara->rate          = gAudioCtrl.Afe.AfeDLSetting.rate;
    pAudPara->src_rate      = gAudioCtrl.Afe.AfeDLSetting.src_rate;
    pAudPara->period        = gAudioCtrl.Afe.AfeDLSetting.period;           /* ms, how many period to trigger */

#if 1   // for FPGA early porting
    if (sink->type == SINK_TYPE_VP_AUDIO) {
        pAudPara->sw_channels = pAudPara->channel_num;
        media_frame_samples = Audio_setting->Audio_VP.Frame_Size;
    } else {
        pAudPara->sw_channels = Audio_setting->Audio_sink.Software_Channel_Num;
        media_frame_samples = Audio_setting->Audio_sink.Frame_Size;//Audio_setting->Audio_sink.Frame_Size;//AUDIO_AAC_FRAME_SAMPLES;
    }
#else
    switch (gAudioCtrl.Afe.OperationMode) {
        case AU_AFE_OP_ESCO_VOICE_MODE:
            media_frame_samples = AUDIO_SBC_FRAME_SAMPLES;  // use mSBC (worst case)
            break;
        case AU_AFE_OP_PLAYBACK_MODE:
            media_frame_samples = AUDIO_AAC_FRAME_SAMPLES; // TODO:
            break;
        default:
            media_frame_samples = AUDIO_SBC_FRAME_SAMPLES;
            break;
    }
#endif

    pAudPara->buffer_size   = AUDIO_AFE_DL_DEFAULT_FRAME_NUM * media_frame_samples *
                              pAudPara->channel_num * pAudPara->format_bytes;

    pAudPara->AfeBlkControl.u4asrc_buffer_size = pAudPara->buffer_size;
    pAudPara->count         = (pAudPara->rate * pAudPara->period) / 1000;

    if (pAudPara->count >= pAudPara->buffer_size) {
        pAudPara->count = pAudPara->buffer_size >> 2;
    }
    if (pAudPara->period == 0) {
        pAudPara->count = media_frame_samples;
        pAudPara->period = media_frame_samples / (pAudPara->rate /1000);
    }
    pAudPara->audio_device                   = gAudioCtrl.Afe.AfeDLSetting.audio_device;
#ifdef ENABLE_2A2D_TEST
    pAudPara->audio_device1                  = HAL_AUDIO_CONTROL_NONE;
    pAudPara->audio_device2                  = HAL_AUDIO_CONTROL_NONE;
    pAudPara->audio_device3                  = HAL_AUDIO_CONTROL_NONE;
#endif
    pAudPara->stream_channel                 = gAudioCtrl.Afe.AfeDLSetting.stream_channel;
    pAudPara->memory                         = gAudioCtrl.Afe.AfeDLSetting.memory;
    pAudPara->audio_interface                = gAudioCtrl.Afe.AfeDLSetting.audio_interface;
#ifdef ENABLE_2A2D_TEST
    pAudPara->audio_interface1               = HAL_AUDIO_INTERFACE_NONE;
    pAudPara->audio_interface2               = HAL_AUDIO_INTERFACE_NONE;
    pAudPara->audio_interface3               = HAL_AUDIO_INTERFACE_NONE;
#endif
    pAudPara->hw_gain                        = gAudioCtrl.Afe.AfeDLSetting.hw_gain;
    pAudPara->echo_reference                 = gAudioCtrl.Afe.AfeDLSetting.echo_reference;
    #ifdef AUTO_ERROR_SUPPRESSION
    pAudPara->misc_parms.I2sClkSourceType    = gAudioCtrl.Afe.AfeDLSetting.misc_parms.I2sClkSourceType;
    pAudPara->misc_parms.MicbiasSourceType   = gAudioCtrl.Afe.AfeDLSetting.misc_parms.MicbiasSourceType;
    #endif
    DSP_MW_LOG_I("audio sink default buffer_size:%d, count:%d\r\n", 2, pAudPara->buffer_size, pAudPara->count);
    DSP_MW_LOG_I("audio sink default device:%d, channel:%d, memory:%d, interface:%d rate:%d\r\n", 5, pAudPara->audio_device,
                                                                                    pAudPara->stream_channel,
                                                                                    pAudPara->memory,
                                                                                    pAudPara->audio_interface,
                                                                                    pAudPara->rate);

    //modfiy for ab1568
    pAudPara->with_sink_src = false;//HWSRC Continuous mode
    //for hal_audio_set_memory
    mem_handle->buffer_length = pAudPara->buffer_size;
    mem_handle->memory_select = hal_memory_convert_dl(pAudPara->memory);//modify for ab1568
    mem_handle->irq_counter = pAudPara->count;
    mem_handle->pcm_format = pAudPara->format;
#ifdef ENABLE_HWSRC_CLKSKEW
    mem_handle->asrc_clkskew_mode = Audio_setting->Audio_sink.clkskew_mode;
#endif
#if 1
    if (mem_handle->memory_select == HAL_AUDIO_MEMORY_DL_DL1) {
        hal_audio_set_value_parameter_t handle;
        if(pAudPara->with_sink_src){
            mem_handle->memory_select = HAL_AUDIO_MEMORY_DL_SRC1;
            handle.register_irq_handler.audio_irq = HAL_AUDIO_IRQ_SRC1;
            handle.register_irq_handler.memory_select = HAL_AUDIO_MEMORY_DL_SRC1;
            handle.register_irq_handler.entry = stream_audio_srcl_interrupt;
        }else{
            handle.register_irq_handler.audio_irq = HAL_AUDIO_IRQ_AUDIOSYS;
            handle.register_irq_handler.memory_select = HAL_AUDIO_MEMORY_DL_DL1;
            handle.register_irq_handler.entry = afe_dl1_interrupt_handler;
        }
        DSP_MW_LOG_I("DL 1 memory_select %d,audio_irq %d,entry %x\r\n",3,handle.register_irq_handler.memory_select,handle.register_irq_handler.audio_irq,handle.register_irq_handler.entry);
        hal_audio_set_value(&handle, HAL_AUDIO_SET_IRQ_HANDLER);
    } else if (mem_handle->memory_select == HAL_AUDIO_MEMORY_DL_DL2) {
        hal_audio_set_value_parameter_t handle;
        if(pAudPara->with_sink_src){
            mem_handle->memory_select = HAL_AUDIO_MEMORY_DL_SRC2;
            handle.register_irq_handler.audio_irq = HAL_AUDIO_IRQ_SRC2;
            handle.register_irq_handler.memory_select = HAL_AUDIO_MEMORY_DL_SRC2;
            handle.register_irq_handler.entry = stream_audio_src2_interrupt;
        }else{
            handle.register_irq_handler.audio_irq = HAL_AUDIO_IRQ_AUDIOSYS;
            handle.register_irq_handler.memory_select = HAL_AUDIO_MEMORY_DL_DL2;
            handle.register_irq_handler.entry = afe_dl2_interrupt_handler;
        }
        DSP_MW_LOG_I("DL 2 memory_select %d,audio_irq %d,entry %x\r\n",3,handle.register_irq_handler.memory_select,handle.register_irq_handler.audio_irq,handle.register_irq_handler.entry);
        hal_audio_set_value(&handle, HAL_AUDIO_SET_IRQ_HANDLER);
    } else if (mem_handle->memory_select == HAL_AUDIO_MEMORY_DL_SLAVE_DMA) {
        hal_audio_set_value_parameter_t handle;
        handle.register_irq_handler.audio_irq = HAL_AUDIO_IRQ_I2S_SLAVE;
        handle.register_irq_handler.entry = afe_dl1_interrupt_handler;//need modify
        DSP_MW_LOG_I("DL Slave DMA audio_irq %d,entry %x\r\n",2,handle.register_irq_handler.audio_irq,handle.register_irq_handler.entry);
        hal_audio_set_value(&handle, HAL_AUDIO_SET_IRQ_HANDLER);
    } else if (mem_handle->memory_select == HAL_AUDIO_MEMORY_DL_SLAVE_TDM) {
        hal_audio_set_value_parameter_t handle;
        handle.register_irq_handler.audio_irq = HAL_AUDIO_IRQ_I2S_SLAVE;
        handle.register_irq_handler.entry = afe_dl1_interrupt_handler;//need modify
        DSP_MW_LOG_I("DL Slave TDM audio_irq %d,entry %x\r\n",2,handle.register_irq_handler.audio_irq,handle.register_irq_handler.entry);
        hal_audio_set_value(&handle, HAL_AUDIO_SET_IRQ_HANDLER);
    }
    mem_handle->with_mono_channel = (pAudPara->channel_num == 1) ? true : false;

    //for hal_audio_set_path
    path_handle->audio_path_input_rate = pAudPara->rate;//afe_get_audio_device_samplerate(pAudPara->audio_device , pAudPara->audio_interface);
    path_handle->audio_path_output_rate = pAudPara->rate;//afe_get_audio_device_samplerate(pAudPara->audio_device , pAudPara->audio_interface);
    path_handle->connection_number = pAudPara->channel_num;

    uint32_t i;
    hal_audio_path_port_parameter_t input_port_parameters, output_port_parameters;
    input_port_parameters.memory_select = mem_handle->memory_select;
    output_port_parameters.device_interface = pAudPara->audio_interface;
    for (i=0 ; i<path_handle->connection_number ; i++) {
        path_handle->input.interconn_sequence[i]  = stream_audio_convert_control_to_interconn(HAL_AUDIO_CONTROL_MEMORY_INTERFACE, input_port_parameters, (mem_handle->with_mono_channel) ? 0: i, false);
        path_handle->output.interconn_sequence[i] = stream_audio_convert_control_to_interconn(pAudPara->audio_device, output_port_parameters, i, false);
    }

    path_handle->with_hw_gain = pAudPara->hw_gain;
    path_handle->with_upwdown_sampler = false;
    path_handle->with_dl_deq_mixer = false;//for anc & deq
    //for hal_audio_set_device
    device_handle->common.audio_device = pAudPara->audio_device;
    if (pAudPara->audio_device & HAL_AUDIO_CONTROL_DEVICE_INTERNAL_DAC_DUAL) {
        device_handle->dac.rate = pAudPara->rate;
        device_handle->dac.dac_mode = gAudioCtrl.Afe.AfeDLSetting.adc_mode;//HAL_AUDIO_ANALOG_OUTPUT_CLASSAB;
        device_handle->dac.dc_compensation_value = afe.stream_out.dc_compensation_value;
        device_handle->dac.with_high_performance = false;
        device_handle->dac.with_phase_inverse = false;
        device_handle->dac.with_force_change_rate = false;
    } else if (pAudPara->audio_device & HAL_AUDIO_CONTROL_DEVICE_I2S_MASTER) {
        device_handle->i2s_master.rate = pAudPara->rate;
        device_handle->i2s_master.i2s_interface = gAudioCtrl.Afe.AfeDLSetting.audio_interface;
        device_handle->i2s_master.i2s_format = HAL_AUDIO_I2S_I2S;
        device_handle->i2s_master.word_length = HAL_AUDIO_I2S_WORD_LENGTH_32BIT;
        device_handle->i2s_master.mclk_divider = 2;
        device_handle->i2s_master.with_mclk = false;
        device_handle->i2s_master.is_low_jitter = false;
        device_handle->i2s_master.is_recombinant = false;
    } else if (pAudPara->audio_device & HAL_AUDIO_CONTROL_DEVICE_I2S_SLAVE) {
        device_handle->i2s_slave.rate = pAudPara->rate;
        device_handle->i2s_slave.i2s_interface = HAL_AUDIO_CONTROL_DEVICE_INTERFACE_1;
        device_handle->i2s_slave.i2s_format = HAL_AUDIO_I2S_I2S;
        device_handle->i2s_slave.word_length = HAL_AUDIO_I2S_WORD_LENGTH_32BIT;
        device_handle->i2s_slave.is_vdma_mode = false;
        device_handle->i2s_slave.memory_select = mem_handle->memory_select;
    } else if (pAudPara->audio_device & HAL_AUDIO_CONTROL_DEVICE_SPDIF) {
        device_handle->spdif.i2s_setting.rate = pAudPara->rate;
        device_handle->spdif.i2s_setting.i2s_interface = HAL_AUDIO_CONTROL_DEVICE_INTERFACE_1;
        device_handle->spdif.i2s_setting.i2s_format = HAL_AUDIO_I2S_I2S;
        device_handle->spdif.i2s_setting.word_length = HAL_AUDIO_I2S_WORD_LENGTH_32BIT;
        device_handle->spdif.i2s_setting.mclk_divider = 2;
        device_handle->spdif.i2s_setting.with_mclk = false;
        device_handle->spdif.i2s_setting.is_low_jitter = false;
        device_handle->spdif.i2s_setting.is_recombinant = false;
    }
    DSP_MW_LOG_I("with_hw_gain %d\r\n",path_handle->with_hw_gain);
#endif
}

extern void afe_vul1_interrupt_handler(void);

hal_audio_bias_selection_t micbias_para_convert(MICBIAS_SOURCE_TYPE  MicbiasSourceType){
    hal_audio_bias_selection_t bias_selection;
    switch(MicbiasSourceType){
        case MICBIAS_0:
            bias_selection = HAL_AUDIO_BIAS_SELECT_BIAS0;
        break;
        case MICBIAS_1:
            bias_selection = HAL_AUDIO_BIAS_SELECT_BIAS1;
        break;
        case MICBIAS_ALL:
            bias_selection = HAL_AUDIO_BIAS_SELECT_ALL;
        break;
        default:
            bias_selection = HAL_AUDIO_BIAS_SELECT_ALL;
            DSP_MW_LOG_I("micbias selection wrong:%d",1,MicbiasSourceType);
        break;

    }
    return bias_selection;
}

VOID Source_Audio_Get_Default_Parameters(SOURCE source)
{
    AUDIO_PARAMETER *pAudPara = &source->param.audio;
    afe_pcm_format_t format;
    uint32_t media_frame_samples;
    hal_audio_path_parameter_t *path_handle = &source->param.audio.path_handle;//modify for ab1568
    hal_audio_memory_parameter_t *mem_handle = &source->param.audio.mem_handle;//modify for ab1568
    hal_audio_device_parameter_t *device_handle = &source->param.audio.device_handle;//modify for ab1568
    #ifdef ENABLE_2A2D_TEST
    hal_audio_device_parameter_t *device_handle1 = &source->param.audio.device_handle1;//modify for ab1568
    hal_audio_device_parameter_t *device_handle2 = &source->param.audio.device_handle2;//modify for ab1568
    hal_audio_device_parameter_t *device_handle3 = &source->param.audio.device_handle3;//modify for ab1568
    #endif
    memset(&source->param.audio.AfeBlkControl, 0, sizeof(afe_block_t));

    format = gAudioCtrl.Afe.AfeULSetting.format ;
    /* calculate memory size for delay */
    if (format == AFE_PCM_FORMAT_S32_LE ||
        format == AFE_PCM_FORMAT_U32_LE ||
        format == AFE_PCM_FORMAT_S24_LE ||
        format == AFE_PCM_FORMAT_U24_LE)
        pAudPara->format_bytes = 4;
    else
        pAudPara->format_bytes = 2;

    pAudPara->format        = format;

    if((pAudPara->channel_sel == AUDIO_CHANNEL_A) || (pAudPara->channel_sel == AUDIO_CHANNEL_B)) {
        pAudPara->channel_num = 1;
    } else if (pAudPara->channel_sel == AUDIO_CHANNEL_A_AND_B) {
        pAudPara->channel_num = 2;
    } else if (pAudPara->channel_sel == AUDIO_CHANNEL_3ch) {
        pAudPara->channel_num = 3;
    } else if (pAudPara->channel_sel == AUDIO_CHANNEL_4ch) {
        pAudPara->channel_num = 4;
    } else {
        pAudPara->channel_num = 2;
    }

    DSP_MW_LOG_I("audio source default channel_num:%d, channel_sel:%d", 2, pAudPara->channel_num, pAudPara->channel_sel);


   // pAudPara->channel_num   = 1;
    pAudPara->rate          = gAudioCtrl.Afe.AfeULSetting.rate;
    pAudPara->src_rate      = gAudioCtrl.Afe.AfeULSetting.src_rate;
    pAudPara->period        = gAudioCtrl.Afe.AfeULSetting.period;

    // for early porting
    media_frame_samples = Audio_setting->Audio_source.Frame_Size;//AUDIO_AAC_FRAME_SAMPLES;

    uint8_t channel_num     = (pAudPara->channel_num>=2) ? 2 : 1;
    pAudPara->buffer_size   = media_frame_samples*Audio_setting->Audio_source.Buffer_Frame_Num*channel_num*pAudPara->format_bytes;

    pAudPara->AfeBlkControl.u4asrc_buffer_size = AUDIO_AFE_SOURCE_ASRC_BUFFER_SIZE;//AUDIO_AFE_BUFFER_SIZE;//pAudPara->buffer_size;
    //pAudPara->buffer_size   = AUDIO_SOURCE_DEFAULT_FRAME_NUM * media_frame_samples *
    //                          pAudPara->channel_num * pAudPara->format_bytes;
// printf("===> %d %d %d %d = %d\r\n", AUDIO_SOURCE_DEFAULT_FRAME_NUM, media_frame_samples, pAudPara->channel_num,pAudPara->format_bytes,   pAudPara->buffer_size);
    pAudPara->count         = (pAudPara->rate * pAudPara->period) / 1000;
    if (pAudPara->count >= pAudPara->buffer_size) {
        pAudPara->count = pAudPara->buffer_size >> 2;
    }
    if (pAudPara->period == 0) {
        pAudPara->count = media_frame_samples;
        pAudPara->period = media_frame_samples / (pAudPara->rate /1000);
    }
    pAudPara->audio_device                   = gAudioCtrl.Afe.AfeULSetting.audio_device;
#ifdef ENABLE_2A2D_TEST
    pAudPara->audio_device1                  = gAudioCtrl.Afe.AfeULSetting.audio_device1;
    pAudPara->audio_device2                  = gAudioCtrl.Afe.AfeULSetting.audio_device2;
    pAudPara->audio_device3                  = gAudioCtrl.Afe.AfeULSetting.audio_device3;
#endif
    pAudPara->stream_channel                 = gAudioCtrl.Afe.AfeULSetting.stream_channel;
    pAudPara->memory                         = gAudioCtrl.Afe.AfeULSetting.memory;
    pAudPara->audio_interface                = gAudioCtrl.Afe.AfeULSetting.audio_interface;
#ifdef ENABLE_2A2D_TEST
    pAudPara->audio_interface1               = gAudioCtrl.Afe.AfeULSetting.audio_interface1;
    pAudPara->audio_interface2               = gAudioCtrl.Afe.AfeULSetting.audio_interface2;
    pAudPara->audio_interface3               = gAudioCtrl.Afe.AfeULSetting.audio_interface3;
#endif
    pAudPara->hw_gain                        = gAudioCtrl.Afe.AfeULSetting.hw_gain;
    pAudPara->echo_reference                 = gAudioCtrl.Afe.AfeULSetting.echo_reference;
    #ifdef AUTO_ERROR_SUPPRESSION
    pAudPara->misc_parms.I2sClkSourceType    = gAudioCtrl.Afe.AfeULSetting.misc_parms.I2sClkSourceType;
    pAudPara->misc_parms.MicbiasSourceType   = gAudioCtrl.Afe.AfeULSetting.misc_parms.MicbiasSourceType;
    #endif
    DSP_MW_LOG_I("audio source default buffer_size:%d, count:%d\r\n", 2, pAudPara->buffer_size, pAudPara->count);
    DSP_MW_LOG_I("audio source default device:%d, channel:%d, memory:%d, interface:%d rate %d\r\n", 5, pAudPara->audio_device,
                                                                                      pAudPara->stream_channel,
                                                                                      pAudPara->memory,
                                                                                      pAudPara->audio_interface,
                                                                                      pAudPara->rate);

    //modify for ab1568
    //for hal_audio_set_memory

    mem_handle->buffer_length = pAudPara->buffer_size;
    mem_handle->memory_select = hal_memory_convert_ul(pAudPara->memory);//modify for ab1568
    if (!(mem_handle->memory_select&(HAL_AUDIO_MEMORY_UL_SLAVE_TDM|HAL_AUDIO_MEMORY_UL_SLAVE_DMA))) {
        if (pAudPara->channel_num >= 3) {
            mem_handle->memory_select |= HAL_AUDIO_MEMORY_UL_VUL2;
        } else if (pAudPara->channel_num >= 5) {
            mem_handle->memory_select |= HAL_AUDIO_MEMORY_UL_VUL2|HAL_AUDIO_MEMORY_UL_VUL3;
        }
    }
    mem_handle->irq_counter = pAudPara->count;
    mem_handle->pcm_format = pAudPara->format;

#ifdef MTK_MULTI_MIC_STREAM_ENABLE
    if ((source->type >= SOURCE_TYPE_SUBAUDIO_MIN) && (source->type <= SOURCE_TYPE_SUBAUDIO_MAX)) {
        hal_audio_set_value_parameter_t handle;
        handle.register_irq_handler.audio_irq = HAL_AUDIO_IRQ_AUDIOSYS;
        handle.register_irq_handler.memory_select = HAL_AUDIO_MEMORY_UL_MASK;
        handle.register_irq_handler.entry = afe_subsource_interrupt_handler;
        DSP_MW_LOG_I("DSP AFE Sub-Source memory_select 0x%x, entry 0x%x\r\n", 2, handle.register_irq_handler.memory_select, handle.register_irq_handler.entry);
        hal_audio_set_value((hal_audio_set_value_parameter_t *)&handle, HAL_AUDIO_SET_IRQ_HANDLER);
    } else
#endif
    {
        if (mem_handle->memory_select&HAL_AUDIO_MEMORY_UL_VUL1) {

            hal_audio_set_value_parameter_t handle;
            handle.register_irq_handler.audio_irq = HAL_AUDIO_IRQ_AUDIOSYS;
            handle.register_irq_handler.memory_select = HAL_AUDIO_MEMORY_UL_VUL1;
            handle.register_irq_handler.entry = afe_vul1_interrupt_handler;
            DSP_MW_LOG_I("memory_select %d,audio_irq %d,entry %x\r\n",3,handle.register_irq_handler.memory_select,handle.register_irq_handler.audio_irq,handle.register_irq_handler.entry);
            hal_audio_set_value(&handle, HAL_AUDIO_SET_IRQ_HANDLER);

        } else if (mem_handle->memory_select == HAL_AUDIO_MEMORY_UL_VUL2) {

        } else if (mem_handle->memory_select == HAL_AUDIO_MEMORY_UL_SLAVE_DMA) {
            hal_audio_set_value_parameter_t handle;
            handle.register_irq_handler.audio_irq = HAL_AUDIO_IRQ_I2S_SLAVE;
            handle.register_irq_handler.entry = afe_vul1_interrupt_handler;//need modify
            DSP_MW_LOG_I("UL Slave DMA audio_irq %d,entry %x\r\n",2,handle.register_irq_handler.audio_irq,handle.register_irq_handler.entry);

            hal_audio_set_value(&handle, HAL_AUDIO_SET_IRQ_HANDLER);
        } else if (mem_handle->memory_select == HAL_AUDIO_MEMORY_UL_SLAVE_TDM) {
            hal_audio_set_value_parameter_t handle;
            handle.register_irq_handler.audio_irq = HAL_AUDIO_IRQ_I2S_SLAVE;
            handle.register_irq_handler.entry = afe_vul1_interrupt_handler;//need modify
            DSP_MW_LOG_I("UL Slave TDM audio_irq %d,entry %x\r\n",2,handle.register_irq_handler.audio_irq,handle.register_irq_handler.entry);
            hal_audio_set_value(&handle, HAL_AUDIO_SET_IRQ_HANDLER);
        }
    }
    mem_handle->with_mono_channel = (pAudPara->channel_num == 1) ? true : false;

    //path
    path_handle->audio_path_input_rate = pAudPara->rate;//afe_get_audio_device_samplerate(pAudPara->audio_device, pAudPara->audio_interface);
    path_handle->audio_path_output_rate = pAudPara->rate;//afe_get_audio_device_samplerate(pAudPara->audio_device, pAudPara->audio_interface);
    path_handle->connection_selection = pAudPara->channel_num;//pAudPara->stream_channel;

    //handle.input.parameters.audio_interface;

    //path_handle->input.parameters.audio_interface = pAudPara->memory;//modify for ab1568
    //path_handle->input.port = pAudPara->audio_device;//modify for ab1568
    //path_handle->input.parameters.audio_interface = pAudPara->audio_interface;//modify for ab1568
    //path_handle->output.port = HAL_AUDIO_CONTROL_MEMORY_INTERFACE;//modify for ab1568
    //path_handle->output.parameters.memory_select = HAL_AUDIO_MEMORY_UL_VUL1;//modify for ab1568
    path_handle->connection_number = pAudPara->channel_num;

    uint32_t i;
    hal_audio_path_port_parameter_t input_port_parameters, output_port_parameters;
    input_port_parameters.device_interface = pAudPara->audio_interface;
    output_port_parameters.memory_select = mem_handle->memory_select&(~HAL_AUDIO_MEMORY_UL_AWB2);
    #ifdef ENABLE_2A2D_TEST
    hal_audio_device_t path_audio_device[HAL_AUDIO_PATH_SUPPORT_SEQUENCE] = {pAudPara->audio_device,pAudPara->audio_device1,pAudPara->audio_device2,pAudPara->audio_device3};
    hal_audio_device_interface_t device_interface[HAL_AUDIO_PATH_SUPPORT_SEQUENCE]= {pAudPara->audio_interface,pAudPara->audio_interface1,pAudPara->audio_interface2,pAudPara->audio_interface3};
    hal_audio_memory_selection_t memory_select[HAL_AUDIO_PATH_SUPPORT_SEQUENCE]= {HAL_AUDIO_MEMORY_UL_VUL1, HAL_AUDIO_MEMORY_UL_VUL1 ,HAL_AUDIO_MEMORY_UL_VUL2, HAL_AUDIO_MEMORY_UL_VUL2};
    #else
    hal_audio_device_t path_audio_device[HAL_AUDIO_PATH_SUPPORT_SEQUENCE] = {pAudPara->audio_device,pAudPara->audio_device};
    hal_audio_device_interface_t device_interface[HAL_AUDIO_PATH_SUPPORT_SEQUENCE]= {pAudPara->audio_interface,pAudPara->audio_interface};
    hal_audio_memory_selection_t memory_select[HAL_AUDIO_PATH_SUPPORT_SEQUENCE]= {output_port_parameters.memory_select, output_port_parameters.memory_select};
    #endif
    for (i=0 ; i<path_handle->connection_number ; i++) {
        input_port_parameters.device_interface = device_interface[i];
        output_port_parameters.memory_select = memory_select[i];
        path_handle->input.interconn_sequence[i]  = stream_audio_convert_control_to_interconn(path_audio_device[i], input_port_parameters, i, true);
        path_handle->output.interconn_sequence[i] = stream_audio_convert_control_to_interconn(HAL_AUDIO_CONTROL_MEMORY_INTERFACE, output_port_parameters, i, true);
    }
    path_handle->with_hw_gain = pAudPara->hw_gain ;
    path_handle->with_upwdown_sampler = false;


#ifdef MTK_MULTI_MIC_STREAM_ENABLE
    //Update memory and path selection for Sub-source
    if ((source->type >= SOURCE_TYPE_SUBAUDIO_MIN) && (source->type <= SOURCE_TYPE_SUBAUDIO_MAX)) {
        bool memory_fined = false;
        mem_handle->memory_select = 0;

        //whether if use same memory interface
        SOURCE_TYPE     search_source_type;
        hal_audio_memory_selection_t memory_assign;
        uint32_t        interconn_sequence;

        for (i=0; i<path_handle->connection_number ; i+=2) {
            memory_fined = false;
            if (path_handle->input.interconn_sequence[i] == (uint8_t)(HAL_AUDIO_INTERCONN_SEQUENCE_DUMMY&0xFF)){
                if (i == 0) {
                     path_handle->connection_number = 0;
                }
                DSP_MW_LOG_I("DSP audio path_handle->input.interconn_sequence[%d]==NULL \n", 1, i);
                break;
            }

            for (search_source_type = SOURCE_TYPE_AUDIO ; search_source_type<=SOURCE_TYPE_SUBAUDIO_MAX ; search_source_type++) {
                if ((!Source_blks[search_source_type]) || (source->type == search_source_type)) {
                    continue;
                }
                for (interconn_sequence=0 ; interconn_sequence<HAL_AUDIO_PATH_SUPPORT_SEQUENCE ; interconn_sequence+=2) {
                    if ((path_handle->input.interconn_sequence[i] == Source_blks[search_source_type]->param.audio.path_handle.input.interconn_sequence[interconn_sequence]) &&
                        (path_handle->input.interconn_sequence[i+1] == Source_blks[search_source_type]->param.audio.path_handle.input.interconn_sequence[interconn_sequence+1])) {
                        path_handle->output.interconn_sequence[i] = Source_blks[search_source_type]->param.audio.path_handle.output.interconn_sequence[interconn_sequence];
                        path_handle->output.interconn_sequence[i+1] = Source_blks[search_source_type]->param.audio.path_handle.output.interconn_sequence[interconn_sequence+1];
                        mem_handle->memory_select |= stream_audio_convert_interconn_to_memory(source->param.audio.path_handle.output.interconn_sequence[i]);
                        pAudPara->buffer_size = Source_blks[search_source_type]->param.audio.buffer_size;
                        DSP_MW_LOG_I("audio source buffer find exist memory agent 0x%x  %d\n", 2, mem_handle->memory_select, source->param.audio.path_handle.output.interconn_sequence[i]);
                        search_source_type=SOURCE_TYPE_SUBAUDIO_MAX;
                        memory_fined = true;
                        break;
                    }
                }
            }
            if (!memory_fined) {
                if (!(mem_handle->memory_select&HAL_AUDIO_MEMORY_UL_VUL3)) {
                    memory_assign = HAL_AUDIO_MEMORY_UL_VUL3;
                } else {
                    memory_assign = HAL_AUDIO_MEMORY_UL_AWB;
                }
                mem_handle->memory_select |= memory_assign;
                path_handle->output.interconn_sequence[i] = stream_audio_convert_control_to_interconn(HAL_AUDIO_CONTROL_MEMORY_INTERFACE, (hal_audio_path_port_parameter_t)memory_assign, i, true);
                path_handle->output.interconn_sequence[i+1] = stream_audio_convert_control_to_interconn(HAL_AUDIO_CONTROL_MEMORY_INTERFACE, (hal_audio_path_port_parameter_t)memory_assign, i+1, true);
            }
        }
        DSP_MW_LOG_I("DSP audio sub-source:%d, memory_agent:0x%x \n", 2, source->type,mem_handle->memory_select);
    }
#endif

    if(pAudPara->echo_reference){
        mem_handle->memory_select = mem_handle->memory_select | HAL_AUDIO_MEMORY_UL_AWB2;
    }

    if (mem_handle->memory_select == HAL_AUDIO_MEMORY_UL_AWB2) {
        //Echo path Only
        DSP_MW_LOG_I("DSP audio source echo paht Only \n", 0);
    }

    //for hal_audio_set_device
    if ((pAudPara->audio_device)&(HAL_AUDIO_CONTROL_DEVICE_ANALOG_MIC_DUAL|HAL_AUDIO_CONTROL_DEVICE_LINE_IN_DUAL|HAL_AUDIO_CONTROL_DEVICE_DIGITAL_MIC_DUAL|HAL_AUDIO_CONTROL_DEVICE_ANC|HAL_AUDIO_CONTROL_DEVICE_I2S_MASTER|HAL_AUDIO_CONTROL_DEVICE_I2S_SLAVE)) {
        device_handle->common.rate = pAudPara->rate;
        device_handle->common.device_interface = pAudPara->audio_interface;
#ifdef ENABLE_2A2D_TEST
        device_handle1->common.rate = pAudPara->rate;
        device_handle1->common.device_interface = pAudPara->audio_interface1;
        device_handle2->common.rate = pAudPara->rate;
        device_handle2->common.device_interface = pAudPara->audio_interface2;
        device_handle3->common.rate = pAudPara->rate;
        device_handle3->common.device_interface = pAudPara->audio_interface3;
#endif
        DSP_MW_LOG_I("set device common.rate %d,source rate %d",2,device_handle->common.rate,pAudPara->rate);
    }

    if (pAudPara->audio_device == HAL_AUDIO_CONTROL_DEVICE_I2S_SLAVE) {
        device_handle->i2s_slave.memory_select = mem_handle->memory_select;
    }

    device_handle->common.audio_device = pAudPara->audio_device;
#ifdef ENABLE_2A2D_TEST
    device_handle1->common.audio_device = pAudPara->audio_device1;
    device_handle2->common.audio_device = pAudPara->audio_device2;
    device_handle3->common.audio_device = pAudPara->audio_device3;
#endif
    Source_device_set_para(device_handle);
#ifdef ENABLE_2A2D_TEST
    Source_device_set_para(device_handle1);
    Source_device_set_para(device_handle2);
    Source_device_set_para(device_handle3);
#endif


}

void Source_device_set_para(hal_audio_device_parameter_t *device_handle){

    if ((device_handle->common.audio_device)&HAL_AUDIO_CONTROL_DEVICE_ANALOG_MIC_DUAL) {
        device_handle->analog_mic.rate = device_handle->common.rate;//AUDIO_SOURCE_DEFAULT_ANALOG_VOICE_RATE;
        device_handle->analog_mic.mic_interface = device_handle->common.device_interface;//HAL_AUDIO_CONTROL_DEVICE_INTERFACE_1;
        device_handle->analog_mic.bias_voltage = HAL_AUDIO_BIAS_VOLTAGE_1_85V;
        device_handle->analog_mic.bias_select = HAL_AUDIO_BIAS_SELECT_ALL;
        device_handle->analog_mic.adc_parameter.adc_mode = gAudioCtrl.Afe.AfeULSetting.adc_mode;
        device_handle->analog_mic.iir_filter = HAL_AUDIO_UL_IIR_50HZ_AT_48KHZ;//HAL_AUDIO_UL_IIR_5HZ_AT_48KHZ;
        device_handle->analog_mic.with_external_bias = false;
        device_handle->analog_mic.bias1_2_with_LDO0 = false;
        device_handle->analog_mic.with_bias_lowpower = false;
        device_handle->analog_mic.adc_parameter.performance = AFE_PEROFRMANCE_NORMAL_MODE;
    } else if ((device_handle->common.audio_device)&HAL_AUDIO_CONTROL_DEVICE_LINE_IN_DUAL) {
        device_handle->linein.rate =  device_handle->common.rate;//AUDIO_SOURCE_DEFAULT_ANALOG_AUDIO_RATE;
        device_handle->linein.bias_voltage = HAL_AUDIO_BIAS_VOLTAGE_2_40V;
        device_handle->linein.bias_select = HAL_AUDIO_BIAS_SELECT_ALL;
        device_handle->linein.iir_filter = HAL_AUDIO_UL_IIR_DISABLE;
        device_handle->linein.adc_parameter.adc_mode = HAL_AUDIO_ANALOG_INPUT_ACC10K;
        device_handle->linein.adc_parameter.performance = AFE_PEROFRMANCE_NORMAL_MODE;
    } else if ((device_handle->common.audio_device)&HAL_AUDIO_CONTROL_DEVICE_DIGITAL_MIC_DUAL) {
        device_handle->digital_mic.rate = device_handle->common.rate;//AUDIO_SOURCE_DEFAULT_ANALOG_VOICE_RATE;
        device_handle->digital_mic.mic_interface = device_handle->common.device_interface;//HAL_AUDIO_CONTROL_DEVICE_INTERFACE_1;
        device_handle->digital_mic.dmic_selection = HAL_AUDIO_DMIC_GPIO_DMIC0;
        device_handle->digital_mic.bias_voltage = HAL_AUDIO_BIAS_VOLTAGE_1_85V;
        device_handle->digital_mic.bias_select = HAL_AUDIO_BIAS_SELECT_ALL;
        device_handle->digital_mic.iir_filter = HAL_AUDIO_UL_IIR_DISABLE;
        device_handle->digital_mic.with_external_bias = false;
        device_handle->digital_mic.with_bias_lowpower = false;
        device_handle->digital_mic.bias1_2_with_LDO0 = false;
    } else if ((device_handle->common.audio_device)&HAL_AUDIO_CONTROL_DEVICE_VAD) {
        device_handle->vad.rate = AUDIO_SOURCE_DEFAULT_ANALOG_VOICE_RATE;
    } else if ((device_handle->common.audio_device)&HAL_AUDIO_CONTROL_DEVICE_I2S_MASTER) {
        device_handle->i2s_master.rate = device_handle->common.rate;//48000;
        device_handle->i2s_master.i2s_interface = device_handle->common.device_interface;//HAL_AUDIO_CONTROL_DEVICE_INTERFACE_1;
        device_handle->i2s_master.i2s_format = HAL_AUDIO_I2S_I2S;
        device_handle->i2s_master.word_length = HAL_AUDIO_I2S_WORD_LENGTH_32BIT;
        device_handle->i2s_master.mclk_divider = 2;
        device_handle->i2s_master.with_mclk = false;
        device_handle->i2s_master.is_low_jitter = false;
        device_handle->i2s_master.is_recombinant = false;
    } else if ((device_handle->common.audio_device)&HAL_AUDIO_CONTROL_DEVICE_I2S_SLAVE) {
        device_handle->i2s_slave.rate = device_handle->common.rate;//48000;
        device_handle->i2s_slave.i2s_interface = device_handle->common.device_interface;//HAL_AUDIO_CONTROL_DEVICE_INTERFACE_1;
        device_handle->i2s_slave.i2s_format = HAL_AUDIO_I2S_I2S;
        device_handle->i2s_slave.word_length = HAL_AUDIO_I2S_WORD_LENGTH_32BIT;
        if ((device_handle->i2s_slave.memory_select == HAL_AUDIO_MEMORY_UL_SLAVE_DMA) || (device_handle->i2s_slave.memory_select == HAL_AUDIO_MEMORY_UL_SLAVE_TDM)) {
            device_handle->i2s_slave.is_vdma_mode = true;
        } else {
            device_handle->i2s_slave.is_vdma_mode = false;
        }
    }
}

VOID Sink_Audio_HW_Init_AFE(SINK sink)
{
    // do .hw_params()
    // according to sink to init the choosen AFE IO block
    // 1) hw_type
    // 2) channel
    // 3) mem allocate

    // TODO: AFE Clock init here <----

    if (audio_ops_probe(sink)) {
        DSP_MW_LOG_I("audio sink type : %d probe error\r\n", 1, sink->type);
    }
    if (audio_ops_hw_params(sink)) {
        DSP_MW_LOG_I("audio sink type : %d setting hw_params error\r\n", 1, sink->type);
    }

    switch (sink->param.audio.HW_type)
    {
        case  AUDIO_HARDWARE_PCM :
            //printf_james("Sink_Audio_HW_Init\r\n");
            break;
        case AUDIO_HARDWARE_I2S_M ://I2S master
            //#warning "To do I2S master interface later"
            break;
        case AUDIO_HARDWARE_I2S_S ://I2S slave
            //#warning "To do I2S slave interface later"
            break;
        default:
            configASSERT(0);
            break;
    }
}

VOID Sink_Audio_HW_Init(SINK sink)
{
    UNUSED(sink);
    // DSP_DRV_oDFE_CLK_INIT();
    // switch (sink->param.audio.HW_type)
    // {
        // case  AUDIO_HARDWARE_PCM :
            // AUDIO_DFE.OUT_SET0.field.ODFE_THRP_RATE_SEL = ODFE_RATE_ALIGN_DAC;
            // Audio_setting->Rate.Sink_Output_Sampling_Rate = FS_RATE_96K;
            // break;
        // case AUDIO_HARDWARE_I2S_M ://I2S master
            // AUDIO_DFE.OUT_SET0.field.ODFE_THRP_RATE_SEL = ODFE_RATE_ALIGN_I2S_MASTER;
            // AUDIO_DFE.OUT_SET0.field.I2S_MASTER_SDO_MUX = 0;
            // break;
        // case AUDIO_HARDWARE_I2S_S ://I2S slave
            // AUDIO_DFE.OUT_SET0.field.ODFE_THRP_RATE_SEL = ODFE_RATE_ALIGN_I2S_SLAVE;
            // AUDIO_DFE.OUT_SET0.field.I2S_SLAVE_SDO_MUX = 0;
            // break;
        // case AUDIO_HARDWARE_SPDIF :
            // AUDIO_DFE.OUT_SET0.field.ODFE_THRP_RATE_SEL = ODFE_RATE_ALIGN_SPDIF_TX;
            // AUDIO_DFE.OUT_SET0.field.SPDIF_TX_MUX = 0;
            // break;
    // }
}


VOID Source_Audio_HW_Init(SOURCE source)
{
    // TODO: AFE Clock init here <----
    if (audio_ops_probe(source))
        DSP_MW_LOG_I("audio source type : %d probe error\r\n", 1, source->type);

    if (audio_ops_hw_params(source))
        DSP_MW_LOG_I("audio source type : %d setting hw_params error\r\n", 1, source->type);

}


VOID AudioSourceHW_Ctrl (SOURCE source, BOOL IsEnabled)
{
    UNUSED(source);
    UNUSED(IsEnabled);
    /*U8 I2S_ModeSel,I2S_SampleRate;
    audio_hardware HW_type = (audio_hardware)source->param.audio.HW_type;

    I2S_ModeSel = (U8)I2S_RX_MODE;
    if (Sink_blks[SINK_TYPE_AUDIO] != NULL)
    {
        I2S_ModeSel = (Sink_blks[SINK_TYPE_AUDIO]->param.audio.HW_type == HW_type)? (U8)I2S_TRX_MODE : (U8)I2S_RX_MODE;
    }
    if (IsEnabled)
    {
        //Enable WADMA
        DSP_DRV_SOURCE_WADMA(source, IsEnabled);

        //Enable iDFE
        if(source->type == SOURCE_TYPE_AUDIO)
        {
            DSP_DRV_iDFE_DEC3_INIT( Audio_setting->Rate.Source_DownSampling_Ratio, source->param.audio.channel_num, Audio_setting->resolution.AudioInRes);
        }

        //Enable audio Interface
        switch (HW_type)
        {
            case AUDIO_HARDWARE_PCM:
                DSP_AD_IN_INIT();
                break;

            case AUDIO_HARDWARE_I2S_M:
                if (Audio_setting->Rate.Source_Input_Sampling_Rate > FS_RATE_48K)
                {
                    I2S_SampleRate = I2S_FS_RATE_96K;
                    Audio_setting->Rate.Source_Input_Sampling_Rate = FS_RATE_96K;
                }
                else
                {
                    I2S_SampleRate = I2S_FS_RATE_48K;
                    Audio_setting->Rate.Source_Input_Sampling_Rate = FS_RATE_48K;
                }
                DSP_DRV_I2S_MS_INIT(I2S_SampleRate ,I2S_WORD_LEN_32BIT,I2S_WORD_LEN_32BIT, I2S_WORD_LEN_32BIT ,I2S_ModeSel);
                break;

            case AUDIO_HARDWARE_I2S_S:
                DSP_DRV_I2S_SL_INIT(I2S_WORD_LEN_32BIT,I2S_WORD_LEN_32BIT,I2S_WORD_LEN_32BIT,I2S_ModeSel);
                break;
            case AUDIO_HARDWARE_SPDIF:
                DSP_DRV_SPDIF_RX_INIT();
                break;
            default:
                break;
        }
    }
    else
    {
        //Disable audio Interface
        switch (HW_type)
        {
            case AUDIO_HARDWARE_PCM:
                DSP_AD_IN_END();
                break;

            case AUDIO_HARDWARE_I2S_M :
                if (I2S.SET0.field.I2S_TR_MODE_CTL == I2S_RX_MODE)
                {
                    DSP_DRV_I2S_MS_END();
                }
                else
                {
                    I2S.SET0.field.I2S_TR_MODE_CTL = I2S_TX_MODE;
                }
                break;

            case AUDIO_HARDWARE_I2S_S :
                if (I2S.SET1.field.I2S_TR_MODE_CTL == I2S_RX_MODE)
                {
                    DSP_DRV_I2S_SL_END();
                }
                else
                {
                    I2S.SET1.field.I2S_TR_MODE_CTL = I2S_TX_MODE;
                }
                break;
            case AUDIO_HARDWARE_SPDIF:
                DSP_DRV_SPDIF_RX_END();
                break;
            default:
                break;
        }

        //Disable iDFE
        if(source->type == SOURCE_TYPE_AUDIO)
        {
            DSP_DRV_iDFE_DEC3_END();
        }

        //Disable WADMA
        DSP_DRV_SOURCE_WADMA(source, IsEnabled);
    }*/
}


VOID AudioSinkHW_Ctrl (SINK sink, BOOL IsEnabled)
{
    UNUSED(sink);
    UNUSED(IsEnabled);
    /*audio_hardware HW_type = sink->param.audio.HW_type;
    U8 I2S_ModeSel,samplingRate;

    I2S_ModeSel = (U8)I2S_TX_MODE;
    if (Source_blks[SOURCE_TYPE_AUDIO] != NULL)
    {
        I2S_ModeSel = (Source_blks[SOURCE_TYPE_AUDIO]->param.audio.HW_type == HW_type)? (U8)I2S_TRX_MODE : (U8)I2S_TX_MODE;
    }

    if (IsEnabled)
    {
        //Enable RADMA
        DSP_DRV_SINK_RADMA(sink, IsEnabled);

        //Enable SRC
        if (sink->type == SINK_TYPE_AUDIO && Audio_setting->Audio_sink.SRC_Out_Enable)
        {
            DSP_DRV_SRC_A_INIT (DSP_FsChange2SRCInRate(Audio_setting->Rate.SRC_Sampling_Rate),
                                DSP_FsChange2SRCOutRate(AudioSinkSamplingRate_Get()),
                                Audio_setting->resolution.SRCInRes,
                                Audio_setting->resolution.AudioOutRes);
        }

        //Enable oDFE
        if(sink->type == SINK_TYPE_AUDIO)
        {
            DSP_DRV_oDFE_INT4_INIT(Audio_setting->Rate.Sink_UpSampling_Ratio,
                                   sink->param.audio.channel_num,
                                   (Audio_setting->Audio_sink.SRC_Out_Enable)^1,
                                   Audio_setting->Audio_sink.CIC_Filter_Enable,
                                   Audio_setting->resolution.AudioOutRes);
        }
        else if (sink->type == SINK_TYPE_VP_AUDIO)
        {

            DSP_DRV_oDFE_INT6_INIT(Audio_setting->Rate.VP_UpSampling_Ratio,
                                   WITH_CIC,
                                   Audio_setting->resolution.AudioOutRes); //Shall able to adjust sampling rate with audio sink
        }

        //Enable audio Interface
        switch (HW_type)
        {
            case AUDIO_HARDWARE_PCM:
                if (AUDIO_CODEC.CTL0.field.EN_AU_DAC_DSM == 0)
                {
                    DSP_DA_OUT_INIT(sink->param.audio.channel_num);
                }
                break;

            case AUDIO_HARDWARE_I2S_M :
                if (Audio_setting->Rate.Sink_Output_Sampling_Rate > FS_RATE_48K)
                {
                    samplingRate = I2S_FS_RATE_96K;
                    Audio_setting->Rate.Sink_Output_Sampling_Rate = FS_RATE_96K;
                }
                else
                {
                    samplingRate = I2S_FS_RATE_48K;
                    Audio_setting->Rate.Sink_Output_Sampling_Rate = FS_RATE_48K;
                }
                DSP_DRV_I2S_MS_INIT(samplingRate ,I2S_WORD_LEN_32BIT,I2S_WORD_LEN_32BIT, I2S_WORD_LEN_32BIT ,I2S_ModeSel);
                break;

            case AUDIO_HARDWARE_I2S_S :
                DSP_DRV_I2S_SL_INIT(I2S_WORD_LEN_32BIT,I2S_WORD_LEN_32BIT,I2S_WORD_LEN_32BIT,I2S_ModeSel);
                break;
            case AUDIO_HARDWARE_SPDIF :
                samplingRate = DSP_ChangeFs2SpdifRate(Audio_setting->Rate.Sink_Output_Sampling_Rate);
                DSP_DRV_SPDIF_TX_INIT(samplingRate);
                break;
            default:
                break;
        }
    }
    else
    {
        //Disable audio Interface
        switch (HW_type)
        {
            case AUDIO_HARDWARE_PCM:
                DSP_DA_OUT_END();
                break;

            case AUDIO_HARDWARE_I2S_M :
                if (I2S.SET0.field.I2S_TR_MODE_CTL == I2S_TX_MODE)
                {
                    DSP_DRV_I2S_MS_END();
                }
                else
                {
                    I2S.SET0.field.I2S_TR_MODE_CTL = I2S_RX_MODE;
                }
                break;

            case AUDIO_HARDWARE_I2S_S :
                if (I2S.SET1.field.I2S_TR_MODE_CTL == I2S_TX_MODE)
                {
                    DSP_DRV_I2S_SL_END();
                }
                else
                {
                    I2S.SET1.field.I2S_TR_MODE_CTL = I2S_RX_MODE;
                }
                break;
            case AUDIO_HARDWARE_SPDIF:
                DSP_DRV_SPDIF_TX_END();
                break;
            default:
                break;
        }

        //Disable oDFE
        if(sink->type == SINK_TYPE_AUDIO)
        {
            DSP_DRV_oDFE_INT4_END();
        }
        else if (sink->type == SINK_TYPE_VP_AUDIO)
        {
            DSP_DRV_oDFE_INT6_END();
        }


        //Disable SRC
        if (sink->type == SINK_TYPE_AUDIO && Audio_setting->Audio_sink.SRC_Out_Enable)
        {
            DSP_DRV_SRC_A_END();
        }

        //Disable RADMA
        DSP_DRV_SINK_RADMA(sink, IsEnabled);
    }

    if (sink->type == SINK_TYPE_AUDIO)
        Audio_setting->Audio_sink.Output_Enable = IsEnabled;
    else if (sink->type == SINK_TYPE_VP_AUDIO)
        Audio_setting->Audio_VP.Output_Enable = IsEnabled;

    #if 0
    if ((Audio_setting->Audio_sink.Output_Enable==FALSE) &&
        (Audio_setting->Audio_VP.Output_Enable==FALSE))
        DSP_DRV_DisableAudioDfeClock();
    #endif*/
}

