/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "sink.h"
#include "source.h"
#include "common.h"
#include "dsp_buffer.h"
#include "stream_audio_driver.h"
#include "hal_audio_afe_control.h"
#include "hal_audio_afe_define.h"
#include "audio_afe_common.h"
#include "clk_skew.h"
#include "stream_audio_setting.h"
#include "stream_n9sco.h"
#include "dsp_callback.h"
#include "FreeRTOS.h"
#include "dsp_drv_afe.h"
#include "dsp_audio_msg.h"
#ifdef MTK_ANC_ENABLE
#include "anc_api.h"
#endif
#include "sfr_bt.h"


extern afe_sram_manager_t audio_sram_manager;

#define HW_SYSRAM_PRIVATE_MEMORY_CCNI_START_ADDR *(U8*)0x8423FC00

const afe_stream_channel_t connect_type[2][2] = { // [Stream][AFE]
    {STREAM_M_AFE_M, STREAM_M_AFE_S} ,
    {STREAM_S_AFE_M, STREAM_S_AFE_S}
};

#define WriteREG(_addr, _value) (*(volatile uint32_t *)(_addr) = (_value))
#define ReadREG(_addr)          (*(volatile uint32_t *)(_addr))
static volatile uint32_t AFE_CLASSG_MON0_b29 = 0;

#define ASRC_CLCOK_SKEW_DEBUG (false)

extern bool hal_src_set_start(afe_asrc_id_t src_id, bool enable);

BOOL chk_dl1_cur(uint32_t cur_addr)
{
#if 0
    if((AFE_GET_REG(AFE_AUDIO_BT_SYNC_CON0) & afe_get_bt_sync_enable_bit(AUDIO_DIGITAL_BLOCK_MEM_DL1)) //use sync mode
       && (afe_get_bt_sync_monitor_state(AUDIO_DIGITAL_BLOCK_MEM_DL1) == 0) //play en not start yet
       && ((cur_addr < AFE_GET_REG(AFE_DL1_BASE)) || (cur_addr > AFE_GET_REG(AFE_DL1_END)))) //cur addr isn't correct
#else
    //Keep flase before play en start
    if((AFE_GET_REG(AFE_AUDIO_BT_SYNC_CON0) & afe_get_bt_sync_enable_bit(AUDIO_DIGITAL_BLOCK_MEM_DL1)) //use sync mode
       && (afe_get_bt_sync_monitor_state(AUDIO_DIGITAL_BLOCK_MEM_DL1) == 0)) //play en not start yet
#endif
    {
        DSP_MW_LOG_W("chk_dl1_cur addr=0x%08x, base=0x%08x, end=0x%08x\n", 3,cur_addr,AFE_GET_REG(AFE_DL1_BASE),AFE_GET_REG(AFE_DL1_END));
        return false;
    }

    return true;
}

void vRegSetBit(uint32_t addr, uint32_t bit)
{
    uint32_t u4CurrValue, u4Mask;
    u4Mask = 1 << bit;
    u4CurrValue = ReadREG(addr);
    WriteREG(addr, (u4CurrValue | u4Mask));
    return;
}

void vRegResetBit(uint32_t addr, uint32_t bit)
{
    uint32_t u4CurrValue, u4Mask;
    u4Mask = 1 << bit;
    u4CurrValue = ReadREG(addr);
    WriteREG(addr, (u4CurrValue & (~u4Mask)));
    return;
}
#if 0
static void audio_split_source_to_interleaved(SOURCE source, uint8_t *afe_src, uint32_t pos, uint32_t size)
{
    BUFFER_INFO *buffer_info = &source->streamBuffer.BufferInfo;
    uint8_t *dst_ch0 = NULL, *dst_ch1 = NULL;
    afe_stream_channel_t channel_type = source->param.audio.connect_channel_type;
    uint32_t i, copysize, offset;

    if (size == 0) {
        return ;
    }
    if (channel_type == STREAM_S_AFE_S) {
        pos = pos>>1;
    }
    offset = (buffer_info->WriteOffset + pos) % (buffer_info->length);
    dst_ch0 = (uint8_t *)(buffer_info->startaddr[0] + offset);

    if (channel_type == STREAM_S_AFE_S) {
        copysize = size >> 1;
        dst_ch1 = (uint8_t *)(buffer_info->startaddr[1] + offset);
        for (i=0; i< copysize>>1; i++) {
            *(dst_ch0++) = *(afe_src++);
            *(dst_ch0++) = *(afe_src++);
            *(dst_ch1++) = *(afe_src++);
            *(dst_ch1++) = *(afe_src++);
            if (dst_ch0 == (uint8_t *)(buffer_info->startaddr[0] + buffer_info->length)){
                dst_ch0 = (uint8_t *)(buffer_info->startaddr[0]);
                dst_ch1 = (uint8_t *)(buffer_info->startaddr[1]);
            }
        }
    }else {
        DSP_D2C_BufferCopy(dst_ch0, afe_src, size, buffer_info->startaddr[0], buffer_info->length);
        if (channel_type == STREAM_S_AFE_M) {    // even if the stream is stereo, the SRAM updates 1 ch
            dst_ch1 = (uint8_t *)(buffer_info->startaddr[1] + offset);
            DSP_D2C_BufferCopy(dst_ch1, afe_src, size, buffer_info->startaddr[1], buffer_info->length);
        }
    }
}

static void audio_split_echo_to_interleaved(SOURCE source, uint8_t *afe_src, uint32_t pos, uint32_t size)
{
    BUFFER_INFO *buffer_info = &source->streamBuffer.BufferInfo;
    uint8_t *dst_ch2 = NULL;
    afe_stream_channel_t channel_type = source->param.audio.connect_channel_type;
    uint32_t i, copysize, offset;

    if (size == 0) {
        return ;
    }
    if (channel_type == STREAM_S_AFE_S) {
        pos = pos>>1;
    }
    offset = (buffer_info->WriteOffset + pos) % (buffer_info->length);
    dst_ch2 = (uint8_t *)(buffer_info->startaddr[2] + offset);

    if (channel_type == STREAM_S_AFE_S) {
        copysize = size >> 1;
        for (i=0; i< copysize>>1; i++) {
            *(dst_ch2++) = *(afe_src++);
            *(dst_ch2++) = *(afe_src++);
            afe_src++;
            afe_src++;
            if (dst_ch2 == (uint8_t *)(buffer_info->startaddr[2] + buffer_info->length)){
                dst_ch2 = (uint8_t *)(buffer_info->startaddr[2]);
            }
        }
    } else {
        DSP_D2C_BufferCopy(dst_ch2, afe_src, size, buffer_info->startaddr[2], buffer_info->length);
    }
}

static void audio_split_sink_to_interleaved(SINK sink, uint8_t *afe_dst, uint32_t pos, uint32_t size)
{
    BUFFER_INFO *buffer_info = &sink->streamBuffer.BufferInfo;
    uint8_t *src_ch0 = NULL, *src_ch1 = NULL, *p_dst;
    afe_stream_channel_t channel_type = sink->param.audio.connect_channel_type;
    uint32_t i, offset, copysize = size;
    p_dst = afe_dst;

    if (size == 0) {
        return ;
    }
    if (channel_type == STREAM_S_AFE_S) {
        pos = pos>>1;
    }
    offset = (buffer_info->ReadOffset + pos) % (buffer_info->length);
    src_ch0 = (uint8_t *)(buffer_info->startaddr[0] + offset);

    if (channel_type == STREAM_S_AFE_S) {
        src_ch1 = (uint8_t *)(buffer_info->startaddr[1] + offset);
        copysize = copysize >> 1;  //2 channels
        for (i = 0; i < copysize >> 1; i++) {  //16-bit
            *(p_dst++) = *(src_ch0++);
            *(p_dst++) = *(src_ch0++);
            *(p_dst++) = *(src_ch1++);
            *(p_dst++) = *(src_ch1++);

            if (src_ch0 == (uint8_t *)(buffer_info->startaddr[0] + buffer_info->length)) { //wrap
                src_ch0 = (uint8_t *)(buffer_info->startaddr[0]);
                src_ch1 = (uint8_t *)(buffer_info->startaddr[1]);
            }
        }
    } else {// mono
        DSP_C2D_BufferCopy(p_dst, src_ch0, size, buffer_info->startaddr[0], buffer_info->length);
    }
}

static void audio_afe_source_deinterleaved(SOURCE source, uint32_t size)
{
    BUFFER_INFO *buffer_info = &source->streamBuffer.BufferInfo;
    uint8_t *src_afe, *dst_ch0, *dst_ch1, *dst_echo;
    uint32_t i, j, sample, src_offset, dst_offset;
    uint32_t rpt, wpt, toggle=0, channel;

    if (size == 0) {
        return ;
    }

    dst_offset = (buffer_info->WriteOffset) % (buffer_info->length);
    src_offset= (source->param.audio.AfeBlkControl.u4ReadIdx) % source->param.audio.AfeBlkControl.u4BufferSize;

    src_afe = (uint8_t *)source->param.audio.AfeBlkControl.phys_buffer_addr;
    dst_ch0 = buffer_info->startaddr[0];
    dst_ch1 = buffer_info->startaddr[1];
    dst_echo = buffer_info->startaddr[2];

    //format_bytes
    sample = size/source->param.audio.format_bytes;

    //Echo path
    if (source->param.audio.echo_reference == true) {
        sample = sample + (sample>>1);
        toggle = 0;
    } else {
        toggle = 1;
    }
    channel = source->param.audio.channel_num+1;

    for (i=0 ; i<sample ; i++) {
        for (j=0 ; j<source->param.audio.format_bytes ; j++ ) {
            wpt = (dst_offset + j) % buffer_info->length;
            rpt = (src_offset + j) % source->param.audio.AfeBlkControl.u4BufferSize;

            if ((toggle==0) && (source->param.audio.echo_reference == true) && (dst_echo!=NULL)) {
                *(dst_echo+wpt) = *(src_afe+rpt+source->param.audio.AfeBlkControl.u4BufferSize);
            } else if ((toggle==1) && (dst_ch0!=NULL)) {
                *(dst_ch0+wpt) = *(src_afe+rpt);
            } else if ((toggle==2)&& (dst_ch1!=NULL)) {
                *(dst_ch1+wpt) = *(src_afe+rpt);
            }
        }

        if (toggle != 0) {
            src_offset = ( src_offset + source->param.audio.format_bytes)%source->param.audio.AfeBlkControl.u4BufferSize;
        }
        toggle++;
        if (channel == toggle) {
            dst_offset = (dst_offset + source->param.audio.format_bytes) % buffer_info->length;
            toggle = (source->param.audio.echo_reference == true) ? 0 : 1;
        }
    }
}


static void audio_afe_sink_interleaved(SINK sink, uint32_t size)
{
    BUFFER_INFO *buffer_info = &sink->streamBuffer.BufferInfo;
    uint8_t *src_ch0, *src_ch1, *dst_afe;
    uint32_t i, j, sample, src_offset, dst_offset;
    uint32_t rpt, wpt, toggle=0;

    if (size == 0) {
        return ;
    }

    src_offset = (buffer_info->ReadOffset) % (buffer_info->length);
    dst_offset = (sink->param.audio.AfeBlkControl.u4WriteIdx) % sink->param.audio.AfeBlkControl.u4BufferSize;

    src_ch0 = buffer_info->startaddr[0];
    src_ch1 = buffer_info->startaddr[1];
    //modify for asrc
    /*dst_afe = (sink->param.audio.AfeBlkControl.u4asrcflag)
                ? (uint8_t *)(sink->param.audio.AfeBlkControl.phys_buffer_addr+sink->param.audio.buffer_size)
                : (uint8_t *)(sink->param.audio.AfeBlkControl.phys_buffer_addr);*/
#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
    dst_afe = (sink->param.audio.AfeBlkControl.u4asrcflag)
                ? (uint8_t *)(sink->param.audio.AfeBlkControl.phys_buffer_addr+sink->param.audio.AfeBlkControl.u4asrc_buffer_size)
                : (uint8_t *)(sink->param.audio.AfeBlkControl.phys_buffer_addr);
#else
    dst_afe = (sink->param.audio.AfeBlkControl.u4asrcflag)
            ? (uint8_t *)(sink->param.audio.AfeBlkControl.phys_buffer_addr+sink->param.audio.buffer_size)
            : (uint8_t *)(sink->param.audio.AfeBlkControl.phys_buffer_addr);
#endif //ENABLE_HWSRC_ON_MAIN_STREAM

    //format_bytes
    sample = size/sink->param.audio.format_bytes;

    for (i=0 ; i<sample ; i++) {
        for (j=0 ; j<sink->param.audio.format_bytes ; j++ ) {
            wpt = (dst_offset + j) % sink->param.audio.AfeBlkControl.u4BufferSize;
            rpt = (src_offset + j) % buffer_info->length;

            if ((sink->param.audio.channel_num>=2) && (toggle == 1) && (src_ch1 != NULL)) {
                *(dst_afe+wpt) = *(src_ch1+rpt);
            } else {
                *(dst_afe+wpt) = *(src_ch0+rpt);
            }
        }

        dst_offset = (dst_offset + sink->param.audio.format_bytes)%sink->param.audio.AfeBlkControl.u4BufferSize;
        toggle ^= 1;
        if ((sink->param.audio.channel_num<2) || (toggle==0)) {
            src_offset = (src_offset + sink->param.audio.format_bytes) % buffer_info->length;
        }

    }
}
#endif

/**/
/////////////////////////////////////////////////////////////////////////////////////////////
bool audio_ops_distinguish_audio_sink(void *param)
{
    bool is_au_sink = FALSE;
    if ((param == Sink_blks[SINK_TYPE_VP_AUDIO]) || (param == Sink_blks[SINK_TYPE_AUDIO])) {
        is_au_sink = TRUE;
    }
    return is_au_sink;
}
bool audio_ops_distinguish_audio_source(void *param)
{
    SOURCE source_ptr = (SOURCE)param;
    bool is_au_source = FALSE;
    if ((source_ptr) &&
        ((source_ptr->type == SOURCE_TYPE_AUDIO)||
#ifdef MTK_MULTI_MIC_STREAM_ENABLE
         ((source_ptr->type>=SOURCE_TYPE_SUBAUDIO_MIN) && (source_ptr->type<=SOURCE_TYPE_SUBAUDIO_MAX)))){
#else
         (0))){
#endif
        is_au_source = TRUE;
    }
    return is_au_source;
}


int32_t audio_ops_probe(void *param)
{
    int ret = -1;
    if (param == NULL) {
        DSP_MW_LOG_E("DSP audio ops parametser invalid\r\n", 0);
        return ret;
    }
    if (audio_ops_distinguish_audio_sink(param)) {
        SINK sink = param;
        if (sink->param.audio.ops->probe != NULL) {
            sink->param.audio.ops->probe(param);
            ret = 0;
        }
    } else if (audio_ops_distinguish_audio_source(param)){
        SOURCE source = param;
        if (source->param.audio.ops->probe != NULL){
            source->param.audio.ops->probe(param);
            ret = 0;
        }
    }
    return ret;
}

int32_t audio_ops_hw_params(void *param)
{
    int ret = -1;
    if (param == NULL) {
        DSP_MW_LOG_E("DSP audio ops parametser invalid\r\n", 0);
        return ret;
    }
    if (audio_ops_distinguish_audio_sink(param)) {
        SINK sink = param;
        if (sink->param.audio.ops->hw_params != NULL) {
            sink->param.audio.ops->hw_params(param);
            ret = 0;
        }
    } else if (audio_ops_distinguish_audio_source(param)){
        SOURCE source = param;
        if (source->param.audio.ops->hw_params != NULL) {
            source->param.audio.ops->hw_params(param);
            ret = 0;
        }
    }
    return ret;
}

int32_t audio_ops_open(void *param)
{
    int ret = -1;
    if (param == NULL) {
        DSP_MW_LOG_E("DSP audio ops parametser invalid\r\n", 0);
        return ret;
    }
    if (audio_ops_distinguish_audio_sink(param)) {
        SINK sink = param;
        if (sink->param.audio.ops->open != NULL) {
            sink->param.audio.ops->open(param);
            ret = 0;
        }
    } else if (audio_ops_distinguish_audio_source(param)){
        SOURCE source = param;
        if (source->param.audio.ops->open != NULL) {
            source->param.audio.ops->open(param);
            ret = 0;
        }
    }
    return ret;
}

bool audio_ops_close(void *param)
{
    int ret = false;
    if (param == NULL) {
        DSP_MW_LOG_E("DSP audio ops parametser invalid\r\n", 0);
        return ret;
    }
    if (audio_ops_distinguish_audio_sink(param)) {
        SINK sink = param;
        if (sink->param.audio.ops->close != NULL) {
            sink->param.audio.ops->close(param);
            ret = true;
        }
    } else if (audio_ops_distinguish_audio_source(param)){
        SOURCE source = param;
        if (source->param.audio.ops->close != NULL) {
            source->param.audio.ops->close(param);
            ret = true;
        }
    }
    return ret;
}

int32_t audio_ops_trigger(void *param, int cmd)
{
    int ret = -1;
    if (param == NULL) {
        DSP_MW_LOG_E("DSP audio ops parametser invalid\r\n", 0);
        return ret;
    }
        if (audio_ops_distinguish_audio_sink(param) == TRUE) {
        SINK sink = param;
        if (sink->param.audio.ops->trigger != NULL) {
            sink->param.audio.ops->trigger(param, cmd);
            ret = 0;
        }
    } else if (audio_ops_distinguish_audio_source(param) == TRUE) {
        SOURCE source = param;
        if (source->param.audio.ops->trigger != NULL) {
            source->param.audio.ops->trigger(param, cmd);
            ret = 0;
        }
    }
    return ret;
}

ATTR_TEXT_IN_IRAM_LEVEL_2 bool audio_ops_copy(void *param, void *src, uint32_t count)
{
    int ret = false;
    if (param == NULL) {
        DSP_MW_LOG_E("DSP audio ops parametser invalid\r\n", 0);
        return ret;
    }
    if (audio_ops_distinguish_audio_sink(param)) {
        SINK sink = param;
        if (sink->param.audio.ops->copy != NULL) {
            sink->param.audio.ops->copy(param, src, count);
            ret = true;
        }
    } else if (audio_ops_distinguish_audio_source(param)){
        SOURCE source = param;
        if (source->param.audio.ops->copy != NULL) {
            source->param.audio.ops->copy(param, src, count);
            ret = true;
        }
    }
    return ret;
}

extern audio_sink_pcm_ops_t afe_platform_dl1_ops;
#ifdef MTK_PROMPT_SOUND_ENABLE
extern audio_sink_pcm_ops_t afe_platform_dl2_ops;
#endif
extern audio_source_pcm_ops_t afe_platform_ul1_ops;
#if defined(MTK_I2S_SLAVE_ENABLE)
extern audio_sink_pcm_ops_t i2s_slave_dl_ops;
extern audio_source_pcm_ops_t i2s_slave_ul_ops;
#endif

void audio_afe_set_ops(void *param)
{
    if (param == NULL) {
        DSP_MW_LOG_E("DSP audio ops parametser invalid\r\n", 0);
        return;
    }
    if (audio_ops_distinguish_audio_sink(param)) {
        SINK sink = param;
#if defined(MTK_I2S_SLAVE_ENABLE)
        if (sink->param.audio.audio_device == HAL_AUDIO_DEVICE_I2S_SLAVE) {
            sink->param.audio.ops = &i2s_slave_dl_ops;
            return;
        }
#endif
        sink->param.audio.ops = (audio_pcm_ops_p)&afe_platform_dl1_ops;
    } else if (audio_ops_distinguish_audio_source(param)) {
        SOURCE source = param;
#if defined(MTK_I2S_SLAVE_ENABLE)
        if (source->param.audio.audio_device == HAL_AUDIO_DEVICE_I2S_SLAVE) {
            source->param.audio.ops = &i2s_slave_ul_ops;
            return;
        }
#endif
        source->param.audio.ops = (audio_pcm_ops_p)&afe_platform_ul1_ops;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
void afe_sink_prefill_silence_data(SINK sink)
{
    afe_block_t *afe_block = &sink->param.audio.AfeBlkControl;
    BUFFER_INFO *buffer_info = &sink->streamBuffer.BufferInfo;

    if (afe_block->u4BufferSize <= buffer_info->length) {
        afe_block->u4BufferSize =  buffer_info->length;
    }

    afe_block->u4DataRemained = ((buffer_info->WriteOffset >= buffer_info->ReadOffset)
                                    ? (buffer_info->WriteOffset - buffer_info->ReadOffset)
                                    :(buffer_info->length - buffer_info->ReadOffset + buffer_info->WriteOffset));

    if (sink->param.audio.channel_num == 2) {
        afe_block->u4DataRemained <<= 1;
    }

    afe_block->u4WriteIdx += afe_block->u4DataRemained;
    afe_block->u4WriteIdx %= afe_block->u4BufferSize;
}

void afe_source_prefill_silence_data(SOURCE source)
{
    afe_block_t *afe_block = &source->param.audio.AfeBlkControl;
    BUFFER_INFO *buffer_info = &source->streamBuffer.BufferInfo;

    if (afe_block->u4BufferSize <= buffer_info->length) {
        afe_block->u4BufferSize =  buffer_info->length;
    }

    if (source->param.audio.channel_num == 2) {
        afe_block->u4ReadIdx = ( buffer_info->ReadOffset << 1) % afe_block->u4BufferSize;
    }
    else {
        afe_block->u4ReadIdx = buffer_info->ReadOffset % afe_block->u4BufferSize;
    }
}

/*
 * Get dl1 afe and sink buffer
 * Units: sample
*/
ATTR_TEXT_IN_IRAM_LEVEL_2 uint32_t afe_get_dl1_query_data_amount(void)
{
    volatile SINK sink = Sink_blks[SINK_TYPE_AUDIO];
    uint32_t afe_sram_data_count, sink_data_count;

    if (sink == NULL) {
        return 0;
    }
    //AFE DL1 SRAM data amount
    #if 0
    afe_block_t *afe_block = &sink->param.audio.AfeBlkControl;
    int32_t hw_current_read_idx = AFE_GET_REG(AFE_DL1_CUR);
    afe_block->u4ReadIdx = hw_current_read_idx - AFE_GET_REG(AFE_DL1_BASE);
    if (afe_block->u4WriteIdx > afe_block->u4ReadIdx) {
        *afe_sram_data_count = afe_block->u4WriteIdx - afe_block->u4ReadIdx;
    } else {
        *afe_sram_data_count = afe_block->u4BufferSize + afe_block->u4WriteIdx - afe_block->u4ReadIdx;
    }
    #else
    afe_sram_data_count = 0;
    #endif
    //Sink audio data amount
    U32 buffer_per_channel_shift = ((sink->param.audio.channel_num>=2) && (sink->buftype == BUFFER_TYPE_INTERLEAVED_BUFFER ))
                                     ? 1
                                     : 0;
    sink_data_count = (sink->streamBuffer.BufferInfo.length>>buffer_per_channel_shift) - SinkSlack(sink);

    return ((afe_sram_data_count+sink_data_count)/sink->param.audio.format_bytes);
}

#if defined(MTK_I2S_SLAVE_ENABLE)
/***************************************************
               I2 slave TX / RX IRQ handler
***************************************************/
#include "hal_pdma_internal.h"
extern const vdma_channel_t g_i2s_slave_vdma_channel[];
extern uint32_t i2s_slave_port_translate(hal_audio_interface_t audio_interface);

void i2s_slave_ul_interrupt_handler(vdma_event_t event, void  *user_data)
{
    uint32_t mask;
    uint32_t port;
    vdma_channel_t rx_dma_channel;
    uint32_t hw_current_write_idx, hw_current_read_idx;
    afe_block_t *afe_block = &Source_blks[SOURCE_TYPE_AUDIO]->param.audio.AfeBlkControl;
    SOURCE source = Source_blks[SOURCE_TYPE_AUDIO];
    hal_audio_interface_t audio_interface = source->param.audio.audio_interface;
    BUFFER_INFO *buffer_info = &source->streamBuffer.BufferInfo;
    uint32_t update_frame_size = source->param.audio.frame_size * source->param.audio.channel_num;

    hal_nvic_save_and_set_interrupt_mask(&mask);

    port = i2s_slave_port_translate(audio_interface);
    rx_dma_channel = g_i2s_slave_vdma_channel[port * 2 + 1];

    // Get last WPTR and record current WPTR
    vdma_get_hw_write_point(rx_dma_channel, &hw_current_write_idx);
    if (afe_block->u4asrcflag) {
        #if 0
        hw_current_read_idx = AFE_GET_REG(ASM_CH01_IBUF_RDPNT) - AFE_GET_REG(ASM_IBUF_SADR);
        update_frame_size = (hw_current_read_idx + afe_block->u4BufferSize - afe_block->u4ReadIdx)%afe_block->u4BufferSize;
        afe_block->u4ReadIdx = hw_current_read_idx;
        #else
        afe_block->u4ReadIdx = AFE_GET_REG(ASM_CH01_IBUF_RDPNT) - AFE_GET_REG(ASM_IBUF_SADR);
        vdma_get_hw_read_point(rx_dma_channel, &hw_current_read_idx);
        update_frame_size = (hw_current_write_idx + afe_block->u4asrc_buffer_size - hw_current_read_idx - 8)%afe_block->u4asrc_buffer_size;
        #endif

        afe_block->u4WriteIdx = hw_current_write_idx - afe_block->phys_buffer_addr;
        AFE_SET_REG(ASM_CH01_IBUF_WRPNT, hw_current_write_idx<<ASM_CH01_IBUF_WRPNT_POS, ASM_CH01_IBUF_WRPNT_MASK);

        buffer_info->WriteOffset = AFE_GET_REG(ASM_CH01_OBUF_WRPNT) - AFE_GET_REG(ASM_OBUF_SADR);

        /*
        if(OFFSET_OVERFLOW_CHK(afe_block->u4ReadIdx, hw_current_read_idx-afe_block->phys_buffer_addr, afe_block->u4WriteIdx)) {
            log_hal_error("DSP I2D UL with SRC wrap. SRC_WR:0x%x, SRC_RD:0x%x, I2S_RD:0x%x", afe_block->u4WriteIdx, afe_block->u4ReadIdx, hw_current_read_idx-afe_block->phys_buffer_addr);
        }
        */

    } else {
        buffer_info->WriteOffset = hw_current_write_idx - afe_block->phys_buffer_addr;
    }

    vdma_disable_interrupt(rx_dma_channel);
    i2s_slave_ul_update_rptr(source, update_frame_size*4);
    vdma_enable_interrupt(rx_dma_channel);

    AudioCheckTransformHandle(source->transform);

    hal_nvic_restore_interrupt_mask(mask);
}

void i2s_slave_dl_interrupt_handler(vdma_event_t event, void  *user_data)
{
    int16_t cp_samples = 0;
    uint32_t hw_current_read_idx;
    uint32_t mask,pre_offset,isr_interval;
    volatile SINK sink = Sink_blks[SINK_TYPE_AUDIO];
    BUFFER_INFO *buffer_info = &sink->streamBuffer.BufferInfo;
    AUDIO_PARAMETER *runtime = &sink->param.audio;
    uint32_t dl_base_addr = buffer_info->startaddr[0];
    vdma_channel_t tx_dma_channel;
    uint32_t port;
    hal_audio_interface_t audio_interface = sink->param.audio.audio_interface;

    hal_nvic_save_and_set_interrupt_mask(&mask);

    port = i2s_slave_port_translate(audio_interface);
    tx_dma_channel = g_i2s_slave_vdma_channel[port * 2];

    /* clock skew adjustment */
    cp_samples = clk_skew_check_dl_status();
    vdma_set_threshold(tx_dma_channel, (runtime->count + cp_samples) * 2 * 32);

    /* Get last RPTR and record current RPTR */
    pre_offset = buffer_info->ReadOffset;
    vdma_get_hw_read_point(tx_dma_channel, &hw_current_read_idx);
    buffer_info->ReadOffset = hw_current_read_idx - dl_base_addr;
    isr_interval = (pre_offset <= buffer_info->ReadOffset)
                     ? (buffer_info->ReadOffset - pre_offset)
                     : (buffer_info->length + buffer_info->ReadOffset - pre_offset);

    if (buffer_info->ReadOffset != buffer_info->WriteOffset) {
        buffer_info->bBufferIsFull = FALSE;
    }

    /* Check whether underflow happen */
    if  (OFFSET_OVERFLOW_CHK(pre_offset,(buffer_info->ReadOffset)%buffer_info->length, buffer_info->WriteOffset )&&(buffer_info->bBufferIsFull == FALSE)) {
        DSP_MW_LOG_I("SRAM Empty play en:%d pR:%d R:%d W:%d",4,isr_interval, pre_offset, buffer_info->ReadOffset, buffer_info->WriteOffset);
        buffer_info->WriteOffset = (buffer_info->ReadOffset + 2*isr_interval)%buffer_info->length;
        if (buffer_info->WriteOffset > buffer_info->ReadOffset){
            memset((void *)(dl_base_addr + buffer_info->ReadOffset), 0, buffer_info->WriteOffset - buffer_info->ReadOffset);
        } else {
            memset((void *)(dl_base_addr+ buffer_info->ReadOffset), 0, buffer_info->length - buffer_info->ReadOffset);
            memset((void *)dl_base_addr, 0, buffer_info->WriteOffset);
        }
    } else if (buffer_info->ReadOffset != buffer_info->WriteOffset) {
        buffer_info->bBufferIsFull = FALSE;
    }

    AudioCheckTransformHandle(sink->transform);

    hal_nvic_restore_interrupt_mask(mask);
}
#endif


ATTR_TEXT_IN_IRAM void afe_dl1_interrupt_handler(void)
{
    uint32_t mask,pre_offset,isr_interval ;
    uint32_t hw_current_read_idx = 0;
    volatile SINK sink = Sink_blks[SINK_TYPE_AUDIO];
    volatile SOURCE source;


#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
    afe_block_t *afe_block = &sink->param.audio.AfeBlkControl;
#endif
    BUFFER_INFO *buffer_info = &sink->streamBuffer.BufferInfo;

    hw_current_read_idx = word_size_align(AFE_GET_REG(AFE_DL1_CUR));
    AUDIO_PARAMETER *runtime = &sink->param.audio;
    int16_t cp_samples = 0;
    uint32_t dl_base_addr = (uint32_t)buffer_info->startaddr[0];
    uint32_t pre_AFE_CLASSG_MON0_b29 = 0;
#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
    if (afe_block->u4asrcflag) {
        dl_base_addr = AFE_GET_REG(ASM_IBUF_SADR);
        hw_current_read_idx = AFE_GET_REG(ASM_CH01_IBUF_RDPNT);

    } else {
        dl_base_addr = AFE_GET_REG(AFE_DL1_BASE);
        hw_current_read_idx = AFE_GET_REG(AFE_DL1_CUR);
    }
#endif
    /*Mce play en check*/
    if (runtime->irq_exist == false) {
        runtime->irq_exist = true;
        U32 first_dl_irq_time;
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &first_dl_irq_time);
        DSP_MW_LOG_I("DSP afe dl1 interrupt exist, bt_clk 0x%x, gpt_time:%d", 2, rBb->rClkCtl.rNativeClock, first_dl_irq_time);
        source = sink->transform->source;
#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
        if (afe_block->u4asrcflag) {
            hal_src_set_start(AFE_SRC_1, true);
        }
#endif
        if ((sink->transform != NULL)&&(sink->transform->source->type == SOURCE_TYPE_N9SCO))
        {
            if (SourceSize(source) != 0)
            {
                DSP_MW_LOG_I("eSCO DL audio irq first Wo:%d Ro:%d", 2,source->streamBuffer.AVMBufferInfo.WriteIndex, source->streamBuffer.AVMBufferInfo.ReadIndex);
            }
            else
            {
                DSP_MW_LOG_I("eSCO First IRQ meet size 0 Wo:%d Ro:%d", 2,source->streamBuffer.AVMBufferInfo.WriteIndex, source->streamBuffer.AVMBufferInfo.ReadIndex);
            }
        }
    } else if (runtime->AfeBlkControl.u4awsflag == true) {
        uint32_t sync_reg_mon1 = afe_get_bt_sync_monitor(AUDIO_DIGITAL_BLOCK_MEM_DL1);
        uint32_t sync_reg_mon2 = afe_get_bt_sync_monitor_state(AUDIO_DIGITAL_BLOCK_MEM_DL1);
        if ((sync_reg_mon1 == 0) || (sync_reg_mon2 == 0)) {
            DSP_MW_LOG_I("DSP afe BT sync monitor by dl1 0x%x wait cnt: %d Wo: %d Ro: %d Bf: %d", 5,sync_reg_mon1,runtime->afe_wait_play_en_cnt,buffer_info->WriteOffset,buffer_info->ReadOffset,buffer_info->bBufferIsFull);
            if ((runtime->afe_wait_play_en_cnt != PLAY_EN_REINIT_DONE_MAGIC_NUM) && (runtime->afe_wait_play_en_cnt != PLAY_EN_TRIGGER_REINIT_MAGIC_NUM))
            {
                runtime->afe_wait_play_en_cnt++;
                if ((runtime->afe_wait_play_en_cnt * runtime->period) > PLAY_EN_DELAY_TOLERENCE)
                {
                   runtime->afe_wait_play_en_cnt = PLAY_EN_TRIGGER_REINIT_MAGIC_NUM;
                   buffer_info->bBufferIsFull = FALSE;
                }
            }
        }
    }

    #if 0//modify for ab1568
    if (afe_get_memory_path_enable(AUDIO_DIGITAL_BLOCK_MEM_DL1))
    #endif
    {
        hal_nvic_save_and_set_interrupt_mask(&mask);
        if ((hw_current_read_idx == 0) || (chk_dl1_cur(hw_current_read_idx) == false)) { //should chk setting if =0
#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
            hw_current_read_idx = word_size_align((S32) dl_base_addr);
#else
            hw_current_read_idx = dl_base_addr;
#endif
        }
        pre_offset = buffer_info->ReadOffset;

        /* Update Clk Skew: clk information */
        if(afe_get_bt_sync_monitor_state(AUDIO_DIGITAL_BLOCK_MEM_DL1)){
           Clock_Skew_Offset_Update();
        }

        /* For Downlink Clk Skew IRQ period control */
#ifdef ENABLE_HWSRC_CLKSKEW
        if(ClkSkewMode_g == CLK_SKEW_V1){
            cp_samples = clk_skew_check_dl_status();
        } else {
            cp_samples = 0;
        }
#else
        cp_samples = clk_skew_check_dl_status();
#endif
        #if 0//modify for ab1568
        afe_update_audio_irq_cnt(afe_irq_request_number(AUDIO_DIGITAL_BLOCK_MEM_DL1),
                                 runtime->count + cp_samples);
        #else
        hal_audio_memory_irq_period_parameter_t irq_period;
        irq_period.memory_select= HAL_AUDIO_MEMORY_DL_DL1;
        irq_period.rate = runtime->rate;
        irq_period.irq_counter = (uint32_t)((int32_t)runtime->count + (int32_t)cp_samples);
        hal_audio_control_set_value((hal_audio_set_value_parameter_t *)&irq_period, HAL_AUDIO_SET_MEMORY_IRQ_PERIOD);
        #endif


        buffer_info->ReadOffset = hw_current_read_idx - dl_base_addr;
        isr_interval = (pre_offset <= buffer_info->ReadOffset)
                         ? (buffer_info->ReadOffset - pre_offset)
                         : (buffer_info->length + buffer_info->ReadOffset - pre_offset);
        //printf("dl1 pre %d rpt %d wpt %d len %d\r\n",pre_offset,buffer_info->ReadOffset,buffer_info->WriteOffset,buffer_info->length);
        /*Clear up last time used memory */
        if (buffer_info->ReadOffset >= pre_offset) {
            memset((void *)(dl_base_addr+pre_offset), 0, buffer_info->ReadOffset - pre_offset);
        } else {
            memset((void *)(dl_base_addr+pre_offset), 0, buffer_info->length - pre_offset);
            memset((void *)dl_base_addr, 0, buffer_info->ReadOffset);
        }

        if (buffer_info->ReadOffset != buffer_info->WriteOffset) {
            buffer_info->bBufferIsFull = FALSE;
        }

        if ((sink->transform != NULL)&&(sink->transform->source->type == SOURCE_TYPE_N9SCO))
        {
            source = sink->transform->source;
            DSP_CALLBACK_PTR callback_ptr;

            callback_ptr = DSP_Callback_Get(source, sink);
            #if DL_TRIGGER_UL
            if (sink->transform->source->param.n9sco.dl_enable_ul == TRUE)
            {
                hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_32K, &(sink->transform->source->param.n9sco.ul_play_gpt));
                sink->transform->source->param.n9sco.dl_enable_ul = FALSE;
            }
            #endif
            if (callback_ptr->Status != CALLBACK_SUSPEND)
            {
                DSP_MW_LOG_I("Callback Busy : %d", 1,callback_ptr->Status);
                #if DL_TRIGGER_UL
                sink->transform->source->param.n9sco.dl_enable_ul = TRUE;
                SourceDrop(source,source->param.n9sco.process_data_length);
                sink->transform->source->param.n9sco.dl_enable_ul = FALSE;
                #else
                SourceDrop(source,source->param.n9sco.process_data_length);
                #endif
                buffer_info->WriteOffset = (buffer_info->WriteOffset + (sink->param.audio.channel_num * sink->param.audio.rate * sink->param.audio.period * sink->param.audio.format_bytes / 1000))%buffer_info->length;
            }

#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
//#if (AFE_REGISTER_ASRC_IRQ)
            if(ClkSkewMode_g == CLK_SKEW_V2) {
                if(afe_get_asrc_irq_is_enabled(AFE_MEM_ASRC_1, ASM_IER_IBUF_EMPTY_INTEN_MASK)== false){//modify for clock skew
                    afe_set_asrc_irq_enable(AFE_MEM_ASRC_1, true);
                    DSP_MW_LOG_I("asrc afe_dl1_interrupt_handler asrc_irq_is_enabled %d",1,afe_get_asrc_irq_is_enabled(AFE_MEM_ASRC_1, ASM_IER_IBUF_EMPTY_INTEN_MASK));
                }
            }
//#endif
#endif
            #ifndef MTK_BT_HFP_FORWARDER_ENABLE
            if (SourceSize(source) == 0)
            {
                DSP_MW_LOG_I("eSCO DL audio irq no data in Wo:%d Ro:%d", 2,source->streamBuffer.AVMBufferInfo.WriteIndex,source->streamBuffer.AVMBufferInfo.ReadIndex);
                SourceConfigure(source,SCO_SOURCE_WO_ADVANCE,2);// add 2 frame in advance for HFP MCE
            }
            #endif
        }

#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
        if ((OFFSET_OVERFLOW_CHK(pre_offset, buffer_info->ReadOffset, buffer_info->WriteOffset)&&(buffer_info->bBufferIsFull==FALSE))
            || (((buffer_info->WriteOffset+buffer_info->length-buffer_info->ReadOffset)%buffer_info->length < 32) && (afe_block->u4asrcflag)))
#else
        if  (OFFSET_OVERFLOW_CHK(pre_offset,(buffer_info->ReadOffset)%buffer_info->length, buffer_info->WriteOffset )&&(buffer_info->bBufferIsFull == FALSE))//Sram empty
#endif
        {

            bool empty = true;
#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
            if(afe_block->u4asrcflag) {
                uint32_t src_out_size, src_out_read, src_out_write, remain_data;
                src_out_write = AFE_READ(ASM_CH01_OBUF_WRPNT);
                src_out_read = AFE_READ(ASM_CH01_OBUF_RDPNT);
                src_out_size = AFE_READ(ASM_OBUF_SIZE);
                remain_data = (src_out_write > src_out_read) ? src_out_write-src_out_read : src_out_write+src_out_size- src_out_read;
                remain_data = (sink->param.audio.channel_num>=2) ? remain_data>>1 : remain_data;
                if (remain_data > (afe_block->u4asrcrate*sink->param.audio.period*sink->param.audio.format_bytes)/1000) {
                    empty = false;
                }
            }
#endif
            if(empty){
                DSP_MW_LOG_W("SRAM Empty play en:%d pR:%d R:%d W:%d", 4,isr_interval, pre_offset, buffer_info->ReadOffset, buffer_info->WriteOffset);
                U16 pre_write_offset = buffer_info->WriteOffset;
#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
                if (afe_block->u4asrcflag) {
                    buffer_info->WriteOffset = (buffer_info->ReadOffset + buffer_info->length/2) % buffer_info->length;
                } else {
                    if (pre_offset < buffer_info->ReadOffset) {
                        buffer_info->WriteOffset = (buffer_info->ReadOffset*2 - pre_offset)% buffer_info->length;
                    } else {
                        buffer_info->WriteOffset = (buffer_info->ReadOffset*2 + (buffer_info->length - pre_offset))% buffer_info->length;
                    }
                }

//#if (AFE_REGISTER_ASRC_IRQ)
#ifdef ENABLE_HWSRC_CLKSKEW
                if(ClkSkewMode_g== CLK_SKEW_V2) {
                    if(afe_get_asrc_irq_is_enabled(AFE_MEM_ASRC_1, ASM_IER_IBUF_EMPTY_INTEN_MASK)== false){//modify for clock skew
                        afe_set_asrc_irq_enable(AFE_MEM_ASRC_1, true);
                        DSP_MW_LOG_W("asrc afe_dl1_interrupt_handler asrc_irq_is_enabled %d",1,afe_get_asrc_irq_is_enabled(AFE_MEM_ASRC_1, ASM_IER_IBUF_EMPTY_INTEN_MASK));
                    }
                }
#endif
#endif /*ENABLE_HWSRC_ON_MAIN_STREAM*/

                if ((sink->transform != NULL)&&(sink->transform->source->type == SOURCE_TYPE_A2DP)&&(sink->transform->source->param.n9_a2dp.mce_flag))
                {
#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
                    DSP_MW_LOG_W("add empty_fill_size %d %d", 2, sink->param.audio.sram_empty_fill_size, ((pre_write_offset >= buffer_info->WriteOffset) ? (pre_write_offset - buffer_info->WriteOffset) : (buffer_info->length - buffer_info->WriteOffset + pre_write_offset)));
                    sink->param.audio.sram_empty_fill_size += (( buffer_info->WriteOffset >= pre_write_offset) ? (buffer_info->WriteOffset - pre_write_offset ) : (buffer_info->length - pre_write_offset + buffer_info->WriteOffset));
#else
                    DSP_MW_LOG_W("add empty_fill_size %d %d",2,sink->param.audio.sram_empty_fill_size,2*isr_interval + ((buffer_info->ReadOffset >= buffer_info->WriteOffset) ? (buffer_info->ReadOffset - buffer_info->WriteOffset) : (buffer_info->length - buffer_info->WriteOffset + buffer_info->ReadOffset)));
                    sink->param.audio.sram_empty_fill_size += 2*isr_interval + ((buffer_info->ReadOffset >= buffer_info->WriteOffset) ? (buffer_info->ReadOffset - buffer_info->WriteOffset) : (buffer_info->length - buffer_info->WriteOffset + buffer_info->ReadOffset));
#endif
                }

                buffer_info->WriteOffset = (buffer_info->ReadOffset + 2*isr_interval)%buffer_info->length;
                if (buffer_info->WriteOffset > buffer_info->ReadOffset){
                    memset((void *)(dl_base_addr + buffer_info->ReadOffset), 0, buffer_info->WriteOffset - buffer_info->ReadOffset);

                } else {
                    memset((void *)(dl_base_addr + buffer_info->ReadOffset), 0, buffer_info->length - buffer_info->ReadOffset);
                    memset((void *)dl_base_addr, 0, buffer_info->WriteOffset);

                }
            }
        } else if (buffer_info->ReadOffset != buffer_info->WriteOffset) {
            buffer_info->bBufferIsFull = FALSE;
        }


#if 0
        remain_sink_data = sink->streamBuffer.BufferInfo.length - SinkSlack(sink);
        if(remain_sink_data == 0) {
            //printf("[AUD]underflow\r\n");
            SinkBufferUpdateReadPtr(sink, 0);
            hal_nvic_restore_interrupt_mask(mask);
            return;
        }

        if (afe_block->u4ReadIdx > afe_block->u4WriteIdx) {
            sram_free_space = afe_block->u4ReadIdx - afe_block->u4WriteIdx;
        }
        else if ((afe_block->u4bufferfullflag == TRUE)&&(afe_block->u4ReadIdx == afe_block->u4WriteIdx)){ // for Play_en not active yet
            sram_free_space = 0;
        }
        else{
            sram_free_space = afe_block->u4BufferSize + afe_block->u4ReadIdx - afe_block->u4WriteIdx;
        }
        sram_free_space = word_size_align(sram_free_space);// 4-byte alignment
        if (channel_type == STREAM_S_AFE_S){
            copy_size = MINIMUM(sram_free_space >> 1, remain_sink_data);
            afe_block->u4bufferfullflag = (sram_free_space == copy_size << 1) ? TRUE : FALSE;
            sram_free_space = copy_size << 1;
        } else {
            copy_size = MINIMUM(sram_free_space, remain_sink_data);
            afe_block->u4bufferfullflag = (sram_free_space == copy_size) ? TRUE : FALSE;
            sram_free_space = copy_size;
        }

        #if 0
        if (afe_block->u4WriteIdx + sram_free_space < afe_block->u4BufferSize) {
            audio_split_sink_to_interleaved(sink, (afe_block->pSramBufAddr + afe_block->u4WriteIdx), 0, sram_free_space);
        } else {
            uint32_t size_1 = 0, size_2 = 0;
            size_1 = word_size_align((afe_block->u4BufferSize - afe_block->u4WriteIdx));
            size_2 = word_size_align((sram_free_space - size_1));
            audio_split_sink_to_interleaved(sink, (afe_block->pSramBufAddr + afe_block->u4WriteIdx), 0, size_1);
            audio_split_sink_to_interleaved(sink, (afe_block->pSramBufAddr), size_1, size_2);
        }
        #else
        audio_afe_sink_interleaved(sink, sram_free_space);
        #endif
        afe_block->u4WriteIdx = (afe_block->u4WriteIdx + sram_free_space)% afe_block->u4BufferSize;
        SinkBufferUpdateReadPtr(sink, copy_size);
#endif


        if (sink->transform != NULL) {
            DSP_STREAMING_PARA_PTR  pStream =DSP_Streaming_Get(sink->transform->source ,sink);
            if (pStream!=NULL) {
                if (pStream->streamingStatus == STREAMING_END) {
                    if (Audio_setting->Audio_sink.Zero_Padding_Cnt>0) {
                        Audio_setting->Audio_sink.Zero_Padding_Cnt--;
                        printf("DL zero pad %d",Audio_setting->Audio_sink.Zero_Padding_Cnt);//modify for ab1568
                    }
                }
            }
        }

        AudioCheckTransformHandle(sink->transform);
        hal_nvic_restore_interrupt_mask(mask);
    }

    pre_AFE_CLASSG_MON0_b29 = AFE_CLASSG_MON0_b29 & 0x20000000;
    AFE_CLASSG_MON0_b29 = ReadREG(0x70000EFC) & 0x20000000;

    if (((pre_AFE_CLASSG_MON0_b29 == 0x20000000) && (AFE_CLASSG_MON0_b29 == 0)) || ((pre_AFE_CLASSG_MON0_b29 == 0x0) && (AFE_CLASSG_MON0_b29 == 0))) {
        vRegResetBit(0xA207022C, 8);
        hal_gpt_delay_us(20);
        vRegSetBit(0xA207022C, 8);
    }
#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
    if (afe_block->u4asrcflag) {
//#if (AFE_REGISTER_ASRC_IRQ)
//#else
#ifdef ENABLE_HWSRC_CLKSKEW
       if(ClkSkewMode_g == CLK_SKEW_V2) {
       } else
#endif
       {
          AFE_WRITE(ASM_CH01_IBUF_WRPNT, buffer_info->WriteOffset + AFE_READ(ASM_IBUF_SADR));
       }
//#endif /*AFE_REGISTER_ASRC_IRQ*/
    }
#endif /*ENABLE_HWSRC_ON_MAIN_STREAM*/

}


#ifdef ENABLE_HWSRC_CLKSKEW
void clock_skew_asrc_set_compensated_sample(afe_asrc_compensating_t cp_point){
    volatile SINK sink = Sink_blks[SINK_TYPE_AUDIO];
    afe_block_t *afe_block = &sink->param.audio.AfeBlkControl;
    afe_block->u4asrcSetCompensatedSamples = cp_point;
    //DSP_MW_LOG_W("clock_skew_asrc_set_compensated_sample: %d", 1, afe_block->u4asrcSetCompensatedSamples);
}

void clock_skew_asrc_get_compensated_sample(uint32_t *accumulate_array){
    volatile SINK sink = Sink_blks[SINK_TYPE_AUDIO];
    afe_block_t *afe_block = &sink->param.audio.AfeBlkControl;
    if(accumulate_array !=NULL && sizeof(accumulate_array[0])>=sizeof(afe_block->u4asrcGetaccumulate_array[0]) ){
        memcpy( accumulate_array, afe_block->u4asrcGetaccumulate_array, sizeof(afe_block->u4asrcGetaccumulate_array) );
    }else{
        DSP_MW_LOG_W("clock_skew_asrc_get_compensated_sample fail",0);

    }
}
#endif

ATTR_TEXT_IN_IRAM void stream_audio_srcl_interrupt(void)
{
    uint32_t output_write_pointer, input_read_pointer, input_read_offset;
    hal_audio_get_value_parameter_t get_value_parameter;
    hal_audio_set_value_parameter_t set_value_parameter;
    SINK sink = Sink_blks[SINK_TYPE_AUDIO];
    uint32_t mask,pre_offset,isr_interval ;
    uint32_t hw_current_read_idx = 0;
    BUFFER_INFO *buffer_info = &sink->streamBuffer.BufferInfo;
    AUDIO_PARAMETER *runtime = &sink->param.audio;
    uint32_t dl_base_addr = (uint32_t)buffer_info->startaddr[0];
    get_value_parameter.get_current_offset.memory_select = HAL_AUDIO_MEMORY_DL_SRC1;
    input_read_pointer = hal_audio_get_value(&get_value_parameter, HAL_AUDIO_GET_MEMORY_INPUT_CURRENT_OFFSET);
    output_write_pointer = hal_audio_get_value(&get_value_parameter, HAL_AUDIO_GET_MEMORY_OUTPUT_CURRENT_OFFSET);
    UNUSED(input_read_offset);

    if (!sink) {
        DSP_MW_LOG_I("sink == NULL\r\n", 0);
        return;
    }
    if (!sink->streamBuffer.BufferInfo.startaddr[0])
    {
        DSP_MW_LOG_I("ufferInfo.startaddr[0] == NULL\r\n", 0);
        return;
    }

    dl_base_addr = AFE_GET_REG(ASM_IBUF_SADR);
    //hw_current_read_idx = AFE_GET_REG(ASM_CH01_IBUF_RDPNT);
    //input_read_offset = input_read_pointer - (uint32_t)sink->streamBuffer.BufferInfo.startaddr[0];
    hw_current_read_idx = input_read_pointer;
    #if 1
    if (runtime->irq_exist == false) {
        runtime->irq_exist = true;
        DSP_MW_LOG_I("DSP afe dl1 interrupt exist\r\n", 0);
    }
    //stream_audio_sink_handler(sink->type, input_read_offset);
    hal_nvic_save_and_set_interrupt_mask(&mask);

    if ((hw_current_read_idx == 0)) { //should chk setting if =0

        hw_current_read_idx = (uint32_t)sink->streamBuffer.BufferInfo.startaddr[0];

    }
    pre_offset = buffer_info->ReadOffset;


    buffer_info->ReadOffset = hw_current_read_idx - dl_base_addr;
    isr_interval = (pre_offset <= buffer_info->ReadOffset)
                     ? (buffer_info->ReadOffset - pre_offset)
                     : (buffer_info->length + buffer_info->ReadOffset - pre_offset);
    //printf("srcl pre %d rpt %d wpt %d len %d asrc out wo %d Ro %d\r\n",pre_offset,buffer_info->ReadOffset,buffer_info->WriteOffset,buffer_info->length,AFE_GET_REG(ASM_CH01_OBUF_WRPNT)-AFE_GET_REG(ASM_OBUF_SADR),AFE_GET_REG(ASM_CH01_OBUF_RDPNT)-AFE_GET_REG(ASM_OBUF_SADR));
    /*Clear up last time used memory */
    if (buffer_info->ReadOffset >= pre_offset) {
        memset((void *)(dl_base_addr+pre_offset), 0, buffer_info->ReadOffset - pre_offset);
    } else {
        memset((void *)(dl_base_addr+pre_offset), 0, buffer_info->length - pre_offset);
        memset((void *)dl_base_addr, 0, buffer_info->ReadOffset);
    }

    if (buffer_info->ReadOffset != buffer_info->WriteOffset) {
        buffer_info->bBufferIsFull = FALSE;
    }

    if  (OFFSET_OVERFLOW_CHK(pre_offset,(buffer_info->ReadOffset)%buffer_info->length, buffer_info->WriteOffset )&&(buffer_info->bBufferIsFull == FALSE))//Sram empty

    {

        bool empty = true;

        if(empty){
            DSP_MW_LOG_W("SRAM Empty play en:%d pR:%d R:%d W:%d", 4,isr_interval, pre_offset, buffer_info->ReadOffset, buffer_info->WriteOffset);
            if ((sink->transform != NULL)&&(sink->transform->source->type == SOURCE_TYPE_A2DP)&&(sink->transform->source->param.n9_a2dp.mce_flag))
            {
                DSP_MW_LOG_W("add empty_fill_size %d %d",2,sink->param.audio.sram_empty_fill_size,2*isr_interval + ((buffer_info->ReadOffset >= buffer_info->WriteOffset) ? (buffer_info->ReadOffset - buffer_info->WriteOffset) : (buffer_info->length - buffer_info->WriteOffset + buffer_info->ReadOffset)));
                sink->param.audio.sram_empty_fill_size += 2*isr_interval + ((buffer_info->ReadOffset >= buffer_info->WriteOffset) ? (buffer_info->ReadOffset - buffer_info->WriteOffset) : (buffer_info->length - buffer_info->WriteOffset + buffer_info->ReadOffset));
            }
            buffer_info->WriteOffset = (buffer_info->ReadOffset + 2*isr_interval)%buffer_info->length;
            if (buffer_info->WriteOffset > buffer_info->ReadOffset){
                memset((void *)(dl_base_addr + buffer_info->ReadOffset), 0, buffer_info->WriteOffset - buffer_info->ReadOffset);

            } else {
                memset((void *)(dl_base_addr + buffer_info->ReadOffset), 0, buffer_info->length - buffer_info->ReadOffset);
                memset((void *)dl_base_addr, 0, buffer_info->WriteOffset);

            }
        }
    } else if (buffer_info->ReadOffset != buffer_info->WriteOffset) {
        buffer_info->bBufferIsFull = FALSE;
    }


    if (sink->transform != NULL) {
        DSP_STREAMING_PARA_PTR  pStream =DSP_Streaming_Get(sink->transform->source ,sink);
        if (pStream!=NULL) {
            if (pStream->streamingStatus == STREAMING_END) {
                if (Audio_setting->Audio_sink.Zero_Padding_Cnt>0) {
                    Audio_setting->Audio_sink.Zero_Padding_Cnt--;
                    DSP_MW_LOG_W("DL zero pad %d",1,Audio_setting->Audio_sink.Zero_Padding_Cnt);//modify for ab1568
                }
            }
        }
    }


    #else
    //Verification code
    sink->streamBuffer.BufferInfo.ReadOffset = input_read_offset;
    sink->streamBuffer.BufferInfo.WriteOffset = (sink->streamBuffer.BufferInfo.length + sink->streamBuffer.BufferInfo.ReadOffset - 16)%sink->streamBuffer.BufferInfo.length;
    #endif

    set_value_parameter.set_current_offset.memory_select = HAL_AUDIO_MEMORY_DL_SRC1;
    set_value_parameter.set_current_offset.offset = sink->streamBuffer.BufferInfo.WriteOffset + AFE_GET_REG(ASM_IBUF_SADR);//(uint32_t)sink->streamBuffer.BufferInfo.startaddr[0];
    hal_audio_set_value(&set_value_parameter, HAL_AUDIO_SET_SRC_INPUT_CURRENT_OFFSET);
    AudioCheckTransformHandle(sink->transform);
    hal_nvic_restore_interrupt_mask(mask);
}

ATTR_TEXT_IN_IRAM void afe_vul1_interrupt_handler(void)
{
    uint32_t mask;
    uint32_t hw_current_write_idx_vul1, hw_current_write_idx_awb;
    uint32_t wptr_vul1_offset, pre_wptr_vul1_offset, pre_offset;

    SOURCE source = Source_blks[SOURCE_TYPE_AUDIO];

    AUDIO_PARAMETER *runtime = &source->param.audio;
    BUFFER_INFO *buffer_info = &source->streamBuffer.BufferInfo;
    int32_t  wptr_vul1_offset_diff_defualt = (runtime->format_bytes)*(runtime->count)*(runtime->channel_num);
    int32_t  wptr_vul1_offset_diff = 0;
    int16_t cp_samples = 0;


    /*Mce play en check*/
    if (runtime->irq_exist == false) {
        runtime->irq_exist = true;
        U32 vul_irq_time;
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &vul_irq_time);
        DSP_MW_LOG_I("DSP afe ul1 interrupt exist, time: %d", 1, vul_irq_time);
        runtime->pop_noise_pkt_num = 0;
        runtime->mute_flag = TRUE;
    } else if ((Sink_blks[SINK_TYPE_AUDIO] != NULL) &&
               (Sink_blks[SINK_TYPE_AUDIO]->param.audio.AfeBlkControl.u4awsflag == true) &&
               (Sink_blks[SINK_TYPE_AUDIO]->param.audio.irq_exist == false)) {
        uint32_t read_reg = afe_get_bt_sync_monitor(AUDIO_DIGITAL_BLOCK_MEM_DL1);
        if (read_reg == 0) {
            DSP_MW_LOG_I("DSP afe BT sync monitor by ul1 0x%x", 1, read_reg);
        }
    }

    hw_current_write_idx_vul1 = AFE_GET_REG(AFE_VUL_CUR);
    pre_wptr_vul1_offset = buffer_info->WriteOffset;

    if (source->param.audio.echo_reference == true) {
        #if 1 //modify for ab1568 workaround
        hw_current_write_idx_awb = AFE_GET_REG(AFE_AWB2_CUR);
        #else
        hw_current_write_idx_awb = AFE_GET_REG(AFE_AWB_CUR);
        #endif

    }

    #if 1//mdify for ab1568
    if(fgULRCDCPolling==TRUE){
        while(wptr_vul1_offset_diff < (wptr_vul1_offset_diff_defualt + 8)){
             wptr_vul1_offset = AFE_GET_REG(AFE_VUL_CUR) - AFE_GET_REG(AFE_VUL_BASE);
             if(wptr_vul1_offset >= pre_wptr_vul1_offset) {
                wptr_vul1_offset_diff = wptr_vul1_offset - pre_wptr_vul1_offset;
             } else {
                wptr_vul1_offset_diff = buffer_info->length + wptr_vul1_offset - pre_wptr_vul1_offset;
             }
        }
        fgULRCDCPolling = FALSE;
        wptr_vul1_offset_diff = wptr_vul1_offset_diff_defualt;
    }
    else {
        wptr_vul1_offset = hw_current_write_idx_vul1 - AFE_GET_REG(AFE_VUL_BASE);
    }
    #endif
    #if 0//modify for ab1568
    if (afe_get_memory_path_enable(AUDIO_DIGITAL_BLOCK_MEM_VUL1))
    #endif

    {
        hal_nvic_save_and_set_interrupt_mask(&mask);
        if (hw_current_write_idx_vul1 == 0) {
            hal_nvic_restore_interrupt_mask(mask);

            return;
        }
        if ((source->param.audio.echo_reference == true) && (hw_current_write_idx_awb == 0)) {
            hal_nvic_restore_interrupt_mask(mask);
            return;
        }

        /* Fill zero packet to prevent UL pop noise (Units:ms) */
        if(runtime->pop_noise_pkt_num < 240) {
            runtime->mute_flag = TRUE;
            runtime->pop_noise_pkt_num += source->param.audio.period;
        } else {
            runtime->mute_flag = FALSE;
        }


        /* For Uplink Clk Skew IRQ period control */
        cp_samples = clk_skew_check_ul_status();
        #if 0//modify for ab1568
        afe_update_audio_irq_cnt(afe_irq_request_number(AUDIO_DIGITAL_BLOCK_MEM_VUL1),
                                 (uint32_t)((int32_t)runtime->count + (int32_t)cp_samples));
        #else
        hal_audio_memory_irq_period_parameter_t irq_period;
        irq_period.memory_select= HAL_AUDIO_MEMORY_UL_VUL1;
        irq_period.rate = runtime->rate;
        irq_period.irq_counter = (uint32_t)((int32_t)runtime->count + (int32_t)cp_samples);
        hal_audio_control_set_value((hal_audio_set_value_parameter_t *)&irq_period, HAL_AUDIO_SET_MEMORY_IRQ_PERIOD);
        #endif

        if(source->transform && source->transform->sink)
        {
           SINK sink = source->transform->sink;

           DSP_CALLBACK_PTR callback_ptr = DSP_Callback_Get(source, sink);
           if (sink->type == SINK_TYPE_N9SCO && callback_ptr->Status == CALLBACK_HANDLER)
           {
               DSP_MW_LOG_W("UL Callback busy! source=%d, sink=%d", 2, source->type, sink->type);
               if (sink->param.n9sco.IsFirstIRQ == TRUE)
               {
                  SourceDrop(source,(runtime->format_bytes)*(runtime->count));
               }
               #if !DL_TRIGGER_UL
               if ((sink->param.n9sco.IsFirstIRQ == FALSE)&&(callback_ptr->IsBusy == TRUE))
               {
                  SourceDrop(source,(runtime->format_bytes)*(runtime->count));
                  SinkFlush(sink,sink->param.n9sco.process_data_length);
               }
               else
               {
                  callback_ptr->IsBusy = TRUE;
               }
               #else
               else
               {
                    SourceDrop(source,(runtime->format_bytes)*(runtime->count));
                    SinkFlush(sink,sink->param.n9sco.process_data_length);
                    N9SCO_setting->N9Sco_sink.N9_Ro_abnormal_cnt = 0;
               }
               #endif
           }
        }
        pre_offset = buffer_info->WriteOffset;//check overflow
        buffer_info->WriteOffset = wptr_vul1_offset;
        //printf("vul pre %d wpt %d,rpt %d len %d\r\n",pre_offset,buffer_info->WriteOffset, buffer_info->ReadOffset, buffer_info->length);
        if(runtime->irq_exist){
            if(OFFSET_OVERFLOW_CHK(pre_offset, buffer_info->WriteOffset, buffer_info->ReadOffset)){
                DSP_MW_LOG_W("UL OFFSET_OVERFLOW ! pre %d,w %d,r %d",3,pre_offset,buffer_info->WriteOffset,buffer_info->ReadOffset);
                if (pre_offset < buffer_info->WriteOffset) {
                    buffer_info->ReadOffset = (buffer_info->WriteOffset*2 - pre_offset)% buffer_info->length;
                } else {
                    buffer_info->ReadOffset = (buffer_info->WriteOffset*2 + (buffer_info->length - pre_offset))% buffer_info->length;
                }
            }
        }

        AudioCheckTransformHandle(source->transform);
        hal_nvic_restore_interrupt_mask(mask);
    }

}

ATTR_TEXT_IN_IRAM void afe_subsource_interrupt_handler(void)
{
#ifdef MTK_MULTI_MIC_STREAM_ENABLE

    SOURCE_TYPE      source_type;
    uint32_t hw_current_write_idx, pre_offset;

    SOURCE source;

    AUDIO_PARAMETER *runtime = &source->param.audio;
    BUFFER_INFO *buffer_info = &source->streamBuffer.BufferInfo;

    for (source_type=SOURCE_TYPE_SUBAUDIO_MIN ; source_type<=SOURCE_TYPE_SUBAUDIO_MAX ; source_type++) {
        source = Source_blks[source_type];

        if ((!source)||(!source->param.audio.is_memory_start) ){
            continue;
        }

        runtime = &source->param.audio;
        buffer_info = &source->streamBuffer.BufferInfo;

        /* First handle */
        if (runtime->irq_exist == false) {
            runtime->irq_exist = true;
            uint32_t vul_irq_time;
            hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &vul_irq_time);
            DSP_MW_LOG_I("DSP afe sub-source:%d interrupt exist, time: %d", 2, source_type, vul_irq_time);
            runtime->pop_noise_pkt_num = 0;
            runtime->mute_flag = TRUE;
        }

        /* Fill zero packet to prevent UL pop noise (Units:ms) */
        if(runtime->pop_noise_pkt_num < 240) {
            runtime->mute_flag = TRUE;
            runtime->pop_noise_pkt_num += source->param.audio.period;
        } else {
            runtime->mute_flag = FALSE;
        }

        /* Get current offset */
        hal_audio_memory_selection_t memory_search;
        hal_audio_current_offset_parameter_t get_current_offset;
        for (memory_search=HAL_AUDIO_MEMORY_UL_VUL1 ; memory_search<=HAL_AUDIO_MEMORY_UL_AWB2 ; memory_search<<=1) {
            if (source->param.audio.mem_handle.memory_select&memory_search) {
                break;
            }
        }
        get_current_offset.memory_select = memory_search;
        get_current_offset.pure_agent_with_src = false;
        hw_current_write_idx = hal_audio_get_value((hal_audio_get_value_parameter_t *)&get_current_offset, HAL_AUDIO_GET_MEMORY_INPUT_CURRENT_OFFSET);
        pre_offset = buffer_info->WriteOffset;
        buffer_info->WriteOffset = hw_current_write_idx - get_current_offset.base_address;

        #if 0
        /* Clock skew */
        hal_audio_memory_irq_period_parameter_t irq_period;
        int16_t cp_sample = 0;
        cp_sample = clk_skew_check_ul_status();

        irq_period.memory_select = HAL_AUDIO_MEMORY_UL_AWB2;//Keep at AWB2 for sub source
        irq_period.rate = Audio_setting->Audio_source.memory.audio_path_rate;
        irq_period.irq_counter = (uint32_t)(Audio_setting->Audio_source.memory.irq_counter + cp_sample);
        hal_audio_set_value((hal_audio_set_value_parameter_t *)&irq_period , HAL_AUDIO_SET_MEMORY_IRQ_PERIOD);
        #endif

        /* overflow check */
        if(OFFSET_OVERFLOW_CHK(pre_offset, buffer_info->WriteOffset, buffer_info->ReadOffset)){
            DSP_MW_LOG_W("DSP Sub-Source:%d OFFSET_OVERFLOW ! pre:0x%x, w:0x%x, r:0x%x", 4, source_type, pre_offset, buffer_info->WriteOffset, buffer_info->ReadOffset);
        }

        /* Stream handler */
        if(source->transform && source->transform->sink)
        {
            AudioCheckTransformHandle(source->transform);
        }
    }
#endif
}


#ifdef MTK_PROMPT_SOUND_ENABLE
volatile uint32_t vp_sram_empty_flag = 0;
extern volatile uint32_t vp_config_flag;
void afe_dl2_interrupt_handler(void)
{
    //printf("afe_dl2_interrupt_handler\r\n");
    uint32_t mask,pre_offset ;
    int32_t hw_current_read_idx = 0;
    volatile SINK sink = Sink_blks[SINK_TYPE_VP_AUDIO];
    afe_block_t *afe_block = &sink->param.audio.AfeBlkControl;
    BUFFER_INFO *buffer_info = &sink->streamBuffer.BufferInfo;
    uint32_t dl_base_addr;
    AUDIO_PARAMETER *runtime = &sink->param.audio;

    #if 0//modify for ab1568
    if (afe_block->u4asrcflag) {
    #else
    if (sink->param.audio.mem_handle.pure_agent_with_src) {
    #endif
        if (runtime->irq_exist == false) {
            runtime->irq_exist = true;
            hal_src_set_start(AFE_SRC_2, true);
        }
        dl_base_addr = AFE_GET_REG(ASM2_IBUF_SADR);
        hw_current_read_idx = AFE_GET_REG(ASM2_CH01_IBUF_RDPNT);
    } else {
        dl_base_addr = AFE_GET_REG(AFE_DL2_BASE);
        hw_current_read_idx = AFE_GET_REG(AFE_DL2_CUR);
    }


    //printf("AFE_DL2_CUR:%x\r\n", hw_current_read_idx);
    //printf("AFE_DL2_BASE:%x\r\n", AFE_GET_REG(AFE_DL2_BASE));
    //printf("addr:%x, value is %x\r\n", hw_current_read_idx, (*(volatile uint32_t *)hw_current_read_idx));

    //if (afe_get_memory_path_enable(AUDIO_DIGITAL_BLOCK_MEM_DL2))//modify for ab1568
    if (1)//modify for ab1568
    {

        hal_nvic_save_and_set_interrupt_mask(&mask);
        if (hw_current_read_idx == 0) { //should chk setting if =0
            hw_current_read_idx = word_size_align((S32) dl_base_addr);
        }
        pre_offset = buffer_info->ReadOffset;
        buffer_info->ReadOffset = hw_current_read_idx - dl_base_addr;
        /*Clear up last time used memory */
        if (buffer_info->ReadOffset >= pre_offset) {
            memset((void *)(dl_base_addr+pre_offset), 0, buffer_info->ReadOffset - pre_offset);
        } else {
            memset((void *)(dl_base_addr+pre_offset), 0, buffer_info->length - pre_offset);
            memset((void *)dl_base_addr, 0, buffer_info->ReadOffset);
        }

        /*Clear up last time used memory */
        if (buffer_info->ReadOffset >= pre_offset) {
            memset((void *)(dl_base_addr+pre_offset), 0, buffer_info->ReadOffset - pre_offset);
        } else {
            memset((void *)(dl_base_addr+pre_offset), 0, buffer_info->length - pre_offset);
            memset((void *)(dl_base_addr), 0, buffer_info->ReadOffset);
        }

        if ((OFFSET_OVERFLOW_CHK(pre_offset, buffer_info->ReadOffset, buffer_info->WriteOffset)&&(buffer_info->bBufferIsFull==FALSE))
            || (((buffer_info->WriteOffset+buffer_info->length-buffer_info->ReadOffset)%buffer_info->length < 32) && (afe_block->u4asrcflag)))
        { // SRAM Empty
            bool empty = true;
            if(afe_block->u4asrcflag) {
                uint32_t src_out_size, src_out_read, src_out_write, remain_data;
                src_out_write = AFE_READ(ASM2_CH01_OBUF_WRPNT);
                src_out_read = AFE_READ(ASM2_CH01_OBUF_RDPNT);
                src_out_size = AFE_READ(ASM2_OBUF_SIZE);
                remain_data = (src_out_write > src_out_read) ? src_out_write-src_out_read : src_out_write+src_out_size- src_out_read;
                remain_data = (sink->param.audio.channel_num>=2) ? remain_data>>1 : remain_data;
                if (remain_data > (afe_block->u4asrcrate*sink->param.audio.period*sink->param.audio.format_bytes)/1000) {
                    empty = false;
                }
            }
            if (empty) {
                DSP_MW_LOG_W("SRAM Empty play pR:%d R:%d W:%d==============", 3,pre_offset, buffer_info->ReadOffset, buffer_info->WriteOffset);

                #if 0//modify for ab1568
                if (afe_block->u4asrcflag) {
                #else
                if (sink->param.audio.mem_handle.pure_agent_with_src) {
                #endif
                    buffer_info->WriteOffset = (buffer_info->ReadOffset + buffer_info->length/2) % buffer_info->length;
                } else {
                    if (pre_offset < buffer_info->ReadOffset) {
                        buffer_info->WriteOffset = (buffer_info->ReadOffset*2 - pre_offset)% buffer_info->length;
                    } else {
                        buffer_info->WriteOffset = (buffer_info->ReadOffset*2 + (buffer_info->length - pre_offset))% buffer_info->length;
                    }
                }



                if (buffer_info->WriteOffset > buffer_info->ReadOffset) {
                    memset((void *)hw_current_read_idx, 0, buffer_info->WriteOffset - buffer_info->ReadOffset);
                } else {
                    memset((void *)hw_current_read_idx, 0, buffer_info->length - buffer_info->ReadOffset);
                    memset((void *)dl_base_addr, 0, buffer_info->WriteOffset);
                }

                if (vp_config_flag == 1) {
                    vp_sram_empty_flag = 1;
                    xTaskResumeFromISR((TaskHandle_t)pDPR_TaskHandler);
                    portYIELD_FROM_ISR(pdTRUE); // force to do context switch
                }

            }
        } else if (buffer_info->ReadOffset != buffer_info->WriteOffset) {
            buffer_info->bBufferIsFull = FALSE;
        }
#if 0
        if (afe_block->u4ReadIdx > afe_block->u4WriteIdx) {
            sram_free_space = afe_block->u4ReadIdx - afe_block->u4WriteIdx;
        } else {
            sram_free_space = afe_block->u4BufferSize + afe_block->u4ReadIdx - afe_block->u4WriteIdx;
        }
        sram_free_space = word_size_align(sram_free_space);// 4-byte alignment
        if (channel_type == STREAM_S_AFE_S){
            copy_size = MINIMUM(sram_free_space >> 1, remain_sink_data);
            afe_block->u4bufferfullflag = (sram_free_space == copy_size << 1) ? TRUE : FALSE;
            sram_free_space = copy_size << 1;
        } else {
            copy_size = MINIMUM(sram_free_space, remain_sink_data);
            afe_block->u4bufferfullflag = (sram_free_space == copy_size) ? TRUE : FALSE;
            sram_free_space = copy_size;
        }


        #if 0
        if (afe_block->u4WriteIdx + sram_free_space < afe_block->u4BufferSize) {
            audio_split_sink_to_interleaved(sink, (dl_base_addr + afe_block->u4WriteIdx), 0, sram_free_space);
        } else {
            uint32_t size_1 = 0, size_2 = 0;
            size_1 = word_size_align((afe_block->u4BufferSize - afe_block->u4WriteIdx));
            size_2 = word_size_align((sram_free_space - size_1));
            audio_split_sink_to_interleaved(sink, (dl_base_addr + afe_block->u4WriteIdx), 0, size_1);
            audio_split_sink_to_interleaved(sink, (dl_base_addr), size_1, size_2);
        }
        #else
        audio_afe_sink_interleaved(sink, sram_free_space);
        #endif

        afe_block->u4WriteIdx = (afe_block->u4WriteIdx + sram_free_space)% afe_block->u4BufferSize;
        SinkBufferUpdateReadPtr(sink, copy_size);
        hal_nvic_restore_interrupt_mask(mask);
        #endif
        AudioCheckTransformHandle(sink->transform);
        hal_nvic_restore_interrupt_mask(mask);
    }
    if (afe_block->u4asrcflag) {
        AFE_WRITE(ASM2_CH01_IBUF_WRPNT, buffer_info->WriteOffset + AFE_READ(ASM2_IBUF_SADR));
    }

}

void stream_audio_src2_interrupt(void)
{
    uint32_t output_write_pointer, input_read_pointer, input_read_offset;
    hal_audio_get_value_parameter_t get_value_parameter;
    hal_audio_set_value_parameter_t set_value_parameter;
    SINK sink = Sink_blks[SINK_TYPE_VP_AUDIO];
    uint32_t mask,pre_offset ;
    int32_t hw_current_read_idx = 0;
    BUFFER_INFO *buffer_info = &sink->streamBuffer.BufferInfo;
    uint32_t dl_base_addr;
    get_value_parameter.get_current_offset.memory_select = HAL_AUDIO_MEMORY_DL_SRC2;
    input_read_pointer = hal_audio_get_value(&get_value_parameter, HAL_AUDIO_GET_MEMORY_INPUT_CURRENT_OFFSET);
    output_write_pointer = hal_audio_get_value(&get_value_parameter, HAL_AUDIO_GET_MEMORY_OUTPUT_CURRENT_OFFSET);

    if (!sink) {
        return;
    }
    if (!sink->streamBuffer.BufferInfo.startaddr[0])
    {
        return;
    }

    input_read_offset = input_read_pointer - (uint32_t)sink->streamBuffer.BufferInfo.startaddr[0];
    hw_current_read_idx = input_read_offset;
    dl_base_addr = (uint32_t)sink->streamBuffer.BufferInfo.startaddr[0];
    #if 1
    //stream_audio_sink_handler(sink->type, input_read_offset);
    hal_nvic_save_and_set_interrupt_mask(&mask);
    if (hw_current_read_idx == 0) { //should chk setting if =0
        hw_current_read_idx = word_size_align((S32) dl_base_addr);
    }
    pre_offset = buffer_info->ReadOffset;
    buffer_info->ReadOffset = hw_current_read_idx - dl_base_addr;
    /*Clear up last time used memory */
    if (buffer_info->ReadOffset >= pre_offset) {
        memset((void *)(dl_base_addr+pre_offset), 0, buffer_info->ReadOffset - pre_offset);
    } else {
        memset((void *)(dl_base_addr+pre_offset), 0, buffer_info->length - pre_offset);
        memset((void *)dl_base_addr, 0, buffer_info->ReadOffset);
    }

    /*Clear up last time used memory */
    if (buffer_info->ReadOffset >= pre_offset) {
        memset((void *)(dl_base_addr+pre_offset), 0, buffer_info->ReadOffset - pre_offset);
    } else {
        memset((void *)(dl_base_addr+pre_offset), 0, buffer_info->length - pre_offset);
        memset((void *)(dl_base_addr), 0, buffer_info->ReadOffset);
    }

    if ((OFFSET_OVERFLOW_CHK(pre_offset, buffer_info->ReadOffset, buffer_info->WriteOffset)&&(buffer_info->bBufferIsFull==FALSE)))
    { // SRAM Empty
        bool empty = true;

        if (empty) {
            DSP_MW_LOG_W("SRAM Empty play pR:%d R:%d W:%d==============", 3,pre_offset, buffer_info->ReadOffset, buffer_info->WriteOffset);
            if (pre_offset < buffer_info->ReadOffset) {
                buffer_info->WriteOffset = (buffer_info->ReadOffset*2 - pre_offset)% buffer_info->length;
            } else {
                buffer_info->WriteOffset = (buffer_info->ReadOffset*2 + (buffer_info->length - pre_offset))% buffer_info->length;
            }

            if (buffer_info->WriteOffset > buffer_info->ReadOffset) {
                memset((void *)hw_current_read_idx, 0, buffer_info->WriteOffset - buffer_info->ReadOffset);
            } else {
                memset((void *)hw_current_read_idx, 0, buffer_info->length - buffer_info->ReadOffset);
                memset((void *)dl_base_addr, 0, buffer_info->WriteOffset);
            }

            if (vp_config_flag == 1) {
                vp_sram_empty_flag = 1;
                xTaskResumeFromISR((TaskHandle_t)pDPR_TaskHandler);
                portYIELD_FROM_ISR(pdTRUE); // force to do context switch
            }

        }
    } else if (buffer_info->ReadOffset != buffer_info->WriteOffset) {
        buffer_info->bBufferIsFull = FALSE;
    }
    #else
    //Verification code
    sink->streamBuffer.BufferInfo.ReadOffset = input_read_offset;
    sink->streamBuffer.BufferInfo.WriteOffset = (sink->streamBuffer.BufferInfo.length + sink->streamBuffer.BufferInfo.ReadOffset - 16)%sink->streamBuffer.BufferInfo.length;
    #endif

    set_value_parameter.set_current_offset.memory_select = HAL_AUDIO_MEMORY_DL_SRC2;
    set_value_parameter.set_current_offset.offset = sink->streamBuffer.BufferInfo.WriteOffset + (uint32_t)sink->streamBuffer.BufferInfo.startaddr[0];
    hal_audio_set_value(&set_value_parameter, HAL_AUDIO_SET_SRC_INPUT_CURRENT_OFFSET);

    AudioCheckTransformHandle(sink->transform);
    hal_nvic_restore_interrupt_mask(mask);
}

void afe_asrc_interrupt_handler(afe_mem_asrc_id_t asrc_id)
{
    uint32_t irq_status = afe_get_asrc_irq_status(asrc_id);

    if (afe_get_memory_path_enable(AUDIO_DIGITAL_BLOCK_MEM_DL1) && asrc_id == AFE_MEM_ASRC_1) {

        if (afe_get_asrc_irq_is_enabled(asrc_id, ASM_IER_IBUF_EMPTY_INTEN_MASK) && (irq_status&ASM_IFR_IBUF_EMPTY_INT_MASK)) {

        }

        if (afe_get_asrc_irq_is_enabled(asrc_id, ASM_IER_IBUF_AMOUNT_INTEN_MASK) && (irq_status&ASM_IFR_IBUF_AMOUNT_INT_MASK)) {

        }

        if (afe_get_asrc_irq_is_enabled(asrc_id, ASM_IER_OBUF_OV_INTEN_MASK) && (irq_status&ASM_IFR_OBUF_OV_INT_MASK)) {

        }

        if (afe_get_asrc_irq_is_enabled(asrc_id, ASM_IER_OBUF_AMOUNT_INTEN_MASK) && (irq_status&ASM_IFR_OBUF_AMOUNT_INT_MASK)) {

        }
    } else if (afe_get_memory_path_enable(AUDIO_DIGITAL_BLOCK_MEM_DL2) && asrc_id == AFE_MEM_ASRC_2) {
        if (afe_get_asrc_irq_is_enabled(asrc_id, ASM_IER_IBUF_EMPTY_INTEN_MASK) && (irq_status&ASM_IFR_IBUF_EMPTY_INT_MASK)) {

        }

        if (afe_get_asrc_irq_is_enabled(asrc_id, ASM_IER_IBUF_AMOUNT_INTEN_MASK) && (irq_status&ASM_IFR_IBUF_AMOUNT_INT_MASK)) {

        }

        if (afe_get_asrc_irq_is_enabled(asrc_id, ASM_IER_OBUF_OV_INTEN_MASK) && (irq_status&ASM_IFR_OBUF_OV_INT_MASK)) {

        }

        if (afe_get_asrc_irq_is_enabled(asrc_id, ASM_IER_OBUF_AMOUNT_INTEN_MASK) && (irq_status&ASM_IFR_OBUF_AMOUNT_INT_MASK)) {
            afe_dl2_interrupt_handler();
        }
    }
}

void afe_dl2_query_data_amount(uint32_t *sink_data_count, uint32_t *afe_sram_data_count)
{
    volatile SINK sink = Sink_blks[SINK_TYPE_VP_AUDIO];
    afe_block_t *afe_block = &sink->param.audio.AfeBlkControl;
    int32_t hw_current_read_idx = AFE_GET_REG(AFE_DL2_CUR);
    afe_block->u4ReadIdx = hw_current_read_idx - AFE_GET_REG(AFE_DL2_BASE);

    //AFE DL2 SRAM data amount
    if (afe_block->u4WriteIdx > afe_block->u4ReadIdx) {
        *afe_sram_data_count = afe_block->u4WriteIdx - afe_block->u4ReadIdx;
    } else {
        *afe_sram_data_count = afe_block->u4BufferSize + afe_block->u4WriteIdx - afe_block->u4ReadIdx;
    }

    //Sink audio data amount
    *sink_data_count = sink->streamBuffer.BufferInfo.length - SinkSlack(sink);
}
#endif

int32_t afe_set_mem_block(afe_stream_type_t type, audio_digital_block_t mem_blk)
{
    afe_block_t *afe_block;
    AUDIO_PARAMETER *audio_para;
    uint32_t write_index, read_index;
    if (type == AFE_SINK) {
        afe_block = &Sink_blks[SINK_TYPE_AUDIO]->param.audio.AfeBlkControl;
        audio_para = &Sink_blks[SINK_TYPE_AUDIO]->param.audio;
        write_index = 0x800;
        read_index = 0;
    } else if (type == AFE_SINK_VP){
        afe_block = &Sink_blks[SINK_TYPE_VP_AUDIO]->param.audio.AfeBlkControl;
        audio_para = &Sink_blks[SINK_TYPE_VP_AUDIO]->param.audio;
        write_index = audio_para->buffer_size*3/4;
        read_index = 0;
    } else {
        afe_block = &Source_blks[SOURCE_TYPE_AUDIO]->param.audio.AfeBlkControl;
        audio_para = &Source_blks[SOURCE_TYPE_AUDIO]->param.audio;
        write_index = 0;
        read_index = 960;
    }

    afe_block->u4BufferSize = audio_para->buffer_size;  // [Tochk] w/ channel, unit: bytes
    //afe_block->pSramBufAddr = (uint8_t *)((volatile uint32_t *)afe_block->phys_buffer_addr);
    afe_block->u4SampleNumMask = 0x001f; /* 32 byte align */
    afe_block->u4WriteIdx = (write_index)%afe_block->u4BufferSize;
    afe_block->u4ReadIdx = (read_index)%afe_block->u4BufferSize;

    afe_block->u4DataRemained = 0;

    afe_set_mem_block_addr(mem_blk, afe_block);
    return 0;
}

void afe_free_audio_sram(afe_pcm_format_t type)
{
    uint32_t i = 0, mask;
    afe_block_t *afe_block;
    afe_sram_block_t *sram_block = NULL;

    if (type == AFE_SINK) {
        afe_block = &Sink_blks[SINK_TYPE_AUDIO]->param.audio.AfeBlkControl;
#ifdef MTK_PROMPT_SOUND_ENABLE
    } else if (type == AFE_SINK_VP) {
        afe_block = &Sink_blks[SINK_TYPE_VP_AUDIO]->param.audio.AfeBlkControl;
        return;
#endif
    } else {
        afe_block = &Source_blks[SOURCE_TYPE_AUDIO]->param.audio.AfeBlkControl;
    }

    hal_nvic_save_and_set_interrupt_mask(&mask);

    for (i = 0; i < audio_sram_manager.mBlocknum; i++) {
        sram_block = &audio_sram_manager.mAud_sram_block[i];
        if (sram_block->mUser == afe_block) {
            sram_block->mUser = NULL;
        }
    }
    hal_nvic_restore_interrupt_mask(mask);
}

hal_audio_irq_callback_function_t afe_irq_ops = {
    .afe_dl1_interrupt_handler     = afe_dl1_interrupt_handler,
#ifdef MTK_PROMPT_SOUND_ENABLE
    .afe_dl2_interrupt_handler     = afe_dl2_interrupt_handler,
#else
    .afe_dl2_interrupt_handler     = NULL,
#endif
    .afe_dl3_interrupt_handler     = NULL,
    .afe_vul1_interrupt_handler    = afe_vul1_interrupt_handler,
    .afe_vul2_interrupt_handler    = NULL,
    .afe_awb_interrupt_handler     = NULL,
    .afe_awb2_interrupt_handler    = NULL,
    .afe_dl12_interrupt_handler    = NULL,
#ifdef MTK_ANC_ENABLE
    .afe_anc_pwrdet_interrupt_handler = afe_anc_pwrdet_interrupt_handler,
#else
    .afe_anc_pwrdet_interrupt_handler = NULL,
#endif
};

/*Hook AFE IRQ callback by user's implementation.*/
void afe_register_irq_ops(void)
{
    hal_audio_afe_register_irq_callback(&afe_irq_ops);

    afe_register_asrc_irq_callback_function(afe_asrc_interrupt_handler);

}

#ifdef ENABLE_AMP_TIMER
void afe_send_amp_status_ccni(bool enable)
{
    hal_ccni_message_t msg;
    uint32_t status;
    memset((void *)&msg, 0, sizeof(hal_ccni_message_t));
    msg.ccni_message[0] = ((MSG_DSP2MCU_AUDIO_AMP << 16) | enable);
    status = aud_msg_tx_handler(msg, 0, FALSE);

    DSP_MW_LOG_I("DSP AMP afe_send_amp_status_ccni:%d \r\n", 1, enable);
}

bool afe_amp_open_handler(uint32_t samplerate)
{
#if 0
    return true;
#else
    bool reboot_dac;
    reboot_dac = fw_amp_timer_stop(samplerate);
    afe_send_amp_status_ccni(true);
    return reboot_dac;
#endif
}

bool afe_amp_closure_handler(void)
{
#if 0
    return true;
#else
    if (fw_amp_get_status() == FW_AMP_TIMER_END) {
        fw_amp_set_status(FW_AMP_TIMER_STOP);
        return true;
    } else {
        //Set amp timer
        fw_amp_timer_start();
        return false;
    }
#endif
}

hal_amp_function_t afe_amp_ops = {
    .open_handler       = afe_amp_open_handler,
    .closure_handler    = afe_amp_closure_handler,
};

/*Hook AFE Amp handler.*/
void afe_register_amp_handler(void)
{
    fw_amp_init_semaphore();
    fw_amp_init_timer();
    hal_audio_afe_register_amp_handle(&afe_amp_ops);
}
#endif

/*Calculate greatest common factor*/
uint32_t audio_get_gcd(uint32_t m, uint32_t n)
{
    while(n != 0) {
        uint32_t r = m % n;
        m = n;
        n = r;
    }
    return m;
}

void afe_set_asrc_ul_configuration_parameters(SOURCE source, afe_asrc_config_p asrc_config)
{
    UNUSED(source);
    UNUSED(asrc_config);
    #if 0//modify for ab1568
    uint32_t device_rate;
    asrc_config->ul_mode = true;
    asrc_config->stereo = (source->param.audio.channel_num>=2);
    asrc_config->hw_update_obuf_rdpnt = false;

    if (source->param.audio.audio_device == HAL_AUDIO_DEVICE_I2S_SLAVE)
    {
        asrc_config->tracking_mode = MEM_ASRC_TRACKING_MODE_RX;
        asrc_config->tracking_clock = afe_set_asrc_tracking_clock(source->param.audio.audio_interface);
        device_rate = source->param.audio.rate;
    }
    else
    {
        asrc_config->tracking_mode = MEM_ASRC_NO_TRACKING;
        device_rate = afe_get_audio_device_samplerate(source->param.audio.audio_device, source->param.audio.audio_interface);
    }

    asrc_config->input_buffer.addr = source->param.audio.AfeBlkControl.phys_buffer_addr;
    asrc_config->input_buffer.size = source->param.audio.AfeBlkControl.u4asrc_buffer_size;
    asrc_config->input_buffer.rate = device_rate;
    asrc_config->input_buffer.offset = 32;////((((source->param.audio.period+5)*source->param.audio.format_bytes*asrc_config->input_buffer.rate*((asrc_config->stereo==true) ? 2 : 1)/1000)+ 7) & (~7))%asrc_config->input_buffer.size;
    asrc_config->input_buffer.format = source->param.audio.format;

    asrc_config->output_buffer.addr = source->param.audio.AfeBlkControl.phys_buffer_addr+source->param.audio.AfeBlkControl.u4asrc_buffer_size;
    asrc_config->output_buffer.size = source->param.audio.buffer_size;
    asrc_config->output_buffer.rate = source->param.audio.src_rate;
    asrc_config->output_buffer.offset = source->streamBuffer.BufferInfo.ReadOffset;
    asrc_config->output_buffer.format = source->param.audio.format;

    DSP_MW_LOG_I("DSP asrc in rate:%d, out rate:%d\r\n",2, asrc_config->input_buffer.rate, asrc_config->output_buffer.rate);
    #endif
}

void afe_set_asrc_dl_configuration_parameters(SINK sink, afe_asrc_config_p asrc_config)
{
    asrc_config->ul_mode = false;
    asrc_config->tracking_mode = MEM_ASRC_NO_TRACKING;
    asrc_config->stereo = (sink->param.audio.channel_num>=2);
    asrc_config->hw_update_obuf_rdpnt = true;


    asrc_config->input_buffer.addr = sink->param.audio.AfeBlkControl.phys_buffer_addr+sink->param.audio.AfeBlkControl.u4asrc_buffer_size;
    asrc_config->input_buffer.size = sink->param.audio.buffer_size;
    asrc_config->input_buffer.rate = sink->param.audio.src_rate;
    asrc_config->input_buffer.offset = sink->streamBuffer.BufferInfo.WriteOffset;
    asrc_config->input_buffer.format = sink->param.audio.format;

    asrc_config->output_buffer.addr = sink->param.audio.AfeBlkControl.phys_buffer_addr;
    asrc_config->output_buffer.size = sink->param.audio.AfeBlkControl.u4asrc_buffer_size;
    asrc_config->output_buffer.rate = afe_get_audio_device_samplerate(sink->param.audio.audio_device, sink->param.audio.audio_interface);
    asrc_config->output_buffer.offset = ((((sink->param.audio.period+5)*sink->param.audio.format_bytes*asrc_config->output_buffer.rate*((asrc_config->stereo==true) ? 2 : 1)/1000)+ 16 +7)& ~7 )%asrc_config->output_buffer.size ;
    asrc_config->output_buffer.format = sink->param.audio.format;


    DSP_MW_LOG_I("DSP asrc in rate:%d, out rate:%d\r\n", 2, asrc_config->input_buffer.rate, asrc_config->output_buffer.rate);
}
 
