/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

/*!
 *@file   Stream.c
 *@brief  define stream api provides efficiently processing streams
 *
 @verbatim
 @endverbatim
 */

//- system header
#include "dlist.h"
#include "stream.h"
#include "dtm.h"
//- module header
#include "source_inter.h"
#include "sink_inter.h"
#include "dsp_callback.h"

#include "stream.h"
#include <string.h>
#include "dsp_memory.h"
#include "stream_memory.h"
#include "stream_n9_a2dp.h"
#include "types.h"
#include "stream_n9sco.h"
#include "hal_hw_semaphore.h"
#include "hal_audio_afe_control.h"
#include "stream_audio_hardware.h"
#include "stream_audio_driver.h"
#include "stream_audio_afe.h"
#include "stream_audio.h"
#include "dsp_audio_ctrl.h"
#include "stream_cm4_record.h"
#include <string.h>
#include "stream_cm4_playback.h"
#include "stream_cm4_vp_playback.h"
#include "stream_data_ul.h"

#ifdef ENABLE_HWSRC_CLKSKEW
#include "clk_skew.h"
#endif


////////////////////////////////////////////////////////////////////////////////
// Constant Definitions ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#define HwSemRetryTimes 50000
#define ASRC_OUT_BUFFER_SIZE 4096;
////////////////////////////////////////////////////////////////////////////////
// Function Declarations ///////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
BOOL StreamBufferToBufferHandler(SOURCE source, SINK sink)
{
    U32 result = FALSE;
    U32 RxSize,TxSize,MoveLength;

    if(source && sink)
    {
        RxSize = SourceSize(source);
        TxSize = SinkSlack(sink);
        MoveLength = RxSize < TxSize ? RxSize : TxSize;

    /* use drop and flush to handle wrap case */
    #if 1
        VOLATILE BUFFER_INFO_PTR sourceBuffer = &source->streamBuffer.BufferInfo;
        VOLATILE BUFFER_INFO_PTR sinkBuffer   = &sink->streamBuffer.BufferInfo;

        while(MoveLength)
        {
            U32 sourceResidue = sourceBuffer->length - sourceBuffer->ReadOffset;
            U32 sinkResidue   = sinkBuffer->length - sinkBuffer->WriteOffset;

            U32 length = MIN(MoveLength,MIN(sourceResidue,sinkResidue));
            memcpy(sinkBuffer->startaddr[0] + sinkBuffer->WriteOffset,sourceBuffer->startaddr[0] + sourceBuffer->ReadOffset,length);
            SinkFlush(sink, length);
            SourceDrop(source, length);
            MoveLength -= length;
        }
    /* normal case */
    #else
        if(MoveLength)
        {
            result = TRUE;
            BUFFER_INFO_PTR sourceBuffer = &source->streamBuffer.BufferInfo;
            BUFFER_INFO_PTR sinkBuffer   = &sink->streamBuffer.BufferInfo;

            U32 sourceResidue = sourceBuffer->length - sourceBuffer->ReadOffset;
            U32 sinkResidue   = sinkBuffer->length - sinkBuffer->WriteOffset;

            /* source buffer wrap */
            if(MoveLength > sourceResidue)
            {
                /* sink buffer wrap */
                if(MoveLength > sinkResidue)
                {
                    /* sink more residue */
                    if(sinkResidue > sourceResidue)
                    {
                        U32 len1 = sourceResidue;
                        U32 len2 = sinkResidue - len1;
                        U32 len3 = MoveLength - len1 - len2;
                        memcpy(sinkBuffer->startaddr[0] + sinkBuffer->WriteOffset, sourceBuffer->startaddr[0] + sourceBuffer->ReadOffset, len1);
                        memcpy(sinkBuffer->startaddr[0] + sinkBuffer->WriteOffset + len1, sourceBuffer->startaddr[0], len2);
                        memcpy(sinkBuffer->startaddr[0] , sourceBuffer->startaddr[0] + len2, len3);
                    }
                    /* source more residue */
                    else
                    {
                        U32 len1 = sinkResidue;
                        U32 len2 = sourceResidue - len1;
                        U32 len3 = MoveLength - len1 - len2;
                        memcpy(sinkBuffer->startaddr[0] + sinkBuffer->WriteOffset, sourceBuffer->startaddr[0] + sourceBuffer->ReadOffset, len1);
                        memcpy(sinkBuffer->startaddr[0], sourceBuffer->startaddr[0] + sourceBuffer->ReadOffset + len1, len2);
                        memcpy(sinkBuffer->startaddr[0] + len2 , sourceBuffer->startaddr[0], len3);
                    }
                }
                /* sink buffer no wrap */
                else
                {
                    U32 len1 = sourceResidue;
                    U32 len2 = MoveLength - len1;
                    memcpy(sinkBuffer->startaddr[0] + sinkBuffer->WriteOffset, sourceBuffer->startaddr[0] + sourceBuffer->ReadOffset, len1);
                    memcpy(sinkBuffer->startaddr[0] + sinkBuffer->WriteOffset + len1, sourceBuffer->startaddr[0], len2);
                }
            }
            /* souce buffer no wrap */
            else
            {
                /* sink buffer wrap */
                if(MoveLength > sinkResidue)
                {
                    U32 len1 = sinkResidue;
                    U32 len2 = MoveLength - len1;
                    memcpy(sinkBuffer->startaddr[0] + sinkBuffer->WriteOffset, sourceBuffer->startaddr[0] + sourceBuffer->ReadOffset, len1);
                    memcpy(sinkBuffer->startaddr[0] , sourceBuffer->startaddr[0] + sourceBuffer->ReadOffset + len1, len2);
                }
                /* sink buffer no wrap */
                else
                {
                    memcpy(sinkBuffer->startaddr[0] + sinkBuffer->WriteOffset, sourceBuffer->startaddr[0] + sourceBuffer->ReadOffset, MoveLength);
                }
            }
        }
        SinkFlush(sink, MoveLength);
        SourceDrop(source, MoveLength);
    #endif
    result = TRUE;
    }

    return result;
}

BOOL StreamBufferToQueueHandler(SOURCE source, SINK sink)
{
    BOOL result = FALSE;
    U32 RxSize,TxSize,MoveLength;

    if(source && sink)
    {
        RxSize = SourceSize(source);
        TxSize = SinkSlack(sink);
        MoveLength = RxSize < TxSize ? RxSize : TxSize;

        if(MoveLength)
        {
            BUFFER_INFO_PTR sourceBuffer = &source->streamBuffer.BufferInfo;

            U32 SinkOffset = SinkClaim(sink,MoveLength);
            U8* TxBuffer   = SinkMap(sink);
            U32 sourceResidue = sourceBuffer->length - sourceBuffer->ReadOffset;

            /* source buffer wrap */
            if(MoveLength > sourceResidue)
            {
                U32 len1 = sourceResidue;
                U32 len2 = MoveLength - len1;
                memcpy(TxBuffer + SinkOffset, sourceBuffer->startaddr[0] + sourceBuffer->ReadOffset, len1);
                memcpy(TxBuffer + SinkOffset + len1, sourceBuffer->startaddr[0], len2);
            }
            /* source buffer no wrap */
            else
            {
                memcpy(TxBuffer + SinkOffset, sourceBuffer->startaddr[0] + sourceBuffer->ReadOffset, MoveLength);
            }
            SinkFlush(sink, MoveLength);
            SourceDrop(source, MoveLength);
            result = TRUE;
        }
    }
    return result;
}


BOOL StreamDirectHandler(SOURCE source, SINK sink)
{
    U32 size;
    U8* Source_ptr;
    U8* Sink_ptr;

    size = SourceSize(source);
    if(!size)
        return TRUE;

    // Try to get space of the same size for writing.
    if (SinkClaim(sink, size) != 0xffffffff) {
        Sink_ptr = SinkMap(sink);
        Source_ptr = SourceMap(source);
        memcpy(Sink_ptr, Source_ptr, size);
        SinkFlush(sink, size);
        // Clear the input data.
        SourceDrop(source, size);
        return TRUE;
    }

    return FALSE;

}

/**
 * @brief malloc memory for source
 *
 * @param  SourceType   specific which source to malloc
 * @return   pointer of source
 */
SOURCE new_source(SOURCE_TYPE SourceType)
{
    SOURCE source;

    source = pvPortMalloc(sizeof(SOURCE_T));
    if (source)
    {
        memset(source, 0, sizeof(SOURCE_T));
        source->type = SourceType;
        Source_blks[SourceType] = source;
    }
    return source;
}

/**
 * @brief malloc memory for sink
 *
 * @param  SinkType   specific which sink to malloc
 * @return   pointer of sink
 */
SINK new_sink(SINK_TYPE SinkType)
{
    SINK sink;

    sink = pvPortMalloc(sizeof(SINK_T));
    if (sink)
    {
        memset(sink, 0, sizeof(SINK_T));
        sink->type = SinkType;
        Sink_blks[SinkType] = sink;
    }
    return sink;
}

/**
 * @brief Move the specified number of bytes from source to sink.
 *
 * @param sink   The Sink to move data to.
 * @param source The Source to move data from.
 * @param count  The number of bytes to move.
 * @return Zero on failure and the count on success.
 */
U32 StreamMove(SINK sink, SOURCE source, U32 count)
{
    //#warning not implement yet
    UNUSED(sink);
    UNUSED(source);
    UNUSED(count);
    return 0;
}

/**
 * @brief Make an automatic connection between a source and sink
 *
 * @param source The Source data will be taken from.
 * @param sink   The Sink data will be written to.
 *
 * @return a transform on success, or zero on failure.
 */
TRANSFORM StreamConnect(SOURCE source, SINK sink)
{
    TRANSFORM transform = NULL;

    if (source && sink)
    {
        /* check if source and sink have transform*/
        if((source->transform == NULL) && (sink->transform == NULL))
        {
            transform = pvPortMalloc(sizeof(TRANSFORM_T));
            if(transform)
            {
                memset(transform, 0, sizeof(TRANSFORM_T));
                transform->source = source;
                transform->sink = sink;
                source->transform = transform;
                sink->transform = transform;

                dlist_init(&transform->list);
                dlist_append(&transform->list, &gTransformList);

                /* assign handler */
                if ((source->buftype == BUFFER_TYPE_CIRCULAR_BUFFER) && (sink->buftype == BUFFER_TYPE_CIRCULAR_BUFFER))
                {
                    transform->Handler = StreamBufferToBufferHandler;
                }
                else if ((source->buftype == BUFFER_TYPE_CIRCULAR_BUFFER) && (sink->buftype == BUFFER_TYPE_QUEUE))
                {
                    transform->Handler = StreamBufferToQueueHandler;
                }
                else if ((source->buftype == BUFFER_TYPE_INTERLEAVED_BUFFER) && (sink->buftype == BUFFER_TYPE_INTERLEAVED_BUFFER))
                {
                    transform->Handler = StreamBufferToQueueHandler;
                }
                else if ((source->buftype == BUFFER_TYPE_INTERLEAVED_BUFFER) && (sink->buftype == BUFFER_TYPE_QUEUE))
                {
                    transform->Handler = StreamBufferToQueueHandler;
                }
                else if ((source->buftype == BUFFER_TYPE_QUEUE) && (sink->buftype == BUFFER_TYPE_CIRCULAR_BUFFER))
                {
                    /* do we have this case? */
                }
                else
                {
                    transform->Handler = StreamDirectHandler;
                }
            }
        }
        else if((source->transform == sink->transform))
        {
            transform = source->transform;
        }
    }
    return transform;
}

/**
 * @brief Break transform of source and sink
 *
 * @param transform the transform to break
 */
VOID StreamDisconnect(TRANSFORM transform)
{
    if(transform)
    {
        if((transform->source)&&(transform->sink))
        {
            if(transform == transform->source->transform)
            {
                if (transform->TransformClose != NULL)
                {
                    PL_CRITICAL(TransformChangeHandlerClose, (VOID*)transform);
                }
                else
                {
                    StreamTransformClose(transform);
                }
            }
        }
    }
}

/**
 * @brief Request to create an audio source
 * @param hardware The audio hardware which would be reserved as a source
 * @param instance The audio hardware instance (meaning depends on \e hardware)
 * @param channel The audio channel (meaning depends on \e hardware)
 *
 * @return The Source ID associated with the audio hardware.
 */
SOURCE StreamAudioAfeSource(audio_hardware hardware, audio_instance instance , audio_channel channel)
{
    UNUSED(instance);

    // TODO: line in and eSCO input selection
    Source_Audio_SelectInstance(hardware, instance);

    SOURCE source;
    if ((Source_blks[SOURCE_TYPE_AUDIO] != NULL)
        &&(Source_blks[SOURCE_TYPE_AUDIO]->param.audio.HW_type == hardware)
        &&(Source_blks[SOURCE_TYPE_AUDIO]->param.audio.channel_sel == channel))
    {
        return Source_blks[SOURCE_TYPE_AUDIO];
    }
    else if (Source_blks[SOURCE_TYPE_AUDIO] == NULL)
    {
        source = new_source(SOURCE_TYPE_AUDIO);
        if (source == NULL)
        return NULL;
    }
    else
    {
        source = Source_blks[SOURCE_TYPE_AUDIO];
    }

    source->param.audio.HW_type     = hardware;
    source->param.audio.channel_sel = channel;
    source->taskId     = DAV_TASK_ID;
    source->buftype                   = BUFFER_TYPE_INTERLEAVED_BUFFER;

    Audio_Default_setting_init(); // Temp for audio test
    Source_Audio_Get_Default_Parameters(source);

    audio_afe_set_ops(source);
    Source_Audio_HW_Init(source);
    #ifndef HAL_AUDIO_ENABLE_PATH_MEM_DEVICE
    if (source->param.audio.audio_device == HAL_AUDIO_DEVICE_I2S_SLAVE) {
        source->param.audio.AfeBlkControl.u4asrcflag = true;
    }
    #endif
    Source_Audio_Path_Init(source);
    Source_Audio_Afe_Path_Interface_Init(source);

    return source;
}

/**
 * @brief Request to create an audio sub-source
 * @param hardware The audio hardware which would be reserved as a source
 * @param instance The audio hardware instance (meaning depends on \e hardware)
 * @param channel The audio channel (meaning depends on \e hardware)
 *
 * @return The Source ID associated with the audio hardware.
 */
SOURCE StreamAudioAfeSubSource(audio_hardware hardware, audio_instance instance , audio_channel channel)
{
    SOURCE source = NULL;
    UNUSED(instance);
    UNUSED(hardware);
    UNUSED(channel);
#ifdef MTK_MULTI_MIC_STREAM_ENABLE
    // TODO: line in and eSCO input selection
    Source_Audio_SelectInstance(hardware, instance);

    SOURCE_TYPE      source_type;
    for (source_type = SOURCE_TYPE_SUBAUDIO_MIN ; source_type<=SOURCE_TYPE_SUBAUDIO_MAX ; source_type++) {
        if (Source_blks[source_type] == NULL) {
            source = new_source(source_type);
            break;
        }
    }

    if (source == NULL) {
        return NULL;
    }

    source->param.audio.HW_type     = hardware;
    source->param.audio.channel_sel = channel;
    source->taskId                  = DAV_TASK_ID;
    source->buftype                 = BUFFER_TYPE_INTERLEAVED_BUFFER;

    Audio_Default_setting_init(); // Temp for audio test
    Source_Audio_Get_Default_Parameters(source);

    audio_afe_set_ops(source);
    Source_Audio_HW_Init(source);
    #ifndef HAL_AUDIO_ENABLE_PATH_MEM_DEVICE
    if (source->param.audio.audio_device == HAL_AUDIO_DEVICE_I2S_SLAVE) {
        source->param.audio.AfeBlkControl.u4asrcflag = true;
    }
    #endif
    Source_Audio_Path_Init(source);
    Source_Audio_Afe_Path_Interface_Init(source);
#endif
    return source;
}


/**
 * @brief Request to create an audio source
 * @param hardware The audio hardware which would be reserved as a source
 * @param instance The audio hardware instance (meaning depends on \e hardware)
 * @param channel The audio channel (meaning depends on \e hardware)
 *
 * @return The Source ID associated with the audio hardware.
 */
SOURCE StreamAudioSource(audio_hardware hardware, audio_instance instance , audio_channel channel)
{
    UNUSED(instance);

    Source_Audio_SelectInstance(hardware, instance);

    SOURCE source;
    if ((Source_blks[SOURCE_TYPE_AUDIO] != NULL)
        &&(Source_blks[SOURCE_TYPE_AUDIO]->param.audio.HW_type == hardware)
        &&(Source_blks[SOURCE_TYPE_AUDIO]->param.audio.channel_sel == channel))
    {
        return Source_blks[SOURCE_TYPE_AUDIO];
    }
    else if (Source_blks[SOURCE_TYPE_AUDIO] == NULL)
    {
        source = new_source(SOURCE_TYPE_AUDIO);
        if (source == NULL)
        return NULL;
    }
    else
    {
        source = Source_blks[SOURCE_TYPE_AUDIO];
    }

    source->param.audio.HW_type     = hardware;
    source->param.audio.channel_sel = channel;
    source->taskId     = DAV_TASK_ID;

    Audio_Default_setting_init(); // Temp for audio test

    Source_Audio_HW_Init(source);
    Source_Audio_Path_Init(source);
    Source_Audio_Path_Interface_Init(source);

    return source;
}
/**
 * @brief Request to create an audio sink afe
 * @param hardware The audio hardware which would be reserved as a sink
 * @param instance The audio hardware instance (meaning depends on \e hardware)
 * @param channel The audio channel (meaning depends on \e hardware)
 *
 * @return The Sink ID associated with the audio hardware.
 */
SINK StreamAudioAfeSink(audio_hardware hardware, audio_instance instance, audio_channel channel)
{
    UNUSED(instance);
    SINK sink;
    if ((Sink_blks[SINK_TYPE_AUDIO] != NULL)
        &&(Sink_blks[SINK_TYPE_AUDIO]->param.audio.HW_type == hardware)
        &&(Sink_blks[SINK_TYPE_AUDIO]->param.audio.channel_sel == channel))
    {
        return Sink_blks[SINK_TYPE_AUDIO];
    }
    else if (Sink_blks[SINK_TYPE_AUDIO] == NULL)
    {
        sink = new_sink(SINK_TYPE_AUDIO);

        if (sink == NULL)
        return NULL;

    }
    else
    {
        return NULL;
    }

    sink->param.audio.HW_type       = hardware;
    sink->param.audio.channel_sel   = channel;
    sink->taskid                    = DAV_TASK_ID;
    sink->type                      = SINK_TYPE_AUDIO;
    sink->buftype                   = BUFFER_TYPE_INTERLEAVED_BUFFER;//BUFFER_TYPE_INTERLEAVED_BUFFER; BUFFER_TYPE_CIRCULAR_BUFFER

    Sink_Audio_Get_Default_Parameters(sink);
    Audio_Default_setting_init(); // Temp for audio test

    audio_afe_set_ops(sink);
    Sink_Audio_HW_Init_AFE(sink);

#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
    #ifndef ENABLE_HWSRC_CLKSKEW
    if ((sink->param.audio.rate != sink->param.audio.src_rate)&&(sink->param.audio.with_sink_src == false))//modify for HWSRC power measuring
    #else
    if (((sink->param.audio.rate != sink->param.audio.src_rate)&&(sink->param.audio.with_sink_src == false)) || (ClkSkewMode_g == CLK_SKEW_V2))//modify for HWSRC power measuring
    #endif /*ENABLE_HWSRC_CLKSKEW*/
    {
        sink->param.audio.AfeBlkControl.u4asrcflag = true;//Enable DL1 asrc //modify for asrc
        sink->param.audio.AfeBlkControl.u4asrc_buffer_size= ASRC_OUT_BUFFER_SIZE;//modify for asrc
        #ifdef HAL_AUDIO_ENABLE_PATH_MEM_DEVICE
        sink->param.audio.mem_handle.pure_agent_with_src = true;//modify for ab1568
        #endif
    }
#endif /*ENABLE_HWSRC_ON_MAIN_STREAM*/

    Sink_Audio_Path_Init_AFE(sink);
    Sink_Audio_Afe_Path_Interface_Init(sink);

    return sink;
}

#ifdef MTK_PROMPT_SOUND_ENABLE
/**
 * @brief Request to create an audio sink afe for VP
 * @param hardware The audio hardware which would be reserved as a sink
 * @param instance The audio hardware instance (meaning depends on \e hardware)
 * @param channel The audio channel (meaning depends on \e hardware)
 *
 * @return The Sink ID associated with the audio hardware.
 */
SINK StreamAudioAfe2Sink(audio_hardware hardware, audio_instance instance, audio_channel channel)
{
    UNUSED(instance);
    SINK sink;
    if ((Sink_blks[SINK_TYPE_VP_AUDIO] != NULL)
        &&(Sink_blks[SINK_TYPE_VP_AUDIO]->param.audio.HW_type == hardware)
        &&(Sink_blks[SINK_TYPE_VP_AUDIO]->param.audio.channel_sel == channel))
    {
        return Sink_blks[SINK_TYPE_VP_AUDIO];
    }
    else if (Sink_blks[SINK_TYPE_VP_AUDIO] == NULL)
    {
        sink = new_sink(SINK_TYPE_VP_AUDIO);

        if (sink == NULL)
        return NULL;

    }
    else
    {
        return NULL;
    }

    sink->param.audio.HW_type       = hardware;
    sink->param.audio.channel_sel   = channel;
    sink->taskid                    = DAV_TASK_ID; ///TODO: need check task ID later
    sink->type                      = SINK_TYPE_VP_AUDIO;
    sink->buftype                   = BUFFER_TYPE_INTERLEAVED_BUFFER;

    Sink_Audio_Get_Default_Parameters(sink);
    Audio_Default_setting_init(); // Temp for audio test

    //audio_afe2_set_sink_ops(hal_audio_get_stream_out_device());
    audio_afe_set_ops(sink);

    Sink_Audio_HW_Init_AFE(sink);
    if (sink->param.audio.with_sink_src == false){
        sink->param.audio.AfeBlkControl.u4asrcflag = true;//Enable DL2 asrc
        #ifdef HAL_AUDIO_ENABLE_PATH_MEM_DEVICE
        sink->param.audio.mem_handle.pure_agent_with_src = true;//modify for ab1568
        #endif
    }

#ifdef ENABLE_HWSRC_ON_MAIN_STREAM
    sink->param.audio.AfeBlkControl.u4asrc_buffer_size= ASRC_OUT_BUFFER_SIZE;
#endif

    Sink_Audio_Path_Init_AFE(sink);
    Sink_Audio_Afe_Path_Interface_Init(sink);

	DSP_MW_LOG_I("StreamAudioAfe2Sink done\r\n", 0);

    return sink;
}
#endif

/**
 * @brief Request to create an audio sink
 * @param hardware The audio hardware which would be reserved as a sink
 * @param instance The audio hardware instance (meaning depends on \e hardware)
 * @param channel The audio channel (meaning depends on \e hardware)
 *
 * @return The Sink ID associated with the audio hardware.
 */
SINK StreamAudioSink(audio_hardware hardware, audio_instance instance, audio_channel channel)
{
    UNUSED(instance);
    SINK sink;
    if ((Sink_blks[SINK_TYPE_AUDIO] != NULL)
        &&(Sink_blks[SINK_TYPE_AUDIO]->param.audio.HW_type == hardware)
        &&(Sink_blks[SINK_TYPE_AUDIO]->param.audio.channel_sel == channel))
    {
        return Sink_blks[SINK_TYPE_AUDIO];
    }
    else if (Sink_blks[SINK_TYPE_AUDIO] == NULL)
    {
        sink = new_sink(SINK_TYPE_AUDIO);

        if (sink == NULL)
        return NULL;

    }
    else
    {
        return NULL;
    }

    sink->param.audio.HW_type       = hardware;
    sink->param.audio.channel_sel   = channel;
    sink->taskid                    = DAV_TASK_ID;

    Audio_Default_setting_init(); // Temp for audio test

    Sink_Audio_HW_Init(sink);
    Sink_Audio_Path_Init(sink);
    Sink_Audio_Path_Interface_Init(sink);

    return sink;
}
/**
 * @brief Request to create an audio aux path sink
 * @this sink is an auxiliary path for StreamAudioSink which shared the same audio output
 * @return The Sink ID associated with the audio hardware.
 */
SINK StreamVPPathSink(audio_hardware hardware , audio_instance instance)
{
    UNUSED(instance);
    SINK sink;
    if ((Sink_blks[SINK_TYPE_VP_AUDIO] != NULL)
        &&(Sink_blks[SINK_TYPE_VP_AUDIO]->param.audio.HW_type == hardware))
    {
        return Sink_blks[SINK_TYPE_VP_AUDIO];
    }
    else if (Sink_blks[SINK_TYPE_VP_AUDIO] == NULL)
    {
        sink = new_sink(SINK_TYPE_VP_AUDIO);

        if (sink == NULL)
        return NULL;
    }
    else
    {
        sink = Sink_blks[SINK_TYPE_VP_AUDIO];
    }
    sink->param.audio.HW_type     = hardware;
    sink->param.audio.channel_sel = AUDIO_CHANNEL_VP;
    sink->taskid                  = DPR_TASK_ID;

    Audio_Default_setting_init(); // Temp for audio test

    if (Audio_Status.audio_in_use == 0)
    {
        Sink_Audio_HW_Init(sink);
    }
    Sink_Audio_SubPath_Init(sink);
    Sink_Audio_Path_Interface_Init(sink);

    return sink;
}


/**
 * @brief Request to create a sink which is joint to transform
 * @param transform The audio transform to connect
 * @param channel The audio channel (meaning depends on \e hardware)
 *
 * @return The Sink ID associated with the audio hardware.
 */
SINK StreamJointSink(TRANSFORM transform, audio_channel channel)
{
    SINK sink;
    if ((Sink_blks[SINK_TYPE_DSP_JOINT_0] != NULL)
        &&(Sink_blks[SINK_TYPE_DSP_JOINT_0]->param.dsp.transform == transform)
        &&(Sink_blks[SINK_TYPE_DSP_JOINT_0]->param.audio.channel_sel == channel))
    {
        return Sink_blks[SINK_TYPE_DSP_JOINT_0];
    }
    else if ((Sink_blks[SINK_TYPE_DSP_JOINT_1] != NULL)
             &&(Sink_blks[SINK_TYPE_DSP_JOINT_1]->param.dsp.transform == transform)
             &&(Sink_blks[SINK_TYPE_DSP_JOINT_1]->param.audio.channel_sel == channel))
    {
        return Sink_blks[SINK_TYPE_DSP_JOINT_1];
    }
    else if (Sink_blks[SINK_TYPE_DSP_JOINT_0] == NULL)
    {
        sink = new_sink(SINK_TYPE_DSP_JOINT_0);

        if (sink == NULL)
        return NULL;
    }
    else if (Sink_blks[SINK_TYPE_DSP_JOINT_1] == NULL)
    {
        sink = new_sink(SINK_TYPE_DSP_JOINT_1);

        if (sink == NULL)
        return NULL;
    }
    else
    {
        sink = Sink_blks[SINK_TYPE_DSP_JOINT_0];
    }


    sink->param.dsp.channel_sel = channel;
    sink->param.dsp.frame_size  = transform->sink->param.audio.frame_size;
    sink->param.dsp.channel_num = transform->sink->param.audio.channel_num;
    sink->taskid                = DAV_TASK_ID;
    sink->type                  = SINK_TYPE_DSP_JOINT;
    sink->buftype               = BUFFER_TYPE_CIRCULAR_BUFFER;
    sink->transform             = NULL;

    Sink_Audio_Buffer_Ctrl(sink, TRUE);
    Sink_Audio_Path_Interface_Init(sink);
    sink->param.dsp.transform     = transform;
    return sink;
}

/**
 * @brief Request to create a virtual sink which can report result
 * @param entry The function entry which  will be call when Flush sink
 * @param report_length The length of report
 *
 * @return The Sink ID associated with the audio hardware.
 */
SINK StreamVirtualSink(VOID* entry, U32 report_length)
{
    SINK sink;
    U8* mem_ptr;

    if (Sink_blks[SINK_TYPE_DSP_VIRTUAL] == NULL) {
        sink = new_sink(SINK_TYPE_DSP_VIRTUAL);

    } else {
        sink = Sink_blks[SINK_TYPE_DSP_VIRTUAL];
        sink->param.virtual_para.user_count++;
        DSP_MW_LOG_I("DSP stream VirtualSink :%d", 1, sink->param.virtual_para.user_count);
        return sink;
    }

    if (sink == NULL) {
        return NULL;
    }

    sink->taskid                  = DAV_TASK_ID;
    sink->buftype                 = BUFFER_TYPE_CIRCULAR_BUFFER;
    sink->transform               = NULL;
    sink->param.virtual_para.entry     = entry;
    sink->param.virtual_para.mem_size  = report_length;
    sink->param.virtual_para.user_count = 1;

    if (report_length) {
        mem_ptr = DSPMEM_tmalloc(sink->taskid, report_length, (VOID*)sink);
        if (mem_ptr != NULL)
        {
            memset(mem_ptr, 0, report_length);
            sink->streamBuffer.BufferInfo.startaddr[0] = mem_ptr;
        }
    }
    Sink_VirtualSink_Interface_Init(sink);
    return sink;
}

/**
 * @brief Request to create a source which is branched from transform
 * @param transform The audio transform to connect
 * @param instance The audio hardware instance (meaning depends on \e hardware)
 * @param channel The audio channel (meaning depends on \e hardware)
 *
 * @return The Source ID associated with the audio hardware.
 */
SOURCE StreamBranchSource(TRANSFORM transform, audio_channel channel)
{
    SOURCE source;
    if ((Source_blks[SOURCE_TYPE_DSP_BRANCH_0] != NULL)
        &&(Source_blks[SOURCE_TYPE_DSP_BRANCH_0]->param.dsp.transform == transform)
        &&(Source_blks[SOURCE_TYPE_DSP_BRANCH_0]->param.audio.channel_sel == channel))
    {
        return Source_blks[SOURCE_TYPE_DSP_BRANCH_0];
    }
    else if ((Source_blks[SOURCE_TYPE_DSP_BRANCH_1] != NULL)
        &&(Source_blks[SOURCE_TYPE_DSP_BRANCH_1]->param.dsp.transform == transform)
        &&(Source_blks[SOURCE_TYPE_DSP_BRANCH_1]->param.audio.channel_sel == channel))
    {
        return Source_blks[SOURCE_TYPE_DSP_BRANCH_1];
    }
    else if (Source_blks[SOURCE_TYPE_DSP_BRANCH_0] == NULL)
    {
        source = new_source(SOURCE_TYPE_DSP_BRANCH_0);
        if (source == NULL)
            return NULL;
    }
    else if (Source_blks[SOURCE_TYPE_DSP_BRANCH_1] == NULL)
    {
        source = new_source(SOURCE_TYPE_DSP_BRANCH_1);
        if (source == NULL)
            return NULL;
    }
    else
    {
        source = Source_blks[SOURCE_TYPE_DSP_BRANCH_0];
    }


    source->param.dsp.channel_sel = channel;
    source->param.dsp.frame_size  = transform->sink->param.audio.frame_size;
    source->param.dsp.channel_num = transform->sink->param.audio.channel_num;
    source->taskId                = DAV_TASK_ID;//DSP_CallbackTask_Get(transform->source, transform->sink);//DAV_TASK_ID;
    source->buftype               = BUFFER_TYPE_CIRCULAR_BUFFER;
    source->type                  = SOURCE_TYPE_DSP_BRANCH;
    source->transform             = NULL;

    Source_Audio_Buffer_Ctrl(source,TRUE);
    Source_Audio_Path_Interface_Init(source);
    source->param.dsp.transform   = transform;
    return source;
}




/**
 * @breif init stream system
 */
VOID Stream_Init(VOID)
{
    TransformList_Init();
}

VOID StreamTransformClose(VOID* pTransform)
{
    TRANSFORM transform = (TRANSFORM)pTransform;
    dlist_remove(&transform->list);
    transform->source->transform = NULL;
    transform->sink->transform = NULL;
    vPortFree(transform);
}


SINK StreamN9ScoSink(void *param)
{
    SINK sink;
    if (Sink_blks[SINK_TYPE_N9SCO] != NULL)
    {
        return Sink_blks[SINK_TYPE_N9SCO];
    }
    sink = new_sink(SINK_TYPE_N9SCO);
    if (sink == NULL)
    {
        return NULL;
    }
    sink->taskid = DAV_TASK_ID;
    sink->param.n9sco.share_info_base_addr = param;

    SinkInitN9Sco(sink);

    return sink;
}


SOURCE StreamN9ScoSource(void *param)
{
    SOURCE source;

    if (Source_blks[SOURCE_TYPE_N9SCO] != NULL)
    {
        return Source_blks[SOURCE_TYPE_N9SCO];
    }

    source = new_source(SOURCE_TYPE_N9SCO);
    if (source == NULL)
    {
        return NULL;
    }
    source->taskId = DAV_TASK_ID;
    source->param.n9sco.share_info_base_addr = param;

    SourceInitN9Sco(source);

    return source;
}

SOURCE StreamMemorySource(U8* Memory_addr, U32 Memory_len)
{
    SOURCE source;

    if (Source_blks[SOURCE_TYPE_MEMORY] != NULL)
    {
        return Source_blks[SOURCE_TYPE_MEMORY];
    }

    source = new_source(SOURCE_TYPE_MEMORY);
    if (source)
    {
        source->streamBuffer.BufferInfo.startaddr[0]   = Memory_addr;
        source->streamBuffer.BufferInfo.length         = Memory_len;
        SourceInit_Memory(source);
    }

    return source;
}

SINK StreamMemorySink(U8* Memory_addr, U32 Memory_len)
{
    SINK sink;

    if (Sink_blks[SINK_TYPE_MEMORY] != NULL)
    {
        return Sink_blks[SINK_TYPE_MEMORY];
    }

    sink = new_sink(SINK_TYPE_MEMORY);
    if (sink)
    {
        sink->streamBuffer.BufferInfo.startaddr[0]   = Memory_addr;
        sink->streamBuffer.BufferInfo.length         = Memory_len;
        sink->param.memory.memory_type = (((U32)Memory_addr>>24) == 0x41) ? Flash_addr : Ram_addr;
        sink->param.memory.remain_len = 0;
        sink->taskid = DAV_TASK_ID;
        SinkInit_Memory(sink);
    }

    return sink;
}

#ifdef MTK_SENSOR_SOURCE_ENABLE
SOURCE StreamGsensorSource(void)
{
    SOURCE source = NULL;
    if (Source_blks[SOURCE_TYPE_GSENSOR] != NULL)
    {
        return Source_blks[SOURCE_TYPE_GSENSOR];
    }
    source = new_source(SOURCE_TYPE_GSENSOR);
    if (source)
    {
        SourceInit_GSENSOR(source);
    }
    return source;
}

SOURCE StreamGsensorSink(void *param)
{
    SINK sink = NULL;
    data_ul_open_param_p data_ul_open;

    if (Sink_blks[SINK_TYPE_GSENSOR] != NULL) {
        return Sink_blks[SINK_TYPE_GSENSOR];
    }
    sink = new_sink(SINK_TYPE_GSENSOR);
    if (sink) {
        data_ul_open = (data_ul_open_param_p)param;
        sink->taskid = DAV_TASK_ID; // TBD
        sink->param.data_ul.share_info_base_addr = (SHARE_BUFFER_INFO_PTR)hal_memview_cm4_to_dsp0((U32)data_ul_open->p_share_info);
        sink->param.data_ul.share_info_base_addr->bBufferIsFull = false;
        sink->param.data_ul.payload_size = data_ul_open->payload_size;
        sink->param.data_ul.scenario_type = 3;
        sink->param.data_ul.scenario_id = 1;
        SinkInit_GSENSOR(sink);
    }
    return sink;
}

#endif

SOURCE StreamN9A2dpSource(void *param)
{
    SOURCE source;

    // printf("0x%08X\r\n", (uint32_t)param);

    if ( Source_blks[SOURCE_TYPE_A2DP] ) {
        return Source_blks[SOURCE_TYPE_A2DP];
    }
    source = new_source(SOURCE_TYPE_A2DP);

    if ( source != NULL ) {
    // printf("0x%08X\r\n", (uint32_t)&(source->param.n9_a2dp.codec_info));
    // printf("0x%08X\r\n", (uint32_t)&(source->param.n9_a2dp.share_info_base_addr));
    memcpy(&(source->param.n9_a2dp.codec_info), param, sizeof(bt_codec_a2dp_audio_t));
    memcpy(&(source->param.n9_a2dp.share_info_base_addr), (param + sizeof(bt_codec_a2dp_audio_t)), 4);
    #ifdef MTK_BT_AVM_SHARE_BUF
    memcpy(&(source->streamBuffer.AVMBufferInfo), (U8*)source->param.n9_a2dp.share_info_base_addr, 36);/* share info fix 36 byte */
    source->param.n9_a2dp.pcdc_info_buf = ((bt_a2dp_open_param_p)param)->p_pcdc_anchor_info_buf;
    #else
    memcpy(&(source->streamBuffer.ShareBufferInfo), (U8*)source->param.n9_a2dp.share_info_base_addr, 32);/* share info fix 32 byte */
    memcpy(&(source->param.n9_a2dp.asi_buf), (param + sizeof(bt_codec_a2dp_audio_t))+ 4, 4);
    memcpy(&(source->param.n9_a2dp.min_gap_buf), (param + sizeof(bt_codec_a2dp_audio_t))+ 4 + 4, 4);
    #endif

    memcpy(&(source->param.n9_a2dp.a2dp_bitrate_report.p_a2dp_bitrate_report), (param + sizeof(bt_codec_a2dp_audio_t))+ 4 + 4 + 4, 4);
    memcpy(&(source->param.n9_a2dp.sink_latency), (param + sizeof(bt_codec_a2dp_audio_t))+ 4 + 4 + 4 + 4, 4);
    memcpy(&(source->param.n9_a2dp.a2dp_lostnum_report), (param + sizeof(bt_codec_a2dp_audio_t))+ 4 + 4 + 4 + 4 +4 + 4 + 4, 4);

    source->param.n9_a2dp.sync_info = (AUDIO_SYNC_INFO*)hal_memview_cm4_to_dsp0(((bt_a2dp_open_param_p)param)->p_audio_sync_info);

    #ifdef MTK_BT_AVM_SHARE_BUF
    source->streamBuffer.AVMBufferInfo.StartAddr = hal_memview_cm4_to_dsp0(source->streamBuffer.AVMBufferInfo.StartAddr);
    #else

    source->streamBuffer.ShareBufferInfo.startaddr = hal_memview_cm4_to_dsp0(source->streamBuffer.ShareBufferInfo.startaddr);
    memset(source->param.n9_a2dp.a2dp_lostnum_report,0,8);
    #endif
    DSP_MW_LOG_I("share_info_base_addr: 0x%08X 0x%08X 0x%08X\r\n", 3, source->param.n9_a2dp.share_info_base_addr,source->param.n9_a2dp.asi_buf,source->param.n9_a2dp.a2dp_bitrate_report.p_a2dp_bitrate_report);
        SourceInit_N9_a2dp(source);
    }
    else {
        return NULL;
    }

    return source;
}

SOURCE StreamCM4PlaybackSource(void *param)
{
    #ifdef MTK_CM4_PLAYBACK_ENABLE
    cm4_playback_share_info_t *info_from_cm4 = (cm4_playback_share_info_t *)param;

    SOURCE source;
    U32 sample_rate;

    if ( Source_blks[SOURCE_TYPE_CM4_PLAYBACK] ) {
        return Source_blks[SOURCE_TYPE_CM4_PLAYBACK];
    }

    source = new_source(SOURCE_TYPE_CM4_PLAYBACK);
    if ( source != NULL ) {

        switch (info_from_cm4->sampling_rate) {
            case AUDIO_SAMPLING_RATE_8KHZ:
                sample_rate = 8000;
                break;

            case AUDIO_SAMPLING_RATE_11_025KHZ:
                sample_rate = 11025;
                break;

            case AUDIO_SAMPLING_RATE_12KHZ:
                sample_rate = 12000;
                break;

            case AUDIO_SAMPLING_RATE_16KHZ:
                sample_rate = 16000;
                break;

            case AUDIO_SAMPLING_RATE_22_05KHZ:
                sample_rate = 22050;
                break;

            case AUDIO_SAMPLING_RATE_24KHZ:
                sample_rate = 24000;
                break;

            case AUDIO_SAMPLING_RATE_32KHZ:
                sample_rate = 32000;
                break;

            case AUDIO_SAMPLING_RATE_44_1KHZ:
                sample_rate = 44100;
                break;

            case AUDIO_SAMPLING_RATE_48KHZ:
                sample_rate = 48000;
                break;

            case AUDIO_SAMPLING_RATE_96KHZ:
                sample_rate = 96000;
                break;

            default:
                sample_rate = 16000;
                DSP_MW_LOG_I("[CM4_PB] Unknown FS", 0);
        }

        source->param.cm4_playback.info.bit_type = (audio_bits_per_sample_t)info_from_cm4->bit_type;
        source->param.cm4_playback.info.sampling_rate = sample_rate;
        source->param.cm4_playback.info.channel_number = (audio_channel_number_t)info_from_cm4->channel_number;
        source->param.cm4_playback.info.codec_type = info_from_cm4->codec_type;
        source->param.cm4_playback.info.share_info_base_addr = hal_memview_cm4_to_dsp0(info_from_cm4->share_info_base_addr);
        source->param.cm4_playback.info.source_channels
            = (AUDIO_MONO == source->param.cm4_playback.info.channel_number) ? 1 : 2;

        DSP_MW_LOG_I("[CM4_PB] info_from_cm4 = 0x%x", 1, param);
        DSP_MW_LOG_I("[CM4_PB] bit_type = %d", 1, info_from_cm4->bit_type);
        DSP_MW_LOG_I("[CM4_PB] sampling_rate = %d", 1, info_from_cm4->sampling_rate);
        DSP_MW_LOG_I("[CM4_PB] channel_number = %d", 1, info_from_cm4->channel_number);
        DSP_MW_LOG_I("[CM4_PB] codec_type = %d", 1, info_from_cm4->codec_type);
        DSP_MW_LOG_I("[CM4_PB] share_info_base_addr = 0x%x", 1, info_from_cm4->share_info_base_addr);

        SHARE_BUFFER_INFO *share_buff_info  = (SHARE_BUFFER_INFO*)source->param.cm4_playback.info.share_info_base_addr;
        UNUSED(share_buff_info);
        DSP_MW_LOG_I("[CM4_PB] wo = %x", 1, share_buff_info->WriteOffset);

        source->taskId = DAV_TASK_ID; // Yo: Should switch to DPR_TASK_ID after applying VP afe sink

        SourceInit_CM4_playback(source);
    }
    else {
        return NULL;
    }

    return source;

    #else
    DSP_MW_LOG_I("[CM4_PB] Source is not supported ", 0);

    return NULL;
    #endif
}

#ifdef MTK_PROMPT_SOUND_ENABLE
SOURCE StreamCM4VPPlaybackSource(void *param)
{
    cm4_playback_share_info_t *info_from_cm4 = (cm4_playback_share_info_t *)param;

    SOURCE source;
    U32 sample_rate;

    if ( Source_blks[SOURCE_TYPE_CM4_VP_PLAYBACK] ) {
        return Source_blks[SOURCE_TYPE_CM4_VP_PLAYBACK];
    }

    source = new_source(SOURCE_TYPE_CM4_VP_PLAYBACK);
    if ( source != NULL ) {

        switch (info_from_cm4->sampling_rate) {
            case AUDIO_SAMPLING_RATE_8KHZ:
                sample_rate = 8000;
                break;

            case AUDIO_SAMPLING_RATE_11_025KHZ:
                sample_rate = 11025;
                break;

            case AUDIO_SAMPLING_RATE_12KHZ:
                sample_rate = 12000;
                break;

            case AUDIO_SAMPLING_RATE_16KHZ:
                sample_rate = 16000;
                break;

            case AUDIO_SAMPLING_RATE_22_05KHZ:
                sample_rate = 22050;
                break;

            case AUDIO_SAMPLING_RATE_24KHZ:
                sample_rate = 24000;
                break;

            case AUDIO_SAMPLING_RATE_32KHZ:
                sample_rate = 32000;
                break;

            case AUDIO_SAMPLING_RATE_44_1KHZ:
                sample_rate = 44100;
                break;

            case AUDIO_SAMPLING_RATE_48KHZ:
                sample_rate = 48000;
                break;

            case AUDIO_SAMPLING_RATE_96KHZ:
                sample_rate = 96000;
                break;

            default:
                sample_rate = 16000;
                DSP_MW_LOG_I("[CM4_VP_PB] Unknown FS", 0);
        }

        source->param.cm4_playback.info.bit_type = (audio_bits_per_sample_t)info_from_cm4->bit_type;
        source->param.cm4_playback.info.sampling_rate = sample_rate;
        source->param.cm4_playback.info.channel_number = (audio_channel_number_t)info_from_cm4->channel_number;
        source->param.cm4_playback.info.codec_type = info_from_cm4->codec_type;
        source->param.cm4_playback.info.share_info_base_addr = hal_memview_cm4_to_dsp0(info_from_cm4->share_info_base_addr);
        source->param.cm4_playback.info.source_channels
            = (AUDIO_MONO == source->param.cm4_playback.info.channel_number) ? 1 : 2;

        DSP_MW_LOG_I("[CM4_VP_PB] info_from_cm4 = 0x%x", 1, param);
        DSP_MW_LOG_I("[CM4_VP_PB] bit_type = %d", 1, info_from_cm4->bit_type);
        DSP_MW_LOG_I("[CM4_VP_PB] sampling_rate = %d", 1, info_from_cm4->sampling_rate);
        DSP_MW_LOG_I("[CM4_VP_PB] channel_number = %d", 1, info_from_cm4->channel_number);
        DSP_MW_LOG_I("[CM4_VP_PB] codec_type = %d", 1, info_from_cm4->codec_type);
        DSP_MW_LOG_I("[CM4_VP_PB] share_info_base_addr = 0x%x", 1, info_from_cm4->share_info_base_addr);

        SHARE_BUFFER_INFO *share_buff_info  = (SHARE_BUFFER_INFO*)source->param.cm4_playback.info.share_info_base_addr;
        UNUSED(share_buff_info);
        DSP_MW_LOG_I("[CM4_VP_PB] wo = %x", 1, share_buff_info->WriteOffset);

        source->taskId = DAV_TASK_ID; // Yo: Should switch to DPR_TASK_ID after applying VP afe sink

        SourceInit_CM4_vp_playback(source);
    }
    else {
        return NULL;
    }

    return source;

}
#endif

/**
 * StreamCm4RecordSink
 * @breif record data to share buffer
 */
SINK StreamCm4RecordSink(void *param)
{
    SINK sink = NULL;
    #ifdef MTK_CM4_RECORD_ENABLE
    cm4_record_open_param_p record_open;
    if (Sink_blks[SINK_TYPE_CM4RECORD] != NULL)
    {
        return Sink_blks[SINK_TYPE_CM4RECORD];
    }
    sink = new_sink(SINK_TYPE_CM4RECORD);
    if (sink == NULL)
    {
        return NULL;
    }
    record_open = (cm4_record_open_param_p)param;

    #ifdef ENABLE_2A2D_TEST
    sink->taskid = DHP_TASK_ID;
    #else
    sink->taskid = DAV_TASK_ID;
    #endif
    sink->param.cm4_record.share_info_base_addr = (SHARE_BUFFER_INFO_PTR)hal_memview_cm4_to_dsp0((U32)record_open->p_share_info);
    sink->param.cm4_record.frames_per_message = record_open->frames_per_message;
    sink->param.cm4_record.bitrate = record_open->bitrate;
    sink->param.cm4_record.interleave = record_open->interleave;
    SinkInitCm4Record(sink);
    #endif
    return sink;
}

SINK StreamDataULSink(void *param)
{
    SINK sink = NULL;
    data_ul_open_param_p data_ul_open;
    if (Sink_blks[SINK_TYPE_DATAUL] != NULL)
    {
        return Sink_blks[SINK_TYPE_DATAUL];
    }
    sink = new_sink(SINK_TYPE_CM4RECORD);
    if (sink == NULL)
    {
        return NULL;
    }
    data_ul_open = (data_ul_open_param_p)param;

    sink->taskid = DAV_TASK_ID; // TBD
    sink->param.cm4_record.share_info_base_addr = (SHARE_BUFFER_INFO_PTR)hal_memview_cm4_to_dsp0((U32)data_ul_open->p_share_info);
    sink->param.data_ul.current_frame_size = data_ul_open->current_frame_size;
    sink->param.data_ul.payload_size = data_ul_open->payload_size;
    sink->param.data_ul.data_format = data_ul_open->data_format;

    SinkInitDataUL(sink);
    return sink;
}

VOID StreamCloseAll(TRANSFORM transform, CLOSE_MODE_enum_s mode)
{
    DSP_STREAMING_PARA_PTR pStream;
    pStream = DSP_Streaming_Get(transform->source,transform->sink);
    U16 AckEndId = MSG_DSP_NULL_REPORT;
    DSP_MW_LOG_I("close all", 0);
    if(InstantCloseMode == mode)
    {
        audio_ops_trigger(transform->sink, AFE_PCM_TRIGGER_STOP);
        audio_ops_trigger(transform->source, AFE_PCM_TRIGGER_STOP);
        if (NULL != pStream)
        {
            AckEndId = pStream->DspReportEndId;
            DSP_Callback_Config(transform->source,transform->sink, NULL, FALSE);
        }
        PL_CRITICAL(StreamTransformClose, (VOID*)transform);
        if (AckEndId != MSG_DSP_NULL_REPORT)
        {
            aud_msg_ack(AckEndId, FALSE);
        }
    }
    else if(DspSoftCloeMode == mode)
    {
        if (NULL != pStream)
        {
            PL_CRITICAL(TransformChangeHandlerClose, (VOID*)transform);
        }
        else
        {
            configASSERT(0);
        }
    }
    else
    {
        configASSERT(0);
    }
}
VOID StreamDSPClose(SOURCE source,SINK sink, U16 msgID)
{
    DSP_STREAMING_PARA_PTR  pStream =DSP_Streaming_Get(source,sink);
    pStream->DspReportEndId = msgID;
    pStream->streamingStatus = STREAMING_END;
    Sink_Audio_ClosureSmooth(sink);
    vTaskResume(pStream->callback.EntryPara.DSPTask);
    //source->transform->Handler(source,sink);
}
volatile uint32_t int_mask;
ATTR_TEXT_IN_IRAM_LEVEL_1 VOID StreamDSP_HWSemaphoreTake(VOID)
{
    // uint32_t int_mask;
    uint32_t take_times = 0;

    /* Add hw semaphore to avoid multi-core access */
    while( ++take_times ) {
        hal_nvic_save_and_set_interrupt_mask((uint32_t*)&int_mask);
        if (HAL_HW_SEMAPHORE_STATUS_OK == hal_hw_semaphore_take(HW_SEMAPHORE_DSP_CIRCULAR_BUFFER)) {
            break;
        }

        if( take_times > HwSemRetryTimes ) {
            hal_nvic_restore_interrupt_mask(int_mask);
            //error handling
            // printf("%s : Can not take HW Semaphore\r\n", __func__);
            configASSERT(0);
        }
        hal_nvic_restore_interrupt_mask(int_mask);
        // vTaskDelay(10/portTICK_PERIOD_MS);
    }
}

ATTR_TEXT_IN_IRAM_LEVEL_1 VOID StreamDSP_HWSemaphoreGive(VOID)
{
    // uint32_t int_mask;
    if ( HAL_HW_SEMAPHORE_STATUS_OK == hal_hw_semaphore_give(HW_SEMAPHORE_DSP_CIRCULAR_BUFFER) ) {
        hal_nvic_restore_interrupt_mask(int_mask);
    } else {
        hal_nvic_restore_interrupt_mask(int_mask);

        //error handling
        // printf("%s : Can not give HW Semaphore\r\n", __func__);
        configASSERT(0);
    }
}

