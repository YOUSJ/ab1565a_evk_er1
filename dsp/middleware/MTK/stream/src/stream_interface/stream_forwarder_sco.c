/* Copyright Statement:
 *
 * (C) 2014  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
#include "types.h"
#include "dsp_memory.h"
#include "stream_audio.h"
#include "source_inter.h"
#include "sink_inter.h"
#include "transform.h"
#include "stream_n9sco.h"
#include "dsp_audio_msg.h"
#include "voice_plc_interface.h"
//-drivers
#include "audio_config.h"
#include "dsp_audio_process.h"
#include "dsp_memory.h"
#include "dsp_share_memory.h"
#include "dsp_temp.h"
#include "dsp_buffer.h"

#include "sfr_bt.h"
#include "timers.h"

#include "dsp_dump.h"

U8* tempptr[6];
U32 tempnum[6];
typedef BOOL (*SCOHANDLER)(SOURCE source,SINK sink);

// #include "Drv_gpio.h"

////////////////////////////////////////////////////////////////////////////////
// Constant Definitions ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define DSP_FORWARD_BUFFER_SIZE                 (360)
#define DSP_SCO_INBAND_INFORMATION              (20)

#define BTCLK_LEN 4
#define STATE_LEN 4
#define VOICE_HEADER (BTCLK_LEN + STATE_LEN)
#define ESCO_UL_ERROR_DETECT_THD (8)
#define BT_FRAME_UNIT (2500)
#define BT_NCLK_MASK (0x0FFFFFFC)
#define BT_SLOT_UNIT        (625)


////////////////////////////////////////////////////////////////////////////////
// Global Variables ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

Stream_n9sco_Config_Ptr N9SCO_setting;
U16 escoseqn;
VOLATILE t_hardware_baseband_registers *rBb = (VOLATILE t_hardware_baseband_registers *)0xB0010000;
static TimerHandle_t rx_for_timer = NULL;
static TimerHandle_t tx_for_timer = NULL;

////////////////////////////////////////////////////////////////////////////////
// Type Defintions /////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Function Declarations ///////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
EXTERN VOID StreamDSP_HWSemaphoreTake(VOID);
EXTERN VOID StreamDSP_HWSemaphoreGive(VOID);
VOID N9ScoRx_update_from_share_information(SOURCE source);
VOID N9ScoRx_update_readoffset_share_information( SOURCE source,U32 ReadOffset);
VOID N9ScoRx_update_writeoffset_share_information(SOURCE source,U32 WriteOffset);
VOID N9ScoTx_update_from_share_information(SINK sink);
VOID N9ScoTx_update_from_share_information_forwarder(SINK sink);
VOID N9ScoTx_update_readoffset_share_information(SINK sink,U32 ReadOffset);
VOID N9ScoTx_update_writeoffset_share_information(SINK sink,U32 WriteOffset);


/**
 * Source_Sco_Audio_Fwd_Ctrl
 *
 * Function to enable/disable Audio Forwarder.
 *
 * param :ctrl - enable/disable Audio Forwarder.
 *
 */
hal_nvic_status_t Sco_Audio_Fwd_Ctrl(fowarder_ctrl forwarder_en, fowarder_type forwarder_type)
{
    hal_nvic_status_t ret = HAL_NVIC_STATUS_OK;

    if(forwarder_type == RX_FORWARDER) {
        if (forwarder_en == ENABLE_FORWARDER){
            hal_nvic_disable_irq(BT_AURX_IRQn);
            ret = hal_nvic_register_isr_handler(BT_AURX_IRQn, (hal_nvic_isr_t)Sco_RX_IntrHandler);
            if (ret != HAL_NVIC_STATUS_OK) {
                DSP_MW_LOG_W("[Rx FWD] registerd callback handler fail!", 0);
                return ret;
            }
            ret = hal_nvic_enable_irq(BT_AURX_IRQn);
            SCO_Rx_Intr_Ctrl(FALSE);
            DSP_MW_LOG_I("[Rx FWD] registerd callback handler done!", 0);
        } else {
            ret = hal_nvic_disable_irq(BT_AURX_IRQn);
            if (ret != HAL_NVIC_STATUS_OK) {
                DSP_MW_LOG_W("[Rx FWD] un-registerd callback handler fail!", 0);
                return ret;
            }
            SCO_Rx_Intr_Ctrl(FALSE);
            DSP_MW_LOG_I("[Rx FWD] un-registerd callback handler done!", 0);
        }
    } else if(forwarder_type == TX_FORWARDER) {
        if (forwarder_en == ENABLE_FORWARDER) {
            hal_nvic_disable_irq(BT_AUTX_IRQn);
            ret = hal_nvic_register_isr_handler(BT_AUTX_IRQn, (hal_nvic_isr_t)Sco_TX_IntrHandler);
            if (ret != HAL_NVIC_STATUS_OK) {
                DSP_MW_LOG_W("[Tx FWD] registerd callback handler fail!", 0);
                return ret;
            }
            ret = hal_nvic_enable_irq(BT_AUTX_IRQn);
            SCO_Tx_Intr_Ctrl(FALSE);
            DSP_MW_LOG_I("[Tx FWD] registerd callback handler done!", 0);

        } else {
            ret = hal_nvic_disable_irq(BT_AUTX_IRQn);
            if (ret != HAL_NVIC_STATUS_OK) {
                DSP_MW_LOG_W("[Tx FWD] un-registerd callback handler fail!", 0);
                return ret;
            }
            SCO_Tx_Intr_Ctrl(FALSE);
            DSP_MW_LOG_I("[Tx FWD] un-registerd callback handler done!", 0);
        }
    } else {
        DSP_MW_LOG_W("No this kind of forwarder!", 0);
        ret = HAL_NVIC_STATUS_ERROR;
    }

    return ret;
}

static void rx_forwarder_timer_callback(TimerHandle_t xTimer)
{
    UNUSED(xTimer);
    printf("Rx IRQ Handler \r\n");
    MCE_LatchSrcTiming();
}

static void tx_forwarder_timer_callback(TimerHandle_t xTimer)
{
    UNUSED(xTimer);
    printf("Tx IRQ Handler \r\n");
    MCE_LatchSrcTiming();
}

void Forwarder_IRQ_init(BOOL isRx)
{
    printf("Forwarder Init");
    rBb->rClkCtl.rNativeClkCtl = 1;
    MCE_LatchSrcTiming();

    if(isRx == true)
    {
        if(rx_for_timer == NULL)
        {
            rx_for_timer = xTimerCreate("RX_FORWARDER_TIMER", pdMS_TO_TICKS(1000), pdTRUE, 0, rx_forwarder_timer_callback);
            if(!rx_for_timer) {
                printf("rx forwarder create timer FAIL \n");
            } else {
                printf("rx forwarder create timer PASS \n");
                xTimerStart(rx_for_timer, 0);
            }
        }

    } else {
        if(tx_for_timer == NULL)
        {
            tx_for_timer = xTimerCreate("RX_FORWARDER_TIMER", pdMS_TO_TICKS(1000), pdTRUE, 0, tx_forwarder_timer_callback);
            if(!tx_for_timer) {
                printf("tx forwarder create timer FAIL \n");
            } else {
                printf("tx forwarder create timer PASS \n");
                xTimerStart(tx_for_timer, 0);
            }
        }
    }
}



/**
 * Sco_RX_IntrHandler
 *
 * Function ISR when Rx done.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
VOID Sco_RX_IntrHandler(VOID)
{
    volatile SOURCE source = Source_blks[SOURCE_TYPE_N9SCO];
    volatile SINK sink = Sink_blks[SINK_TYPE_AUDIO];
    AUDIO_PARAMETER *sink_param = &sink->param.audio;
    N9SCO_PARAMETER *src_param = &source->param.n9sco;
    if ((source == NULL) || (sink == NULL))
    {
        DSP_MW_LOG_I("[RX FWD] source or sink == NULL", 0);
        SCO_Rx_Intr_HW_Handler();
        return;
    }
    if ( (sink_param->irq_exist == false) && (src_param->rx_forwarder_en == false) )
    {
        U32 rx_forwarder_debug_time;
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &rx_forwarder_debug_time);
        DSP_MW_LOG_I("[RX FWD] irq not exist, rx_forwarder_en is false, rx_forwarder_time:%d", 1, rx_forwarder_debug_time);
        //return;
    }
    N9ScoRx_update_from_share_information(source);

    U16 pattern_framesize = source->streamBuffer.AVMBufferInfo.MemBlkSize;
    U8* fd_packet_ptr; //inlcude inband info + pattern
    U8* fd_pattern_ptr; // include only pattern
    U8* avm_buf_ptr; // put pattern
    U8* avm_info_ptr; // put info
    U32 avm_buf_len = source->streamBuffer.AVMBufferInfo.MemBlkSize*source->streamBuffer.AVMBufferInfo.MemBlkNum;
    U32 avm_buf_next_wo;
    U32 first_irq_avm_buf_wo = source->streamBuffer.AVMBufferInfo.MemBlkSize*N9SCO_setting->N9Sco_source.Process_Frame_Num;

    fd_packet_ptr = (U8 *)(source->streamBuffer.AVMBufferInfo.ForwarderAddr + SCO_Rx_Status()*DSP_FORWARD_BUFFER_SIZE);
    fd_pattern_ptr = (U8 *)(fd_packet_ptr + DSP_SCO_INBAND_INFORMATION);
    avm_buf_ptr = (U8 *)(source->streamBuffer.AVMBufferInfo.StartAddr + (U32)source->streamBuffer.AVMBufferInfo.WriteIndex);
    avm_info_ptr = (U8 *)(source->streamBuffer.AVMBufferInfo.StartAddr + avm_buf_len + DSP_SCO_INBAND_INFORMATION*(((U32)source->streamBuffer.AVMBufferInfo.WriteIndex)/pattern_framesize) );

    // Need to Add:
    //     - MSBC Sync Word

    //*** Memcpy Inband Info ***//
    DSP_D2C_BufferCopy(avm_info_ptr, fd_packet_ptr, DSP_SCO_INBAND_INFORMATION, (VOID *)(source->streamBuffer.AVMBufferInfo.StartAddr+avm_buf_len), DSP_SCO_INBAND_INFORMATION*source->streamBuffer.AVMBufferInfo.MemBlkNum);

    //*** Memcpy Pattern ***//
    DSP_D2C_BufferCopy(avm_buf_ptr, fd_pattern_ptr, pattern_framesize, (VOID *)source->streamBuffer.AVMBufferInfo.StartAddr, (U16)avm_buf_len);
    //DSP_MW_LOG_I("[RX FWD] rx_status:%d, RxED:%d, HEC:%d, CRC:%d, data: 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x",10, SCO_Rx_Status(), ((VOICE_RX_SINGLE_PKT_STRU_PTR_t)fd_packet_ptr)->InbandInfo.RxEd, ((VOICE_RX_SINGLE_PKT_STRU_PTR_t)fd_packet_ptr)->InbandInfo.HecErr, ((VOICE_RX_SINGLE_PKT_STRU_PTR_t)fd_packet_ptr)->InbandInfo.CrcErr,*(fd_pattern_ptr+1),*(fd_pattern_ptr+2),*(fd_pattern_ptr+3),*(fd_pattern_ptr+4),*(fd_pattern_ptr+5),*(fd_pattern_ptr+6));


    //*** For pkt lost debug: Accumulate Lost Pkt Num ***//
#ifdef DEBUG_HFP_PLK_LOST
    src_param->forwarder_pkt_num ++;
    if(Voice_PLC_CheckInfoValid((VOICE_RX_SINGLE_PKT_STRU_PTR_t)avm_info_ptr) == FALSE){
        src_param->lost_pkt_num++;
    }
    if(src_param->forwarder_pkt_num == 200){
        DSP_MW_LOG_I("lost packet: %d per %d pkt",1,src_param->lost_pkt_num, src_param->forwarder_pkt_num);
        src_param->lost_pkt_num = 0;
        src_param->forwarder_pkt_num = 0;
    }
#endif

    //*** Clean Fwd Pattern & Info ***//
    memset(fd_pattern_ptr, 0, pattern_framesize);
    Voice_PLC_CleanInfo((VOICE_RX_SINGLE_PKT_STRU_PTR_t)fd_packet_ptr);

    //*** Update AVM buffer writeoffset ***//
    avm_buf_next_wo = ((U32)source->streamBuffer.AVMBufferInfo.WriteIndex + (U32)pattern_framesize)%avm_buf_len;
    if(avm_buf_next_wo == source->streamBuffer.AVMBufferInfo.ReadIndex)
    {
        DSP_MW_LOG_I("[RX FWD] WO == RO %d %d", 2, avm_buf_next_wo, source->streamBuffer.AVMBufferInfo.ReadIndex);
        //U32 ro = (source->streamBuffer.AVMBufferInfo.ReadIndex + pattern_framesize)%avm_buf_len;
        //N9ScoRx_update_readoffset_share_information(source, ro);
    }
    N9ScoRx_update_writeoffset_share_information(source, avm_buf_next_wo);

    //*** Trigger 1st DL AFE IRQ ***//
    if ( (sink_param->irq_exist == FALSE) && (src_param->rx_forwarder_en == TRUE) && (avm_buf_next_wo == first_irq_avm_buf_wo))
    {
        if (source->transform->Handler != NULL)
        {
            U32 play_en_bt_clk = SCO_Rx_AncClk(); // next rx fwd anchor time + 2*312.5us
            DSP_MW_LOG_I("[RX FWD] First Rx Forwarder IRQ, play_en_bt_clk:%d %d, wo:%d, ro:%d", 4, SCO_Rx_AncClk(), play_en_bt_clk, avm_buf_next_wo, source->streamBuffer.AVMBufferInfo.ReadIndex);

            U32 play_en_nclk;
            U16 play_en_intra_clk;
            MCE_TransBT2NativeClk(play_en_bt_clk, 0, &play_en_nclk, &play_en_intra_clk);
            DSP_MW_LOG_I("[RX FWD] PhsOffset:%d, ClkOffset:%d, play_en_nclk: %d, play_en_intra: %d", 4, rBb->rAudioCtl.rRxPhsOffset, rBb->rAudioCtl.rRxClkOffset, play_en_nclk, play_en_intra_clk);
            hal_audio_afe_set_play_en(play_en_nclk, (U32)play_en_intra_clk);

            //*** Clean FWD IRQ ***//
            SCO_Rx_Intr_HW_Handler();
            src_param->rx_forwarder_en = FALSE;

            U32 rx_first_forwarder_time;
            hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &rx_first_forwarder_time);
            DSP_MW_LOG_I("[RX FWD] First Rx Forwarder IRQ resume DAVT, gpt_time: %d", 1, rx_first_forwarder_time);
            source->transform->Handler(source,sink);
            xTaskResumeFromISR((TaskHandle_t)DAV_TASK_ID);
        }
    }else {
        //*** Clean FWD IRQ ***//
        SCO_Rx_Intr_HW_Handler();
    }

}

/**
 * Sco_TX_IntrHandler
 *
 * Function ISR when Rx done.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
VOID Sco_TX_IntrHandler(VOID)
{
    //printf("Tx IRQ Handler, rTxIntrTmr:%d, rTxFlushTmr:%d, rTxAncClk:%d\r\n", rBb->rAudioCtl.rTxIntrTmr, rBb->rAudioCtl.rTxFlushTmr , rBb->rAudioCtl.rTxAncClk);
    //MCE_LatchSrcTiming();
    volatile SINK sink = Sink_blks[SINK_TYPE_N9SCO];
    if (sink == NULL)
    {
        DSP_MW_LOG_I("[TX FWD] sink == NULL", 0);
        SCO_Tx_Intr_HW_Handler();
        return;
    }
    N9ScoTx_update_from_share_information_forwarder(sink);

    if( (gDspAlgParameter.EscoMode.Tx == mSBC) && ((rBb->rAudioCtl.rTxAirMode != 3)||(rBb->rAudioCtl.rTxDataLen != 60)) )
    {
        DSP_MW_LOG_I("[TX FWD] mSBC strange AirMode & DataLen, rTxAirMode:%d rTxDataLen:%d", 2, rBb->rAudioCtl.rTxAirMode, rBb->rAudioCtl.rTxDataLen);
        return;
    }

    if(sink->param.n9sco.tx_forwarder_en == TRUE)
    {
        U32 tx_first_forwarder_time;
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &tx_first_forwarder_time);
        DSP_MW_LOG_I("[TX FWD] First Tx forwarder, gpt_time: %d", 1, tx_first_forwarder_time);
        sink->param.n9sco.tx_forwarder_en = FALSE;
    }

    U16 pattern_framesize = sink->streamBuffer.AVMBufferInfo.MemBlkSize; // data framesize
    U8* fd_packet_ptr; //forwarder pattern ptr
    U8* avm_buf_ptr; // avm pattern
    U32 avm_buf_len = (sink->streamBuffer.AVMBufferInfo.MemBlkSize)*(sink->streamBuffer.AVMBufferInfo.MemBlkNum);
    U32 avm_buf_next_ro;

    fd_packet_ptr = (U8 *)(sink->streamBuffer.AVMBufferInfo.ForwarderAddr + SCO_Tx_Status()*DSP_FORWARD_BUFFER_SIZE);
    avm_buf_ptr = (U8 *)(sink->streamBuffer.AVMBufferInfo.StartAddr + (U32)sink->streamBuffer.AVMBufferInfo.ReadIndex);
    memcpy(fd_packet_ptr, avm_buf_ptr, pattern_framesize);

    SCO_Tx_Intr_HW_Handler();

    //DSP_MW_LOG_I("[TX FWD] Tx IRQ Handler avm_RO:%d, WO:%d, sco_tx_status:%d", 3, (U32)sink->streamBuffer.AVMBufferInfo.ReadIndex, (U32)sink->streamBuffer.AVMBufferInfo.WriteIndex, SCO_Tx_Status());
    /*for(int i = 0; i < pattern_framesize; i ++) {
       DSP_MW_LOG_I("fd_packet_ptr:0x%x, forwarder data: 0x%x",2, fd_packet_ptr, *(fd_packet_ptr+i));
    }*/

    // Update AVM buffer readoffset
    avm_buf_next_ro = ((U32)sink->streamBuffer.AVMBufferInfo.ReadIndex + (U32)pattern_framesize)%avm_buf_len;
    N9ScoTx_update_readoffset_share_information(sink, avm_buf_next_ro);
}

/**
 * SCO_Rx_Intr_HW_Handler
 *
 * Function Rx HW interrupt Handler.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
VOID SCO_Rx_Intr_HW_Handler(VOID)
{
    rBb->rAuRxIntFlag = 1;
}

VOID SCO_Tx_Intr_HW_Handler(VOID)
{
    rBb->rAuTxIntFlag = 1;
}

/**
 * SCO_Rx_Intr_Ctrl
 *
 * Function to report Rx status.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
U32 SCO_Rx_Status (void)
{
    return rBb->rAudioCtl.rRxDstAddrSelCurrSw;
}

U32 SCO_Tx_Status (void)
{
    return rBb->rAudioCtl.rTxSrcAddrSelNxtSw;
}

U32 SCO_Rx_AncClk (void)
{
    return rBb->rAudioCtl.rRxAncClk; //unit:312.5us
}

U32 SCO_RX_FWD_IntrTime(void)
{
    return (rBb->rAudioCtl.rRxIntrTmr)/625; //rRxIntrTmer unit:0.5us, return unit:312.5us
}


VOID MCE_BtClkPhaseSwitch(BTCLK* pCLK, BTPHASE* pPhase)
{
    if (*pPhase >= 2500)
    {
        *pPhase -= 2500;
        *pCLK += 4;
    }

    if (*pPhase < 1250)
    {
        if (*pPhase >= 625)
        {
            *pCLK += 0x01;
            *pPhase -= 625;
        }
    }
    else if (*pPhase < 1875)
    {
        *pCLK += 0x02;
        *pPhase -= 1250;
    }
    else
    {
        *pCLK += 0x03;
        *pPhase -= 1875;
    }
}



VOID MCE_GetBtClk(BTCLK* pCurrCLK, BTPHASE* pCurrPhase)
{
    BTCLK CurrCLK,NativeCLK;
    BTPHASE CurrPhase;

    BTPHASE    PhaseOffset = rBb->rAudioCtl.rRxPhsOffset;
    BTCLK      ClockOffset = rBb->rAudioCtl.rRxClkOffset;

    do{
        NativeCLK = rBb->rClkCtl.rNativeClock&0x0FFFFFFC; /*Native Clk*/
        CurrCLK = (NativeCLK + ClockOffset)&0x0FFFFFFC;   /*Bt Clk*/
        CurrPhase = rBb->rClkCtl.rNativePhase + PhaseOffset; /*Bt Intra Clk*/
    } while(NativeCLK != (rBb->rClkCtl.rNativeClock&0x0FFFFFFC));

    MCE_BtClkPhaseSwitch(&CurrCLK, &CurrPhase);

    *pCurrCLK = CurrCLK;    /*Bt Clk*/
    *pCurrPhase = CurrPhase;/*Bt Intra Clk*/
}


VOID MCE_Get_BtClkOffset(BTCLK* pClkOffset, BTPHASE* pNClkOffse)
{
    BTPHASE    PhaseOffset = rBb->rAudioCtl.rRxPhsOffset;
    BTCLK      ClockOffset = rBb->rAudioCtl.rRxClkOffset;

    *pClkOffset = ClockOffset;
    *pNClkOffse = PhaseOffset;
}

VOID MCE_TransBT2NativeClk(BTCLK CurrCLK, BTPHASE CurrPhase,BTCLK* pNativeBTCLK, BTPHASE* pNativePhase)
{

    BTPHASE    PhaseOffset = rBb->rAudioCtl.rRxPhsOffset;
    BTCLK      ClockOffset = rBb->rAudioCtl.rRxClkOffset;

    if (CurrPhase > BT_FRAME_UNIT) {
        CurrPhase -= (BT_FRAME_UNIT);
        CurrCLK += 4;
    }
    *pNativeBTCLK = (CurrCLK - ClockOffset);
    *pNativePhase = (CurrPhase - PhaseOffset);
    if (*pNativePhase > BT_FRAME_UNIT) {
        *pNativePhase += (BT_FRAME_UNIT);
        *pNativeBTCLK -= 4;
    }
    uint32_t remain_n = *pNativeBTCLK & 0x03;
    if (remain_n != 0) {
       uint32_t intra_2 = remain_n * BT_SLOT_UNIT + *pNativePhase;
       if (intra_2 > BT_FRAME_UNIT) {
            intra_2 -= BT_FRAME_UNIT;
            *pNativeBTCLK += 4;
            *pNativePhase = intra_2;
       }
    }
    *pNativeBTCLK &= BT_NCLK_MASK;
}


VOID MCE_LatchSrcTiming (VOID)
{
    BTCLK BtClk;
    BTPHASE BtPhase;

    MCE_GetBtClk(&BtClk, &BtPhase);
    printf("BtClk:%d, BtPhase:%d \r\n", BtClk, BtPhase);
}


VOID AT_MCE_LatchSrcTiming (hal_ccni_message_t msg, hal_ccni_message_t *ack)
{
    UNUSED(msg);
    UNUSED(ack);
    BTCLK BtClk;
    BTPHASE BtPhase;

    MCE_GetBtClk(&BtClk, &BtPhase);
    printf("BtClk:%d, BtPhase:%d\r\n", BtClk, BtPhase);
}

ATTR_TEXT_IN_IRAM VOID N9ScoRx_update_from_share_information(SOURCE source)
{
    StreamDSP_HWSemaphoreTake();
    memcpy(&(source->streamBuffer.AVMBufferInfo), source->param.n9sco.share_info_base_addr, 40);/* share info fix 40 byte */
    source->streamBuffer.AVMBufferInfo.StartAddr = hal_memview_cm4_to_dsp0(source->streamBuffer.AVMBufferInfo.StartAddr);
    source->streamBuffer.AVMBufferInfo.ForwarderAddr = hal_memview_cm4_to_dsp0(source->streamBuffer.AVMBufferInfo.ForwarderAddr);
    StreamDSP_HWSemaphoreGive();
}

ATTR_TEXT_IN_IRAM_LEVEL_2 VOID N9ScoRx_update_writeoffset_share_information(SOURCE source,U32 WriteOffset)
{
    StreamDSP_HWSemaphoreTake();
    source->param.n9sco.share_info_base_addr->WriteIndex = (U16)WriteOffset;
    StreamDSP_HWSemaphoreGive();
}

ATTR_TEXT_IN_IRAM_LEVEL_2 VOID N9ScoRx_update_readoffset_share_information( SOURCE source,U32 ReadOffset)
{
    StreamDSP_HWSemaphoreTake();
    source->param.n9sco.share_info_base_addr->ReadIndex = (U16)ReadOffset;
#ifdef PT_bBufferIsFull_ready
    source->param.n9sco.share_info_base_addr->bBufferIsFull = FALSE;
#endif
    StreamDSP_HWSemaphoreGive();
}

ATTR_TEXT_IN_IRAM VOID N9ScoTx_update_from_share_information(SINK sink)
{
    StreamDSP_HWSemaphoreTake();
    memcpy(&(sink->streamBuffer.AVMBufferInfo), sink->param.n9sco.share_info_base_addr, 40);/* share info fix 40 byte */
    sink->streamBuffer.AVMBufferInfo.StartAddr = hal_memview_cm4_to_dsp0(sink->streamBuffer.AVMBufferInfo.StartAddr);
    //sink->streamBuffer.AVMBufferInfo.ForwarderAddr = hal_memview_cm4_to_dsp0(sink->streamBuffer.AVMBufferInfo.ForwarderAddr);
    StreamDSP_HWSemaphoreGive();
}

ATTR_TEXT_IN_IRAM VOID N9ScoTx_update_from_share_information_forwarder(SINK sink)
{
    StreamDSP_HWSemaphoreTake();
    memcpy(&(sink->streamBuffer.AVMBufferInfo), sink->param.n9sco.share_info_base_addr, 40);/* share info fix 40 byte */
    sink->streamBuffer.AVMBufferInfo.StartAddr = hal_memview_cm4_to_dsp0(sink->streamBuffer.AVMBufferInfo.StartAddr);
    sink->streamBuffer.AVMBufferInfo.ForwarderAddr = hal_memview_cm4_to_dsp0(sink->streamBuffer.AVMBufferInfo.ForwarderAddr);
    StreamDSP_HWSemaphoreGive();
}

ATTR_TEXT_IN_IRAM_LEVEL_2 VOID N9ScoTx_update_readoffset_share_information(SINK sink,U32 ReadOffset)
{
    StreamDSP_HWSemaphoreTake();
    sink->param.n9sco.share_info_base_addr->ReadIndex = (U16)ReadOffset;
    StreamDSP_HWSemaphoreGive();
}

ATTR_TEXT_IN_IRAM_LEVEL_2 VOID N9ScoTx_update_writeoffset_share_information(SINK sink,U32 WriteOffset)
{
    StreamDSP_HWSemaphoreTake();
    sink->param.n9sco.share_info_base_addr->WriteIndex = (U16)WriteOffset;
#ifdef PT_bufferfull
    if (WriteOffset == sink->param.n9sco.share_info_base_addr->ReadIndex)
    {
        sink->param.n9sco.share_info_base_addr->bBufferIsFull = 1;
    }
#endif
    StreamDSP_HWSemaphoreGive();
}
static VOID N9Sco_Reset_Sinkoffset_share_information(SINK sink)
{
    StreamDSP_HWSemaphoreTake();
    sink->param.n9sco.share_info_base_addr->WriteIndex = 0;
    sink->param.n9sco.share_info_base_addr->ReadIndex  = 0;
#ifdef PT_bufferfull
    sink->param.n9sco.share_info_base_addr->bBufferIsFull = FALSE;
#endif
    StreamDSP_HWSemaphoreGive();
}


VOID N9SCO_Default_setting_init(VOID)
{
       if (N9SCO_setting != NULL)
       {return;}
       N9SCO_setting = pvPortMalloc(sizeof(Stream_n9sco_Config_t));//for rtos
       memset(N9SCO_setting,0,sizeof(Stream_n9sco_Config_t));

       N9SCO_setting->N9Sco_source.Process_Frame_Num       = 2;

       N9SCO_setting->N9Sco_sink.Process_Frame_Num         = 2;
       N9SCO_setting->N9Sco_sink.N9_Ro_abnormal_cnt        = 0;
}




/**
 * SinkSlackSco
 *
 * Function to know the remain buffer size of SCO sink.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
ATTR_TEXT_IN_IRAM_LEVEL_2 U32 SinkSlackSco(SINK sink)
{
    N9ScoTx_update_from_share_information(sink);
    U32 writeOffset = sink->streamBuffer.AVMBufferInfo.WriteIndex;
    U32 readOffset  = sink->streamBuffer.AVMBufferInfo.ReadIndex;
    U32 sharebuflen = sink->streamBuffer.AVMBufferInfo.MemBlkSize * sink->streamBuffer.AVMBufferInfo.MemBlkNum;
    U32 ProcessFrameLen = (N9SCO_setting->N9Sco_source.Process_Frame_Num)*(sink->streamBuffer.AVMBufferInfo.MemBlkSize);
    U32 RemainBuf = (readOffset >= writeOffset) ?(sharebuflen + writeOffset - readOffset) : (readOffset - writeOffset - readOffset);
    //printf("SinkSlackN9Sco process_data_length : %d\r\n", sink->param.n9sco.process_data_length);
#ifdef PT_bufferfull
    if ((sink->streamBuffer.ShareBufferInfo.bBufferIsFull != 1)&&(RemainBuf >= ProcessFrameLen))
#else
    if (RemainBuf >= ProcessFrameLen)
#endif
    {
        return sink->param.n9sco.process_data_length;
    }
    else
    {
        return sink->param.n9sco.process_data_length;
    }
}


/**
 * SinkClaimSco
 *
 * Function to ask the buffer to write data into SCO sink.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
U32 SinkClaimSco(SINK sink, U32 extra)
{
    N9ScoTx_update_from_share_information(sink);
    U32 sharebuflen = sink->streamBuffer.AVMBufferInfo.MemBlkSize * sink->streamBuffer.AVMBufferInfo.MemBlkNum;
    U32 writeOffset = sink->streamBuffer.AVMBufferInfo.WriteIndex;
    U32 readOffset  = sink->streamBuffer.AVMBufferInfo.ReadIndex;
    U32 RemainBuf = (readOffset >= writeOffset) ? (sharebuflen - writeOffset + readOffset) : (writeOffset - readOffset);
#ifdef PT_bufferfull
    if((extra != 0)&&((sink->streamBuffer.ShareBufferInfo.bBufferIsFull != 1)&&(RemainBuf > extra)) && (sink->transform == NULL))
#else
    if((extra != 0)&&(RemainBuf > extra)&&(sink->transform == NULL))
#endif
    {
        return 0;
    }
    else
    {
        return SINK_INVALID_CLAIM;
    }
}

/**
 * SinkMapSco
 *
 * Function to read the decoded data in SCO sink.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
U8* SinkMapSco(SINK sink)
{
    N9ScoTx_update_from_share_information(sink);
    //memcpy(MapAddr,sink->streamBuffer.ShareBufferInfo.startaddr + sink->streamBuffer.ShareBufferInfo.ReadOffset, sink->streamBuffer.ShareBufferInfo.length - sink->streamBuffer.ShareBufferInfo.ReadOffset);
    if (sink->streamBuffer.AVMBufferInfo.ReadIndex != 0)
    {
        memcpy(MapAddr + sink->streamBuffer.AVMBufferInfo.ReadIndex,&(sink->streamBuffer.AVMBufferInfo.StartAddr), sink->streamBuffer.AVMBufferInfo.ReadIndex);
    }

    return MapAddr;

}

/**
 * SinkFlushSco
 *
 * Function to read the decoded data in SCO sink.
 *
 * param :amount - The amount of data written into sink.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
*/
ATTR_TEXT_IN_IRAM_LEVEL_2 BOOL SinkFlushSco(SINK sink,U32 amount)
{
    uint32_t mask;
    hal_nvic_save_and_set_interrupt_mask(&mask);

    N9ScoTx_update_from_share_information(sink);
    U16 sharebuflen = sink->streamBuffer.AVMBufferInfo.MemBlkSize * sink->streamBuffer.AVMBufferInfo.MemBlkNum;
    U16 framesize = sink->streamBuffer.AVMBufferInfo.MemBlkSize;
    if ((SinkSlackSco(sink) == 0)||(amount != sink->param.n9sco.process_data_length))
    {
        DSP_MW_LOG_W("sinkflush, amount:%d != process_data_length:%d", 2, amount, sink->param.n9sco.process_data_length);
        return FALSE;
    }
    else
    {
        sink->streamBuffer.AVMBufferInfo.WriteIndex = (sink->streamBuffer.AVMBufferInfo.WriteIndex + N9SCO_setting->N9Sco_sink.Process_Frame_Num*framesize)%(sharebuflen);
    }

    #if !DL_TRIGGER_UL
    if (sink->param.n9sco.IsFirstIRQ == TRUE)
    {
        U32 gpt_timer,relative_delay;
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &gpt_timer);
        relative_delay = (gpt_timer - sink->param.n9sco.ul_play_gpt)%15000;
        /* send CCNI data transmit to N9 */
        if ((relative_delay < 8*1000)||(relative_delay > 12*1000)) //First flush time over 6 ms + 15 ms IRQ interval
        {
            U32 delay_time = (relative_delay < 8*1000) ? (8*1000 - relative_delay) : ((15+8)*1000 - relative_delay);
            hal_gpt_delay_us(delay_time);
            hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &gpt_timer);
            DSP_MW_LOG_I("eSCO UL flush abnormal, delay :%d GTP_N :%d, GPT_P :%d\r\n",3, delay_time,gpt_timer,sink->param.n9sco.ul_play_gpt);
        }
        else
        {
            DSP_MW_LOG_I("eSCO UL flush time GTP_N :%d, GPT_P :%d\r\n",2, gpt_timer,sink->param.n9sco.ul_play_gpt);
        }
        #warning "Trigger N9SCO Tx Interrupt handler, Function Name:N9Sco_TX_IntrHandler"
        /* send CCNI data transmit to N9 */
        hal_ccni_message_t msg;
        memset((void *)&msg, 0, sizeof(hal_ccni_message_t));
        msg.ccni_message[0] = MSG_DSP2N9_UL_START << 16;
        aud_msg_tx_handler(msg, 0, FALSE);
        sink->param.n9sco.IsFirstIRQ = FALSE;
    }
    #else
    /*if (((sink->streamBuffer.AVMBufferInfo.ReadIndex + N9SCO_setting->N9Sco_sink.Process_Frame_Num*N9SCO_setting->N9Sco_sink.Frame_Size)%sharebuflen) == sink->streamBuffer.AVMBufferInfo.WriteIndex)
    {
        //DSP_MW_LOG_I("eSCO UL Ro abnormal, cnt: %d %d %d",3,N9SCO_setting->N9Sco_sink.N9_Ro_abnormal_cnt,sink->streamBuffer.ShareBufferInfo.WriteOffset,sink->streamBuffer.ShareBufferInfo.ReadOffset);
        if(++N9SCO_setting->N9Sco_sink.N9_Ro_abnormal_cnt >= ESCO_UL_ERROR_DETECT_THD)
        {
            DSP_MW_LOG_I("eSCO UL trigger ro/wo error handle cnt:%d",1,N9SCO_setting->N9Sco_sink.N9_Ro_abnormal_cnt);
            sink->streamBuffer.AVMBufferInfo.WriteIndex = (sink->streamBuffer.AVMBufferInfo.WriteIndex + N9SCO_setting->N9Sco_sink.Frame_Size)%(sharebuflen);
        }
    }
    else
    {
        N9SCO_setting->N9Sco_sink.N9_Ro_abnormal_cnt = 0;
    }*/
    #endif


    N9ScoTx_update_writeoffset_share_information(sink,sink->streamBuffer.AVMBufferInfo.WriteIndex);
    //DSP_MW_LOG_I("[SinkFlushN9Sco] eSCO, wo:%d, ro:%d", 2, sink->streamBuffer.AVMBufferInfo.WriteIndex, sink->streamBuffer.AVMBufferInfo.ReadIndex);
    if (sink->param.n9sco.tx_forwarder_en == TRUE)
    {
        DSP_MW_LOG_I("[SinkFlushN9Sco] eSCO UL is first IRQ, wo:%d, ro:%d, sink->param.n9sco.tx_forwarder_en:%d", 3, sink->streamBuffer.AVMBufferInfo.WriteIndex, sink->streamBuffer.AVMBufferInfo.ReadIndex, sink->param.n9sco.tx_forwarder_en);
        SCO_Tx_Intr_HW_Handler();
        SCO_Tx_Intr_Ctrl(TRUE);
        SCO_Tx_Buf_Ctrl(TRUE);
    }
    hal_nvic_restore_interrupt_mask(mask);
    return TRUE;
}

ATTR_TEXT_IN_IRAM_LEVEL_2 BOOL SinkBufferWriteSco (SINK sink, U8 *src_addr, U32 length)
{
    U16 i;
    U8* write_ptr;
    N9ScoTx_update_from_share_information(sink);
    U16 sharebuflen = sink->streamBuffer.AVMBufferInfo.MemBlkSize * sink->streamBuffer.AVMBufferInfo.MemBlkNum;
    U16 framesize = sink->streamBuffer.AVMBufferInfo.MemBlkSize;

    if (sink->param.n9sco.process_data_length != length)
    {
        return FALSE;
    }

    for (i = 0 ; i < N9SCO_setting->N9Sco_sink.Process_Frame_Num ; i++)
    {
        write_ptr = (U8*)(sink->streamBuffer.AVMBufferInfo.StartAddr + sink->streamBuffer.AVMBufferInfo.WriteIndex);
        memcpy(write_ptr, src_addr + (U32)(i*framesize) , framesize);
        sink->streamBuffer.AVMBufferInfo.WriteIndex = (sink->streamBuffer.AVMBufferInfo.WriteIndex + framesize)%(sharebuflen);
    }
    return TRUE;

}



/**
 * Sink_Sco_Buffer_Ctrl
 *
 * Function to enable/disable SCO buffer.
 *
 * param :ctrl - enable/disable SCO buffer.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
VOID Sink_N9Sco_Buffer_Init(SINK sink)
{
    N9ScoTx_update_from_share_information(sink);
    N9Sco_Reset_Sinkoffset_share_information(sink);

    // Prefill 1 frame in Sink Buffer
    if(gDspAlgParameter.EscoMode.Tx == mSBC)
    {
        N9ScoTx_update_writeoffset_share_information(sink, 60);
    }else if(gDspAlgParameter.EscoMode.Tx == CVSD)
    {
        N9ScoTx_update_writeoffset_share_information(sink, 120);
    }

}


/**
 * SinkCloseSco
 *
 * Function to shutdown SCO sink.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
BOOL SinkCloseSco(SINK sink)
{
    sink->param.n9sco.process_data_length = 0;
    Sco_Audio_Fwd_Ctrl(DISABLE_FORWARDER, TX_FORWARDER);
    return TRUE;
}



/**
 * SinkInitSco
 *
 * Function to initialize SCO sink.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
VOID SinkInitN9Sco(SINK sink)
{
    /* buffer init */
    N9SCO_Default_setting_init();
    sink->buftype = BUFFER_TYPE_CIRCULAR_BUFFER;
    Sink_N9Sco_Buffer_Init(sink);
    SCO_Tx_Forwarder_Buf_Init(sink);
    Sco_Audio_Fwd_Ctrl(ENABLE_FORWARDER, TX_FORWARDER);

    sink->param.n9sco.process_data_length = N9SCO_setting->N9Sco_sink.Process_Frame_Num*(sink->streamBuffer.AVMBufferInfo.MemBlkSize);
    sink->param.n9sco.tx_forwarder_en = FALSE;
    DSP_MW_LOG_I("eSCO UL process_data_length : %d %d %d %d\r\n", 4, sink->param.n9sco.process_data_length, N9SCO_setting->N9Sco_sink.Process_Frame_Num, sink->streamBuffer.AVMBufferInfo.MemBlkSize, sink->streamBuffer.AVMBufferInfo.MemBlkNum);

    /* interface init */
    sink->sif.SinkSlack       = SinkSlackSco;
    sink->sif.SinkClaim       = SinkClaimSco;
    sink->sif.SinkMap         = SinkMapSco;
    sink->sif.SinkFlush       = SinkFlushSco;
    sink->sif.SinkClose       = SinkCloseSco;
    sink->sif.SinkWriteBuf    = SinkBufferWriteSco;

    sink->param.n9sco.IsFirstIRQ = TRUE;
    escoseqn = 0;

}


/**
 * SourceSizeSco
 *
 * Function to report remaining Source buffer size.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
ATTR_TEXT_IN_IRAM U32 SourceSizeSco(SOURCE source)
{
    N9ScoRx_update_from_share_information(source);
    U32 writeOffset = source->streamBuffer.AVMBufferInfo.WriteIndex;
    U32 readOffset  = source->streamBuffer.AVMBufferInfo.ReadIndex;
    U32 avmbufLen = source->streamBuffer.AVMBufferInfo.MemBlkSize * source->streamBuffer.AVMBufferInfo.MemBlkNum;
    U32 processFrameLen = (N9SCO_setting->N9Sco_source.Process_Frame_Num)*source->streamBuffer.AVMBufferInfo.MemBlkSize;
    U32 dataRemainLen = (readOffset > writeOffset) ? (avmbufLen - readOffset + writeOffset) : (writeOffset - readOffset);

    if ((dataRemainLen >= processFrameLen)||(source->param.n9sco.write_offset_advance != 0))
    {
        //DSP_MW_LOG_I("SourceSize dataRemainLen: %d Ro:%d Wo:%d, avmbufLen:%d", 4,  dataRemainLen, readOffset, writeOffset, avmbufLen);
        return source->param.n9sco.process_data_length;
    }
    else
    {
        DSP_MW_LOG_I("SourceSize = 0, dataRemainLen:%d Ro:%d Wo:%d", 3, dataRemainLen, readOffset, writeOffset);
        return 0;
    }
}


/**
 * SourceMapSco
 *
 * Function to  read the received data in SCO source.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
U8* SourceMapSco(SOURCE source)
{
    N9ScoRx_update_from_share_information(source);
    //memcpy(MapAddr,source->streamBuffer.ShareBufferInfo.startaddr + source->streamBuffer.ShareBufferInfo.ReadOffset, source->streamBuffer.ShareBufferInfo.length - source->streamBuffer.ShareBufferInfo.ReadOffset);
    if (source->streamBuffer.AVMBufferInfo.ReadIndex != 0)
    {
        memcpy(MapAddr + source->streamBuffer.AVMBufferInfo.ReadIndex,&(source->streamBuffer.AVMBufferInfo.StartAddr), source->streamBuffer.AVMBufferInfo.ReadIndex);
    }

    return MapAddr;
}

/**
 * SourceDropSco
 *
 * Function to drop the data in SCO sink.
 *
 * param :amount - The amount of data to drop in sink.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
ATTR_TEXT_IN_IRAM_LEVEL_2 VOID SourceDropSco(SOURCE source, U32 amount)
{
    U16 i;
    U8* write_ptr;

    N9ScoRx_update_from_share_information(source);
    U32 sharebuflen = source->streamBuffer.AVMBufferInfo.MemBlkSize * source->streamBuffer.AVMBufferInfo.MemBlkNum;
    U16 framesize = source->streamBuffer.AVMBufferInfo.MemBlkSize;

    if (amount != source->param.n9sco.process_data_length)
    {
        return;
    }
    else
    {

        for (i = 0 ; i < N9SCO_setting->N9Sco_source.Process_Frame_Num ; i++)
        {
            write_ptr = (U8 *)(source->streamBuffer.AVMBufferInfo.StartAddr + (source->streamBuffer.AVMBufferInfo.ReadIndex + i*framesize)%sharebuflen);
            *(write_ptr + framesize - STATE_LEN) = SCO_PKT_FREE;
        }
        source->streamBuffer.AVMBufferInfo.ReadIndex = (source->streamBuffer.AVMBufferInfo.ReadIndex + N9SCO_setting->N9Sco_source.Process_Frame_Num*framesize)%(sharebuflen);
        N9ScoRx_update_readoffset_share_information(source,source->streamBuffer.AVMBufferInfo.ReadIndex);
    }
    source->param.n9sco.write_offset_advance = 0;


    #if DL_TRIGGER_UL
    if  (((Sink_blks[SINK_TYPE_N9SCO] != NULL) && (Sink_blks[SINK_TYPE_N9SCO]->param.n9sco.IsFirstIRQ == TRUE))&& ((source->transform != NULL)&&(source->param.n9sco.dl_enable_ul == FALSE)))
    {
        volatile SINK eSCO_sink = Sink_blks[SINK_TYPE_N9SCO];
        U32 relative_delay,delay_thd;
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_32K, &(eSCO_sink->param.n9sco.ul_play_gpt));
        relative_delay = ((eSCO_sink->param.n9sco.ul_play_gpt - source->param.n9sco.ul_play_gpt)*1000)>>5;
        delay_thd = ((6 + ((relative_delay>>10)/(15*4)))%15); //extend THD every 15*4 ms
        if ((((relative_delay>>10)%15) < delay_thd)&&(eSCO_sink->transform != NULL))
        {
            eSCO_sink->param.n9sco.IsFirstIRQ = FALSE;
            eSCO_sink->param.n9sco.tx_forwarder_en = TRUE;
            DSP_MW_LOG_I("eSCO UL sync dl, DL time:%d thd :%d GTP_N :%d, GPT_P :%d, tx_forwarder_en:%d\r\n", 5, relative_delay, delay_thd,(eSCO_sink->param.n9sco.ul_play_gpt),source->param.n9sco.ul_play_gpt, eSCO_sink->param.n9sco.tx_forwarder_en);
            hal_gpt_delay_us(delay_thd*1000 - ((relative_delay*1000>>10)%15000));

            hal_audio_trigger_start_parameter_t start_parameter;
            start_parameter.memory_select = eSCO_sink->transform->source->param.audio.mem_handle.memory_select;
            start_parameter.enable = true;
            DSP_MW_LOG_I("UL SET TRIGGER MEM audio.rate %d audio.count %d\r\n", 2, eSCO_sink->transform->source->param.audio.rate, eSCO_sink->transform->source->param.audio.count);
            hal_audio_set_value((hal_audio_set_value_parameter_t *)&start_parameter, HAL_AUDIO_SET_TRIGGER_MEMORY_START);

            hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_32K, &(eSCO_sink->param.n9sco.ul_play_gpt));
            DSP_MW_LOG_I("eSCO UL start from DL drop, delay :%d GTP_N :%d",2, (delay_thd - relative_delay),eSCO_sink->param.n9sco.ul_play_gpt);
        }
        else
        {
            N9SCO_setting->N9Sco_sink.N9_Ro_abnormal_cnt = 0;
            SinkFlushSco(eSCO_sink,eSCO_sink->param.n9sco.process_data_length);
            DSP_MW_LOG_I("eSCO UL start from DL drop too late, delay :%d GTP_N :%d, GPT_P :%d\r\n",3, relative_delay,(eSCO_sink->param.n9sco.ul_play_gpt),source->param.n9sco.ul_play_gpt);
        }
    }
    #endif

    escoseqn += 2;
}

/**
 * SourceConfigureSco
 *
 * Function to configure SCO source.
 *
 * param :type - The configure type.
 *
 * param :value - The configure value.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
BOOL SourceConfigureSco(SOURCE source, stream_config_type type, U32 value)
{

    switch (type)
    {
        case SCO_SOURCE_WO_ADVANCE:
            source->param.n9sco.write_offset_advance = value;
            break;
        default:
            //printf("Wrong configure type");
            return FALSE;
            break;
    }

    return TRUE;
}


/**
 * SourceReadBufSco
 *
 * Function to read data from SCO source.
 *
 * param :dst_addr - The destination buffer to write data into.
 *
 * param :length -The leng of data to read.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
ATTR_TEXT_IN_IRAM_LEVEL_2 BOOL SourceReadBufSco(SOURCE source, U8* dst_addr, U32 length)
{
    U16 i;
    U8* write_ptr;
    U8* info_ptr;

    N9ScoRx_update_from_share_information(source);
    U16 sharebuflen = source->streamBuffer.AVMBufferInfo.MemBlkSize * source->streamBuffer.AVMBufferInfo.MemBlkNum;
    U16 framesize = source->streamBuffer.AVMBufferInfo.MemBlkSize;

    if (source->param.n9sco.process_data_length != length)
    {
        return FALSE;
    }

    for (i = 0 ; i < N9SCO_setting->N9Sco_source.Process_Frame_Num ; i++)
    {
        VOICE_RX_INBAND_INFO_t RxPacketInfo;

        write_ptr = (U8 *)(source->streamBuffer.AVMBufferInfo.StartAddr + source->streamBuffer.AVMBufferInfo.ReadIndex);
        info_ptr = (U8 *)(source->streamBuffer.AVMBufferInfo.StartAddr + (U32)sharebuflen + DSP_SCO_INBAND_INFORMATION*(((U32)source->streamBuffer.AVMBufferInfo.ReadIndex)/(U32)framesize));
        RxPacketInfo = (((VOICE_RX_SINGLE_PKT_STRU_PTR_t)info_ptr)->InbandInfo);
        //DSP_MW_LOG_I("[Source Read] info_ptr RxEd:%d, HEC:%d, CRC:%d, data: 0x%x 0x%x 0x%x 0x%x 0x%x", 8, RxPacketInfo.RxEd, RxPacketInfo.HecErr, RxPacketInfo.CrcErr, *(write_ptr+1) ,*(write_ptr+2) ,*(write_ptr+3),*(write_ptr+4),*(write_ptr+5));

        if(Voice_PLC_CheckInfoValid((VOICE_RX_SINGLE_PKT_STRU_PTR_t)info_ptr) == FALSE)
        {
            DSP_MW_LOG_I("[Source Read] meet packet expired %d", 1, escoseqn + i);
            Voice_PLC_CheckAndFillZeroResponse((S16 *)(dst_addr + i*framesize), gDspAlgParameter.EscoMode.Rx);
        }else
        {
            memcpy(dst_addr + i*framesize, write_ptr, framesize);
        }
        Voice_PLC_UpdateInbandInfo(&RxPacketInfo,sizeof(VOICE_RX_INBAND_INFO_t),i);
        source->streamBuffer.AVMBufferInfo.ReadIndex = (source->streamBuffer.AVMBufferInfo.ReadIndex + framesize)%(sharebuflen);
    }

    return TRUE;
}

/**
 * SourceCloseSco
 *
 * Function to shutdown SCO source.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
BOOL SourceCloseSco(SOURCE source)
{
    source->param.n9sco.process_data_length = 0;
    Sco_Audio_Fwd_Ctrl(DISABLE_FORWARDER, RX_FORWARDER);
    return TRUE;
}

/**
 * SourceInitSco
 *
 * Function to initialize SCO source.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
VOID SourceInitN9Sco(SOURCE source)
{
    /* buffer init */
    N9SCO_Default_setting_init();
    source->buftype = BUFFER_TYPE_CIRCULAR_BUFFER;
    Source_N9Sco_Buffer_Init(source);
    Sco_Audio_Fwd_Ctrl(ENABLE_FORWARDER, RX_FORWARDER); // register rx forwarder irq handler

    source->param.n9sco.process_data_length = (N9SCO_setting->N9Sco_source.Process_Frame_Num)*(source->streamBuffer.AVMBufferInfo.MemBlkSize);
    DSP_MW_LOG_I("source->param.n9sco.process_data_length:%d %d %d %d\r\n", 4, source->param.n9sco.process_data_length, (N9SCO_setting->N9Sco_source.Process_Frame_Num)*(source->streamBuffer.AVMBufferInfo.MemBlkSize), (source->streamBuffer.AVMBufferInfo.MemBlkSize), (source->streamBuffer.AVMBufferInfo.MemBlkNum));


    /* interface init */
    source->sif.SourceSize        = SourceSizeSco;
    source->sif.SourceReadBuf     = SourceReadBufSco;
    source->sif.SourceMap         = SourceMapSco;
    source->sif.SourceConfigure   = SourceConfigureSco;
    source->sif.SourceDrop        = SourceDropSco;
    source->sif.SourceClose       = SourceCloseSco;

    /* Enable Interrupt */
    source->param.n9sco.IsFirstIRQ = TRUE;
    source->param.n9sco.dl_enable_ul = TRUE;
    source->param.n9sco.write_offset_advance = 0;
#ifdef DEBUG_HFP_PLK_LOST
    source->param.n9sco.lost_pkt_num = 0;
    source->param.n9sco.forwarder_pkt_num = 0;
#endif
}


/**
 * Source_Sco_Buffer_Ctrl
 *
 * Function to enable/disable SCO buffer.
 *
 * param :ctrl - enable/disable SCO buffer.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
VOID Source_N9Sco_Buffer_Init(SOURCE source)
{
    N9ScoRx_update_from_share_information(source);
    N9ScoRx_update_readoffset_share_information(source,0);
    N9ScoRx_update_writeoffset_share_information(source,0);
}


/**
 * SCO_Rx_Intr_Ctrl
 *
 * Function to enable/disable Rx interrupt.
 *
 * param :ctrl - enable/disable Audio Forwarder.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
VOID SCO_Rx_Intr_Ctrl(BOOL ctrl)
{
    rBb->rAuRxIntMask = ctrl;
}

/**
 * SCO_Rx_Buf_Ctrl
 *
 * Function to enable/disable Rx interrupt.
 *
 * param :ctrl - enable/disable Audio Forwarder.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
VOID SCO_Rx_Buf_Ctrl(BOOL ctrl)
{
    rBb->rAudioCtl.rRxBufRdy = ctrl;
}


/**
 * SCO_Tx_HW_Init
 *
 * Function to initialize SCO HW.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
VOID SCO_Tx_Forwarder_Buf_Init(SINK sink)
{
    rBb->rAudioCtl.rTxSrcAddr0 = sink->streamBuffer.AVMBufferInfo.ForwarderAddr;
    rBb->rAudioCtl.rTxSrcAddr1 = sink->streamBuffer.AVMBufferInfo.ForwarderAddr + DSP_FORWARD_BUFFER_SIZE;
    memset((void *)sink->streamBuffer.AVMBufferInfo.ForwarderAddr, 0 , DSP_FORWARD_BUFFER_SIZE*2);
    DSP_MW_LOG_I("SCO_Tx_Forwarder_Buf_Init, addr0:0x%x addr1:0x%x", 2, rBb->rAudioCtl.rTxSrcAddr0, rBb->rAudioCtl.rTxSrcAddr1);

    //SCO_Tx_Buf_Ctrl(TRUE);
}


/**
 * SCO_Tx_Intr_Ctrl
 *
 * Function to enable/disable SCO Tx interrupt.
 *
 * param :ctrl - enable/disable SCO Tx interrupt.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
VOID SCO_Tx_Intr_Ctrl(BOOL ctrl)
{
    rBb->rAuTxIntMask = ctrl;
}

/**
 * SCO_Tx_Buf_Ctrl
 *
 * Function to enable/disable SCO Tx interrupt.
 *
 * param :ctrl - enable/disable SCO Tx interrupt.
 *
 * @Author : MachiWu <MachiWu@airoha.com.tw>
 */
VOID SCO_Tx_Buf_Ctrl(BOOL ctrl)
{
    rBb->rAudioCtl.rTxBufRdy = ctrl;
}


