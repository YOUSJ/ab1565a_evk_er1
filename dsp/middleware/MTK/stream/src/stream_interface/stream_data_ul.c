/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
#include "types.h"
#include "source_inter.h"
#include "dsp_buffer.h"
#include "dsp_memory.h"
#include "stream_cm4_record.h"
#include "dsp_callback.h"
#include "dsp_temp.h"
#include "dsp_dump.h"
#include "stream_data_ul.h"


////////////////////////////////////////////////////////////////////////////////
// Constant Definitions ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////
// Global Variables ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Type Defintions /////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
// Function Prototypes /////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
EXTERN VOID StreamDSP_HWSemaphoreTake(VOID);
EXTERN VOID StreamDSP_HWSemaphoreGive(VOID);

////////////////////////////////////////////////////////////////////////////////
// Function Declarations ///////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
static VOID DataUL_update_from_share_information(SINK sink)
{
    StreamDSP_HWSemaphoreTake();
    memcpy(&(sink->streamBuffer.ShareBufferInfo), sink->param.data_ul.share_info_base_addr, 32);/* share info fix 32 byte */
    sink->streamBuffer.ShareBufferInfo.startaddr = hal_memview_cm4_to_dsp0(sink->streamBuffer.ShareBufferInfo.startaddr);
    StreamDSP_HWSemaphoreGive();
}

static VOID DataUL_send_data_ready(VOID)
{
    hal_ccni_message_t msg;
    memset((void *)&msg, 0, sizeof(hal_ccni_message_t));
    msg.ccni_message[0] = MSG_DSP2MCU_DATA_UL_DATA_NOTIFY << 16;
    aud_msg_tx_handler(msg, 0, FALSE);
}

static VOID DataUL_update_writeoffset_share_information(SINK sink,U32 WriteOffset)
{
    StreamDSP_HWSemaphoreTake();
    sink->param.data_ul.share_info_base_addr->WriteOffset = WriteOffset;
    if (WriteOffset == sink->param.data_ul.share_info_base_addr->ReadOffset)
    {
        sink->param.data_ul.share_info_base_addr->bBufferIsFull = 1;
    }
    StreamDSP_HWSemaphoreGive();
}

static VOID DataUL_Reset_Sinkoffset_share_information(SINK sink)
{
    StreamDSP_HWSemaphoreTake();
    sink->param.data_ul.share_info_base_addr->WriteOffset = 0;
    sink->param.data_ul.share_info_base_addr->ReadOffset = 0;
    sink->param.data_ul.share_info_base_addr->bBufferIsFull = false;
    StreamDSP_HWSemaphoreGive();
}

static uint32_t data_ul_port_payload_copy(DATA_PARAMETER *data_ul, uint8_t *src_buf, uint8_t *dst_buf, uint32_t length)
{
    uint8_t *ptemp = dst_buf;

    if (data_ul->scenario_type == AUDIO_TRANSMITTER_RX_TRANSLATOR_UL) {

    } else if (data_ul->scenario_type == AUDIO_TRANSMITTER_RX_LINE_IN_BROADCAST) {

    } else if (data_ul->scenario_type == AUDIO_TRANSMITTER_RX_A2DP_SOURCE) {

    } else if (data_ul->scenario_type == AUDIO_TRANSMITTER_RX_GSENSOR) {
        memcpy(dst_buf,src_buf,length);
        return length;
    } else if (data_ul->scenario_type == AUDIO_TRANSMITTER_RX_MULTI_MIC_STREAM) {

    } else {
        assert(0);
    }
}

static void data_ul_update_from_share_information(SINK sink)
{
    StreamDSP_HWSemaphoreTake();
    memcpy(&(sink->streamBuffer.ShareBufferInfo), sink->param.data_ul.share_info_base_addr, 32);/* share info fix 32 byte */
    sink->streamBuffer.ShareBufferInfo.startaddr = hal_memview_cm4_to_dsp0(sink->streamBuffer.ShareBufferInfo.startaddr);
    StreamDSP_HWSemaphoreGive();
}

static void data_ul_update_writeoffset_share_information(SINK sink, U32 WriteOffset)
{
    StreamDSP_HWSemaphoreTake();
    sink->param.data_ul.share_info_base_addr->WriteOffset = WriteOffset;
    if (WriteOffset == sink->param.data_ul.share_info_base_addr->ReadOffset) {
        sink->param.data_ul.share_info_base_addr->bBufferIsFull = 1;
    }
    StreamDSP_HWSemaphoreGive();
}

static void data_ul_send_data_ready(SINK sink)
{
    hal_ccni_message_t msg;

    memset((void *)&msg, 0, sizeof(hal_ccni_message_t));
    msg.ccni_message[0] = (MSG_DSP2MCU_DATA_UL_DATA_NOTIFY << 16) + (sink->param.data_ul.scenario_type << 8) + sink->param.data_ul.scenario_id;
    aud_msg_tx_handler(msg, 0, FALSE);
}

VOID DataUL_Default_setting_init(VOID)
{

}

/**
 * SinkSlackCm4Record
 *
 * Function to know the remain buffer size of Cm4 Record sink.
 *
 *
 */
U32 SinkSlackDataUL(SINK sink)
{
    DataUL_update_from_share_information(sink);
    //DSP_MW_LOG_I("[Cm4 record] Sink_Slack RemainBuf(%d) bBufferIsFull(%d)", 2, RemainBuf, sink->streamBuffer.ShareBufferInfo.bBufferIsFull);
    if (sink->streamBuffer.ShareBufferInfo.bBufferIsFull == true)
    {
        return 0;
    }
    else
    {
        return sink->streamBuffer.ShareBufferInfo.sub_info.block_info.blk_size;
    }
}


/**
 * SinkClaimCm4Record
 *
 * Function to ask the buffer to write data into Cm4 Record  sink.
 *
 *
 */
U32 SinkClaimDataUL(SINK sink, U32 extra)
{
   UNUSED(sink);
   UNUSED(extra);
   return 0;
}

/**
 * SinkMapCm4Record
 *
 * Function to read the data in Cm4 Record sink.
 *
 *
 */
U8* SinkMapDataUL(SINK sink)
{
    UNUSED(sink);
    return NULL;
}


VOID DataULUpdateWriteIndex(SINK sink)
{
    sink->param.data_ul.payload_internal_offset = 0;
    sink->param.data_ul.write_index = (sink->param.data_ul.write_index+1)%sink->streamBuffer.ShareBufferInfo.sub_info.block_info.blk_num;
    sink->streamBuffer.ShareBufferInfo.WriteOffset = sink->param.data_ul.write_index*sink->streamBuffer.ShareBufferInfo.sub_info.block_info.blk_size;
    DataUL_update_writeoffset_share_information(sink,sink->streamBuffer.ShareBufferInfo.WriteOffset);
}


/**
 * SinkFlushCm4Record
 *
 * Function to read the decoded data in Cm4 Record sink.
 *
 * param :amount - The amount of data written into sink.
 *
 *
*/
BOOL SinkFlushDataUL(SINK sink,U32 amount)
{
    DataUL_update_from_share_information(sink);
    if (SinkSlackDataUL(sink) == 0)
    {
        DataUL_send_data_ready();
        return FALSE;
    }

    if (sink->param.data_ul.data_format == DATA_UL_FORMAT_SBC_MEDIA_PAYLOAD)
    {
        if (amount != sink->param.data_ul.current_frame_size)
        {
            DSP_MW_LOG_I("DATA UL flush abnormal, frame size :%,amount :%d", 2, sink->param.data_ul.current_frame_size,amount);
            return FALSE;
        }
        else
        {
            sink->param.data_ul.payload_internal_offset += sink->param.data_ul.current_frame_size;
            if (sink->param.data_ul.payload_size == sink->param.data_ul.payload_internal_offset)
            {
                DataULUpdateWriteIndex(sink);
            }
            else if (sink->param.data_ul.payload_size < sink->param.data_ul.payload_internal_offset)
            {
                DSP_MW_LOG_I("DATA UL internal offset abnormal, offset :%,amount :%d", 2, sink->param.data_ul.payload_internal_offset,amount);
            }
        }
    }
    return TRUE;
}


BOOL SinkBufferWriteDataUL (SINK sink, U8 *src_addr, U32 length)
{
    U8* write_ptr = (U8*)(sink->streamBuffer.ShareBufferInfo.startaddr + sink->streamBuffer.ShareBufferInfo.WriteOffset + (U32)sink->param.data_ul.payload_internal_offset);
    DataUL_update_from_share_information(sink);
    if ((sink->param.data_ul.data_format == DATA_UL_FORMAT_SBC_MEDIA_PAYLOAD)&&(sink->param.data_ul.payload_internal_offset == 0))
    {
        *write_ptr = sink->param.data_ul.payload_frame_number;
        write_ptr++;
        sink->param.data_ul.payload_internal_offset++;
    }

    if (length != sink->param.data_ul.current_frame_size)
    {
        
        DSP_MW_LOG_I("DATA UL Write abnormal, frame size :%,length :%d", 2, sink->param.data_ul.current_frame_size,length);
        return FALSE;
    }
    memcpy(write_ptr,src_addr,length);
    return TRUE;
}



/**
 * Sink_Cm4Record_Buffer_Init
 *
 * Function to update/reset cm4 record buffer.
 *
 * param :sink - cm4 record sink .
 *
 *
 */
BOOL SinkConfigDataUL(SINK sink, stream_config_type type, U32 value)
{
    UNUSED(value);
    UNUSED(type);

    DataUL_Reset_Sinkoffset_share_information(sink);
    sink->param.data_ul.payload_internal_offset = 0;
    sink->param.data_ul.write_index = 0;
}


VOID Sink_DataUL_Buffer_Init(SINK sink)
{
    DataUL_Reset_Sinkoffset_share_information(sink);
    sink->param.data_ul.payload_internal_offset = 0;
    sink->param.data_ul.write_index = 0;
}


/**
 * SinkCloseCm4Record
 *
 * Function to shutdown cm4 record  sink.
 *
 *
 */
BOOL SinkCloseDataUL(SINK sink)
{
    sink->param.data_ul.current_frame_size = 0;
    sink->param.data_ul.payload_size = 0;
    return TRUE;
}

/**
 * SinkSlack_GSENSOR
 *
 *
 *
 *
 */
U32 SinkSlack_GSENSOR(SINK sink)
{
    data_ul_update_from_share_information(sink);
    if (sink->streamBuffer.ShareBufferInfo.bBufferIsFull == true) {
        //data_ul_send_data_ready();
        return 0;
    } else {
        if ((sink->streamBuffer.ShareBufferInfo.sub_info.block_info.blk_size - sizeof(DATA_UL_HEADER_T) - sink->param.data_ul.payload_internal_offset) <= 0) {
            return 0;
        }
        return (sink->streamBuffer.ShareBufferInfo.sub_info.block_info.blk_size - sizeof(DATA_UL_HEADER_T) - sink->param.data_ul.payload_internal_offset);
    }
}

/**
 * SinkMap_GSENSOR
 *
 *
 *
 *
 */
U8* SinkMap_GSENSOR(SINK sink)
{
    return NULL;
}

/**
 * SinkConfigure_GSENSOR
 *
 *
 *
 *
 */
BOOL SinkConfigure_GSENSOR(SINK sink, stream_config_type type, U32 value)
{
    return true;
}

/**
 * SinkWriteBuf_GSENSOR
 *
 *
 *
 *
 */
BOOL SinkWriteBuf_GSENSOR(SINK sink, U8 *src_addr, U32 length)
{
    DATA_UL_HEADER_T *data_ul_header;
    uint32_t avail_block_num, total_buffer_size, payload_size;
    SHARE_BUFFER_INFO_PTR ShareBufferInfo;

    data_ul_update_from_share_information(sink);
    ShareBufferInfo = &(sink->streamBuffer.ShareBufferInfo);

    /* Check whether overflow happen */
    if ((sink->param.data_ul.current_frame_size == 0) && (sink->param.data_ul.payload_internal_offset == 0)) {
        total_buffer_size = ShareBufferInfo->sub_info.block_info.blk_size * ShareBufferInfo->sub_info.block_info.blk_num;
        if (ShareBufferInfo->bBufferIsFull == true) {
            avail_block_num = 0;
        } else if(ShareBufferInfo->ReadOffset > ShareBufferInfo->WriteOffset) {
            avail_block_num = (ShareBufferInfo->ReadOffset - ShareBufferInfo->WriteOffset) / ShareBufferInfo->sub_info.block_info.blk_size;
        } else {
            avail_block_num = (total_buffer_size - (ShareBufferInfo->WriteOffset - ShareBufferInfo->ReadOffset)) / ShareBufferInfo->sub_info.block_info.blk_size;
        }
        if ((avail_block_num == 0) || (length > (ShareBufferInfo->sub_info.block_info.blk_size - sizeof(DATA_UL_HEADER_T)))) {
            DSP_MW_LOG_I("share buffer overflow!!!", 0);
            return FALSE;
        }
    }

    /* Copy payload header and payload */
    payload_size = data_ul_port_payload_copy(&sink->param.data_ul, src_addr, ShareBufferInfo->startaddr + ShareBufferInfo->WriteOffset + sizeof(DATA_UL_HEADER_T), length);
    sink->param.data_ul.current_frame_size = payload_size;

    /* Copy header */
    if (sink->param.data_ul.current_frame_size) {
        data_ul_header = (DATA_UL_HEADER_T *)(ShareBufferInfo->startaddr + ShareBufferInfo->WriteOffset);
        data_ul_header->seqn = sink->param.data_ul.seqn++;
        data_ul_header->payload_len = payload_size;
    }

    return TRUE;
}

/**
 * SinkFlush_GSENSOR
 *
 *
 *
 *
 */
BOOL SinkFlush_GSENSOR(SINK sink, U32 amount)
{
    uint32_t total_buffer_size;
    SHARE_BUFFER_INFO_PTR ShareBufferInfo;

    /* If one frame has not fill done, don't notice host */
    if (sink->param.data_ul.current_frame_size == 0) {
        return FALSE;
    }

    data_ul_update_from_share_information(sink);
    ShareBufferInfo = &sink->streamBuffer.ShareBufferInfo;
    total_buffer_size = ShareBufferInfo->sub_info.block_info.blk_size * ShareBufferInfo->sub_info.block_info.blk_num;
    ShareBufferInfo->WriteOffset = (ShareBufferInfo->WriteOffset + ShareBufferInfo->sub_info.block_info.blk_size) % total_buffer_size;
    data_ul_update_writeoffset_share_information(sink, ShareBufferInfo->WriteOffset);
    sink->param.data_ul.payload_internal_offset = 0;
    sink->param.data_ul.current_frame_size = 0;

    data_ul_send_data_ready(sink);

    return true;
}

/**
 * SinkClose_GSENSOR
 *
 * 
 *
 *
 */
BOOL SinkClose_GSENSOR(SINK sink)
{
    return true;
}

/**
 * SinkInitCm4Record
 *
 * Function to initialize cm4 record sink.
 *
 *
 */
VOID SinkInitDataUL(SINK sink)
{
    /* buffer init */
    Sink_DataUL_Buffer_Init(sink);
    if (sink->param.data_ul.data_format == DATA_UL_FORMAT_SBC_MEDIA_PAYLOAD)
    {
        sink->param.data_ul.payload_frame_number = (sink->param.data_ul.payload_size - 1)/sink->param.data_ul.current_frame_size;
    }
    /* interface init */
    sink->sif.SinkSlack       = SinkSlackDataUL;
    sink->sif.SinkClaim       = SinkClaimDataUL;
    sink->sif.SinkMap         = SinkMapDataUL;
    sink->sif.SinkFlush       = SinkFlushDataUL;
    sink->sif.SinkClose       = SinkCloseDataUL;
    sink->sif.SinkWriteBuf    = SinkBufferWriteDataUL;
    sink->sif.SinkConfigure   = SinkConfigDataUL;

}

/**
 * SinkInit_GSENSOR
 *
 *
 *
 *
 */
void SinkInit_GSENSOR(SINK sink)
{
    sink->type = SINK_TYPE_GSENSOR;
    /* interface init */
    sink->sif.SinkSlack        = SinkSlack_GSENSOR;
    sink->sif.SinkMap          = SinkMap_GSENSOR;
    sink->sif.SinkConfigure    = SinkConfigure_GSENSOR;
    sink->sif.SinkFlush        = SinkFlush_GSENSOR;
    sink->sif.SinkClose        = SinkClose_GSENSOR;
    sink->sif.SinkWriteBuf     = SinkWriteBuf_GSENSOR;
}



