/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "leakage_compensation.h"
//#include "dsp_feature_interface.h"
#include "hal_define.h"
#include "dsp_callback.h"
#include "dsp_dump.h"
#include "dsp_audio_msg_define.h"
#include "dsp_temp.h"
#include <assert.h>
#include "dsp_memory.h"
#include "dsp_audio_msg.h"

#define LEAKAGE_REPORT_THD  (5000000)
#define LEAKAGE_DEBUG_NO_RESPONSE_THD  (10000000)
#define LEAKAGE_DEBUG_PERIOD  (1000000)



int *out_tempbuf = NULL;
unsigned long lc_max_count = 0;
static bool wz_report_flag = false;
FB_ANC_k_ctrl_struct_ptr Sph_Enh_ctrl_ptr;
anc_leakage_compensation_parameters_nvdm_t leakage_compensation_thd;
uint32_t leakage_report_start_count = 0;
uint32_t leakage_report_period_start_count = 0;


bool stream_function_leakage_compensation_initialize(void *para)
{
    int mem_size = 0;
    Sph_Enh_ctrl_ptr = stream_codec_get_workingbuffer(para);
    DSP_STREAMING_PARA_PTR  stream_ptr;
    DSP_ENTRY_PARA_PTR entry_para = (DSP_ENTRY_PARA_PTR)para;
    stream_ptr = DSP_STREAMING_GET_FROM_PRAR(entry_para);
    lc_max_count = 0;
    wz_report_flag = false;
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &leakage_report_start_count);
    leakage_report_period_start_count = leakage_report_start_count;

    Sph_Enh_ctrl_ptr->sample_rate = stream_function_get_samplingrate(para) * 1000;
    Sph_Enh_ctrl_ptr->frame_length = stream_function_get_output_size(para) / 2;
    if (Sph_Enh_ctrl_ptr->frame_length == 0) {
        DSP_MW_LOG_I("[RECORD_LC]frame_length == 0\r\n", 0);
        return 0;
    }
    printf("[RECORD_LC]src_out_size:%d, codec_out_size:%d\r\n", ((DSP_ENTRY_PARA_PTR)para)->src_out_size, ((DSP_ENTRY_PARA_PTR)para)->codec_out_size);
    DSP_MW_LOG_I("[RECORD_LC]stream_function_leakage_compensation_initialize, sr=%d, frame size=%d\r\n", 2, Sph_Enh_ctrl_ptr->sample_rate, Sph_Enh_ctrl_ptr->frame_length);
    if(out_tempbuf){
        DSPMEM_Free(entry_para->DSPTask, out_tempbuf);
        DSP_MW_LOG_I("[RECORD_LC]out_tempbuf!=NULL, free mem\r\n", 0);
    }
    mem_size = FB_ANC_K_API_Get_Memory(Sph_Enh_ctrl_ptr);
    out_tempbuf = DSPMEM_tmalloc(entry_para->DSPTask, mem_size, stream_ptr);

    if(out_tempbuf){
        FB_ANC_K_API_Alloc(Sph_Enh_ctrl_ptr, (Word32 *)out_tempbuf);

        if(!FB_ANC_K_API_Rst(Sph_Enh_ctrl_ptr)) {
            DSP_MW_LOG_I("[RECORD_LC]FB_ANC_K_API_Rst fail\r\n", 0);
            assert(0);
        }
    }
    else{
        DSP_MW_LOG_I("[RECORD_LC]mem_ptr==NULL\r\n", 0);
    }

    //set param from nvdm
    Sph_Enh_ctrl_ptr->Sz_thd2 = leakage_compensation_thd.Sz_thd2;
    Sph_Enh_ctrl_ptr->Sz_thd3 = leakage_compensation_thd.Sz_thd3;
    Sph_Enh_ctrl_ptr->Delay_sample = leakage_compensation_thd.Delay_sample;

    DSP_MW_LOG_I("[RECORD_LC]after Rst\n \
        Sz_thd2=%d, Sz_thd3=%d, Delay_sample=%d \
        Calibration_status=%d, Wz_set=%d, pwr_db=%d, algo_duration=%d \r\n", 7, \
        Sph_Enh_ctrl_ptr->Sz_thd2, Sph_Enh_ctrl_ptr->Sz_thd3, Sph_Enh_ctrl_ptr->Delay_sample, \
        Sph_Enh_ctrl_ptr->Calibration_status, Sph_Enh_ctrl_ptr->Wz_set, Sph_Enh_ctrl_ptr->pwr_db, lc_max_count);
    return 0;
}

bool stream_function_leakage_compensation_process(void *para)
{
    uint32_t start_count = 0;
    uint32_t end_count = 0;
    uint32_t count = 0;
    uint32_t leakage_report_cur_count = 0;
    uint32_t leakage_report_count = 0;
    uint32_t leakage_report_period_count = 0;
    hal_ccni_message_t msg;
    Sph_Enh_ctrl_ptr = stream_codec_get_workingbuffer(para);
    uint32_t channel_number;
    S16* EC_path_buf;
    uint16_t output_size = stream_function_get_output_size(para);
    channel_number = stream_function_get_channel_number(para);
    EC_path_buf = (channel_number == 2) ? (S16*)stream_function_get_2nd_inout_buffer(para) : (S16*)stream_function_get_3rd_inout_buffer(para);
    S16* InBuf     = (S16*)stream_function_get_1st_inout_buffer(para);

    Sph_Enh_ctrl_ptr->DL_ptr = EC_path_buf;
    Sph_Enh_ctrl_ptr->UL_ptr = InBuf;


#ifdef MTK_AUDIO_DUMP_BY_CONFIGTOOL
    LOG_AUDIO_DUMP((U8*)Sph_Enh_ctrl_ptr->UL_ptr, (U32)output_size, VOICE_TX_MIC_1);
    LOG_AUDIO_DUMP((U8*)Sph_Enh_ctrl_ptr->DL_ptr, (U32)output_size, VOICE_TX_REF);
#else
    UNUSED(output_size);
#endif


    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &start_count);
    FB_ANC_K_API_Process(Sph_Enh_ctrl_ptr);
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &end_count);
    hal_gpt_get_duration_count(start_count, end_count, &count);
    lc_max_count = (count > lc_max_count)? count: lc_max_count;



    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &leakage_report_cur_count);
    hal_gpt_get_duration_count(leakage_report_start_count, leakage_report_cur_count, &leakage_report_count);
    hal_gpt_get_duration_count(leakage_report_period_start_count, leakage_report_cur_count, &leakage_report_period_count);

    if((Sph_Enh_ctrl_ptr->Calibration_status == 3) && (!wz_report_flag) && (leakage_report_count > LEAKAGE_REPORT_THD))
    {
        DSP_MW_LOG_I("[RECORD_LC]DSP send ccni, Sz_thd2=%d, Sz_thd3=%d, Delay_sample=%d, Calibration_status=%d, Wz_set=%d, pwr_db=%d, algo_duration=%d, leakage_report_count:%d us \r\n", 8,
            Sph_Enh_ctrl_ptr->Sz_thd2, Sph_Enh_ctrl_ptr->Sz_thd3, Sph_Enh_ctrl_ptr->Delay_sample,
            Sph_Enh_ctrl_ptr->Calibration_status, Sph_Enh_ctrl_ptr->Wz_set, Sph_Enh_ctrl_ptr->pwr_db, lc_max_count, leakage_report_count);
        msg.ccni_message[0] = (MSG_DSP2MCU_RECORD_LC_WZ_REPORT << 16) | Sph_Enh_ctrl_ptr->Calibration_status;
        msg.ccni_message[1] = Sph_Enh_ctrl_ptr->Wz_set;
        aud_msg_tx_handler(msg, 0, false);
        wz_report_flag = true;
    } else if ((!wz_report_flag) && (leakage_report_count > LEAKAGE_DEBUG_NO_RESPONSE_THD)) {
        DSP_MW_LOG_E("[RECORD_LC] No response within 10 sec, state:%d reported:%d output_size:%d", 3, Sph_Enh_ctrl_ptr->Calibration_status, wz_report_flag, output_size);
        msg.ccni_message[0] = (MSG_DSP2MCU_RECORD_LC_WZ_REPORT << 16) | Sph_Enh_ctrl_ptr->Calibration_status;
        msg.ccni_message[1] = LD_STATUS_TIMEOUT;
        aud_msg_tx_handler(msg, 0, false);
        wz_report_flag = true;
    } else if ((!wz_report_flag) && (leakage_report_period_count > LEAKAGE_DEBUG_PERIOD)) {
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &leakage_report_period_start_count);
        DSP_MW_LOG_I("[RECORD_LC]DSP debug, Sz_thd2=%d, Sz_thd3=%d, Delay_sample=%d, Calibration_status=%d, Wz_set=%d, pwr_db=%d, algo_duration=%d leakage_report_count:%d us \r\n", 8,
            Sph_Enh_ctrl_ptr->Sz_thd2, Sph_Enh_ctrl_ptr->Sz_thd3, Sph_Enh_ctrl_ptr->Delay_sample,
            Sph_Enh_ctrl_ptr->Calibration_status, Sph_Enh_ctrl_ptr->Wz_set, Sph_Enh_ctrl_ptr->pwr_db, lc_max_count, leakage_report_count);
    }

    return 0;
}

