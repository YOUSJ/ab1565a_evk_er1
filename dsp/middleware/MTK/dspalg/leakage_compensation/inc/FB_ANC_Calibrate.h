/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef _FB_ANC_Calibrate_H
#define _FB_ANC_Calibrate_H

#include "typedef.h"

#define ENH_IIR_LENGTH 42

#define LD_STATUS_PASS                 (1) //from lib
#define LD_STATUS_FAIL_CASE_1          (2) //from lib
#define LD_STATUS_FAIL_CASE_2          (3) //from lib
#define LD_STATUS_TIMEOUT              (4)
#define LD_STATUS_TERMINATE            (5) //terminated by higher priority job

typedef struct
{
	Word16 Calibration_status; // 1: Start, 2:Running, 3: Done
	Word32 sample_rate;

	Word32 frame_length;

	Word16 *DL_ptr;
	Word16 *UL_ptr;
	Word16 Delay_buffer[DELAY_MAX_LENGTH];
	Word16 Delay_sample;
	Word16 DL_delay_index;

	void *SCH_mem;

	Word32 pwr_db;

	Word32 Sz_thd1;
	Word32 Sz_thd2;
	Word32 Sz_thd3;

	Word16 Wz_set;

	Word16 EPL_data_size;
	void *EPL_buffer;
} FB_ANC_k_ctrl_struct, *FB_ANC_k_ctrl_struct_ptr;

Word32 FB_ANC_K_API_Get_Memory(FB_ANC_k_ctrl_struct *Sph_Enh_ctrl);
Word16 FB_ANC_K_API_Alloc(FB_ANC_k_ctrl_struct *Sph_Enh_ctrl, Word32* mem_ptr);
Word16 FB_ANC_K_API_Rst(FB_ANC_k_ctrl_struct *Sph_Enh_ctrl);

void FB_ANC_K_API_Process(FB_ANC_k_ctrl_struct *Sph_Enh_ctrl);
void FB_ANC_K_Delay_time(FB_ANC_k_ctrl_struct *Sph_Enh_ctrl);
Word16 FB_ANC_K_Free(FB_ANC_k_ctrl_struct *Sph_Enh_ctrl);
#endif
