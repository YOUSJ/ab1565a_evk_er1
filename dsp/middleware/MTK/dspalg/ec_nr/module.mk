
###################################################
ECNR_FUNC_PATH = middleware/MTK/dspalg/ec_nr


C_SRC += $(ECNR_FUNC_PATH)/aec_nr_interface.c

#C_SRC += $(ECNR_FUNC_PATH)/dsp_para_ecnr.c

ifneq ($(MTK_BT_A2DP_ECNR_USE_PIC),y)
	ifeq ($(MTK_INEAR_ENHANCEMENT), y)
	else ifeq ($(MTK_DUALMIC_INEAR), y)
	else ifeq ($(MTK_3RD_PARTY_NR), y)
	else
		LIBS += $(strip $(LIBDIR2))/ec_nr/$(IC_CONFIG)/libecnr50.a
	endif
else
	ifeq ($(MTK_INEAR_ENHANCEMENT), y)
		PIC += $(strip $(LIBDIR2))/ec_nr/$(IC_CONFIG)/pisplit/pisplit_ecnr_inear.o
	else ifeq ($(MTK_DUALMIC_INEAR), y)
		PIC += $(strip $(LIBDIR2))/ec_nr/$(IC_CONFIG)/pisplit/pisplit_ecnr_inear_v2.o
    else ifeq ($(MTK_3RD_PARTY_NR), y)
		PIC += $(strip $(LIBDIR2))/ec_nr/$(IC_CONFIG)/pisplit/pisplit_ec_rxnr.o
	else
		PIC += $(strip $(LIBDIR2))/ec_nr/$(IC_CONFIG)/pisplit/pisplit_ecnr.o
   	endif
	C_SRC += $(ECNR_FUNC_PATH)/portable/ecnr_portable.c
endif

###################################################
# include path


INC += $(ECNR_FUNC_PATH)
INC += $(ECNR_FUNC_PATH)/portable

