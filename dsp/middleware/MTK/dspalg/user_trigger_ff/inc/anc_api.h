#ifndef _UTFF_ANC_API_H
#define _UTFF_ANC_API_H

#include "typedef.h"
#include "anc_status.h"

#define ANC_sample_rate 50000

typedef struct
{
	val16 *PCM_L_buffer;
	val16 *PCM_R_buffer;
	val32 sample_rate;
	val16 frame_length;

	val16 Cal_status;

	val16 PCM_R_buffer2[ANC_sample_rate/100];

	val32 *SCH_mem;
} ANC_ENH_ctrl_struct, *ANC_ENH_ctrl_struct_ptr;

val32 ANC_API_Get_Memory(void);
//val16 ANC_API_Get_Version(ANC_ENH_ctrl_struct *ANC_ENH_ctrl_struct);


val16 ANC_API_Alloc(void *Sph_Enh_ctrl);
//void ANC_API_Update_Parameter(ANC_ENH_ctrl_struct *p_ANC_ENH_ctrl_struct);
val16 ANC_API_Rst(ANC_ENH_ctrl_struct *ANC_ENH_ctrl_struct);
val16 ANC_API_Free(void *Sph_Enh_ctrl);
void ANC_Get_Info(void *Sph_Enh_ctrl, int *sample_rate, int *frame_length, int *Cal_status);

void ANC_API_Process(void *Sph_Enh_ctrl);
val16 ANC_API_Set_Sz_Coef(ANC_ENH_ctrl_struct *ANC_ENH_ctrl_struct, val16 Sz_len, val32 *SzB, val32 *SzA);
val16 ANC_API_Set_FIR_ptr(ANC_ENH_ctrl_struct *ANC_ENH_ctrl_struct, val32 *FIR_ptr);
void ANC_Init(void *Sph_Enh_ctrl, int sample_rate, short frame_length, int *FIR_coef, int *SzB, int *SzA, int Sz_length);
void ANC_Assign_PCM(void *Sph_Enh_ctrl, short *PCM_Buffer_DL, short *PCM_Buffer_UL);

val16 ANC_API_Set_FF_Noise_thd(void *Sph_Enh_ctrl, val16 Noise_Level);
val16 ANC_API_Set_FB_Noise_thd(void *Sph_Enh_ctrl, val16 Noise_Level);
val16 ANC_API_Set_mu(void *Sph_Enh_ctrl, val16 mu);
val16 ANC_API_Set_Shaping_switch(void *Sph_Enh_ctrl, val16 switch_value);
val16 ANC_API_Set_time_end(void *Sph_Enh_ctrl, uval16 parm_set);


val32 ANC_Compare_API_Get_Memory(void);
val16 ANC_Compare_API_Alloc(void *Sph_Enh_ctrl, val32* mem_ptr);
val16 ANC_Compare_API_init(void *Sph_Enh_ctrl);
void ANC_Compare_API_error(void *Sph_Enh_ctrl, void *default_filter, void *New_filter);
val16 ANC_Compare_API_Set_Sz(void *Sph_Enh_ctrl, val16 Sz_len, val32 *SzB, val32 *SzA);
#endif
