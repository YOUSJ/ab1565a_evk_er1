#ifndef __UTFF_TYPEDEF__
#define __UTFF_TYPEDEF__

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define ALIGN16(i) (((i)+(16)-1)&~((16)-1))
#define SIZEOF(i) ALIGN16(sizeof(i))

typedef long long val64;
typedef int val32;
typedef short val16;
typedef unsigned short uval16;
typedef unsigned long uval32;
typedef unsigned long long uval64;

typedef int val24;
typedef long long  val56;
typedef long long  val48;

#define INT24_MAX 2147483392
#define INT24_MIN -2147483648LL

#define MAX_16 32767
#define MIN_16 -32768
#define MAX_u16 65535

#define Adaptive_FF_PCM_LENGTH 500

#define Sz_len_MAX 101
//#define Matlab_input

#endif
