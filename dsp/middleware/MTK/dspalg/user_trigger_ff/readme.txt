Airoha User Trigger adaptive FF module usage guide

Brief:          This module is the Airoha User Trigger adaptive FF implementation.

Usage:          GCC:  For Airoha User Trigger adaptive FF, make sure to include the following:
                      1) Add the following module.mk for libs and source file:
                         include $(SOURCE_DIR)/middleware/MTK/dspalg/user_trigger_ff/module.mk
                      2) Module.mk provides different options to enable or disable according to the profiles.
                         Please configure the related options on the specified XT-XCC/feature.mk.
                         MTK_USER_TRIGGER_FF_ENABLE
                      3) Add the header file path:
                         CFLAGS += -I$(SOURCE_DIR)/middleware/MTK/dspalg/user_trigger_ff/inc
                      4) Add FUNC_USER_TRIGGER_FF into the feature table in dsp_sdk.c to apply User Trigger adaptive FF in the specified scenario,
                         like stream_feature_list_user_trigger_ff[].

Dependency:     User Trigger adaptive FF module uses the protected User Trigger adaptive FF library in $(ROOTDIR)/prebuilt/middleware/MTK/dspalg/user_trigger_ff
                Make sure to include this library in the project Makefile, like dsp/project/ab155x_evk/apps/dsp0_headset_ref_design/XT-XCC/Makefile

Notice:         None

Relative doc:   None

Example project:None
