/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "user_trigger_ff.h"
#include "dsp_callback.h"
#include <assert.h>
#include "dsp_dump.h"
#include "dsp_memory.h"
#include "common.h"
#include "dsp_audio_msg_define.h"
#include "dsp_audio_msg.h"
#include "audio_nvdm_common.h"
#include "anc_driver.h"
#include "stream_cm4_record.h"


#define UTFF_DEBUG_PRINT
#define UTFF_SZ_COEF_ARRAY_LEN      (101)
#define UTFF_FIR_COEF_LENGTH        (300)
#define UTFF_NO_RESPONESE_THD   (5000000)
#define UTFF_UL_SILENCE_TIME     (250000)
#define UTFF_NO_CMD_B_THD      (15000000)

bool utff_enable = false;
anc_user_trigger_adaptive_ff_sz_coef_nvdm_t *sz_coef_nvkey_ptr;
ANC_ENH_ctrl_struct_ptr user_trigger_ff_Sph_Enh_ctrl_ptr = NULL;
int *FIR_coef_ptr;
int *user_trigger_ff_cmp_filter_buff_ptr = NULL;
anc_fwd_iir_t *utff_cmp_filter_ori = NULL, *utff_cmp_filter_new = NULL;

bool FIR_report_flag = false;
bool cmp_filter_flag = false;
bool cmp_filter_report_flag = false;

uint32_t utff_report_start_count = 0;


EXTERN VOID Cm4Record_update_from_share_information(SINK sink);
EXTERN VOID Cm4Record_Reset_Sinkoffset_share_information(SINK sink);


static VOID UserTriggerFF_send_data_ready(uint16_t data_length)
{
    hal_ccni_message_t msg;
    memset((void *)&msg, 0, sizeof(hal_ccni_message_t));
    msg.ccni_message[0] = (MSG_DSP2MCU_RECORD_DATA_NOTIFY << 16) | (data_length);//+1:status
    aud_msg_tx_handler(msg, 0, FALSE);
    DSP_MW_LOG_I("[user_trigger_ff]UserTriggerFF_send_data_ready, data_length=%d", 1, data_length);
}

static VOID UserTriggerFF_write_result(SINK sink, uint32_t cal_status)
{
    DSP_MW_LOG_I("[user_trigger_ff]UserTriggerFF_write_result, cal_status=%d", 1, cal_status);
    /*reset share buff*/
    Cm4Record_update_from_share_information(sink);
    Cm4Record_Reset_Sinkoffset_share_information(sink);

    /*write status to share buff*/
    if(!SinkWriteBuf(sink, (U8*)&cal_status, (U32)sizeof(uint32_t))) {
        DSP_MW_LOG_I("[user_trigger_ff]SinkWriteBuf status FAIL!!!!", 0);
    }
    if(!SinkFlush(sink, (U32)sizeof(uint32_t))) {
        DSP_MW_LOG_I("[user_trigger_ff]SinkFlush status FAIL!!!!", 0);
    }

    if (cal_status <= ANC_K_STATUS_DONE) {
        /*write coefs to share buff*/
        if(!SinkWriteBuf(sink, (U8*)FIR_coef_ptr, (U32)sizeof(int)*UTFF_FIR_COEF_LENGTH)) {
            DSP_MW_LOG_I("[user_trigger_ff]SinkWriteBuf coef FAIL!!!!", 0);
        }
        if(!SinkFlush(sink, (U32)sizeof(int)*UTFF_FIR_COEF_LENGTH)) {
            DSP_MW_LOG_I("[user_trigger_ff]SinkFlush coef FAIL!!!!", 0);
        }

        UserTriggerFF_send_data_ready(1+UTFF_FIR_COEF_LENGTH);
        return;
    }

    UserTriggerFF_send_data_ready(1);

}

void UserTriggerFF_reset_share_buff(SINK sink)
{
    /*reset share buff*/
    Cm4Record_update_from_share_information(sink);
    Cm4Record_Reset_Sinkoffset_share_information(sink);
}

bool stream_function_user_trigger_ff_initialize(void *para)
{
    DSP_STREAMING_PARA_PTR  stream_ptr;
    DSP_ENTRY_PARA_PTR entry_para = (DSP_ENTRY_PARA_PTR)para;
    stream_ptr = DSP_STREAMING_GET_FROM_PRAR(entry_para);
    int  mem_size = 0;

    /*initailize*/
    FIR_report_flag = false;
    cmp_filter_flag = false;
    cmp_filter_report_flag = false;

    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &utff_report_start_count);

    /*Allocate memory*/
    if(user_trigger_ff_Sph_Enh_ctrl_ptr){
        DSPMEM_Free(entry_para->DSPTask, user_trigger_ff_Sph_Enh_ctrl_ptr);
        DSP_MW_LOG_W("[user_trigger_ff]utff_mem_ptr!=NULL, free mem\r\n", 0);
    }
    if(FIR_coef_ptr){
        DSPMEM_Free(entry_para->DSPTask, FIR_coef_ptr);
        DSP_MW_LOG_W("[user_trigger_ff]FIR_coef_ptr!=NULL, free mem\r\n", 0);
    }
    if(sz_coef_nvkey_ptr){
        DSPMEM_Free(entry_para->DSPTask, sz_coef_nvkey_ptr);
        DSP_MW_LOG_W("[user_trigger_ff]sz_coef_nvkey_ptr!=NULL, free mem\r\n", 0);
    }

    mem_size = ANC_API_Get_Memory();
    user_trigger_ff_Sph_Enh_ctrl_ptr = DSPMEM_tmalloc(entry_para->DSPTask, mem_size, stream_ptr);

    FIR_coef_ptr = DSPMEM_tmalloc(entry_para->DSPTask, (sizeof(int) * UTFF_FIR_COEF_LENGTH), stream_ptr);
    memset(FIR_coef_ptr, 0, (sizeof(int) * UTFF_FIR_COEF_LENGTH));

    sz_coef_nvkey_ptr = DSPMEM_tmalloc(entry_para->DSPTask, sizeof(anc_user_trigger_adaptive_ff_sz_coef_nvdm_t), stream_ptr);

    if (!FIR_coef_ptr) {
        DSP_MW_LOG_E("[user_trigger_ff]FIR_coef_ptr==NULL\r\n", 0);
        assert(0);
    }
    if (!sz_coef_nvkey_ptr) {
        DSP_MW_LOG_E("[user_trigger_ff]sz_coef_nvkey_ptr==NULL\r\n", 0);
        assert(0);
    }


    /*read params nvkey*/
    anc_user_trigger_adaptive_ff_param_nvdm_t *param_nvkey_ptr = DSPMEM_tmalloc(entry_para->DSPTask, sizeof(anc_user_trigger_adaptive_ff_param_nvdm_t), stream_ptr);
    if (param_nvkey_ptr) {
        nvkey_read_full_key(NVKEY_DSP_PARA_ADAPTIVE_FF, (void *)param_nvkey_ptr, sizeof(anc_user_trigger_adaptive_ff_param_nvdm_t));
        DSP_MW_LOG_I("[user_trigger_ff]read param, FF_Noise_thd: %d, FB_Noise_thd: %d, stepsize: %d, Shaping_switch: %d, time_end: %d\r\n", 5, param_nvkey_ptr->FF_Noise_thd, param_nvkey_ptr->FB_Noise_thd, param_nvkey_ptr->stepsize, param_nvkey_ptr->Shaping_switch, param_nvkey_ptr->time_end);
    } else {
        DSP_MW_LOG_E("[user_trigger_ff]param_nvkey_ptr==NULL\r\n", 0);
        assert(0);
    }

    /*read Sz coef nvkey*/
    nvkey_read_full_key(NVKEY_DSP_PARA_ADAPTIVE_FF_SZ_COEF, (void *)sz_coef_nvkey_ptr, sizeof(anc_user_trigger_adaptive_ff_sz_coef_nvdm_t));


    #ifdef UTFF_DEBUG_PRINT
    DSP_MW_LOG_I("[user_trigger_ff]read Sz coef array, length: %d\r\n", 1, sz_coef_nvkey_ptr->Sz_length);

    DSP_MW_LOG_I("[user_trigger_ff]==========array B==========\r\n", 0);
    for (int i=0; i<1; i++) {
        DSP_MW_LOG_I("row %2d | %10d %10d %10d %10d %10d %10d %10d %10d %10d %10d\r\n", 11, i, 
            *(sz_coef_nvkey_ptr->utff_sz_coef_array_B+i*10), *(sz_coef_nvkey_ptr->utff_sz_coef_array_B+i*10+1), *(sz_coef_nvkey_ptr->utff_sz_coef_array_B+i*10+2), *(sz_coef_nvkey_ptr->utff_sz_coef_array_B+i*10+3), *(sz_coef_nvkey_ptr->utff_sz_coef_array_B+i*10+4),
            *(sz_coef_nvkey_ptr->utff_sz_coef_array_B+i*10+5), *(sz_coef_nvkey_ptr->utff_sz_coef_array_B+i*10+6), *(sz_coef_nvkey_ptr->utff_sz_coef_array_B+i*10+7), *(sz_coef_nvkey_ptr->utff_sz_coef_array_B+i*10+8), *(sz_coef_nvkey_ptr->utff_sz_coef_array_B+i*10+9));
    }

    DSP_MW_LOG_I("[user_trigger_ff]==========array A==========\r\n", 0);
    for (int i=0; i<1; i++) {
        DSP_MW_LOG_I("row %2d | %10d %10d %10d %10d %10d %10d %10d %10d %10d %10d\r\n", 11, i, 
            *(sz_coef_nvkey_ptr->utff_sz_coef_array_A+i*10), *(sz_coef_nvkey_ptr->utff_sz_coef_array_A+i*10+1), *(sz_coef_nvkey_ptr->utff_sz_coef_array_A+i*10+2), *(sz_coef_nvkey_ptr->utff_sz_coef_array_A+i*10+3), *(sz_coef_nvkey_ptr->utff_sz_coef_array_A+i*10+4),
            *(sz_coef_nvkey_ptr->utff_sz_coef_array_A+i*10+5), *(sz_coef_nvkey_ptr->utff_sz_coef_array_A+i*10+6), *(sz_coef_nvkey_ptr->utff_sz_coef_array_A+i*10+7), *(sz_coef_nvkey_ptr->utff_sz_coef_array_A+i*10+8), *(sz_coef_nvkey_ptr->utff_sz_coef_array_A+i*10+9));
    }
    #endif



    if(user_trigger_ff_Sph_Enh_ctrl_ptr){
        ANC_API_Alloc(user_trigger_ff_Sph_Enh_ctrl_ptr);

        ANC_Init(user_trigger_ff_Sph_Enh_ctrl_ptr, ANC_sample_rate, Adaptive_FF_PCM_LENGTH, FIR_coef_ptr, sz_coef_nvkey_ptr->utff_sz_coef_array_B, sz_coef_nvkey_ptr->utff_sz_coef_array_A, sz_coef_nvkey_ptr->Sz_length);

        ANC_API_Set_FF_Noise_thd(user_trigger_ff_Sph_Enh_ctrl_ptr, 1000);
    	ANC_API_Set_FB_Noise_thd(user_trigger_ff_Sph_Enh_ctrl_ptr, 1000);
    	ANC_API_Set_mu(user_trigger_ff_Sph_Enh_ctrl_ptr, param_nvkey_ptr->stepsize);
        ANC_API_Set_Shaping_switch(user_trigger_ff_Sph_Enh_ctrl_ptr, param_nvkey_ptr->Shaping_switch);
        ANC_API_Set_time_end(user_trigger_ff_Sph_Enh_ctrl_ptr, 16384);
    } else{
        DSP_MW_LOG_E("[user_trigger_ff]user_trigger_ff_Sph_Enh_ctrl_ptr==NULL\r\n", 0);
        assert(0);
    }

    DSP_MW_LOG_I("[user_trigger_ff]initialize, sample rate = %d, frame length = %d, mem_size = %d", 3,
            stream_function_get_samplingrate(para) * 1000,
            Adaptive_FF_PCM_LENGTH,
            mem_size);

    DSPMEM_Free(entry_para->DSPTask, param_nvkey_ptr);

    /*comparing filter mem alloc*/

    if(user_trigger_ff_cmp_filter_buff_ptr){
        DSPMEM_Free(entry_para->DSPTask, user_trigger_ff_cmp_filter_buff_ptr);
        printf("[user_trigger_ff]user_trigger_ff_cmp_filter_buff_ptr!=NULL, free mem\r\n");
    }
    if(utff_cmp_filter_ori){
        DSPMEM_Free(entry_para->DSPTask, utff_cmp_filter_ori);
        printf("[user_trigger_ff]utff_cmp_filter_ori!=NULL, free mem\r\n");
    }
    if(utff_cmp_filter_new){
        DSPMEM_Free(entry_para->DSPTask, utff_cmp_filter_new);
        printf("[user_trigger_ff]utff_cmp_filter_new!=NULL, free mem\r\n");
    }
    mem_size = ANC_Compare_API_Get_Memory();
    DSP_MW_LOG_I("[user_trigger_ff]Compare API mem_size = %d", 1, mem_size);

    user_trigger_ff_cmp_filter_buff_ptr = DSPMEM_tmalloc(entry_para->DSPTask, mem_size, stream_ptr);
    if (!user_trigger_ff_cmp_filter_buff_ptr) {
        DSP_MW_LOG_E("[user_trigger_ff]user_trigger_ff_cmp_filter_buff_ptr==NULL\r\n", 0);
        assert(0);
    }

    utff_cmp_filter_ori = DSPMEM_tmalloc(entry_para->DSPTask, (sizeof(anc_fwd_iir_t)), stream_ptr);
    if (!utff_cmp_filter_ori) {
        DSP_MW_LOG_E("[user_trigger_ff]utff_cmp_filter_ori==NULL\r\n", 0);
        assert(0);
    }

    utff_cmp_filter_new = DSPMEM_tmalloc(entry_para->DSPTask, (sizeof(anc_fwd_iir_t)), stream_ptr);
    if (!utff_cmp_filter_new) {
        DSP_MW_LOG_E("[user_trigger_ff]utff_cmp_filter_new==NULL\r\n", 0);
        assert(0);
    }
    return 0;
}

bool stream_function_user_trigger_ff_process(void *para)
{
    DSP_ENTRY_PARA_PTR entry_para = (DSP_ENTRY_PARA_PTR)para;
    DSP_STREAMING_PARA_PTR  stream_ptr = DSP_STREAMING_GET_FROM_PRAR(entry_para);
    S16* InBufLeft     = (S16*)stream_function_get_1st_inout_buffer(para);
    S16* InBufRight     = (S16*)stream_function_get_2nd_inout_buffer(para);
//    U32 out_size = stream_ptr->callback.Src.out_frame_size;
    uint32_t utff_report_cur_count = 0;
    uint32_t utff_report_duration_count = 0;
    uint32_t utff_process_start_count = 0;
    uint32_t utff_process_end_count = 0;
    uint32_t utff_process_duration_count = 0;
    int   sample_rate, frame_length, Cal_status;


    /*Step 1: calculate 300 Tap.*/
    if (!cmp_filter_flag) {
        ANC_Assign_PCM(user_trigger_ff_Sph_Enh_ctrl_ptr, InBufLeft, InBufRight);

        /*get time*/
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &utff_report_cur_count);
        hal_gpt_get_duration_count(utff_report_start_count, utff_report_cur_count, &utff_report_duration_count);

        if (utff_report_duration_count > UTFF_UL_SILENCE_TIME) {

            /*audio dump would lose data in 50K sampling rate, so dump 48K  in stream_pcm_copy_process() now */
//            #ifdef MTK_AUDIO_DUMP_BY_CONFIGTOOL
//            LOG_AUDIO_DUMP((U8*)InBufLeft, out_size, VOICE_TX_MIC_0);
//            LOG_AUDIO_DUMP((U8*)InBufRight, out_size, VOICE_TX_MIC_1);
//            #endif

            hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &utff_process_start_count);
            ANC_API_Process(user_trigger_ff_Sph_Enh_ctrl_ptr);
            hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &utff_process_end_count);
            hal_gpt_get_duration_count(utff_process_start_count, utff_process_end_count, &utff_process_duration_count);

            ANC_Get_Info(user_trigger_ff_Sph_Enh_ctrl_ptr, &sample_rate, &frame_length, &Cal_status);


        //    printf("[user_trigger_ff]process, src out_size=%d, sample_rate=%d, frame_length=%d, Cal_status=%d, process_duration=%d us", out_size, sample_rate, frame_length, Cal_status, utff_process_duration_count);

            if ((Cal_status == ANC_K_STATUS_DONE) && (!FIR_report_flag)) {
                FIR_report_flag = true;

                UserTriggerFF_write_result(stream_ptr->sink, Cal_status);

                DSP_MW_LOG_I("[user_trigger_ff]Done, status:%d, reported:%d", 2, Cal_status, FIR_report_flag);



                #ifdef UTFF_DEBUG_PRINT
                    DSP_MW_LOG_I("[user_trigger_ff]FIR_coef:\n", 0);
                    for (int i=0; i<UTFF_FIR_COEF_LENGTH; i+=10) {
                        DSP_MW_LOG_I("index %3d | %10d %10d %10d %10d %10d %10d %10d %10d %10d %10d\r\n", 11, i,
                            *(FIR_coef_ptr+i), *(FIR_coef_ptr+i+1), *(FIR_coef_ptr+i+2), *(FIR_coef_ptr+i+3), *(FIR_coef_ptr+i+4),
                            *(FIR_coef_ptr+i+5), *(FIR_coef_ptr+i+6), *(FIR_coef_ptr+i+7), *(FIR_coef_ptr+i+8), *(FIR_coef_ptr+i+9));
                    }
                #endif

            } else if ((!FIR_report_flag) && (utff_report_duration_count >= UTFF_NO_RESPONESE_THD)) {

                FIR_report_flag = true;

                UserTriggerFF_write_result(stream_ptr->sink, Cal_status);

                DSP_MW_LOG_I("[user_trigger_ff]no response within %d sec, status:%d, reported:%d", 3, UTFF_NO_RESPONESE_THD/1000000, Cal_status, FIR_report_flag);

                #ifdef UTFF_DEBUG_PRINT
                    DSP_MW_LOG_I("[user_trigger_ff]FIR_coef:\n", 0);
                    for (int i=0; i<UTFF_FIR_COEF_LENGTH; i+=10) {
                        DSP_MW_LOG_I("index %3d | %10d %10d %10d %10d %10d %10d %10d %10d %10d %10d\r\n", 11, i,
                            *(FIR_coef_ptr+i), *(FIR_coef_ptr+i+1), *(FIR_coef_ptr+i+2), *(FIR_coef_ptr+i+3), *(FIR_coef_ptr+i+4),
                            *(FIR_coef_ptr+i+5), *(FIR_coef_ptr+i+6), *(FIR_coef_ptr+i+7), *(FIR_coef_ptr+i+8), *(FIR_coef_ptr+i+9));
                    }
                #endif
            } else if (FIR_report_flag && (utff_report_duration_count >= UTFF_NO_CMD_B_THD)) {

                UserTriggerFF_write_result(stream_ptr->sink, DSP_USER_TRIGGER_FF_NO_CMD_B_STATUS);
                DSP_MW_LOG_I("[user_trigger_ff]no race cmd B within %d sec, stop record", 1, UTFF_NO_CMD_B_THD/1000000);

            }
        }

    /*Step 2: Compare with new filter.*/
    } else {

        /*initialize*/
#ifdef UTFF_DEBUG_PRINT
        DSP_MW_LOG_I("[user_trigger_ff]comparing filter initialize\n", 0);

        DSP_MW_LOG_I("cmp_filter_flag:%d, filter_unique_gain=%d, iir_filter_coef_B00=%d, shift_a=%d, shift_b=%d", 5, cmp_filter_flag, utff_cmp_filter_new->filter_unique_gain, utff_cmp_filter_new->iir_filter_coef_B00, utff_cmp_filter_new->shift_a, utff_cmp_filter_new->shift_b);
        DSP_MW_LOG_I("iir_filter_coef[64]:", 0);
        for (int i=0; i<64; i+=8) {
            DSP_MW_LOG_I("%d, %d, %d, %d, %d, %d, %d, %d", 8,
                *(utff_cmp_filter_new->iir_filter_coef+i), *(utff_cmp_filter_new->iir_filter_coef+i+1), *(utff_cmp_filter_new->iir_filter_coef+i+2), *(utff_cmp_filter_new->iir_filter_coef+i+3), 
                *(utff_cmp_filter_new->iir_filter_coef+i+4), *(utff_cmp_filter_new->iir_filter_coef+i+5), *(utff_cmp_filter_new->iir_filter_coef+i+6), *(utff_cmp_filter_new->iir_filter_coef+i+7));
        }
#endif
        ANC_Get_Info(user_trigger_ff_Sph_Enh_ctrl_ptr, &sample_rate, &frame_length, &Cal_status);

        ANC_Compare_API_Alloc(user_trigger_ff_Sph_Enh_ctrl_ptr, user_trigger_ff_cmp_filter_buff_ptr);

        ANC_Compare_API_init(user_trigger_ff_Sph_Enh_ctrl_ptr);

        ANC_Compare_API_Set_Sz(user_trigger_ff_Sph_Enh_ctrl_ptr, sz_coef_nvkey_ptr->Sz_length, sz_coef_nvkey_ptr->utff_sz_coef_array_B, sz_coef_nvkey_ptr->utff_sz_coef_array_A);


        /*process*/
        ANC_Assign_PCM(user_trigger_ff_Sph_Enh_ctrl_ptr, InBufLeft, InBufRight);

        ANC_Compare_API_error(user_trigger_ff_Sph_Enh_ctrl_ptr, utff_cmp_filter_ori, utff_cmp_filter_new);

        ANC_Get_Info(user_trigger_ff_Sph_Enh_ctrl_ptr, &sample_rate, &frame_length, &Cal_status);

        if ((Cal_status > ANC_K_STATUS_Compare) && (!cmp_filter_report_flag)) {
            cmp_filter_report_flag = true;

            UserTriggerFF_write_result(stream_ptr->sink, Cal_status);

            DSP_MW_LOG_I("[user_trigger_ff]comparing filter done, status:%d, reported:%d", 2, Cal_status, cmp_filter_report_flag);

        }
    }
    return 0;
}

