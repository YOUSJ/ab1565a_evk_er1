/*
 * Copyright (c) 2009-2017 by Cadence Design Systems, Inc.  ALL RIGHTS RESERVED.
 * These coded instructions, statements, and computer programs are the
 * copyrighted works and confidential proprietary information of
 * Cadence Design Systems Inc.  They may be adapted and modified by bona fide
 * purchasers for internal use, but neither the original nor any adapted
 * or modified version may be disclosed or distributed to third parties
 * in any manner, medium, or form, in whole or in part, without the prior
 * written consent of Cadence Design Systems Inc.  This software and its
 * derivatives are to be executed solely on products incorporating a Cadence
 * Design Systems processor.
 */

#ifndef  __XA_TYPE_DEF_H__
#define  __XA_TYPE_DEF_H__


#define PLATFORM_INLINE __inline

typedef struct xa_codec_opaque { signed int _; } *xa_codec_handle_t;

typedef signed int XA_ERRORCODE;

typedef XA_ERRORCODE xa_codec_func_t(xa_codec_handle_t p_xa_module_obj,
				     signed int            i_cmd,
				     signed int            i_idx,
				     void*             pv_value);




#endif /* __XA_TYPE_DEF_H__ */

#ifndef __XA_ERROR_STANDARDS_H__
#define __XA_ERROR_STANDARDS_H__


#define XA_NO_ERROR	0
/* error handling 'AND' definition */
#define XA_FATAL_ERROR	0x80000000

enum xa_error_severity {
  xa_severity_nonfatal = 0,
  xa_severity_fatal    = 0xffffffff
};

enum xa_error_class {
  xa_class_api     = 0,
  xa_class_config  = 1,
  xa_class_execute = 2
};

#define XA_CODEC_GENERIC	0

#define XA_ERROR_CODE(severity, class, codec, index)	((severity << 15) | (class << 11) | (codec << 6) | index)
#define XA_ERROR_SEVERITY(code)	(((code) & XA_FATAL_ERROR) != 0)
#define XA_ERROR_CLASS(code)	(((code) >> 11) & 0x0f)
#define XA_ERROR_CODEC(code)    (((code) >>  6) & 0x1f)
#define XA_ERROR_SUBCODE(code)	(((code) >>  0) & 0x3f)

/* Our convention is that only api-class errors can be generic ones. */

/*****************************************************************************/
/* Class 0: API Errors                                                       */
/*****************************************************************************/
/* Non Fatal Errors */
/* (none) */
/* Fatal Errors */
enum xa_error_fatal_api_generic {
  XA_API_FATAL_MEM_ALLOC        = XA_ERROR_CODE(xa_severity_fatal, xa_class_api, XA_CODEC_GENERIC, 0),
  XA_API_FATAL_MEM_ALIGN        = XA_ERROR_CODE(xa_severity_fatal, xa_class_api, XA_CODEC_GENERIC, 1),
  XA_API_FATAL_INVALID_CMD      = XA_ERROR_CODE(xa_severity_fatal, xa_class_api, XA_CODEC_GENERIC, 2),
  XA_API_FATAL_INVALID_CMD_TYPE = XA_ERROR_CODE(xa_severity_fatal, xa_class_api, XA_CODEC_GENERIC, 3)
};

#endif /* __XA_ERROR_STANDARDS_H__ */




#ifndef __XA_AAC_DEC_API_H__
#define __XA_AAC_DEC_API_H__

/* aac_dec-specific configuration parameters */
enum xa_config_param_aac_dec {
  XA_AACDEC_CONFIG_PARAM_BDOWNSAMPLE          = 0,    /* Applicable only for aac*plus* libraries */
  XA_AACDEC_CONFIG_PARAM_BBITSTREAMDOWNMIX    = 1,    /* Applicable only for aac*plus* libraries */
  XA_AACDEC_CONFIG_PARAM_EXTERNAL_SAMPLERATE  = 2,
#define XA_AACDEC_CONFIG_PARAM_EXTERNALSAMPLINGRATE XA_AACDEC_CONFIG_PARAM_EXTERNAL_SAMPLERATE // Renamed, maintained for backward comapatible
  XA_AACDEC_CONFIG_PARAM_EXTERNALBSFORMAT     = 3,
  XA_AACDEC_CONFIG_PARAM_TO_STEREO            = 4,
  XA_AACDEC_CONFIG_PARAM_OUT_SAMPLERATE       = 5,
#define  XA_AACDEC_CONFIG_PARAM_SAMP_FREQ           XA_AACDEC_CONFIG_PARAM_OUT_SAMPLERATE // Renamed, maintained for backward comapatible
  XA_AACDEC_CONFIG_PARAM_NUM_CHANNELS         = 6,
  XA_AACDEC_CONFIG_PARAM_PCM_WDSZ             = 7,
  XA_AACDEC_CONFIG_PARAM_SBR_TYPE             = 8,
  XA_AACDEC_CONFIG_PARAM_AAC_SAMPLERATE       = 9,
  XA_AACDEC_CONFIG_PARAM_DATA_RATE            = 10,
  XA_AACDEC_CONFIG_PARAM_OUTNCHANS            = 11,
  XA_AACDEC_CONFIG_PARAM_CHANROUTING          = 12,
  XA_AACDEC_CONFIG_PARAM_SBR_SIGNALING        = 13,    /* Applicable only for aac*plus* libraries */
  XA_AACDEC_CONFIG_PARAM_CHANMAP              = 14,
  XA_AACDEC_CONFIG_PARAM_ACMOD                = 15,
  XA_AACDEC_CONFIG_PARAM_AAC_FORMAT           = 16,
  XA_AACDEC_CONFIG_PARAM_ZERO_UNUSED_CHANS    = 17,
  XA_AACDEC_CONFIG_PARAM_DECODELAYERS         = 18,   /* Depricated, no more implemented */
  XA_AACDEC_CONFIG_PARAM_EXTERNALCHCONFIG     = 19,   /* Depricated, no more implemented */
  XA_AACDEC_CONFIG_PARAM_RAW_AU_SIDEINFO      = 20,   /* For DAB-plus only */
  XA_AACDEC_CONFIG_PARAM_EXTERNALBITRATE      = 21,   /* For DAB-plus only */
  XA_AACDEC_CONFIG_PARAM_PAD_SIZE             = 22,   /* For DAB-plus only */
  XA_AACDEC_CONFIG_PARAM_PAD_PTR              = 23,   /* For DAB-plus only */
  XA_AACDEC_CONFIG_PARAM_MPEGSURR_PRESENT     = 24,   /* For DAB-plus only */
  XA_AACDEC_CONFIG_PARAM_METADATASTRUCT_PTR   = 25,   /* Only if Audio MetaData support is present for the library */
  XA_AACDEC_CONFIG_PARAM_ASCONFIGSTRUCT_PTR   = 26,   /* Only if Audio MetaData support is present for the library */
  XA_AACDEC_CONFIG_PARAM_LIMITBANDWIDTH       = 27,   /* Depricated, no more implemented */
  XA_AACDEC_CONFIG_PARAM_PCE_STATUS           = 28,   /* for Loas build only */
  XA_AACDEC_CONFIG_PARAM_DWNMIX_METADATA      = 29,   /* for Loas build only */
  XA_AACDEC_CONFIG_PARAM_MPEG_ID              = 30,   /* Applicable only for adts streams */
  XA_AACDEC_CONFIG_PARAM_DWNMIX_LEVEL_DVB     = 31    /* for Loas build only */
  /* DRC and PRL information as per ISO/IEC 14496.3 */
  /* PRL Parametbers */
  ,XA_AACDEC_CONFIG_PARAM_ENABLE_APPLY_PRL     = 32     /* for Loas build only */
  ,XA_AACDEC_CONFIG_PARAM_TARGET_LEVEL         = 33     /* for Loas build only */
  ,XA_AACDEC_CONFIG_PARAM_PROG_REF_LEVEL       = 34     /* for Loas build only */
  /* DRC Parametbers */
  ,XA_AACDEC_CONFIG_PARAM_ENABLE_APPLY_DRC     = 35     /* for Loas build only */
  ,XA_AACDEC_CONFIG_PARAM_DRC_COMPRESS_FAC     = 36     /* for Loas build only */
  ,XA_AACDEC_CONFIG_PARAM_DRC_BOOST_FAC        = 37     /* for Loas build only */
  ,XA_AACDEC_CONFIG_PARAM_DRC_EXT_PRESENT      = 38
  ,XA_AACDEC_CONFIG_PARAM_ORIGINAL_OR_COPY     = 39     /* for ADTS and ADIF files only */
  ,XA_AACDEC_CONFIG_PARAM_COPYRIGHT_ID_PTR     = 40     /* for ADTS and ADIF files only */
  ,XA_AACDEC_CONFIG_PARAM_PARSED_DRC_INFO      = 41     /* applicable only for aacmch* builds */
  ,XA_AACDEC_CONFIG_PARAM_INPUT_BITOFFSET      = 42
  ,XA_AACDEC_CONFIG_PARAM_ENABLE_FRAME_BY_FRAME_DECODE      = 43
  ,XA_AACDEC_CONFIG_PARAM_CONCEALMENT_FADE_OUT_FRAMES       = 44
  ,XA_AACDEC_CONFIG_PARAM_CONCEALMENT_MUTE_RELEASE_FRAMES   = 45
  ,XA_AACDEC_CONFIG_PARAM_CONCEALMENT_FADE_IN_FRAMES        = 46
};

/* Types of channel modes (acmod) */
typedef enum {
  XA_AACDEC_CHANNELMODE_UNDEFINED = 0,        // undefined
  XA_AACDEC_CHANNELMODE_MONO,                 // mono (1/0 )
  XA_AACDEC_CHANNELMODE_PARAMETRIC_STEREO,    // parametric stereo (aacPlus v2 only)
  XA_AACDEC_CHANNELMODE_DUAL_CHANNEL,         // dual mono (1/0 + 1/0)
  XA_AACDEC_CHANNELMODE_STEREO,               // stereo (2/0)
  XA_AACDEC_CHANNELMODE_3_CHANNEL_FRONT,      // L, C, R (3/0)
  XA_AACDEC_CHANNELMODE_3_CHANNEL_SURR,       // L, R, l (2/1)
  XA_AACDEC_CHANNELMODE_4_CHANNEL_2SURR,      // L, R, l, r (2/2)
  XA_AACDEC_CHANNELMODE_4_CHANNEL_1SURR,      // L, C, R, Cs (3/0/1)
  XA_AACDEC_CHANNELMODE_5_CHANNEL,            // L, C, R, l, r (3/2)
  XA_AACDEC_CHANNELMODE_6_CHANNEL,            // L, C, R, l, r, Cs (3/2/1)
  XA_AACDEC_CHANNELMODE_7_CHANNEL,            // L, C, R, l, r, Sbl, Sbr (3/2/2)
  XA_AACDEC_CHANNELMODE_2_1_STEREO,           // L, R, LFE (2/0.1)
  XA_AACDEC_CHANNELMODE_3_1_CHANNEL_FRONT,    // L, C, R, LFE (3/0.1)
  XA_AACDEC_CHANNELMODE_3_1_CHANNEL_SURR,     // L, R, Cs, LFE (2/0/1.1)
  XA_AACDEC_CHANNELMODE_4_1_CHANNEL_2SURR,    // L, R, Ls, Rs, LFE (2/2.1)
  XA_AACDEC_CHANNELMODE_4_1_CHANNEL_1SURR,    // L, C, R, Cs, LFE (3/0/1.1)
  XA_AACDEC_CHANNELMODE_5_1_CHANNEL,          // L, C, R, l, r, LFE (5.1 mode)
  XA_AACDEC_CHANNELMODE_6_1_CHANNEL,          // L, C, R, l, r, Cs, LFE (3/2/1.1)
  XA_AACDEC_CHANNELMODE_7_1_CHANNEL           // L, C, R, l, r, Sbl, Sbr, LFE (7.1 mode)
} XA_AACDEC_CHANNELMODE;

/* Types of bitstreams */
typedef enum {
  /* The bitstream type has not (yet) been successfully determined. */
  XA_AACDEC_EBITSTREAM_TYPE_UNKNOWN = 0,
  /* ADIF is an unsynced, unframed format. Errors in the bitstream cannot always
     be detected, and when they occur, no further parsing is possible. Avoid ADIF at
     all costs. */
  XA_AACDEC_EBITSTREAM_TYPE_AAC_ADIF = 1,
  /* ADTS is a simple synced framing format similar to MPEG layer-3. */
  XA_AACDEC_EBITSTREAM_TYPE_AAC_ADTS = 2,
  /* LATM, with in-band config. This format cannot be detected by the library;
     it needs to be signaled explicitely. */
  XA_AACDEC_EBITSTREAM_TYPE_AAC_LATM = 3,
  /* LATM, with out of band config. This format is not supported. */
  XA_AACDEC_EBITSTREAM_TYPE_AAC_LATM_OUTOFBAND_CONFIG = 4,
  /* Low overhead audio stream. */
  XA_AACDEC_EBITSTREAM_TYPE_AAC_LOAS = 5,

  /* Raw bitstream. This format cannot be detected by the library;
     it needs to be signaled explicitly. */
  XA_AACDEC_EBITSTREAM_TYPE_AAC_RAW = 6,

  /* Raw DAB+ bitstream. It needs sideInfo for every frame for error recovery */
  XA_AACDEC_EBITSTREAM_TYPE_DABPLUS_RAW_SIDEINFO = 8,

  /* DAB+ audio superframe bitstream */
  XA_AACDEC_EBITSTREAM_TYPE_DABPLUS = 9

} XA_AACDEC_EBITSTREAM_TYPE;

/* commands */


#ifndef __XA_API_CMD_STANDARDS_H__
#define __XA_API_CMD_STANDARDS_H__

/*****************************************************************************/
/* Standard API commands                                                     */
/*****************************************************************************/

enum xa_api_cmd_generic {
  XA_API_CMD_GET_LIB_ID_STRINGS	      = 0x0001,

  XA_API_CMD_GET_API_SIZE             = 0x0002,
  XA_API_CMD_INIT                     = 0x0003,

  XA_API_CMD_SET_CONFIG_PARAM         = 0x0004,
  XA_API_CMD_GET_CONFIG_PARAM         = 0x0005,

  XA_API_CMD_GET_MEMTABS_SIZE         = 0x0006,
  XA_API_CMD_SET_MEMTABS_PTR          = 0x0007,
  XA_API_CMD_GET_N_MEMTABS            = 0x0008,

  XA_API_CMD_EXECUTE                  = 0x0009,

  XA_API_CMD_PUT_INPUT_QUERY          = 0x000A,
  XA_API_CMD_GET_CURIDX_INPUT_BUF     = 0x000B,
  XA_API_CMD_SET_INPUT_BYTES          = 0x000C,
  XA_API_CMD_GET_OUTPUT_BYTES         = 0x000D,
  XA_API_CMD_INPUT_OVER               = 0x000E,

  XA_API_CMD_GET_MEM_INFO_SIZE        = 0x0010,
  XA_API_CMD_GET_MEM_INFO_ALIGNMENT   = 0x0011,
  XA_API_CMD_GET_MEM_INFO_TYPE        = 0x0012,
  XA_API_CMD_GET_MEM_INFO_PLACEMENT   = 0x0013,
  XA_API_CMD_GET_MEM_INFO_PRIORITY    = 0x0014,
  XA_API_CMD_SET_MEM_PTR              = 0x0015,
  XA_API_CMD_SET_MEM_INFO_SIZE        = 0x0016,
  XA_API_CMD_SET_MEM_PLACEMENT        = 0x0017,

  XA_API_CMD_GET_N_TABLES             = 0x0018,
  XA_API_CMD_GET_TABLE_INFO_SIZE      = 0x0019,
  XA_API_CMD_GET_TABLE_INFO_ALIGNMENT = 0x001A,
  XA_API_CMD_GET_TABLE_INFO_PRIORITY  = 0x001B,
  XA_API_CMD_SET_TABLE_PTR            = 0x001C,
  XA_API_CMD_GET_TABLE_PTR            = 0x001D
};

/*****************************************************************************/
/* Standard API command indices                                              */
/*****************************************************************************/

enum xa_cmd_type_generic {
  /* XA_API_CMD_GET_LIB_ID_STRINGS indices */
  XA_CMD_TYPE_LIB_NAME                    = 0x0100,
  XA_CMD_TYPE_LIB_VERSION                 = 0x0200,
  XA_CMD_TYPE_API_VERSION                 = 0x0300,

  /* XA_API_CMD_INIT indices */
  XA_CMD_TYPE_INIT_API_PRE_CONFIG_PARAMS  = 0x0100,
  XA_CMD_TYPE_INIT_API_POST_CONFIG_PARAMS = 0x0200,
  XA_CMD_TYPE_INIT_PROCESS                = 0x0300,
  XA_CMD_TYPE_INIT_DONE_QUERY             = 0x0400,

  /* XA_API_CMD_EXECUTE indices */
  XA_CMD_TYPE_DO_EXECUTE                  = 0x0100,
  XA_CMD_TYPE_DONE_QUERY                  = 0x0200,
  XA_CMD_TYPE_DO_RUNTIME_INIT             = 0x0300,
};

/*****************************************************************************/
/* Standard API configuration parameters                                     */
/*****************************************************************************/

enum xa_config_param_generic {
  XA_CONFIG_PARAM_CUR_INPUT_STREAM_POS    = 0x0100,
  XA_CONFIG_PARAM_GEN_INPUT_STREAM_POS    = 0x0200,
};

#endif /* __XA_API_CMD_STANDARDS_H__ */




#define XA_CODEC_AAC_DEC 3


enum xa_error_nonfatal_api_aac_dec {
  XA_AACDEC_API_NONFATAL_CMD_TYPE_NOT_SUPPORTED = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_api, XA_CODEC_AAC_DEC, 0),
  XA_AACDEC_API_NONFATAL_INVALID_API_SEQ        = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_api, XA_CODEC_AAC_DEC, 1)
};

enum xa_error_fatal_api_aac_dec {
  XA_AACDEC_API_FATAL_INVALID_API_SEQ        = XA_ERROR_CODE(xa_severity_fatal, xa_class_api, XA_CODEC_AAC_DEC, 4)
};

/* Nonfatal Errors */
enum xa_error_nonfatal_config_aac_dec {
  XA_AACDEC_CONFIG_NONFATAL_PARAMS_NOT_SET          = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_config, XA_CODEC_AAC_DEC, 0),
  XA_AACDEC_CONFIG_NONFATAL_DATA_RATE_NOT_SET       = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_config, XA_CODEC_AAC_DEC, 1),
  XA_AACDEC_CONFIG_NONFATAL_PARTIAL_CHANROUTING    = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_config, XA_CODEC_AAC_DEC, 2)
  ,XA_AACDEC_CONFIG_NONFATAL_INVALID_GEN_STRM_POS   = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_config, XA_CODEC_AAC_DEC, 3)
  ,XA_AACDEC_CONFIG_NONFATAL_CPID_NOT_PRESENT       = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_config, XA_CODEC_AAC_DEC, 4)
  ,XA_AACDEC_CONFIG_NONFATAL_INVALID_PRL_PARAMS     = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_config, XA_CODEC_AAC_DEC, 5)
  ,XA_AACDEC_CONFIG_NONFATAL_INVALID_DRC_PARAMS     = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_config, XA_CODEC_AAC_DEC, 6)
};
/* Fatal Errors */
enum xa_error_fatal_config_aac_dec {
  XA_AACDEC_CONFIG_FATAL_INVALID_BDOWNSAMPLE          = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 0),
  XA_AACDEC_CONFIG_FATAL_INVALID_BBITSTREAMDOWNMIX    = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 1),
  XA_AACDEC_CONFIG_FATAL_INVALID_EXTERNALSAMPLINGRATE = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 2),
  XA_AACDEC_CONFIG_FATAL_INVALID_EXTERNALBSFORMAT     = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 3),
  XA_AACDEC_CONFIG_FATAL_INVALID_TO_STEREO            = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 4),
  XA_AACDEC_CONFIG_FATAL_INVALID_OUTNCHANS            = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 5),
  XA_AACDEC_CONFIG_FATAL_INVALID_SBR_SIGNALING        = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 6),
  XA_AACDEC_CONFIG_FATAL_INVALID_CHANROUTING          = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 7),
  XA_AACDEC_CONFIG_FATAL_INVALID_PCM_WDSZ             = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 8),
  XA_AACDEC_CONFIG_FATAL_INVALID_ZERO_UNUSED_CHANS    = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 9),
  /* Code For Invalid Number of input channels */
  XA_AACDEC_CONFIG_FATAL_INVALID_EXTERNALCHCONFIG    = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 10), // Depricated, no more implemented
  XA_AACDEC_CONFIG_FATAL_INVALID_DECODELAYERS        = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 11), // Depricated, no more implemented
  XA_AACDEC_CONFIG_FATAL_INVALID_EXTERNALBITRATE     = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 12),
  XA_AACDEC_CONFIG_FATAL_INVALID_CONCEALMENT_PARAM   = XA_ERROR_CODE(xa_severity_fatal, xa_class_config, XA_CODEC_AAC_DEC, 13)
};

enum xa_error_nonfatal_execute_aac_dec {
  XA_AACDEC_EXECUTE_NONFATAL_INSUFFICIENT_FRAME_DATA    = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_execute, XA_CODEC_AAC_DEC, 0)
  ,XA_AACDEC_EXECUTE_NONFATAL_RUNTIME_INIT_RAMP_DOWN     = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_execute, XA_CODEC_AAC_DEC, 1)
  ,XA_AACDEC_EXECUTE_NONFATAL_RAW_FRAME_PARSE_ERROR     = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_execute, XA_CODEC_AAC_DEC, 2)
  ,XA_AACDEC_EXECUTE_NONFATAL_ADTS_HEADER_ERROR       = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_execute, XA_CODEC_AAC_DEC, 3) // Depreciated error, decoder don't return this error anymore
  ,XA_AACDEC_EXECUTE_NONFATAL_ADTS_HEADER_NOT_FOUND   = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_execute, XA_CODEC_AAC_DEC, 4) // Depreciated error, decoder don't return this error anymore
  ,XA_AACDEC_EXECUTE_NONFATAL_DABPLUS_HEADER_NOT_FOUND  = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_execute, XA_CODEC_AAC_DEC, 5)
  ,XA_AACDEC_EXECUTE_NONFATAL_LOAS_HEADER_ERROR       = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_execute, XA_CODEC_AAC_DEC, 6) // Depreciated error, decoder don't return this error anymore
  ,XA_AACDEC_EXECUTE_NONFATAL_STREAM_CHANGE             = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_execute, XA_CODEC_AAC_DEC, 7)
  ,XA_AACDEC_EXECUTE_NONFATAL_HEADER_NOT_FOUND          = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_execute, XA_CODEC_AAC_DEC, 8)
  ,XA_AACDEC_EXECUTE_NONFATAL_UNSUPPORTED_FEATURE       = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_execute, XA_CODEC_AAC_DEC, 9)
  ,XA_AACDEC_EXECUTE_NONFATAL_HEADER_ERROR              = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_execute, XA_CODEC_AAC_DEC, 10)
  ,XA_AACDEC_EXECUTE_NONFATAL_PARTIAL_LAST_FRAME        = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_execute, XA_CODEC_AAC_DEC, 11)
  ,XA_AACDEC_EXECUTE_NONFATAL_EMPTY_INPUT_BUFFER        = XA_ERROR_CODE(xa_severity_nonfatal, xa_class_execute, XA_CODEC_AAC_DEC, 12)
};
/* Fatal Errors */
enum xa_error_fatal_execute_aac_dec {
  XA_AACDEC_EXECUTE_FATAL_PARSING_ERROR                  = XA_ERROR_CODE(xa_severity_fatal, xa_class_execute, XA_CODEC_AAC_DEC, 0) // Depreciated error, decoder don't return this error anymore
  ,XA_AACDEC_EXECUTE_FATAL_RAW_FRAME_PARSE_ERROR          = XA_ERROR_CODE(xa_severity_fatal, xa_class_execute, XA_CODEC_AAC_DEC, 1)
  ,XA_AACDEC_EXECUTE_FATAL_BAD_INPUT_FAILURE              = XA_ERROR_CODE(xa_severity_fatal, xa_class_execute, XA_CODEC_AAC_DEC, 2) // Depreciated error, decoder don't return this error anymore
  ,XA_AACDEC_EXECUTE_FATAL_UNSUPPORTED_FEATURE             = XA_ERROR_CODE(xa_severity_fatal, xa_class_execute, XA_CODEC_AAC_DEC, 3)
  ,XA_AACDEC_EXECUTE_FATAL_ERROR_IN_CHANROUTING           = XA_ERROR_CODE(xa_severity_fatal, xa_class_execute, XA_CODEC_AAC_DEC, 4)
  ,XA_AACDEC_EXECUTE_FATAL_EMPTY_INPUT_BUFFER              = XA_ERROR_CODE(xa_severity_fatal, xa_class_execute, XA_CODEC_AAC_DEC, 5)
  ,XA_AACDEC_EXECUTE_FATAL_LOAS_HEADER_CHANGE            = XA_ERROR_CODE(xa_severity_fatal, xa_class_execute, XA_CODEC_AAC_DEC, 6) // Depreciated error, decoder don't return this error anymore
  ,XA_AACDEC_EXECUTE_FATAL_INIT_ERROR                    = XA_ERROR_CODE(xa_severity_fatal, xa_class_execute, XA_CODEC_AAC_DEC, 7) // Depreciated error, decoder don't return this error anymore
  ,XA_AACDEC_EXECUTE_FATAL_UNKNOWN_STREAM_FORMAT         = XA_ERROR_CODE(xa_severity_fatal, xa_class_execute, XA_CODEC_AAC_DEC, 8)
  ,XA_AACDEC_EXECUTE_FATAL_ADIF_HEADER_NOT_FOUND         = XA_ERROR_CODE(xa_severity_fatal, xa_class_execute, XA_CODEC_AAC_DEC, 9)

};



typedef enum {
  XA_AACDEC_PCE_NOT_FOUND_YET = 0,      /* No PCE found in the stream yet. */
  XA_AACDEC_PCE_NEW           = 1,      /* New PCE found in the current frame. */
  XA_AACDEC_PCE_USE_PREV      = 2       /* No PCE in current frame, using previous PCE. */
} xa_aac_dec_pce_status;


typedef struct
{
  unsigned char bMatrixMixdownIdxPresent;   /* Flag indicating if ucMatrixMixdownIndex & bPseudoSurroundEnable were present in PCE */
  unsigned char ucMatrixMixdownIndex;       /* 2-bit value selecting the coefficient set for matrix downmix.
                                        Note, ucMatrixMixdownIndex is valid only if bMatrixMixdownIdxPresent = 1 */
  unsigned char bPseudoSurroundEnable;      /* Flag indicating the possibility of mixdown for pseudo surround reproduction.
                                        Note, bPseudoSurroundEnable is valid only if bMatrixMixdownIdxPresent = 1 */
} xa_aac_dec_dwnmix_metadata_t;




typedef struct _inp_buffer_t
{
     signed char* pb_inp_buf;
    unsigned int ui_inp_size;
    signed int  i_bytes_consumed, i_bytes_read;
    signed int  i_buff_size;
    signed int  i_inp_over;
} inp_buffer_t;



typedef struct {
  unsigned char new_dvb_downmix_data;
  unsigned char mpeg_audio_type;
  unsigned char dolby_surround_mode;
  unsigned char center_mix_level_on;
  unsigned char center_mix_level_value;
  unsigned char surround_mix_level_on;
  unsigned char surround_mix_level_value;
  unsigned char coarse_grain_timecode_on;
  signed int  coarse_grain_timecode_value;
  unsigned char fine_grain_timecode_on;
  signed int  fine_grain_timecode_value;
} xa_aac_dec_dwnmix_level_dvb_info_t;

#define MAX_NUM_CHANNELS   8
#define MAX_NUM_DRC_BANDS    16

typedef struct {
  unsigned char drc_info_valid;
  unsigned char exclude_masks[MAX_NUM_CHANNELS];
  unsigned char drc_bands_present;
  unsigned char drc_interpolation_scheme;
  unsigned char drc_num_bands;
  unsigned char drc_band_incr;
  unsigned char drc_band_top[MAX_NUM_DRC_BANDS];
  unsigned char prog_ref_level_present;
  unsigned char prog_ref_level;
  char dyn_rng_dbx4[MAX_NUM_DRC_BANDS];
} xa_aac_dec_parsed_drc_info_t;


#if 0
#define AAC_API_CALL(cmd, idx, pvalue, errstr) \
    err_code = (*p_xa_process_api)(p_xa_process_api_obj, (cmd), (idx), (pvalue));\
_XA_HANDLE_ERROR(p_proc_err_info, (errstr), err_code);
#else
#define AAC_API_CALL(cmd, idx, pvalue, errstr) \
    err_code = (*p_xa_process_api)(p_xa_process_api_obj, (cmd), (idx), (pvalue));
#endif



#define GET_CONFIG(idx, pvalue, errstr) AAC_API_CALL(XA_API_CMD_GET_CONFIG_PARAM,idx, pvalue, errstr)



void AAC_DECODER_INIT1(inp_buffer_t *inp_buffer,signed char  *AAC_STUCT ,signed char  *AAC_MEMTABS,\
signed char  *AAC_INPUT_BUF,signed char  *AAC_OUTPUT_BUF,signed char  *AAC_PERRAM,signed char  *AAC_SCRAM);


int AAC_DECODER_INIT2(inp_buffer_t *inp_buffer,int a,int i_value ,signed int  ui_value) ;
extern unsigned int FILL_AAC_DATA( inp_buffer_t *inp_buffer,int len_read);
int get_params(void *p_hdl, int * p_numChannels,  int *p_channelMode,int *p_sampleRate,int *p_sbrType,  int *p_bsfmt,int *p_data_rate, int *p_pcm_sample_size);

#if defined(USE_DLL) && defined(_WIN32)
#define DLL_SHARED __declspec(dllimport)
#elif defined (_WINDLL)
#define DLL_SHARED __declspec(dllexport)
#else
#define DLL_SHARED
#endif

#if defined(__cplusplus)
extern "C" {
#endif  /* __cplusplus */
DLL_SHARED xa_codec_func_t xa_aac_dec;
DLL_SHARED xa_codec_func_t xa_dabplus_dec;
#if defined(__cplusplus)
}
#endif  /* __cplusplus */







#ifndef __XA_ERROR_HANDLER_H__
#define __XA_ERROR_HANDLER_H__

#define XA_ERROR_NON_FATAL_IDX		0x0
#define XA_ERROR_FATAL_IDX			0x1

#define XA_ERROR_CLASS_0		0x0
#define XA_ERROR_CLASS_1		0x1
#define XA_ERROR_CLASS_2		0x2
#define XA_ERROR_CLASS_3		0x3
#define XA_ERROR_CLASS_4		0x4
#define XA_ERROR_CLASS_5		0x5
#define XA_ERROR_CLASS_6		0x6
#define XA_ERROR_CLASS_7		0x7
#define XA_ERROR_CLASS_8		0x8
#define XA_ERROR_CLASS_9		0x9
#define XA_ERROR_CLASS_A		0xA
#define XA_ERROR_CLASS_B		0xB
#define XA_ERROR_CLASS_C		0xC
#define XA_ERROR_CLASS_D		0xD
#define XA_ERROR_CLASS_E		0xE
#define XA_ERROR_CLASS_F		0xF



/*****************************************************************************/
/* Type definitions                                                          */
/*****************************************************************************/
typedef struct {
  const char  *pb_module_name;
  const char  *ppb_class_names[16];
  const char **ppppb_error_msg_pointers[2][16];
} xa_error_info_struct;


XA_ERRORCODE xa_error_handler(xa_error_info_struct *p_mod_err_info,
			      const char *pb_context,
			      XA_ERRORCODE code);


#define _XA_HANDLE_ERROR(p_mod_err_info, context, e) \
	if((e) != XA_NO_ERROR) \
	{ \
		xa_error_handler((p_mod_err_info), (context), (e)); \
		if((e) & XA_FATAL_ERROR) \
			return (e); \
	}

#endif /* __XA_ERROR_HANDLER_H__ */

#ifndef __XA_MEMORY_STANDARDS_H__
#define __XA_MEMORY_STANDARDS_H__


/* when you don't need alignment, pass this to memory library */
#define XA_MEM_NO_ALIGN				0x01

/* standard memory types */
/* to be used inter frames */
#define XA_MEMTYPE_PERSIST				0x00
/* read write, to be used intra frames */
#define XA_MEMTYPE_SCRATCH				0x01
/* read only memory, intra frame */
#define XA_MEMTYPE_INPUT				0x02
/* read-write memory, for usable output, intra frame */
#define XA_MEMTYPE_OUTPUT				0x03
/* readonly memory, inter frame */
#define XA_MEMTYPE_TABLE				0x04
/* input buffer before mem tabs allocation */
#define XA_MEMTYPE_PRE_FRAME_INPUT		0x05
/* input buffer before mem tabs allocation */
#define XA_MEMTYPE_PRE_FRAME_SCRATCH	0x06
/* for local variables */
#define XA_MEMTYPE_AUTO_VAR				0x80

/* standard memory priorities */
#define XA_MEMPRIORITY_ANYWHERE			0x00
#define XA_MEMPRIORITY_LOWEST			0x01
#define XA_MEMPRIORITY_LOW				0x02
#define XA_MEMPRIORITY_NORM				0x03
#define XA_MEMPRIORITY_ABOVE_NORM		0x04
#define XA_MEMPRIORITY_HIGH				0x05
#define XA_MEMPRIORITY_HIGHER			0x06
#define XA_MEMPRIORITY_CRITICAL			0x07

/* standard memory placements */
/* placement is defined by 64 bits */

#define XA_MEMPLACE_FAST_RAM_0			0x000001
#define XA_MEMPLACE_FAST_RAM_1			0x000002
#define XA_MEMPLACE_FAST_RAM_2			0x000004
#define XA_MEMPLACE_FAST_RAM_3			0x000008
#define XA_MEMPLACE_FAST_RAM_4			0x000010
#define XA_MEMPLACE_FAST_RAM_5			0x000020
#define XA_MEMPLACE_FAST_RAM_6			0x000040
#define XA_MEMPLACE_FAST_RAM_7			0x000080

#define XA_MEMPLACE_INT_RAM_0			0x000100
#define XA_MEMPLACE_INT_RAM_1			0x000200
#define XA_MEMPLACE_INT_RAM_2			0x000400
#define XA_MEMPLACE_INT_RAM_3			0x000800
#define XA_MEMPLACE_INT_RAM_4			0x001000
#define XA_MEMPLACE_INT_RAM_5			0x002000
#define XA_MEMPLACE_INT_RAM_6			0x004000
#define XA_MEMPLACE_INT_RAM_7			0x008000

#define XA_MEMPLACE_EXT_RAM_0			0x010000
#define XA_MEMPLACE_EXT_RAM_1			0x020000
#define XA_MEMPLACE_EXT_RAM_2			0x040000
#define XA_MEMPLACE_EXT_RAM_3			0x080000
#define XA_MEMPLACE_EXT_RAM_4			0x100000
#define XA_MEMPLACE_EXT_RAM_5			0x200000
#define XA_MEMPLACE_EXT_RAM_6			0x400000
#define XA_MEMPLACE_EXT_RAM_7			0x800000

#define XA_MEMPLACE_DONTCARE_H			0xFFFFFFFF
#define XA_MEMPLACE_DONTCARE_L			0xFFFFFFFF

#define XA_PC_RAM_H					0x00000000
#define XA_PC_RAM_L					XA_MEMPLACE_EXT_RAM_0
#endif

void xa_aac_dec_error_handler_init();
void xa_testbench_error_handler_init();

extern xa_codec_func_t *p_xa_process_api;
extern xa_error_info_struct *p_proc_err_info ;
extern xa_codec_handle_t p_xa_process_api_obj ;

#ifndef XT_ALIGN8
#define XT_ALIGN8 __attribute__((aligned(16)))
#endif


typedef struct AAC_HANDLE
{
	XT_ALIGN8 signed char AAC_STUCT[176];
	XT_ALIGN8 signed char AAC_MEMTABS[144];
	XT_ALIGN8 signed char AAC_INPUT_BUF[8480];
	XT_ALIGN8 signed char AAC_OUTPUT_BUF[8192];
	XT_ALIGN8 signed char AAC_PERRAM[6392];
	XT_ALIGN8 signed char AAC_SCRAM[12392];
	inp_buffer_t inp_buffer ;

	int acmod;
    int i_num_chan;
    int i_samp_freq;
    int i_data_rate;
    int err_code;
    int bsfmt;
    int sbrType;
    int pcm_sample_size;
    int i_out_bytes;
    int LengthRead;
    int InitDone;
    int ExecDone;
    int FrameworkReset;
}AAC_HAD, *AAC_HAD_PTR;




#endif /* __XA_AAC_DEC_API_H__ */


