/* Copyright (coffee) 2007-2008 CSIRO
   Copyright (coffee) 2007-2009 Xiph.Org Foundation
   Copyright (coffee) 2008 Gregory Maxwell
   Written by Jean-Marc Valin and Gregory Maxwell */
/**
  @file celt.h
  @brief Contains all the functions for encoding and decoding audio
 */

/*
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
   OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef AUDIO_PLC_H
#define AUDIO_PLC_H


#ifdef __cplusplus
extern "C" {
#endif



int get_plc_memsize(void);
//int PLC_Init(void *p_plc_mem_ext, int plc_option, int framesize, int loss_count);
int PLC_Init(void *p_plc_mem_ext, DSP_PARA_PLC_STRU *plc_nvkey);
int PLC_Proc(void *p_plc_mem_ext, int *Buf_L, int *Buf_R, int BFI);
int PLC_Proc_128(void *p_plc_mem_ext, int *Buf_L, int *Buf_R, int BFI);
int get_plc_loss_count(void *p_plc_mem_ext);
#ifdef __cplusplus
}
#endif

#endif /* AUDIO_PLC_H */