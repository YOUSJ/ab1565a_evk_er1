/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
#include "dsp_feature_interface.h"
#include "dsp_utilities.h"
#include "dsp_buffer.h"
#include "audio_plc.h"
#include "config.h"
#include "audio_plc_interface.h"
//#include "DSP_Dump.h"
#include "dsp_dump.h"

#include "dsp_callback.h"

#include "common.h"
#include "dsp_para_plc_nvstruc.h"

#define QCONST16(x,bits) ((short)(.5+(x)*(((int)1)<<(bits))))
#define FRAME_LEN 512


#define SBC_FRAME_LEN 128 //128sample
#define AAC_FRAME_LEN 1024

#define DUMP_AUDIO_PLC 0
#define AUDIO_PLC_MIPS_E 0


DSP_PARA_AUDIO_PLC_STRU plc_nvkey = {
           0x1,                            // U8  ENABLE
           0x1,                            // U8  REVISION
           0x0003,                         // U16 option: b[1] = 1(2-channel     ), 0(1-channel      )
                                           //                     b[0] = 1(Enable PRE_EMP), 0(Disable PRE_EMP)
           FRAME_LEN,                      // S16 framesize
           3,                              // S16 loss_count, default = 5//2
           965,                            // S16 pitch_lag_max //965//720
           500,                            // S16 pitch_lag_min //500//100
           QCONST16(0.8f, 15),            // S16 fade
           0x0000,                         // U16 reserve_1
           0x0000,                         // U16 reserve_2
           0x0000,                         // U16 reserve_3
           0x0000,                         // U16 reserve_4
           0x0000,                         // U16 reserve_5
           0x0000,                         // U16 reserve_6
           0x0000                          // U16 reserve_7
};

static DSP_AUDIO_PLC_CTRL_t g_audio_plc_ctrl;


U8 DSP_GetFuncStreamCodecType(VOID* para)
{
    DSP_STREAMING_PARA_PTR stream_ptr;
    DSP_FEATURE_TABLE_PTR  FeatureTablePtr;

    stream_ptr = DSP_STREAMING_GET_FROM_PRAR(para);
    FeatureTablePtr = (DSP_FEATURE_TABLE_PTR)(stream_ptr->callback.FeatureTablePtr);
    return FeatureTablePtr->FeatureType;
}

ATTR_TEXT_IN_IRAM_LEVEL_2 U16 DSP_GetStreamCodecOutSize(VOID* para)
{
    return ((DSP_ENTRY_PARA_PTR)para)->codec_out_size;
}


bool Audio_PLC_Init (VOID* para);

bool Audio_PLC_Process (VOID* para);

bool Audio_PLC_Init (VOID* para){

    DSP_STREAMING_PARA_PTR stream_ptr;
    AUDIO_PLC_SCRATCH_PTR_t audio_plc_feature_ptr;
    U32 ch_num = stream_function_get_channel_number(para);//DSP_GetFuncStreamChannelNum(para);
    stream_ptr = DSP_STREAMING_GET_FROM_PRAR(para);
    audio_plc_feature_ptr = stream_function_get_working_buffer(para);//DSP_GetFuncMemoryInstancePtr(para);
    audio_plc_feature_ptr->init_done = 0;

    DSP_MW_LOG_I("Audio_PLC_Init\r\n",0);

    return FALSE;
}

bool Audio_PLC_Process (VOID* para){
    DSP_STREAMING_PARA_PTR stream_ptr;
    AUDIO_PLC_SCRATCH_PTR_t audio_plc_feature_ptr;
    U32 ch_num = stream_function_get_channel_number(para);//DSP_GetFuncStreamChannelNum(para);
    void *Buf_L;
    void *Buf_R;
    U16 streamsize;
    U16 streamlength;
    U32 plcProcOutLen;
#ifdef AUDIO_PLC_MIPS_E
    static uint32_t timer0 = 0, timer1 = 0;
    static uint32_t counter = 0, mips_total = 0;
    static uint32_t preplcProcOutLen = 0;
#endif
    stream_ptr = DSP_STREAMING_GET_FROM_PRAR(para);
    audio_plc_feature_ptr = stream_function_get_working_buffer(para);//DSP_GetFuncMemoryInstancePtr(para);
    Buf_L = stream_function_get_1st_inout_buffer(para);//DSP_GetFuncStream1Ptr(para);
    Buf_R = stream_function_get_2nd_inout_buffer(para);//DSP_GetFuncStream2Ptr(para);
    streamlength = DSP_GetStreamCodecOutSize(para);
    streamsize = streamlength/sizeof(U32);
    if(audio_plc_feature_ptr->init_done == 0 && g_audio_plc_ctrl.enable == 1){
        if(MAX_PLC_MEM_SIZE >= get_plc_memsize()&& ch_num == 2){
                if(DSP_GetFuncStreamCodecType(para) == CODEC_DECODER_SBC)
                {
                    audio_plc_feature_ptr->codec_frame_size = SBC_FRAME_LEN;
                }
                else if(DSP_GetFuncStreamCodecType(para) == CODEC_DECODER_AAC)
                {
                     audio_plc_feature_ptr->codec_frame_size = AAC_FRAME_LEN;
                     plc_nvkey.framesize = AAC_FRAME_LEN;
                }else{
                    platform_assert("Audio PLC not support codec type.",__FILE__,__LINE__);
                }
                //PLC_Init(audio_plc_feature_ptr->p_plc_mem_ext, audio_plc_feature_ptr->plc_option, audio_plc_feature_ptr->codec_frame_size, audio_plc_feature_ptr->loss_count);
                PLC_Init(audio_plc_feature_ptr->p_plc_mem_ext, &plc_nvkey);
                audio_plc_feature_ptr->init_done = 1;
                DSP_MW_LOG_I("Audio PLC init get memsize %d framesize %d\r\n", 2,get_plc_memsize(),audio_plc_feature_ptr->codec_frame_size); 
            }else{
                DSP_MW_LOG_E("Audio PLC error:memory not enough %d < %d\r\n", 2, MAX_PLC_MEM_SIZE,get_plc_memsize());
            }
    }

    if(streamsize == audio_plc_feature_ptr->codec_frame_size && audio_plc_feature_ptr->init_done == 1)
    {

        audio_plc_feature_ptr->pre_bfi= audio_plc_feature_ptr->bfi;
        if(stream_codec_get_mute_flag(para) == TRUE && g_audio_plc_ctrl.enable == 1){//DSP_GetCodecMuteFlag(para) == TRUE
            audio_plc_feature_ptr->bfi = 1;//bed frame
            //DSP_MW_LOG_I("Audio PLC bad frame\r\n", 0);
        }
        else{
            audio_plc_feature_ptr->bfi = 0;//good frame
            //DSP_MW_LOG_E("Audio PLC good frame\r\n", 0);
        }
#if (0)
        if(preplcProcOutLen!=0 && audio_plc_feature_ptr->bfi == 1){
            taskENTER_CRITICAL();
            hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M,&timer0);
            //DSP_MW_LOG_I("preplcProcOutLen %d t0 %d", 2,preplcProcOutLen,timer0);
        }

#endif
        if(audio_plc_feature_ptr->pre_bfi==1 && audio_plc_feature_ptr->bfi == 0){
            audio_plc_feature_ptr->bad_to_good_flag = 1;//for decodec muted to good frame.
        }
        if(audio_plc_feature_ptr->p_plc_mem_ext !=NULL){
#if (0)
            {
                U16  tempbuffer_L[128];
                U32 *src_buf_L = (U32 *)Buf_L;
                for (int i=0; i<128; i++)
                {
                    tempbuffer_L[i] = (U16)(src_buf_L[i] >> 16);
                }
                LOG_AUDIO_DUMP((U8*)tempbuffer_L, 128*2, AUDIO_SOURCE_IN_L);
            }
#endif
            //DSP_MW_LOG_E("Audio PLC bfi %d streamsize %d\r\n", 2,audio_plc_feature_ptr->bfi,streamsize*sizeof(U32));


            //PLC_Proc(audio_plc_feature_ptr->p_plc_mem_ext, Buf_L, Buf_R, audio_plc_feature_ptr->bfi);
            if(audio_plc_feature_ptr->bad_to_good_flag ==1){//for decodec muted to good frame.
#if (DUMP_AUDIO_PLC)
            {
                U8  tempbuffer_L[AAC_FRAME_LEN];
                U16  tempbuffer[AAC_FRAME_LEN];
                U32 *src_buf_L = (U32 *)Buf_L;
                if(audio_plc_feature_ptr->bad_to_good_flag == 1){

                    for (int i=0; i<plc_nvkey.framesize; i++)
                    {
                        tempbuffer_L[i] = 127;
                        tempbuffer[i] = (U16)(src_buf_L[i] >> 16);
                    }

                }else{

                    for (int i=0; i<plc_nvkey.framesize; i++)
                    {
                        tempbuffer_L[i] = 0;
                    }

                }

                LOG_AUDIO_DUMP((U8*)tempbuffer_L, 128, AUDIO_SOURCE_IN_L);
                //LOG_AUDIO_DUMP((U8*)tempbuffer, 128*2, AUDIO_INS_IN_L);
            }
#endif
#if (AUDIO_PLC_MIPS_E)
                    taskENTER_CRITICAL();
                    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M,&timer0);
                    DSP_MW_LOG_I("t0 %d bfi %d", 2,timer0,audio_plc_feature_ptr->bad_to_good_flag);

#endif

                if(DSP_GetFuncStreamCodecType(para) == CODEC_DECODER_SBC)
                {
                    plcProcOutLen = PLC_Proc_128(audio_plc_feature_ptr->p_plc_mem_ext, Buf_L, Buf_R, audio_plc_feature_ptr->bad_to_good_flag);
                }
                else if(DSP_GetFuncStreamCodecType(para) == CODEC_DECODER_AAC)
                {
                    plcProcOutLen = PLC_Proc(audio_plc_feature_ptr->p_plc_mem_ext, Buf_L, Buf_R, audio_plc_feature_ptr->bad_to_good_flag);

                }else{
                    platform_assert("Audio PLC not support codec type.",__FILE__,__LINE__);
                }
                audio_plc_feature_ptr->bad_to_good_flag = 0;
#if (AUDIO_PLC_MIPS_E)
                hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &timer1);
                taskEXIT_CRITICAL();
                DSP_MW_LOG_I("t1 %d out %d", 2,timer1,plcProcOutLen);

#endif

            }else{

#if (DUMP_AUDIO_PLC)
            {
                U8  tempbuffer_L[AAC_FRAME_LEN];
                U16  tempbuffer[AAC_FRAME_LEN];
                U32 *src_buf_L = (U32 *)Buf_L;
                if(audio_plc_feature_ptr->bfi == 1){

                    for (int i=0; i<plc_nvkey.framesize; i++)
                    {
                        tempbuffer_L[i] = 127;
                        tempbuffer[i] = (U16)(src_buf_L[i] >> 16);
                    }

                }else{

                    for (int i=0; i<plc_nvkey.framesize; i++)
                    {
                        tempbuffer_L[i] = 0;
                        tempbuffer[i] = (U16)(src_buf_L[i] >> 16);
                    }

                }

                LOG_AUDIO_DUMP((U8*)tempbuffer_L, 128, AUDIO_SOURCE_IN_L);
                //LOG_AUDIO_DUMP((U8*)tempbuffer, 128*2, AUDIO_INS_IN_L);
            }
#endif
#if (AUDIO_PLC_MIPS_E)
                taskENTER_CRITICAL();
                hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M,&timer0);
                DSP_MW_LOG_I("t0 %d bfi %d", 2,timer0,audio_plc_feature_ptr->bfi);

#endif

                if(DSP_GetFuncStreamCodecType(para) == CODEC_DECODER_SBC)
                {
                    plcProcOutLen = PLC_Proc_128(audio_plc_feature_ptr->p_plc_mem_ext, Buf_L, Buf_R, audio_plc_feature_ptr->bfi);
                }
                else if(DSP_GetFuncStreamCodecType(para) == CODEC_DECODER_AAC)
                {
                    plcProcOutLen = PLC_Proc(audio_plc_feature_ptr->p_plc_mem_ext, Buf_L, Buf_R, audio_plc_feature_ptr->bfi);

                }else{
                    platform_assert("Audio PLC not support codec type.",__FILE__,__LINE__);
                }
#if (AUDIO_PLC_MIPS_E)
                hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &timer1);
                taskEXIT_CRITICAL();
                DSP_MW_LOG_I("t1 %d out %d", 2,timer1,plcProcOutLen);

#endif


            }
#if (0)
            hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &timer1);
            //DSP_MW_LOG_I("plcProcOutLen %d t1 %d", 2,plcProcOutLen,timer1);
            preplcProcOutLen = plcProcOutLen;
            taskEXIT_CRITICAL();
            if((timer1 > timer0) && ( plcProcOutLen != 0))
            {
                U32 cycles = (timer1-timer0)*312;
                U32 MIPS = (cycles*44100)/plcProcOutLen;
                mips_total += MIPS/1000; //mini
                counter++;
                DSP_MW_LOG_I("[counter1]%d [cycle]%d [samples]%d [MIPS] %d [AVG] %d\n", 5,counter, cycles, plcProcOutLen, MIPS, mips_total/counter);
                //the AVG value needs to be divided by 1000 to get the correct value.
            }
#endif
            //DSP_MW_LOG_I("plcProcOutLen %d", 1,plcProcOutLen);
            stream_function_modify_output_size(para,(plcProcOutLen*sizeof(U32)));//DSP_ModifyFuncStreamSize(para,(plcProcOutLen*sizeof(U32)));
            //Audio_PLC_Modify_OutputSize((plcProcOutLen*sizeof(U32)));
#if (0)
            {
                U16  tempbuffer_L[512];
                U32 *src_buf_L = (U32 *)Buf_L;
                for (int i=0; i<plcProcOutLen; i++)
                {
                    tempbuffer_L[i] = (U16)(src_buf_L[i] >> 16);
                }
                LOG_AUDIO_DUMP((U8*)tempbuffer_L, plcProcOutLen*2, AUDIO_INS_OUT_L);
            }
#endif
#if (DUMP_AUDIO_PLC)
            {
                U8  tempbuffer_L[AAC_FRAME_LEN];
                if(get_plc_loss_count(audio_plc_feature_ptr->p_plc_mem_ext)>= 3){

                    for (int i=0; i<plcProcOutLen; i++)
                    {
                        tempbuffer_L[i] = 127;
                    }

                }else if(get_plc_loss_count(audio_plc_feature_ptr->p_plc_mem_ext)== 2){

                    for (int i=0; i<plcProcOutLen; i++)
                    {
                        tempbuffer_L[i] = 100;
                    }

                }else if(get_plc_loss_count(audio_plc_feature_ptr->p_plc_mem_ext)== 1){

                    for (int i=0; i<plcProcOutLen; i++)
                    {
                        tempbuffer_L[i] = 50;
                    }

                }else{
                    for (int i=0; i<plcProcOutLen; i++)
                    {
                        tempbuffer_L[i] = 0;
                    }
                }

                LOG_AUDIO_DUMP((U8*)tempbuffer_L, plcProcOutLen, AUDIO_INS_OUT_L);
                //LOG_AUDIO_DUMP((U8*)tempbuffer, 128*2, AUDIO_INS_IN_L);
            }

#endif

            //DSP_MW_LOG_E("Audio PLC bfi %d streamsize %d plcProcOutLen %d\r\n", 3,audio_plc_feature_ptr->bfi,streamsize,plcProcOutLen);

        }else{
            DSP_MW_LOG_I("Audio PLC get null p_plc_mem_ext\r\n", 0);
        }


    }else{

        //do nothing
        DSP_MW_LOG_I("Audio PLC get streamsize %d\r\n",1, streamsize);
    }


    return FALSE;
}

void Audio_PLC_ctrl (dsp_audio_plc_ctrl_t audio_plc_ctrl){

    g_audio_plc_ctrl.enable = audio_plc_ctrl.enable;
    DSP_MW_LOG_E("Audio_PLC_ctrl enable %d\r\n", 1,audio_plc_ctrl.enable);
}

U16 Audio_PLC_Get_OutputSize (void){
    U16 outputsize;
    outputsize = g_audio_plc_ctrl.outputsize;
    return outputsize;
}

void Audio_PLC_Modify_OutputSize (U16 outputsize){

    g_audio_plc_ctrl.outputsize = outputsize;

}




