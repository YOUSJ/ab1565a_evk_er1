/* Copyright Statement:
 *
 * (C) 2020  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

/* Includes ------------------------------------------------------------------*/
//#include "bma456.h"
#include "bsp_multi_axis_sensor.h"
#include "bsp_multi_axis_sensor_config.h"

/* hal includes */
#include "hal.h"
#include "syslog.h"
#include "hal_i2c_master.h"
#include "memory_attribute.h"
#include "dsp_dump.h"
#include "dtm.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static volatile bool g_bsp_i2c_inited_flg = false;
static volatile bool g_bsp_sensor_is_busy = false;
static volatile bool g_bsp_sensor_inited_flg = false;
static volatile bool g_i2c_send_to_recv_done_flg = false;
static ATTR_RWDATA_IN_DRAM_4BYTE_ALIGN unsigned char g_i2c_send_data[I2C_SEND_TO_RECEIVE_SEND_LEN] = {0x00};
static ATTR_RWDATA_IN_DRAM_4BYTE_ALIGN unsigned char g_i2c_fifo[I2C_BUFFER_MAX_LEN] = {0x00};
static ATTR_RWDATA_IN_DRAM_4BYTE_ALIGN uint8_t bma456_buf[1024] = {0};
static struct bma4_accel sens_data[64]; //64 frames data
struct bma4_fifo_frame fifo_frame = {
    .data = bma456_buf,
    .length = 448,
    .fifo_data_enable = BMA4_ENABLE,
    .fifo_header_enable = BMA4_ENABLE,
};

static struct bma4_dev accel = {
    .interface = BMA4_I2C_INTERFACE,
    .dev_addr = BMA4_I2C_ADDR_PRIMARY,
    .bus_read = bsp_i2c_read_data,
    .bus_write = bsp_i2c_write_data,
    .delay = bsp_delay_ms,
    //.delay_ms = bsp_delay_ms,
    .read_write_len = 8,
}; /* Declare an instance of the BMA4xy device */

struct bma4_int_pin_config int_pin_config = {
    .edge_ctrl = BMA4_LEVEL_TRIGGER,
    .lvl =       BMA4_ACTIVE_HIGH,
    .od =        BMA4_PUSH_PULL,
    .output_en = BMA4_OUTPUT_ENABLE,
    .input_en =  BMA4_INPUT_DISABLE
};

static volatile sensor_ringbuf_handler_t sensor_buf_handler;
/*sensor data sw buffer allocate*/
static volatile uint8_t g_sensor_buffer[AXIS_SENSOR_BUFFER_SIZE];

/* Private functions ---------------------------------------------------------*/
static void i2c_dma_callback(unsigned char slave_addr, hal_i2c_callback_event_t event, void *user_data)
{
    //walk around build warning,no use
    slave_addr = slave_addr;
    user_data = user_data;

    if (event != HAL_I2C_EVENT_SUCCESS) {
        log_hal_msgid_error("[G-sensor]i2c_dma_callback err event = %d", 1, event);
        while (1);
    }
    g_i2c_send_to_recv_done_flg = true;
}

/*BSP porting layer*/
uint16_t bsp_i2c_read_data(uint8_t dev_addr, uint8_t reg_addr, uint8_t *read_data, uint16_t len)
{
    hal_i2c_config_t i2c;
    hal_i2c_status_t sta;
    hal_i2c_send_to_receive_config_ex_t i2c_send_to_receive_config_ex;

    if (false == g_bsp_i2c_inited_flg) {
        /*init i2c hw*/
        /*gpio config*/
        hal_pinmux_set_function(AXIS_SENSOR_IIC_SCL_PIN, 6);
        hal_pinmux_set_function(AXIS_SENSOR_IIC_SDA_PIN, 6);

        /*i2c hw init*/
        i2c.frequency = AXIS_SENSOR_IIC_FREQENCE;
        if (hal_i2c_master_init(AXIS_SENSOR_IIC_PORT, &i2c)) {
            log_hal_msgid_error("[G-sensor]g_sensor i2c hw init fail", 0);
            while (1);
        }

        /*register callback*/
        hal_i2c_master_register_callback(AXIS_SENSOR_IIC_PORT, i2c_dma_callback, NULL);
        hal_i2c_master_set_io_config(AXIS_SENSOR_IIC_PORT, HAL_I2C_IO_OPEN_DRAIN);

        g_bsp_i2c_inited_flg = true;
    }
    /*read data from g-sensor*/
    g_i2c_send_data[0] = reg_addr;
    i2c_send_to_receive_config_ex.receive_buffer = read_data;
    i2c_send_to_receive_config_ex.receive_bytes_in_one_packet = len;
    i2c_send_to_receive_config_ex.receive_packet_length = 1;
    i2c_send_to_receive_config_ex.send_packet_length = 1;
    i2c_send_to_receive_config_ex.send_bytes_in_one_packet = 1;
    i2c_send_to_receive_config_ex.send_data = g_i2c_send_data;
    i2c_send_to_receive_config_ex.slave_address = dev_addr;

    sta = hal_i2c_master_send_to_receive_dma_ex(AXIS_SENSOR_IIC_PORT, &i2c_send_to_receive_config_ex);
    if (HAL_I2C_STATUS_OK != sta ) {
        log_hal_msgid_error("[G-sensor]g_sensor i2c driver read data fail,err sta = %d,dev_addr = 0x%08x,reg_addr = 0x%08x", 3, sta, dev_addr, reg_addr);
        while (1);
    }
    //wait transfer done
    while (false == g_i2c_send_to_recv_done_flg);
    g_i2c_send_to_recv_done_flg = false;

    return BMA4_OK;
}

uint16_t bsp_i2c_write_data(uint8_t dev_addr, uint8_t reg_addr, uint8_t *write_data, uint16_t len)
{
    hal_i2c_config_t i2c;
    hal_i2c_status_t sta;
    hal_i2c_send_config_t send_cfg;
    unsigned char i;

    if (false == g_bsp_i2c_inited_flg) {
        /*init i2c hw*/
        /*gpio config*/
        hal_pinmux_set_function(AXIS_SENSOR_IIC_SCL_PIN, 6);
        hal_pinmux_set_function(AXIS_SENSOR_IIC_SDA_PIN, 6);

        /*i2c hw init*/
        i2c.frequency = AXIS_SENSOR_IIC_FREQENCE;
        if (hal_i2c_master_init(AXIS_SENSOR_IIC_PORT, &i2c)) {
            log_hal_msgid_error("[G-sensor]g_sensor i2c hw init fail", 0);
            while (1);
        }
        /*register callback*/
        hal_i2c_master_register_callback(HAL_I2C_MASTER_0, i2c_dma_callback, NULL);
        hal_i2c_master_set_io_config(AXIS_SENSOR_IIC_PORT, HAL_I2C_IO_OPEN_DRAIN);

        g_bsp_i2c_inited_flg = true;
    }

    /*write data from g-sensor*/
    g_i2c_fifo[0] = reg_addr;
    for (i = 0; i < len; i++) {
        g_i2c_fifo[i + 1] = *(write_data + i);
    }

    send_cfg.send_bytes_in_one_packet = len + 1;
    send_cfg.slave_address = dev_addr;
    send_cfg.send_packet_length = 1;
    send_cfg.send_data = g_i2c_fifo;

    sta = hal_i2c_master_send_dma_ex(AXIS_SENSOR_IIC_PORT, &send_cfg);
    if (HAL_I2C_STATUS_OK != sta) {
        log_hal_msgid_error("[G-sensor]g_sensor i2c driver write data fail,err sta = %d,dev_addr = 0x%08x,reg_addr = 0x%08x", 3, sta, dev_addr, reg_addr);
        while (1);
    }

    //wait transfer done
    while (false == g_i2c_send_to_recv_done_flg);
    g_i2c_send_to_recv_done_flg = false;

    return BMA4_OK;
}

void bsp_delay_ms(uint32_t period)
{
    hal_gpt_delay_ms(period);
}

void bsp_delay_us(uint32_t period)
{
    hal_gpt_delay_us(period);
}


/*reset & clear pre-roll buffer and state machine*/
static bool ringbuf_reset(void)
{
    memset((void *)&sensor_buf_handler, 0, sizeof(sensor_ringbuf_handler_t));

    /*init handler ring buffer size*/
    sensor_buf_handler.buffer_size = sizeof(g_sensor_buffer);

    /*init handler ring buffer address*/
    sensor_buf_handler.p_buffer = (uint8_t *)g_sensor_buffer;

    /*init read pointer*/
    sensor_buf_handler.p_read = 0;

    /*init write pointer*/
    sensor_buf_handler.p_write = 0;

    /*init data length*/
    sensor_buf_handler.data_length = 0;

    /*clear data buffer*/
    memset(sensor_buf_handler.p_buffer, 0, sensor_buf_handler.data_length);

    return true;
}

static bool ringbuf_read_data(uint8_t *read_buf, uint32_t read_length)
{
    uint32_t length = 0;
    uint32_t p_read = sensor_buf_handler.p_read;
    uint32_t buffer_size = sensor_buf_handler.buffer_size;
    uint8_t *p_buffer = sensor_buf_handler.p_buffer;

    if (read_length > sensor_buf_handler.data_length) {
        log_hal_msgid_error("[G-sensor]ringbuf_read_data: buffer have not data", 0);
        return false;
    }

    /*read data length not lead to buffer roll-back*/
    if ((p_read + read_length) <= buffer_size) {
        memcpy(read_buf, p_buffer + p_read, read_length);
    } else {
        /*read data length lead to buffer roll-back*/
        /*calculate the buffer remain length*/
        length = buffer_size - p_read;

        /*copy remain buffer to read buffer*/
        memcpy(read_buf, p_buffer + p_read, length);

        /*copy buffer roll-back address data to read buffer*/
        memcpy(read_buf + length, p_buffer, read_length - length);
    }

    /*update current ring buffer read pointer*/
    sensor_buf_handler.p_read = (sensor_buf_handler.p_read + read_length) % buffer_size;

    /*update current valid data length*/
    sensor_buf_handler.data_length -= read_length;

    return true;
}

static bool ringbuf_write_data(uint8_t *write_buf, uint32_t write_length)
{
    uint32_t length = 0;
    uint32_t p_write = sensor_buf_handler.p_write;
    uint32_t buffer_size = sensor_buf_handler.buffer_size;
    uint8_t *p_buffer = sensor_buf_handler.p_buffer;

    if (sensor_buf_handler.data_length + write_length > sensor_buf_handler.buffer_size) {
        log_hal_msgid_error("[G-sensor]ringbuf_write_data: buffer have not free space", 0);
        return false;
    }

    /*write data length not lead to buffer roll-back*/
    if ((p_write + write_length) <= buffer_size) {
        memcpy(p_buffer + p_write, write_buf, write_length);
    } else {
        /*write data length lead to buffer roll-back*/
        /*calculate the buffer remain length*/
        length = buffer_size - p_write;

        /*copy write data to remain buffer*/
        memcpy(p_buffer + p_write, write_buf, length);

        /*copy remain write data to buffer roll-back address*/
        memcpy(p_buffer, write_buf + length, write_length - length);
    }

    /*update current ring buffer write pointer*/
    sensor_buf_handler.p_write = (sensor_buf_handler.p_write + write_length) % buffer_size;
    /*update current valid data length*/
    sensor_buf_handler.data_length += write_length;

    return true;
}

static uint32_t ringbuf_get_data_length(void)
{
    return sensor_buf_handler.data_length;
}

static void init1_callback(void *user_data)
{
    user_data = user_data;

#ifdef AXIS_SENSOR_DEBUG_ENABLE
    log_hal_msgid_info("init1_callback trigger\r\n", 0);
#endif
    hal_eint_mask(AXIS_SENSOR_EINT_NUM);

    DTM_enqueue(DTM_EVENT_ID_GSENSOR_WATERMARK_TRIGGER, 0, true);
}

static void int1_gpio_init(void)
{
    hal_eint_config_t hwvad_eint_config = {HAL_EINT_LEVEL_HIGH, 1};

    hal_gpio_init(AXIS_SENSOR_EINT_PIN);
    hal_pinmux_set_function(AXIS_SENSOR_EINT_PIN, AXIS_SENSOR_EINT_PINMUX);
    hal_gpio_set_direction(AXIS_SENSOR_EINT_PIN, HAL_GPIO_DIRECTION_INPUT);
    hal_gpio_disable_pull(AXIS_SENSOR_EINT_PIN);

    /*init GPIO eint*/
    if (HAL_EINT_STATUS_OK != hal_eint_mask(AXIS_SENSOR_EINT_NUM)) {
        log_hal_msgid_error("driver hal_eint_mask fail\r\n", 0);
        return;
    }

    if (HAL_EINT_STATUS_OK != hal_eint_init(AXIS_SENSOR_EINT_NUM, &hwvad_eint_config)) {
        log_hal_msgid_error("driver eint init fail\r\n", 0);
        return;
    }

    /*register eint callback*/
    if (HAL_EINT_STATUS_OK != hal_eint_register_callback(AXIS_SENSOR_EINT_NUM, init1_callback, NULL)) {
        log_hal_msgid_error("driver eint register callback fail\r\n", 0);
        return;
    }

    if (HAL_EINT_STATUS_OK != hal_eint_unmask(AXIS_SENSOR_EINT_NUM)) {
        log_hal_msgid_error("driver hal_eint_mask fail\r\n", 0);
        return;
    }

#ifdef AXIS_SENSOR_DEBUG_ENABLE
    log_hal_msgid_info("init1 eint init OK\r\n",0);
#endif
    return;
}

static void int1_gpio_deinit(void)
{
    hal_gpio_deinit(AXIS_SENSOR_EINT_PIN);

    /*init GPIO eint*/
    if (HAL_EINT_STATUS_OK != hal_eint_mask(AXIS_SENSOR_EINT_NUM)) {
        log_hal_msgid_error("driver hal_eint_mask fail\r\n", 0);
        return;
    }

    if (HAL_EINT_STATUS_OK != hal_eint_deinit(AXIS_SENSOR_EINT_NUM)) {
        log_hal_msgid_error("driver eint init fail\r\n", 0);
        return;
    }

    printf("init1 eint deinit OK\r\n");
    return;
}

void bsp_multi_axis_read_sensor_data(void)
{
    uint16_t fifo_length = 0;
    uint16_t rslt = BMA4_OK;
    uint16_t n_instances = 0;
    uint8_t int1_sta = 0;
    uint32_t i;
    uint32_t nvic_mask = 0;
    struct bma4_accel padding_data;

    hal_nvic_save_and_set_interrupt_mask(&nvic_mask);
    if (g_bsp_sensor_is_busy == true) {
        hal_nvic_restore_interrupt_mask(nvic_mask);
        log_hal_msgid_error("[G-sensor] busy now\r\n", 0);
        return;
    } else {
        g_bsp_sensor_is_busy = true;
    }
    hal_nvic_restore_interrupt_mask(nvic_mask);

    rslt |= bma4_read_int_status_1(&int1_sta, &accel);

    if (int1_sta == 0x02) {
        rslt |= bma4_get_fifo_length(&fifo_length, &accel);
        if (rslt != BMA4_OK) {
            log_hal_msgid_error("[G-sensor]bsp_multi_axis_read_sensor_data ,bma4_get_fifo_length() rslt = %d", 1, rslt);
            g_bsp_sensor_is_busy = false;
            return;
        }

        accel.fifo->length = fifo_length;

#ifdef AXIS_SENSOR_DEBUG_ENABLE
        log_hal_msgid_info("[G-sensor] accel.fifo->length = %d\r\n", 1,accel.fifo->length);
#endif

        /* Read all data from the sensor FIFO buffer */
        rslt |= bma4_read_fifo_data(&accel); // Read FIFO data
        if (rslt != BMA4_OK) {
            log_hal_msgid_error("[G-sensor]bsp_multi_axis_read_sensor_data ,bma4_read_fifo_data() rslt = %d", 1, rslt);
            g_bsp_sensor_is_busy = false;
            return;
        }

        /*if fifo data is over the 64 frames(448 bytes),the over data will be ignore
                if fifo data is less than 64 frames(448 bytes),the last data will padding with last frame*/
        if (fifo_length >= 448) {
            accel.fifo->length = 448;
        } else {
            accel.fifo->length = fifo_length - (fifo_length % 7);
        }

        /* Parse the FIFO buffer and extract requried number of accelerometer data frames */
        rslt |= bma4_extract_accel(sens_data, &n_instances, &accel);
        if (rslt != BMA4_OK) {
            log_hal_msgid_error("[G-sensor]bsp_multi_axis_read_sensor_data,bma4_extract_accel() rslt = %d", 1, rslt);
            g_bsp_sensor_is_busy = false;
            return;
        }

        if (n_instances > 0) {
            /*copy data to the ringbuf*/
            if (ringbuf_write_data((uint8_t *)sens_data, n_instances * sizeof(struct bma4_accel)) == false) {
                log_hal_msgid_error("[G-sensor]bsp_multi_axis_read_sensor_data:ringbuf_write_data fail\r\n", 0);
                g_bsp_sensor_is_busy = false;
                return;
            }
        } else {
            log_hal_msgid_error("[G-sensor]bsp_multi_axis_read_sensor_data:instance convert error\r\n", 0);
            g_bsp_sensor_is_busy = false;
            return;
        }

        if (n_instances < 64) {
            /*padding last frame data to the ringbuf*/
            padding_data = sens_data[n_instances - 1];
            for (i = n_instances; i < 64; i++) {
                sens_data[i] = padding_data;
            }
            if (ringbuf_write_data((uint8_t *)(&(sens_data[n_instances - 1])), (64 - n_instances) * sizeof(struct bma4_accel)) == false) {
                log_hal_msgid_error("[G-sensor]bsp_multi_axis_read_sensor_data:ringbuf_write_data fail\r\n", 0);
                g_bsp_sensor_is_busy = false;
                return;
            }
        }
    } else {
        log_hal_msgid_error("[G-sensor] bsp_multi_axis_read_sensor_data INT1 irq status is not correct,int1_sta = 0x%08x\r\n", 1, int1_sta);
        g_bsp_sensor_is_busy = false;
        return;
    }

    hal_eint_unmask(AXIS_SENSOR_EINT_NUM);

    g_bsp_sensor_is_busy = false;
}



/**
  *  @brief  This function is used to initialize the axis sensor.
  *  @param vendor : sensor chip name .
  *  @return If the function succeeds, the return value is BSP_MULTI_AXIS_OK.
  *  Otherwise, an error code is returned
  */
bsp_multi_axis_status_t bsp_multi_axis_initialize(bsp_multi_axis_chip_type_t vendor, bsp_multi_axis_config_t *acc_cfg)
{
    uint16_t rslt = BMA4_OK;
    bsp_multi_axis_status_t ret = BSP_MULTI_AXIS_RETURN_INIT_VALUE;
    /* Declare an accelerometer configuration structure */
    struct bma4_accel_config accel_conf;
    uint32_t nvic_mask = 0;

    acc_cfg = acc_cfg;

    hal_nvic_save_and_set_interrupt_mask(&nvic_mask);
    if (g_bsp_sensor_is_busy == true) {
        hal_nvic_restore_interrupt_mask(nvic_mask);
        log_hal_msgid_error("[G-sensor] busy now\r\n", 0);
        return BSP_MULTI_AXIS_BUSY_ERROR;
    } else {
        g_bsp_sensor_is_busy = true;
    }
    hal_nvic_restore_interrupt_mask(nvic_mask);

    switch (vendor) {
        case BMA456:
            /* Initialize the device instance as per the initialization example */
            rslt |= bma456_init(&accel);
            if (rslt != BMA4_OK) {
                log_hal_msgid_error("[G-sensor] bma456_init() rslt = %d", 1, rslt);
                break;
            }

            /* Disable the advanced power save mode to configure the FIFO buffer */
            rslt |= bma4_set_advance_power_save(BMA4_DISABLE, &accel);
            if (rslt != BMA4_OK) {
                log_hal_msgid_error("[G-sensor] bma4_set_advance_power_save() rslt = %d", 1, rslt);
                break;
            }


            /* b. Performing initialization sequence.
                        c. Checking the correct status of the initialization sequence.
                    */
            rslt |= bma456_write_config_file(&accel);
            if (rslt != BMA4_OK) {
                log_hal_msgid_error("[G-sensor] bma456_write_config_file() rslt = %d", 1, rslt);
                break;
            }

            /* Configure the accelerometer */
            /* Assign the desired settings */
            accel_conf.odr = BMA4_OUTPUT_DATA_RATE_1600HZ;
            accel_conf.range = BMA4_ACCEL_RANGE_4G;
            accel_conf.bandwidth = BMA4_ACCEL_NORMAL_AVG4;
            accel_conf.perf_mode = BMA4_CONTINUOUS_MODE;

            /* Set the configuration */
            rslt |= bma4_set_accel_config(&accel_conf, &accel);
            if (rslt != BMA4_OK) {
                log_hal_msgid_error("[G-sensor] bma4_set_accel_config() rslt = %d", 1, rslt);
                break;
            }

            rslt |= bma4_set_int_pin_config(&int_pin_config, BMA4_INTR1_MAP, &accel);
            if (rslt != BMA4_OK) {
                log_hal_msgid_error("[G-sensor] bma4_set_int_pin_config() rslt = %d", 1, rslt);
                break;
            }
            hal_gpt_delay_ms(50);

            /*434 water mark is a tuning val that could adapt 64 frames read data once a time*/
            rslt |= bma4_set_fifo_wm(434, &accel);
            if (rslt != BMA4_OK) {
                log_hal_msgid_error("[G-sensor] bma4_set_fifo_wm() rslt = %d", 1, rslt);
                break;
            }
            hal_gpt_delay_ms(50);

            rslt |= bma4_set_interrupt_mode(BMA4_NON_LATCH_MODE, &accel);
            if (rslt != BMA4_OK) {
                log_hal_msgid_error("[G-sensor] bma4_set_interrupt_mode() rslt = %d", 1, rslt);
                break;
            }

            accel.fifo = &fifo_frame;

            /* Configure the FIFO buffer */
            rslt |= bma4_set_fifo_config((BMA4_FIFO_ACCEL | BMA4_FIFO_HEADER), BMA4_ENABLE, &accel);
            if (rslt != BMA4_OK) {
                log_hal_msgid_error("[G-sensor] bma4_set_fifo_config() rslt = %d", 1, rslt);
                break;
            }

            /*config fifo watermark & remap watermark IRQ to INT1 pin*/
            rslt |= bma456_map_interrupt(BMA4_INTR1_MAP, BMA4_FIFO_WM_INT, BMA4_ENABLE, &accel);
            if (rslt != BMA4_OK) {
                log_hal_msgid_error("[G-sensor] bma456_map_interrupt() rslt = %d", 1, rslt);
                break;
            }

            int1_gpio_init();

            /*reset ring buf*/
            ringbuf_reset();
            g_bsp_sensor_inited_flg = true;
            ret = BSP_MULTI_AXIS_OK;
            break;
        default:
            break;
    }

    g_bsp_sensor_is_busy = false;

    return ret;
}

/**
  *  @brief  This function is used to deinit the axis sensor.
  *  @param vendor : sensor chip name .
  *  @return If the function succeeds, the return value is BSP_MULTI_AXIS_OK.
  *  Otherwise, an error code is returned
  */
bsp_multi_axis_status_t bsp_multi_axis_deinit(bsp_multi_axis_chip_type_t vendor)
{
    bsp_multi_axis_status_t ret = BSP_MULTI_AXIS_RETURN_INIT_VALUE;
    uint32_t nvic_mask = 0;

    hal_nvic_save_and_set_interrupt_mask(&nvic_mask);
    if (g_bsp_sensor_is_busy == true) {
        hal_nvic_restore_interrupt_mask(nvic_mask);
        log_hal_msgid_error("[G-sensor] busy now\r\n", 0);
        return BSP_MULTI_AXIS_BUSY_ERROR;
    } else {
        g_bsp_sensor_is_busy = true;
    }
    hal_nvic_restore_interrupt_mask(nvic_mask);

    switch (vendor) {
        case BMA456:
            if (g_bsp_sensor_inited_flg == true) {
                int1_gpio_deinit();
                /*disable BMA456 accel*/
                bma4_set_accel_enable(0, &accel);
                /*flush BMA456 fifo*/
                bma4_set_command_register(0xb0, &accel);
                /*reset BMA456*/
                bma4_set_command_register(0xb6, &accel);
                hal_gpt_delay_ms(200);
                if (true == g_bsp_i2c_inited_flg) {
                    /*deinit I2C*/
                    if (hal_i2c_master_deinit(AXIS_SENSOR_IIC_PORT) != HAL_I2C_STATUS_OK) {
                        log_hal_msgid_error("[G-sensor] I2C deinit error", 0);
                        break;
                    }
                }
                g_bsp_i2c_inited_flg = false;
                ringbuf_reset();
                ret = BSP_MULTI_AXIS_OK;
            } else {
                log_hal_msgid_error("[G-sensor] sensor did not init", 0);
                break;
            }
            break;
        default:
            break;
    }

    g_bsp_sensor_is_busy = false;

    return ret;
}

/**
  *  @brief  This function is used to enable the axis sensor.
  *  @param vendor : sensor chip name .
  *  @return If the function succeeds, the return value is BSP_MULTI_AXIS_OK.
  *  Otherwise, an error code is returned
  */
bsp_multi_axis_status_t bsp_multi_axis_enable(bsp_multi_axis_chip_type_t vendor)
{
    bsp_multi_axis_status_t ret = BSP_MULTI_AXIS_RETURN_INIT_VALUE;
    uint16_t rslt = BMA4_OK;
    uint32_t nvic_mask = 0;

    hal_nvic_save_and_set_interrupt_mask(&nvic_mask);
    if (g_bsp_sensor_is_busy == true) {
        hal_nvic_restore_interrupt_mask(nvic_mask);
        log_hal_msgid_error("[G-sensor] busy now\r\n", 0);
        return BSP_MULTI_AXIS_BUSY_ERROR;
    } else {
        g_bsp_sensor_is_busy = true;
    }
    hal_nvic_restore_interrupt_mask(nvic_mask);

    if (g_bsp_sensor_inited_flg == false) {
        log_hal_msgid_error("[G-sensor] sensor did not init", 0);
        return ret;
    }

    switch (vendor) {
        case BMA456:
            /* Enable the accelerometer */
            rslt |= bma4_set_accel_enable(1, &accel);
            if (rslt != BMA4_OK) {
                log_hal_msgid_error("[G-sensor] bma4_set_accel_enable() rslt = %d", 1, rslt);
                break;
            }
            ret = BSP_MULTI_AXIS_OK;
            break;
        default:
            break;
    }

    g_bsp_sensor_is_busy = false;

    return ret;
}

/**
  *  @brief  This function is used to disable the axis sensor.
  *  @param vendor : sensor chip name .
  *  @return If the function succeeds, the return value is BSP_MULTI_AXIS_OK.
  *  Otherwise, an error code is returned
  */
bsp_multi_axis_status_t bsp_multi_axis_disable(bsp_multi_axis_chip_type_t vendor)
{
    bsp_multi_axis_status_t ret = BSP_MULTI_AXIS_RETURN_INIT_VALUE;
    uint16_t rslt = BMA4_OK;
    uint32_t nvic_mask = 0;

    hal_nvic_save_and_set_interrupt_mask(&nvic_mask);
    if (g_bsp_sensor_is_busy == true) {
        hal_nvic_restore_interrupt_mask(nvic_mask);
        log_hal_msgid_error("[G-sensor] busy now\r\n", 0);
        return BSP_MULTI_AXIS_BUSY_ERROR;
    } else {
        g_bsp_sensor_is_busy = true;
    }
    hal_nvic_restore_interrupt_mask(nvic_mask);

    if (g_bsp_sensor_inited_flg == false) {
        log_hal_msgid_error("[G-sensor] sensor did not init", 0);
        return ret;
    }

    switch (vendor) {
        case BMA456:
            /* Enable the accelerometer */
            rslt |= bma4_set_accel_enable(0, &accel);
            if (rslt != BMA4_OK) {
                log_hal_msgid_error("[G-sensor] bma4_set_accel_enable() rslt = %d", 1, rslt);
                break;
            }
            ret = BSP_MULTI_AXIS_OK;
            break;
        default:
            break;
    }

    g_bsp_sensor_is_busy = false;

    return ret;
}

/**
  *  @brief  This function is used to get sw fifo frame cnt.
  *  @param[in] vendor : sensor chip name .
  *  @return the count of frame.
  */
uint32_t bsp_multi_axis_get_frame_cnt(bsp_multi_axis_chip_type_t vendor)
{
    uint32_t frame_cnt = 0;

    vendor = vendor;

    frame_cnt = ringbuf_get_data_length();
    return frame_cnt;
}

/**
  *  @brief  This function is used to get sensor data that stored in the sw fifo.
  *  @param vendor : sensor chip name .
  *  @param addr : addr you want to store sensor data.
  *  @param frame_cnt:frame you want to get.
  *  @return the frame number that you really get.
  */
uint32_t bsp_multi_axis_get_data(bsp_multi_axis_chip_type_t vendor, bsp_multi_axis_data_t *addr, uint32_t frame_cnt)
{
    vendor = vendor;

    if (ringbuf_read_data((uint8_t *)addr, frame_cnt) == false) {
        log_hal_msgid_error("[G-sensor] bsp_multi_axis_get_data ringbuffer read fail!", 0);
        return 0;
    }
    return frame_cnt;
}

/*debug functions*/
/*for read sensor register by one byte*/
bool bsp_multi_axis_read_register(uint32_t addr, uint8_t *data)
{
    if (g_bsp_sensor_inited_flg == false) {
        bma456_init(&accel);
        g_bsp_sensor_inited_flg = true;
    }

    /*read 1 byte address rg data*/
    if (BMA4_OK != bma4_read_regs((uint8_t)addr, data, 1, &accel)) {
        return false;
    }

    return true;
}

/*for write sensor register by one byte*/
bool bsp_multi_axis_write_register(uint32_t addr, uint8_t *data)
{
    if (g_bsp_sensor_inited_flg == false) {
        bma456_init(&accel);
        g_bsp_sensor_inited_flg = true;
    }

    /*write 1 bytes data to addr rg address*/
    if (BMA4_OK != bma4_write_regs((uint8_t)addr, data, 1, &accel)) {
        return false;
    }

    return true;
}

/*for read sensor acc raw data*/
static bool read_raw_data_init_flg = false;
bool bsp_multi_axis_read_raw_data(void)
{
    uint16_t rslt = BMA4_OK;
    /* Declare an instance of the sensor data structure */
    struct bma4_accel sens_data;
    /* Declare an accelerometer configuration structure */
    struct bma4_accel_config accel_conf;
    float x, y, z;

    if (read_raw_data_init_flg == false) {

        /* Initialize the device instance as per the initialization example */
        rslt |= bma456_init(&accel);
        if (BMA4_OK != rslt) {
            log_hal_msgid_error("[G-sensor] bma456_init fail", 0);
            return false;
        }

        /* b. Performing initialization sequence.
              c. Checking the correct status of the initialization sequence.
         */
        rslt |= bma456_write_config_file(&accel);
        if (BMA4_OK != rslt) {
            log_hal_msgid_error("[G-sensor] bma456_write_config_file fail", 0);
            return false;
        }

        /* Configure the accelerometer */
        /* Assign the desired settings */
        accel_conf.odr = BSP_MULTI_AXIS_OUTPUT_DATA_RATE_1600HZ;
        accel_conf.range = BMA4_ACCEL_RANGE_4G;
        accel_conf.bandwidth = BMA4_ACCEL_NORMAL_AVG4;
        accel_conf.perf_mode = BMA4_CONTINUOUS_MODE;

        /* Set the configuration */
        rslt |= bma4_set_accel_config(&accel_conf, &accel);
        if (BMA4_OK != rslt) {
            log_hal_msgid_error("[G-sensor] bma4_set_accel_config fail", 0);
            return false;
        }

        /* Enable the accelerometer */
        rslt |= bma4_set_accel_enable(1, &accel);
        if (BMA4_OK != rslt) {
            log_hal_msgid_error("[G-sensor] bma4_set_accel_enable fail", 0);
            return false;
        }

        read_raw_data_init_flg = true;
    }

    /* Read the sensor data into the sensor data instance */
    rslt |= bma4_read_accel_xyz(&sens_data, &accel);
    if (BMA4_OK != rslt) {
        log_hal_msgid_error("[G-sensor] bma4_read_accel_xyz fail", 0);
        return false;
    }

    /* Use the data */
    log_hal_msgid_info("[G-sensor] raw data X: %d, Y: %d, Z: %d", 3, sens_data.x, sens_data.y, sens_data.z);
    x = (float)((sens_data.x) / SENSITIVITY_4G);
    y = (float)((sens_data.y) / SENSITIVITY_4G);
    z = (float)((sens_data.z) / SENSITIVITY_4G);
    printf("[G-sensor] X: %.2f g,Y: %.2f g,Z: %.2f g", x, y, z);

    return true;
}







