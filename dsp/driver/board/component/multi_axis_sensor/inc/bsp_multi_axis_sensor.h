/* Copyright Statement:
 *
 * (C) 2020  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __BSP_MULTI_AXIS_H__
#define __BSP_MULTI_AXIS_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bma4_defs.h"
#include "bma4.h"
#include "bma456.h"

/* Private macro -------------------------------------------------------------*/
#define SENSITIVITY_2G 16384.00
#define SENSITIVITY_4G 8192.00
#define SENSITIVITY_8G 4096.00
#define SENSITIVITY_16G 2048.00

typedef uint32_t (*bsp_multi_axis_ready_to_read_callback_t)(uint32_t frame_cnt, void *user_data);

/** @brief This enum defines the return type of axis sensor API. */
typedef enum {
    BSP_MULTI_AXIS_GET_CHIP_ID_ERROR                    = -8,
    BSP_MULTI_AXIS_OUT_OF_RANGE_ERROR                   = -5,
    BSP_MULTI_AXIS_BUSY_ERROR                           = -4,
    BSP_MULTI_AXIS_TIME_OUT_ERROR                       = -3,
    BSP_MULTI_AXIS_NULL_POINT_ERROR                     = -2,
    BSP_MULTI_AXIS_RETURN_INIT_VALUE                    = -1,
    BSP_MULTI_AXIS_OK                                   = 0,
} bsp_multi_axis_status_t;

/** @brief This enum defines the accel axis sensor range. */
typedef enum {
    BSP_MULTI_AXIS_ACCEL_RANGE_2G = BMA4_ACCEL_RANGE_2G,
    BSP_MULTI_AXIS_ACCEL_RANGE_4G = BMA4_ACCEL_RANGE_4G,
    BSP_MULTI_AXIS_ACCEL_RANGE_8G = BMA4_ACCEL_RANGE_8G ,
    BSP_MULTI_AXIS_ACCEL_RANGE_16G = BMA4_ACCEL_RANGE_16G,
} bsp_multi_axis_accel_range_t;

/** @brief This enum defines the accel axis sensor oversample. */
typedef enum {
    BSP_MULTI_AXIS_ACCEL_OSR4_AVG1   = BMA4_ACCEL_OSR4_AVG1,
    BSP_MULTI_AXIS_ACCEL_OSR2_AVG2   = BMA4_ACCEL_OSR2_AVG2,
    BSP_MULTI_AXIS_ACCEL_NORMAL_AVG4 = BMA4_ACCEL_NORMAL_AVG4,
    BSP_MULTI_AXIS_ACCEL_CIC_AVG8    = BMA4_ACCEL_CIC_AVG8,
} bsp_multi_axis_accel_bandwidth_t;

/** @brief This enum defines the axis sensor output data rate. */
typedef enum {
    BSP_MULTI_AXIS_OUTPUT_DATA_RATE_25HZ = BMA4_OUTPUT_DATA_RATE_25HZ,
    BSP_MULTI_AXIS_OUTPUT_DATA_RATE_50HZ = BMA4_OUTPUT_DATA_RATE_50HZ,
    BSP_MULTI_AXIS_OUTPUT_DATA_RATE_100HZ = BMA4_OUTPUT_DATA_RATE_100HZ,
    BSP_MULTI_AXIS_OUTPUT_DATA_RATE_200HZ = BMA4_OUTPUT_DATA_RATE_200HZ,
    BSP_MULTI_AXIS_OUTPUT_DATA_RATE_400HZ = BMA4_OUTPUT_DATA_RATE_400HZ,
    BSP_MULTI_AXIS_OUTPUT_DATA_RATE_800HZ = BMA4_OUTPUT_DATA_RATE_800HZ,
    BSP_MULTI_AXIS_OUTPUT_DATA_RATE_1600HZ = BMA4_OUTPUT_DATA_RATE_1600HZ,
} bsp_multi_axis_output_data_rate_t;


/** @brief Structure bma456 acc config. */
typedef struct  {
    bsp_multi_axis_accel_range_t accel_range;
    bsp_multi_axis_accel_bandwidth_t accel_bandwidth;
    bsp_multi_axis_output_data_rate_t accel_odr;
}bsp_multi_axis_config_t;

/** @brief This enum defines the axis sensor vendor. */
typedef enum {
    BMA456 = 1,
    NOT_SUPPORT_AXIS_SENSOR,
} bsp_multi_axis_chip_type_t;

/** @brief This enum defines the axis sensor type. */
typedef enum {
    BSP_MULTI_AXIS_ACCEL_ENABLE = 1,
} bsp_multi_axis_enable_t;

/**@brief Structure bma456 acc data with float output*/
typedef struct  {
    float  x;   /**<bma456 X g convert data*/
    float  y;   /**<bma456 Y g convert data*/
    float  z;   /**<bma456 Z g convert data*/
} bsp_multi_axis_acc_float_t;

/**@brief Structure bma456 acc data with 16bit output*/
typedef struct  {
    int16_t  x;    /**<bma456 X raw data*/
    int16_t  y;    /**<bma456 Y raw data*/
    int16_t  z;    /**<bma456 Z raw data*/
} bsp_multi_axis_acc_int_t;

/** @brief Structure containing sensor all data*/
typedef struct  {
    bsp_multi_axis_acc_int_t accel_data_i;
    //bsp_multi_axis_acc_float_t accel_data_f;   
} bsp_multi_axis_data_t;

/*define pre-roll manage handler*/
typedef struct {
    uint8_t *p_buffer;       //ring buffer address
    uint32_t buffer_size;    //ring buffer total size
    uint32_t p_read;         //ring buffer read pointer
    uint32_t p_write;        //ring buffer write pointer
    uint32_t data_length;    //ring buffer current data length
} sensor_ringbuf_handler_t;

/*static functions*/
uint16_t bsp_i2c_read_data(uint8_t dev_addr, uint8_t reg_addr, uint8_t *read_data, uint16_t len);
uint16_t bsp_i2c_write_data(uint8_t dev_addr, uint8_t reg_addr, uint8_t *write_data, uint16_t len);
void bsp_delay_ms(uint32_t period);
void bsp_delay_us(uint32_t period);


/*public functions*/
bsp_multi_axis_status_t bsp_multi_axis_initialize(bsp_multi_axis_chip_type_t vendor,bsp_multi_axis_config_t *acc_cfg);
bsp_multi_axis_status_t bsp_multi_axis_deinit(bsp_multi_axis_chip_type_t vendor);
bsp_multi_axis_status_t bsp_multi_axis_enable(bsp_multi_axis_chip_type_t vendor);
bsp_multi_axis_status_t bsp_multi_axis_disable(bsp_multi_axis_chip_type_t vendor);
void bsp_multi_axis_read_sensor_data(void);
uint32_t bsp_multi_axis_get_frame_cnt(bsp_multi_axis_chip_type_t vendor);
uint32_t bsp_multi_axis_get_data(bsp_multi_axis_chip_type_t vendor, bsp_multi_axis_data_t *addr, uint32_t frame_cnt);
//bool bsp_multi_axis_read_register(uint32_t addr,uint8_t *data);
//bool bsp_multi_axis_write_register(uint32_t addr,uint8_t *data);
//bool bsp_multi_axis_read_raw_data(void);




#endif

