/* Copyright Statement:
 *
 * (C) 2020  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

/* if it is iic interface,please define this marco */
#ifndef BSP_MULTI_AXIS_SENSOR_CONFIGH_H
#define BSP_MULTI_AXIS_SENSOR_CONFIGH_H
#include <hal_platform.h>

/*I2C peripheral hw port */
#define AXIS_SENSOR_IIC_PORT HAL_I2C_MASTER_0

/*I2C peripheral SCL clk freq*/
#define AXIS_SENSOR_IIC_FREQENCE HAL_I2C_FREQUENCY_400K

/*I2C peripheral SCL GPIO*/
#define AXIS_SENSOR_IIC_SCL_PIN  HAL_GPIO_8

/*I2C peripheral SDA GPIO*/
#define AXIS_SENSOR_IIC_SDA_PIN  HAL_GPIO_9

/*I2C peripheral send data buffer max size*/
#define I2C_BUFFER_MAX_LEN 9

/*I2C peripheral send to receive send data length*/
#define I2C_SEND_TO_RECEIVE_SEND_LEN 1

/*sensor SW buffer size*/
#define AXIS_SENSOR_BUFFER_SIZE (4 * 64 * 6)

/*EINT GPIO pinmux*/
#define AXIS_SENSOR_EINT_PIN HAL_GPIO_26

/*EINT GPIO pinmux*/
#define AXIS_SENSOR_EINT_PINMUX 9

/*EINT numbfer*/
#define AXIS_SENSOR_EINT_NUM HAL_EINT_NUMBER_26

/*debug option*/
//#define AXIS_SENSOR_DEBUG_ENABLE

#endif
