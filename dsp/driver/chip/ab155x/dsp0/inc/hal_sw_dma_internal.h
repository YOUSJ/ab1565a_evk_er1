/* Copyright Statement:
 *
 * (C) 2018  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __HAL_SW_DMA_INTERNAL_H__
#define __HAL_SW_DMA_INTERNAL_H__
#include "hal_sw_dma.h"

#ifdef HAL_SW_DMA_MODULE_ENABLED

#ifdef __cplusplus
extern "C" {
#endif

typedef struct __SW_DMA_BUFFER_INFO {
    uint8_t Read;            /* field Current Read index. */
    uint8_t Write;           /* field Current Write index. */
    uint8_t Length;          /* field Length of buffer */
} SW_DMA_BUFFER_INFO;

typedef void (*sw_dma_callback_t)(void);

#define ResetFifo(Buffer)       (Buffer.Write = Buffer.Read = 0)
#define BWrite(Buffer)          (Buffer->Write)
#define BRead(Buffer)           (Buffer->Read)
#define BLength(Buffer)         (Buffer->Length)

#define Buff_isFull    1
#define Buff_notFull   0


#define Buf_init(_Buffer,_uTotalSize) \
{\
   SW_DMA_BUFFER_INFO *_Buf=_Buffer;\
   _Buf->Read = 0;\
    _Buf->Write = 0;\
    _Buf->Length = _uTotalSize;\
}\


#define Buf_GetRoomLeft(_Buffer,_RoomLeft)   \
{\
   SW_DMA_BUFFER_INFO *_Buf=_Buffer;\
   if ( BRead(_Buf) <= BWrite(_Buf) ) \
    {\
      _RoomLeft = BLength(_Buf) - BWrite(_Buf) + BRead(_Buf) - 1;\
    }\
    else\
    {\
        _RoomLeft = BRead(_Buf) - BWrite(_Buf) - 1;\
    }\
}\


#define Buf_Push(_Buffer) \
{\
   SW_DMA_BUFFER_INFO *_Buf=_Buffer;\
   if(BWrite(_Buf) >= (BLength(_Buf) - 1))\
   {\
    BWrite(_Buf) = 0;\
   }\
   else\
   {\
    BWrite(_Buf)++;\
   }\
}\

#define Buf_GetAvail(_Buffer,_Available) \
{\
   SW_DMA_BUFFER_INFO *_Buf = _Buffer;\
    _Available = 0;\
    if (BWrite(_Buf) >= BRead(_Buf))\
        _Available = BWrite(_Buf) - BRead(_Buf);\
    else\
        _Available = BLength(_Buf) - BRead(_Buf) + BWrite(_Buf);    \
}\


#define Buf_Pop(_Buffer,_pop_num)   \
{\
   SW_DMA_BUFFER_INFO *_Buf = _Buffer;\
    BRead(_Buf)+= _pop_num;\
    if (BRead(_Buf) >= BLength(_Buf))\
    {\
        BRead(_Buf) -= BLength(_Buf);\
    }\
}\


#define Buf_Flush(_Buffer) \
{\
   SW_DMA_BUFFER_INFO *_Buf = _Buffer;\
    _Buf->Write = _Buf->Read = 0;\
}

#define sw_dma_handle_t     uint32_t
#define SW_DMA_HANDLE_MAGIC  0x80000000

#define DMA_MIN_LENGHT_VALUE 0x0001
#define DMA_MAX_LENGTH_VALUE 0xFFFF


#define SW_DMA_GLBSTA_BASE_FOR_DSP    ((uint32_t)0xA3000000)
#define SW_DMA_GLBSTA_BASE_FOR_CM4    ((uint32_t)0xA0020000)
#define SW_DMA_GLBSTA_FOR_DSP         (*(volatile uint32_t *)(SW_DMA_GLBSTA_BASE_FOR_DSP))
#define SW_DMA_GLBSTA_FOR_CM4         (*(volatile uint32_t *)(SW_DMA_GLBSTA_BASE_FOR_CM4))

#define SW_DMA_CLK_SET_BASE_FOR_DSP   ((uint32_t)0xA3000074)
#define SW_DMA_CLK_SET_BASE_FOR_CM4   ((uint32_t)0xA0020074)
#define SW_DMA_CLK_SET_FOR_DSP        (*(volatile uint32_t *)(SW_DMA_CLK_SET_BASE_FOR_DSP))
#define SW_DMA_CLK_SET_FOR_CM4        (*(volatile uint32_t *)(SW_DMA_CLK_SET_BASE_FOR_CM4))

#define SW_DMA_INT_SET_BASE_FOR_DSP0  ((uint32_t)0xA3000018)
#define SW_DMA_INT_SET_BASE_FOR_DSP1  ((uint32_t)0xA3000054)
#define SW_DMA_INT_SET_BASE_FOR_CM4   ((uint32_t)0xA002000C)
#define SW_DMA_INT_SET_FOR_DSP0       (*(volatile uint32_t *)(SW_DMA_INT_SET_BASE_FOR_DSP0))
#define SW_DMA_INT_SET_FOR_DSP1       (*(volatile uint32_t *)(SW_DMA_INT_SET_BASE_FOR_DSP1))
#define SW_DMA_INT_SET_FOR_CM4        (*(volatile uint32_t *)(SW_DMA_INT_SET_BASE_FOR_CM4))

#define SW_DMA_REG_BASE_DSP(channel)  ((uint32_t)(SW_DMA_GLBSTA_BASE_FOR_DSP+0x100*(channel+1)))
#define SW_DMA_REG_BASE_CM4(channel)  ((uint32_t)(SW_DMA_GLBSTA_BASE_FOR_CM4+0x100*(channel-4+1)))

#define SW_DMA_REG_SRC_DSP(channel)       (*(volatile uint32_t *)(SW_DMA_REG_BASE_DSP(channel)+0x0))
#define SW_DMA_REG_DST_DSP(channel)       (*(volatile uint32_t *)(SW_DMA_REG_BASE_DSP(channel)+0x4))
#define SW_DMA_REG_WPPT_DSP(channel)      (*(volatile uint32_t *)(SW_DMA_REG_BASE_DSP(channel)+0x8))
#define SW_DMA_REG_WPTO_DSP(channel)      (*(volatile uint32_t *)(SW_DMA_REG_BASE_DSP(channel)+0xC))
#define SW_DMA_REG_COUNT_DSP(channel)     (*(volatile uint32_t *)(SW_DMA_REG_BASE_DSP(channel)+0x10))
#define SW_DMA_REG_CON_DSP(channel)       (*(volatile uint32_t *)(SW_DMA_REG_BASE_DSP(channel)+0x14))
#define SW_DMA_REG_START_DSP(channel)     (*(volatile uint32_t *)(SW_DMA_REG_BASE_DSP(channel)+0x18))
#define SW_DMA_REG_ACKINT_DSP(channel)    (*(volatile uint32_t *)(SW_DMA_REG_BASE_DSP(channel)+0x20))
#define SW_DMA_REG_LIMITER_DSP(channel)   (*(volatile uint32_t *)(SW_DMA_REG_BASE_DSP(channel)+0x28))

#define SW_DMA_REG_SRC_CM4(channel)       (*(volatile uint32_t *)(SW_DMA_REG_BASE_CM4(channel)+0x0))
#define SW_DMA_REG_DST_CM4(channel)       (*(volatile uint32_t *)(SW_DMA_REG_BASE_CM4(channel)+0x4))
#define SW_DMA_REG_WPPT_CM4(channel)      (*(volatile uint32_t *)(SW_DMA_REG_BASE_CM4(channel)+0x8))
#define SW_DMA_REG_WPTO_CM4(channel)      (*(volatile uint32_t *)(SW_DMA_REG_BASE_CM4(channel)+0xC))
#define SW_DMA_REG_COUNT_CM4(channel)     (*(volatile uint32_t *)(SW_DMA_REG_BASE_CM4(channel)+0x10))
#define SW_DMA_REG_CON_CM4(channel)       (*(volatile uint32_t *)(SW_DMA_REG_BASE_CM4(channel)+0x14))
#define SW_DMA_REG_START_CM4(channel)     (*(volatile uint32_t *)(SW_DMA_REG_BASE_CM4(channel)+0x18))
#define SW_DMA_REG_ACKINT_CM4(channel)    (*(volatile uint32_t *)(SW_DMA_REG_BASE_CM4(channel)+0x20))
#define SW_DMA_REG_LIMITER_CM4(channel)   (*(volatile uint32_t *)(SW_DMA_REG_BASE_CM4(channel)+0x28))

bool sw_dma_channel_is_idle(uint32_t channel);
bool sw_dma_channel_has_interrupt(uint32_t channel);
hal_sw_gdma_status_t sw_dma_start_interrupt(hal_sw_dma_config_info_t *info);
void sw_dma_init(uint32_t channel);
void sw_dma_register_callback(sw_dma_callback_t callback);

#ifdef __cplusplus
}
#endif

#endif /* HAL_SW_DMA_MODULE_ENABLED */
#endif /* __HAL_SW_DMA_INTERNAL_H__ */




