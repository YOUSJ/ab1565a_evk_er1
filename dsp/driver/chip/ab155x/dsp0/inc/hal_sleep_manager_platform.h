/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#include "hal_platform.h"

#ifndef __HAL_SLEEP_MANAGER_PLATFORM_H__
#define __HAL_SLEEP_MANAGER_PLATFORM_H__

#ifdef HAL_SLEEP_MANAGER_ENABLED

typedef enum {
    SLEEP_LOCK_DMA                      = 0,    //to do dsp0 sleep and dcm both lock
    SLEEP_LOCK_SFC                      = 1,
    SLEEP_LOCK_I2S0                     = 2,
    SLEEP_LOCK_I2S1                     = 3,
    SLEEP_LOCK_I2S2                     = 4,
    SLEEP_LOCK_I2S3                     = 5,
    SLEEP_LOCK_SPI_MST0                 = 6,
    SLEEP_LOCK_SPI_MST1                 = 7,
    SLEEP_LOCK_SPI_SLV                  = 8,
    SLEEP_LOCK_UART0                    = 9,
    SLEEP_LOCK_UART1                    = 10,
    SLEEP_LOCK_I2C0                     = 11,
    SLEEP_LOCK_I2C1                     = 12,  
#ifdef HAL_SLEEP_MANAGER_LOCK_SLEEP_CROSS_CORE
    SLEEP_LOCK_CM4_CROSS_CORE_START     ,//for lock CM4 sleep cross multi core
    SLEEP_LOCK_CM4_CROSS_CORE_END       ,
    SLEEP_LOCK_DSP0_CROSS_CORE_START    ,//for lock DSP0 sleep cross multi core
    SLEEP_LOCK_DSP0_CROSS_CORE_PRELOADER,       //to do dsp0 sleep and dcm both lock
    SLEEP_LOCK_DSP0_CROSS_CORE_MEMORY_ACCESS,   //to do dsp0 sleep and dcm both lock
    SLEEP_LOCK_DSP0_CROSS_CORE_END      ,
    SLEEP_LOCK_DSP1_CROSS_CORE_START    ,//for lock DSP1 sleep cross multi core
    SLEEP_LOCK_DSP1_CROSS_CORE_PRELOADER,       //to do dsp1 sleep and dcm both lock
    SLEEP_LOCK_DSP1_CROSS_CORE_MEMORY_ACCESS,   //to do dsp1 sleep and dcm both lock
    SLEEP_LOCK_DSP1_CROSS_CORE_END      ,
    SLEEP_LOCK_N9_CROSS_CORE_START      ,//for lock N9 sleep cross multi core
    SLEEP_LOCK_N9_CROSS_CORE_END        ,
#endif
    SLEEP_LOCK_USER_START_ID            ,
    SLEEP_LOCK_INVALID_ID               = 0xFF
} sleep_management_lock_request_t;

typedef enum {
    SLEEP_BACKUP_RESTORE_FLASH          = 0,
    SLEEP_BACKUP_RESTORE_SPI_MASTER     = 1,
    SLEEP_BACKUP_RESTORE_SPI_SLAVE      = 2,
    SLEEP_BACKUP_RESTORE_DMA            = 3,
    SLEEP_BACKUP_RESTORE_UART           = 4,
    SLEEP_BACKUP_RESTORE_I2C            = 5,
    SLEEP_BACKUP_RESTORE_ANC            = 6,
    SLEEP_BACKUP_RESTORE_MODULE_MAX     = 7,
    SLEEP_BACKUP_RESTORE_USER           = 8
} sleep_management_backup_restore_module_t;

#define SLEEP_BACKUP_RESTORE_USER_CALLBACK_FUNC_MAX 8

#endif /* HAL_SLEEP_MANAGER_ENABLED */
#endif

