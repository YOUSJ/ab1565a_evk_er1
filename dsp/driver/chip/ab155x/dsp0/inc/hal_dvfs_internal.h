/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __HAL_DVFS_INTERNAL_H__
#define __HAL_DVFS_INTERNAL_H__

#include "hal_platform.h"

#ifdef HAL_DVFS_MODULE_ENABLED
#include "hal_ccni.h"
#define DVFS_VOLTAGE_SHIFT 0
#define DVFS_FREQUENCY_SHIFT 2
#define DVFS_LOCK_SHIFT 4
#define DVFS_STATUS_SHIFT 5
#define DVFS_VOL_FREQ_MASK 0x3
#define DVFS_VCORE_MODE_MAX_NUM 4

typedef enum {
    DVFS_52M_SPEED = 0,
    DVFS_156M_SPEED,
    DVFS_312M_SPEED,
    DVFS_ERROR_SPEED,
} dvfs_frequency_t;

typedef enum {
    DVFS_UNLOCK,
    DVFS_LOCK,
} dvfs_lock_parameter_t;

void dvfs_set_register_value(uint32_t address, short int mask, short int shift, short int value);
uint32_t dvfs_get_register_value(uint32_t address, short int mask, short int shift);
hal_dvfs_status_t dvfs_lock_control(uint32_t mode,dvfs_frequency_t freq,dvfs_lock_parameter_t lock);
void dvfs_sent_ccni_to_cm4(uint8_t mode, uint8_t index, dvfs_lock_parameter_t lock);
void dvfs_dps0_receive(hal_ccni_event_t event, void *msg);
uint32_t hal_dvfs_get_cpu_frequency(void);

#endif /* HAL_DVFS_MODULE_ENABLED */

#endif /* __HAL_DVFS_INTERNAL_H__ */
