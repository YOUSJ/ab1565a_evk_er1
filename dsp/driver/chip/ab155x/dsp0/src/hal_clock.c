/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_clock.h" 
#include "hal_clock_internal.h"
#include "hal_nvic.h"
extern ATTR_TEXT_IN_IRAM hal_nvic_status_t hal_nvic_save_and_set_interrupt_mask(uint32_t*);
extern ATTR_TEXT_IN_IRAM hal_nvic_status_t hal_nvic_restore_interrupt_mask(uint32_t);
#ifdef HAL_CLOCK_MODULE_ENABLED
struct _clock_ref_cnt {
    uint32_t cnt;
};

ATTR_ZIDATA_IN_DRAM static struct _clock_ref_cnt dsp_dcm[NR_DSP];

ATTR_TEXT_IN_IRAM void dsp0_dcm_enable_from_dsp0(uint8_t en)
{
    *DSP0_SLOW_CON0 = en;
}

ATTR_TEXT_IN_IRAM void dsp1_dcm_enable_from_dsp0(uint8_t en)
{
    *DSP1_SLOW_CON1 = en;
}

ATTR_TEXT_IN_IRAM hal_clock_status_t clock_dsp_dcm_enable(clock_dsp_num dsp_num)
{
    uint32_t irq_mask = 0;

    if (dsp_num > CLK_DSP1) {
#ifdef CLK_DEBUG
    /* TODO cannot print log before log_hal_info init done */
    log_hal_info("%s: dsp_num = %d\r\n", __FUNCTION__, dsp_num);
#endif /* ifdef CLK_DEBUG */
        return HAL_CLOCK_STATUS_INVALID_PARAMETER;
    }

    hal_nvic_save_and_set_interrupt_mask(&irq_mask);  /* disable interrupt */

    if (dsp_dcm[dsp_num].cnt > 0) {
        dsp_dcm[dsp_num].cnt--;
    }

    if (dsp_dcm[dsp_num].cnt == 0) {
        if (dsp_num == CLK_DSP0)
            dsp0_dcm_enable_from_dsp0(1);
        else
            dsp1_dcm_enable_from_dsp0(1);
    }

    hal_nvic_restore_interrupt_mask(irq_mask);  /* restore interrupt */
    return HAL_CLOCK_STATUS_OK;
}

ATTR_TEXT_IN_IRAM hal_clock_status_t clock_dsp_dcm_disable(clock_dsp_num dsp_num)
{
    uint32_t irq_mask = 0;

    if (dsp_num > CLK_DSP1) {
#ifdef CLK_DEBUG
    /* TODO cannot print log before log_hal_info init done */
    log_hal_info("%s: dsp_num = %d\r\n", __FUNCTION__, dsp_num);
#endif /* ifdef CLK_DEBUG */
        return HAL_CLOCK_STATUS_INVALID_PARAMETER;
    }

    hal_nvic_save_and_set_interrupt_mask(&irq_mask);  /* disable interrupt */

    if (dsp_dcm[dsp_num].cnt == 0) {
        if (dsp_num == CLK_DSP0)
            dsp0_dcm_enable_from_dsp0(0);
        else
            dsp1_dcm_enable_from_dsp0(0);
    }

    if (dsp_dcm[dsp_num].cnt < 32767) {
        dsp_dcm[dsp_num].cnt++;
    }

    hal_nvic_restore_interrupt_mask(irq_mask);  /* restore interrupt */
    return HAL_CLOCK_STATUS_OK;
}

#endif /* HAL_CLOCK_MODULE_ENABLED */

