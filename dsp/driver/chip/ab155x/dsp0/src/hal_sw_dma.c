/* Copyright Statement:
 *
 * (C) 2018  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_sw_dma.h"

#ifdef HAL_SW_DMA_MODULE_ENABLED

#include "stdio.h"
#include "hal_sw_dma_internal.h"
#include "assert.h"
#include "hal_nvic.h"
#include "hal_cache.h"
#ifdef HAL_SLEEP_MANAGER_ENABLED
#include "hal_sleep_manager_internal.h"
#endif

extern void dma_set_interrupt (uint32_t dma_channel);

SW_DMA_BUFFER_INFO sw_dma_buffer[1];

static bool g_sw_dma_init_done = false;
static volatile bool g_sw_dma_is_ongoing = false;
hal_sw_dma_config_info_t  sw_dma_internal_config_info[SW_DMA_USER_NUM];

/*
  [2] for single or 4-beat
  [3] for h_size setting 0, 1, 2
*/
const uint8_t sw_dma_alignment_result[2][3] = {
    {1, 2, 4},
    {4, 8, 16}
};

static hal_sw_gdma_status_t sw_gdma_parameter_check(hal_sw_dma_config_info_t *info)
{
#if 0
    log_hal_msgid_info("source_address: 0x%x", 1, info->source_address);
    log_hal_msgid_info("destination_address: 0x%x", 1, info->destination_address);
    log_hal_msgid_info("length: %d", 1, info->length);
    log_hal_msgid_info("callback func: 0x%x", 1, info->func);
    log_hal_msgid_info("dma_type: %d", 1, info->dma_type);
    log_hal_msgid_info("h_size: %d", 1, info->h_size);
    log_hal_msgid_info("transfer_type: %d", 1, info->transfer_type);
#endif
    if (((info->length) >> (info->h_size)) < DMA_MIN_LENGHT_VALUE ||
            ((info->length) >> (info->h_size)) > DMA_MAX_LENGTH_VALUE) {
        return HAL_SW_DMA_STATUS_INVALID_PARAMETER;
    }

#ifdef HAL_CACHE_MODULE_ENABLED
    /*the address for DMA buffer must be non-cacheable*/
    if (true == hal_cache_is_cacheable(info->destination_address)) {
        assert(0);
        return HAL_SW_DMA_STATUS_INVALID_PARAMETER;
    }

    /*the address for DMA buffer must be non-cacheable, except Flash on DSP side*/
    if ((info->source_address < DSP0_BASE) && (info->source_address >= (DSP0_BASE + DSP0_LENGTH))) {
        if (true == hal_cache_is_cacheable(info->source_address)) {
            assert(0);
            return HAL_SW_DMA_STATUS_INVALID_PARAMETER;
        }
    }
#endif
    uint8_t alignment_result = sw_dma_alignment_result[(info->transfer_type >> 1)][info->h_size];
#if 0
    log_hal_msgid_info("alignment_result:%d", 1, alignment_result);
#endif
    if (alignment_result > 1) {
        uint8_t address_align = info->source_address & (alignment_result - 1);
        uint8_t length_align = info->length & (alignment_result - 1);
#if 0
        log_hal_msgid_info("address_align:%d", 1, address_align);
        log_hal_msgid_info("length_align:%d", 1, length_align);
#endif
        return ((address_align != 0) || (length_align != 0)) ? HAL_SW_DMA_STATUS_INVALID_PARAMETER : HAL_SW_DMA_STATUS_OK;
    } else {
        return HAL_SW_DMA_STATUS_OK;
    }
}

static void sw_dma_common_callback(void)
{
    uint8_t sw_dma_read_pointer = 0x0;
    uint32_t room_available = 0xff;
    hal_sw_dma_callback_t callback;
    hal_sw_dma_config_info_t config_info;
    void *argument;
    uint32_t irq_status;

    hal_nvic_save_and_set_interrupt_mask(&irq_status);
    /* DMA transaction finished */
    sw_dma_read_pointer = BRead(sw_dma_buffer);
    callback = sw_dma_internal_config_info[sw_dma_read_pointer].func;
    argument = sw_dma_internal_config_info[sw_dma_read_pointer].argument;
    Buf_Pop(sw_dma_buffer, 1);
    g_sw_dma_is_ongoing = false;
    hal_nvic_restore_interrupt_mask(irq_status);

    if (callback != NULL) {
        callback(HAL_SW_DMA_EVENT_TRANSACTION_SUCCESS, argument);
    }

    hal_nvic_save_and_set_interrupt_mask(&irq_status);
    /* check if there has data wait for send */
    Buf_GetAvail(sw_dma_buffer, room_available);
    if (room_available >= 1) {
        /* User may start another transaction in the callback, so need check here */
        if (g_sw_dma_is_ongoing == false) {
            sw_dma_read_pointer = BRead(sw_dma_buffer);
            config_info.source_address = sw_dma_internal_config_info[sw_dma_read_pointer].source_address;
            config_info.destination_address = sw_dma_internal_config_info[sw_dma_read_pointer].destination_address;
            config_info.length = sw_dma_internal_config_info[sw_dma_read_pointer].length;
            config_info.dma_type = sw_dma_internal_config_info[sw_dma_read_pointer].dma_type;
            config_info.h_size = sw_dma_internal_config_info[sw_dma_read_pointer].h_size;
            config_info.transfer_type = sw_dma_internal_config_info[sw_dma_read_pointer].transfer_type;
            sw_dma_start_interrupt(&config_info);
            g_sw_dma_is_ongoing = true;
        }
    }
    hal_nvic_restore_interrupt_mask(irq_status);
}

#ifdef HAL_SLEEP_MANAGER_ENABLED
static bool g_sw_dma_register_sleep_handler_done = false;

static void sw_gdma_restore_all_registers(void)
{
    g_sw_dma_init_done = false;
}
#endif

hal_sw_gdma_status_t hal_sw_gdma_start(hal_sw_dma_config_info_t *info)
{
    uint8_t  room_left = 0xff;
    uint8_t  sw_dma_read_pointer = 0x0;
    uint32_t irq_status;
    hal_sw_gdma_status_t status = HAL_SW_DMA_STATUS_OK;
    hal_sw_dma_config_info_t config_info;

    hal_nvic_save_and_set_interrupt_mask(&irq_status);

#ifdef HAL_SLEEP_MANAGER_ENABLED
    if (g_sw_dma_register_sleep_handler_done == false) {
        sleep_management_register_resume_callback(SLEEP_BACKUP_RESTORE_DMA, (sleep_management_resume_callback_t)sw_gdma_restore_all_registers, NULL);
        g_sw_dma_register_sleep_handler_done = true;
    }
#endif

    /* init SW DMA buffer and HW DMA only once */
    if (g_sw_dma_init_done == false) {
        sw_dma_buffer->Read = 0x0;
        sw_dma_buffer->Write = 0x0;
        sw_dma_buffer->Length = SW_DMA_USER_NUM;
        g_sw_dma_init_done = true;

        sw_dma_init(SW_DMA_CHANNEL);
        sw_dma_register_callback((sw_dma_callback_t)sw_dma_common_callback);
        /*register irq handler*/
        dma_set_interrupt(SW_DMA_CHANNEL);
    }

    /* check the buffer left room space*/
    Buf_GetRoomLeft(sw_dma_buffer, room_left);
    if (room_left >= 1)  {
        status = sw_gdma_parameter_check(info);
        if (status != HAL_SW_DMA_STATUS_OK) {
            hal_nvic_restore_interrupt_mask(irq_status);
            return status;
        }
        sw_dma_internal_config_info[BWrite(sw_dma_buffer)].source_address = info->source_address;
        sw_dma_internal_config_info[BWrite(sw_dma_buffer)].destination_address = info->destination_address;
        sw_dma_internal_config_info[BWrite(sw_dma_buffer)].length = info->length;
        sw_dma_internal_config_info[BWrite(sw_dma_buffer)].func = info->func;
        sw_dma_internal_config_info[BWrite(sw_dma_buffer)].argument = info->argument;
        sw_dma_internal_config_info[BWrite(sw_dma_buffer)].dma_type = info->dma_type;
        sw_dma_internal_config_info[BWrite(sw_dma_buffer)].h_size = info->h_size;
        sw_dma_internal_config_info[BWrite(sw_dma_buffer)].transfer_type = info->transfer_type;
        Buf_Push(sw_dma_buffer);
    } else {
        hal_nvic_restore_interrupt_mask(irq_status);
        return HAL_SW_DMA_STATUS_BUFFER_FULL;
    }

    if (g_sw_dma_is_ongoing == false) {
        sw_dma_read_pointer = BRead(sw_dma_buffer);
        config_info.source_address = sw_dma_internal_config_info[sw_dma_read_pointer].source_address;
        config_info.destination_address = sw_dma_internal_config_info[sw_dma_read_pointer].destination_address;
        config_info.length = sw_dma_internal_config_info[sw_dma_read_pointer].length;
        config_info.dma_type = sw_dma_internal_config_info[sw_dma_read_pointer].dma_type;
        config_info.h_size = sw_dma_internal_config_info[sw_dma_read_pointer].h_size;
        config_info.transfer_type = sw_dma_internal_config_info[sw_dma_read_pointer].transfer_type;
        status = sw_dma_start_interrupt(&config_info);
        g_sw_dma_is_ongoing = true;
    }

    hal_nvic_restore_interrupt_mask(irq_status);

    return status;
}

#endif /*HAL_SW_DMA_MODULE_ENABLED*/

