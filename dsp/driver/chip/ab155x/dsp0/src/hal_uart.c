/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_uart.h"
#ifdef HAL_UART_MODULE_ENABLED
#ifdef HAL_SLEEP_MANAGER_ENABLED
#include "hal_spm.h"
#include "hal_sleep_manager.h"
#include "hal_sleep_manager_internal.h"
#endif
#include "hal_resource_assignment.h"
#include "hal_pdma_internal.h"

#ifdef __cplusplus
extern "C" {
#endif


static UART_REGISTER_T *const g_uart_regbase[] = {UART0, UART1, UART2};

uint32_t uart_send_polling(hal_uart_port_t uart_port, const uint8_t *data, uint32_t size);
static VDMA_REGISTER_T * const vdma[VDMA_NUMBER] = {VDMA6, VDMA7, VDMA8, VDMA9, VDMA10, VDMA11, VDMA12, VDMA13,
                                                    VDMA14, VDMA15, VDMA16, VDMA17, VDMA18, VDMA19
                                                   };


vdma_channel_t  uart_port_to_dma_map[2][3] =
{
    {VDMA_UART0TX, VDMA_UART1TX, VDMA_UART2TX},
    {VDMA_UART0RX, VDMA_UART1RX, VDMA_UART2RX},
};

#define     uart_port_to_dma_channel(uart_port,is_rx)   (uart_port_to_dma_map[is_rx][uart_port])


#ifdef HAL_SLEEP_MANAGER_ENABLED
static const uint32_t g_uart_dma_base[HAL_UART_MAX]   = {0xA0020610, 0xA0020410, 0xA0020210};
static const uint8_t  g_uart_dma_tx_bit[HAL_UART_MAX] = {0x5, 0x3, 0x1};
static const uint32_t g_uart_pdn_value[HAL_UART_MAX]  = {0x100000, 0x800, 0x1000};
void uart_backup_all_registers(void)
{
    hal_uart_port_t uart_port;
    hal_uart_port_t g_uart_port_for_logging = HAL_UART_MAX;
    UART_REGISTER_T *uartx;
    uint32_t data_length = 0;
    uint32_t *share_buffer;
    int core_state_cm4,core_state_dsp1;


    share_buffer = (uint32_t *)HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_START;
    g_uart_port_for_logging = (hal_uart_port_t)(*share_buffer++);
    uart_port = g_uart_port_for_logging;
    if(uart_port >= HAL_UART_MAX)
        return;
    uartx = g_uart_regbase[uart_port];
    while (hal_hw_semaphore_take(HW_SEMAPHORE_SLEEP) != HAL_HW_SEMAPHORE_STATUS_OK);
    core_state_cm4 = hal_core_status_read(HAL_CORE_CM4);
    core_state_dsp1 = hal_core_status_read(HAL_CORE_DSP0);
    if(((core_state_cm4 == HAL_CORE_OFF)||(core_state_cm4 == HAL_CORE_SLEEP))&&(((core_state_dsp1 == HAL_CORE_OFF)||(core_state_dsp1 == HAL_CORE_SLEEP)))) {
        data_length = *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x28); //VDMA_FFCNT
        if (data_length <= 10) {
            while (!((uartx->LSR) & UART_LSR_TEMT_MASK));
            SPM_CLEAR_LOCK_INFRA;
            //hal_clock_disable(g_uart_port_to_pdn[uart_port]);
            //hal_clock_disable(HAL_CLOCK_CG_CM4_DMA);
            //*(volatile uint32_t *)(0xA2270310) = g_uart_pdn_value[uart_port];
            //*(volatile uint32_t *)(0xA2270340) = 0x400; // VDMA_PDN
            //*(volatile uint32_t *)(0xA0020078) = (0x1 << g_uart_dma_tx_bit[uart_port]);
        } else {
            SPM_SET_LOCK_INFRA;
            // Set related VFIFO TX channel threshold as 0x1
            *(volatile uint32_t *)(g_uart_dma_base[uart_port]) = 0x1;
            //global top interrupt clr
            *(volatile uint32_t *)(0xA0020010) = (0x1 << g_uart_dma_tx_bit[uart_port]);
            //ACK INT
            *(volatile uint32_t *)(g_uart_dma_base[uart_port] + 0x10) = 0x8000;
            //global top interrupt set
            *(volatile uint32_t *)(0xA002000C) = (0x1 << g_uart_dma_tx_bit[uart_port]);
            // Enable related VFIFO TX channel interrupt, ITEN = 1
            *(volatile uint8_t *)(g_uart_dma_base[uart_port] + 0x7) = 0x1;
        }
    }
    while (hal_hw_semaphore_give(HW_SEMAPHORE_SLEEP) != HAL_HW_SEMAPHORE_STATUS_OK);
}

extern void log_sleep_restore_callback(void);


void uart_restore_all_registers(void)
{
    uint32_t delay;
    hal_uart_port_t uart_port;
    hal_uart_port_t g_uart_port_for_logging = HAL_UART_MAX;
    UART_REGISTER_T *uartx;
    uint32_t *share_buffer;
    uint32_t *share_buffer_end;
    uint32_t u32temp = 0;

    share_buffer = (uint32_t *)HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_START;
    share_buffer_end = (uint32_t *)(HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_START + HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_LEN - 4);
    g_uart_port_for_logging = (hal_uart_port_t)(*share_buffer++);
    uart_port = g_uart_port_for_logging;
    if(uart_port >= HAL_UART_MAX)
        return;
    while (hal_hw_semaphore_take(HW_SEMAPHORE_SLEEP) != HAL_HW_SEMAPHORE_STATUS_OK);
    if(SPM_INFRA_OFF_FLAG != 0) {

        SPM_INFRA_OFF_FLAG = 0;
        uartx = g_uart_regbase[uart_port];
        *(volatile uint32_t *)(0xA0020074) = (0x1 << g_uart_dma_tx_bit[uart_port]); // VDMA_CLOCK

        uartx->DLM_DLL = *share_buffer++;
        uartx->FCR_UNION.FCR = *share_buffer++;
        uartx->LCR_UNION.LCR = *share_buffer++;
        uartx->HIGHSPEED = *share_buffer++;
        uartx->SAMPLE_REG_UNION.SAMPLE_REG = *share_buffer++;
        uartx->RATEFIX_UNION.RATEFIX = *share_buffer++;
        uartx->GUARD = *share_buffer++;
        uartx->SLEEP_REG = *share_buffer++;
        uartx->FRACDIV = *share_buffer++;
        u32temp = *share_buffer++;

        /* Because of hardware limitation, we have to send XON manually
        * when software flow control is turn on for that port.
        */
        uartx->EFR_UNION.EFR = 0;
        uartx->XON_XOFF_UNION.XON_XOFF = (0x11 << UART_XON_XOFF_XONCHAR_OFFSET) |
                                         (0x13 << UART_XON_XOFF_XOFFCHAR_OFFSET);
        uartx->EFR_UNION.EFR_CELLS.SEND_XON = 1;
        //uart_send_polling(uart_port, (const uint8_t *)&xon_char, 1);
        /* must delay until xon character is sent out, to avoid xon error with escape */
        delay = ((1000000 * 12) / (*share_buffer_end)) + 1;
        hal_gpt_delay_us(delay);
        uartx->EFR_UNION.EFR_CELLS.SEND_XON = 0;

        uartx->ESCAPE_REG_UNION.ESCAPE_REG = (0x1 << UART_ESCAPE_REG_EN_OFFSET) |
                                                (0x77 << UART_ESCAPE_REG_CHAR_OFFSET);
        uartx->EFR_UNION.EFR_CELLS.SW_FLOW_CONT = (UART_EFR_SW_TX_FLOWCTRL_MASK |
                UART_EFR_SW_RX_FLOWCTRL_MASK);

        log_sleep_restore_callback();

        // Disable related VFIFO TX channel interrupt, ITEN = 0
        *(volatile uint8_t *)(g_uart_dma_base[uart_port] + 0x7) = 0x0;
        //global top interrupt clr
        *(volatile uint32_t *)(0xA0020010) = (0x1 << g_uart_dma_tx_bit[uart_port]);
        //ACK INT
        *(volatile uint32_t *)(g_uart_dma_base[uart_port] + 0x10) = 0x8000;

        //hal_nvic_clear_pending_irq(MCU_DMA_IRQn);
        *(volatile uint32_t *)(g_uart_dma_base[uart_port])     = *share_buffer++; //VDMA_COUNT
        *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x4) = *share_buffer++; //VDMA_CON
        *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x1C)= *share_buffer++; //VDMA_PGMADDR
        *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x30)= *share_buffer++; //VDMA_ALTLEN
        *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x34)= *share_buffer++; //VDMA_FFSIZE
        *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x8) = 0x8000; //VDMA_START
        uartx->FCR_UNION.FCR |=  UART_FCR_CLRT_MASK | UART_FCR_CLRR_MASK;
        uartx->DMA_CON_UNION.DMA_CON = u32temp;
    } else {
        // Disable related VFIFO TX channel interrupt, ITEN = 0
        *(volatile uint8_t *)(g_uart_dma_base[uart_port] + 0x7) = 0x0;
        //global top interrupt clr
        *(volatile uint32_t *)(0xA0020010) = (0x1 << g_uart_dma_tx_bit[uart_port]);
        //ACK INT
        *(volatile uint32_t *)(g_uart_dma_base[uart_port] + 0x10) = 0x8000;
        //hal_nvic_clear_pending_irq(MCU_DMA_IRQn);
    }
    while (hal_hw_semaphore_give(HW_SEMAPHORE_SLEEP) != HAL_HW_SEMAPHORE_STATUS_OK);
}
#endif




uint32_t uart_get_hw_rptr(hal_uart_port_t uart_port)
{
    vdma_channel_t dma_channel;
    uint32_t read_pointer, *share_buffer;
    uint32_t offset = 0;

    if(uart_port > HAL_UART_MAX)
        return 0;

    share_buffer = (uint32_t *)HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_START;
    dma_channel  = uart_port_to_dma_channel(uart_port, 0);
    //vdma_get_hw_read_point(dma_channel, &read_pointer);
    offset       = dma_channel - VDMA_START_CHANNEL;
    read_pointer = vdma[offset]->VDMA_RDPTR;

    return (read_pointer - share_buffer[13]);
}

hal_uart_status_t uart_set_sw_move_byte(hal_uart_port_t uart_port, uint16_t sw_move_byte)
{
    vdma_channel_t  dma_channel;
    uint32_t        offset = 0;
    uint8_t         h_size = 0;

    if(uart_port > HAL_UART_MAX)
        return 0;

    dma_channel = uart_port_to_dma_channel(uart_port, 0);
    //status = vdma_set_sw_move_byte(dma_channel, sw_move_byte);
    offset = dma_channel - VDMA_START_CHANNEL;
    h_size = vdma[offset]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE;
    if(h_size == 0x2 && (sw_move_byte & 0x3) != 0x0)
    {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }
    vdma[offset]->VDMA_SW_MV_BYTE = (sw_move_byte >> h_size);
    return HAL_UART_STATUS_OK;
}




uint32_t uart_get_hw_wptr(hal_uart_port_t uart_port)
{
    vdma_channel_t dma_channel;
    uint32_t write_pointer, *share_buffer;
    uint32_t        offset = 0;

    if(uart_port > HAL_UART_MAX)
        return 0;

    share_buffer  = (uint32_t *)HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_START;
    dma_channel   = uart_port_to_dma_channel(uart_port, 0);
    //vdma_get_hw_write_point(dma_channel, &write_pointer);
    offset        = dma_channel - VDMA_START_CHANNEL;
    write_pointer = vdma[offset]->VDMA_WRPTR;

    return (write_pointer - share_buffer[13]);
}

uint32_t uart_send_polling(hal_uart_port_t uart_port, const uint8_t *data, uint32_t size)
{
    uint32_t i = 0;
    uint32_t LSR;
    UART_REGISTER_T *uartx;
    uartx = g_uart_regbase[uart_port];

    for (i = 0; i < size; i++) {
        while (1) {
            LSR = uartx->LSR;
            if (LSR & UART_LSR_THRE_MASK) {
                uartx->THR = *data;
                break;
            }
        }
        data++;
    }
    return size;
}

#ifdef __cplusplus
}
#endif

#endif

