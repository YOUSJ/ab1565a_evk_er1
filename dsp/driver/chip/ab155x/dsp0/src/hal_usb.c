/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#include "hal_usb.h"
#include "hal_usb_internal.h"
#include "hal_eint.h"
#include "hal_gpt.h"
#include "hal_nvic.h"
#include "hal_resource_assignment.h"

#if 1//def HAL_USB_MODULE_ENABLED
static volatile USB_REGISTER_T *musb = (USB_REGISTER_T *)USB_BASE;

/* Exception flag*/
static volatile USB_Drv_Info *g_UsbDrvInfo = (volatile USB_Drv_Info*)(HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_USB_VAR_START);

static uint8_t usb_get_dma_channel_num(uint8_t ep_num, hal_usb_endpoint_direction_t direction);


__attribute__((optimize ("-O0"))) static uint8_t usb_get_dma_channel_num(uint8_t ep_num, hal_usb_endpoint_direction_t direction)
{
    uint8_t  dma_chan;
    dma_chan = g_UsbDrvInfo->dma_port[direction][ep_num - 1];

    if ((dma_chan == 0) || (dma_chan > HAL_USB_MAX_NUMBER_DMA) || (ep_num == 0)) {
        //log_hal_error("ASSERT\r\n");
    }

    return dma_chan;
}

__attribute__((optimize ("-O0"))) hal_usb_status_t hal_usb_start_dma_channel(uint32_t ep_num, hal_usb_endpoint_direction_t direction, hal_usb_endpoint_transfer_type_t ep_type, void *addr, uint32_t length,
        hal_usb_dma_handler_t callback, bool callback_upd_run, hal_usb_dma_type_t dma_type)
{
    uint16_t dma_ctrl;
    uint8_t dma_chan;
    static uint32_t dma_burst_mode = 0;
    uint32_t savedMask;
    bool known_size = true;
    uint32_t temp_reg;

    if (length == 0) {
        //log_hal_error("ASSERT\r\n");
    }

    if (g_UsbDrvInfo->usb_disconnect == true) {
        return HAL_USB_STATUS_OK;
    }

    dma_chan = usb_get_dma_channel_num(ep_num, direction);
    if (g_UsbDrvInfo->dma_running[dma_chan - 1] != false) {
        //log_hal_error("ASSERT\r\n");
    }

    if (g_UsbDrvInfo->is_bidirection_dma[dma_chan - 1] == true) {
        g_UsbDrvInfo->dma_dir[dma_chan - 1] = direction;
    } else if (g_UsbDrvInfo->dma_dir[dma_chan - 1] != direction) {
        //log_hal_error("ASSERT\r\n");
    }

    g_UsbDrvInfo->dma_running[dma_chan - 1] = true;
    //g_UsbDrvInfo->dma_callback[dma_chan - 1] = callback;
    if (callback == NULL) {
        /*do nothing.*/
    }
    g_UsbDrvInfo->dma_callback_upd_run[dma_chan - 1] = callback_upd_run;
    g_UsbDrvInfo->dma_pktrdy[dma_chan - 1] = false;

    if (direction == HAL_USB_EP_DIRECTION_RX) {
        if (ep_type == HAL_USB_EP_TRANSFER_BULK) {
            if (g_UsbDrvInfo->Is_HS_mode == true) {
                if (((length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_BULK_HIGH_SPEED) != 0)
                        && ((length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_BULK_HIGH_SPEED) <= (HAL_USB_MAX_PACKET_SIZE_ENDPOINT_BULK_HIGH_SPEED - 4))) {
                    g_UsbDrvInfo->dma_pktrdy[dma_chan - 1] = true;
                }
            } else {
                if (((length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_BULK_FULL_SPEED) != 0)
                        && ((length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_BULK_FULL_SPEED) <= (HAL_USB_MAX_PACKET_SIZE_ENDPOINT_BULK_FULL_SPEED - 4))) {
                    g_UsbDrvInfo->dma_pktrdy[dma_chan - 1] = true;
                }
            }
        } else if (ep_type == HAL_USB_EP_TRANSFER_ISO) {
            if (g_UsbDrvInfo->Is_HS_mode == true) {
                if (((length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_HIGH_SPEED) != 0)
                        && ((length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_HIGH_SPEED) <= (HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_HIGH_SPEED - 4))) {
                    g_UsbDrvInfo->dma_pktrdy[dma_chan - 1] = true;
                }
            } else {
                if (((length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_FULL_SPEED) != 0)
                        && ((length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_FULL_SPEED) <= (HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_FULL_SPEED - 4))) {
                    g_UsbDrvInfo->dma_pktrdy[dma_chan - 1] = true;
                }
            }
        }
    } else {
        if (ep_type == HAL_USB_EP_TRANSFER_BULK) {
            if (g_UsbDrvInfo->Is_HS_mode == true) {
                if (length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_BULK_HIGH_SPEED) {
                    g_UsbDrvInfo->dma_pktrdy[dma_chan - 1] = true;
                }
            } else {
                if (length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_BULK_FULL_SPEED) {
                    g_UsbDrvInfo->dma_pktrdy[dma_chan - 1] = true;
                }
            }
        } else if (ep_type == HAL_USB_EP_TRANSFER_INTR) {
            if (g_UsbDrvInfo->Is_HS_mode == true) {
                if (length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_INTERRUPT_HIGH_SPEED) {
                    g_UsbDrvInfo->dma_pktrdy[dma_chan - 1] = true;
                }
            } else {
                if (length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_INTERRUPT_FULL_SPEED) {
                    g_UsbDrvInfo->dma_pktrdy[dma_chan - 1] = true;
                }
            }
        } else if (ep_type == HAL_USB_EP_TRANSFER_ISO) {
            if (g_UsbDrvInfo->Is_HS_mode == true) {
                if (length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_HIGH_SPEED) {
                    g_UsbDrvInfo->dma_pktrdy[dma_chan - 1] = true;
                }
            } else {
                if (length % HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_FULL_SPEED) {
                    g_UsbDrvInfo->dma_pktrdy[dma_chan - 1] = true;
                }
            }
        }
    }

    if (g_UsbDrvInfo->b_enable_dma_burst_auto_chge == true) {
        dma_burst_mode++;
    } else {
        dma_burst_mode = g_UsbDrvInfo->dma_burst_mode;
    }

    /* DMA_CONFIG */
    if (dma_type == HAL_USB_DMA0_TYPE) {

    } else if (dma_type == HAL_USB_DMA1_TYPE) {
        if (direction == HAL_USB_EP_DIRECTION_TX) {
            hal_nvic_save_and_set_interrupt_mask(&savedMask);
//            USB_DRV_WriteReg8(&musb->index, ep_num);
            temp_reg = USB_DRV_Reg32(&musb->frame);
            temp_reg = ((temp_reg & 0xFF00FFFF) | (ep_num << 16));
            USB_DRV_WriteReg32(&musb->frame,temp_reg);

            /* Only is configured as multiple packet DMA TX mode */
            if (ep_type == HAL_USB_EP_TRANSFER_ISO) {
                USB_DRV_WriteReg(&musb->txcsr, USB_DMA_TX_CSR_ISO);
            } else {
//                USB_DRV_WriteReg(&musb->txcsr, USB_DMA_TX_CSR);
                temp_reg = USB_DRV_Reg32(&musb->txmap);
                temp_reg = ((temp_reg & 0xFFFF) | ((USB_DMA_TX_CSR) << 16));
                USB_DRV_WriteReg32(&musb->txmap,temp_reg);
            }

            hal_nvic_restore_interrupt_mask(savedMask);
            g_UsbDrvInfo->dma_tx_length[dma_chan - 1] = length;
            //log_hal_info("USB DMA Setup: leng: %d addr:0x%x, first byte: 0x%X\n", length, addr);
            USB_DRV_WriteReg32(USB_DMAADDR(dma_chan), addr);
            USB_DRV_WriteReg32(USB_DMACNT(dma_chan), length);
            //log_hal_info("USB DMA Setup: read back DMAADDR: 0x%x = 0x%x, DMACNT: 0x%x = 0x%x\n",
            //												USB_DMAADDR(dma_chan), USB_DRV_Reg32(USB_DMAADDR(dma_chan)),
            //												USB_DMACNT(dma_chan), USB_DRV_Reg32(USB_DMACNT(dma_chan)));

            dma_ctrl = USB_DMACNTL_DMADIR | USB_DMACNTL_DMAMODE | USB_DMACNTL_INTEN | (ep_num << 4);
            dma_ctrl |= ((dma_burst_mode & 0x03) << 9) | USB_DMACNTL_DMAEN;

            USB_DRV_WriteReg32(USB_DMACNTL(dma_chan), dma_ctrl);
        } else if (direction == HAL_USB_EP_DIRECTION_RX) {
            /* Stop DMA channel */
            USBDMA_Stop(dma_chan);

            if (known_size == true) {
                hal_nvic_save_and_set_interrupt_mask(&savedMask);
                USB_DRV_WriteReg8(&musb->index, ep_num);

                if (ep_type == HAL_USB_EP_TRANSFER_ISO) {
                    USB_DRV_WriteReg(&musb->rxcsr, USB_DMA_RX_CSR_ISO | USB_RXCSR_RXPKTRDY);
                } else {
                    USB_DRV_WriteReg(&musb->rxcsr, USB_DMA_RX_CSR | USB_RXCSR_RXPKTRDY);
                }

                hal_nvic_restore_interrupt_mask(savedMask);
            } else {
#ifdef  __DMA_UNKNOWN_RX__
                hal_nvic_save_and_set_interrupt_mask(&savedMask);
                USB_DRV_WriteReg8(&musb->index, ep_num);

                if (ep_type == HAL_USB_EP_TRANSFER_ISO) {
                    USB_DRV_WriteReg(&musb->rxcsr, USB_DMA_RX_CSR_ISO | USB_RXCSR_RXPKTRDY | USB_RXCSR_DMAREQMODE);
                } else {
                    USB_DRV_WriteReg(&musb->rxcsr, USB_DMA_RX_CSR | USB_RXCSR_RXPKTRDY | USB_RXCSR_DMAREQMODE);
                }

                hal_nvic_restore_interrupt_mask(savedMask);

                usb_enable_dma_timer_count(dma_chan, true, 0x7F);

#else   /* __DMA_UNKNOWN_RX__ */
                //log_hal_error("ASSERT\r\n");
#endif  /* __DMA_UNKNOWN_RX__ */
            }

            USB_DRV_WriteReg32(USB_DMAADDR(dma_chan), addr);
            USB_DRV_WriteReg32(USB_DMACNT(dma_chan), length);

            dma_ctrl = USB_DMACNTL_DMAMODE | USB_DMACNTL_INTEN | (ep_num << 4);
            dma_ctrl |= ((dma_burst_mode & 0x03) << 9) | USB_DMACNTL_DMAEN;
            USB_DRV_WriteReg32(USB_DMACNTL(dma_chan), dma_ctrl);
        }
    } else {
        //log_hal_error("ASSERT\r\n");
    }
    return HAL_USB_STATUS_OK;
}


__attribute__((optimize ("-O0"))) bool hal_usb_is_dma_running(uint32_t ep_num, hal_usb_endpoint_direction_t direction)
{
    bool result;
    uint8_t   dma_chan;

    dma_chan = usb_get_dma_channel_num(ep_num, direction);
    result = g_UsbDrvInfo->dma_running[dma_chan - 1];

    return result;;
}


__attribute__((optimize ("-O0"))) hal_usb_status_t hal_usb_init(void)
{

//    hal_usb_drv_create_isr();
//	hal_nvic_register_isr_handler(USB_IRQn,the_test_usb_isr);
//	hal_nvic_register_isr_handler(USB_IRQn,usb_hisr);
//	hal_nvic_enable_irq(USB_IRQn);
//    ((*(volatile uint32_t *)(0xA102076C)) = (uint32_t)(0x06));
//    ((*(volatile uint32_t *)(0xA1020774)) = (uint32_t)(0x09));  // Turn on DMA and TX interrupt for DSP
//    ((*(volatile uint32_t *)(0xA10200A4)) = (uint32_t)(0x76));  // Turn off DMA interrupt for CM4
    
    return HAL_USB_STATUS_OK;
}


#endif /*HAL_USB_MODULE_ENABLED*/
