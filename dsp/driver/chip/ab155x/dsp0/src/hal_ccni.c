/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#include "hal_ccni.h"

#ifdef HAL_CCNI_MODULE_ENABLED
#include "hal_resource_assignment.h"
#include "hal_ccni_internal.h"
#include "memory_attribute.h"
#include "hal_nvic.h"
#include "hal_log.h"

#ifdef __cplusplus
extern "C" {
#endif

//#define CCNI_DEBUG

#define IRQ_MASK_OFFSET       (0x0)
#define IRQ_SET_OFFSET        (0x4)
#define IRQ_CLR_OFFSET        (0x8)
#define IRQ_STATUS_OFFSET     (0xc)
#define CCNI_MAX_EVENT        (0x20)
#define CCNI_EVENT_MASK_EN    (0x1)
#define CCNI_EVENT_UNMASK_EN  (0x0)
#define CCNI_EVENT_CLEAR_EN   (0x1)

#define CCNI_MAX_MSG          (0x8)
#define CCNI_INVALID_EVENT    (0xFFFFFFFF)

const uint32_t dsp0_send_message_memory_pool[4] = {
    CCNI_MSG_BASE + CCNI_CM4_TO_N9_MSG_SIZE,    //DSP0 to CM4
    0,
    CCNI_MSG_BASE + CCNI_DSP0_TO_CM4_MSG_SIZE,  //DSP0 to DSP1
    CCNI_MSG_BASE + CCNI_DSP0_TO_DSP1_MSG_SIZE  //DSP0 to N9
};

const uint32_t dsp0_receive_message_memory_pool[4] = {
    CCNI_MSG_BASE,                               //CM4 to DSP0
    0,
    CCNI_MSG_BASE + CCNI_DSP1_TO_CM4_MSG_SIZE,   //DSP1 to DSP0
    CCNI_MSG_BASE + CCNI_N9_TO_CM4_MSG_SIZE      //N9 to DSP0
};

const uint32_t base_addr[4][4] = {
    {0,              CM4_CCNI_BASE,        CM4_CCNI_BASE + 0x10, CM4_CCNI_BASE + 0x20 },
    {DSP0_CCNI_BASE, 0,                    DSP0_CCNI_BASE+ 0x10, DSP0_CCNI_BASE + 0x20},
    {DSP1_CCNI_BASE, DSP1_CCNI_BASE+0x10,  0,                    DSP1_CCNI_BASE + 0x20},
    {N9_CCNI_BASE,   N9_CCNI_BASE + 0x10,  N9_CCNI_BASE + 0x20,  0,                   },
};

const hal_ccni_function_t* ccni_xxx_to_dsp0_function_table[4]={
    ccni_cm4_to_dsp0_function_table,
    NULL,
    ccni_dsp1_to_dsp0_function_table,
    ccni_n9_to_dsp0_function_table,
};

const uint32_t dsp0_receive_xxx_event_status_table[4] = {
    CM4_CCNI_BASE  + 0x0C, 
    0,
    DSP1_CCNI_BASE + 0x1C,
    N9_CCNI_BASE   + 0x1C,
};

const IRQn_Type dsp0_ccni_irq_to_index_table[3] = {0,2,3};
//const IRQn_Type dsp1_ccni_irq_to_index_table[3] = {0,1,3};
//const IRQn_Type CM4_ccni_irq_to_index_table[3] = {1,2,3}

const uint32_t exception_event_table[4] = {IRQGEN_CM4_TO_DSP0_EXCEPTION,CCNI_INVALID_EVENT,IRQGEN_DSP1_TO_DSP0_EXCEPTION, CCNI_INVALID_EVENT};

static void ccni_irq_handler(hal_nvic_irq_t irq);
extern void exception_handler(hal_ccni_event_t event, void * msg);

hal_ccni_status_t hal_ccni_init(void)
{
    hal_ccni_status_t status;
    hal_nvic_register_isr_handler(CM4_IRQn,ccni_irq_handler);
    hal_nvic_register_isr_handler(DSP1_IRQn,ccni_irq_handler);
    hal_nvic_register_isr_handler(N9_IRQn,ccni_irq_handler);
    hal_nvic_enable_irq(CM4_IRQn);
    hal_nvic_enable_irq(DSP1_IRQn);
    hal_nvic_enable_irq(N9_IRQn);
    return (status = HAL_CCNI_STATUS_OK);
}

static void ccni_irq_handler(hal_nvic_irq_t irq)
{
    uint32_t i;
    uint32_t event;
    uint32_t msg_data[2];
    uint32_t *p_share_memory,src_index;
    const hal_ccni_function_t *function_table;
    src_index = dsp0_ccni_irq_to_index_table[irq-CM4_IRQn];

    for (i = 0; i < CCNI_MAX_EVENT; i++) {
        uint32_t irq_event =  *((volatile uint32_t*)(dsp0_receive_xxx_event_status_table[src_index]));
        function_table = ccni_xxx_to_dsp0_function_table[src_index];
        event = exception_event_table[src_index];

        if (event!=CCNI_INVALID_EVENT) {
            if (irq_event & (0x1<< (event&CCNI_EVENT_MASK))) {
                //handle exception at first
                hal_ccni_mask_event(event);
                function_table[event & CCNI_EVENT_MASK].hal_ccni_callback(event, NULL);
                //Exception can't reach here
            }
        }

        if (irq_event & (1 << i)) {
            uint32_t sr_addr;
            event = i | ((src_index+1)<<CCNI_EVENT_SRC_OFFSET) | CCNI_EVENT_DST_DSP0;
            sr_addr = base_addr[src_index][((event&CCNI_DST_MASK) >>CCNI_EVENT_DST_OFFSET) -1] + IRQ_MASK_OFFSET;
            if(((*(volatile uint32_t*)sr_addr)&(CCNI_REG_ONE_BIT_STATUS<<(event&CCNI_EVENT_MASK)))){
                //do nothing if event is marked
                #ifdef CCNI_DEBUG
                log_hal_msgid_info("[DSP0]:event =0x%x is masked !!!\r\n", 1, event);
                #endif
                continue;
            }

            hal_ccni_mask_event(event);
            if (function_table[i].hal_ccni_callback) {
                if (i < CCNI_MAX_MSG) {
                    p_share_memory = (uint32_t*)dsp0_receive_message_memory_pool[src_index];
                    msg_data[0] = p_share_memory[2*(event & CCNI_EVENT_MASK)];
                    msg_data[1] = p_share_memory[2*(event & CCNI_EVENT_MASK)+1];
                    #ifdef CCNI_DEBUG
                    log_hal_msgid_info("[DSP0 Receive]:event=0x%x, msg_0=0x%x, msg_1=0x%x \r\n",3, event,msg_data[0], msg_data[1]);
                    #endif
                    function_table[i].hal_ccni_callback(event, msg_data);
                    #ifdef CCNI_DEBUG
                    log_hal_msgid_info("[DSP0 Receive]: callback is exit\r\n", 0);
                    #endif
                } else {
                    #ifdef CCNI_DEBUG
                    log_hal_msgid_info("[DSP0 Receive]:event=0x%x \r\n", 1, irq_event);
                    #endif
                    function_table[i].hal_ccni_callback(event, NULL);
                }
            }
        }
    }
}

//Query event status
hal_ccni_status_t hal_ccni_query_event_status(hal_ccni_event_t event, uint32_t *data)
{
    hal_ccni_status_t status = HAL_CCNI_STATUS_OK;
    uint32_t mask;
    uint32_t src_index, dst_index, sr_addr;

    if (data == NULL) {
        return (status = HAL_CCNI_STATUS_INVALID_PARAMETER);
    }
    //Not query self to self event, and event is less than max defined event
    if (((event&CCNI_SRC_MASK) == ((event&CCNI_DST_MASK)<<8)) || (((event&CCNI_EVENT_MASK)>=CCNI_MAX_EVENT))) {
         return (status = HAL_CCNI_STATUS_INVALID_PARAMETER);
    }

    src_index = ((event&CCNI_SRC_MASK) >>CCNI_EVENT_SRC_OFFSET) -1;
    dst_index = ((event&CCNI_DST_MASK) >>CCNI_EVENT_DST_OFFSET) -1;

    hal_nvic_save_and_set_interrupt_mask(&mask);
    sr_addr = base_addr[src_index][dst_index] + IRQ_STATUS_OFFSET;
    *data = ((*(volatile uint32_t*)sr_addr)&(CCNI_REG_ONE_BIT_STATUS<<(event&CCNI_EVENT_MASK))) ? HAL_CCNI_EVENT_STATUS_BUSY : HAL_CCNI_EVENT_STATUS_IDLE;
    hal_nvic_restore_interrupt_mask(mask);
    return status;
}

/**
 * @brief This function trigger an an intterupt of the destination processer .
 * @param[in] dcore is the interrupt receiver.
 * @param[in] event is the intterrupt name
 * @param[in] message is optional
 */
hal_ccni_status_t hal_ccni_set_event(hal_ccni_event_t event, hal_ccni_message_t *message)
{
    hal_ccni_status_t status = HAL_CCNI_STATUS_OK;
    uint32_t mask;
    uint32_t src_index, dst_index, data, setr_addr;
    uint32_t* p_share_memory;
    uint32_t event_index;
    src_index = ((event&CCNI_SRC_MASK) >>CCNI_EVENT_SRC_OFFSET) - 1;
    dst_index = ((event&CCNI_DST_MASK) >>CCNI_EVENT_DST_OFFSET) - 1;

    //only supports DSP0 to clear received event
    if ((CCNI_EVENT_SRC_DSP0!=(event&CCNI_SRC_MASK)) || (CCNI_EVENT_DST_DSP0==(event&CCNI_DST_MASK))
        || ((event&CCNI_EVENT_MASK)>=CCNI_MAX_EVENT)) {
        return (status = HAL_CCNI_STATUS_INVALID_PARAMETER);
    }

    hal_nvic_save_and_set_interrupt_mask(&mask);
    //check event if it is busy
    status = hal_ccni_query_event_status(event, &data);
    if (status != HAL_CCNI_STATUS_OK) {
        hal_nvic_restore_interrupt_mask(mask);
        return status;
    }    
    if (data == HAL_CCNI_EVENT_STATUS_BUSY) {
        hal_nvic_restore_interrupt_mask(mask);
        return (status = HAL_CCNI_STATUS_BUSY);
    }
    event_index = (event & CCNI_EVENT_MASK);
    if (event_index < CCNI_MAX_MSG) {
        p_share_memory = (uint32_t*)dsp0_send_message_memory_pool[dst_index];
        p_share_memory[event_index*2] =message->ccni_message[0];
        p_share_memory[event_index*2 +1]=message->ccni_message[1];
    }
    setr_addr = base_addr[src_index][dst_index] + IRQ_SET_OFFSET;
    (*(volatile uint32_t*)setr_addr) |= (CCNI_REG_ONE_BIT_SET<<(event&CCNI_EVENT_MASK));
    hal_nvic_restore_interrupt_mask(mask);
    return status;
}

static hal_ccni_status_t ccni_write_register(hal_ccni_event_t event, uint32_t register_offset, uint32_t value)
{
    uint32_t reg_addr,mask;
    uint32_t src_index, dst_index;
    //only supports DSP0 to clear event / mask event / unmask event
    if ((CCNI_EVENT_SRC_DSP0 == (event&CCNI_SRC_MASK)) || (CCNI_EVENT_DST_DSP0 != (event&CCNI_DST_MASK)) 
        || ((event&CCNI_EVENT_MASK)>=CCNI_MAX_EVENT)) {
        return HAL_CCNI_STATUS_INVALID_PARAMETER;
    }

    src_index = ((event&CCNI_SRC_MASK) >>CCNI_EVENT_SRC_OFFSET) - 1;
    dst_index = ((event&CCNI_DST_MASK) >>CCNI_EVENT_DST_OFFSET) - 1;

#ifdef CCNI_DEBUG
    log_hal_msgid_info("[ccni_write_register]:event=0x%x, reg=0x%x, value =0x%x \r\n",3,event,register_offset,value);
#endif

    hal_nvic_save_and_set_interrupt_mask(&mask);
    reg_addr = base_addr[src_index][dst_index] + register_offset;
    if (value == 1) {
        (*(volatile uint32_t*)reg_addr)|=(0x1<<(event&CCNI_EVENT_MASK));
    } else {
        (*(volatile uint32_t*)reg_addr) &= (~(0x1<<(event&CCNI_EVENT_MASK)));
    }
    hal_nvic_restore_interrupt_mask(mask);
    return HAL_CCNI_STATUS_OK;
}

//CM4 Clear the event from other processer after handler it
hal_ccni_status_t hal_ccni_clear_event(hal_ccni_event_t event)
{
    return ccni_write_register(event,IRQ_CLR_OFFSET,CCNI_EVENT_CLEAR_EN);
}

//CM4 MASK the event from other processer after handler it
hal_ccni_status_t hal_ccni_mask_event(hal_ccni_event_t event)
{
    return ccni_write_register(event,IRQ_MASK_OFFSET,CCNI_EVENT_MASK_EN);
}

//CM4 UNMASK the event from other processer after handler it
hal_ccni_status_t hal_ccni_unmask_event(hal_ccni_event_t event)
{
    return ccni_write_register(event,IRQ_MASK_OFFSET,CCNI_EVENT_UNMASK_EN);
}

hal_ccni_status_t hal_ccni_deinit(void)
{
    return HAL_CCNI_STATUS_OK;
}

#ifdef __cplusplus
}
#endif

#endif /* HAL_CCNI_MODULE_ENABLED */

