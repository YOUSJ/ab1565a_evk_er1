/* Copyright Statement:
 *
 * (C) 2018  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_audio.h"

#ifdef HAL_AUDIO_MODULE_ENABLED

#include "hal_audio_afe_control.h"
#include "hal_audio_afe_define.h"
#include "hal_audio_afe_clock.h"
#ifdef MTK_ANC_ENABLE
#include "anc_api.h"
#endif


extern afe_t afe;

void hal_audio_init(void)
{
    afe_control_init();
    afe_sidetone_init();
    //set default device in case device is setting after app on. null ops would cause fatal error.
    afe.stream_in.audio_device = HAL_AUDIO_DEVICE_DUAL_DIGITAL_MIC;
    afe.stream_out.audio_device = HAL_AUDIO_DEVICE_HEADSET;
    afe.adc_state.adc_l_state = 0;
    afe.adc_state.adc_r_state = 0;
#ifdef ENABLE_2A2D_TEST
    afe.adc_state.adc_dmic_state = 0;
#endif
    afe.micbias_state.micbias0_state = 0;
    afe.micbias_state.micbias1_state = 0;
#ifdef MTK_ANC_ENABLE
    afe_anc_init();
#endif
}

/**
  * @ Control the audio output device
  * @ device: output device
  */
hal_audio_status_t hal_audio_set_stream_out_device(hal_audio_device_t device)
{
    afe.stream_out.audio_device = device;
    /*should coding for device switch during audio on*/
    /*should return invalid device setting if device is not support in AB155x*/
    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Control the audio input device
  * @ device: input device
  */
hal_audio_status_t hal_audio_set_stream_in_device(hal_audio_device_t device)
{
    afe.stream_in.audio_device = device;
    /*should coding for device switch during audio on*/
    /*should return invalid device setting if device is not support in AB155x*/
    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Query the audio output device
  */
hal_audio_device_t hal_audio_get_stream_out_device(void)
{
    return afe.stream_out.audio_device;
}

/**
  * @ Query the audio input device
  */
hal_audio_device_t hal_audio_get_stream_in_device(void)
{
    return afe.stream_in.audio_device;
}

/**
  * @ Updates the audio output volume
  * @ digital_volume_index: digital gain index
  * @ analog_volume_index : analog gain index
  */
void hal_audio_set_stream_out_volume(uint16_t digital_volume_index, uint16_t analog_volume_index)
{
    if (analog_volume_index != HAL_AUDIO_INVALID_ANALOG_GAIN_INDEX) {
        afe.stream_out.analog_gain_index  = (int32_t)((int16_t)analog_volume_index);
    }
    if (digital_volume_index != HAL_AUDIO_INVALID_DIGITAL_GAIN_INDEX) {
        afe.stream_out.digital_gain_index = (int32_t)((int16_t)digital_volume_index);
    }
    if (afe_get_dac_clock_status()) {
        afe_audio_set_output_analog_gain();
    }
    afe_audio_set_output_digital_gain(AUDIO_HW_GAIN);
}

/**
  * @ Updates the audio output volume
  * @ digital_volume_index: digital gain index
  * @ analog_volume_index : analog gain index
  */
void hal_audio_set_stream_out_volume2(uint16_t digital_volume_index, uint16_t analog_volume_index)
{
    if (analog_volume_index != HAL_AUDIO_INVALID_ANALOG_GAIN_INDEX) {
        afe.stream_out.analog_gain_index  = (int32_t)((int16_t)analog_volume_index);
    }
    if (digital_volume_index != HAL_AUDIO_INVALID_DIGITAL_GAIN_INDEX) {
        afe.stream_out.digital_gain_index2 = (int32_t)((int16_t)digital_volume_index);
    }
    if (afe_get_dac_clock_status()) {
        afe_audio_set_output_analog_gain();
    }
    afe_audio_set_output_digital_gain(AUDIO_HW_GAIN2);
}

/**
  * @ Updates the audio input volume for multiple microphones.
  * @ volume_index0: input gain index 0
  * @ volume_index1: input gain index 1
  * @ gain_select  : select which pair of gain to be setting
  */
void hal_audio_set_stream_in_volume_for_multiple_microphone(uint16_t volume_index0, uint16_t volume_index1, hal_audio_input_gain_select_t gain_select)
{
    if (gain_select == HAL_AUDIO_INPUT_GAIN_SELECTION_D0_D1) {
        if (volume_index0 != HAL_AUDIO_INVALID_DIGITAL_GAIN_INDEX) {
            afe.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_0] = (int32_t)((int16_t)volume_index0);
        }
        if (volume_index1 != HAL_AUDIO_INVALID_DIGITAL_GAIN_INDEX) {
            afe.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_1] = (int32_t)((int16_t)volume_index1);
        }
    } else if (gain_select == HAL_AUDIO_INPUT_GAIN_SELECTION_D2_D3) {
        if (volume_index0 != HAL_AUDIO_INVALID_DIGITAL_GAIN_INDEX) {
            afe.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_2] = (int32_t)((int16_t)volume_index0);
        }
        if (volume_index1 != HAL_AUDIO_INVALID_DIGITAL_GAIN_INDEX) {
            afe.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_3] = (int32_t)((int16_t)volume_index1);
        }
    } else if (gain_select == HAL_AUDIO_INPUT_GAIN_SELECTION_D4) {
        if (volume_index0 != HAL_AUDIO_INVALID_DIGITAL_GAIN_INDEX) {
            afe.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_ECHO_PATH] = (int32_t)((int16_t)volume_index0);
        }
    } else if (gain_select == HAL_AUDIO_INPUT_GAIN_SELECTION_A0_A1) {
        if (volume_index0 != HAL_AUDIO_INVALID_ANALOG_GAIN_INDEX) {
            afe.stream_in.analog_gain_index[INPUT_ANALOG_GAIN_FOR_MIC_L] = (int32_t)((int16_t)volume_index0);
        }
        if (volume_index1 != HAL_AUDIO_INVALID_ANALOG_GAIN_INDEX) {
            afe.stream_in.analog_gain_index[INPUT_ANALOG_GAIN_FOR_MIC_R] = (int32_t)((int16_t)volume_index1);
        }
    }
    afe_audio_set_input_analog_gain();
    //set input digital gain (use IP's gain)
}

/**
  * @ Updates the audio input volume
  * @ digital_volume_index: digital gain index
  * @ analog_volume_index : analog gain index
  */
void hal_audio_set_stream_in_volume(uint16_t digital_volume_index, uint16_t analog_volume_index)
{
    if (analog_volume_index != HAL_AUDIO_INVALID_ANALOG_GAIN_INDEX) {
        afe.stream_in.analog_gain_index[INPUT_ANALOG_GAIN_FOR_MIC_L]  = (int32_t)((int16_t)analog_volume_index);
        afe.stream_in.analog_gain_index[INPUT_ANALOG_GAIN_FOR_MIC_R]  = (int32_t)((int16_t)analog_volume_index);
    }
    if (digital_volume_index != HAL_AUDIO_INVALID_DIGITAL_GAIN_INDEX) {
        afe.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_0] = (int32_t)((int16_t)digital_volume_index);
        afe.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_1] = (int32_t)((int16_t)digital_volume_index);
        afe.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_2] = (int32_t)((int16_t)digital_volume_index);
        afe.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_3] = (int32_t)((int16_t)digital_volume_index);
        afe.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_ECHO_PATH] = (int32_t)((int16_t)digital_volume_index);
    }
    afe_audio_set_input_analog_gain();
    //set input digital gain (use IP's gain)
}

#if defined(HAL_AUDIO_SUPPORT_MULTIPLE_STREAM_OUT)
/**
  * @ Mute/Unmute stream ouput path
  * @ mute: true-> set mute / false->set unmute
  * @ hw_gain_index: HAL_AUDIO_STREAM_OUT1-> indicate hw gain1 / HAL_AUDIO_STREAM_OUT2-> indicate hw gain2 / HAL_AUDIO_STREAM_OUT_ALL-> indicate hw gain1 and hw gain2
  */
void hal_audio_mute_stream_out(bool mute, hal_audio_hw_stream_out_index_t hw_gain_index)
{
    if (hw_gain_index == HAL_AUDIO_STREAM_OUT1 || hw_gain_index == HAL_AUDIO_STREAM_OUT_ALL) {
        afe.stream_out.mute_flag = mute;
        afe_audio_set_output_digital_gain(AUDIO_HW_GAIN);
    }
    if (hw_gain_index == HAL_AUDIO_STREAM_OUT2 || hw_gain_index == HAL_AUDIO_STREAM_OUT_ALL) {
        afe.stream_out.mute_flag2 = mute;
        afe_audio_set_output_digital_gain(AUDIO_HW_GAIN2);
    }
}
#else
/**
  * @ Mute stream ouput path
  * @ mute: true-> set mute / false->set unmute
  */
void hal_audio_mute_stream_out(bool mute)
{
    afe.stream_out.mute_flag = mute;
    afe_audio_set_output_analog_gain();
}
#endif


/**
  * @ Mute stream input path
  * @ mute: true-> set mute / false->set unmute
  */
void hal_audio_mute_stream_in(bool mute)
{
    afe.stream_in.mute_flag = mute;
    afe_audio_set_input_analog_gain();
}

/**
  * @ Updates the audio output channel number
  * @ channel_number : audio channel mode to play the audio stream
  * @ This API should be called before hal_audio_start_stream_out() to adjust the output channel number
  * @ Retval: HAL_AUDIO_STATUS_OK if operation success, others if channel number is invalid
  */
hal_audio_status_t hal_audio_set_stream_out_channel_number(hal_audio_channel_number_t channel_number)
{
    hal_audio_status_t result = HAL_AUDIO_STATUS_OK;
    switch (channel_number) {
        case HAL_AUDIO_MONO:
            afe.stream_out.stream_channel = HAL_AUDIO_MONO;
            break;
        case HAL_AUDIO_STEREO:
        case HAL_AUDIO_STEREO_BOTH_L_CHANNEL:
        case HAL_AUDIO_STEREO_BOTH_R_CHANNEL:
        case HAL_AUDIO_STEREO_BOTH_L_R_SWAP:
            afe.stream_out.stream_channel = HAL_AUDIO_STEREO;
            break;
        default:
            result = HAL_AUDIO_STATUS_INVALID_PARAMETER;
            break;
    }
    return result;
}

/**
  * @ Updates the audio input channel number
  * @ channel_number : audio channel mode to record the audio stream
  * @ This API should be called before hal_audio_start_stream_in() to adjust the input channel number
  * @ Retval: HAL_AUDIO_STATUS_OK if operation is successful, others if channel number is invalid
  */
hal_audio_status_t hal_audio_set_stream_in_channel_number(hal_audio_channel_number_t channel_number)
{
    switch (channel_number) {
        case HAL_AUDIO_MONO:
        case HAL_AUDIO_STEREO:
            afe.stream_in.stream_channel = channel_number;
            return HAL_AUDIO_STATUS_OK;
        default:
            return HAL_AUDIO_STATUS_INVALID_PARAMETER;
    }
}

/**
  * @ Query the output channel number
  */
hal_audio_channel_number_t hal_audio_get_stream_out_channel_number(void)
{
    return afe.stream_out.stream_channel;
}


void hal_audio_afe_register_irq_callback(hal_audio_irq_callback_function_t* function)
{
    afe.func_handle = function;
}

void hal_audio_afe_register_amp_handle(hal_amp_function_t *function)
{
#ifdef ENABLE_AMP_TIMER
    afe.amp_handle = function;
#else
    UNUSED(function);
#endif
}


#endif /*HAL_AUDIO_MODULE_ENABLED*/
