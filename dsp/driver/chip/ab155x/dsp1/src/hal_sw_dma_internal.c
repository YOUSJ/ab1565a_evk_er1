/* Copyright Statement:
 *
 * (C) 2018  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_sw_dma.h"

#ifdef HAL_SW_DMA_MODULE_ENABLED

#include "hal_sw_dma_internal.h"
#include "hal_pdma_internal.h"
#include "hal_sleep_manager.h"
#include "hal_sleep_manager_internal.h"
#include "hal_cache.h"
#include "assert.h"
#include "hal_resource_assignment.h"

/* channel value is start from channel 0 */
bool sw_dma_channel_is_idle(uint32_t channel)
{
    uint32_t  global_status = 0;

    global_status = SW_DMA_GLBSTA_FOR_DSP;
    if (global_status & (0x1 << (0x2 * channel))) {
        return false;
    } else {
        return true;
    }
}

bool sw_dma_channel_has_interrupt(uint32_t channel)
{
    uint32_t  global_status = 0;

    global_status = SW_DMA_GLBSTA_FOR_DSP;
    if (global_status & (0x1 << (0x2 * channel + 0x1))) {
        return true;
    } else {
        return false;
    }
}

void sw_dma_init(uint32_t channel)
{
#if defined (CORE_DSP0)
    *(volatile uint32_t *)(0xa2270350) = 0x800;
    SW_DMA_CLK_SET_FOR_DSP = 0x1 << channel;
    SW_DMA_INT_SET_FOR_DSP0 = 0x1 << channel;
#elif defined (CORE_DSP1)
    *(volatile uint32_t *)(0xa2270350) = 0x800;
    SW_DMA_CLK_SET_FOR_DSP = 0x1 << channel;
    SW_DMA_INT_SET_FOR_DSP1 = 0x1 << channel;
#elif defined (CORE_CM4)
    *(volatile uint32_t *)(0xa2270350) = 0x400;
    SW_DMA_CLK_SET_FOR_CM4 = 0x1 << (channel - 4);
    SW_DMA_INT_SET_FOR_CM4 = 0x1 << (channel - 4);
#endif

    /*don't check gdma busy now, since sw_dma_init() only called once */
    SW_DMA_REG_SRC_DSP(channel)     = 0x0;
    SW_DMA_REG_DST_DSP(channel)     = 0x0;
    SW_DMA_REG_WPPT_DSP(channel)    = 0x0;
    SW_DMA_REG_WPTO_DSP(channel)    = 0x0;
    SW_DMA_REG_CON_DSP(channel)     = 0x0;
    SW_DMA_REG_START_DSP(channel)   = 0x0;
    SW_DMA_REG_ACKINT_DSP(channel)  = 0x8000;
    SW_DMA_REG_LIMITER_DSP(channel) = 0x0;
}

static hal_sw_gdma_status_t sw_dma_start_setting(hal_sw_dma_config_info_t *info)
{
    uint32_t src_addr, dst_addr;
    uint32_t saved_mask;

    src_addr = hal_memview_dsp1_to_infrasys(info->source_address);
    dst_addr = hal_memview_dsp1_to_infrasys(info->destination_address);

    hal_nvic_save_and_set_interrupt_mask(&saved_mask);
    if ((sw_dma_channel_is_idle(SW_DMA_CHANNEL) == false) || (sw_dma_channel_has_interrupt(SW_DMA_CHANNEL) == true)) {
        hal_nvic_restore_interrupt_mask(saved_mask);
        assert(0);
        return HAL_SW_DMA_STATUS_ERROR;
    } else {
        hal_nvic_restore_interrupt_mask(saved_mask);
    }

    SW_DMA_REG_CON_DSP(SW_DMA_CHANNEL) = ((info->dma_type & 0x3) | (info->h_size << 8) | (info->transfer_type << 18) | (0x1 << 24));
    SW_DMA_REG_COUNT_DSP(SW_DMA_CHANNEL) = (info->length) >> (info->h_size);
    SW_DMA_REG_SRC_DSP(SW_DMA_CHANNEL) = src_addr;
    SW_DMA_REG_DST_DSP(SW_DMA_CHANNEL) = dst_addr;

    return HAL_SW_DMA_STATUS_OK;
}
hal_sw_gdma_status_t sw_dma_start_interrupt(hal_sw_dma_config_info_t *info)
{
    hal_sw_gdma_status_t status;

    status = sw_dma_start_setting(info);
    if (status != HAL_SW_DMA_STATUS_OK) {
        return status;
    }

    /*lock sleep mode */
#ifdef HAL_SLEEP_MANAGER_ENABLED
    hal_sleep_manager_lock_sleep(SLEEP_LOCK_DMA);
#endif

    SW_DMA_REG_START_DSP(SW_DMA_CHANNEL) = 0x8000;

    return HAL_SW_DMA_STATUS_OK;
}

#endif /*HAL_SW_DMA_MODULE_ENABLED*/

