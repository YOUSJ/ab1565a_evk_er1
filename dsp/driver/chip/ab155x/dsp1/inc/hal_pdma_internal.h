/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#ifndef __HAL_PDMA_INTERNAL_H__
#define __HAL_PDMA_INTERNAL_H__

/**
 * @ingroup HAL
 * @addtogroup Peripheral DMA
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif


#include "hal_nvic.h"
#include "hal_nvic_internal.h"
#include "hal_log.h"

#define DSP_DMA_GLB_CLK_SET  (0xA3000074)
#define DSP_DMA_GLB_CLK_CLR  (0xA3000078)
#define CM4_DMA_GLB_CLK_SET  (0xA0020074)
#define CM4_DMA_GLB_CLK_CLR  (0xA0020078)
#define I2S_DMA_GLB_CLK_SET  (0xA3010074)
#define I2S_DMA_GLB_CLK_CLR  (0xA3010078)
#define I2C_DMA_GLB_CLK_SET  (0xA0180074)
#define I2C_DMA_GLB_CLK_CLR  (0xA0180078)

#define DSP_DMA_BASE         (0xA3000000)
#define CM4_DMA_BASE         (0xA0020000)
#define I2S_DMA_BASE         (0xA3010000)
#define I2C_DMA_BASE         (0xA0180000)

#define CPU0_INT_SET_OFFSET  (0x0C)
#define CPU0_INT_CLR_OFFSET  (0x10)
#define CPU1_INT_SET_OFFSET  (0x18)
#define CPU1_INT_CLR_OFFSET  (0x1C)
#define CPU2_INT_SET_OFFSET  (0x54)
#define CPU2_INT_CLR_OFFSET  (0x58)
#define CPU3_INT_SET_OFFSET  (0x64)
#define CPU3_INT_CLR_OFFSET  (0x68)

#define INVALID_INDEX  0xff

/*vdma channel number definition
*/
#define VDMA_NUMBER_FOR_DSP_CM4   6

#define VDMA_NUMBER   14

/*all dma channel number definition,DMA1~DMA20
*/
#define DMA_NUMBER   25

#define DMA_FOR_DSP

#define VDMA_INIT  1
#define VDMA_DEINIT  0
#define VDMA_CHECK_AND_SET_INIT(vdma_port)  \
    do{ \
        uint32_t saved_mask; \
         hal_nvic_save_and_set_interrupt_mask(&saved_mask); \
        if(vdma_init_status[vdma_port] == VDMA_INIT){ \
            assert(0); \
        } else { \
            vdma_init_status[vdma_port] = VDMA_INIT;  \
        } \
            hal_nvic_restore_interrupt_mask(saved_mask); \
    }while(0)

#define VDMA_SET_IDLE(vdma_port)   \
    do{ \
        uint32_t saved_mask; \
         hal_nvic_save_and_set_interrupt_mask(&saved_mask); \
        if(vdma_init_status[vdma_port] == VDMA_DEINIT){ \
                assert(0); \
        } else { \
                vdma_init_status[vdma_port] = VDMA_DEINIT;  \
            } \
                hal_nvic_restore_interrupt_mask(saved_mask); \
        }while(0)

/*****************************************************************************
* Enum
*****************************************************************************/


/** @brief  dma interrupt type define */
typedef enum {
    DMA_IRQ_DSP_ONLY  = 0,       /**< dma interrupt type--for dsp only*/
    DMA_IRQ_DSP_CM4  = 1,        /**< dma interrupt type--for dsp and cm4 common*/
    DMA_IRQ_I2S = 2              /**< dma interrupt type--for i2s*/
} dma_domain_type_t;

/** @brief  dma  for master mcu control define */
typedef enum {
    DMA_FOR_CM4  = 0,       /**< dma interrupt type--for cm4*/
    DMA_FOR_DSP_0  = 1,     /**< dma interrupt type--for dsp0*/
    DMA_FOR_DSP_1 = 2       /**< dma interrupt type--for dsp1*/
} dma_master_type_t;

/** @brief  dma direction define */
typedef enum {
    DMA_RX  = 0,                /**< dma direction type --dma rx dection*/
    DMA_TX  = 1                 /**< dma direction type --dma tx dection*/
} dma_direction_t;

/** @brief virtual fifo  dma master name */
typedef enum {
    VDMA_START_CHANNEL = 5,               /**< virtual fifo dma start channel */

    VDMA_UART2TX = VDMA_START_CHANNEL,    /**<virtual fifo  dma channel 6*/
    VDMA_UART2RX = 6,                     /**< virtual fifo dma channel 7*/
    VDMA_UART1TX = 7,                     /**< virtual fifo dma channel 8 */
    VDMA_UART1RX = 8,                     /**<virtual fifo  dma channel 9 */
    VDMA_UART0TX = 9,                     /**<virtual fifo  dma channel 10 */
    VDMA_UART0RX = 10,                    /**< virtual fifo dma channel 11*/
    VDMA_I2S0TX = 11,                     /**< virtual fifo dma channel 12 */
    VDMA_I2S0RX = 12,                     /**<virtual fifo  dma channel 13 */
    VDMA_I2S1TX = 13,                     /**<virtual fifo  dma channel 14*/
    VDMA_I2S1RX = 14,                     /**< virtual fifo dma channel 15*/
    VDMA_I2S2TX = 15,                     /**<virtual fifo  dma channel 16*/
    VDMA_I2S2RX = 16,                     /**< virtual fifo dma channel 17*/
    VDMA_I2S3TX = 17,                     /**<virtual fifo  dma channel 18*/
    VDMA_I2S3RX = 18,                     /**< virtual fifo dma channel 19*/
    VDMA_END_CHANNEL                      /**< virtual fifo dma  end channel (invalid) */

} vdma_channel_t;

/** @brief virtual fifo dma status */
typedef enum {
    VDMA_ERROR               = -3,        /**<virtual fifo dma function EEROR */
    VDMA_ERROR_CHANNEL       = -2,        /**<virtual fifo dma error channel */
    VDMA_INVALID_PARAMETER   = -1,        /**< virtual fifo dma error invalid parameter */
    VDMA_OK   = 0                         /**< virtual fifo dma function OK*/
} vdma_status_t;



/** @brief virtual fifo dma  running status */
typedef enum {
    VDMA_IDLE = 0,                         /**< vdma idle */
    VDMA_BUSY = 1                          /**< vdma busy */
} vdma_running_status_t;

/** @brief virtual fifo  dma transaction error */
typedef enum {
    VDMA_EVENT_TRANSACTION_ERROR = -1,         /**<virtual fifo  dma transaction error */
    VDMA_EVENT_TRANSACTION_SUCCESS = 0         /**<virtual fifo  dma transaction success */
} vdma_event_t;

#define INVALID_STATUS          0xff

#define VALID_STATUS             0x0

#define HALF_TYPE_SIZE            2

#define WORD_TYPE_SIZE            4

#define VDMA_MAX_THRESHOLD       0xFFFF

#define VDMA_MAX_ALERT_LENGTH    0x3F


/*****************************************************************************
* Structure
*****************************************************************************/

/** @brief  virtual  dma transaction mode */
typedef struct {
    uint32_t          base_address;                /**< the  virtual fifo  start address */
    uint32_t          size;                        /**< the  virtual fifo  size */
} vdma_config_t;

/** @brief virtual dma callback typedef */
typedef void (*vdma_callback_t)(vdma_event_t event, void  *user_data);


/*****************************************************************************
* Function Interface
*****************************************************************************/

void dma_dsp_only_interrupt_hander(void);
void dma_dsp_cm4_interrupt_hander(void);
void dma_i2s_interrupt_hander(void);

uint32_t dma_enable_clock(uint8_t dma_channel);

uint32_t dma_disable_clock(uint8_t dma_channel);

uint32_t dma_set_int_for_cpu_master(uint8_t dma_channel);

uint32_t dma_clear_int_for_cpu_master(uint8_t dma_channel);

void dma_set_interrupt (uint32_t dma_channel);

/**
 * @brief       init virtual fifo dma init
 * @param[in] channel,dma master name definition in enum vdma_channel_t
 * @return if OK ,returns VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @par       pdma_config_t *pdma_config
 * @code      pdma_init(VDMA_UART1TX);
 * @endcode
 */
vdma_status_t vdma_init(vdma_channel_t channel);


/**
 * @brief     reset peripheral dma register and reset state
 * @param[in] channel,dma master name definition in enum vdma_channel_t
 * @return if OK ,returns VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @code      pdma_deinit(VDMA_UART1TX);
 * @endcode
 */
vdma_status_t vdma_deinit(vdma_channel_t channel);

vdma_status_t vdma_start(vdma_channel_t channel);
vdma_status_t vdma_stop(vdma_channel_t channel);

/**
 * @brief     peripheral dma all configuration
 * @param[in] channel,dma master name definition in enum vdma_channel_t
 * @param[in] config sructure pointer,vdma_config
 * @return if set correctly ,return VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX,vdma_config_t  *vdma_config
 * @par           vdma_config->base_address  = vfifo_start_address;
 * @par       pdma_config->size= 1024;
 * @code      vdma_configure(VDMA_UART1TX,vdma_config);
 * @endcode
 */
vdma_status_t vdma_configure(vdma_channel_t channel, vdma_config_t *vdma_config);


/**
 * @brief     enable vfifo dma interrupt
 * @param[in] channel,dma master name definition in enum vdma_channel_t
 * @return if set correctly ,return VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @code      vdma_enable_interrupt(VDMA_UART1TX,address,data_length);
 * @endcode
 */
vdma_status_t vdma_enable_interrupt(vdma_channel_t channel);


/**
 * @brief     disable vfifo dma interrupt  ,
 * @param[in] channel,dma master name definition in enum  vdma_channel_t
 * @return if set correctly ,return VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @code      vdma_disable_interrupt(VDMA_UART1TX);
 * @endcode
 */
vdma_status_t vdma_disable_interrupt(vdma_channel_t channel);

/**
 * @brief     set vfifo dma  threshold ,
 * @param[in] channel,dma master name definition in enum  vdma_channel_t
 * @param[in] threshold
 * @return if set correctly ,return VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @code      vdma_set_threshold(VDMA_UART1TX,1024);
 * @endcode
 */
vdma_status_t vdma_set_threshold(vdma_channel_t channel, uint32_t threshold);

/**
 * @brief     set vfifo dma  alert length,
 * @param[in] channel,dma master name definition in enum  vdma_channel_t
 * @param[in] alert_length,range 0~31
 * @return if set correctly ,return VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @code      vdma_set_alert_length(VDMA_UART1TX,16);
 * @endcode
 */
vdma_status_t vdma_set_alert_length(vdma_channel_t channel, uint32_t alert_length);


/**
 * @brief     push data to virtual fifo,
 * @param[in] channel,dma master name definition in enum  vdma_channel_t
 * @param[in] data
 * @return if set correctly ,return VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @code      vdma_push_data(VDMA_UART1TX,data);
 * @endcode
 */
vdma_status_t vdma_push_data(vdma_channel_t channel, uint8_t data);

vdma_status_t vdma_push_data_4bytes(vdma_channel_t channel, uint32_t data);

/**
 * @brief    Push data to virtual fifo, user should call
 *           vdma_get_available_send_space() firstly to get the
 *           available bytes for sending.
 * @param[in] channel,dma master name definition in enum  vdma_channel_t
 * @param[in] data
 * @param[in] size, data size in byte unit that user wants to
 *       push to VFIFO.
 * @return if set correctly ,return VDMA_OK
 * @note
 * @warning
 * @endcode
 */
vdma_status_t vdma_push_data_multi_bytes(vdma_channel_t channel, uint8_t *data, uint32_t size);

/**
 * @brief     pop data from virtual fifo,
 * @param[in] channel,dma master name definition in enum  vdma_channel_t
 * @param[out] *data,get single  byte from virtual fifo and put in data
 * @return if pop data correctly ,return VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @code      vdma_pop_data(VDMA_UART1TX,&data);
 * @endcode
 */
vdma_status_t vdma_pop_data(vdma_channel_t channel, uint8_t *data);

vdma_status_t vdma_pop_data_4bytes(vdma_channel_t channel, uint32_t *data);

/**
 * @brief    Pop data from virtual fifo, user should call
 *           vdma_get_available_receive_bytes() firstly to get
 *           the data bytes available for reading from VFIFO.
 * @param[in] channel,dma master name definition in enum  vdma_channel_t
 * @param[out] *data, get multiple bytes from virtual fifo and
 *       put in data buffer.
 * @param[in] size, data size in byte unit that shows the
 *       available data in virtual fifo.
 * @return if set correctly ,return VDMA_OK
 * @note
 * @warning
 * @endcode
 */
vdma_status_t vdma_pop_data_multi_bytes(vdma_channel_t channel, uint8_t *data, uint32_t size);

/**
 * @brief    get the number of data stored in virtual fifo
 * @param[in] channel,dma master name definition in enum  vdma_channel_t
 * @param[out] *receive_bytes,get available received bytes from virtual fifo and put in receive_bytes,this is for RX VFIFO
 * @return if get data correctly ,return VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @code      vdma_get_available_room(VDMA_UART1TX,&receive_bytes);
 * @endcode
 */
vdma_status_t vdma_get_available_receive_bytes(vdma_channel_t channel, uint32_t *receive_bytes);

/**
 * @brief    get the left room of   virtual fifo
 * @param[in] channel,dma master name definition in enum  vdma_channel_t
 * @param[out] *available_space ,get available space in virtual fifo and put in  available_space,this is for TX VFIFO
 * @return if get data correctly ,return VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @code      vdma_get_available_send_space(VDMA_UART1TX,&available_space);
 * @endcode
 */
vdma_status_t vdma_get_available_send_space(vdma_channel_t channel, uint32_t *available_space);

/**
 * @brief    get the the read pointer of   virtual fifo
 * @param[in] channel,dma master name definition in enum  vdma_channel_t
 * @param[out] *read_point ,get the read point in virtual fifo RDPTR register
 * @return if get the read point correctly ,return VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @code      vdma_get_hw_read_point(VDMA_UART1TX,&read_point);
 * @endcode
 */
vdma_status_t vdma_get_hw_read_point(vdma_channel_t channel, uint32_t *read_point);

/**
 * @brief    set the write point in  virtual fifo
 * @param[in] channel, dma master name definition in enum  vdma_channel_t
 * @param[in] sw_move_byte, set the write point in  virtual fifo SW_MV_BYTE register
 * @return if set the sw_move_byte correctly , return VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @code      vdma_set_sw_move_byte(VDMA_UART1TX,sw_move_byte);
 * @endcode
 */
vdma_status_t vdma_set_sw_move_byte(vdma_channel_t channel, uint16_t sw_move_byte);


/**
 * @brief    get the the write pointer of virtual fifo
 * @param[in] channel,dma master name definition in enum  vdma_channel_t
 * @param[out] *write_point ,get the write point in virtual fifo WRPTR register
 * @return if get the read point correctly ,return VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @code      vdma_get_hw_read_point(VDMA_UART1TX,&write_point);
 * @endcode
 */
vdma_status_t vdma_get_hw_write_point(vdma_channel_t channel, uint32_t *write_point);

/**
 * @brief     virtual dma register callback,
 * @param[in] channel,dma master name definition in enum pdma_channel_t
 * @param[in] function_callback,function pointer
 * @param[in] parameter,user's parameter
 * @return if set correctly ,return VDMA_OK
 * @note
 * @warning
 * @par       VDMA_UART1TX
 * @par       void function_callback(void *parameter)
 * @code      vdma_register_callback(VDMA_UART1TX,function_callback,parameter);
 * @endcode
 */
vdma_status_t vdma_register_callback(vdma_channel_t channel, vdma_callback_t callback, void *user_data);


#ifdef __cplusplus
}
#endif

/**@ }*/


#endif /* __HAL_PDMA_INTERNAL_H__ */

/**example
 Need to do
**/

