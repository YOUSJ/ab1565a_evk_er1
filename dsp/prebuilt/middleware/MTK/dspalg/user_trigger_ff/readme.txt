User Trigger adaptive FF prebuilt module usage guide

Brief:          This module is the User Trigger adaptive FF prebuilt library.

Usage:          GCC:  For User Trigger adaptive FF, include the module with
                      1) Add the following module.mk for libs and source file:
                         include $(SOURCE_DIR)/middleware/MTK/dspalg/user_trigger_ff/module.mk
                      2) Module.mk provides different options to enable or disable according profiles, please configure these options on specified GCC/feature.mk:
                         MTK_USER_TRIGGER_FF_ENABLE
                      3) Add the header file path:
                         CFLAGS += -I$(SOURCE_DIR)/middleware/MTK/dspalg/user_trigger_ff/inc

Dependency:     None

Notice:         None

Relative doc:   None

Example project:None