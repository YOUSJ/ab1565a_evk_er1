/*
 * FreeRTOS Kernel V10.1.1
 * Copyright (C) 2018 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 * 1 tab == 4 spaces!
 */


/*-----------------------------------------------------------
 * Implementation of functions defined in portable.h for the ARM CM4F port.
 *----------------------------------------------------------*/

#include "hal.h"
#include "FreeRTOS.h"
#include "port_tick.h"
#include "hal_clock.h"
#include "hal_nvic.h"
#include "timers.h"
#include "hal_gpt_internal.h"
#include <assert.h>

#if configUSE_TICKLESS_IDLE == 2
#include "task.h"
#include "memory_attribute.h"
#include "hal_sleep_manager.h"
#include "hal_sleep_manager_internal.h"
#include "hal_sleep_manager_platform.h"
#include "hal_gpt.h"
#include "hal_log.h"
#include "hal_eint.h"

#endif

#define MaximumIdleTime 10  //ms
#define DEEP_SLEEP_HW_WAKEUP_TIME 1
#define DEEP_SLEEP_SW_BACKUP_RESTORE_TIME 1

#define SPM_PCM_RESERVE2                           ((volatile uint32_t*)(0xA2110000 + 0x0B04))

volatile uint32_t systick_change_period = 0;

//#define TICKLESS_DEEBUG_ENABLE
#ifdef  TICKLESS_DEEBUG_ENABLE
#define log_debug_tickless(_message,...) printf(_message, ##__VA_ARGS__)
#else
#define log_debug_tickless(_message,...)
#endif

static uint32_t count_idle_time;
static uint32_t count_sleep_time;
static uint32_t before_sleep_time;

GPT_REGISTER_T *os_gpt1 = OS_GPT1;
OS_GPT_REGISTER_GLOABL_T *os_gpt_glb = OS_GPTGLB;
bool reset_gpt_to_systick = false;

extern BaseType_t xPortSysTickHandler( void );
void os_gpt_interrupt_handle(hal_nvic_irq_t irq_number)
{
    (void)irq_number;
    os_gpt1->GPT_IRQ_ACK = 0x01;

    /* Run FreeRTOS tick handler*/
    xPortSysTickHandler();
}

void os_gpt_start_ms_for_tickless(uint32_t timeout_time_ms)
{
    if(timeout_time_ms > 130150523 )
    {
        assert(0);
    }

    //hal_nvic_save_and_set_interrupt_mask(&mask);
    os_gpt1->GPT_IRQ_ACK = 0x1;                   //clear interrupt status
    os_gpt1->GPT_CON_UNION.GPT_CON |= (1 << 16);  // disabled the clock source
    os_gpt1->GPT_CLK= 0x10;                       // set the 32k frequence
    os_gpt1->GPT_CON_UNION.GPT_CON &= ~(1 << 16);   //enable the clock source
    os_gpt1->GPT_CLR      = 0x1;                    // clear the count
    while (os_gpt_glb->OS_GPT_CLRSTA & (1<<1));

    os_gpt1->GPT_CON_UNION.GPT_CON &= ~(3<<8);    // set the one-shot mode
    os_gpt1->GPT_IRQ_EN =0x1;                     // enable the irq
    os_gpt1->GPT_COMPARE= (timeout_time_ms * 32 + (7 * timeout_time_ms) / 10 + (6 * timeout_time_ms) / 100 + (8 * timeout_time_ms) / 1000);
    while (os_gpt_glb->OS_GPT_WCOMPSTA & (1 << 1));
    os_gpt1->GPT_CON_UNION.GPT_CON |= 0x01;

    //hal_nvic_disable_irq(OS_GPT_IRQn);
    // this callback should not execute, because tickless will clear & stop this timer before unmask all IRQ when exit sleep
    hal_nvic_register_isr_handler(OS_GPT_IRQn, NULL);
    hal_nvic_enable_irq(OS_GPT_IRQn);

    os_gpt_glb->OS_GPT_IRQMSK_DSP &= 0x1;  //DSP0 IRQ enable
    os_gpt_glb->OS_GPT_WAKEUPMSK_DSP &= 0x1;  //DSP0 wakeup enable
    //hal_nvic_restore_interrupt_mask(mask);
    return ;
}
void _frxt_tick_timer_init(void);
void os_gpt_init(uint32_t us);
void os_gpt_stop_for_tickless()
{

    //uint32_t mask;
    log_debug_tickless("Wake Up From Sleep For DSP,OS GPT  g_compare_value:0x%x,os_gpt1 ->GPT_IRQ_STA:0x%x \r\n",
        os_gpt1->GPT_COMPARE,os_gpt1 ->GPT_IRQ_STA);
    //hal_nvic_save_and_set_interrupt_mask(&mask);

    /*diable interrupt*/
    os_gpt1->GPT_IRQ_EN &= 0x0;
    /* stop timer */
    os_gpt1->GPT_CON_UNION.GPT_CON_CELLS.EN = 0x0;
    os_gpt1->GPT_IRQ_ACK = 0x1;                    /* clean interrupt status */
    os_gpt1->GPT_IRQ_EN = 0;                       /* disable interrupt */
    os_gpt1->GPT_CON_UNION.GPT_CON = 0;            /* disable timer     */
    os_gpt1->GPT_CLR = 0x1;                       /* clear counter value */
    while (os_gpt_glb->OS_GPT_CLRSTA & (1 << 1));
    os_gpt1->GPT_CLK = 0xc;

    //hal_nvic_restore_interrupt_mask(mask);
    os_gpt_init(portTICK_PERIOD_MS * 1000);
    return ;
}

void os_gpt_init(uint32_t us)
{
    /* set 13 divided with 13M source */
    os_gpt1->GPT_CON_UNION.GPT_CON |= (1 << 16);   // disable clock before config
    os_gpt1->GPT_CLK = 0xc;
    os_gpt1->GPT_CON_UNION.GPT_CON &= ~(1 << 16);   // enable clock

    os_gpt1->GPT_COMPARE = us ;

    while (os_gpt_glb->OS_GPT_WCOMPSTA & (1<<1));

    /* clear */
    os_gpt1->GPT_CLR = 0x01;
    while (os_gpt1->GPT_COUNT);

    /* enable IRQ */
    os_gpt1->GPT_IRQ_EN = 0x1;
    /* enable GPT0 clk and repeat mode and enable GPT0 */
    os_gpt1->GPT_CON_UNION.GPT_CON |= 0x101;

    /* register and enable IRQ */
    hal_nvic_register_isr_handler(OS_GPT_IRQn, (hal_nvic_isr_t)os_gpt_interrupt_handle);
    hal_nvic_enable_irq(OS_GPT_IRQn);
    os_gpt_glb->OS_GPT_IRQMSK_DSP &= 0x1;
    //os_gpt_glb->OS_GPT_WAKEUPMSK &= 0x2;  // mask as system will dead when boot-up, must unmask after sleep<->wake is ok
    //os_gpt_glb->OS_GPT_WAKEUPMSK_DSP &= 0x1;  // mask as system will dead when boot-up, must unmask after sleep<->wake is ok
}

void os_gpt_pause(void)
{
}

void os_gpt_resume(bool update, uint32_t new_compare)
{
    (void)update;
    (void)new_compare;
}

#if configUSE_TICKLESS_IDLE == 2
static uint32_t TimeStampSystick, TimeStampCounter;
void tickless_log_timestamp(void);
static void get_rtc_real_clock_freq(void);
TimerHandle_t timestamp_timer = NULL;
float RTC_Freq; /* RTC 32.768KHz Freq*/
uint32_t nvic_mask;
#ifdef  SLEEP_MANAGEMENT_DEBUG_ENABLE
//extern uint32_t eint_get_status(void);
uint32_t wakeup_eint;
#endif

void doSleepSystickCalibration(uint32_t maxSystickCompensation)
{
    static uint32_t ulCompleteTickPeriods, ulCompletedCountDecrements, ulCompletedTickDecrements;
    static uint32_t nowCount, nowTick;

    /* calculate time(ostick) to jump */
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_32K, &nowCount);
    nowTick = ((uint32_t)xTaskGetTickCount() * (1000 / configTICK_RATE_HZ));
    /* get counter distance from last record */
    if (nowCount >= TimeStampCounter) {
        ulCompletedCountDecrements = nowCount - TimeStampCounter;
    } else {
        ulCompletedCountDecrements = nowCount + (0xFFFFFFFF - TimeStampCounter);
    }
    /* get ostick distance from last record */
    if (nowTick >= TimeStampSystick) {
        ulCompletedTickDecrements = nowTick - TimeStampSystick;
    } else {
        ulCompletedTickDecrements = nowTick + (0xFFFFFFFF - TimeStampSystick);
    }
    /* get counter distance for this sleep */
    ulCompletedCountDecrements = (unsigned int)(((float)ulCompletedCountDecrements) - ((float)ulCompletedTickDecrements * RTC_Freq));
    /* calculate ticks for jumping */
    ulCompleteTickPeriods = ((unsigned int)(((float)ulCompletedCountDecrements) / RTC_Freq)) / ((1000 / configTICK_RATE_HZ));

    count_sleep_time += ((nowCount - before_sleep_time) / RTC_Freq) / (1000 / configTICK_RATE_HZ);

    /* Limit OS Tick Compensation Value */
    if (ulCompleteTickPeriods > (maxSystickCompensation - 1)) {
        ulCompleteTickPeriods = maxSystickCompensation - 1;
    }

    vTaskStepTick(ulCompleteTickPeriods);

    log_debug_tickless("CSD=%d\r\n"      , (unsigned int)ulCompleteTickPeriods);
    return;
}
extern uint8_t sleep_manager_handle;
void AST_vPortSuppressTicksAndSleep(TickType_t xExpectedIdleTime)
{
    static volatile unsigned int ulAST_Reload_ms;

    hal_nvic_save_and_set_interrupt_mask(&nvic_mask);

    /* Stop the OS GPT momentarily.  */
    os_gpt_pause();

    /* Calculate total idle time to ms */
    ulAST_Reload_ms = (xExpectedIdleTime - 1) * (1000 / configTICK_RATE_HZ);
    ulAST_Reload_ms = ulAST_Reload_ms - DEEP_SLEEP_SW_BACKUP_RESTORE_TIME - DEEP_SLEEP_HW_WAKEUP_TIME;

    if (eTaskConfirmSleepModeStatus() == eAbortSleep) {
        /* Restart OS GPT. */
        os_gpt_resume(false, 0);
        /* Re-enable interrupts */
        hal_nvic_restore_interrupt_mask(nvic_mask);
        return;
    } else {
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_32K, &before_sleep_time);

        /* Enter Sleep mode */
        if (ulAST_Reload_ms > 0) {
            hal_sleep_manager_set_sleep_time((uint32_t)ulAST_Reload_ms);
            hal_sleep_manager_enter_sleep_mode(HAL_SLEEP_MODE_SLEEP);
        }

        /* Calculate and Calibration Sleep Time to OS Tick */
        doSleepSystickCalibration(xExpectedIdleTime);

        /* Restart OS GPT. */
        os_gpt_resume(false, 0);

#ifdef  SLEEP_MANAGEMENT_DEBUG_ENABLE
     //   wakeup_eint = eint_get_status();
#endif
        /* Re-enable interrupts */
        hal_nvic_restore_interrupt_mask(nvic_mask);

        //sleep_management_dump_debug_log(SLEEP_MANAGEMENT_DEBUG_LOG_DUMP);

#ifdef  SLEEP_MANAGEMENT_DEBUG_ENABLE
#ifdef  SLEEP_MANAGEMENT_DEBUG_SLEEP_WAKEUP_LOG_ENABLE
        sleep_management_dump_wakeup_source(sleep_management_dsp_status.wakeup_source, wakeup_eint);
#endif
#endif
        log_debug_tickless("\r\nEIT=%u\r\n"  , (unsigned int)xExpectedIdleTime);

        log_debug_tickless("RL=%u\r\n"       , (unsigned int)ulAST_Reload_ms);
    }
}

static void tickless_log_timestamp_callback(TimerHandle_t timer_id)
{
    if(timer_id == NULL) {
        log_debug_tickless("ERROR : tickless_log_timestamp_callback timer is NILL\r\n");
    }
    xTimerChangePeriod( timestamp_timer, 1000 * 60 * 60 * 12 / portTICK_PERIOD_MS, 0 ); //12hours
    tickless_log_timestamp();
}

void tickless_log_timestamp()
{
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_32K, &TimeStampCounter);
    TimeStampSystick = (uint32_t)xTaskGetTickCount() * (1000 / configTICK_RATE_HZ);
    get_rtc_real_clock_freq();
}

static void get_rtc_real_clock_freq(void)
{
    //rtc_count = f32k_measure_count(1, 1, windows_cnt);
    RTC_Freq = 32.768;
}

void tickless_handler(uint32_t xExpectedIdleTime)
{
    static unsigned int audio_running_flag_to_dsp;
    static uint32_t before_idle_time;
    static uint32_t after_idle_time;

#if configUSE_TICKLESS_IDLE == 2
    static int init_timestamp = 0;
    if (init_timestamp == 0) {
        init_timestamp = 1;

        tickless_log_timestamp();
        timestamp_timer = xTimerCreate("timestamp_timer",
                                       1000 * 3 / portTICK_PERIOD_MS,
                                       true,
                                       NULL,
                                       tickless_log_timestamp_callback);

        if (timestamp_timer == NULL) {
            printf("timestamp_timer create fail\n");
        } else {
            if (xTimerStart(timestamp_timer, 0) != pdPASS) {
                printf("timestamp_timer xTimerStart fail\n");
            }
        }
    }
#endif /* configUSE_TICKLESS_IDLE */

    /* if CM4 have any audio request, DSP need to lock sleep */
    if(audio_running_flag_to_dsp != *SPM_PCM_RESERVE2) {
        if(((*SPM_PCM_RESERVE2 >> 31)&0x01) == 1) {
            hal_sleep_manager_lock_sleep(sleep_manager_handle);
        }else {
            hal_sleep_manager_unlock_sleep(sleep_manager_handle);
        }
    }
    audio_running_flag_to_dsp = *SPM_PCM_RESERVE2;

    if ((xExpectedIdleTime > (MaximumIdleTime / (1000 / configTICK_RATE_HZ))) && (hal_sleep_manager_is_sleep_locked() == 0)) {
        /* Enter a critical section but don't use the taskENTER_CRITICAL()
        method as that will mask interrupts that should exit sleep mode. */

        AST_vPortSuppressTicksAndSleep(xExpectedIdleTime);
        return;
    }

    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &before_idle_time);

    /* Enter Idle mode */
    __asm__ __volatile__ (  " dsync             \n"
                            " waiti 0           \n"
                            " isync             \n");

    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &after_idle_time);
    count_idle_time += (after_idle_time - before_idle_time) / 1000;

    log_debug_tickless("\r\nST_CPT=%u\r\n"   ,  (unsigned int)xExpectedIdleTime);
}
#endif


TimerHandle_t xTimerofTest;
void log_cup_resource_callback(TimerHandle_t pxTimer);

/*
A peripheral General Purpose Timer is used for OS timer, and the interrupt is handled in GPT handler.
*/
void _frxt_tick_timer_init(void)
{
    os_gpt_init(portTICK_PERIOD_MS * 1000);
#if configGENERATE_RUN_TIME_STATS == 1
    xTimerofTest = xTimerCreate("TimerofTest", (5 * 1000 / portTICK_PERIOD_MS), pdTRUE, NULL, log_cup_resource_callback);
    xTimerStart(xTimerofTest, 0);
#endif
}

#if configGENERATE_RUN_TIME_STATS == 1
void log_cup_resource_callback(TimerHandle_t pxTimer)
{
    TaskStatus_t *pxTaskStatusArray;
    UBaseType_t uxArraySize, x;
    uint32_t ulTotalTime, ulStatsAsPercentage;
    static uint32_t ulTotalTime_now = 0,ulTotalTime_last = 0;
    float Percentage;
    float sleep_percentage;
    float idle_percentage;
    float busy_percentage;

    /* Optionally do something if the pxTimer parameter is NULL. */
    configASSERT( pxTimer );

    uxArraySize = uxTaskGetNumberOfTasks();

    pxTaskStatusArray = pvPortMalloc( uxArraySize * sizeof( TaskStatus_t ) );

    uxArraySize = uxTaskGetSystemState( pxTaskStatusArray, uxArraySize, &ulTotalTime );

    ulTotalTime_now = ulTotalTime;

    if(ulTotalTime_now > ulTotalTime_last){
        ulTotalTime = ulTotalTime_now - ulTotalTime_last;
    }else {
        ulTotalTime = ulTotalTime_now;
    }
    ulTotalTime_last = ulTotalTime_now;

    if( ulTotalTime > 0UL )
    {
        log_hal_info("ulTotalTimeL:%lu\r\n", ulTotalTime);
        log_hal_info("----------------------DSP Dump OS Task Info-----------------------------\r\n");
        for( x = 0; x < uxArraySize; x++ )
        {
            Percentage = ((float)pxTaskStatusArray[x].ulRunTimeCounter) / ((float)ulTotalTime);
            ulStatsAsPercentage = pxTaskStatusArray[x].ulRunTimeCounter / ulTotalTime;
            log_hal_info("Task[%s] State[%lu] Percentage[%lu.%lu] MinStack[%lu] RunTime[%lu]\r\n"
                                                        , pxTaskStatusArray[x].pcTaskName
                                                        , pxTaskStatusArray[x].eCurrentState
                                                        , (uint32_t)(Percentage*100)
                                                        , ((uint32_t)(Percentage*1000))%10
                                                        , pxTaskStatusArray[x].usStackHighWaterMark
                                                        , pxTaskStatusArray[x].ulRunTimeCounter
                                                        );
        }

        log_hal_info("Idle time : %d ms\r\n", count_idle_time);
        log_hal_info("Sleep time : %d ms\r\n", count_sleep_time);
        idle_percentage = ((float)count_idle_time) / 5000;
        sleep_percentage = ((float)count_sleep_time) / 5000;
        busy_percentage = 1 - idle_percentage - sleep_percentage;
        log_hal_info("Idle time percentage: [%lu]\r\n", (uint32_t)(idle_percentage*100));
        log_hal_info("Sleep time percentage: [%lu]\r\n", (uint32_t)(sleep_percentage*100));
        log_hal_info("Busy time percentage: [%lu]\r\n", (uint32_t)(busy_percentage*100));
        count_idle_time = 0;
        count_sleep_time = 0;

        log_hal_info("----------------------------------------------------------------------\r\n");
        vPortFree(pxTaskStatusArray);
    }
    vTaskClearTaskRunTimeCounter();
#ifdef  SLEEP_MANAGEMENT_DEBUG_ENABLE
    sleep_management_debug_dump_lock_sleep_time();
#endif
}
#endif
