XTENSA_CORE               = dsp0_core_winabi_xtensac
PRODUCT_VERSION           = 1552
MTK_SYSLOG_VERSION_2     ?= y
MTK_SYSLOG_SUB_FEATURE_STRING_LOG_SUPPORT = y
MTK_SYSLOG_SUB_FEATURE_BINARY_LOG_SUPPORT = y
MTK_SYSLOG_SUB_FEATURE_USB_ACTIVE_MODE = y
MTK_SYSLOG_SUB_FEATURE_OFFLINE_DUMP_ACTIVE_MODE = y
MTK_CPU_NUMBER_1                      ?= y
FPGA_ENV                 ?= n

ifeq ($(IC_CONFIG),ab155x)
COM_CFLAGS += -DAB155X
endif

ifeq ($(IC_TYPE),ab1552)
CCFLAG += -DAB1552
ASFLAG += -DAB1552
endif

ifeq ($(IC_TYPE),ab1555)
CCFLAG += -DAB1555
ASFLAG += -DAB1555
endif

ifeq ($(IC_TYPE),ab1556)
CCFLAG += -DAB1556
ASFLAG += -DAB1556
endif

ifeq ($(IC_TYPE),ab1558)
CCFLAG += -DAB1558
ASFLAG += -DAB1558
endif

CCFLAG += -DPRODUCT_VERSION=$(PRODUCT_VERSION)
CCFLAG += -DCORE_DSP0
ASFLAG += -DCORE_DSP0
CCFLAG += -D$(TARGET)_BOOTING
ifeq ($(FPGA_ENV),y)
CCFLAG += -DFPGA_ENV
endif

# Link and Compile Configuration

#CCFLAG      :=  
CCFLAG      +=  -g
CCFLAG      +=  -W
CCFLAG      +=  -Wall
CCFLAG      +=  -O2
CCFLAG      +=  -INLINE:requested
CCFLAG      +=  -mlongcalls
CCFLAG      +=  -std=gnu99
CCFLAG      +=  -ffunction-sections -fdata-sections -mtext-section-literals

#ASFLAG      :=  
ASFLAG      +=  -W
ASFLAG      +=  -Wall
ASFLAG      +=  -g
ASFLAG      +=  -mlongcalls --text-section-literals

#LDFLAG      :=  
LDFLAG      +=  --gc-sections
LDFLAG      +=  --no-relax
LDFLAG      +=  -wrap=printf
LDFLAG	    += -u _UserExceptionVector
LDFLAG	    += -u _KernelExceptionVector
LDFLAG	    += -u _DoubleExceptionVector


##
## MTK_DEBUG_LEVEL
## Brief:       This option is to configure system log debug level.
## Usage:       The valid values are empty, error, warning, info, debug, and none.
##              The setting will determine whether a debug log will be compiled.
##              However, the setting has no effect on the prebuilt library.
##              empty   : All debug logs are compiled.
##              error   : Only error logs are compiled.
##              warning : Only warning and error logs are compiled.
##              info    : Only info, warning, and error logs are compiled.
##              debug   : All debug logs are compiled.
##              none    : All debugs are disabled.
## Path:        kernel/service
## Dependency:  None
## Notice:      None
## Realted doc: Please refer to doc/Airoha_IoT_SDK_for_BT_Audio_155x_API_Reference_Manual.html
##
ifeq ($(MTK_DEBUG_LEVEL),)
CCFLAG += -DMTK_DEBUG_LEVEL_DEBUG
CCFLAG += -DMTK_DEBUG_LEVEL_INFO
CCFLAG += -DMTK_DEBUG_LEVEL_WARNING
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),error)
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),warning)
CCFLAG += -DMTK_DEBUG_LEVEL_WARNING
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),info)
CCFLAG += -DMTK_DEBUG_LEVEL_INFO
CCFLAG += -DMTK_DEBUG_LEVEL_WARNING
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),debug)
CCFLAG += -DMTK_DEBUG_LEVEL_DEBUG
CCFLAG += -DMTK_DEBUG_LEVEL_INFO
CCFLAG += -DMTK_DEBUG_LEVEL_WARNING
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),printf)
CCFLAG += -DMTK_DEBUG_LEVEL_PRINTF
endif
ifeq ($(MTK_DEBUG_LEVEL),none)
CCFLAG += -DMTK_DEBUG_LEVEL_NONE
endif

##
## MTK_SAVE_LOG_TO_FLASH_ENABLE
## Brief:       This option is used to enable log saving to flash feature.
## Usage:       Enable the feature by configuring it as y.
##              y : save runtime log content to flash
##              n : transfer runtime log over logging port
## Path:        kernel/service/syslog
## Dependency:  flash driver
## Notice:      Reserve flash blocks to store runtime log
## Related doc :None
##
ifeq ($(MTK_SAVE_LOG_TO_FLASH_ENABLE),y)
CCFLAG += -DMTK_SAVE_LOG_TO_FLASH_ENABLE
endif
##

##
## MTK_I2S_SLAVE_ENABLE
## Brief:       This option is to open i2s slave driver.
## Usage:       Enable the i2s slave driver by configuring it as y.
##              y : open i2s slave driver.
##              n : not open i2s slave driver.
## Path:        middleware/MTK/dspfw/port/chip/ab155x/src/dsp_lower_layer/dsp_drv
## Dependency:  None
## Notice:      None
## Realted doc: None
##
ifeq ($(MTK_I2S_SLAVE_ENABLE),y)
CCFLAG += -DMTK_I2S_SLAVE_ENABLE
endif

##
## MTK_PROMPT_SOUND_ENABLE
## Brief:       This option is to enable prompt sound feature.
## Usage:       Enable enable prompt sound feature by configuring it as y.
##              y : enable prompt sound feature.
##              n : not enable prompt sound feature.
## Path:        middleware\MTK\stream
## Dependency:  None.
## Notice:      None.
## Realted doc: None.
##
ifeq ($(MTK_PROMPT_SOUND_ENABLE),y)
CCFLAG += -DMTK_PROMPT_SOUND_ENABLE
endif


##
## PRELOADER_ENABLE_
## Brief:       This option is to enable and disable preload pisplit features(dynamic to load PIC libraries)
## Usage:       If the value is "y", the PRELOADER_ENABLE compile option will be defined. You must also include the kernel/service/pre_libloader/dsp0/module.mk in your Makefile before setting the option to "y".
## Path:        kernel/service/pre_libloader
## Dependency:  None
## Notice:      None
## Relative doc:None
##
ifeq ($(PRELOADER_ENABLE),y)
CCFLAG += -DPRELOADER_ENABLE
ASFLAG += -DPRELOADER_ENABLE
endif

##
## DSP0_PISPLIT_DEMO_LIBRARY
## Brief:       This option is to enable and disable the demo of DSP0 PIC library
## Usage:       If the value is "y", the DSP0_PISPLIT_DEMO_LIBRARY compile option will be defined. This is a sub-feature option of PRELOADER_ENABLE.
## Path:        kernel/service/pre_libloader/dsp0/dsp0_pic_demo_portable
## Dependency:  PRELOADER_ENABLE
## Notice:      None
## Relative doc:None
##
ifeq ($(PRELOADER_ENABLE),y)
ifeq ($(DSP0_PISPLIT_DEMO_LIBRARY),y)
CCFLAG += -DDSP0_PISPLIT_DEMO_LIBRARY
endif
endif

##
## DSP1_PISPLIT_DEMO_LIBRARY
## Brief:       This option is to enable and disable the demo of DSP1 PIC library
## Usage:       If the value is "y", the DSP0_PISPLIT_DEMO_LIBRARY compile option will be defined. This is a sub-feature option of PRELOADER_ENABLE.
## Path:        kernel/service/pre_libloader/dsp1/dsp1_pic_demo_portable
## Dependency:  PRELOADER_ENABLE
## Notice:      None
## Relative doc:None
##
ifeq ($(PRELOADER_ENABLE),y)
ifeq ($(DSP1_PISPLIT_DEMO_LIBRARY),y)
CCFLAG += -DDSP1_PISPLIT_DEMO_LIBRARY
endif
endif

##
## PRELOADER_ENABLE_DSP0_LOAD_FOR_DSP1
## Brief:       This option is to enable and disable dsp0 help dsp1 to load dsp1 library
## Usage:       If the value is "y", the PRELOADER_ENABLE_DSP0_LOAD_FOR_DSP1 compile option will be defined. This is a sub-feature option of PRELOADER_ENABLE.
## Path:        kernel/service/pre_libloader/dsp0/dsp0_load_dsp1_pic_demo_portable
## Dependency:  PRELOADER_ENABLE
## Notice:      None
## Relative doc:None
##
ifeq ($(PRELOADER_ENABLE),y)
ifeq ($(PRELOADER_ENABLE_DSP0_LOAD_FOR_DSP1),y)
CCFLAG += -DPRELOADER_ENABLE_DSP0_LOAD_FOR_DSP1
ASFLAG += -DPRELOADER_ENABLE_DSP0_LOAD_FOR_DSP1
endif
endif
###############################################################################
##
## The following makefile options are not configurable or only for internal user. They may be removed in the future. 

##
## MTK_CPU_NUMBER_1
## Brief:       Internal use.
##
ifeq ($(MTK_CPU_NUMBER_1),y)
CCFLAG += -DMTK_CPU_NUMBER_1
CCFLAG += -DMTK_MAX_CPU_NUMBER_4
endif

##
## MTK_SWLA_ENABLE
## Brief:       Internal use.
## Brief:       This option is to enable and disable the Software Logical Analyzer service, Each event(task/isr activity) is recorded while CPU context switching, also support customization tag
## Usage:       If the value is "y", the MTK_SWLA_ENABLE compile option will be defined. You must also include the gva\kernel\service\module.mk in your Makefile before setting the option to "y".
## Path:        kernel/service
## Dependency:  None
## Notice:      None
## Relative doc:None
##
ifeq ($(MTK_SWLA_ENABLE),y)
CCFLAG += -DMTK_SWLA_ENABLE
CCFLAG += -DPRODUCT_VERSION_STR=\"$(PRODUCT_VERSION)\"
endif
##

##
## MTK_SUPPORT_HEAP_DEBUG
## Brief:       Internal use.
## MTK_SUPPORT_HEAP_DEBUG is a option to show heap status (alocatted or free),
## It's for RD internal development and debug. Default should be disabled.
##
ifeq ($(MTK_SUPPORT_HEAP_DEBUG),y)
CCFLAG += -DMTK_SUPPORT_HEAP_DEBUG
endif

##
## MTK_HEAP_SIZE_GUARD_ENABLE
## Brief:       Internal use.
## MTK_HEAP_SIZE_GUARD_ENABLE is a option to profiling heap usage,
## It's for RD internal development and debug. Default should be disabled.
##
ifeq ($(MTK_HEAP_SIZE_GUARD_ENABLE),y)
LDFLAG  += -wrap=pvPortMalloc -wrap=vPortFree
CCFLAG  += -DMTK_HEAP_SIZE_GUARD_ENABLE
endif

##
## MTK_DSP_AUDIO_MESSAGE_ENABLE
## Brief:       Internal use.
## Notice:      MTK_DSP_AUDIO_MESSAGE_ENABLE is a option to enable audio message ISR handler. Default must be enabled.
##
ifeq ($(MTK_DSP_AUDIO_MESSAGE_ENABLE),y)
CCFLAG += -DMTK_DSP_AUDIO_MESSAGE_ENABLE
endif

##
## MTK_CM4_PLAYBACK_ENABLE
## Brief:       Internal use.
## Notice:      MTK_CM4_PLAYBACK_ENABLE is a option to support CM4 playback function.
##
ifeq ($(MTK_CM4_PLAYBACK_ENABLE),y)
CCFLAG += -DMTK_CM4_PLAYBACK_ENABLE
endif

##
## MTK_CM4_RECORD_ENABLE
## Brief:       Internal use.
## Notice:      MTK_CM4_RECORD_ENABLE is a option to support CM4 record function.
##
ifeq ($(MTK_CM4_RECORD_ENABLE),y)
CCFLAG += -DMTK_CM4_RECORD_ENABLE
endif

##
## MTK_ANC_ENABLE
## Brief:       Internal use.
## Notice:      MTK_ANC_ENABLE is a option to support ANC function.
##
ifeq ($(MTK_ANC_ENABLE),y)
CCFLAG += -DMTK_ANC_ENABLE
endif

##
## MTK_INEAR_ENHANCEMENT
## Brief:       Internal use.
## Notice:      MTK_INEAR_ENHANCEMENT is a option to support INEAR function.
##
ifeq ($(MTK_INEAR_ENHANCEMENT),y)
CCFLAG += -DMTK_INEAR_ENHANCEMENT
endif

##
## MTK_DUALMIC_INEAR
## Brief:       Internal use.
## Notice:      MTK_DUALMIC_INEAR is a option to support 2+1NR function.
##
ifeq ($(MTK_DUALMIC_INEAR),y)
CCFLAG += -DMTK_DUALMIC_INEAR
endif

##
## MTK_PEQ_ENABLE
## Brief:       Internal use.
## Notice:      MTK_PEQ_ENABLE is a option to support PEQ function.
##
ifeq ($(MTK_PEQ_ENABLE),y)
CCFLAG += -DMTK_PEQ_ENABLE
endif

##
## MTK_LINEIN_PEQ_ENABLE
## Brief:       Internal use.
## Notice:      MTK_LINEIN_PEQ_ENABLE is a option to support LINEIN PEQ function.
##
ifeq ($(MTK_LINEIN_PEQ_ENABLE),y)
CCFLAG += -DMTK_LINEIN_PEQ_ENABLE
endif

##
## MTK_LINEIN_INS_ENABLE
## Brief:       Internal use.
## Notice:      MTK_LINEIN_INS_ENABLE is a option to support LINEIN INS function.
##
ifeq ($(MTK_LINEIN_INS_ENABLE),y)
CCFLAG += -DMTK_LINEIN_INS_ENABLE
endif

##
## MTK_DEQ_ENABLE
## Brief:       Internal use.
## Notice:      MTK_DEQ_ENABLE is a option to support DEQ function for hybrid ANC. Dependency: MTK_PEQ_ENABLE
##
ifeq ($(MTK_PEQ_ENABLE),y)
ifeq ($(MTK_DEQ_ENABLE),y)
CCFLAG += -DMTK_DEQ_ENABLE
endif
endif

##
## MTK_BT_PEQ_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_PEQ_USE_PIC is a option to use PEQ PIC. If MTK_PEQ_ENABLE is enabled, this one should be enabled too.
##
ifeq ($(MTK_BT_PEQ_USE_PIC),y)
CCFLAG += -DMTK_BT_PEQ_USE_PIC
endif

##
## MTK_BT_A2DP_CPD_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_CPD_USE_PIC is a option to use CPD PIC. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_CPD_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_CPD_USE_PIC
endif

ifeq ($(MTK_BT_A2DP_AAC_INHOUSE_ENABLE),y)
CCFLAG += -DMTK_BT_A2DP_AAC_INHOUSE_ENABLE 
endif

##
## MTK_BT_A2DP_AAC_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_AAC_USE_PIC is a option to use AAC PIC. If MTK_BT_A2DP_AAC_ENABLE is enabled, this one should be enabled too.
##
ifeq ($(MTK_BT_A2DP_AAC_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_AAC_USE_PIC
endif

##
## MTK_BT_A2DP_SBC_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_SBC_USE_PIC is a option to use SBC PIC. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_SBC_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_SBC_USE_PIC
endif

##
## MTK_BT_A2DP_MSBC_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_MSBC_USE_PIC is a option to use mSBC PIC. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_MSBC_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_MSBC_USE_PIC
endif

##
## MTK_BT_A2DP_CVSD_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_CVSD_USE_PIC is a option to use CVSD PIC. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_CVSD_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_CVSD_USE_PIC
endif

##
## MTK_BT_CLK_SKEW_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_CLK_SKEW_USE_PIC is a option to use clock skew PIC. Default should be enabled.
##
ifeq ($(MTK_BT_CLK_SKEW_USE_PIC),y)
CCFLAG += -DMTK_BT_CLK_SKEW_USE_PIC
endif

##
## MTK_BT_A2DP_VENDOR_ENABLE
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_VENDOR_ENABLE is a option to use Vendor codec & Vendor codec PIC. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_VENDOR_ENABLE),y)
CCFLAG += -DMTK_BT_A2DP_VENDOR_ENABLE
ifeq ($(MTK_BT_A2DP_VENDOR_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_VENDOR_USE_PIC
endif
ifeq ($(MTK_BT_A2DP_VENDOR_BC_ENABLE),y)
CCFLAG += -DMTK_BT_A2DP_VENDOR_BC_ENABLE
endif
endif

ifeq ($(MTK_BT_A2DP_VENDOR_1_ENABLE),y)
CCFLAG += -DMTK_BT_A2DP_VENDOR_1_ENABLE
ifeq ($(MTK_BT_A2DP_VENDOR_1_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_VENDOR_1_USE_PIC
endif
endif

ifeq ($(MTK_BT_A2DP_VENDOR_2_ENABLE),y)
CCFLAG += -DMTK_BT_A2DP_VENDOR_2_ENABLE
endif

ifeq ($(MTK_BT_A2DP_VENDOR_3_ENABLE),y)
CCFLAG += -DMTK_BT_A2DP_VENDOR_3_ENABLE
endif

##
## MTK_PLC_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_PLC_USE_PIC is a option to use Packet Lost Compensation(PLC) PIC. Default should be enabled.
##
ifeq ($(MTK_PLC_USE_PIC),y)
CCFLAG += -DMTK_PLC_USE_PIC
endif

##
## MTK_INHOUSE_ECNR_ENABLE
## Brief:       Internal use.
## Notice:      MTK_INHOUSE_ECNR_ENABLE is a option to use inhouse ECNR(Echo Cancellation / Noice Reduction) library. Default should be enabled.
##
ifeq ($(MTK_INHOUSE_ECNR_ENABLE),y)
CCFLAG += -DMTK_INHOUSE_ECNR_ENABLE
endif

##
## MTK_BT_A2DP_ECNR_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_ECNR_USE_PIC is a option to use ECNR(Echo Cancellation / Noice Reduction) PIC. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_ECNR_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_ECNR_USE_PIC
endif

##
## MTK_AUDIO_DUMP_BY_CONFIGTOOL
## Brief:       Internal use.
## Notice:      MTK_AUDIO_DUMP_BY_CONFIGTOOL is a option to choose that whether the dump path will be selected by Config Tool(y) or by Coding(n). Default should be Config Tool(y).
##
ifeq ($(MTK_AUDIO_DUMP_BY_CONFIGTOOL),y)
CCFLAG += -DMTK_AUDIO_DUMP_BY_CONFIGTOOL
endif

##
## MTK_AIRDUMP_EN
## Brief:       Internal use.
## Notice:      MTK_AIRDUMP_EN is a option to to support AirDump function.
ifeq ($(MTK_AIRDUMP_EN),y)
CCFLAG += -DMTK_AIRDUMP_EN
endif

##
## MTK_PCDC_TIMEOUT_ENABLE
## Brief:       Internal use.
## Notice:      MTK_PCDC_TIMEOUT_ENABLE is a option for Speaker WPC mode to set PCDC connection timeout mechnism.
ifeq ($(MTK_PCDC_TIMEOUT_ENABLE),y)
CCFLAG += -DMTK_PCDC_TIMEOUT_ENABLE
endif

##
## MTK_AUDIO_SUPPORT_MULTIPLE_MICROPHONE
## Brief:       Internal use.
## Notice:      MTK_AUDIO_SUPPORT_MULTIPLE_MICROPHONE is a option to to support multiple microphone.
##
ifeq ($(MTK_AUDIO_SUPPORT_MULTIPLE_MICROPHONE),y)
CCFLAG += -DHAL_AUDIO_SUPPORT_MULTIPLE_MICROPHONE
CCFLAG += -DENABLE_2A2D_TEST
endif

###############################################################################


ifneq ($(MTK_LOWPOWER_LEVEL),)
CCFLAG += -DMTK_LOWPOWER_LEVEL=$(MTK_LOWPOWER_LEVEL)
endif

ifeq ($(ENABLE_HWSRC_ON_MAIN_STREAM),y)
CCFLAG += -DENABLE_HWSRC_ON_MAIN_STREAM
endif

ifeq ($(ENABLE_AMP_TIMER),y)
CCFLAG += -DENABLE_AMP_TIMER
endif

ifeq ($(ENABLE_SIDETONE_RAMP_TIMER),y)
CCFLAG += -DENABLE_SIDETONE_RAMP_TIMER
endif

ifeq ($(MTK_RECORD_OPUS_ENABLE),y)
CCFLAG += -DMTK_RECORD_OPUS_ENABLE
endif

ifeq ($(MTK_AUDIO_PLC_ENABLE),y)
CCFLAG += -DMTK_AUDIO_PLC_ENABLE
endif

ifeq ($(MTK_DSP1_DRAM_FOR_DSP0_POOL_ENABLE),y)
CCFLAG += -DMTK_DSP1_DRAM_FOR_DSP0_POOL_ENABLE
endif

ifeq ($(MTK_HWSRC_IN_STREAM),y)
CCFLAG += -DMTK_HWSRC_IN_STREAM
endif

ifeq ($(MTK_LEAKAGE_DETECTION_ENABLE),y)
CCFLAG += -DMTK_LEAKAGE_DETECTION_ENABLE
endif

ifeq ($(MTK_USER_TRIGGER_FF_ENABLE),y)
CCFLAG += -DMTK_USER_TRIGGER_FF_ENABLE
endif
