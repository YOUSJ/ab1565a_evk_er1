XTENSA_CORE               = dsp1_core_winabi_xtensac
PRODUCT_VERSION           = 1552
MTK_SYSLOG_VERSION_2     ?= y
MTK_SYSLOG_SUB_FEATURE_STRING_LOG_SUPPORT = y
MTK_SYSLOG_SUB_FEATURE_BINARY_LOG_SUPPORT = y
MTK_SYSLOG_SUB_FEATURE_OFFLINE_DUMP_ACTIVE_MODE = n
MTK_CPU_NUMBER_2                      ?= y
MTK_CLIB_PRINTF_ENABLE    = y
FPGA_ENV                 ?= n

ifeq ($(IC_CONFIG),ab155x)
COM_CFLAGS += -DAB155X
endif

ifeq ($(IC_TYPE),ab1552)
CCFLAG += -DAB1552
ASFLAG += -DAB1552
endif

ifeq ($(IC_TYPE),ab1555)
CCFLAG += -DAB1555
ASFLAG += -DAB1555
endif

ifeq ($(IC_TYPE),ab1556)
CCFLAG += -DAB1556
ASFLAG += -DAB1556
endif

ifeq ($(IC_TYPE),ab1558)
CCFLAG += -DAB1558
ASFLAG += -DAB1558
endif

CCFLAG += -DPRODUCT_VERSION=$(PRODUCT_VERSION)
CCFLAG += -DCORE_DSP1
ASFLAG += -DCORE_DSP1
CCFLAG += -D$(TARGET)_BOOTING
ifeq ($(FPGA_ENV),y)
CCFLAG += -DFPGA_ENV
endif

# Link and Compile Configuration

#CCFLAG      :=  
CCFLAG      +=  -g
CCFLAG      +=  -W
CCFLAG      +=  -Wall
CCFLAG      +=  -O2
CCFLAG      +=  -INLINE:requested
CCFLAG      +=  -mlongcalls
CCFLAG      +=  -std=gnu99
CCFLAG      +=  -ffunction-sections -fdata-sections

#ASFLAG      :=  
ASFLAG      +=  -W
ASFLAG      +=  -Wall
ASFLAG      +=  -g
ASFLAG      +=  -mlongcalls

#LDFLAG      :=  
LDFLAG      +=  --gc-sections
LDFLAG      +=  --relax
LDFLAG      +=  -wrap=printf
LDFLAG      +=  -wrap=snprintf
LDFLAG      +=  -wrap=vsnprintf
LDFLAG	    += -u _UserExceptionVector
LDFLAG	    += -u _KernelExceptionVector
LDFLAG	    += -u _DoubleExceptionVector


##
## MTK_DEBUG_LEVEL
## Brief:       This option is to configure system log debug level.
## Usage:       The valid values are empty, error, warning, info, debug, and none.
##              The setting will determine whether a debug log will be compiled.
##              However, the setting has no effect on the prebuilt library.
##              empty   : All debug logs are compiled.
##              error   : Only error logs are compiled.
##              warning : Only warning and error logs are compiled.
##              info    : Only info, warning, and error logs are compiled.
##              debug   : All debug logs are compiled.
##              none    : All debugs are disabled.
## Path:        kernel/service
## Dependency:  None
## Notice:      None
## Realted doc: Please refer to doc/Airoha_IoT_SDK_for_BT_Audio_155x_API_Reference_Manual.html
##
ifeq ($(MTK_DEBUG_LEVEL),)
CCFLAG += -DMTK_DEBUG_LEVEL_DEBUG
CCFLAG += -DMTK_DEBUG_LEVEL_INFO
CCFLAG += -DMTK_DEBUG_LEVEL_WARNING
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),error)
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),warning)
CCFLAG += -DMTK_DEBUG_LEVEL_WARNING
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),info)
CCFLAG += -DMTK_DEBUG_LEVEL_INFO
CCFLAG += -DMTK_DEBUG_LEVEL_WARNING
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),debug)
CCFLAG += -DMTK_DEBUG_LEVEL_DEBUG
CCFLAG += -DMTK_DEBUG_LEVEL_INFO
CCFLAG += -DMTK_DEBUG_LEVEL_WARNING
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),printf)
CCFLAG += -DMTK_DEBUG_LEVEL_PRINTF
endif
ifeq ($(MTK_DEBUG_LEVEL),none)
CCFLAG += -DMTK_DEBUG_LEVEL_NONE
endif

##
## MTK_SAVE_LOG_TO_FLASH_ENABLE
## Brief:       This option is used to enable log saving to flash feature.
## Usage:       Enable the feature by configuring it as y.
##              y : save runtime log content to flash
##              n : transfer runtime log over logging port
## Path:        kernel/service/syslog
## Dependency:  flash driver
## Notice:      Reserve flash blocks to store runtime log
## Related doc :None
##
ifeq ($(MTK_SAVE_LOG_TO_FLASH_ENABLE),y)
CCFLAG += -DMTK_SAVE_LOG_TO_FLASH_ENABLE
endif
##


##
## PRELOADER_ENABLE_
## Brief:       This option is to enable and disable preload pisplit features(dynamic to load PIC libraries)
## Usage:       If the value is "y", the PRELOADER_ENABLE compile option will be defined. You must also include the kernel/service/pre_libloader/dsp1/module.mk in your Makefile before setting the option to "y".
## Path:        kernel/service/pre_libloader
## Dependency:  None
## Notice:      None
## Relative doc:None
##
ifeq ($(PRELOADER_ENABLE),y)
CCFLAG += -DPRELOADER_ENABLE
ASFLAG += -DPRELOADER_ENABLE
endif

##
## PRELOADER_ENABLE_DSP0_LOAD_FOR_DSP1
## Brief:       This option is to enable and disable dsp0 help dsp1 to load dsp1 library
## Usage:       If the value is "y", the PRELOADER_ENABLE_DSP0_LOAD_FOR_DSP1 compile option will be defined. This is a sub-feature option of PRELOADER_ENABLE.
## Path:        kernel/service/pre_libloader
## Dependency:  PRELOADER_ENABLE
## Notice:      None
## Relative doc:None
##
ifeq ($(PRELOADER_ENABLE),y)
ifeq ($(PRELOADER_ENABLE_DSP0_LOAD_FOR_DSP1),y)
CCFLAG += -DPRELOADER_ENABLE_DSP0_LOAD_FOR_DSP1
ASFLAG += -DPRELOADER_ENABLE_DSP0_LOAD_FOR_DSP1
endif
endif

##
## DSP1_PISPLIT_DEMO_LIBRARY
## Brief:       This option is to enable and disable the demo of DSP1 PIC library
## Usage:       If the value is "y", the DSP0_PISPLIT_DEMO_LIBRARY compile option will be defined. This is a sub-feature option of PRELOADER_ENABLE.
## Path:        kernel/service/pre_libloader/dsp1/dsp1_pic_demo_portable
## Dependency:  PRELOADER_ENABLE PRELOADER_ENABLE_DSP0_LOAD_FOR_DSP1
## Notice:      None
## Relative doc:None
##
ifeq ($(PRELOADER_ENABLE),y)
ifeq ($(PRELOADER_ENABLE_DSP0_LOAD_FOR_DSP1),y)
ifeq ($(DSP1_PISPLIT_DEMO_LIBRARY),y)
CCFLAG += -DDSP1_PISPLIT_DEMO_LIBRARY
endif
endif
endif

###############################################################################
##
## The following makefile options are not configurable or only for internal user. They may be removed in the future. 

##
## MTK_CPU_NUMBER_2
## Brief:       Internal use.
##
ifeq ($(MTK_CPU_NUMBER_2),y)
CCFLAG += -DMTK_CPU_NUMBER_2
CCFLAG += -DMTK_MAX_CPU_NUMBER_4
endif
###############################################################################

