XTENSA_CORE               = AB1568_i64B_d32B_512K
PRODUCT_VERSION           = 1565
MTK_SYSLOG_VERSION_2     ?= y
MTK_SYSLOG_SUB_FEATURE_STRING_LOG_SUPPORT = y
MTK_SYSLOG_SUB_FEATURE_BINARY_LOG_SUPPORT = y
MTK_SYSLOG_SUB_FEATURE_USB_ACTIVE_MODE = y
MTK_SYSLOG_SUB_FEATURE_OFFLINE_DUMP_ACTIVE_MODE = y
MTK_DEBUG_PLAIN_LOG_ENABLE            = n
MTK_SAVE_LOG_AND_CONTEXT_DUMP_ENABLE  ?= n
MTK_CPU_NUMBER_1                      ?= y
MTK_MUX_ENABLE                        ?= y
FPGA_ENV                 ?= n
IOT_SDK_XTENSA_VERSION                 := 809


ifeq ($(IC_TYPE),ab156x)
CCFLAG += -DAB1565
endif

CCFLAG += -DPRODUCT_VERSION=$(PRODUCT_VERSION)
CCFLAG += -DCORE_DSP0
ASFLAG += -DCORE_DSP0
CCFLAG += -D$(TARGET)_BOOTING
ifeq ($(FPGA_ENV),y)
CCFLAG += -DFPGA_ENV
endif

# Link and Compile Configuration

#CCFLAG      :=  
CCFLAG      +=  -g
CCFLAG      +=  -W
CCFLAG      +=  -Wall
CCFLAG      +=  -O2
CCFLAG      +=  -INLINE:requested
CCFLAG      +=  -mlongcalls
CCFLAG      +=  -std=gnu99
CCFLAG      +=  -ffunction-sections -fdata-sections -mtext-section-literals

#ASFLAG      :=  
ASFLAG      +=  -W
ASFLAG      +=  -Wall
ASFLAG      +=  -g
ASFLAG      +=  -mlongcalls --text-section-literals

#LDFLAG      :=  
LDFLAG      +=  --gc-sections
LDFLAG      +=  --no-relax
LDFLAG      +=  -wrap=printf
LDFLAG	    += -u _UserExceptionVector
LDFLAG	    += -u _KernelExceptionVector
LDFLAG	    += -u _DoubleExceptionVector

##
## MTK_DEBUG_PLAIN_LOG_ENABLE
## Brief:       This option is used to force log display with plain style.
## Usage:       Enable the feature by configuring it as y.
##              y : log display with plain style
##              n : log display with race style, need pc logging tool support
## Path:        kernel/service/syslog
## Dependency:  None
## Notice:      None
## Related doc :None
##
ifeq ($(MTK_DEBUG_PLAIN_LOG_ENABLE),y)
CCFLAG += -DMTK_DEBUG_PLAIN_LOG_ENABLE
endif

##
## MTK_DEBUG_LEVEL
## Brief:       This option is to configure system log debug level.
## Usage:       The valid values are empty, error, warning, info, debug, and none.
##              The setting will determine whether a debug log will be compiled.
##              However, the setting has no effect on the prebuilt library.
##              empty   : All debug logs are compiled.
##              error   : Only error logs are compiled.
##              warning : Only warning and error logs are compiled.
##              info    : Only info, warning, and error logs are compiled.
##              debug   : All debug logs are compiled.
##              none    : All debugs are disabled.
## Path:        kernel/service
## Dependency:  None
## Notice:      None
## Realted doc: Please refer to doc/LinkIt_for_RTOS_System_Log_Developers_Guide.pdf
##
ifeq ($(MTK_DEBUG_LEVEL),)
CCFLAG += -DMTK_DEBUG_LEVEL_DEBUG
CCFLAG += -DMTK_DEBUG_LEVEL_INFO
CCFLAG += -DMTK_DEBUG_LEVEL_WARNING
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),error)
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),warning)
CCFLAG += -DMTK_DEBUG_LEVEL_WARNING
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),info)
CCFLAG += -DMTK_DEBUG_LEVEL_INFO
CCFLAG += -DMTK_DEBUG_LEVEL_WARNING
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),debug)
CCFLAG += -DMTK_DEBUG_LEVEL_DEBUG
CCFLAG += -DMTK_DEBUG_LEVEL_INFO
CCFLAG += -DMTK_DEBUG_LEVEL_WARNING
CCFLAG += -DMTK_DEBUG_LEVEL_ERROR
endif

ifeq ($(MTK_DEBUG_LEVEL),printf)
CCFLAG += -DMTK_DEBUG_LEVEL_PRINTF
endif
ifeq ($(MTK_DEBUG_LEVEL),none)
CCFLAG += -DMTK_DEBUG_LEVEL_NONE
endif

##
## MTK_SAVE_LOG_AND_CONTEXT_DUMP_ENABLE
## Brief:       This option is to enable runtime log and crash context save in flash feature.
## Usage:       Enable the feature by configuring it as y.
##              y : save runtime logging registers and all memory in coredump format
##              n   : no effect
## Path:        kernel/service/src_core
## Dependency:  flash driver
## Notice:      Reserve flash blocks to store runtime log and dumped data
## Related doc :None
##
ifeq ($(MTK_SAVE_LOG_AND_CONTEXT_DUMP_ENABLE),y)
CCFLAG += -DMTK_SAVE_LOG_AND_CONTEXT_DUMP_ENABLE
endif
##

##
## MTK_USB_DEMO_ENABLED
## Brief:       This option is to enable USB device feature.
## Usage:       Enable the feature by configuring it as y.
## Path:        middleware/MTK/usb/
## Dependency:  None
## Notice:      None
## Related doc :None
##
ifeq ($(MTK_USB_DEMO_ENABLED),y)
CCFLAG += -DMTK_USB_DEMO_ENABLED
endif

##
## MTK_PROMPT_SOUND_ENABLE
## Brief:       This option is to enable prompt sound feature.
## Usage:       Enable enable prompt sound feature by configuring it as y.
##              y : enable prompt sound feature.
##              n : not enable prompt sound feature.
## Path:        middleware\MTK\stream
## Dependency:  None.
## Notice:      None.
## Realted doc: None.
##
ifeq ($(MTK_PROMPT_SOUND_ENABLE),y)
CCFLAG += -DMTK_PROMPT_SOUND_ENABLE
endif

##
## MTK_I2S_SLAVE_ENABLE
## Brief:       This option is to open i2s slave driver.
## Usage:       Enable the i2s slave driver by configuring it as y.
##              y : open i2s slave driver.
##              n : not open i2s slave driver.
## Path:        middleware/MTK/dspfw/port/chip/ab156x/src/dsp_lower_layer/dsp_drv
## Dependency:  None
## Notice:      None
## Realted doc: None
##
ifeq ($(MTK_I2S_SLAVE_ENABLE),y)
CCFLAG += -DMTK_I2S_SLAVE_ENABLE
endif

##
## PRELOADER_ENABLE
## Brief:       This option is to enable and disable preload pisplit features(dynamic to load PIC libraries)
## Usage:       If the value is "y", the PRELOADER_ENABLE compile option will be defined. You must also include the kernel/service/pre_libloader/dsp0/module.mk in your Makefile before setting the option to "y".
## Path:        kernel/service/pre_libloader
## Dependency:  None
## Notice:      None
## Relative doc:None
##
ifeq ($(PRELOADER_ENABLE),y)
CCFLAG += -DPRELOADER_ENABLE
ASFLAG += -DPRELOADER_ENABLE

##
## DSP0_PISPLIT_DEMO_LIBRARY
## Brief:       This option is to enable and disable the demo of DSP0 PIC library
## Usage:       If the value is "y", the DSP0_PISPLIT_DEMO_LIBRARY compile option will be defined. This is a sub-feature option of PRELOADER_ENABLE.
## Path:        kernel/service/pre_libloader/dsp0/dsp0_pic_demo_portable
## Dependency:  PRELOADER_ENABLE
## Notice:      None
## Relative doc:None
##
ifeq ($(DSP0_PISPLIT_DEMO_LIBRARY),y)
CCFLAG += -DDSP0_PISPLIT_DEMO_LIBRARY
endif
endif

##
## CCCI_ENABLE
## Brief:       This option is to enable and disable CCCI(Cross Core communication Interface)
## Usage:       If the value is "y", the CCCI_ENABLE compile option will be defined. 
## Path:        kernel/service/ccci
## Dependency:  None
## Notice:      None
## Relative doc:None
##
ifeq ($(CCCI_ENABLE),y)
CCFLAG += -DCCCI_ENABLE
endif


##
## LINE_IN_PURE_FOR_AMIC_CLASS_G_HQA
## Brief:       This option is to enable for Audio HQA verification.
## Usage:       If the value is "y",  the LINE_IN_PURE_FOR_AMIC_CLASS_G_HQA option will be defined.
## Path:        middleware/MTK/audio/
## Dependency:  None
## Notice:      None
## Relative doc:None
##
ifeq ($(LINE_IN_PURE_FOR_AMIC_CLASS_G_HQA),y)
CCFLAG += -DLINE_IN_PURE_FOR_AMIC_CLASS_G_HQA
endif

##
## ANALOG_OUTPUT_CLASSD_ENABLE
## Brief:       This option is to enable for default setting to class-d.
## Usage:       If the value is "y",  the ANALOG_OUTPUT_CLASSD_ENABLE option will be defined.
## Path:        middleware/MTK/audio/
## Dependency:  None
## Notice:      None
## Relative doc:None
##
ifeq ($(ANALOG_OUTPUT_CLASSD_ENABLE),y)
CCFLAG += -DANALOG_OUTPUT_CLASSD_ENABLE
endif

###############################################################################
##
## The following makefile options are not configurable or only for internal user. They may be removed in the future. 

##
## MTK_CPU_NUMBER_1
## Brief:       Internal use.
##
ifeq ($(MTK_CPU_NUMBER_1),y)
CCFLAG += -DMTK_CPU_NUMBER_1
CCFLAG += -DMTK_MAX_CPU_NUMBER_2
endif

##
## MTK_WWE_ENABLE
## Brief:       Internal use.
## Notice:      MTK_WWE_ENABLE is a option to support WWE function.
##
ifeq ($(MTK_WWE_ENABLE),y)
CCFLAG += -DMTK_WWE_ENABLE
endif

##
## MTK_SENSOR_SOURCE_ENABLE
## Brief:       Internal use.
## Notice:      MTK_SENSOR_SOURCE_ENABLE is a option to support Sensor Source.
##
ifeq ($(MTK_SENSOR_SOURCE_ENABLE),y)
CCFLAG += -DMTK_SENSOR_SOURCE_ENABLE
endif

##
## MTK_SUPPORT_HEAP_DEBUG
## Brief:       Internal use.
## MTK_SUPPORT_HEAP_DEBUG is a option to show heap status (alocatted or free),
## It's for RD internal development and debug. Default should be disabled.
##
ifeq ($(MTK_SUPPORT_HEAP_DEBUG),y)
CCFLAG += -DMTK_SUPPORT_HEAP_DEBUG
endif

##
## MTK_HEAP_SIZE_GUARD_ENABLE
## Brief:       Internal use.
## MTK_HEAP_SIZE_GUARD_ENABLE is a option to profiling heap usage,
## It's for RD internal development and debug. Default should be disabled.
##
ifeq ($(MTK_HEAP_SIZE_GUARD_ENABLE),y)
LDFLAG  += -wrap=pvPortMalloc -wrap=vPortFree
CCFLAG  += -DMTK_HEAP_SIZE_GUARD_ENABLE
endif

##
## MTK_SWLA_ENABLE
## Brief:       Internal use.
## Brief:       This option is to enable and disable the Software Logical Analyzer service, Each event(task/isr activity) is recorded while CPU context switching, also support customization tag
## Usage:       If the value is "y", the MTK_SWLA_ENABLE compile option will be defined. You must also include the gva\kernel\service\module.mk in your Makefile before setting the option to "y".
## Path:        kernel/service
## Dependency:  None
## Notice:      None
## Relative doc:None
##
ifeq ($(MTK_SWLA_ENABLE),y)
CCFLAG += -DMTK_SWLA_ENABLE
CCFLAG += -DPRODUCT_VERSION_STR=\"$(PRODUCT_VERSION)\"
endif
##

##
## MTK_AUDIO_SUPPORT_MULTIPLE_MICROPHONE
## Brief:       Internal use.
## Notice:      MTK_AUDIO_SUPPORT_MULTIPLE_MICROPHONE is a option to to support multiple microphone.
##
ifeq ($(MTK_AUDIO_SUPPORT_MULTIPLE_MICROPHONE),y)
CCFLAG += -DHAL_AUDIO_SUPPORT_MULTIPLE_MICROPHONE
CCFLAG += -DENABLE_2A2D_TEST
endif

##
## MTK_AUDIO_DUMP_BY_CONFIGTOOL
## Brief:       Internal use.
## Notice:      MTK_AUDIO_DUMP_BY_CONFIGTOOL is a option to choose that whether the dump path will be selected by Config Tool(y) or by Coding(n). Default should be Config Tool(y).
##
ifeq ($(MTK_AUDIO_DUMP_BY_CONFIGTOOL),y)
CCFLAG += -DMTK_AUDIO_DUMP_BY_CONFIGTOOL
endif

##
## MTK_BT_A2DP_MSBC_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_MSBC_USE_PIC is a option to use mSBC PIC. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_MSBC_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_MSBC_USE_PIC
endif

##
## MTK_BT_A2DP_CVSD_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_CVSD_USE_PIC is a option to use CVSD PIC. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_CVSD_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_CVSD_USE_PIC
endif

##
## MTK_BT_A2DP_ECNR_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_ECNR_USE_PIC is a option to use ECNR(Echo Cancellation / Noice Reduction) PIC. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_ECNR_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_ECNR_USE_PIC
endif

##
## MTK_BT_CLK_SKEW_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_CLK_SKEW_USE_PIC is a option to use clock skew PIC. Default should be enabled.
##
ifeq ($(MTK_BT_CLK_SKEW_USE_PIC),y)
CCFLAG += -DMTK_BT_CLK_SKEW_USE_PIC
endif

##
## MTK_VOICE_AGC_ENABLE
## Brief:       Internal use.
## Notice:      MTK_VOICE_AGC_ENABLE is a option to enable Voice AGC. Default should be enabled.
##
ifeq ($(MTK_VOICE_AGC_ENABLE),y)
CCFLAG += -DMTK_VOICE_AGC_ENABLE
endif


##
## MTK_BT_AGC_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_AGC_USE_PIC is a option to use AGC PIC. If MTK_VOICE_AGC_ENABLE is enabled, this compile option will be enabled.
##
ifeq ($(MTK_BT_AGC_USE_PIC),y)
CCFLAG += -DMTK_BT_AGC_USE_PIC
endif



##
## MTK_PLC_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_PLC_USE_PIC is a option to use Packet Lost Compensation(PLC) PIC. Default should be enabled.
##
ifeq ($(MTK_PLC_USE_PIC),y)
CCFLAG += -DMTK_PLC_USE_PIC
endif

##
## MTK_BT_HFP_SPE_ALG_V2
## Brief:       Internal use.
## Notice:      MTK_BT_HFP_SPE_ALG_V2 is a option for mt2822 which use different algorithm interface (CPD, clk skew). Default should be enabled.
##
ifeq ($(MTK_BT_HFP_SPE_ALG_V2),y)
CCFLAG += -DMTK_BT_HFP_SPE_ALG_V2
endif

##
## ENABLE_HWSRC_CLKSKEW
## Brief:       Internal use.
## Notice:      ENABLE_HWSRC_CLKSKEW is a option for hwsrc clk skew. Default should not be enabled.
##
ifeq ($(ENABLE_HWSRC_CLKSKEW),y)
CCFLAG += -DENABLE_HWSRC_CLKSKEW
endif

##
## MTK_BT_HFP_FORWARDER_ENABLE
## Brief:       Internal use.
## Notice:      MTK_BT_HFP_FORWARDER_ENABLE is a option for mt2822 HFP which have audio forwarder. Default should be enabled.
##
ifeq ($(MTK_BT_HFP_FORWARDER_ENABLE),y)
CCFLAG += -DMTK_BT_HFP_FORWARDER_ENABLE
endif


##
## MTK_DSP_AUDIO_MESSAGE_ENABLE
## Brief:       Internal use.
## Notice:      MTK_DSP_AUDIO_MESSAGE_ENABLE is a option to enable audio message ISR handler. Default must be enabled.
##
ifeq ($(MTK_DSP_AUDIO_MESSAGE_ENABLE),y)
CCFLAG += -DMTK_DSP_AUDIO_MESSAGE_ENABLE
endif

##
## MTK_CM4_PLAYBACK_ENABLE
## Brief:       Internal use.
## Notice:      MTK_CM4_PLAYBACK_ENABLE is a option to support CM4 playback function.
##
ifeq ($(MTK_CM4_PLAYBACK_ENABLE),y)
CCFLAG += -DMTK_CM4_PLAYBACK_ENABLE
endif

##
## MTK_CM4_RECORD_ENABLE
## Brief:       Internal use.
## Notice:      MTK_CM4_RECORD_ENABLE is a option to support CM4 record function.
##
ifeq ($(MTK_CM4_RECORD_ENABLE),y)
CCFLAG += -DMTK_CM4_RECORD_ENABLE
endif

##
## MTK_ANC_ENABLE
## Brief:       Internal use.
## Notice:      MTK_ANC_ENABLE is a option to support ANC function.
##
ifeq ($(MTK_ANC_ENABLE),y)
CCFLAG += -DMTK_ANC_ENABLE
endif

##
## MTK_PEQ_ENABLE
## Brief:       Internal use.
## Notice:      MTK_PEQ_ENABLE is a option to support PEQ function.
##
ifeq ($(MTK_PEQ_ENABLE),y)
CCFLAG += -DMTK_PEQ_ENABLE
endif

##
## MTK_BT_A2DP_AAC_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_AAC_USE_PIC is a option to use AAC PIC. If MTK_BT_A2DP_AAC_ENABLE is enabled, this one should be enabled too.
##
ifeq ($(MTK_BT_A2DP_AAC_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_AAC_USE_PIC 
endif

##
## MTK_BT_A2DP_SBC_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_SBC_USE_PIC is a option to use SBC PIC. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_SBC_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_SBC_USE_PIC
endif

##
## MTK_BT_A2DP_CPD_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_CPD_USE_PIC is a option to use CPD PIC. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_CPD_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_CPD_USE_PIC
endif

##
## MTK_BT_PEQ_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_PEQ_USE_PIC is a option to use PEQ PIC. If MTK_PEQ_ENABLE is enabled, this one should be enabled too.
##
ifeq ($(MTK_BT_PEQ_USE_PIC),y)
CCFLAG += -DMTK_BT_PEQ_USE_PIC
endif

##
## MTK_BT_A2DP_VENDOR_ENABLE
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_VENDOR_ENABLE is an option to use Vendor codec & Vendor codec PIC. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_VENDOR_ENABLE),y)
CCFLAG += -DMTK_BT_A2DP_VENDOR_ENABLE
##
## MTK_BT_A2DP_VENDOR_USE_PIC
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_VENDOR_USE_PIC is an option to use Vendor codec PIC. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_VENDOR_USE_PIC),y)
CCFLAG += -DMTK_BT_A2DP_VENDOR_USE_PIC
endif
##
## MTK_BT_A2DP_VENDOR_BC_ENABLE
## Brief:       Internal use.
## Notice:      MTK_BT_A2DP_VENDOR_BC_ENABLE is an option to apply buffer control to the Vendor codec. Default should be enabled.
##
ifeq ($(MTK_BT_A2DP_VENDOR_BC_ENABLE),y)
CCFLAG += -DMTK_BT_A2DP_VENDOR_BC_ENABLE
endif
endif

##
## MTK_INEAR_ENHANCEMENT
## Brief:       Internal use.
## Notice:      MTK_INEAR_ENHANCEMENT is a option to support INEAR function.
##
ifeq ($(MTK_INEAR_ENHANCEMENT),y)
CCFLAG += -DMTK_INEAR_ENHANCEMENT
endif

##
## MTK_DUALMIC_INEAR
## Brief:       Internal use.
## Notice:      MTK_DUALMIC_INEAR is a option to support 2+1NR function.
##
ifeq ($(MTK_DUALMIC_INEAR),y)
CCFLAG += -DMTK_DUALMIC_INEAR
endif

##
## MTK_3RD_PARTY_NR
## Brief:       Internal use.
## Notice:      MTK_3RD_PARTY_ECNR is a option to support 3rd party ECNR function.
##
ifeq ($(MTK_3RD_PARTY_NR),y)
CCFLAG += -DMTK_3RD_PARTY_NR
endif

## MTK_MULTI_MIC_STREAM_ENABLE
## Brief:       Internal use.
## Notice:      MTK_MULTI_MIC_STREAM_ENABLE is a option to concurrently use AFE source. 
##
ifeq ($(MTK_MULTI_MIC_STREAM_ENABLE),y)
CCFLAG += -DMTK_MULTI_MIC_STREAM_ENABLE
endif

##
## MTK_AIRDUMP_EN
## Brief:       Internal use.
## Notice:      MTK_AIRDUMP_EN is a option to to support AirDump function.
ifeq ($(MTK_AIRDUMP_EN),y)
CCFLAG += -DMTK_AIRDUMP_EN
endif

###############################################################################


ifneq ($(MTK_LOWPOWER_LEVEL),)
CCFLAG += -DMTK_LOWPOWER_LEVEL=$(MTK_LOWPOWER_LEVEL)
endif

ifeq ($(ENABLE_HWSRC_ON_MAIN_STREAM),y)
CCFLAG += -DENABLE_HWSRC_ON_MAIN_STREAM
endif

ifeq ($(MTK_HWSRC_IN_STREAM),y)
CCFLAG += -DMTK_HWSRC_IN_STREAM
endif

ifeq ($(ENABLE_SIDETONE_RAMP_TIMER),y)
CCFLAG += -DENABLE_SIDETONE_RAMP_TIMER
endif

ifeq ($(ENABLE_FRAMEWORK_MULTIPLE_CHANNEL),y)
CCFLAG += -DENABLE_FRAMEWORK_MULTIPLE_CHANNEL
endif

ifeq ($(ENABLE_AUDIO_WITH_JOINT_MIC),y)
CCFLAG += -DENABLE_AUDIO_WITH_JOINT_MIC
endif

ifeq ($(MTK_DSP1_DRAM_FOR_DSP0_POOL_ENABLE),y)
CCFLAG += -DMTK_DSP1_DRAM_FOR_DSP0_POOL_ENABLE
endif

ifeq ($(AB1568_BRING_UP_DSP_DEFAULT_HW_LOOPBACK),y)
CCFLAG += -DAB1568_BRING_UP_DSP_DEFAULT_HW_LOOPBACK
endif

ifeq ($(MTK_BT_AVM_SHARE_BUF),y)
CCFLAG += -DMTK_BT_AVM_SHARE_BUF
endif

ifeq ($(MTK_AUDIO_PLC_ENABLE),y)
CCFLAG += -DMTK_AUDIO_PLC_ENABLE
endif

ifeq ($(MTK_SLT_AUDIO_HW),y)
CCFLAG += -DMTK_SLT_AUDIO_HW
endif

