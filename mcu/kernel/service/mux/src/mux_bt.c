/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO ObtAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "hal_platform.h"
#include "hal_nvic.h"

#include "mux.h"
#include "mux_port_device.h"
#include "serial_port.h"
#include "assert.h"
#include "mux_port.h"

#ifdef MTK_MUX_BT_ENABLE

#define BT_GPT_TIMEOUT 1
#define BT_PORT_INDEX_TO_MUX_PORT(port_index) (port_index + MUX_BT_BEGIN)

mux_irq_handler_t g_mux_bt_callback;
virtual_read_write_point_t g_mux_bt_r_w_point[MUX_BT_END - MUX_BT_BEGIN +1];
serial_port_handle_t serial_port_bt_handle[MUX_BT_END - MUX_BT_BEGIN +1]; //MUX_SPP MUX_BLE MUX_AIRUPDATE

#include "FreeRTOS.h"
#include "semphr.h"

SemaphoreHandle_t x_mux_bt_Semaphore;
static uint32_t g_mux_bt_phase2_send_status;
static uint32_t g_bt_gpt_handler;

log_create_module(MUX_BT, PRINT_LEVEL_INFO);

static void port_mux_bt_set_rx_hw_wptr_internal_use(uint8_t port_index, uint32_t move_bytes);
static void port_mux_bt_set_tx_hw_rptr_internal_use(uint8_t port_index, uint32_t move_bytes);
void port_mux_bt_phase2_send(uint8_t port_index);

void bt_send_data(uint8_t port_index, uint32_t addr, uint32_t len, volatile uint32_t *sending_point)
{
    serial_port_status_t status;
    serial_port_write_data_t send_data;
    uint32_t per_cpu_irq_mask;

    if(len == 0) {
        return;
    }

    send_data.data = (uint8_t *)addr;
    send_data.size = len;

    /*Must be  set the value of tx_sending_read_point firstly!!!!!must before call bt send!!!!!*/
    // *sending_point = addr + send_data.size;

    status = serial_port_control(serial_port_bt_handle[port_index], SERIAL_PORT_CMD_WRITE_DATA, (serial_port_ctrl_para_t *)&send_data);
    if(status != SERIAL_PORT_STATUS_OK) {
        LOG_MSGID_W(MUX_BT, "port_mux_bt_write_data, status:%d", 1, status);
    }
    // Manually update tx read pointer, because no SERIAL_PORT_EVENT_READY_TO_WRITE event received.
    port_mux_local_cpu_enter_critical(&per_cpu_irq_mask);
    port_mux_bt_set_tx_hw_rptr_internal_use(port_index, len);
    port_mux_local_cpu_exit_critical(per_cpu_irq_mask);

    if(send_data.ret_size != send_data.size) {
        LOG_MSGID_W(MUX_BT, "send error, ret = %d", 1, send_data.ret_size);
    } else {
        LOG_MSGID_I(MUX_BT, "send ok, ret = %d", 1, send_data.ret_size);
    }
}

static void mux_bt_callback(serial_port_dev_t device, serial_port_callback_event_t event, void *parameter)
{
    uint8_t port_index = device - SERIAL_PORT_DEV_BT_TYPE_BEGIN;
    virtual_read_write_point_t *p = &g_mux_bt_r_w_point[port_index];
    uint32_t next_free_block_len;
    serial_port_read_data_t read_data;
    serial_port_status_t status;

    switch(event) {
        case SERIAL_PORT_EVENT_READY_TO_WRITE:
            /* Note: There is no need to write a callback function, because the port fails to send the packet and is directly dropped. ignore this event */
            // if(p->tx_send_is_running != MUX_DEVICE_HW_RUNNING){
            //     return;
            // }

            // //transfer done , then update tx_buff_read_point
            // //port_mux_bt_set_tx_hw_rptr_internal_use(port_index, p->tx_sending_read_point - p->tx_buff_read_point);

            // g_mux_bt_callback(BT_PORT_INDEX_TO_MUX_PORT(port_index), MUX_EVENT_READY_TO_WRITE);
            // //Judge Tx buffer whether have data need to send

            // port_mux_local_cpu_enter_critical(&per_cpu_irq_mask);
            // next_available_block_len = mux_common_device_get_buf_next_available_block_len(p->tx_buff_start, p->tx_buff_read_point, p->tx_buff_write_point, p->tx_buff_end, p->tx_buff_available_len);
            // if(next_available_block_len == 0) {
            //     p->tx_send_is_running = MUX_DEVICE_HW_IDLE;//change to idle
            //     port_mux_local_cpu_exit_critical(per_cpu_irq_mask);
            // } else {
            //     p->tx_send_is_running = MUX_DEVICE_HW_RUNNING;//keep running
            //     //p->tx_sending_read_point = p->tx_buff_read_point + next_available_block_len;
            //     port_mux_local_cpu_exit_critical(per_cpu_irq_mask);
            //     bt_send_data(port_index, p->tx_buff_read_point, next_available_block_len, &p->tx_sending_read_point);
            // }
            break;

        case SERIAL_PORT_EVENT_READY_TO_READ:
#if 1
            read_data.buffer = (uint8_t *)port_mux_malloc(p->rx_buff_len);
            read_data.size = p->rx_buff_len;

            status = serial_port_control(serial_port_bt_handle[port_index], SERIAL_PORT_CMD_READ_DATA, (serial_port_ctrl_para_t *)&read_data);
            if(status != SERIAL_PORT_STATUS_OK) {
                port_mux_free(read_data.buffer);
                LOG_MSGID_E(MUX_BT, "bt rx port control read data error status[%d] handle[%d]", 2, status, serial_port_bt_handle[port_index]);
                return;
            }

            if(read_data.ret_size > (p->rx_buff_end - p->rx_buff_start - p->rx_buff_available_len)) {
                port_mux_free(read_data.buffer);
                LOG_MSGID_E(MUX_BT, "bt rx buffer not enough to save, len:%d", 1, read_data.ret_size);
                return;
            }

            // Rx buffer have some space to do receive.
            next_free_block_len = mux_common_device_get_buf_next_free_block_len(p->rx_buff_start, p->rx_buff_read_point, p->rx_buff_write_point, p->rx_buff_end, p->rx_buff_available_len);
            //printf("mux_bt_callback() next_free_block_len: %d read_data.ret_size:%d\r\n",(int)next_free_block_len,(int)read_data.ret_size);

            if(next_free_block_len >= read_data.ret_size){
                memcpy((void*)(p->rx_buff_write_point), read_data.buffer, read_data.ret_size);
            }
            else{
                memcpy((void*)(p->rx_buff_write_point), read_data.buffer, next_free_block_len);
                memcpy((void*)(p->rx_buff_start), read_data.buffer+next_free_block_len, read_data.ret_size - next_free_block_len);
            }
            port_mux_bt_set_rx_hw_wptr_internal_use(port_index, read_data.ret_size);
            port_mux_free(read_data.buffer);
#else
            // Rx buffer have some space to do receive.
            next_free_block_len = mux_common_device_get_buf_next_free_block_len(p->rx_buff_start,p->rx_buff_read_point,p->rx_buff_write_point,p->rx_buff_end,p->rx_buff_available_len);
            read_data.buffer = (uint8_t *)p->rx_buff_write_point;
            read_data.size = next_free_block_len;

            status = serial_port_control(serial_port_bt_handle[port_index], SERIAL_PORT_CMD_READ_DATA, (serial_port_ctrl_para_t *)&read_data);
            if(status != SERIAL_PORT_STATUS_OK)
                assert(0);
            printf("Liming: next_free_block_len:%d read_data.ret_size:%d\r\n",(int)next_free_block_len,(int)read_data.ret_size);

            port_mux_bt_set_rx_hw_wptr_internal_use(port_index,read_data.ret_size);
#endif
            g_mux_bt_callback(BT_PORT_INDEX_TO_MUX_PORT(port_index), MUX_EVENT_READY_TO_READ);

            break;

        case SERIAL_PORT_EVENT_BT_CONNECTION:
            LOG_MSGID_I(MUX_BT, "event: SERIAL_PORT_EVENT_BT_CONNECTION ", 0);
            g_mux_bt_callback(BT_PORT_INDEX_TO_MUX_PORT(port_index),MUX_EVENT_CONNECTION);
            break;

        case SERIAL_PORT_EVENT_BT_DISCONNECTION:
            LOG_MSGID_I(MUX_BT, "event: SERIAL_PORT_EVENT_BT_DISCONNECTION ", 0);
            g_mux_bt_callback(BT_PORT_INDEX_TO_MUX_PORT(port_index),MUX_EVENT_DISCONNECTION);
            break;
    }
}
mux_status_t port_mux_bt_init(mux_port_t port)
{
    mux_status_t status;
    mux_port_setting_t setting;
    mux_protocol_t mux_bt_pro_callback;

    setting.tx_buffer_size = 512;
    setting.rx_buffer_size = 512;
    mux_bt_pro_callback.tx_protocol_callback = NULL;
    mux_bt_pro_callback.rx_protocol_callback = NULL;
    status = mux_init(port,&setting,&mux_bt_pro_callback);
    //LOG_MSGID_I(MUX_BT, "port_mux_bt_init, status:%d", 1, status);
    return status;
}


mux_status_t port_mux_bt_normal_init(uint8_t port_index, mux_port_config_t *p_setting,mux_irq_handler_t irq_handler)
{
    serial_port_status_t serial_port_status;
    serial_port_open_para_t serial_port_bt_config;
    mux_common_device_r_w_point_init(&g_mux_bt_r_w_point[port_index],p_setting);

    if(x_mux_bt_Semaphore == NULL) {
        x_mux_bt_Semaphore = xSemaphoreCreateMutex();
        configASSERT(x_mux_bt_Semaphore != NULL);
    }

    g_mux_bt_callback = irq_handler;
    serial_port_bt_config.callback = mux_bt_callback;

    if(port_index == 0) {
        serial_port_bt_config.tx_buffer_size = 1024;
        serial_port_bt_config.rx_buffer_size = 1024;
    } else {
        serial_port_bt_config.tx_buffer_size = 1024;
        serial_port_bt_config.rx_buffer_size = 1024 + 256;
    }

    serial_port_status = serial_port_open(port_index + SERIAL_PORT_DEV_BT_SPP,&serial_port_bt_config,&serial_port_bt_handle[port_index]);
    LOG_MSGID_I(MUX_BT, "bt port open status[%d] port_idenx[%d] port_handle[%d]", 3, serial_port_status, port_index, serial_port_bt_handle[port_index]);
    if(serial_port_status != SERIAL_PORT_STATUS_OK) {
        return MUX_STATUS_ERROR;
    }

    mux_driver_debug_for_check(&g_mux_bt_r_w_point[port_index]);
    return MUX_STATUS_OK;
}

mux_status_t port_mux_bt_deinit(uint8_t port_index)
{
    serial_port_status_t status;

    mux_driver_debug_for_check(&g_mux_bt_r_w_point[port_index]);

    status = serial_port_close(serial_port_bt_handle[port_index]);
    LOG_MSGID_I(MUX_BT, "bt port close status[%d] port_idenx[%d] port_handle[%d]", 3, status, port_index, serial_port_bt_handle[port_index]);
    if(status != SERIAL_PORT_STATUS_OK) {
        return MUX_STATUS_ERROR_DEINIT_FAIL;
    }
    return MUX_STATUS_OK;
}

void port_mux_bt_exception_init(uint8_t port_index)
{
    PORT_MUX_UNUSED(port_index);
}

void port_mux_bt_exception_send(uint8_t port_index, uint8_t *buffer, uint32_t size)
{
    //TODO: need bt replace
    //maybe exception bt disconnect ???
    //bt_mux_dump_data(port_index, buffer, size);
}

bool port_mux_bt_buf_is_full(uint8_t port_index, bool is_rx)
{
    return mux_common_device_buf_is_full(&g_mux_bt_r_w_point[port_index],is_rx);
}

uint32_t port_mux_bt_get_hw_rptr(uint8_t port_index, bool is_rx)
{
    return mux_common_device_get_hw_rptr(&g_mux_bt_r_w_point[port_index],is_rx);
}

uint32_t port_mux_bt_get_hw_wptr(uint8_t port_index, bool is_rx)
{
    return mux_common_device_get_hw_wptr(&g_mux_bt_r_w_point[port_index],is_rx);
}

void port_mux_bt_set_rx_hw_rptr(uint8_t port_index, uint32_t move_bytes)
{
    mux_common_device_set_rx_hw_rptr(&g_mux_bt_r_w_point[port_index],move_bytes);
}

static void port_mux_bt_set_rx_hw_wptr_internal_use(uint8_t port_index, uint32_t move_bytes)
{
    mux_common_device_set_rx_hw_wptr_internal_use(&g_mux_bt_r_w_point[port_index],move_bytes);
}

static void port_mux_bt_set_tx_hw_rptr_internal_use(uint8_t port_index, uint32_t move_bytes)
{
    mux_common_device_set_tx_hw_rptr_internal_use(&g_mux_bt_r_w_point[port_index],move_bytes);
}

void port_mux_bt_set_tx_hw_wptr(uint8_t port_index, uint32_t move_bytes)
{
    virtual_read_write_point_t *p=&g_mux_bt_r_w_point[port_index];

    mux_common_device_set_tx_hw_wptr(p, move_bytes);
}

/* maybe have online log request , user is syslog,
    syslog can use any where.
*/
static void bt_gpt_callback(void *user_data)
{
    port_mux_bt_phase2_send(*(uint8_t*)user_data);
}

void port_mux_bt_phase1_send(uint8_t port_index)
{
    PORT_MUX_UNUSED(port_index);
    return;
}

void port_mux_bt_phase2_send(uint8_t port_index)
{
    uint32_t per_cpu_irq_mask;
    uint32_t send_addr,send_len;
    virtual_read_write_point_t *p = &g_mux_bt_r_w_point[port_index];

    port_mux_local_cpu_enter_critical(&per_cpu_irq_mask);
    mux_driver_debug_for_check(p);
    port_mux_local_cpu_exit_critical(per_cpu_irq_mask);

    if (HAL_NVIC_QUERY_EXCEPTION_NUMBER == HAL_NVIC_NOT_EXCEPTION) { //xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED
        //LOG_MSGID_I(MUX_BT,"Task thread ", 0);
        /* Task context */
        xSemaphoreTake(x_mux_bt_Semaphore, portMAX_DELAY);
        g_mux_bt_phase2_send_status = MUX_DEVICE_HW_RUNNING;
        p->tx_send_is_running = MUX_DEVICE_HW_RUNNING;

        LOG_MSGID_I(MUX_BT,"xSemaphoreTake OK ", 0);
        send_len  = mux_common_device_get_buf_next_available_block_len(p->tx_buff_start, p->tx_buff_read_point, p->tx_buff_write_point, p->tx_buff_end, p->tx_buff_available_len);
        send_addr = p->tx_buff_read_point;

        LOG_MSGID_I(MUX_BT,"first get_buf_next_available send_len = %d address = %08x ", 2, send_len, send_addr);
        bt_send_data(port_index, send_addr, send_len, &p->tx_sending_read_point);// user must update Read_point equle Write_point in this function

        if(p->tx_buff_read_point != p->tx_buff_write_point) {
            send_len  = mux_common_device_get_buf_next_available_block_len(p->tx_buff_start, p->tx_buff_read_point, p->tx_buff_write_point, p->tx_buff_end, p->tx_buff_available_len);
            send_addr = p->tx_buff_read_point;
            LOG_MSGID_I(MUX_BT,"second get_buf_next_available send_len = %d address = %08x ", 2, send_len, send_addr);
            bt_send_data(port_index, send_addr, send_len, &p->tx_sending_read_point);// user must update Read_point equle Write_point in this function
        }
        p->tx_send_is_running = MUX_DEVICE_HW_IDLE;
        g_mux_bt_phase2_send_status = MUX_DEVICE_HW_IDLE;
        xSemaphoreGive(x_mux_bt_Semaphore);
        LOG_MSGID_I(MUX_BT,"xSemaphoreGive OK \r\n", 0);
    } else {
        /* IRQ context */
        port_mux_local_cpu_enter_critical(&per_cpu_irq_mask);
        if(g_mux_bt_phase2_send_status == MUX_DEVICE_HW_IDLE)
        {
            g_mux_bt_phase2_send_status = MUX_DEVICE_HW_RUNNING;
            p->tx_send_is_running = MUX_DEVICE_HW_RUNNING;
            port_mux_local_cpu_exit_critical(per_cpu_irq_mask);

            send_len  = mux_common_device_get_buf_next_available_block_len(p->tx_buff_start, p->tx_buff_read_point, p->tx_buff_write_point, p->tx_buff_end, p->tx_buff_available_len);
            send_addr = p->tx_buff_read_point;
            bt_send_data(port_index, send_addr, send_addr, &p->tx_sending_read_point);// user must update Read_point equle Write_point in this function

            p->tx_send_is_running = MUX_DEVICE_HW_IDLE;
            g_mux_bt_phase2_send_status = MUX_DEVICE_HW_IDLE;
        }
        else
        {
            port_mux_local_cpu_exit_critical(per_cpu_irq_mask);
            if(p->tx_buff_available_len != 0)
            {
                hal_gpt_sw_start_timer_ms ( g_bt_gpt_handler,
                                            BT_GPT_TIMEOUT,
                                            bt_gpt_callback,
                                            (uint8_t*)&port_index); // delay 1ms retry...
            }
            return ;
        }
    }
}

mux_status_t port_mux_bt_control(uint8_t port_index, mux_ctrl_cmd_t command, mux_ctrl_para_t *para)
{
    printf("ERROR!!! MUX BT can not support port_mux_bt_control!!!\r\n");
    return MUX_STATUS_ERROR;
}
port_mux_device_ops_t g_port_mux_bt_ops = {
#ifdef MTK_CPU_NUMBER_0
    port_mux_bt_normal_init,
    port_mux_bt_deinit,
    port_mux_bt_exception_init,
    port_mux_bt_exception_send,
    port_mux_bt_buf_is_full,
#endif
    port_mux_bt_get_hw_rptr,
    port_mux_bt_set_rx_hw_rptr,
    port_mux_bt_get_hw_wptr,
    port_mux_bt_set_tx_hw_wptr,
    port_mux_bt_phase1_send,
    port_mux_bt_phase2_send,
    port_mux_bt_control,
};
#endif
