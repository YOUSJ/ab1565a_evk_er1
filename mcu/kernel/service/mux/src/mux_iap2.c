/* Copyright Statement:
 *
 * (C) 2020  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifdef MTK_IAP2_VIA_MUX_ENABLE
#include "iAP2.h"
#include "iAP2_spp.h"
#include "syslog.h"
#include "FreeRTOS.h"
#include "bt_callback_manager.h"
#include "bt_connection_manager.h"

#include "mux.h"
#include "mux_iap2.h"
#include "mux_port_device.h"

#ifdef MTK_AWS_MCE_ENABLE
#include "bt_role_handover.h"
#endif

#ifndef MTK_BT_CM_SUPPORT
#include "bt_sink_srv.h"
#endif

#define MUX_IAP2_FLAG_INIT      0x01U
#define MUX_IAP2_FLAG_REPLY_RHO 0x02U
typedef uint32_t mux_iap2_flag_t;

typedef struct mux_iap2_packet {
    uint8_t *data;
    uint16_t data_length;
    struct mux_iap2_packet *next;
} mux_iap2_packet_t;

typedef struct {
    uint32_t handle;
    mux_iap2_flag_t flag;
    uint16_t max_packet_length;
    bt_bd_addr_t remote_address;
} mux_iap2_handle_t;

typedef struct {
    mux_irq_handler_t handler;
    mux_port_config_t *config;
    virtual_read_write_point_t read_write_point;

    uint16_t protocol_id;
    uint16_t session_id;

    mux_iap2_packet_t *tx_head_packet;
    mux_iap2_packet_t *tx_tail_packet;
} mux_iap2_context_t;

typedef struct {
    uint8_t context_num;
} mux_iap2_rho_header_t;

typedef struct {
    uint16_t protocol_id;
    uint16_t session_id;
} mux_iap2_rho_context_t;

#ifdef MTK_CPU_NUMBER_0
mux_status_t mux_iap2_normal_init(uint8_t port_index, mux_port_config_t *p_setting, mux_irq_handler_t irq_handler);
mux_status_t mux_iap2_deinit(uint8_t port_index);
void mux_iap2_exception_init(uint8_t port_index);
void mux_iap2_exception_send(uint8_t port_index, uint8_t *buffer, uint32_t size);
bool mux_iap2_get_buf_is_full(uint8_t port_index, bool is_rx);
#endif
uint32_t mux_iap2_get_hw_rptr(uint8_t port_index, bool is_rx);
void mux_iap2_set_hw_rptr(uint8_t port_index, uint32_t move_bytes);
uint32_t mux_iap2_get_hw_wptr(uint8_t port_index, bool is_rx);
void mux_iap2_set_hw_wptr(uint8_t port_index, uint32_t move_bytes);
void mux_iap2_phase1_send(uint8_t port_index);
void mux_iap2_phase2_send(uint8_t port_index);
mux_status_t mux_iap2_control(uint8_t port_index, mux_ctrl_cmd_t command, mux_ctrl_para_t *para);
static void mux_iap2_reset_handle(void);
static void mux_iap2_reset_context(mux_iap2_context_t * context);
static void mux_iap2_reset_all_context(void);
static mux_iap2_packet_t *mux_iap2_alloc_packet(void);
static void mux_iap2_free_packet(mux_iap2_packet_t *packet);
static void mux_iap2_send_context_data(mux_iap2_context_t *context);
static void mux_iap2_context_callback(mux_iap2_context_t *context, mux_event_t event, void *parameter);
static mux_port_t mux_iap2_get_port_by_context(mux_iap2_context_t *context);
static mux_iap2_context_t *mux_iap2_get_context_by_port_index(uint8_t port_index);
static mux_iap2_context_t *mux_iap2_get_context_by_protocol_id(uint16_t protocol_id);
static mux_iap2_context_t *mux_iap2_get_context_by_session_id(uint16_t session_id);
static void mux_iap2_callback(iap2_event_t event, void *parameter);
static bt_status_t mux_iap2_app_event_callback(bt_msg_type_t msg, bt_status_t status, void *parameter);
#ifdef MTK_BT_CM_SUPPORT
bt_status_t mux_iap2_cm_callback(bt_cm_profile_service_handle_t type, void *data);
#else
bt_status_t mux_iap2_action_hanler(bt_sink_srv_action_t action, void *parameter);
#endif
#if defined(MTK_AWS_MCE_ENABLE) && !defined(BT_ROLE_HANDOVER_WITH_SPP_BLE)
bt_status_t mux_iap2_role_handover_allow_execution(const bt_bd_addr_t *address);
bt_status_t mux_iap2_role_handover_update(bt_role_handover_update_info_t *info);
void mux_iap2_role_handover_status_callback(const bt_bd_addr_t *address, bt_aws_mce_role_t role, bt_role_handover_event_t event, bt_status_t status);
#endif

port_mux_device_ops_t g_mux_iap2_ops = {
#ifdef MTK_CPU_NUMBER_0
    .normal_init = mux_iap2_normal_init,
    .deinit = mux_iap2_deinit,
    .exception_init = mux_iap2_exception_init,
    .exception_send = mux_iap2_exception_send,
    .get_buf_is_full = mux_iap2_get_buf_is_full,
#endif
    .get_hw_rptr = mux_iap2_get_hw_rptr,
    .set_hw_rptr = mux_iap2_set_hw_rptr,
    .get_hw_wptr = mux_iap2_get_hw_wptr,
    .set_hw_wptr = mux_iap2_set_hw_wptr,
    .phase1_send = mux_iap2_phase1_send,
    .phase2_send = mux_iap2_phase2_send,
    .control = mux_iap2_control
};

static mux_iap2_handle_t g_mux_iap2_handle;
static mux_iap2_packet_t g_mux_iap2_packet[MAX_MUX_IAP2_PACKET_NUM];
log_create_module(MUX_IAP2, PRINT_LEVEL_INFO);

static mux_iap2_context_t g_mux_iap2_context[MAX_MUX_IAP2_NUM] = {
    {NULL, NULL, {0}, MUX_IAP2_SESSION1_PROTOCOL_ID, 0, NULL, NULL},
    {NULL, NULL, {0}, MUX_IAP2_SESSION2_PROTOCOL_ID, 0, NULL, NULL},
    {NULL, NULL, {0}, MUX_IAP2_SESSION3_PROTOCOL_ID, 0, NULL, NULL}
};

#define MUX_IAP2_IS_PACKET_FREE(packet) \
    ((packet)->data == NULL)

#define MUX_IAP2_EXPAND_BD_ADDRESS(address) \
    ((address)[0], (address)[1], (address)[2], (address)[3], (address)[4], (address)[5])

#define MUX_IAP2_IS_SESSION_CONNECTED(context) \
    ((g_mux_iap2_handle.handle != 0) && ((context) != NULL) && ((context)->handler != NULL) && ((context)->session_id != 0))

void mux_iap2_register_callbacks(void)
{
#if defined(MTK_AWS_MCE_ENABLE) && !defined(BT_ROLE_HANDOVER_WITH_SPP_BLE)
    bt_role_handover_callbacks_t callbacks = {
        .allowed_cb = mux_iap2_role_handover_allow_execution,
        .update_cb = mux_iap2_role_handover_update,
        .status_cb = mux_iap2_role_handover_status_callback
    };

    bt_role_handover_register_callbacks(BT_ROLE_HANDOVER_MODULE_IAP2, &callbacks);
#endif
#ifdef MTK_BT_CM_SUPPORT
    bt_cm_profile_service_register(BT_CM_PROFILE_SERVICE_CUSTOMIZED_IAP2, mux_iap2_cm_callback);
#endif
    iap2_register_callback(mux_iap2_callback);
    bt_callback_manager_register_callback(bt_callback_type_app_event, MODULE_MASK_SYSTEM, mux_iap2_app_event_callback);
}

#ifdef MTK_CPU_NUMBER_0
mux_status_t mux_iap2_normal_init(uint8_t port_index, mux_port_config_t *p_setting, mux_irq_handler_t irq_handler)
{
    mux_iap2_context_t *context = NULL;
    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]normal_init, port_index:0x%x p_setting:0x%x irq_handler:0x%x", 3, port_index, p_setting, irq_handler);

    if ((context = mux_iap2_get_context_by_port_index(port_index)) == NULL)
    {
        return MUX_STATUS_ERROR_PARAMETER;
    }

    if (context->handler == NULL)
    {
        context->handler = irq_handler;
        context->config = p_setting;
        mux_common_device_r_w_point_init(&context->read_write_point, p_setting);
    }
    else
    {
        return MUX_STATUS_ERROR_INITIATED;
    }

    if (MUX_IAP2_IS_SESSION_CONNECTED(context))
    {
        mux_iap2_connection_t connection = {
            .session_id = context->session_id,
            .max_packet_size = g_mux_iap2_handle.max_packet_length
        };

        memcpy(connection.remote_address, g_mux_iap2_handle.remote_address, sizeof(bt_bd_addr_t));
        mux_iap2_context_callback(context, MUX_EVENT_CONNECTION, &connection);
    }

    return MUX_STATUS_OK;
}

mux_status_t mux_iap2_deinit(uint8_t port_index)
{
    mux_iap2_context_t *context = NULL;
    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]deinit, port_index:0x%x", 1, port_index);

    if ((context = mux_iap2_get_context_by_port_index(port_index)) != NULL)
    {
        mux_iap2_context_callback(context, MUX_EVENT_DISCONNECTION, NULL);
        context->handler = NULL;
    }
    else
    {
        return MUX_STATUS_ERROR_PARAMETER;
    }

    return MUX_STATUS_OK;
}

void mux_iap2_exception_init(uint8_t port_index)
{
    return;
}

void mux_iap2_exception_send(uint8_t port_index, uint8_t *buffer, uint32_t size)
{
    return;
}

bool mux_iap2_get_buf_is_full(uint8_t port_index, bool is_rx)
{
    bool is_full = false;
    mux_iap2_context_t *context = mux_iap2_get_context_by_port_index(port_index);

    if (context != NULL)
    {
        is_full = mux_common_device_buf_is_full(&context->read_write_point, is_rx);
    }

    return is_full;
}
#endif

ATTR_TEXT_IN_TCM uint32_t mux_iap2_get_hw_rptr(uint8_t port_index, bool is_rx)
{
    uint32_t hw_rptr = 0;
    mux_iap2_context_t *context = mux_iap2_get_context_by_port_index(port_index);

    if (context != NULL)
    {
        hw_rptr = mux_common_device_get_hw_rptr(&context->read_write_point, is_rx);
    }

    return hw_rptr;
}

void mux_iap2_set_hw_rptr(uint8_t port_index, uint32_t move_bytes)
{
    mux_iap2_context_t *context = mux_iap2_get_context_by_port_index(port_index);

    if (context != NULL)
    {
        mux_common_device_set_rx_hw_rptr(&context->read_write_point, move_bytes);
    }
}

ATTR_TEXT_IN_TCM uint32_t mux_iap2_get_hw_wptr(uint8_t port_index, bool is_rx)
{
    uint32_t hw_wptr = 0;
    mux_iap2_context_t *context = mux_iap2_get_context_by_port_index(port_index);

    if (context != NULL)
    {
        hw_wptr = mux_common_device_get_hw_wptr(&context->read_write_point, is_rx);
    }

    return hw_wptr;
}

ATTR_TEXT_IN_TCM void mux_iap2_set_hw_wptr(uint8_t port_index, uint32_t move_bytes)
{
    mux_iap2_packet_t *packet = NULL;
    mux_iap2_context_t *context = NULL;
    //LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]set_hw_wptr, port_index:0x%x move_bytes:0x%x", 2, port_index, move_bytes);

    if ((context = mux_iap2_get_context_by_port_index(port_index)) == NULL)
    {
        return;
    }

    // enter critical, start process packet
    mux_common_device_set_tx_hw_wptr(&context->read_write_point, move_bytes);

    for (uint32_t i = 0; i < MAX_MUX_IAP2_PACKET_NUM; i++)
    {
        if (MUX_IAP2_IS_PACKET_FREE(&g_mux_iap2_packet[i]))
        {
            packet = &g_mux_iap2_packet[i];
            break;
        }
    }

    if (packet == NULL)
    {
        //LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]set_hw_wptr, alloc packet FAILED!", 0);
        return;
    }

    packet->data = context->read_write_point.tx_buff_read_point;
    packet->data_length = move_bytes;
    packet->next = NULL;

    if (context->tx_head_packet == NULL)
    {
        context->tx_head_packet = packet;
        context->tx_tail_packet = packet;
    }
    else
    {
        context->tx_tail_packet->next = packet;
        context->tx_tail_packet = packet;
    }

    //LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]set_hw_wptr, add packet:0x%x, current head:0x%x tail:0x%x", 3,
    //    packet, context->tx_head_packet, context->tx_tail_packet);

    // exit critial, start send packet
}

void mux_iap2_phase1_send(uint8_t port_index)
{
    PORT_MUX_UNUSED(port_index);
    return;
}

void mux_iap2_phase2_send(uint8_t port_index)
{
    mux_iap2_context_t *context = NULL;
    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]phase2_send, port_index:0x%x", 1, port_index);

    if ((context = mux_iap2_get_context_by_port_index(port_index)) != NULL)
    {
        LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]phase2_send, current head:0x%x tail:0x%x", 2, context->tx_head_packet, context->tx_tail_packet);
        mux_iap2_send_context_data(context);
    }
}

mux_status_t mux_iap2_control(uint8_t port_index, mux_ctrl_cmd_t command, mux_ctrl_para_t *para)
{
    mux_iap2_context_t *context = NULL;
    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]control, port_index:0x%x command:0x%x para:0x%x", 3, port_index, command, para);

    if ((para == NULL) || ((context = mux_iap2_get_context_by_port_index(port_index)) == NULL))
    {
        return MUX_STATUS_ERROR;
    }

    if (command == MUX_CMD_GET_CONNECTION_PARAM)
    {
        mux_get_connection_param_t *connection_param = (mux_get_connection_param_t *)para;

        connection_param->iap2_session_id = context->session_id;
        connection_param->max_packet_size = g_mux_iap2_handle.max_packet_length;
        memcpy(connection_param->remote_address, g_mux_iap2_handle.remote_address, sizeof(bt_bd_addr_t));
    }

    return MUX_STATUS_OK;
}

static void mux_iap2_reset_handle(void)
{
    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]reset_handle", 0);
    memset(&g_mux_iap2_handle, 0, sizeof(mux_iap2_handle_t));
}

static void mux_iap2_reset_context(mux_iap2_context_t * context)
{
    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]reset_context, context:0x%x", 1, context);

    while (context->tx_head_packet != NULL)
    {
        mux_iap2_packet_t *delete_packet = context->tx_head_packet;
        context->tx_head_packet = delete_packet->next;

        mux_common_device_set_tx_hw_rptr_internal_use(&context->read_write_point, delete_packet->data_length);
        //vPortFree(delete_packet);
        mux_iap2_free_packet(delete_packet);

        LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]reset_context, delete packet:0x%x, current head:0x%x tail:0x%x", 3,
            delete_packet, context->tx_head_packet, context->tx_tail_packet);
    }

    context->session_id = 0;
    context->tx_head_packet = NULL;
    context->tx_tail_packet = NULL;

#if 0
    if (context->config != NULL)
    {
        mux_common_device_r_w_point_init(&context->read_write_point, context->config);
    }
    else
    {
        LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]reset_context, config is NULL!", 0);
    }
#endif
}

static void mux_iap2_reset_all_context(void)
{
    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]reset_all_context", 0);

    for (uint8_t i = 0; i < MAX_MUX_IAP2_NUM; i++)
    {
        mux_iap2_reset_context(&g_mux_iap2_context[i]);
    }
}

static mux_iap2_packet_t *mux_iap2_alloc_packet(void)
{
    mux_iap2_packet_t *packet = NULL;

    for (uint8_t i = 0; i < MAX_MUX_IAP2_PACKET_NUM; i++)
    {
        if (MUX_IAP2_IS_PACKET_FREE(&g_mux_iap2_packet[i]))
        {
            packet = &g_mux_iap2_packet[i];
            break;
        }
    }

    //LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2] alloc packet, packet:0x%x", 1, packet);
    return packet;
}

static void mux_iap2_free_packet(mux_iap2_packet_t *packet)
{
    //LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2] free packet:0x%x", 1, packet);
    memset(packet, 0, sizeof(mux_iap2_packet_t));
}

static void mux_iap2_send_context_data(mux_iap2_context_t *context)
{
    while ((context->tx_head_packet != NULL) &&
        (MUX_IAP2_IS_SESSION_CONNECTED(context)) &&
        (context->read_write_point.tx_send_is_running != MUX_DEVICE_HW_RUNNING))
    {
        uint32_t next_read_point = 0;
        mux_iap2_packet_t *send_packet = context->tx_head_packet;
        context->read_write_point.tx_send_is_running = MUX_DEVICE_HW_RUNNING;

        if (context->read_write_point.tx_buff_read_point + send_packet->data_length <= context->read_write_point.tx_buff_end)
        {
            if (iap2_send_data_by_external_accessory_session(g_mux_iap2_handle.handle, context->session_id, context->read_write_point.tx_buff_read_point, send_packet->data_length) == IAP2_STATUS_SUCCESS)
            {
                next_read_point = context->read_write_point.tx_buff_read_point + send_packet->data_length;
            }
        }
        else
        {
            uint8_t * full_data = pvPortMalloc(send_packet->data_length);

            if (full_data == NULL)
            {
                LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]send_context_data, alloc full data FAILED!", 0);
                return;
            }

            uint8_t *first_data = context->read_write_point.tx_buff_read_point;
            uint16_t first_data_length = context->read_write_point.tx_buff_end - (uint32_t)context->read_write_point.tx_buff_read_point;
            memcpy(full_data, first_data, first_data_length);

            uint8_t *continue_data = context->read_write_point.tx_buff_start;
            uint16_t continue_data_length = send_packet->data_length - first_data_length;
            memcpy(full_data + first_data_length, continue_data, continue_data_length);

            if (iap2_send_data_by_external_accessory_session(g_mux_iap2_handle.handle, context->session_id, full_data, send_packet->data_length) == IAP2_STATUS_SUCCESS)
            {
                next_read_point = context->read_write_point.tx_buff_start + continue_data_length;
            }

            vPortFree(full_data);
        }

        if (next_read_point != 0)
        {
            if ((context->tx_head_packet = send_packet->next) == NULL)
            {
                context->tx_tail_packet = NULL;
            }

            mux_common_device_set_tx_hw_rptr_internal_use(&context->read_write_point, send_packet->data_length);
            mux_iap2_free_packet(send_packet);
            //vPortFree(send_packet);
            //context->read_write_point.tx_buff_read_point = next_read_point;

            LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]send_context_data, send packet:0x%x, current head:0x%x tail:0x%x", 3,
                send_packet, context->tx_head_packet, context->tx_tail_packet);
        }
        else
        {
            context->read_write_point.tx_send_is_running = MUX_DEVICE_HW_IDLE;
            return;
        }

        context->read_write_point.tx_send_is_running = MUX_DEVICE_HW_IDLE;
    }
}

static void mux_iap2_context_callback(mux_iap2_context_t *context, mux_event_t event, void *parameter)
{
    mux_port_t port = mux_iap2_get_port_by_context(context);
    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]context_callback, context:0x%x event:0x%x parameter:0x%x", 3, context, event, parameter);

    if ((port >= MUX_IAP2_BEGIN) && (port <= MUX_IAP2_END) && (context->handler != NULL))
    {
        // TODO: Extra parameter for event
        context->handler(port, event);
    }
}

static mux_port_t mux_iap2_get_port_by_context(mux_iap2_context_t *context)
{
    mux_port_t port = MUX_IAP2_BEGIN + (context - &g_mux_iap2_context[0]);
    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]get_port_by_context, context:0x%x, port:0x%x", 2, context, port);

    return port;
}

static ATTR_TEXT_IN_TCM mux_iap2_context_t *mux_iap2_get_context_by_port_index(uint8_t port_index)
{
    mux_iap2_context_t *context = NULL;

    if (port_index < MAX_MUX_IAP2_NUM)
    {
        context = &g_mux_iap2_context[port_index];
    }

    //LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]get_context_by_port_index, index:0x%x, context:0x%x", 2, port_index, context);
    return context;
}

static mux_iap2_context_t *mux_iap2_get_context_by_protocol_id(uint16_t protocol_id)
{
    mux_iap2_context_t *context = NULL;

    for (uint8_t i = 0; i < MAX_MUX_IAP2_NUM; i++)
    {
        if (g_mux_iap2_context[i].protocol_id == protocol_id)
        {
            context = &g_mux_iap2_context[i];
            break;
        }
    }

    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]get_context_by_protocol_id, protocol_id:0x%x, context:0x%x", 2, protocol_id, context);
    return context;
}

static mux_iap2_context_t *mux_iap2_get_context_by_session_id(uint16_t session_id)
{
    mux_iap2_context_t *context = NULL;

    for (uint8_t i = 0; i < MAX_MUX_IAP2_NUM; i++)
    {
        if (g_mux_iap2_context[i].session_id == session_id)
        {
            context = &g_mux_iap2_context[i];
            break;
        }
    }

    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]get_context_by_session_id, session_id:0x%x, context:0x%x", 2, session_id, context);
    return context;
}

static void mux_iap2_callback(iap2_event_t event, void *parameter)
{
    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]callback, event:0x%x parameter:0x%x", 2, event, parameter);

    switch (event)
    {
        case IAP2_CONNECT_IND:
        {
            iap2_connect_ind_t *connect_ind = (iap2_connect_ind_t *)parameter;

            if (connect_ind->status == IAP2_STATUS_SUCCESS)
            {
                g_mux_iap2_handle.handle = connect_ind->handle;
                g_mux_iap2_handle.max_packet_length = connect_ind->max_packet_length;
                memcpy(&g_mux_iap2_handle.remote_address, connect_ind->address, sizeof(bt_bd_addr_t));

            #ifdef MTK_BT_CM_SUPPORT
                bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_CUSTOMIZED_IAP2, g_mux_iap2_handle.remote_address, BT_CM_PROFILE_SERVICE_STATE_CONNECTED, BT_STATUS_SUCCESS);
            #else
                bt_sink_srv_cm_profile_status_notify(&g_mux_iap2_handle.remote_address, BT_SINK_SRV_PROFILE_IAP2, BT_SINK_SRV_PROFILE_CONNECTION_STATE_CONNECTED, BT_STATUS_SUCCESS);
            #endif

                LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]callback, IAP2_CONNECT_IND max_paxket_length:0x%x remote_address:0x%x:%x:%x:%x:%x:%x", 7,
                    g_mux_iap2_handle.max_packet_length, MUX_IAP2_EXPAND_BD_ADDRESS(g_mux_iap2_handle.remote_address));
            }
            else
            {
            #ifdef MTK_BT_CM_SUPPORT
                bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_CUSTOMIZED_IAP2, g_mux_iap2_handle.remote_address, BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED, connect_ind->status);
            #else
                bt_sink_srv_cm_profile_status_notify(&g_mux_iap2_handle.remote_address, BT_SINK_SRV_PROFILE_IAP2, BT_SINK_SRV_PROFILE_CONNECTION_STATE_DISCONNECTED, connect_ind->status);
            #endif
            #ifdef MTK_AWS_MCE_ENABLE
                if ((g_mux_iap2_handle.flag & MUX_IAP2_FLAG_REPLY_RHO) != 0)
                {
                    bt_role_handover_reply_prepare_request(BT_ROLE_HANDOVER_MODULE_IAP2);
                    g_mux_iap2_handle.flag &= ~MUX_IAP2_FLAG_REPLY_RHO;
                }
            #endif
                mux_iap2_reset_handle();
            }

            break;
        }

        case IAP2_DISCONNECT_IND:
        {
        #ifdef MTK_AWS_MCE_ENABLE
            if ((g_mux_iap2_handle.flag & MUX_IAP2_FLAG_REPLY_RHO) != 0)
            {
                bt_role_handover_reply_prepare_request(BT_ROLE_HANDOVER_MODULE_IAP2);
                g_mux_iap2_handle.flag &= ~MUX_IAP2_FLAG_REPLY_RHO;
            }
        #endif

            for (uint8_t i = 0; i < MAX_MUX_IAP2_NUM; i++)
            {
                if (MUX_IAP2_IS_SESSION_CONNECTED(&g_mux_iap2_context[i]))
                {
                    mux_iap2_context_callback(&g_mux_iap2_context[i], MUX_EVENT_DISCONNECTION, NULL);
                }
            }

            if (g_mux_iap2_handle.handle != 0)
            {
            #ifdef MTK_BT_CM_SUPPORT
                bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_CUSTOMIZED_IAP2, g_mux_iap2_handle.remote_address, BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED, BT_STATUS_SUCCESS);
            #else
                bt_sink_srv_cm_profile_status_notify(&g_mux_iap2_handle.remote_address, BT_SINK_SRV_PROFILE_IAP2, BT_SINK_SRV_PROFILE_CONNECTION_STATE_DISCONNECTED, BT_STATUS_SUCCESS);
            #endif
            }

            mux_iap2_reset_all_context();
            mux_iap2_reset_handle();

            break;
        }

        case IAP2_EA_SESSION_OPEN_IND:
        {
            iap2_ea_session_open_close_t *ea_session = (iap2_ea_session_open_close_t *)parameter;
            mux_iap2_context_t *context = mux_iap2_get_context_by_protocol_id(ea_session->protocol_id);

            if (context != NULL)
            {
                mux_iap2_connection_t connection = {
                    .session_id = ea_session->session_id,
                    .max_packet_size = g_mux_iap2_handle.max_packet_length
                };

                context->session_id = ea_session->session_id;
                memcpy(connection.remote_address, g_mux_iap2_handle.remote_address, sizeof(bt_bd_addr_t));
                mux_iap2_context_callback(context, MUX_EVENT_CONNECTION, &connection);
            }

            break;
        }

        case IAP2_EA_SESSION_CLOSE_IND:
        {
            iap2_ea_session_open_close_t *ea_session = (iap2_ea_session_open_close_t *)parameter;
            mux_iap2_context_t *context = mux_iap2_get_context_by_session_id(ea_session->session_id);

            if (context != NULL)
            {
                mux_iap2_context_callback(context, MUX_EVENT_DISCONNECTION, NULL);
                mux_iap2_reset_context(context);
            }

            break;
        }

        case IAP2_RECIEVED_DATA_IND:
        {
            mux_iap2_context_t *context = NULL;
            iap2_data_received_ind_t *received_data = (iap2_data_received_ind_t *)parameter;

            if (received_data->session_type == IAP2_SESSION_TYPE_EXTERNAL_ACCESSORY)
            {
                if (((context = mux_iap2_get_context_by_session_id(received_data->session_id)) != NULL) &&
                    MUX_IAP2_IS_SESSION_CONNECTED(context))
                {
                    virtual_read_write_point_t * rw_point = &context->read_write_point;
                    uint32_t free_length = mux_common_device_get_buf_next_free_block_len(rw_point->rx_buff_start, rw_point->rx_buff_read_point, rw_point->rx_buff_write_point, rw_point->rx_buff_end, rw_point->rx_buff_available_len);
                    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]callback, packet_length:0x%x, free_length:0x%x", 2, received_data->packet_length, free_length);

                    if (free_length >= received_data->packet_length)
                    {
                        memcpy(rw_point->rx_buff_write_point, received_data->packet, received_data->packet_length);
                    }
                    else
                    {
                        memcpy(rw_point->rx_buff_write_point, received_data->packet, free_length);
                        memcpy(rw_point->rx_buff_start, received_data->packet + free_length, received_data->packet_length - free_length);
                    }

                    mux_common_device_set_rx_hw_wptr_internal_use(rw_point, received_data->packet_length);
                    mux_iap2_context_callback(context, MUX_EVENT_READY_TO_READ, NULL);
                }

                iap2_release_data(IAP2_SESSION_TYPE_EXTERNAL_ACCESSORY, received_data->packet);
            }
            else if (received_data->session_type == IAP2_SESSION_TYPE_CONTROL)
            {
                LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]callback, received control session data", 0);
                iap2_release_data(IAP2_SESSION_TYPE_CONTROL, received_data->packet);
            }
            else
            {
                LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]callback, received unknown session data", 0);
            }

            break;
        }

        case IAP2_READY_TO_SEND_IND:
        {
            for (uint8_t i = 0; i < MAX_MUX_IAP2_NUM; i++)
            {
                if (MUX_IAP2_IS_SESSION_CONNECTED(&g_mux_iap2_context[i]))
                {
                    mux_iap2_send_context_data(&g_mux_iap2_context[i]);
                    mux_iap2_context_callback(&g_mux_iap2_context[i], MUX_EVENT_READY_TO_WRITE, NULL);
                }
            }

            break;
        }

        default:
        {
            break;
        }
    }
}

static bt_status_t mux_iap2_app_event_callback(bt_msg_type_t msg, bt_status_t status, void *parameter)
{
    if ((msg == BT_POWER_ON_CNF) && ((g_mux_iap2_handle.flag & MUX_IAP2_FLAG_INIT) == 0))
    {
        iap2_status_t status = iap2_init();
        LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2]app_event_callback, iAP2 init status:0x%x", 1, status);

        if (status == BT_STATUS_SUCCESS)
        {
            g_mux_iap2_handle.flag |= MUX_IAP2_FLAG_INIT;
        }
    }

    return BT_STATUS_SUCCESS;
}

#ifdef MTK_BT_CM_SUPPORT
bt_status_t mux_iap2_cm_callback(bt_cm_profile_service_handle_t type, void *data)
{
    switch (type)
    {
        case BT_CM_PROFILE_SERVICE_HANDLE_CONNECT:
        {
            memcpy(g_mux_iap2_handle.remote_address, data, sizeof(bt_bd_addr_t));
            iap2_status_t status = iap2_connect(&g_mux_iap2_handle.handle, (const bt_bd_addr_t *)data);
            LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2][CM]cm_callback, connect status:0x%x", 1, status);
            break;
        }

        case BT_CM_PROFILE_SERVICE_HANDLE_DISCONNECT:
        {
            iap2_status_t status = iap2_disconnect(g_mux_iap2_handle.handle);
            LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2][CM]callback, disconnect status:0x%x", 1, status);
            break;
        }

        default:
        {
            break;
        }
    }

    return BT_STATUS_SUCCESS;
}
#else
bt_status_t mux_iap2_action_hanler(bt_sink_srv_action_t action, void *parameter)
{
    if (action == BT_SINK_SRV_ACTION_PROFILE_CONNECT)
    {
        bt_sink_srv_profile_connection_action_t *connection_action = (bt_sink_srv_profile_connection_action_t *)parameter;

        if (connection_action->profile_connection_mask & BT_SINK_SRV_PROFILE_IAP2)
        {
            memcpy(g_mux_iap2_handle.remote_address, connection_action->address, sizeof(bt_bd_addr_t));
            iap2_status_t status = iap2_connect(&g_mux_iap2_handle.handle, (const bt_bd_addr_t *)&connection_action->address);
            LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2][CM]action_handler, connect status:0x%x", 1, status);
        }
    }

    return BT_STATUS_SUCCESS;
}
#endif

#ifdef MTK_AWS_MCE_ENABLE
#ifdef BT_ROLE_HANDOVER_WITH_SPP_BLE
bt_status_t iap2_rho_allow_execution_ext(const bt_bd_addr_t *address)
{
    return BT_STATUS_SUCCESS;
}

uint8_t iap2_rho_get_data_length_ext(const bt_bd_addr_t *address)
{
    return sizeof(mux_iap2_rho_header_t) + MAX_MUX_IAP2_NUM * sizeof(mux_iap2_rho_context_t);
}

bt_status_t iap2_rho_get_data_ext(const bt_bd_addr_t *address, void *data)
{
    mux_iap2_rho_header_t *header = (mux_iap2_rho_header_t *)data;
    mux_iap2_rho_context_t *rho_context = (mux_iap2_rho_context_t *)(header + 1);

    header->context_num = MAX_MUX_IAP2_NUM;

    for (uint8_t i = 0; i < header->context_num; i++)
    {
        (rho_context + i)->protocol_id = g_mux_iap2_context[i].protocol_id;
        (rho_context + i)->session_id = g_mux_iap2_context[i].session_id;
    }

    return BT_STATUS_SUCCESS;
}

bt_status_t iap2_rho_update_ext(iap2_connect_ind_t *ind, bt_aws_mce_role_t role, void *data)
{
    switch (role)
    {
        case BT_AWS_MCE_ROLE_AGENT:
        {
            LOG_MSGID_I(MUX_IAP2, "[Port][iAP2][RHO]update, Agent->Partner success", 0);

            mux_iap2_reset_all_context();
            mux_iap2_reset_handle();

            break;
        }

        case BT_AWS_MCE_ROLE_PARTNER:
        {
            LOG_MSGID_I(MUX_IAP2, "[Port][iAP2][RHO]update, Partner->Agent success", 0);
            mux_iap2_rho_header_t *header = (mux_iap2_rho_header_t *)data;
            mux_iap2_rho_context_t *rho_context = (mux_iap2_rho_context_t *)(header + 1);

            for (uint8_t i = 0; i < header->context_num; i++)
            {
                mux_iap2_context_t *context = mux_iap2_get_context_by_protocol_id((rho_context + i)->protocol_id);

                if (context != NULL)
                {
                    context->session_id = (rho_context + i)->session_id;
                    LOG_MSGID_I(MUX_IAP2, "[Port][iAP2][RHO]update, protocol:0x%x session:0x%x", 2, context->protocol_id, context->session_id);
                }
            }

        #if 0
            g_mux_iap2_handle.handle = bt_spp_get_handle_by_local_server_id(address, BT_SPP_IAP2_SERVER_ID);
            g_mux_iap2_handle.flag = 0;
            g_mux_iap2_handle.max_packet_length = bt_rfcomm_get_max_frame_size((void *)g_mux_iap2_handle.handle);
            memcpy(g_mux_iap2_handle.remote_address, address, sizeof(bt_bd_addr_t));
        #endif

            g_mux_iap2_handle.handle = ind->handle;
            g_mux_iap2_handle.flag = MUX_IAP2_FLAG_INIT;
            g_mux_iap2_handle.max_packet_length = ind->max_packet_length;
            memcpy(&g_mux_iap2_handle.remote_address, ind->address, sizeof(bt_bd_addr_t));

            break;
        }

        default:
        {
            break;
        }
    }

    return BT_STATUS_SUCCESS;
}
#else
bt_status_t mux_iap2_role_handover_allow_execution(const bt_bd_addr_t *address)
{
    bt_status_t status = BT_STATUS_SUCCESS;

    if (g_mux_iap2_handle.handle != 0)
    {
        status = BT_STATUS_PENDING;
        g_mux_iap2_handle.flag |= MUX_IAP2_FLAG_REPLY_RHO;
    }

    LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2][RHO]allow_execution, status:0x%x", 1, status);
    return status;
}

bt_status_t mux_iap2_role_handover_update(bt_role_handover_update_info_t *info)
{
    if (info == NULL)
    {
        LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2][RHO]update, NULL info return FAIL!", 0);
        return BT_STATUS_FAIL;
    }

    switch (info->role)
    {
        case BT_AWS_MCE_ROLE_AGENT:
        {
            LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2][RHO]update, agent->partner success", 0);
            break;
        }

        case BT_AWS_MCE_ROLE_PARTNER:
        {
            LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2][RHO]update, partner->agent success", 0);

            iap2_status_t status = iap2_connect(&g_mux_iap2_handle.handle, info->addr);
            LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2][RHO]update, reconnect result:0x%x", 1, status);

            break;
        }

        default:
        {
            LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2][RHO]update, unknown role!", 0);
            break;
        }
    }

    return BT_STATUS_SUCCESS;
}

void mux_iap2_role_handover_status_callback(const bt_bd_addr_t *address, bt_aws_mce_role_t role, bt_role_handover_event_t event, bt_status_t status)
{
    switch (event)
    {
        case BT_ROLE_HANDOVER_PREPARE_REQ_IND:
        {
            if (g_mux_iap2_handle.handle != 0)
            {
                iap2_status_t status = iap2_disconnect(g_mux_iap2_handle.handle);
                LOG_MSGID_I(MUX_IAP2, "[MUX][iAP2][RHO]status_callback, disconnect status:0x%x", 1, status);
            }

            break;
        }

        case BT_ROLE_HANDOVER_COMPLETE_IND:
        {
            if (role == BT_AWS_MCE_ROLE_AGENT && status != BT_STATUS_SUCCESS)
            {
                iap2_status_t status = iap2_connect(&g_mux_iap2_handle.handle, address);
                LOG_MSGID_I(MUX_IAP2, "[Port][iAP2][RHO]status_callback, reconnect status:0x%x", 1, status);
            }

            break;
        }

        default:
        {
            LOG_MSGID_I(MUX_IAP2, "[Port][iAP2][RHO]status_callback, role:0x%x event:0x%x status:0x%x", 3, role, event, status);
            break;
        }
    }
}
#endif
#endif
#endif
