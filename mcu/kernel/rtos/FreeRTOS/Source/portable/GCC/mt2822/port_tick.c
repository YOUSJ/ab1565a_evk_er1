/*
 * FreeRTOS Kernel V10.1.1
 * Copyright (C) 2018 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 * 1 tab == 4 spaces!
 */

/*-----------------------------------------------------------
 * Implementation of functions defined in portable.h for the ARM CM4F port.
 *----------------------------------------------------------*/

#include "FreeRTOS.h"
#include "port_tick.h"
#include "hal_clock.h"
#include "hal_nvic.h"
#include "timers.h"
#include "task.h"
#include "hal_log.h"

#if configUSE_TICKLESS_IDLE == 2
#include "task.h"
#include "memory_attribute.h"
#include "hal_sleep_manager.h"
#include "hal_sleep_manager_internal.h"
#include "hal_sleep_manager_platform.h"
#include "core_cm4.h"
#include "hal_gpt.h"
#include "hal_rtc.h"
#include "hal_dvfs.h"
#include "hal_eint.h"
#include "hal_wdt.h"
#ifdef MTK_SYSTEM_HANG_TRACER_ENABLE
#include "systemhang_tracer.h"
#endif /* MTK_SYSTEM_HANG_TRACER_ENABLE */
#include "assert.h"
#endif

#define MaximumIdleTime 10  //ms
#define DEEP_SLEEP_HW_WAKEUP_TIME 2
#define DEEP_SLEEP_SW_BACKUP_RESTORE_TIME 2

//#define TICKLESS_DEEBUG_ENABLE
#ifdef  TICKLESS_DEEBUG_ENABLE
#define log_debug_tickless(_message,...) log_hal_info(_message, ##__VA_ARGS__)
#else
#define log_debug_tickless(_message,...)
#endif

#if configUSE_TICKLESS_IDLE != 0
/*
 * The number of OS GPT increments that make up one tick period.
 */
static uint32_t ulTimerCountsForOneTick = 0;

/*
 * The maximum number of tick periods that can be suppressed is limited by the
 * 32 bit resolution of the OS GPT timer.
 */
static uint32_t xMaximumPossibleSuppressedTicks = 0;
#endif

#if configUSE_TICKLESS_IDLE == 2
TimerHandle_t timestamp_timer = NULL;
float RTC_Freq = 32.768; /* RTC 32.768KHz Freq*/
static long unsigned int before_idle_time, before_sleep_time;

#ifdef  SLEEP_MANAGEMENT_DEBUG_ENABLE
extern uint32_t eint_get_status(void);
uint32_t wakeup_eint;
#endif

static uint32_t count_idle_time_us;
static uint32_t count_sleep_time_us;
static bool count_idle = FALSE;

uint8_t tickless_sleep_manager_handle;

void os_gpt0_pause(void);
void os_gpt0_resume(bool update, uint32_t new_compare);

void doIdelSystickCalibration(uint32_t maxSystickCompensation)
{
    static long unsigned int after_idle_time, sleep_time = 0;
    static uint32_t ulCompleteTickPeriods;

    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, (uint32_t *)&after_idle_time);

    if (after_idle_time >= before_idle_time) {
        sleep_time = after_idle_time - before_idle_time;
    } else {
        sleep_time = after_idle_time + (0xFFFFFFFF - before_idle_time);
    }

    ulCompleteTickPeriods = (sleep_time / 1000) / (1000 / configTICK_RATE_HZ);

    if(count_idle) {
        count_idle_time_us += sleep_time;
    }

    /* Limit OS Tick Compensation Value */
    if (ulCompleteTickPeriods >= (maxSystickCompensation)) {
        ulCompleteTickPeriods = maxSystickCompensation;
    }

    vTaskStepTick(ulCompleteTickPeriods);
    return;
}

void doSleepSystickCalibration(uint32_t maxSystickCompensation)
{
    static uint32_t ulCompleteTickPeriods;
    static long unsigned int after_sleep_time, sleep_time = 0;

    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_32K, &after_sleep_time);

    if (after_sleep_time >= before_sleep_time) {
        sleep_time = after_sleep_time - before_sleep_time;
    } else {
        sleep_time = after_sleep_time + (0xFFFFFFFF - before_sleep_time);
    }

    ulCompleteTickPeriods = ((unsigned int)(((float)sleep_time) / RTC_Freq)) / ((1000 / configTICK_RATE_HZ));

    if(count_idle) {
        count_sleep_time_us += sleep_time;
    }

    /* Limit OS Tick Compensation Value */
    if (ulCompleteTickPeriods > (maxSystickCompensation - 1)) {
        ulCompleteTickPeriods = maxSystickCompensation - 1;
    }

    vTaskStepTick(ulCompleteTickPeriods);

    log_debug_tickless("CTP=%u\r\n"  , (unsigned int)ulCompleteTickPeriods);
    return;
}

void AST_vPortSuppressTicksAndSleep(TickType_t xExpectedIdleTime)
{
    volatile static unsigned int ulAST_Reload_ms;

    __asm volatile("cpsid i");

#ifdef MTK_SYSTEM_HANG_TRACER_ENABLE
    /* make sure the sleep time does not overflow the wdt limitation. */
    uint32_t sleep_time_sec = ((xExpectedIdleTime) / configTICK_RATE_HZ);
    if (sleep_time_sec > (HAL_WDT_MAX_TIMEOUT_VALUE - 10))
    {
        sleep_time_sec = (HAL_WDT_MAX_TIMEOUT_VALUE - 10);
        xExpectedIdleTime = sleep_time_sec * configTICK_RATE_HZ;
        /* maybe xExpectedIdleTime is still larger than OS GPT reload value, it will be cut again */
        /* in any case, wdt timeout value must be larger than sleep time */
    }
#endif /* MTK_SYSTEM_HANG_TRACER_ENABLE */

    /* Calculate total idle time to ms */
    ulAST_Reload_ms = (xExpectedIdleTime - 1) * (1000 / configTICK_RATE_HZ);
    ulAST_Reload_ms = ulAST_Reload_ms - DEEP_SLEEP_SW_BACKUP_RESTORE_TIME - DEEP_SLEEP_HW_WAKEUP_TIME;

    if (eTaskConfirmSleepModeStatus() == eAbortSleep) {
        /* Restart OS GPT. */
        os_gpt0_resume(false, 0);

        /* Re-enable interrupts */
        __asm volatile("cpsie i");
        return;
    } else {
        /* Enter Sleep mode */
        if (ulAST_Reload_ms > 0) {
            hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_32K, &before_sleep_time);

#ifdef MTK_SYSTEM_HANG_TRACER_ENABLE
            /* in here, sleep_time_sec is between 0 and HAL_WDT_MAX_TIMEOUT_VALUE - 10 */
            extern void hal_wdt_enter_sleep(uint32_t seconds);
            /* disable wdt and config wdt into reset mode */
            hal_wdt_enter_sleep(sleep_time_sec + 10);
            /* after here, wdt is in reset mode for prevent sleep flow hang */
#endif /* MTK_SYSTEM_HANG_TRACER_ENABLE */

            hal_sleep_manager_set_sleep_time((uint32_t)ulAST_Reload_ms);
            hal_sleep_manager_enter_sleep_mode(HAL_SLEEP_MODE_SLEEP);

#ifdef MTK_SYSTEM_HANG_TRACER_ENABLE
            /* restore wdt status to the configuration before sleep */
            extern void hal_wdt_exit_sleep(void);
            hal_wdt_exit_sleep();
            /* update safe duration */
            systemhang_set_safe_duration(SYSTEMHANG_USER_CONFIG_COUNT_TOTAL - 1, 60*20);
#endif /* MTK_SYSTEM_HANG_TRACER_ENABLE */
        }

        /* Calculate and Calibration Sleep Time to OS Tick */
        doSleepSystickCalibration(xExpectedIdleTime);

        /* Restart OS GPT. */
        os_gpt0_resume(false, 0);

#ifdef  SLEEP_MANAGEMENT_DEBUG_ENABLE
        wakeup_eint = eint_get_status();
#endif

        /* Re-enable interrupts */
        __asm volatile("cpsie i");

        sleep_management_dump_debug_log(SLEEP_MANAGEMENT_DEBUG_LOG_DUMP);

#ifdef  SLEEP_MANAGEMENT_DEBUG_ENABLE
#ifdef  SLEEP_MANAGEMENT_DEBUG_SLEEP_WAKEUP_LOG_ENABLE
        sleep_management_dump_wakeup_source(sleep_management_status.wakeup_source, wakeup_eint);
#endif
#endif
        log_debug_tickless("\r\nEIT=%u\r\n"  , (unsigned int)xExpectedIdleTime);
        log_debug_tickless("RL=%u\r\n"       , (unsigned int)ulAST_Reload_ms);
    }
}

void tickless_handler(uint32_t xExpectedIdleTime)
{
    static long unsigned int ulReloadValue;
    static long unsigned int after, idle = 0;

    /* Enter a critical section but don't use the taskENTER_CRITICAL()
    method as that will mask interrupts that should exit sleep mode. */
    __asm volatile("cpsid i");

    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, (uint32_t *)&before_idle_time);

    if(hal_sleep_manager_is_sleep_handle_alive(SLEEP_LOCK_BT_CONTROLLER) == true) {
        __asm volatile("dsb");
        __asm volatile("wfi");
        __asm volatile("isb");

        if(count_idle) {
            hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, (uint32_t *)&after);

            if (after >= before_idle_time) {
                idle = after - before_idle_time;
            } else {
                idle = after + (0xFFFFFFFF - before_idle_time);
            }
            count_idle_time_us += idle;
        }

        __asm volatile("cpsie i");
        return;
    }

    /* Stop the OS GPT momentarily.  */
    //os_gpt0_pause();

    if ((xExpectedIdleTime > (MaximumIdleTime / (1000 / configTICK_RATE_HZ))) && (hal_sleep_manager_is_sleep_locked() == 0)) {
        AST_vPortSuppressTicksAndSleep(xExpectedIdleTime);
        return;
    }

    /* Make sure the OS GPT reload value does not overflow the counter. */
    if (xExpectedIdleTime > (xMaximumPossibleSuppressedTicks)) {
        xExpectedIdleTime = (xMaximumPossibleSuppressedTicks);
    }

    /* If a context switch is pending or a task is waiting for the scheduler
    to be unsuspended then abandon the low power entry. */
    if (eTaskConfirmSleepModeStatus() == eAbortSleep) {
        /* Restart OS GPT. */
        os_gpt0_resume(false, 0);

        /* Re-enable interrupts - see comments above the cpsid instruction()
        above. */
        __asm volatile("cpsie i");
    } else {
        /* Enter IDLE mode */
#ifdef MTK_SYSTEM_HANG_TRACER_ENABLE
        /* make sure the IDLE time does not overflow the wdt timeout value. */
        uint32_t sleep_time_sec = ((xExpectedIdleTime) / configTICK_RATE_HZ);
        extern uint32_t systemhang_wdt_timeout;
        if (sleep_time_sec > (systemhang_wdt_timeout - 10))
        {
            sleep_time_sec = (systemhang_wdt_timeout - 10);
            xExpectedIdleTime = sleep_time_sec * configTICK_RATE_HZ;
            /* maybe xExpectedIdleTime is still larger than OS GPT reload value, it will be cut again */
            /* in any case, wdt timeout value must be larger than IDLE time */
        }
        /* feed wdt to keep time for idle sleep */
        systemhang_wdt_feed_in_task_switch();
        /* after here, wdt is in interrupt mode for prevent sleep(IDLE) flow hang */
#endif /* MTK_SYSTEM_HANG_TRACER_ENABLE */

        ulReloadValue = ((xExpectedIdleTime - 1UL) * (1000 / configTICK_RATE_HZ));

        if (ulReloadValue > 0) {
            hal_sleep_manager_set_sleep_time((uint32_t)ulReloadValue);
            hal_sleep_manager_enter_sleep_mode(HAL_SLEEP_MODE_IDLE);
        }

#ifdef MTK_SYSTEM_HANG_TRACER_ENABLE
        /* update safe duration */
        systemhang_set_safe_duration(SYSTEMHANG_USER_CONFIG_COUNT_TOTAL - 1, 60*20);
        /* feed wdt */
        systemhang_wdt_feed_in_task_switch();
#endif /* MTK_SYSTEM_HANG_TRACER_ENABLE */

        /* Calculate and Calibration Idle Time to OS Tick */
        doIdelSystickCalibration(xExpectedIdleTime);

        /* Restart OS GPT. */
        //os_gpt0_resume(false, 0);

        /* Re-enable interrupts - see comments above the cpsid instruction() above. */
        __asm volatile("cpsie i");

        log_debug_tickless("\r\nST_CPT=%u\r\n"   , (unsigned int)xExpectedIdleTime);
    }
}
#endif

GPT_REGISTER_T *os_gpt0 = OS_GPT0;
OS_GPT_REGISTER_GLOABL_T *os_gpt_glb = OS_GPTGLB;
bool reset_gpt_to_systick = false;

extern void xPortSysTickHandler(void);
void os_gpt_interrupt_handle(hal_nvic_irq_t irq_number)
{
    os_gpt0->GPT_IRQ_ACK = 0x01;

    /* Run FreeRTOS tick handler*/
    xPortSysTickHandler();
}

void os_gpt_init(uint32_t ms)
{
    /* set 13 divided with 13M source */
    os_gpt0->GPT_CON_UNION.GPT_CON |= (1 << 16);   // disable clock before config
    os_gpt0->GPT_CLK = 0xc;
    os_gpt0->GPT_CON_UNION.GPT_CON &= ~(1 << 16);   // enable clock
    os_gpt0->GPT_COMPARE = ms * 1000;

    /* clear */
    os_gpt0->GPT_CLR = 0x01;
    while (os_gpt0->GPT_COUNT);

    /* enable IRQ */
    os_gpt0->GPT_IRQ_EN = 0x1;
    /* enable GPT0 clk and repeat mode and enable GPT0 */
    os_gpt0->GPT_CON_UNION.GPT_CON |= 0x101;

    /* register and enable IRQ */
    hal_nvic_register_isr_handler(OS_GPT_IRQn, (hal_nvic_isr_t)os_gpt_interrupt_handle);
    NVIC_EnableIRQ(OS_GPT_IRQn);
    os_gpt_glb->OS_GPT_IRQMSK_CM4 &= 0x2;
    //os_gpt_glb->OS_GPT_WAKEUPMSK &= 0x2;  // mask as system will dead when boot-up, must unmask after sleep<->wake is ok
    //os_gpt_glb->OS_GPT_WAKEUPMSK_CM4 &= 0x2;  // mask as system will dead when boot-up, must unmask after sleep<->wake is ok
}

void os_gpt_start_ms_for_tickless(uint32_t timeout_time_ms)
{
    //uint32_t mask;

    if(timeout_time_ms > 130150523 )
    {
        assert(0);
    }

    //hal_nvic_save_and_set_interrupt_mask(&mask);
    os_gpt0->GPT_IRQ_ACK = 0x1;                   //clear interrupt status
    os_gpt0->GPT_CON_UNION.GPT_CON |= (1 << 16);  // disabled the clock source
    os_gpt0->GPT_CLK= 0x10;                       // set the 32k frequence
    os_gpt0->GPT_CON_UNION.GPT_CON &= ~(1 << 16);   //enable the clock source
    os_gpt0->GPT_CLR      = 0x1;                    // clear the count
    while (os_gpt_glb->OS_GPT_CLRSTA & 0x1);

    os_gpt0->GPT_CON_UNION.GPT_CON &= ~(3<<8);    // set the one-shot mode
    os_gpt0->GPT_IRQ_EN =0x1;                     // enable the irq
    os_gpt0->GPT_COMPARE= (timeout_time_ms * 32 + (7 * timeout_time_ms) / 10 + (6 * timeout_time_ms) / 100 + (8 * timeout_time_ms) / 1000);
    while (os_gpt_glb->OS_GPT_WCOMPSTA & 0x1);
    os_gpt0->GPT_CON_UNION.GPT_CON |= 0x01;



    //hal_nvic_disable_irq(OS_GPT_IRQn);
    // this callback should not execute, because tickless will clear & stop this timer before unmask all IRQ when exit sleep
    hal_nvic_register_isr_handler(OS_GPT_IRQn, NULL);
    hal_nvic_enable_irq(OS_GPT_IRQn);

       os_gpt_glb->OS_GPT_IRQMSK_CM4 &= 0x2;  //CM4 IRQ enable
       os_gpt_glb->OS_GPT_WAKEUPMSK_CM4 &= 0x2;  //CM4 wakeup enable

       //hal_nvic_restore_interrupt_mask(mask);
    return ;
}

void os_gpt_stop_for_tickless()
{

    //uint32_t mask;
    log_debug_tickless("Wake Up From Sleep For CM4,OS GPT  g_compare_value:0x%x,os_gpt0 ->GPT_IRQ_STA:0x%x \r\n",
       os_gpt0->GPT_COMPARE,os_gpt0 ->GPT_IRQ_STA);
    //hal_nvic_save_and_set_interrupt_mask(&mask);

    /*diable interrupt*/
    os_gpt0->GPT_IRQ_EN &= 0x0;
    /* stop timer */
    os_gpt0->GPT_CON_UNION.GPT_CON_CELLS.EN = 0x0;
    os_gpt0->GPT_IRQ_ACK = 0x1;                    /* clean interrupt status */
    os_gpt0->GPT_IRQ_EN = 0;                       /* disable interrupt */
    os_gpt0->GPT_CON_UNION.GPT_CON = 0;            /* disable timer     */
    os_gpt0->GPT_CLR = 0x1;                       /* clear counter value */
    while (os_gpt_glb->OS_GPT_CLRSTA & 0x1);
    os_gpt0->GPT_CLK = 0xc;

    //hal_nvic_restore_interrupt_mask(mask);
    os_gpt_init(portTICK_PERIOD_MS);
    return ;

}

void os_gpt0_pause(void)
{
    //os_gpt0->GPT_CON_UNION.GPT_CON &= 0xFFFFFFFE;
}

void os_gpt0_resume(bool update, uint32_t new_compare)
{
    /*if (update) {
        reset_gpt_to_systick = true;
        os_gpt0->GPT_COMPARE = new_compare;
    }
    os_gpt0->GPT_CON_UNION.GPT_CON |= 0x00000001;*/
}

TimerHandle_t xTimerofTest;
void log_cup_resource_callback(TimerHandle_t pxTimer);

void vPortSetupTimerInterrupt(void)
{
    os_gpt_init(portTICK_PERIOD_MS); /* 1tick = 1ms */

#if configUSE_TICKLESS_IDLE != 0
    /* Calculate the constants required to configure the tick interrupt. */
    {
        /* OS GPT one count equal 1us */
        ulTimerCountsForOneTick = (1000000 / configTICK_RATE_HZ);
        /* OS GPT is 32 bits timer */
        xMaximumPossibleSuppressedTicks = 0xFFFFFFFF / ulTimerCountsForOneTick;
    }
#endif /* configUSE_TICKLESS_IDLE  != 0*/
#if !defined(MTK_OS_CPU_UTILIZATION_ENABLE) || defined(SLEEP_MANAGEMENT_DEBUG_ENABLE)
    xTimerofTest = xTimerCreate("TimerofTest", (5 * 1000 / portTICK_PERIOD_MS), pdTRUE, NULL, log_cup_resource_callback);
    xTimerStart(xTimerofTest, 0);
#endif /* !defined(MTK_OS_CPU_UTILIZATION_ENABLE) || defined(SLEEP_MANAGEMENT_DEBUG_ENABLE) */
}


void log_cup_resource_callback(TimerHandle_t pxTimer)
{
#ifndef MTK_OS_CPU_UTILIZATION_ENABLE
    TaskStatus_t *pxTaskStatusArray;
    UBaseType_t uxArraySize, x;
    uint32_t ulTotalTime;
    static uint32_t ulTotalTime_now = 0,ulTotalTime_last = 0;
    float Percentage;

    /* Optionally do something if the pxTimer parameter is NULL. */
    configASSERT( pxTimer );

    uxArraySize = uxTaskGetNumberOfTasks();

    pxTaskStatusArray = pvPortMalloc( uxArraySize * sizeof( TaskStatus_t ) );

    uxArraySize = uxTaskGetSystemState( pxTaskStatusArray, uxArraySize, &ulTotalTime );

    ulTotalTime_now = ulTotalTime;

    if(ulTotalTime_now > ulTotalTime_last){
        ulTotalTime = ulTotalTime_now - ulTotalTime_last;
    }else {
        ulTotalTime = ulTotalTime_now;
    }
    ulTotalTime_last = ulTotalTime_now;

    if( ulTotalTime > 0UL )
    {
        log_hal_info("ulTotalTimeL:%lu\r\n", ulTotalTime);
        log_hal_info("CM4:%lu\r\n", hal_dvfs_get_cpu_frequency());
        log_hal_info("----------------------CM4 Dump OS Task Info-----------------------------\r\n");
        for( x = 0; x < uxArraySize; x++ )
        {
            Percentage = ((float)pxTaskStatusArray[x].ulRunTimeCounter) / ((float)ulTotalTime);
            log_hal_info("Task[%s] State[%lu] Percentage[%lu.%lu] MinStack[%lu] RunTime[%lu]\r\n"
                                                        , pxTaskStatusArray[x].pcTaskName
                                                        , pxTaskStatusArray[x].eCurrentState
                                                        , (uint32_t)(Percentage*100)
                                                        , ((uint32_t)(Percentage*1000))%10
                                                        , pxTaskStatusArray[x].usStackHighWaterMark
                                                        , pxTaskStatusArray[x].ulRunTimeCounter
                                                        );
        }
        log_hal_info("----------------------------------------------------------------------\r\n");
        vPortFree(pxTaskStatusArray);
    }
    vTaskClearTaskRunTimeCounter();
#endif /* MTK_OS_CPU_UTILIZATION_ENABLE */
#ifdef  SLEEP_MANAGEMENT_DEBUG_ENABLE
    sleep_management_debug_dump_lock_sleep_time();
#endif
}

#if configUSE_TICKLESS_IDLE == 2
uint32_t get_count_sleep_time_us ()
{
    return count_sleep_time_us;
}

uint32_t get_count_idle_time_us ()
{
    return count_idle_time_us;
}

void tickless_start_count_idle_ratio ()
{
    count_idle = TRUE;
    count_sleep_time_us = 0;
    count_idle_time_us = 0;
}

void tickless_stop_count_idle_ratio ()
{
    count_idle = FALSE;
}
#endif