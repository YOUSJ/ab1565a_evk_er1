/* Copyright Statement:
 *
 * (C) 2020  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __ANC_CONTROL_API_H__
#define __ANC_CONTROL_API_H__

#ifdef MTK_ANC_ENABLE
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "hal_audio.h"

#define PACKED  __attribute__((packed))

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MTK_ANC_V2
#define MTK_ANC_V2
#endif

#define ANC_UNASSIGNED_GAIN         (0x7FFF)
#define ANC_BIQUAD_BAND_NUM_MAX     (9)
#define ANC_DEQ_BIQUAD_BAND_NUM_MAX (8)

#define FROM_ATCI    (1)
#define FROM_RACE    (2)
#define FROM_AM      (3)
typedef uint8_t anc_from_user_t;

#define TO_ITSELF    (1)
#define TO_THEOTHER  (2)
#define TO_BOTH      (3)
typedef uint8_t anc_to_role_t;

#define CB_LEVEL_ALL             (0)
#define CB_LEVEL_SUCCESS_ONLY    (1)
typedef uint8_t anc_callback_level_t;

#define ANC_RAMP_DLY_16US   (0)
#define ANC_RAMP_DLY_32US   (1)
#define ANC_RAMP_DLY_64US   (2)
#define ANC_RAMP_DLY_128US  (3)
typedef uint8_t anc_ramp_dly_t;

#define ANC_RAMP_STEP_00125DB   (0)
#define ANC_RAMP_STEP_0025DB    (1)
#define ANC_RAMP_STEP_005DB     (2)
#define ANC_RAMP_STEP_01DB      (3)
#define ANC_RAMP_STEP_02DB      (4)
#define ANC_RAMP_STEP_04DB      (5)
#define ANC_RAMP_STEP_08DB      (6)
#define ANC_RAMP_STEP_16DB      (7)
#define ANC_RAMP_STEP_32DB      (8)
#define ANC_RAMP_STEP_64DB      (9)
typedef uint8_t anc_ramp_step_t;

#define ANC_FLASH_ID_1          (1)
#define ANC_FLASH_ID_2          (2)
#define ANC_FLASH_ID_3          (3)
#define ANC_FLASH_ID_4          (4)
#define ANC_FLASH_ID_5          (5)
#define ANC_FLASH_ID_6          (6)
#define ANC_FLASH_ID_7          (7)
#define ANC_FLASH_ID_USER_DEFINED (0xFF)
typedef uint8_t anc_flash_id_t;

#define ANC_DG_GAIN_MAX         (600)   //+6dB
#define ANC_GAIN_MAX            (1800)  //+18dB
#define ANC_RAMP_GAIN_MAX       (600)   //+6dB
#define ANC_RAMP_GAIN_MIN       (-9000) //-90dB
typedef int16_t anc_gain_t;

#define ANC_SRAM_MASK_FILTER_0_SRAM_0    (0 << 0)
#define ANC_SRAM_MASK_FILTER_0_SRAM_1    (1 << 0)
#define ANC_SRAM_MASK_FILTER_1_SRAM_0    (0 << 2)
#define ANC_SRAM_MASK_FILTER_1_SRAM_1    (1 << 2)
#define ANC_SRAM_MASK_FILTER_2_SRAM_0    (0 << 8)
#define ANC_SRAM_MASK_FILTER_2_SRAM_1    (1 << 8)
#define ANC_SRAM_MASK_FILTER_3_SRAM_0    (0 << 10)
#define ANC_SRAM_MASK_FILTER_3_SRAM_1    (1 << 10)
#define ANC_SRAM_MASK_FILTER_4_SRAM_0    (0 << 16)
#define ANC_SRAM_MASK_FILTER_4_SRAM_1    (1 << 16)
#define ANC_SRAM_MASK_FILTER_5_SRAM_0    (0 << 18)
#define ANC_SRAM_MASK_FILTER_5_SRAM_1    (1 << 18)
#define ANC_SRAM_MASK_FILTER_ALL_SRAM_0  (ANC_SRAM_MASK_FILTER_0_SRAM_0 | ANC_SRAM_MASK_FILTER_1_SRAM_0 | ANC_SRAM_MASK_FILTER_2_SRAM_0 | ANC_SRAM_MASK_FILTER_3_SRAM_0 | ANC_SRAM_MASK_FILTER_4_SRAM_0 | ANC_SRAM_MASK_FILTER_5_SRAM_0)
#define ANC_SRAM_MASK_FILTER_ALL_SRAM_1  (ANC_SRAM_MASK_FILTER_0_SRAM_1 | ANC_SRAM_MASK_FILTER_1_SRAM_1 | ANC_SRAM_MASK_FILTER_2_SRAM_1 | ANC_SRAM_MASK_FILTER_3_SRAM_1 | ANC_SRAM_MASK_FILTER_4_SRAM_1 | ANC_SRAM_MASK_FILTER_5_SRAM_1)
#define ANC_SRAM_MASK_ANC_SRAM_MASK      (0x3)
typedef uint32_t anc_filter_sram_mask_t;

/**
 * @addtogroup Audio
 * @{
 * @addtogroup ANC_and_Pass-through
 * @{
 *
 * The ANC is used for noice cancellation.
 * The pass-through is used for ambient sound.
 *
 */

/** @defgroup anc_enum Enum
  * @{
  */

/** @brief ANC&Pass-through control return values. */
typedef enum {
    ANC_CONTROL_EXECUTION_NONE    = -2,
    ANC_CONTROL_EXECUTION_FAIL    = -1,
    ANC_CONTROL_EXECUTION_SUCCESS =  0,
    ANC_CONTROL_EXECUTION_CANCELLED = 1,
    ANC_CONTROL_EXECUTION_NOT_ALLOWED = 2,
} anc_control_result_t;
/**
  * @}
  */

typedef enum {
    ANC_CONTROL_EVENT_ON                = 1 << 0,
    ANC_CONTROL_EVENT_OFF               = 1 << 1,
    ANC_CONTROL_EVENT_COPY_FILTER       = 1 << 2,
    ANC_CONTROL_EVENT_SET_REG           = 1 << 3,
} anc_control_event_t;

typedef enum {
    ANC_HYBRID_TYPE = 0,
    ANC_FF_TYPE = 1,
    ANC_FB_TYPE = 2,
    ANC_USER_DEFINED_TYPE = 3,
    ANC_PASSTHRU_TYPE_FF = 4,
    ANC_PASSTHRU_TYPE_NUM,

    ANC_TYPE_DUMMY = 0x7FFFFFFF,
} anc_type_t;

typedef enum {
    FILTER_FF_L = 1 << 0,
    FILTER_FF_R = 1 << 1,
    FILTER_FF_STEREO = FILTER_FF_L | FILTER_FF_R,
    FILTER_FF = FILTER_FF_STEREO,

    FILTER_FB_L = 1 << 2,
    FILTER_FB_R = 1 << 3,
    FILTER_FB_STEREO = FILTER_FB_L | FILTER_FB_R,
    FILTER_FB = FILTER_FB_STEREO,

    FILTER_DEQ_L = 1 << 4,
    FILTER_DEQ_R = 1 << 5,
    FILTER_DEQ_STEREO = FILTER_DEQ_L | FILTER_DEQ_R,
    FILTER_DEQ = FILTER_DEQ_STEREO,

    FILTER_ALL = FILTER_FF_STEREO | FILTER_FB_STEREO | FILTER_DEQ_STEREO,

    FILTER_DUMMY = 0x7FFFFFFF,
} anc_filter_t;

typedef enum {
    ANC_SRAM_BANK_0 = 0,
    ANC_SRAM_BANK_1,
    ANC_SRAM_DUMMY = 0xFF,
} anc_sram_bank_t;

typedef enum {
    ANC_FILTER_0_SRAM_0 = 0 << 0,
    ANC_FILTER_0_SRAM_1 = 1 << 0,
    ANC_FILTER_1_SRAM_0 = 0 << 2,
    ANC_FILTER_1_SRAM_1 = 1 << 2,
    ANC_FILTER_2_SRAM_0 = 0 << 8,
    ANC_FILTER_2_SRAM_1 = 1 << 8,
    ANC_FILTER_3_SRAM_0 = 0 << 10,
    ANC_FILTER_3_SRAM_1 = 1 << 10,
    ANC_FILTER_4_SRAM_0 = 0 << 16,
    ANC_FILTER_4_SRAM_1 = 1 << 16,
    ANC_FILTER_5_SRAM_0 = 0 << 18,
    ANC_FILTER_5_SRAM_1 = 1 << 18,
    ANC_FILTER_ALL_SRAM_0 = ANC_FILTER_0_SRAM_0 | ANC_FILTER_1_SRAM_0 | ANC_FILTER_2_SRAM_0 | ANC_FILTER_3_SRAM_0 | ANC_FILTER_4_SRAM_0 | ANC_FILTER_5_SRAM_0,
    ANC_FILTER_ALL_SRAM_1 = ANC_FILTER_0_SRAM_1 | ANC_FILTER_1_SRAM_1 | ANC_FILTER_2_SRAM_1 | ANC_FILTER_3_SRAM_1 | ANC_FILTER_4_SRAM_1 | ANC_FILTER_5_SRAM_1,
    ANC_SRAM_MASK = 0x3,

    ANC_FILTER_SRAM_DUMMY = 0x7FFFFFFF,
} anc_sram_bank_mask_t;

typedef enum {
    ANC_REG_DGAIN           = 1 << 0,
    ANC_REG_BIT_SHIFT       = 1 << 1,
    ANC_REG_BQ_GAIN         = 1 << 2,
    ANC_REG_RAMP_GAIN       = 1 << 3,
    ANC_REG_RAMP_CAP        = 1 << 4,
    ANC_REG_LM_THD_L        = 1 << 5,
    ANC_REG_LM_THD_H        = 1 << 6,
    ANC_REG_LM_AT_TIME      = 1 << 7,
    ANC_REG_LM_RS_TIME      = 1 << 8,
    ANC_REG_DEQ_DLY_TIME    = 1 << 9,
    ANC_REG_REALTIME_UPDATE = 1 << 10,
    ANC_REG_DEBUG_MODE      = 1 << 11,
} anc_set_reg_t;

typedef struct anc_biquad_s {
    uint32_t b0;    //Q31
    uint32_t b1;    //Q1.30
    uint32_t b2;    //Q31
    uint32_t a1;    //Q1.30
    uint32_t a2;    //Q31
} PACKED anc_biquad_t;

typedef struct {
    uint16_t flag;
    uint16_t band_num;
    uint32_t biquad_out_gain;
    anc_biquad_t biquad[ANC_BIQUAD_BAND_NUM_MAX];
} PACKED anc_biquad_coef_t;

typedef struct {
    uint16_t flag;
    uint16_t band_num;
    uint32_t biquad_out_gain;
    anc_biquad_t biquad[ANC_DEQ_BIQUAD_BAND_NUM_MAX];
} PACKED anc_deq_biquad_coef_t;

typedef struct anc_ramp_cap_s {
    anc_ramp_dly_t  delay_time;
    anc_ramp_step_t ramp_up_dly_step;
    anc_ramp_step_t ramp_dn_dly_step;
} anc_ramp_cap_t;

typedef union {
    anc_gain_t     Dgain;
    uint8_t        bit_shift;
    anc_gain_t     bq_gain;
    anc_gain_t     ramp_gain;
    anc_ramp_cap_t ramp;
    uint32_t       limiter_threshold;
    uint32_t       limiter_attack_time;
    uint32_t       limiter_release_time;
    uint8_t        deq_delay;
    uint8_t        realtime_update_ch;
}anc_reg_param_union;

typedef struct anc_filter_cap_s {
    anc_type_t             type;
    anc_flash_id_t         flash_id;
    anc_sram_bank_t        sram_bank;
    anc_filter_t           filter_mask;
    anc_filter_sram_mask_t sram_bank_mask;
    anc_set_reg_t          set_reg_mask;
    anc_biquad_coef_t     *filter_coef;
    anc_reg_param_union    param;
} anc_filter_cap_t;

typedef struct anc_control_misc_s {
    bool     suspend;
    bool     backup_nvdm;
} anc_control_misc_t;

typedef struct anc_control_cap_s {
    anc_to_role_t      role;
    anc_filter_cap_t   filter_cap;
    anc_control_misc_t control_misc;
} anc_control_cap_t;

typedef void (*anc_control_callback_t)(anc_control_event_t event_id, anc_control_result_t result);

/* Main APIs */
anc_control_result_t audio_anc_init(void);
anc_control_result_t audio_anc_copy_filter_from_flash(anc_to_role_t to_role, anc_filter_t filter, anc_flash_id_t flash_id, anc_sram_bank_t sram_bank);
anc_control_result_t audio_anc_copy_filter(anc_to_role_t to_role, anc_filter_t filter, anc_sram_bank_t sram_bank, anc_biquad_coef_t *biquad_coef_ptr);
anc_control_result_t audio_anc_copy_deq_filter(anc_to_role_t to_role, anc_filter_t filter, anc_sram_bank_t sram_bank, anc_deq_biquad_coef_t *deq_biquad_coef_ptr);
anc_control_result_t audio_anc_enable(anc_to_role_t to_role, anc_filter_t filter, anc_filter_sram_mask_t sram_bank_mask);
anc_control_result_t audio_anc_enable_by_type(anc_to_role_t to_role, anc_flash_id_t flash_id, anc_type_t type);
anc_control_result_t audio_anc_disable(anc_to_role_t to_role);
anc_control_result_t audio_anc_get_current_status(anc_to_role_t to_role, anc_flash_id_t *flash_id, anc_type_t *type, anc_filter_t *filter, anc_sram_bank_mask_t *sram_bank_mask);
anc_control_result_t audio_anc_register_callback(anc_control_callback_t callback, anc_control_event_t event_mask, anc_callback_level_t level);
anc_control_result_t audio_anc_deregister_callback(anc_control_callback_t callback);

/* Other APIs for register setting */
anc_control_result_t audio_anc_set_reg_digital_gain(anc_to_role_t to_role, anc_filter_t filter, anc_gain_t value); //for calibrate gain
anc_control_result_t audio_anc_set_reg_bit_shift(anc_to_role_t to_role, anc_filter_t filter, uint8_t value);       //value:0/1/2
anc_control_result_t audio_anc_set_reg_gain(anc_to_role_t to_role, anc_filter_t filter, anc_gain_t value);        //for filter uniqe gain
anc_control_result_t audio_anc_set_reg_ramp_gain(anc_to_role_t to_role, anc_filter_t filter, anc_gain_t value); //for runtime volume
anc_control_result_t audio_anc_set_reg_ramp_rate(anc_to_role_t to_role, anc_filter_t filter, anc_ramp_dly_t switch_dly, anc_ramp_step_t up_gain_step, anc_ramp_step_t down_gain_step);
anc_control_result_t audio_anc_set_reg_limiter_threshold_low(anc_to_role_t to_role, anc_filter_t filter, uint32_t value);
anc_control_result_t audio_anc_set_reg_limiter_threshold_high(anc_to_role_t to_role, anc_filter_t filter, uint32_t value);
anc_control_result_t audio_anc_set_reg_limiter_attack_time(anc_to_role_t to_role, anc_filter_t filter, uint32_t value);
anc_control_result_t audio_anc_set_reg_limiter_release_time(anc_to_role_t to_role, anc_filter_t filter, uint32_t value);
anc_control_result_t audio_anc_set_reg_deq_delay(anc_to_role_t to_role, anc_filter_t filter, uint8_t value); //value:0~7 (delay value*2.65us)

/* Other APIs for MP calibration */
anc_control_result_t audio_anc_read_calibrate_gain_nvdm (anc_to_role_t to_role, anc_filter_t filter, anc_gain_t *gain);
anc_control_result_t audio_anc_write_calibrate_gain_nvdm (anc_to_role_t to_role, anc_filter_t filter, anc_gain_t gain);
uint8_t audio_anc_get_attach_enable(void);
void audio_anc_set_attach_enable(uint8_t enable);
uint16_t audio_anc_get_sync_time(void);
void audio_anc_set_sync_time(uint16_t sync_time);

/* ANC Entry for SDK. */
anc_control_result_t audio_anc_command_handler(anc_from_user_t source, anc_control_event_t event, void* command);

/**
 * @brief     This function re-initializes the nvkey memory for ANC and pass-through.
 * @return    Return status of this API call. Please refer to #anc_control_result_t.
 */
anc_control_result_t anc_reinit_nvdm(void);
/**
* @}
* @}
*/

#ifdef __cplusplus
}
#endif

#endif  /*MTK_ANC_ENABLE*/
#endif  /*__ANC_CONTROL_API_H__*/

