/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "app_power_saving_utils.h"
#include "apps_customer_config.h"
#ifdef MTK_ANC_ENABLE
#ifdef MTK_ANC_V2
  #include "anc_control_api.h"
#else
  #include "anc_control.h"
#endif
#endif
#include "apps_debug.h"
#include "apps_config_features_dynamic_setting.h"
#include "bt_power_on_config.h"
#include "bt_device_manager.h"
#include "ui_shell_manager.h"
#include "apps_events_event_group.h"
#include "apps_events_interaction_event.h"
#include "FreeRTOS.h"

#define LOG_TAG     "[POWER_SAVING][UTILS] "

typedef struct _app_power_saving_utils_modules {
    get_power_saving_target_mode_func_t get_mode_func;
    struct _app_power_saving_utils_modules *next;
} app_power_saving_utils_modules_t;

app_power_saving_utils_modules_t *s_top_module = NULL;
static bool s_started = false;

bool app_power_saving_utils_add_new_callback_func(get_power_saving_target_mode_func_t callback_func)
{
    bool ret = true;
    app_power_saving_utils_modules_t *power_saving_module;
    app_power_saving_utils_modules_t **p_new_module = NULL;

    if (NULL == s_top_module) {
        p_new_module = &s_top_module;
    }
    for (power_saving_module = s_top_module;
            power_saving_module != NULL;
            power_saving_module = power_saving_module->next) {
        if (callback_func == power_saving_module->get_mode_func) {
            APPS_LOG_MSGID_I(LOG_TAG" Already register get_mode_callback=%x",
                    1, callback_func);
            break;
        } else if (NULL == power_saving_module->next) {
            p_new_module = &(power_saving_module->next);
        }
    }
    if (p_new_module) {
        *p_new_module = (app_power_saving_utils_modules_t *)
                pvPortMalloc(sizeof(app_power_saving_utils_modules_t));
        if (*p_new_module) {
            memset(*p_new_module, 0, sizeof(app_power_saving_utils_modules_t));
            (*p_new_module)->get_mode_func = callback_func;
        } else {
            ret = false;
            APPS_LOG_MSGID_I(LOG_TAG" Fail to register get_mode_callback, =%x",
                    1, callback_func);
        }
    }
    return ret;
}

app_power_saving_target_mode_t app_power_saving_utils_get_tagert_mode(ui_shell_activity_t *self)
{
    /* When all of the below status is true, need do power off:
    1. Device is out of case;
    2. Power on and not connected;
    3. ANC/PassThrough is off;
    */
    app_power_saving_target_mode_t target_mode = APP_POWER_SAVING_TARGET_MODE_NORMAL;
    app_power_saving_target_mode_t temp_mode = APP_POWER_SAVING_TARGET_MODE_NORMAL;
    app_power_saving_context_t *local_context = NULL;
    app_power_saving_utils_modules_t *power_saving_module;

    s_started = true;

    if (BT_POWER_ON_NORMAL != bt_power_on_get_config_type()) {
        return APP_POWER_SAVING_TARGET_MODE_NORMAL;
    }

    if (self && self->local_context) {
        local_context = self->local_context;
#if (APPS_POWER_SAVING_MODE == APPS_POWER_SAVING_SYSTEM_OFF)
        if (apps_config_features_is_no_connection_sleep_mode_bt_off()) {
            if (BT_SINK_SRV_STATE_POWER_ON == local_context->bt_sink_srv_state) {
                target_mode = APP_POWER_SAVING_TARGET_MODE_BT_OFF;
            }
        } else {
            if (BT_SINK_SRV_STATE_CONNECTED > local_context->bt_sink_srv_state) {
                target_mode = APP_POWER_SAVING_TARGET_MODE_SYSTEM_OFF;
            }
        }
#elif (APPS_POWER_SAVING_MODE == APPS_POWER_SAVING_DISABLE_BT)
        if (BT_SINK_SRV_STATE_POWER_ON == local_context->bt_sink_srv_state) {
            target_mode = APP_POWER_SAVING_TARGET_MODE_BT_OFF;
        }
#endif

        for (power_saving_module = s_top_module;
                power_saving_module != NULL && target_mode > APP_POWER_SAVING_TARGET_MODE_NORMAL;
                power_saving_module = power_saving_module->next) {
            if (power_saving_module->get_mode_func) {
                temp_mode = power_saving_module->get_mode_func();
            } else {
                APPS_LOG_MSGID_E(LOG_TAG" get_tagert_mode, power_saving_module->get_mode_func = NULL", 0);
            }
            if (target_mode > temp_mode) {
                target_mode = temp_mode;
            }
        }
    }

    return target_mode;
}

bool app_power_saving_utils_notify_mode_changed(get_power_saving_target_mode_func_t callback_func)
{
    bool ret = true;
    ui_shell_status_t send_ret = ui_shell_send_event(false,
            EVENT_PRIORITY_HIGNEST,
            EVENT_GROUP_UI_SHELL_POWER_SAVING,
            APP_POWER_SAVING_EVENT_NOTIFY_CHANGE,
            NULL, 0, NULL, 0);
    if (UI_SHELL_STATUS_OK != send_ret) {
        APPS_LOG_MSGID_E(LOG_TAG" _notify_mode_changed, Fail(%d) to send POWER_SAVING_ALLOWANCE_CHANGE to =%x",
                2, send_ret, callback_func);
        ret = false;
    }
    return ret;
}

bool app_power_saving_utils_register_get_mode_callback(get_power_saving_target_mode_func_t callback_func)
{
    bool ret = true;

    ui_shell_status_t send_ret = UI_SHELL_STATUS_INVALID_STATE;
    if (s_started) {
        send_ret = ui_shell_send_event(false,
                EVENT_PRIORITY_HIGNEST,
                EVENT_GROUP_UI_SHELL_POWER_SAVING,
                APP_POWER_SAVING_EVENT_NOTIFY_CHANGE,
                callback_func, 0, NULL, 0);
    }

    if (UI_SHELL_STATUS_INVALID_STATE == send_ret) {
        APPS_LOG_MSGID_I(LOG_TAG" Register get_mode_callback in main=%x", 1, callback_func);
        ret = app_power_saving_utils_add_new_callback_func(callback_func);
    } else if (UI_SHELL_STATUS_OK != send_ret) {
        APPS_LOG_MSGID_E(LOG_TAG" register_get_mode_callback, Fail(%d) to send POWER_SAVING_ALLOWANCE_CHANGE to =%x",
                2, send_ret, callback_func);
        ret = false;
    } else {
        APPS_LOG_MSGID_I(LOG_TAG" register_get_mode_callback, Send POWER_SAVING_ALLOWANCE_CHANGE to =%x", 1, callback_func);
    }
    return ret;
}

void app_power_saving_utils_update_bt_state(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    app_power_saving_context_t *local_context = (app_power_saving_context_t *)self->local_context;
#if defined(MTK_AWS_MCE_ENABLE)
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
#endif

    switch (event_id) {
        case BT_CM_EVENT_POWER_STATE_UPDATE:
        {
            bt_cm_power_state_update_ind_t *power_update = (bt_cm_power_state_update_ind_t *)extra_data;
            if (!power_update || !local_context) {
                break;
            }
            
            if (APP_POWER_SAVING_BT_OFF != local_context->bt_sink_srv_state
                    && BT_CM_POWER_STATE_OFF == power_update->power_state) {
                local_context->bt_sink_srv_state = APP_POWER_SAVING_BT_OFF;
                APPS_LOG_MSGID_I(LOG_TAG"bt_state = %d", 1, local_context->bt_sink_srv_state);
            } else if (APP_POWER_SAVING_BT_OFF == local_context->bt_sink_srv_state
                    && BT_CM_POWER_STATE_ON == power_update->power_state) {
                local_context->bt_sink_srv_state = APP_POWER_SAVING_BT_DISCONNECTED;
                APPS_LOG_MSGID_I(LOG_TAG"bt_state = %d", 1, local_context->bt_sink_srv_state);
            }
        }
            break;
        case BT_CM_EVENT_REMOTE_INFO_UPDATE:
        {
            bt_cm_remote_info_update_ind_t *remote_update = (bt_cm_remote_info_update_ind_t *)extra_data;
            if (!remote_update || !local_context) {
                break;
            }

#if defined(MTK_AWS_MCE_ENABLE)
            if (BT_AWS_MCE_ROLE_AGENT == role || BT_AWS_MCE_ROLE_NONE == role)
#endif
            {
                if (BT_CM_ACL_LINK_DISCONNECTED != remote_update->pre_acl_state
                        && BT_CM_ACL_LINK_DISCONNECTED == remote_update->acl_state) {
                    if (APP_POWER_SAVING_BT_DISCONNECTED != local_context->bt_sink_srv_state
                            && 0 == bt_cm_get_connected_devices(~BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS), NULL, 0)) {
                        local_context->bt_sink_srv_state = APP_POWER_SAVING_BT_DISCONNECTED;
                    }
                } else if (!((~BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS)) & remote_update->pre_connected_service)
                        && (~BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS)) & remote_update->connected_service) {
                    if (APP_POWER_SAVING_BT_CONNECTED != local_context->bt_sink_srv_state) {
                        local_context->bt_sink_srv_state = APP_POWER_SAVING_BT_CONNECTED;
                    }
                }
            }
#if defined(MTK_AWS_MCE_ENABLE)
            else {
                APPS_LOG_MSGID_I(LOG_TAG"connected_service %x -> %x", 2, remote_update->pre_connected_service, remote_update->connected_service);
                if (BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->pre_connected_service
                        && !(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->connected_service)) {
                    local_context->bt_sink_srv_state = APP_POWER_SAVING_BT_DISCONNECTED;
                } else if (!(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->pre_connected_service)
                        && BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->connected_service) {
                    local_context->bt_sink_srv_state = APP_POWER_SAVING_BT_CONNECTED;
                }
            }
#endif
        }
            break;
        default:
            break;
    }
}