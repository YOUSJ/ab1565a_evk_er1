
/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifdef MTK_VA_XIAOAI_ENABLE

#include "app_va_xiaoai_activity.h"
#include "apps_config_event_list.h"
#include "apps_aws_sync_event.h"
#include "apps_events_event_group.h"
#include "apps_events_interaction_event.h"
#include "apps_config_vp_index_list.h"
#include "apps_config_vp_manager.h"
#include "app_voice_prompt.h"
#include "apps_debug.h"
#include "ui_shell_manager.h"

#include "bt_aws_mce_report.h"
#include "bt_customer_config.h"
#include "bt_sink_srv.h"
#include "bt_sink_srv_hf.h"
#include "bt_connection_manager.h"

#include "xiaoai.h"
#include "app_va_xiaoai_ble_adv.h"
#include "app_smcharger_utils.h"

#ifdef MTK_ANC_ENABLE
#include "anc_control.h"
#endif

// XiaoAI AT CMD Option
#define APP_XIAOAI_TEST

#ifdef APP_XIAOAI_TEST
#include "atci.h"

static atci_status_t app_xiaoai_atci_handler(atci_parse_cmd_param_t *parse_cmd);
static atci_cmd_hdlr_item_t app_xiaoai_atci_cmd[] = {
    {
        .command_head = "AT+XIAOAI",
        .command_hdlr = app_xiaoai_atci_handler,
        .hash_value1 = 0,
        .hash_value2 = 0,
    },
};
#endif

#define APP_VA_XIAOAI_TAG           "[XIAOAI_VA]"

static bool g_xiaoai_init_done = FALSE;
static bt_aws_mce_agent_state_type_t g_xiaoai_aws_state = FALSE;
static bt_sink_srv_state_t g_xiaoai_bt_state;

// BLE ADV (start reason)
typedef enum {
    XIAOAI_BLE_ADV_REASON_XIAOAI_CONN = 0,
    XIAOAI_BLE_ADV_REASON_BOTH_LID_CHARGER_IN,
    XIAOAI_BLE_ADV_REASON_BOTH_LID_OPEN,
    XIAOAI_BLE_ADV_REASON_BOTH_LID_CLOSE
} xiaoai_start_ble_adv_reason;
static bool app_va_xiaoai_check_and_send_ble_adv_event(xiaoai_start_ble_adv_reason reason);

// HFP State & Remote Addr for XiaoMI System XiaoAI APP Wake Up
static bt_sink_srv_profile_connection_state_t g_xiaoai_hfp_state;
static uint8_t g_xiaoai_hfp_addr[BT_BD_ADDR_LEN] = {0};

static void app_va_xiaoai_hf_va_activate(bool enable)
{
    bt_status_t bt_status = bt_sink_srv_send_action(BT_SINK_SRV_ACTION_VOICE_RECOGNITION_ACTIVATE,
                                                    (void *)&enable);
    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [WakeUp] hf_va_activate status=%d", 1, bt_status);
}

static void app_va_xiaoai_hf_xiaomi_atcmd()
{
    char *at_cmd = xiaoai_get_miui_at_cmd();
    if (at_cmd != NULL) {
        bt_sink_srv_hf_xiaomi_custom(at_cmd);
        APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [XIAOMI] hf_xiaomi at_cmd", 0);
    }
}

#ifdef MTK_ANC_ENABLE
typedef enum {
    XIAOAI_ANC_DISABLE = 0,
    XIAOAI_ANC_ENABLE,
    XIAOAI_ANC_ENABLE_PASSTHROUGH
} xiaoai_anc_action;

static void app_va_xiaoai_anc_callback(anc_control_event_t event_id, anc_control_result_t result)
{
    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG", [XIAOMI] ANC Callback event_id=%d result=%d",
                     2, event_id, result);
}
#endif

static void app_va_xiaoai_speech_timeout()
{
    xiaoai_connection_state xiaoai_state = xiaoai_get_connection_state();
    bool is_mi_phone = xiaoai_is_mi_phone(g_xiaoai_hfp_addr);
    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [WakeUp] speech_timeout xiaoai_state=%d hfp_state=%d %02X:%02X:%02X:%02X:%02X:%02X (mi=%d)",
                     9, xiaoai_state, g_xiaoai_hfp_state,
                     g_xiaoai_hfp_addr[0], g_xiaoai_hfp_addr[1], g_xiaoai_hfp_addr[2],
                     g_xiaoai_hfp_addr[3], g_xiaoai_hfp_addr[4], g_xiaoai_hfp_addr[5],
                     is_mi_phone);
    if (g_xiaoai_hfp_state == BT_SINK_SRV_PROFILE_CONNECTION_STATE_CONNECTED
            && is_mi_phone) {
        APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [WakeUp] speech_timeout - AT+BVRA", 0);
        app_va_xiaoai_hf_va_activate(TRUE);
    } else {
        // Customer VP: "Please open XiaoAI APP and connect me"
    }
}

static void app_va_xiaoai_active_update_ble_adv(xiaoai_bat_charger_state charger_state)
{
    xiaoai_connection_state xiaoai_state = xiaoai_get_connection_state();
    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [ACTIVE_UPDATE_BLE_ADV] charger_state=%d xiaoai=%d",
                     2, charger_state, xiaoai_state);
    xiaoai_start_ble_adv_reason reason = 0;
    if (charger_state == XIAOAI_BOTH_CHARGER_IN) {
        reason = XIAOAI_BLE_ADV_REASON_BOTH_LID_CHARGER_IN;
    } else if (charger_state == XIAOAI_BOTH_LID_OPEN) {
        reason = XIAOAI_BLE_ADV_REASON_BOTH_LID_OPEN;
    } else if (charger_state == XIAOAI_BOTH_LID_CLOSE) {
        reason = XIAOAI_BLE_ADV_REASON_BOTH_LID_CLOSE;
    }
    // Update BLE ADV due to SMCharger state
    app_va_xiaoai_check_and_send_ble_adv_event(reason);

    // Customer Config: use XiaoMI HFP ATCMD to cancel pop-up window when BOTH_LID_CLOSE
    // If your charger case maybe block earbud's XiaoAI BLE ADV signal to SP
#if 0
    if (charger_state == XIAOAI_BOTH_LID_CLOSE) {
        app_va_xiaoai_hf_xiaomi_atcmd();
    }
#endif
}

static void va_xiaoai_app_wake_up()
{
    xiaoai_connection_state xiaoai_state = xiaoai_get_connection_state();
    bool is_mi_phone = xiaoai_is_mi_phone(g_xiaoai_hfp_addr);
    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [WakeUp] wake_up xiaoai_state=%d hfp_state=%d %02X:%02X:%02X:%02X:%02X:%02X (mi=%d)",
                     9, xiaoai_state, g_xiaoai_hfp_state,
                     g_xiaoai_hfp_addr[0], g_xiaoai_hfp_addr[1], g_xiaoai_hfp_addr[2],
                     g_xiaoai_hfp_addr[3], g_xiaoai_hfp_addr[4], g_xiaoai_hfp_addr[5],
                     is_mi_phone);

    if (xiaoai_state == XIAOAI_STATE_CONNECTED) {
        if (g_xiaoai_hfp_state == BT_SINK_SRV_PROFILE_CONNECTION_STATE_CONNECTED) {
            // nothing -> Do VA Flow
        } else if (g_xiaoai_hfp_state == BT_SINK_SRV_PROFILE_CONNECTION_STATE_CONNECTING) {
            // Customer VP: "Awaking XiaoAI APP"
        } else {
            // Customer VP: "BT Disconnected, please connect me by SP BT Setting"
        }
    } else {
        if (g_xiaoai_hfp_state == BT_SINK_SRV_PROFILE_CONNECTION_STATE_CONNECTED) {
            if (is_mi_phone) {
                APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [WakeUp] wake_up - AT+BVRA", 0);
                app_va_xiaoai_hf_va_activate(TRUE);
            } else {
                // Customer VP: "Please open XiaoAI APP and connect me"
            }
        } else {
            // Customer VP: "BT Disconnected, please connect me by SP BT Setting"
        }
    }
}

static void app_va_xiaoai_action_in_app(xiaoai_app_action action, void *param)
{
    switch (action) {
        case XIAOAI_ACTION_DISCOVERABLE: {
            bt_status_t bt_status = bt_cm_discoverable(true);
            // unbind->disconect->enter discoverable, will start ble adv after xiaoai disconnect
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" app_va_xiaoai_action_in_app - discoverable bt_status=0x%08X",
                             1, bt_status);
            break;
        }
        case XIAOAI_ACTION_RECONNECT_EDR: {
            if (param != NULL) {
                bt_cm_connect_t connect_param = { {0},
                                    BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS)
                                    | BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP)
                                    | BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_A2DP_SINK)
                                    | BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AVRCP) };
                memcpy(connect_param.address, param, sizeof(bt_bd_addr_t));
                bt_status_t bt_status = bt_cm_connect(&connect_param);
                APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" app_va_xiaoai_action_in_app -  Reconnect EDR status=0x%08X",
                             1, bt_status);
            }
            break;
        }
        case XIAOAI_ACTION_DISCONNECT_EDR: {
            bt_cm_connect_t connect_param = {{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}, BT_CM_PROFILE_SERVICE_MASK_ALL};
            bt_status_t bt_status = bt_cm_disconnect(&connect_param);
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" app_va_xiaoai_action_in_app -  Disconnect EDR status=0x%08X",
                             1, bt_status);
            break;
        }
        case XIAOAI_ACTION_ANC: {
    #ifdef MTK_ANC_ENABLE
            anc_control_result_t control_ret = ANC_CONTROL_EXECUTION_NONE;
            int anc_state = (int)param;
            if (anc_state == XIAOAI_ANC_DISABLE) {
                control_ret = audio_anc_disable(app_va_xiaoai_anc_callback);
            } else if (anc_state == XIAOAI_ANC_ENABLE) {
                control_ret = audio_anc_enable(FILTER_1, ANC_UNASSIGNED_GAIN,
                        app_va_xiaoai_anc_callback);
            } else if (anc_state == XIAOAI_ANC_ENABLE_PASSTHROUGH) {
                control_ret = audio_anc_enable(FILTER_5, ANC_UNASSIGNED_GAIN,
                        app_va_xiaoai_anc_callback);
            }
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" app_va_xiaoai action - ANC and=%d ret=%d",
                             2, anc_state, control_ret);
    #endif
            break;
        }
    }
}

static void app_va_xiaoai_action(xiaoai_app_action action, void *param)
{
    ui_shell_status_t ret = UI_SHELL_STATUS_OK;
    switch (action) {
        case XIAOAI_ACTION_POWER_OFF: {
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" app_va_xiaoai action - power_off", 0);
            ret = ui_shell_send_event(FALSE, EVENT_PRIORITY_HIGNEST,
                                      EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                                      APPS_EVENTS_INTERACTION_REQUEST_POWER_OFF,
                                      NULL, 0, NULL, 0);
            break;
        }
        case XIAOAI_ACTION_REBOOT: {
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" app_va_xiaoai action - reboot", 0);
            ret = ui_shell_send_event(FALSE, EVENT_PRIORITY_HIGNEST,
                                      EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                                      APPS_EVENTS_INTERACTION_REQUEST_REBOOT,
                                      NULL, 0, NULL, 0);
            break;
        }
        case XIAOAI_ACTION_DISCOVERABLE: {
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" app_va_xiaoai action - discoverable", 0);
            bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
            if (role == BT_AWS_MCE_ROLE_AGENT
                    && bt_sink_srv_get_state() >= BT_SINK_SRV_STATE_POWER_ON) {
                ret = ui_shell_send_event(FALSE, EVENT_PRIORITY_HIGNEST,
                                          EVENT_GROUP_UI_SHELL_XIAOAI,
                                          XIAOAI_EVENT_DISCOVERABLE_ACTION,
                                          NULL, 0, NULL, 0);
            }
            break;
        }
        case XIAOAI_ACTION_RECONNECT_EDR: {
            bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
            bt_sink_srv_state_t state = bt_sink_srv_get_state();
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" app_va_xiaoai action - Reconnect EDR role=0x%02X state=0x%04X",
                    2, role, state);
            if (role == BT_AWS_MCE_ROLE_AGENT
                    && state >= BT_SINK_SRV_STATE_POWER_ON
                    && param != NULL) {
                uint8_t *bd_addr = pvPortMalloc(BT_BD_ADDR_LEN);
                memcpy(bd_addr, param, BT_BD_ADDR_LEN);
                // UI_Shell will free extra_data when extra_data + data_len != 0
                ret = ui_shell_send_event(FALSE, EVENT_PRIORITY_HIGNEST,
                                          EVENT_GROUP_UI_SHELL_XIAOAI,
                                          XIAOAI_EVENT_RECONNECT_EDR_ACTION,
                                          bd_addr, BT_BD_ADDR_LEN, NULL, 0);
            }
            break;
        }
        case XIAOAI_ACTION_DISCONNECT_EDR: {
            bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
            bt_sink_srv_state_t state = bt_sink_srv_get_state();
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" app_va_xiaoai action - Disconnect EDR role=0x%02X state=0x%04X",
                    2, role, state);
            if (role == BT_AWS_MCE_ROLE_AGENT
                    && state >= BT_SINK_SRV_STATE_POWER_ON) {
                ret = ui_shell_send_event(FALSE, EVENT_PRIORITY_HIGNEST,
                                          EVENT_GROUP_UI_SHELL_XIAOAI,
                                          XIAOAI_EVENT_DISCONNECT_EDR_ACTION,
                                          NULL, 0, NULL, 0);
            }
            break;
        }
        case XIAOAI_ACTION_SPEECH_VP: {
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" app_va_xiaoai action - Speech VP", 0);
            apps_config_set_vp(VP_INDEX_PRESS, FALSE, 0, VOICE_PROMPT_PRIO_MEDIUM, FALSE, NULL);
            break;
        }
#ifdef MTK_ANC_ENABLE
        case XIAOAI_ACTION_ANC: {
            ret = ui_shell_send_event(FALSE, EVENT_PRIORITY_HIGNEST,
                                      EVENT_GROUP_UI_SHELL_XIAOAI,
                                      XIAOAI_EVENT_ANC_ACTION,
                                      param, 0, NULL, 0);
            break;
        }
#endif
    }
    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" app_va_xiaoai_action %d ret=%d", 2, action, ret);
}

static void app_va_xiaoai_state_change(xiaoai_connection_state state)
{
    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" app_va_xiaoai_state_change state=%d", 1, state);
    if (state == XIAOAI_STATE_CONNECTED) {
        // Restart XiaoAI BLE ADV when XiaoAI Connected
        app_va_xiaoai_check_and_send_ble_adv_event(XIAOAI_BLE_ADV_REASON_XIAOAI_CONN);
    } else if (state == XIAOAI_STATE_DISCONNECTED) {
        // Start XiaoAI BLE ADV when XiaoAI Disconnected
        app_va_xiaoai_check_and_send_ble_adv_event(XIAOAI_BLE_ADV_REASON_XIAOAI_CONN);
    }

#ifdef MTK_VA_XIAOAI_MULTI_VA_ENABLE
    if (state == XIAOAI_STATE_CONNECTED) {
        multi_voice_assistant_manager_notify_va_connected(MULTI_VA_XIAOAI);
    } else if (state == XIAOAI_STATE_DISCONNECTED) {
        multi_voice_assistant_manager_notify_va_disconnected(MULTI_VA_XIAOAI);
    }
#endif
}

static void app_va_xiaoai_ota_state_change(bool ota_ongoing)
{
    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" ota_state_change %d", 1, ota_ongoing);
    app_smartcharger_set_ota_state(ota_ongoing);
}

unsigned short g_xiaoai_version = 0x1001;
//unsigned char  g_xiaoai_custom_vendor_data[10] = {1,2,3};
//int            g_xiaoai_custom_vendor_data_length = 3;

static void app_va_xiaoai_init()
{
    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" init xiaoai", 0);
    xiaoai_init_para_t init_para;
    init_para.vid = 0x0094;             // MY_BT_DI_VENDOR_ID in bt_app_common.c
    init_para.pid = 0x0004;             // MY_BT_DI_PRODUCT_ID in bt_app_common.c
    init_para.version = g_xiaoai_version;         // 1.0.0.1 (initial version)
    init_para.device_name = (unsigned char*)bt_customer_config_get_gap_config()->device_name;
    init_para.app_action_fun = app_va_xiaoai_action;
    init_para.state_change_cb = app_va_xiaoai_state_change;
    init_para.ota_state_cb = app_va_xiaoai_ota_state_change;
    init_para.speech_timeout_cb = app_va_xiaoai_speech_timeout;
    init_para.active_update_ble_adv = app_va_xiaoai_active_update_ble_adv;
    init_para.major_id = 0x01;          // Customer Config: Test major_id (MI Air 2s)
    init_para.minor_id = 0x01;          // Customer Config: Test minor_id (MI Air 2s)
    init_para.color = 0;                // Customer Config: 0 - invalid_color, color 1~7
    init_para.vendor_data = NULL;       // Customer Config: Use global/heap buffer
    init_para.vendor_data_length = 0;
    xiaoai_app_init(init_para);
}

static void app_va_xiaoai_deinit()
{
    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" deinit xiaoai", 0);
    xiaoai_app_deinit();
}

bool va_xiaoai_app_activity_proc_ui_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = FALSE;
    switch (event_id) {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE: {
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" CREATE", 0);
#ifdef APP_XIAOAI_TEST
            atci_status_t ret = atci_register_handler(app_xiaoai_atci_cmd, sizeof(app_xiaoai_atci_cmd) / sizeof(atci_cmd_hdlr_item_t));
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG", app_xiaoai init atci ret=%d", 1, ret);
#endif
        }
        break;
        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY: {
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" DESTROY", 0);
        }
        break;
        case EVENT_ID_SHELL_SYSTEM_ON_RESUME:
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" RESUME", 0);
            break;
        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE:
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" PAUSE", 0);
            break;
        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH:
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" REFRESH", 0);
            break;
        case EVENT_ID_SHELL_SYSTEM_ON_RESULT:
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" RESULT", 0);
            break;
    }
    return ret;
}

static bool va_xiaoai_app_interaction_event_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    switch (event_id) {
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
        case APPS_EVENTS_INTERACTION_RHO_END: { // old Agent -> new Partner
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [new Partner] Received RHO end", 0);
            // Stop XiaoAI BLE ADV when RHO switch to Partner
            app_va_xiaoai_send_ble_adv_event(FALSE, 0, 0);
            // APP Wake Up, clear
            g_xiaoai_hfp_state = BT_SINK_SRV_PROFILE_CONNECTION_STATE_DISCONNECTED;
            memset(g_xiaoai_hfp_addr, 0, BT_BD_ADDR_LEN);
            break;
        }
        case APPS_EVENTS_INTERACTION_PARTNER_SWITCH_TO_AGENT: { // old Partner -> new Agent
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [new Agent] Received RHO end", 0);
            // Start XiaoAI BLE ADV when RHO switch to Agent
            app_va_xiaoai_check_and_send_ble_adv_event(XIAOAI_BLE_ADV_REASON_XIAOAI_CONN);
            // APP Wake Up, get hfp_addr
            uint8_t hfp_addr[BT_BD_ADDR_LEN] = {0};
            uint32_t result = bt_cm_get_connected_devices(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP), (bt_bd_addr_t *)hfp_addr, 1);
            if (result == 1) {
                g_xiaoai_hfp_state = BT_SINK_SRV_PROFILE_CONNECTION_STATE_CONNECTED;
                memcpy(g_xiaoai_hfp_addr, hfp_addr, BT_BD_ADDR_LEN);
            } else {
                g_xiaoai_hfp_state = BT_SINK_SRV_PROFILE_CONNECTION_STATE_DISCONNECTED;
                memset(g_xiaoai_hfp_addr, 0, BT_BD_ADDR_LEN);
            }
            break;
        }
#endif
    }
    return ret;
}

bool va_xiaoai_app_activity_proc_key_event(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    uint16_t key_id = *(uint16_t *)extra_data;
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
    xiaoai_connection_state xiaoai_state = xiaoai_get_connection_state();
    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" key_event role=0x%02X, key_id=0x%04X, xiaoai_state=%d",
                     3, role, key_id, xiaoai_state);

    if (key_id != KEY_VA_XIAOAI_START_NOTIFY
            && key_id != KEY_VA_XIAOAI_START
            && key_id != KEY_VA_XIAOAI_STOP_PLAY) {
        return FALSE;
    }

    // No VA when HFP ongoing
    if (g_xiaoai_bt_state >= BT_SINK_SRV_STATE_INCOMING) {
        APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" key_event HFP state=%d", g_xiaoai_bt_state);
        return FALSE;
    }

    // APP Wake Up
    if (role == BT_AWS_MCE_ROLE_AGENT && key_id == KEY_VA_XIAOAI_START) {
        va_xiaoai_app_wake_up();
    }

    // always Play Press VP when KEY_VA_XIAOAI_START_NOTIFY
    if (key_id == KEY_VA_XIAOAI_START_NOTIFY) {
        APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [%02X] key_event XIAOAI_START_NOTIFY VP", 1, role);
        apps_config_set_vp(VP_INDEX_PRESS, FALSE, 0, VOICE_PROMPT_PRIO_MEDIUM, FALSE, NULL);
    }

    if (role == BT_AWS_MCE_ROLE_AGENT) {
        if (xiaoai_state != XIAOAI_STATE_CONNECTED) {
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [Agent] key_event -> no XiaoAI Connected", 0);
            return FALSE;
        }
    } else if (role == BT_AWS_MCE_ROLE_PARTNER) {
        if (xiaoai_state == XIAOAI_STATE_NONE) {
            return FALSE;
        }
    } else {
        return FALSE;
    }

    // send KEY event via AWS_MCE when PARTNER
#ifdef MTK_AWS_MCE_ENABLE
    if (role == BT_AWS_MCE_ROLE_PARTNER) {
        if (BT_STATUS_SUCCESS == apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_KEY, key_id)) {
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [PARTNER] key_event send key_id=0x%04x to agent succeed", 1, key_id);
        } else {
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [PARTNER] key_event send key_id=0x%04x to agent failed", 1, key_id);
        }
    }
#endif

    if (role == BT_AWS_MCE_ROLE_AGENT) {
        if (key_id == KEY_VA_XIAOAI_START) {
            bool rec_ret = xiaoai_start_recognizing();
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [AGENT] key_event start_recognizing ret=%d", 1, rec_ret);
        } else if (key_id == KEY_VA_XIAOAI_STOP_PLAY) {
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [AGENT] KEY_VA_XIAOAI_STOP_PLAY", 0);
        }
    }
    return TRUE;
}

bool app_va_xiaoai_bt_sink_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    return FALSE;
}

bool app_va_xiaoai_bt_cm_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;

    switch (event_id) {
        case BT_CM_EVENT_POWER_STATE_UPDATE:
            bt_cm_power_state_update_ind_t *power_update = (bt_cm_power_state_update_ind_t *)extra_data;
            if (power_update) {
                APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" POWER_STATE_UPDATE %d", 1, power_update->power_state);
                if (power_update->power_state) {
                    if (!g_xiaoai_init_done) {
                        app_va_xiaoai_init();
                        g_xiaoai_init_done = TRUE;
                    }
                } else {
                    if (g_xiaoai_init_done) {
                        app_va_xiaoai_deinit();
                        g_xiaoai_init_done = FALSE;
                    }
                }
            }
            break;
        case BT_CM_EVENT_REMOTE_INFO_UPDATE:
            bt_cm_remote_info_update_ind_t *remote_update = (bt_cm_remote_info_update_ind_t *)extra_data;
            if (remote_update) {
                bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();

                if (role == BT_AWS_MCE_ROLE_AGENT || role == BT_AWS_MCE_ROLE_NONE) {
                    // Check Agent AWS connected
                    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [Agent] Current AWS_MCE_State 0x%02X",
                                    1, state_change->aws_state);
                    if (!(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->pre_connected_service)
                            && BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->connected_service) {
                        g_xiaoai_aws_state = BT_AWS_MCE_AGENT_STATE_ATTACHED;
                        xiaoai_notify_aws_status(TRUE);
                        // Start XiaoAI BLE ADV when AWS Connected
                        app_va_xiaoai_check_and_send_ble_adv_event(FALSE, 62);
                    } else if (BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->pre_connected_service
                            && !(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->connected_service)) {
                        g_xiaoai_aws_state = BT_AWS_MCE_AGENT_STATE_INACTIVE;
                        xiaoai_notify_aws_status(FALSE);
                    }

                    // Check Agent EDR(HFP) connected
                    if ((BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP) & remote_update->pre_connected_service)
                            != BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP) & remote_update->connected_service) {
                        if (BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP) & remote_update->connected_service) {
                            g_xiaoai_hfp_state = BT_SINK_SRV_PROFILE_CONNECTION_STATE_CONNECTED;
                        } else {
                            g_xiaoai_hfp_state = BT_SINK_SRV_PROFILE_CONNECTION_STATE_DISCONNECTED;
                        }
                        memcpy(g_xiaoai_hfp_addr, &remote_update->address, BT_BD_ADDR_LEN);
                        APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [Agent] HFP Connection State 0x%02X", 1,
                                BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP) & remote_update->connected_service);
                    }

                    if ((BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_A2DP_SINK) & remote_update->pre_connected_service)
                            != BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_A2DP_SINK) & remote_update->connected_service) {
                        APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [Agent] A2DP Connection State 0x%02X", 1,
                                BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_A2DP_SINK) & remote_update->connected_service);
                    }

                    // Notify EDR Connection State
                    if (BT_CM_ACL_LINK_CONNECTED == remote_update->acl_state
                            && BT_CM_ACL_LINK_CONNECTED != remote_update->pre_acl_state) {
                        xiaoai_notify_edr_status(TRUE);
                        // Start XiaoAI BLE ADV when EDR Connected
                        app_va_xiaoai_check_and_send_ble_adv_event(FALSE);
                    } else if (BT_CM_ACL_LINK_CONNECTED != remote_update->acl_state
                            && BT_CM_ACL_LINK_CONNECTED == remote_update->pre_acl_state) {
                        xiaoai_notify_edr_status(FALSE);
                    }
                } else {
                    // Check Partner AWS connected
                    if ((BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->pre_connected_service)
                            != BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->connected_service) {
                        APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [Partner] AWS Connection State 0x%02X", 1,
                                BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->connected_service);
                    }
                }
            }
            break;
        default:
            break;
    }
    return ret;
}

#ifdef MTK_AWS_MCE_ENABLE
bool va_xiaoai_app_activity_proc_aws_data(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = FALSE;

    bt_aws_mce_report_info_t *aws_data_ind = (bt_aws_mce_report_info_t *)extra_data;
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
    if (aws_data_ind->module_id == BT_AWS_MCE_REPORT_MODULE_APP_ACTION) {
        uint32_t event_group;
        uint32_t action;
        apps_aws_sync_event_decode(aws_data_ind, &event_group, &action);

        xiaoai_connection_state xiaoai_state = xiaoai_get_connection_state();
        APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG", Received AWS data, action=%08X role=%02X event_group=%d xiaoai_state=%d",
                        4, action, role, event_group, xiaoai_state);
        if (role == BT_AWS_MCE_ROLE_AGENT && event_group == EVENT_GROUP_UI_SHELL_KEY) {
            if (action == KEY_VA_XIAOAI_START_NOTIFY) {
                APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [AGENT] aws key_event -> XIAOAI_START_NOTIFY", 0);
            } else if (action == KEY_VA_XIAOAI_START) {
                // No VA when HFP ongoing
                if (g_xiaoai_bt_state >= BT_SINK_SRV_STATE_INCOMING) {
                    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [AGENT] aws key_event HFP state=%d", g_xiaoai_bt_state);
                    return FALSE;
                }

                // APP Wake Up
                va_xiaoai_app_wake_up();

                bool ret = xiaoai_start_recognizing();
                APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [AGENT] aws key_event start_recognizing ret=%d", 1, ret);
            }
        }
    }

    return ret;
}
#endif


static bool app_va_xiaoai_check_and_send_ble_adv_event(xiaoai_start_ble_adv_reason reason)
{
    #define MIUI_QUICK_CONNECT_BLE_ADV_TIME     (2 * 60 * 1000)     // 2 min
    #define XIAOAI_BLE_ADV_TIME                 (0xFFFFFFFF)        // always for XiaoAI Connection

    bool ret = FALSE;
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
    bt_sink_srv_state_t bt_sink_state = bt_sink_srv_get_state();
    xiaoai_connection_state xiaoai_state = xiaoai_get_connection_state();
    if (role == BT_AWS_MCE_ROLE_AGENT
            && (g_xiaoai_aws_state == BT_AWS_MCE_AGENT_STATE_ATTACHED
                || bt_sink_state >= BT_SINK_SRV_STATE_CONNECTED)) {
        ret = TRUE;
    }
    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" [BLE_ADV] ret=%d role=0x%02X reason=%d bt_sink=0x%04X aws_state=0x%02X xiaoai=%d",
                     6, ret, role, reason, bt_sink_state, g_xiaoai_aws_state, xiaoai_state);
    if (ret) {
        // For adv_interval, only fast_adv when lid_open/lid_close
        bool fast_adv = (reason == XIAOAI_BLE_ADV_REASON_BOTH_LID_OPEN
                      || reason == XIAOAI_BLE_ADV_REASON_BOTH_LID_CLOSE);
        uint16_t adv_interval_ms = (fast_adv ? 30 : 62);

        // For adv_time
        // Customer Config: BLE_ADV time depend on MIUI_QUICK_CONNECT or XIAOAI Connection
        uint32_t adv_time_ms = 0;
        if (xiaoai_state == XIAOAI_STATE_CONNECTED
                || reason != XIAOAI_BLE_ADV_REASON_XIAOAI_CONN) {
            adv_time_ms = MIUI_QUICK_CONNECT_BLE_ADV_TIME;
        } else {
            adv_time_ms = XIAOAI_BLE_ADV_TIME;
        }
        app_va_xiaoai_send_ble_adv_event(TRUE, adv_interval_ms, adv_time_ms);
    }
    return ret;
}

bool app_va_xiaoai_smcharger_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    // Xiaoai activity cannot stop smcharger event
    bool ret = FALSE;
    bt_aws_mce_role_t role = bt_connection_manager_device_local_info_get_aws_role();
    switch (event_id) {
        case EVENT_ID_SMCHARGER_NOTIFY_PUBLIC_EVENT: {
            app_smcharger_public_event_para_t *event_para = (app_smcharger_public_event_para_t *)extra_data;
            APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" SMCharger group [%02X] NOTIFY_PUBLIC_EVENT %d", 2, role, event_para->action);
            if (event_para->action == SMCHARGER_CHARGER_IN_ACTION) {
                xiaoai_set_charger_state(XIAOAI_SMCHARGER_CHARGER_IN);
            } else if (event_para->action == SMCHARGER_OPEN_LID_ACTION) {
                xiaoai_set_charger_state(XIAOAI_SMCHARGER_OPEN_LID);
            } else if (event_para->action == SMCHARGER_CLOSE_LID_ACTION) {
                xiaoai_set_charger_state(XIAOAI_SMCHARGER_CLOSE_LID);
            } else if (event_para->action == SMCHARGER_CHARGER_OUT_ACTION) {
                xiaoai_set_charger_state(XIAOAI_SMCHARGER_OUT_OF_CASE);
            }
            break;
        }
    }
    return ret;
}

bool app_va_xiaoai_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    APPS_LOG_MSGID_I(APP_VA_XIAOAI_TAG" XIAOAI group, event_id=0x%04X extra_data=0x%08X", 2, event_id, extra_data);
    // only xiaoai activity handle xiaoai group event
    bool ret = TRUE;
    switch (event_id) {
        case XIAOAI_EVENT_DISCOVERABLE_ACTION:
            app_va_xiaoai_action_in_app(XIAOAI_ACTION_DISCOVERABLE, NULL);
            break;
        case XIAOAI_EVENT_RECONNECT_EDR_ACTION:
            app_va_xiaoai_action_in_app(XIAOAI_ACTION_RECONNECT_EDR, extra_data);
            break;
        case XIAOAI_EVENT_DISCONNECT_EDR_ACTION:
            app_va_xiaoai_action_in_app(XIAOAI_ACTION_DISCONNECT_EDR, NULL);
            break;
        case XIAOAI_EVENT_START_BLE_ADV_ACTION: {
            // Start: Add XiaoAI from Multi_BLE_Adv_Manager and notify
            uint16_t advertising_interval = (uint32_t)extra_data;
            app_va_xiaoai_start_ble_adv(TRUE, advertising_interval);
            break;
        }
        case XIAOAI_EVENT_STOP_BLE_ADV_ACTION:
            // STOP: remove XiaoAI from Multi_BLE_Adv_Manager and notify
            app_va_xiaoai_start_ble_adv(FALSE, 0);
            break;
        case XIAOAI_EVENT_ANC_ACTION:
            app_va_xiaoai_action_in_app(XIAOAI_ACTION_ANC, extra_data);
            break;
    }
    return ret;
}

bool app_va_xiaoai_activity_proc(struct _ui_shell_activity *self,
                                 uint32_t event_group,
                                 uint32_t event_id,
                                 void *extra_data,
                                 size_t data_len)
{
    bool ret = FALSE;
    switch (event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM:
            ret = va_xiaoai_app_activity_proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_APP_INTERACTION:
            ret = va_xiaoai_app_interaction_event_group(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_KEY:
            ret = va_xiaoai_app_activity_proc_key_event(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_BT_SINK:
            app_va_xiaoai_bt_sink_event_proc(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_BT_CONN_MANAGER:
            ret = app_va_xiaoai_bt_cm_event_proc(self, event_id, extra_data, data_len);
            break;
#ifdef MTK_AWS_MCE_ENABLE
        case EVENT_GROUP_UI_SHELL_AWS_DATA:
            ret = va_xiaoai_app_activity_proc_aws_data(self, event_id, extra_data, data_len);
            break;
#endif
        case EVENT_GROUP_UI_SHELL_CHARGER_CASE:
            ret = app_va_xiaoai_smcharger_event_proc(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_XIAOAI:
            ret = app_va_xiaoai_event_proc(self, event_id, extra_data, data_len);
            break;
    }
    return ret;
}

/*
 * SmartCharger UT Code by using AT CMD
 */
#ifdef APP_XIAOAI_TEST

static atci_status_t app_xiaoai_atci_handler(atci_parse_cmd_param_t *parse_cmd)
{
    atci_response_t response = {{0}, 0};
    switch (parse_cmd->mode)
    {
        case ATCI_CMD_MODE_EXECUTION: {
            char *atcmd = parse_cmd->string_ptr + parse_cmd->name_len + 1;
            char cmd[20] = {0};
            uint8_t data = 0;
            memcpy(cmd, atcmd, strlen(atcmd) - 2);
            APPS_LOG_I(APP_VA_XIAOAI_TAG", XiaoAI AT CMD=%s", cmd);
            int event_id = EVENT_ID_SMCHARGER_NONE;
            if (strcmp(cmd, "XAVER") == 0) {
                extern unsigned short g_xiaoai_version;
                memset(response.response_buf, 0, ATCI_UART_TX_FIFO_BUFFER_SIZE);
                snprintf((char *)response.response_buf, ATCI_UART_TX_FIFO_BUFFER_SIZE,
                        "XiaoAI Version 0x%04X\r\n", g_xiaoai_version);
                break;
            } else if (strcmp(cmd, "mi,1") == 0) {
                extern bool g_xiaoai_04_test_flag;
                g_xiaoai_04_test_flag = TRUE;
                APPS_LOG_I(APP_VA_XIAOAI_TAG", XiaoAI enable MI_TEST_FLAG");
            } else if (strcmp(cmd, "mi,0") == 0) {
                extern bool g_xiaoai_04_test_flag;
                g_xiaoai_04_test_flag = FALSE;
                APPS_LOG_I(APP_VA_XIAOAI_TAG", XiaoAI disable MI_TEST_FLAG");
            } else if (strcmp(cmd, "ota,test") == 0) {
                extern bool g_xiaoai_ota_ver_compare_flag;
                g_xiaoai_ota_ver_compare_flag = FALSE;
                APPS_LOG_I(APP_VA_XIAOAI_TAG", XiaoAI disable OTA_VERSION_COMPARE_FLAG");
            } else {
                APPS_LOG_I(APP_VA_XIAOAI_TAG", invalid SMCharger AT-CMD");
            }
            memset(response.response_buf, 0, ATCI_UART_TX_FIFO_BUFFER_SIZE);
            snprintf((char *)response.response_buf, ATCI_UART_TX_FIFO_BUFFER_SIZE, "OK - %s\r\n", atcmd);
            response.response_flag = ATCI_RESPONSE_FLAG_APPEND_OK;
            break;
        }
        default:
            response.response_flag = ATCI_RESPONSE_FLAG_APPEND_ERROR;
            break;
    }
    response.response_len = strlen((char *)response.response_buf);
    atci_send_response(&response);
    return ATCI_STATUS_OK;
}
#endif

#endif /* MTK_XIAOAI_ENABLED */
