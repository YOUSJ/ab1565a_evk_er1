/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "record_control.h"
#include "bt_sink_srv_ami.h"
#include "app_ama_audio.h"
#include "FreeRTOS.h"
#include "memory_attribute.h"
// Modify for 2822
//#include "hal_dvfs_internal.h"
#include "hal_platform.h"
#include "audio_codec_manager.h"
#include "audio_codec_opus_encoder.h"
#include "BtAma.h"
#include "app_ama_activity.h"

#ifdef MTK_AMA_ENABLE


/**
 * @brief When received the PCM data from DSP
 *
 */
#define AMA_TASK_EVENT_AUDIO_STREAM_IN                  1

/**
 * @brief Start to run audio recorder and opus encoding
 *
 */
#define AMA_TASK_EVENT_AUDIO_START                      2
/**
 * @brief When start audio recorder done
 *
 */
#define AMA_TASK_EVENT_AUDIO_START_DONE                 3

/**
 * @brief Stop the audio recorder and opus encoding
 *
 */
#define AMA_TASK_EVENT_AUDIO_STOP                       4

/**
 * @brief When audio recorder execute action done
 *
 */
#define AMA_TASK_EVENT_AUDIO_ACTION_DONE                5
/**
 * @brief Send the audio data to SP
 *
 */
#define AMA_TASK_EVENT_AUDIO_SEND_DATA                  6
/**
 * @brief Audio recorder stopped by other application (eg : HFP)
 *
 */
#define AMA_TASK_EVENT_AUDIO_STOP_BY_IND                10

/**
 * @brief The opus codec buffer size for the library
 *
 */
#define AMA_CODEC_BUF_LEN                               15184

/**
 * @brief The audio queue depth
 *
 */
#define AMA_TASK_QUEUE_SIZE                             40
#define AMA_TASK_QUEUE_ITEM_SIZE                        sizeof(ama_msg_item_t)


#define AMA_AM_STATE_IDLE                               0
#define AMA_AM_STATE_STARTING                           1
#define AMA_AM_STATE_STOPING                            2
#define AMA_AM_STATE_STARTED                            3
#define AMA_AM_INVALID_ID                               0

/**
 * @brief Restart the audio recorder and opus encoding
 *
 */
#define AMA_TASK_EVENT_AUDIO_RESTART                    7

/**
 * @brief When the Wake Word Detected.
 *
 */
#define AMA_TASK_EVENT_AUDIO_WWD_NOTIFICATION           8

/**
 * @brief When audio recorder need to clear all data
 *
 */
#define AMA_TASK_EVENT_AUDIO_DATA_ABORT_NOTIFICATION    9

/**
 * @brief The opus encode manager ring buffer cout, the ring buffer size should be 50 * opus output length for one frame.
 *
 */
#define AMA_OPUS_CODEC_RING_BUFFER_COUNT              50

/**
 * @brief The audio queue message item.
 *
 */
typedef struct {
    uint8_t event_id;
    uint8_t *data;
} ama_msg_item_t;

/**
 * @brief The audio recorder / opus information struct.
 *
 */
typedef struct {
    bool                            info_running;
    bool                            info_sending_data;

    uint8_t                         info_compress_ratio;
    uint8_t                         info_state;
    record_id_t                     info_recorder_id;

    uint32_t                        info_pcm_length_in_one_frame;
    uint32_t                        info_encode_output_len;
    uint32_t                        info_codec_ring_buffer_len;
    audio_codec_manager_handle_t    info_codec_manager_handle;

    on_recorder_stop_record         info_recorder_stopped_callback;

    uint8_t                         *info_codec_ring_buffer;
    uint8_t                         *info_pcm_read_buffer;

    uint8_t                         info_codec_buffer[AMA_CODEC_BUF_LEN];

    bool                            info_wwd_is_restart;
    wwe_mode_t                      info_wwd_restart_mode;
    uint16_t                        info_wwd_preroll_recommend_len;
    uint16_t                        info_wwd_preroll_padding_data_len;

    uint32_t                        info_wwd_flash_address;
    uint32_t                        info_wwd_length;

    on_wwd_trigger                  info_wwd_start_speech_callback;
    uint8_t                         *info_wwd_preroll_padding_data;

} ama_audio_context_t;

ATTR_ZIDATA_IN_TCM ama_audio_context_t ama_audio_context;

static void         ama_audio_control_ccni_callback(hal_audio_event_t event, void *data);
static void         ama_audio_codec_manager_callback(audio_codec_manager_handle_t handle, audio_codec_manager_event_t event, void *user_data);
static void         ama_audio_am_callback(bt_sink_srv_am_id_t aud_id,
                                          bt_sink_srv_am_cb_msg_class_t msg_id,
                                          bt_sink_srv_am_cb_sub_msg_t sub_msg,
                                          void *parm);
static int8_t       ama_audio_queue_send(QueueHandle_t q_id, void *data);
static uint16_t     ama_audio_get_item_num(QueueHandle_t q_id);
static bool         ama_audio_recorder_stop(void);

static bool         ama_audio_recorder_start(wwe_mode_t mode);

static QueueHandle_t ama_audio_task_queue = NULL;

static int8_t ama_audio_queue_send(QueueHandle_t q_id, void *data)
{
    BaseType_t ret = -1;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    if ( 0 == q_id ) {
        return 0;
    }

    if( 0 == HAL_NVIC_QUERY_EXCEPTION_NUMBER) {
        ret = xQueueSend(q_id, data, 0);
    } else {
        ret = xQueueSendFromISR(q_id, data, &xHigherPriorityTaskWoken);

        if (xHigherPriorityTaskWoken) {
            portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
        }
    }

    if (pdFAIL != ret) {
        return 0;
    } else {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO",queue_send FAILED: false %d, ret:%d", 2, pdFAIL, ret);
        return -1;
    }
}

int8_t ama_audio_queue_send_front(QueueHandle_t q_id, void *data)
{
    BaseType_t ret = -1;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    if (q_id == 0) {
        return -1;
    }

    if( 0 == HAL_NVIC_QUERY_EXCEPTION_NUMBER) {
        ret = xQueueSendToFront(q_id, data, 0);
    } else {
        ret = xQueueSendToFrontFromISR(q_id, data, &xHigherPriorityTaskWoken);

        if (xHigherPriorityTaskWoken) {
            portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
        }
    }

    if (pdFAIL != ret) {
        return 0;
    } else {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO",queue_send_to_front FAILED: false %d, ret:%d", 2, pdFAIL, ret);
        return -1;
    }
}

static uint16_t ama_audio_get_item_num(QueueHandle_t q_id)
{
    uint16_t queue_item_num = 0;

    if ( 0 == q_id ) {
        return 0;
    }
    if( 0 == HAL_NVIC_QUERY_EXCEPTION_NUMBER) {
        queue_item_num = (uint16_t)uxQueueMessagesWaiting(q_id);
    } else {
        queue_item_num = (uint16_t)uxQueueMessagesWaitingFromISR(q_id);
    }

    return queue_item_num;
}

static void ama_send_audio_data(void)
{
    uint8_t *voice_data = NULL;
    uint8_t frameSize = 160;
    AMAStatus status;

    if (AMA_AM_STATE_STARTED != ama_audio_context.info_state || ama_audio_context.info_sending_data == false) {
        /**
         * @brief If current is stopped, return
         *
         */
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data; send failed, state error , state : %d, sending data : %d",
                                2, ama_audio_context.info_state, ama_audio_context.info_sending_data);
        return;
    }

    if (ama_audio_context.info_wwd_preroll_padding_data_len > 0 && ama_audio_context.info_wwd_preroll_padding_data != NULL) {
        /**
         * @brief If the preroll padding data is not NULL, need to send the padding data of preroll firstly.
         *
         */
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data; Start to send preroll padding data : %d", 1, ama_audio_context.info_wwd_preroll_padding_data_len);
        uint16_t length = 0;

        while (ama_audio_context.info_wwd_preroll_padding_data_len > 0) {
            voice_data = AMA_Target_VoiceStreamSinkClaim(frameSize);
            if (voice_data != NULL) {
                if (ama_audio_context.info_wwd_preroll_padding_data_len > frameSize) {
                    memcpy(voice_data, ama_audio_context.info_wwd_preroll_padding_data + length, frameSize);
                    ama_audio_context.info_wwd_preroll_padding_data_len -= frameSize;
                    length += frameSize;
                } else {
                    memcpy(voice_data, ama_audio_context.info_wwd_preroll_padding_data + length, ama_audio_context.info_wwd_preroll_padding_data_len);
                    length += ama_audio_context.info_wwd_preroll_padding_data_len;
                    ama_audio_context.info_wwd_preroll_padding_data_len -= ama_audio_context.info_wwd_preroll_padding_data_len;
                }
                status = AMA_Target_VoiceStreamSinkFlush();

                if (status != AMA_STATUS_OK) {
                    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data; Send padding data, Packet send failed", 1, status);
                }
            } else {
                APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data; Send padding data, Claim voice data buffer failed", 0);
                ama_audio_recorder_stop();
                break;
            }
        }

        free(ama_audio_context.info_wwd_preroll_padding_data);
        ama_audio_context.info_wwd_preroll_padding_data = NULL;
    } else {
        /**
         * @brief Read the opus encoder length which stored the encoded data.
         *
         */
        //uint16_t current_opus_length = audio_opus_encoder_get_payload_length();
        uint32_t current_opus_length = audio_codec_buffer_mode_get_output_data_length(ama_audio_context.info_codec_manager_handle);

        uint32_t count = current_opus_length / frameSize;
        uint32_t index = 0;

        if (count == 0) {
            return;
        }

        for (index = 0; index < count; index ++) {
            voice_data = AMA_Target_VoiceStreamSinkClaim(frameSize);
            if (voice_data == NULL) {
                APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data; Send voice data, Claim voice data buffer failed", 0);
                ama_audio_recorder_stop();
                return;
            }
            uint8_t *ring_buf_address = NULL;
            uint32_t ring_buf_length = 0;
            uint16_t read_length = 0;

            while (ring_buf_length < frameSize) {
                audio_codec_manager_status_t st = audio_codec_buffer_mode_get_read_information(ama_audio_context.info_codec_manager_handle,
                                                                                               &ring_buf_address,
                                                                                               &ring_buf_length);
                APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data, get read information length : %d", 1, ring_buf_length);

                if (st != AUDIO_CODEC_MANAGER_SUCCESS) {
                    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data, read failed from codec, %d", 1, st);
                    return;
                }

                if (read_length == 0 && ring_buf_length >= frameSize) {
                    /**
                     * @brief If the read data length bigger than the expected data length
                     * Only copy the data of the frame size
                     */
                    memcpy(voice_data, ring_buf_address, frameSize);
                    audio_codec_buffer_mode_read_done(ama_audio_context.info_codec_manager_handle, frameSize);
                    read_length += frameSize;
                    break;
                } else {
                    /**
                     * @brief If the read length smaller than the expected data length, and read more than 1 times.
                     *
                     */
                    if (ring_buf_length >= (frameSize - read_length)) {
                        memcpy(voice_data + read_length, ring_buf_address, frameSize - read_length);
                        audio_codec_buffer_mode_read_done(ama_audio_context.info_codec_manager_handle, frameSize - read_length);
                        read_length += (frameSize - read_length);
                        break;
                    } else {
                        memcpy(voice_data + read_length, ring_buf_address, ring_buf_length);
                        audio_codec_buffer_mode_read_done(ama_audio_context.info_codec_manager_handle, ring_buf_length);
                    }

                    read_length += ring_buf_length;
                }
            }

            if (read_length == frameSize) {
                status = AMA_Target_VoiceStreamSinkFlush();
                if (status != AMA_STATUS_OK) {
                    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data, Send audio data, Packet send failed", 1, status);
                }
            } else {
                APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data, Send voice data, opus encode read failed (length not equal), %d, %d", 2, read_length, frameSize);
                AMA_Target_VoiceStreamSinkFree();
            }
        }

#if 0
        if (current_opus_length >= frameSize) {
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data; send opus length : %d", 1, current_opus_length);
            voice_data = AMA_Target_VoiceStreamSinkClaim(frameSize);
            if (voice_data == NULL) {
                APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data; Send voice data, Claim voice data buffer failed", 0);
                ama_audio_recorder_stop();
                return;
            }
            uint8_t *ring_buf_address = NULL;
            uint32_t ring_buf_length = 0;
            uint16_t read_length = 0;

            while (ring_buf_length < frameSize) {
                audio_codec_manager_status_t st = audio_codec_buffer_mode_get_read_information(ama_audio_context.info_codec_manager_handle,
                                                                                               &ring_buf_address,
                                                                                               &ring_buf_length);
                APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data, get read information length : %d", 1, ring_buf_length);

                if (st != AUDIO_CODEC_MANAGER_SUCCESS) {
                    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data, read failed from codec, %d", 1, st);
                    return;
                }

                if (read_length == 0 && ring_buf_length >= frameSize) {
                    /**
                     * @brief If the read data length bigger than the expected data length
                     * Only copy the data of the frame size
                     */
                    memcpy(voice_data, ring_buf_address, frameSize);
                    audio_codec_buffer_mode_read_done(ama_audio_context.info_codec_manager_handle, frameSize);
                    read_length += frameSize;
                    break;
                } else {
                    /**
                     * @brief If the read length smaller than the expected data length, and read more than 1 times.
                     *
                     */
                    if (ring_buf_length >= (frameSize - read_length)) {
                        memcpy(voice_data + read_length, ring_buf_address, frameSize - read_length);
                        audio_codec_buffer_mode_read_done(ama_audio_context.info_codec_manager_handle, frameSize - read_length);
                        read_length += (frameSize - read_length);
                        break;
                    } else {
                        memcpy(voice_data + read_length, ring_buf_address, ring_buf_length);
                        audio_codec_buffer_mode_read_done(ama_audio_context.info_codec_manager_handle, ring_buf_length);
                    }

                    read_length += ring_buf_length;
                }
            }

            APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data, read length : %d, frame size : %d", 2, read_length, frameSize);

            if (read_length == frameSize) {
                status = AMA_Target_VoiceStreamSinkFlush();
                if (status != AMA_STATUS_OK) {
                    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data, Send audio data, Packet send failed", 1, status);
                }
            } else {
                APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_send_audio_data, Send voice data, opus encode read failed (length not equal), %d, %d", 2, read_length, frameSize);
                AMA_Target_VoiceStreamSinkFree();
            }
        } else {
            // APPS_LOG_MSGID_I(APP_AMA_AUDIO",ama_send_audio_data; Send voice data, current voice data is not enough to send : %d", 1, current_opus_length);
        }
#endif
    }
}

static void ama_handle_wwd_notification(uint32_t information_data)
{
    if (ama_audio_context.info_sending_data == true) {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] Ama WWD already enabled, break", 0);
        return;
    }
    if (AMA_AM_STATE_STARTED != ama_audio_context.info_state) {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] Current state is %d", 1, ama_audio_context.info_state);
        return;
    }

    ama_audio_context.info_sending_data = true;

    APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] AMA_TASK_EVENT_AUDIO_WWD_NOTIFICATION", 0);
    /**
     * @brief
     * The received information from DSP which including the wake word length and the total length of the DSP buffer
     * bit16 ~ bit31 is the total length
     * bit0 ~ bit15 is the wake word length
     */
    /**
     * @brief
     * The total length
     */
    uint16_t total_length = (information_data & 0xFFFF0000) >> 16;
    /**
     * @brief
     * The wake word length
     */
    uint16_t wake_word_length = (information_data & 0x0000FFFF);
    APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] Information length : 0x%x (%d), wake_word_length : 0x%x(%d), total_length : 0x%x(%d)",
                        6, information_data, information_data, wake_word_length, wake_word_length, total_length, total_length);

    /**
     * @brief
     * The current length of the opus ring buffer
     * Which maybe contains the wake word and the preroll
     */
    uint32_t current_opus_payload_length = audio_codec_buffer_mode_get_output_data_length(ama_audio_context.info_codec_manager_handle);//audio_opus_encoder_get_payload_length();

    APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] Current opus ring buffer length : %d", 1, current_opus_payload_length);

    /**
     * @brief
     * The opus preroll length which received
     */
    uint16_t opus_preroll_length = 0;

    if (wake_word_length >= total_length) {
        /**
         * @brief If the wake word length is bigger than the total length of the DSP buffer
         * Means that the wake word has been already sent to CM4 (Partly). So the CM4 ring buffer contains the
         * preroll audio data (opus encoded) and the wake word (opus encoded)
         *
         * So the preroll audio data is : current opus payload length, minus the received wake word opus length
         */
        uint16_t opus_received_ww_length = (wake_word_length - total_length) / ama_audio_context.info_compress_ratio;//OPUS_COMPRESS_RATIO_32KBPS;
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] The received opus wake word length (which stored in the CM4 codec ring buffer): %d", 1, opus_received_ww_length);
        if (current_opus_payload_length < opus_received_ww_length) {
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] <ERROR> The current opus length < opus received wake word length", 0);
            assert(false);
            return;
        }
        opus_preroll_length = current_opus_payload_length - opus_received_ww_length;
    } else {
        /**
         * @brief If the wake word length is smaller than the total length of the DSP buffer
         * Means that the wake word are all stored in the DSP buffer, and the DSP buffer also contains some preroll data (PCM)
         *
         * So the total preroll audio data is : the preroll data in the DSP buffer (PCM), add the stored bytes in the CM4 ring buffer (opus encoded)
         */
        uint16_t opus_receiving_preroll_length = (total_length - wake_word_length) / ama_audio_context.info_compress_ratio;
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] The receiving opus preroll length : %d", 1, opus_receiving_preroll_length);
        opus_preroll_length = opus_receiving_preroll_length + current_opus_payload_length;
    }
    APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] The opus length : %d, required preroll length : %d", 2, opus_preroll_length, ama_audio_context.info_wwd_preroll_recommend_len);

    uint32_t stop_index = AMA_AUDIO_WWD_START_INDEX + (wake_word_length / 2);
    APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] Wake word stop index : %d", 1, stop_index);

    if (ama_audio_context.info_wwd_start_speech_callback != NULL) {
        bool ret = ama_audio_context.info_wwd_start_speech_callback(stop_index);
        if (ret != true) {
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] Start speech failed, restart the WWE", 0);
            return;
        } else {
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] Start speech succeed", 0);
        }
    } else {
        /**
         * @brief If the application layer callback function is NULL,
         * Cannot start speech.
         */
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] Failed to start speech, cause the callback function is NULL", 0);
        ama_audio_stop();
        return;
    }

    /**
     * @brief
     * If the opus preroll length is smaller than the required length, means need to padding the silence data in the forground.
     * If the opus preroll length is bigger than the required length, means need to remove the header of the preroll data that stored in the CM4 ring buffer.
     */
    if (opus_preroll_length < ama_audio_context.info_wwd_preroll_recommend_len) {

        ama_audio_context.info_wwd_preroll_padding_data_len = ama_audio_context.info_wwd_preroll_recommend_len - opus_preroll_length;
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] Preroll padding data length : %d", 1, ama_audio_context.info_wwd_preroll_padding_data_len);
        ama_audio_context.info_wwd_preroll_padding_data = (uint8_t *)malloc(sizeof(uint8_t) * ama_audio_context.info_wwd_preroll_padding_data_len);
        if (ama_audio_context.info_wwd_preroll_padding_data == NULL) {
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] Failed to allocate buffer to fill the padding data for preroll", 0);
            assert(false);
        }
        // TODO need call the opus API to generate the silence data for preroll
        memset(ama_audio_context.info_wwd_preroll_padding_data, 0, sizeof(uint8_t) * ama_audio_context.info_wwd_preroll_padding_data_len);

    } else if (opus_preroll_length > ama_audio_context.info_wwd_preroll_recommend_len) {
        uint16_t length = opus_preroll_length - ama_audio_context.info_wwd_preroll_recommend_len;
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] Dummy read total length : %d", 1, length);

        audio_codec_buffer_mode_read_done(ama_audio_context.info_codec_manager_handle, length);
    } else {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][WWD_NOTIFICATION] Opus preroll length == required length", 0);
    }

    ama_msg_item_t event;
    event.event_id = AMA_TASK_EVENT_AUDIO_SEND_DATA;
    event.data = NULL;
    ama_audio_queue_send((QueueHandle_t)ama_audio_task_queue, &event);
    // APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][TASK] AMA_TASK_EVENT_AUDIO_WWD_NOTIFICATION, send AMA_TASK_EVENT_AUDIO_SEND_DATA", 0);
}

/**
 * @brief Restart the audio recorder for WWD
 *
 * @param mode
 * @return true
 * @return false
 */
static bool ama_audio_recorder_wwd_restart(wwe_mode_t mode)
{
    switch (ama_audio_context.info_state) {
        case AMA_AM_STATE_IDLE:
        {
            return ama_audio_recorder_start(mode);
        }

        case AMA_AM_STATE_STARTING:
        {
            return false;
        }

        case AMA_AM_STATE_STOPING:
        {
            return false;
        }

        case AMA_AM_STATE_STARTED:
        {
            ama_audio_context.info_wwd_restart_mode = mode;
            ama_audio_context.info_wwd_is_restart = true;
            return ama_audio_recorder_stop();
        }

        default:
            return false;
    }
}

void ama_audio_restart(wwe_mode_t wwe_mode, uint32_t wwe_flash_address, uint32_t wwe_length)
{
    if (wwe_mode == WWE_MODE_AMA && (wwe_length == 0 || wwe_flash_address == 0)) {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO ", ama_audio_restart, mode is AMA, but length/address is 0", 0);
        return;
    }
    ama_msg_item_t ama_msg_item_event;
    ama_msg_item_event.event_id = AMA_TASK_EVENT_AUDIO_RESTART;
    ama_msg_item_event.data = (uint8_t *)wwe_mode;

    if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true && wwe_mode == WWE_MODE_AMA) {
        ama_audio_context.info_wwd_flash_address = wwe_flash_address;
        ama_audio_context.info_wwd_length = wwe_length;
    }

    if (ama_audio_queue_send((QueueHandle_t)ama_audio_task_queue, &ama_msg_item_event) == 0) {
        //ama_wwe_running = true;
    }
}

/**
 * @brief Init the opus codec
 *
 * @return true
 * @return false
 */
static bool ama_audio_init_opus(void)
{
    /**
     * @brief For new audio codec manager API implementation
     *
     */
    ama_audio_context.info_codec_ring_buffer_len = 0;
    ama_audio_context.info_compress_ratio = 0;
    ama_audio_context.info_encode_output_len = 0;
    ama_audio_context.info_pcm_length_in_one_frame = 0;

    if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true) {
        ama_audio_context.info_wwd_preroll_padding_data_len = 0;
        ama_audio_context.info_wwd_preroll_recommend_len = 0;
    }

    audio_codec_manager_status_t st;

    audio_codec_manager_config_t opus_config;
    opus_config.codec_mode = CODEC_MODE_BUFFER;

    opus_config.codec_config.opus_encoder_config.param.channel = OPUS_CODEC_CHANNEL_MONO;
    /* Sample rate */
    opus_config.codec_config.opus_encoder_config.param.samplerate = OPUS_CODEC_SAMPLERATE_16KHZ;
    /* Encode bitrate */
    opus_config.codec_config.opus_encoder_config.param.bitrate = OPUS_CODEC_BITRATE_32KBPS;

    memset(ama_audio_context.info_codec_buffer, 0, AMA_CODEC_BUF_LEN);

    opus_config.codec_config.opus_encoder_config.opus_encoder_working_buffer_ptr = (uint32_t *)ama_audio_context.info_codec_buffer;

    st = audio_codec_open(CODEC_TYPE_OPUS_ENCODE, &opus_config, &ama_audio_context.info_codec_manager_handle);
    if (st != AUDIO_CODEC_MANAGER_SUCCESS) {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_init_opus, open opus codec failed, %d", 1, st);
        goto FAILED;
    }
    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_init_opus, opus handle : %d", 1, ama_audio_context.info_codec_manager_handle);

    st = audio_codec_get_inout_frame_length(ama_audio_context.info_codec_manager_handle,
                                            &ama_audio_context.info_pcm_length_in_one_frame,
                                            &ama_audio_context.info_encode_output_len);

    ama_audio_context.info_compress_ratio = ama_audio_context.info_pcm_length_in_one_frame / ama_audio_context.info_encode_output_len;

    if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true) {
        ama_audio_context.info_wwd_preroll_recommend_len = ama_audio_context.info_encode_output_len * 25;
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD] ama_audio_init_opus, Preroll recommend length : %d", 1, ama_audio_context.info_wwd_preroll_recommend_len);
    }

    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_init_opus, get one frame in length : %d, out length : %d, compress ratio : %d", 3,
                                ama_audio_context.info_pcm_length_in_one_frame,
                                ama_audio_context.info_encode_output_len,
                                ama_audio_context.info_compress_ratio);

    if (ama_audio_context.info_codec_ring_buffer != NULL) {
        free(ama_audio_context.info_codec_ring_buffer);
        ama_audio_context.info_codec_ring_buffer = NULL;
    }

    ama_audio_context.info_codec_ring_buffer_len = (ama_audio_context.info_encode_output_len * AMA_OPUS_CODEC_RING_BUFFER_COUNT);
    ama_audio_context.info_codec_ring_buffer = (uint8_t*)malloc(sizeof(uint8_t) * ama_audio_context.info_codec_ring_buffer_len);

    if (ama_audio_context.info_codec_ring_buffer == NULL) {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_init_opus, Failed to allocate codec ring buffer", 0);
        goto FAILED;
    }
    memset(ama_audio_context.info_codec_ring_buffer, 0, sizeof(uint8_t) * ama_audio_context.info_codec_ring_buffer_len);

    audio_codec_manager_user_config_t user_config;
    user_config.output_buffer = ama_audio_context.info_codec_ring_buffer;
    user_config.output_length = ama_audio_context.info_codec_ring_buffer_len;
    user_config.callback = ama_audio_codec_manager_callback;
    user_config.user_data = NULL;
    st = audio_codec_buffer_mode_set_config(ama_audio_context.info_codec_manager_handle, &user_config);
    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_init_opus, set config result : %d", 1, st);

    if (st != AUDIO_CODEC_MANAGER_SUCCESS) {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_init_opus, Set config failed, close codec", 0);
        goto FAILED;
    }

    if (ama_audio_context.info_pcm_read_buffer != NULL) {
        free(ama_audio_context.info_pcm_read_buffer);
        ama_audio_context.info_pcm_read_buffer = NULL;
    }

    ama_audio_context.info_pcm_read_buffer = (uint8_t *)malloc(sizeof(uint8_t) * ama_audio_context.info_pcm_length_in_one_frame);
    if (ama_audio_context.info_pcm_read_buffer == NULL) {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_init_opus, Failed to allocate PCM read buffer", 0);
        goto FAILED;
    }
    memset(ama_audio_context.info_pcm_read_buffer, 0, sizeof(uint8_t) * ama_audio_context.info_pcm_length_in_one_frame);

    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_init_opus, Succeed to init opus codec", 0);

    goto SUCCEED;

FAILED:
    if (ama_audio_context.info_codec_manager_handle != 0) {
        audio_codec_close(ama_audio_context.info_codec_manager_handle);
    }
    if (ama_audio_context.info_codec_ring_buffer != NULL) {
        free(ama_audio_context.info_codec_ring_buffer);
        ama_audio_context.info_codec_ring_buffer = NULL;
    }
    if (ama_audio_context.info_pcm_read_buffer != NULL) {
        free(ama_audio_context.info_pcm_read_buffer);
        ama_audio_context.info_pcm_read_buffer = NULL;
    }

    memset(ama_audio_context.info_codec_buffer, 0, AMA_CODEC_BUF_LEN);

    if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true) {
        ama_audio_context.info_wwd_preroll_recommend_len = 0;
        ama_audio_context.info_wwd_preroll_padding_data_len = 0;
    }

    ama_audio_context.info_compress_ratio = 0;
    ama_audio_context.info_pcm_length_in_one_frame = 0;
    ama_audio_context.info_codec_ring_buffer_len = 0;

    assert(false);

    return false;

SUCCEED:
    return true;
}

static void ama_audio_deinit(void)
{
    record_control_result_t result_record = audio_record_control_deinit(ama_audio_context.info_recorder_id);
    audio_codec_manager_status_t st = AUDIO_CODEC_MANAGER_SUCCESS;

    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_deinit, codec manager handle : %d", 1, ama_audio_context.info_codec_manager_handle);
    if (ama_audio_context.info_codec_manager_handle != 0) {
        st = audio_codec_close(ama_audio_context.info_codec_manager_handle);
    }

    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_deinit, record_deinit : %d, codec close : %d", 2, result_record, st);

    ama_audio_context.info_state = AMA_AM_STATE_IDLE;
    ama_audio_context.info_recorder_id = AMA_AM_INVALID_ID;
    ama_audio_context.info_sending_data = false;

    if (ama_audio_context.info_codec_ring_buffer != NULL) {
        free(ama_audio_context.info_codec_ring_buffer);
        ama_audio_context.info_codec_ring_buffer = NULL;
    }
    if (ama_audio_context.info_pcm_read_buffer != NULL) {
        free(ama_audio_context.info_pcm_read_buffer);
        ama_audio_context.info_pcm_read_buffer = NULL;
    }

    if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true) {
        if (ama_audio_context.info_wwd_preroll_padding_data != NULL) {
            free(ama_audio_context.info_wwd_preroll_padding_data);
            ama_audio_context.info_wwd_preroll_padding_data = NULL;
        }

        ama_audio_context.info_wwd_preroll_recommend_len = 0;
        ama_audio_context.info_wwd_preroll_padding_data_len = 0;
    }

    memset(ama_audio_context.info_codec_buffer, 0, AMA_CODEC_BUF_LEN);

    ama_audio_context.info_compress_ratio = 0;
    ama_audio_context.info_pcm_length_in_one_frame = 0;
    ama_audio_context.info_codec_ring_buffer_len = 0;

    /**
     * @brief If deinit happen, clear all queue message.
     * Fix issue : BTA-6606
     */
    xQueueReset((QueueHandle_t)ama_audio_task_queue);
}

static bool ama_audio_recorder_start(wwe_mode_t mode)
{
    record_encoder_cability_t encoder;

    if (AMA_AM_STATE_STOPING == ama_audio_context.info_state) {
        return false;
    }
    if (AMA_AM_STATE_IDLE != ama_audio_context.info_state) {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_recorder_start, Audio recorder already started. state: %d", 1, ama_audio_context.info_state);
        return false;
    }

    /* 1. audio record init */
    encoder.wwe_mode = mode;
    encoder.bit_rate = ENCODER_BITRATE_16KBPS;//ENCODER_BITRATE_32KBPS;
    if (encoder.wwe_mode == WWE_MODE_AMA) {
        encoder.codec_type = AUDIO_DSP_CODEC_TYPE_PCM_WWE;
        encoder.wwe_language_mode_address = ama_audio_context.info_wwd_flash_address;
        encoder.wwe_language_mode_length = ama_audio_context.info_wwd_length;
    } else if (mode == WWE_MODE_NONE) {
        encoder.codec_type = AUDIO_DSP_CODEC_TYPE_PCM;
    } else {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_recorder_start, Unsupported wwe mode : %d", 1, mode);
        return false;
    }

    ama_audio_context.info_recorder_id = audio_record_control_enabling_encoder_init(ama_audio_control_ccni_callback, NULL, ama_audio_am_callback, &encoder);
    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_recorder_start, Init audio recorder mode : %d, inited ID : %x", 2, encoder.wwe_mode, ama_audio_context.info_recorder_id);

    if (AMA_AM_INVALID_ID == ama_audio_context.info_recorder_id) {
        return false;
    }

    /* 2. audio opus init */
    if (ama_audio_init_opus() == false) {
        record_control_result_t result;

        result = audio_record_control_deinit(ama_audio_context.info_recorder_id);
        APPS_LOG_MSGID_E(APP_AMA_AUDIO", ama_audio_recorder_start, Init opus encoder failed, deinit result: %d", 1, result);

        ama_audio_context.info_recorder_id = AMA_AM_INVALID_ID;
        ama_audio_context.info_state = AMA_AM_STATE_IDLE;
        return false;
    }

    ama_audio_context.info_state  = AMA_AM_STATE_STARTING;

    /* 3. audio record start and wait for msg_id = AUD_SINK_OPEN_CODEC */
    record_control_result_t start_result = audio_record_control_start(ama_audio_context.info_recorder_id);
    if (start_result != RECORD_CONTROL_EXECUTION_SUCCESS) {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_recorder_start, Audio recorder start failed: %d", 1, start_result);
        ama_audio_deinit();
        return false;
    }

    if (mode == WWE_MODE_NONE) {
        /**
         * @brief If current is not VAD mode, means the provide speech or with not support WWD feature (Tap-To-Talk or Push-To-Talk)
         * Send the audio data directly
         */
        ama_audio_context.info_sending_data = true;
    } else {
        /**
         * @brief If current is VAD mode, and WWD is not detected, set to be false
         *
         */
        ama_audio_context.info_sending_data = false;
    }

    /**
     * @brief Lock the frequency.
     *
     */
    hal_dvfs_lock_control(HAL_DVFS_FULL_SPEED_104M , HAL_DVFS_LOCK);

    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_recorder_start, Audio recorder start succeed id %d", 1, ama_audio_context.info_recorder_id);
    return true;
}

static bool ama_audio_recorder_stop(void)
{
    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_recorder_stop, Current state : %d", 1, ama_audio_context.info_state);
    if (AMA_AM_STATE_STARTING == ama_audio_context.info_state) {
        return false;
    }

    if (AMA_AM_STATE_STARTED != ama_audio_context.info_state) {
        APPS_LOG_MSGID_E(APP_AMA_AUDIO", ama_audio_recorder_stop, Audio recorder already stopped. state %d", 1, ama_audio_context.info_state);
        return false;
    }

    if (AMA_AM_INVALID_ID == ama_audio_context.info_recorder_id) {
        APPS_LOG_MSGID_E(APP_AMA_AUDIO", ama_audio_recorder_stop, Audio recorder ID is 0.", 0);
        return false;
    }

    /**
     * @brief Unlock the frequency
     *
     */
    hal_dvfs_lock_control(HAL_DVFS_FULL_SPEED_104M , HAL_DVFS_UNLOCK);

    bool current_sending_data = ama_audio_context.info_sending_data;

    ama_audio_context.info_sending_data = false;
    ama_audio_context.info_state = AMA_AM_STATE_STOPING;
    record_control_result_t recorder_stop_result = audio_record_control_stop(ama_audio_context.info_recorder_id);
    if (RECORD_CONTROL_EXECUTION_SUCCESS != recorder_stop_result) {
        ama_audio_context.info_state = AMA_AM_STATE_STARTED;
        ama_audio_context.info_sending_data = current_sending_data;
    }

    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_recorder_stop, Audio recorder stop_result : %d", 1, recorder_stop_result);
    return true;
}

static void ama_wwe_audio_recorder_action_done(bt_sink_srv_am_cb_sub_msg_t sub_msg)
{
    if (AMA_AM_STATE_STARTING == ama_audio_context.info_state) {
        if (AUD_CMD_COMPLETE == sub_msg) {
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_wwe_audio_recorder_action_done, Start recorder succeed", 0);
            ama_audio_context.info_state = AMA_AM_STATE_STARTED;
        } else {
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_wwe_audio_recorder_action_done, Start recorder failed, error : %d", 1, sub_msg);
            ama_audio_deinit();
        }
    } else if (AMA_AM_STATE_STOPING == ama_audio_context.info_state) {
        if (AUD_CMD_COMPLETE == sub_msg) {
            ama_audio_deinit();
            if (ama_audio_context.info_wwd_is_restart) {
                ama_audio_context.info_wwd_is_restart = false;
                ama_audio_recorder_start(ama_audio_context.info_wwd_restart_mode);
            }
        } else {
            if (ama_audio_context.info_wwd_is_restart) {
                ama_audio_context.info_wwd_is_restart = false;
            }
            ama_audio_context.info_state = AMA_AM_STATE_STARTED;
        }
    }
}

/**
 * @brief The audio task
 *
 * @param arg
 */
void ama_port_task_main(void *arg)
{
    ama_msg_item_t ama_msg_item_event;
    BaseType_t ret = -1;
    ama_audio_task_queue = xQueueCreate(AMA_TASK_QUEUE_SIZE, AMA_TASK_QUEUE_ITEM_SIZE);

    while (1) {
        if (ama_audio_context.info_running == false) {
            vTaskDelay((30 / portTICK_RATE_MS));
            continue;
        }

        // uint16_t num = ama_audio_get_item_num(ama_audio_task_queue);

        ret = xQueueReceive((QueueHandle_t)ama_audio_task_queue, &ama_msg_item_event, portMAX_DELAY / portTICK_PERIOD_MS);

        if (ret == pdPASS) {
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", [AUDIO_TASK] ama_audio_event_id : %d, state : %d, send : %d",
                                3, ama_msg_item_event.event_id, ama_audio_context.info_state, ama_audio_context.info_sending_data);

            switch (ama_msg_item_event.event_id) {
                case AMA_TASK_EVENT_AUDIO_WWD_NOTIFICATION:
                {
                    ama_handle_wwd_notification((uint32_t)ama_msg_item_event.data);
                }
                break;

                case AMA_TASK_EVENT_AUDIO_DATA_ABORT_NOTIFICATION:
                {
                    /**
                     * @brief
                     * Clear the opus data that already encoded.
                     */
                    APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][AUDIO_TASK] AMA_TASK_EVENT_AUDIO_DATA_ABORT_NOTIFICATION, Data abort notification, clear the opus buffer", 0);
                    //audio_opus_encoder_clear_payload();
                    if (ama_audio_context.info_codec_manager_handle != 0) {
                        audio_codec_buffer_mode_clear_output_data(ama_audio_context.info_codec_manager_handle);
                    }
                }
                break;

                case AMA_TASK_EVENT_AUDIO_RESTART:
                {
                    if (ama_audio_recorder_wwd_restart((wwe_mode_t)ama_msg_item_event.data) && AMA_AM_STATE_STOPING == ama_audio_context.info_state) {
                        ama_audio_context.info_wwd_is_restart = true;
                    }
                }
                break;

                case AMA_TASK_EVENT_AUDIO_STREAM_IN:
                {
                    if (ama_audio_context.info_state != AMA_AM_STATE_STARTED) {
                        /**
                         * @brief If current is not started, ignore the event.
                         * Fix issue : BTA-6606
                         */
                        break;
                    }

                    /**
                     * @brief
                     * Encode the PCM audio data to opus format.
                     * TODO if current is ready to send the audio data, need send event to queue to send data to SP.
                     */
                    uint32_t dsp_pcm_size = audio_record_control_get_share_buf_data_byte_count();
                    //uint32_t opus_frame_num = dsp_pcm_size / OPUS_ENCODER_IN_FRAME_SIZE;
                    uint32_t opus_frame_num = dsp_pcm_size / ama_audio_context.info_pcm_length_in_one_frame;
                    APPS_LOG_MSGID_I(APP_AMA_AUDIO", [AUDIO_TASK] AMA_TASK_EVENT_AUDIO_STREAM_IN; Read (PCM data) : %d, opus_frame_num : %d", 2, dsp_pcm_size, opus_frame_num);
                    uint8_t index;

                    if (ama_audio_context.info_pcm_read_buffer == NULL) {
                        APPS_LOG_MSGID_I(APP_AMA_AUDIO", [AUDIO_TASK] AMA_TASK_EVENT_AUDIO_STREAM_IN, pcm read buffer is NULL", 0);
                        assert(false);
                    }

                    uint32_t pcm_codec_len = 0;

                    for (index = 0; index < opus_frame_num; index ++) {
                        if (ama_audio_context.info_state == AMA_AM_STATE_IDLE) {
                            APPS_LOG_MSGID_I(APP_AMA_AUDIO", [AUDIO_TASK] AMA_TASK_EVENT_AUDIO_STREAM_IN; the audio state changed to stopped, break", 0);
                            break;
                        }
                        pcm_codec_len = ama_audio_context.info_pcm_length_in_one_frame;
                        memset(ama_audio_context.info_pcm_read_buffer, 0, sizeof(uint8_t) * ama_audio_context.info_pcm_length_in_one_frame);
                        if (RECORD_CONTROL_EXECUTION_SUCCESS == audio_record_control_read_data(ama_audio_context.info_pcm_read_buffer, ama_audio_context.info_pcm_length_in_one_frame)) {

                            audio_codec_manager_status_t st = audio_codec_buffer_mode_process(ama_audio_context.info_codec_manager_handle,
                                                                                              ama_audio_context.info_pcm_read_buffer,
                                                                                              &pcm_codec_len);
                            if (st != AUDIO_CODEC_MANAGER_SUCCESS) {
                                APPS_LOG_MSGID_I(APP_AMA_AUDIO", [AUDIO_TASK] opus encode FAILED, result : %d", 1, st);
                            }
                        }
                    }

                    if (ama_audio_context.info_sending_data == true) {
                        ama_msg_item_t event;
                        event.event_id = AMA_TASK_EVENT_AUDIO_SEND_DATA;
                        event.data = NULL;
                        ama_audio_queue_send((QueueHandle_t)ama_audio_task_queue, &event);
                    }
                }
                break;

                case AMA_TASK_EVENT_AUDIO_SEND_DATA:
                {
                    ama_send_audio_data();
                }
                break;

                case AMA_TASK_EVENT_AUDIO_START:
                {
                    ama_audio_recorder_start((wwe_mode_t)ama_msg_item_event.data);
                }
                break;

                case AMA_TASK_EVENT_AUDIO_STOP:
                {
                    ama_audio_recorder_stop();
                }
                break;


                case AMA_TASK_EVENT_AUDIO_START_DONE:
                {
                    if (AMA_AM_STATE_STARTING != ama_audio_context.info_state) {
                        break;
                    }

                    if (AUD_CMD_COMPLETE == (uint32_t)ama_msg_item_event.data) {
                        ama_audio_context.info_state = AMA_AM_STATE_STARTED;
                    } else {
                        ama_audio_deinit();
                    }
                }
                break;

                case AMA_TASK_EVENT_AUDIO_ACTION_DONE:
                {
                    ama_wwe_audio_recorder_action_done((bt_sink_srv_am_cb_sub_msg_t)ama_msg_item_event.data);
                }
                break;

                case AMA_TASK_EVENT_AUDIO_STOP_BY_IND:
                {
                     if (AMA_AM_STATE_IDLE != ama_audio_context.info_state) {
                        ama_audio_deinit();

                        if (ama_audio_context.info_recorder_stopped_callback) {
                            ama_audio_context.info_recorder_stopped_callback();
                        }
                     }
                }
                break;

                default:
                break;
            }
        }
    }
}

static void ama_audio_control_ccni_callback(hal_audio_event_t event, void *data)
{
    ama_msg_item_t ama_msg_item_event;

    switch (event) {
        case HAL_AUDIO_EVENT_WWD_NOTIFICATION:
        {
            ama_msg_item_event.event_id = AMA_TASK_EVENT_AUDIO_WWD_NOTIFICATION;
            ama_msg_item_event.data = (uint8_t *)data;
            ama_audio_queue_send((QueueHandle_t)ama_audio_task_queue, &ama_msg_item_event);
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][RECORDER][CALLBACK] HAL_AUDIO_EVENT_WWD_NOTIFICATION", 0);
        }
        break;

        case HAL_AUDIO_EVENT_DATA_ABORT_NOTIFICATION:
        {
            ama_msg_item_event.event_id = AMA_TASK_EVENT_AUDIO_DATA_ABORT_NOTIFICATION;
            ama_msg_item_event.data = NULL;
            ama_audio_queue_send((QueueHandle_t)ama_audio_task_queue, &ama_msg_item_event);
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", [WWD][RECORDER][CALLBACK] HAL_AUDIO_EVENT_DATA_ABORT_NOTIFICATION", 0);
        }
        break;

        case HAL_AUDIO_EVENT_DATA_NOTIFICATION:
        {
            ama_msg_item_event.event_id = AMA_TASK_EVENT_AUDIO_STREAM_IN;
            ama_msg_item_event.data = NULL;
            ama_audio_queue_send((QueueHandle_t)ama_audio_task_queue, &ama_msg_item_event);
        }
        break;

        case HAL_AUDIO_EVENT_ERROR:
        break;

        default:
        break;
    }
}

/**
 * @brief The audio codec manager callback handler
 *
 * @param handle
 * @param event
 * @param user_data
 */
static void ama_audio_codec_manager_callback(audio_codec_manager_handle_t handle, audio_codec_manager_event_t event, void *user_data)
{
    if (handle != ama_audio_context.info_codec_manager_handle) {
        return;
    }

    switch (event) {
        case AUDIO_CODEC_MANAGER_EVENT_OUTPUT_BUFFER_FULL:
        {
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_codec_manager_callback, Codec buffer full", 0);
            if (ama_audio_context.info_sending_data == false) {
                uint32_t total_len = audio_codec_buffer_mode_get_output_data_length(handle);
                audio_codec_buffer_mode_read_done(handle, total_len);
            } else {
                APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_codec_manager_callback, Current is sending data (ERROR), maybe error happen", 0);
            }
        }
    }
}

static void ama_audio_am_callback(bt_sink_srv_am_id_t aud_id,
                                  bt_sink_srv_am_cb_msg_class_t msg_id,
                                  bt_sink_srv_am_cb_sub_msg_t sub_msg,
                                  void *parm)
{
    ama_msg_item_t ama_msg_item_event;

    APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_am_callback id : %d, msg_id : %d, sub_msg : %d, state : %d", 4, aud_id, msg_id, sub_msg, ama_audio_context.info_state);

    switch (msg_id) {
        case AUD_SINK_OPEN_CODEC:
        {
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_am_callback, Recorder open done", 0);
            ama_msg_item_event.event_id = AMA_TASK_EVENT_AUDIO_START_DONE;
            ama_msg_item_event.data = (uint8_t *)sub_msg;
            ama_audio_queue_send_front((QueueHandle_t)ama_audio_task_queue, &ama_msg_item_event);
            break;
        }

        case AUD_SELF_CMD_REQ:
        {
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_am_callback, Recorder audio action done", 0);
            ama_msg_item_event.event_id = AMA_TASK_EVENT_AUDIO_ACTION_DONE;
            ama_msg_item_event.data = (uint8_t *)sub_msg;
            ama_audio_queue_send_front((QueueHandle_t)ama_audio_task_queue, &ama_msg_item_event);
            break;
        }

        case AUD_SUSPEND_BY_IND:
        {
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_am_callback, Recorder suspended by 0x%x", sub_msg);
            if (AUD_SUSPEND_BY_HFP == sub_msg) {
                ama_msg_item_event.event_id = AMA_TASK_EVENT_AUDIO_STOP_BY_IND;
                ama_audio_queue_send_front((QueueHandle_t)ama_audio_task_queue, &ama_msg_item_event);
            }
            break;
        }

        default:
            APPS_LOG_MSGID_I(APP_AMA_AUDIO", default", 0);
            break;
    }
}

void ama_audio_set_wwd_trigger_callback(on_wwd_trigger trigger_callback)
{
    if (trigger_callback != NULL) {
        ama_audio_context.info_wwd_start_speech_callback = trigger_callback;
    }
}

void ama_audio_init(on_recorder_stop_record callback)
{
    if (callback != NULL) {
        ama_audio_context.info_recorder_stopped_callback = callback;
    }
}

void ama_audio_start(wwe_mode_t wwe_mode, uint32_t wwe_flash_address, uint32_t wwe_length)
{
    if (wwe_mode == WWE_MODE_AMA && (wwe_length == 0 || wwe_flash_address == 0)) {
        APPS_LOG_MSGID_I(APP_AMA_AUDIO", ama_audio_start, mode is AMA, but length/address is 0", 0);
        return;
    }
    ama_msg_item_t ama_msg_item_event;
    ama_msg_item_event.event_id = AMA_TASK_EVENT_AUDIO_START;
    ama_msg_item_event.data = (uint8_t *)wwe_mode;

    if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true && wwe_mode == WWE_MODE_AMA) {
        ama_audio_context.info_wwd_flash_address = wwe_flash_address;
        ama_audio_context.info_wwd_length = wwe_length;
    }

    if (ama_audio_queue_send((QueueHandle_t)ama_audio_task_queue, &ama_msg_item_event) == 0) {
        ama_audio_context.info_running = true;
    }
}

void ama_audio_stop(void)
{
    ama_msg_item_t ama_msg_item_event;
    ama_msg_item_event.event_id = AMA_TASK_EVENT_AUDIO_STOP;
    ama_msg_item_event.data = NULL;
    if (ama_audio_queue_send((QueueHandle_t)ama_audio_task_queue, &ama_msg_item_event) == 0) {
        //ama_wwe_running = false;
    }
}

#endif

