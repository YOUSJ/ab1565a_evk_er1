/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "app_ama_idle_activity.h"
#include "apps_config_event_list.h"
#include "apps_aws_sync_event.h"
#include "battery_management.h"
#include "battery_management_core.h"
#include "apps_events_battery_event.h"
#include "apps_events_event_group.h"
#include "apps_config_key_remapper.h"
#include "apps_events_key_event.h"
#include "bt_init.h"
#include "apps_config_vp_index_list.h"
#include "app_voice_prompt.h"
#include "multi_va_manager.h"

//extern void app_ama_init(void);
#ifdef MTK_AMA_ENABLE

bool app_start_ama_activity(void *extra_data, size_t data_len)
{
    ui_shell_start_activity(NULL, app_ama_activity_proc, ACTIVITY_PRIORITY_MIDDLE, extra_data, data_len);
    return false;
}

static bool _proc_ui_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data,size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    switch (event_id) {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE: {
            APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", create", 0);
            // self->local_context = (void*)pvPortMalloc(sizeof(ama_context_t));
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY: {
            APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", destroy", 0);
            // if (self->local_context) {
            //     vPortFree(self->local_context);
            // }
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESUME: {
            APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", resume", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE: {
            APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", pause", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH: {
            APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", refresh", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESULT: {
            APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", result", 0);
            break;
        }
        default:
            break;
    }
    return ret;
}

#if MTK_AWS_MCE_ENABLE
static bool _proc_key_event_group(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    ama_context_t *local_context = (ama_context_t*)self->local_context;
    uint8_t key_id;
    airo_key_event_t key_event;

    app_event_key_event_decode(&key_id, &key_event, event_id);

    apps_config_key_action_t action = apps_config_key_event_remapper_map_action(key_id, key_event);
    APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", key_id : %x, key_event : %x, event_id : %x", 3, key_id, key_event, event_id);
    if (action == KEY_AMA_START_NOTIFY || action == KEY_AMA_START)
    {
        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER) {
            if (BT_STATUS_SUCCESS != apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_KEY, action)) {
                APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", Partner send KEY_AMA aws to agent failed", 0);
            } else {
                APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", Partner send KEY_AMA aws to agent success", 0);
            }
        }
        ret = true;
    }
    return ret;
}
#endif

#if defined(MTK_AWS_MCE_ENABLE) && defined(SUPPORT_ROLE_HANDOVER_SERVICE) && defined(BT_ROLE_HANDOVER_WITH_SPP_BLE)
bool app_ama_idle_activity_process_aws_data_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    bt_aws_mce_report_info_t *aws_data_ind = (bt_aws_mce_report_info_t *)extra_data;
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();

    if (aws_data_ind->module_id == BT_AWS_MCE_REPORT_MODULE_APP_ACTION && role == BT_AWS_MCE_ROLE_AGENT) {
        uint32_t event_group;
        uint32_t event_id;
        void *data = NULL;
        uint32_t data_len = 0;

        apps_aws_sync_event_decode_extra(aws_data_ind, &event_group, &event_id, &data, &data_len);
        if (event_group == EVENT_GROUP_UI_SHELL_BT_AMA && event_id == AMA_CONNECT_CFM) {
            AMA_CONNECT_CFM_T *cnf = (AMA_CONNECT_CFM_T *)data;
            if (cnf != NULL && cnf->status == 0) {
                APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", aws_data->AMA_CONNECT_CFM, Connected address : 0x%x:%x:%x:%x:%x:%x", 6,
                            cnf->bdAddr.addr[0], cnf->bdAddr.addr[1], cnf->bdAddr.addr[2],
                            cnf->bdAddr.addr[3], cnf->bdAddr.addr[4], cnf->bdAddr.addr[5]);

                uint8_t *addr = malloc(sizeof(uint8_t) * 6);
                memcpy(addr, cnf->bdAddr.addr, 6);
                ret = app_start_ama_activity(addr, sizeof(BD_ADDR_T));
            }
        }
    }
    return ret;
}
#endif

bool app_ama_idle_activity_proc(
            struct _ui_shell_activity *self,
            uint32_t event_group,
            uint32_t event_id,
            void *extra_data,
            size_t data_len)
{
    bool ret = false;
    // APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", event_group : %x, id : %x", 2, event_group, event_id);
    switch (event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM: {
            ret = _proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;
        }
#if MTK_AWS_MCE_ENABLE
        case EVENT_GROUP_UI_SHELL_KEY: {
            ret = _proc_key_event_group(self, event_id, extra_data, data_len);
            break;
        }
#endif
        case EVENT_GROUP_UI_SHELL_BT_SINK:
        {
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_AMA: {
            if (event_id == AMA_START_SERVICE_CFM)
            {
                APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", EVENT_GROUP_UI_SHELL_BT_AMA, Start service success", 0);
            } else if (event_id == AMA_CONNECT_CFM) {

                AMA_CONNECT_CFM_T *cnf = (AMA_CONNECT_CFM_T *)extra_data;
                if (cnf) {
                    // ama_context_t *context = (ama_context_t *)self->local_context;

                    // memcpy(&(context->addr), &(cnf->bdAddr), sizeof(BD_ADDR_T));

                    APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", AMA_CONNECT_CFM, Connected address : 0x%x:%x:%x:%x:%x:%x", 6,
                                cnf->bdAddr.addr[0], cnf->bdAddr.addr[1], cnf->bdAddr.addr[2],
                                cnf->bdAddr.addr[3], cnf->bdAddr.addr[4], cnf->bdAddr.addr[5]);

                    uint8_t *addr = malloc(sizeof(uint8_t) * 6);
                    memcpy(addr, cnf->bdAddr.addr, 6);

                    ret = app_start_ama_activity(addr, sizeof(BD_ADDR_T));
                }
            }
            break;
        }
#if defined(MTK_AWS_MCE_ENABLE) && defined(SUPPORT_ROLE_HANDOVER_SERVICE) && defined(BT_ROLE_HANDOVER_WITH_SPP_BLE)
        case EVENT_GROUP_UI_SHELL_AWS_DATA:
        {
            app_ama_idle_activity_process_aws_data_proc(self, event_id, extra_data, data_len);
            break;
        }
#endif
        default:
            break;
    }

    // APPS_LOG_MSGID_I(APP_IDLE_AMA_ACTIVITY", ret : %x", 1, ret);
    return ret;
}

#endif
