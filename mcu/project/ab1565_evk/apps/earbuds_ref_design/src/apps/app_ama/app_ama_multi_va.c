/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifdef MTK_AMA_ENABLE
#include "multi_va_manager.h"
#include "bt_customer_config.h"
#include "FreeRTOS.h"
#include "BtAma.h"
#include "apps_debug.h"
#include "bt_app_common.h"
#include "app_ama_activity.h"

#define LOG_AMA_TAG     "[Multi_VA_AMA] "

static void ama_voice_assistant_init(bool selected)
{
    APPS_LOG_MSGID_E(LOG_AMA_TAG"voice_assistant_init:%d", 1, selected);

    app_ama_init();
    AMA_Target_SetSelected(selected);
}

static multi_va_switch_off_return_t ama_type_switch(bool selected)
{
    //TO do call api get connected or not
    APPS_LOG_MSGID_E(LOG_AMA_TAG"ama_type_switch, selected : %d", 1, selected);
    if (!selected) // be switch other
    {
        bool b = app_ama_multi_va_set_configuration(false);
        if (b == false) {
            return MULTI_VA_SWITCH_OFF_SET_INACTIVE_DONE;
        } else {
            return MULTI_VA_SWITCH_OFF_WAIT_INACTIVE;
        }
    }
    return MULTI_VA_SWITCH_OFF_SET_INACTIVE_DONE;
}

static uint32_t ama_on_get_ble_adv_data(multi_ble_adv_info_t *adv_info)
{
    uint16_t len;
    bt_status_t ret;
    uint8_t default_unique_id[16] = {0};

    uint8_t data_context[31] = {0};
    char ble_name[BT_GAP_LE_MAX_DEVICE_NAME_LENGTH] = { 0 };
    adv_info->adv_data->advertising_data_length = 31;
    adv_info->scan_rsp->scan_response_data_length = 31;

    AMA_Target_Build_Adv_Data((void *)data_context, &len);
    APPS_LOG_DUMP_I("ama_adv_data1", data_context, len);
    Ama_Memcpy(adv_info->adv_data->advertising_data , data_context, len);
    adv_info->adv_data->advertising_data_length = len;
    APPS_LOG_DUMP_I("ama_adv_data2", adv_info->adv_data->advertising_data, adv_info->adv_data->advertising_data_length);

    bt_customer_config_get_ble_device_name(ble_name);
    len = strlen(ble_name);

    if(len > 31 - 12) {
       len = 31 - 12;
    }
    adv_info->scan_rsp->scan_response_data[0] = len + 1;
    adv_info->scan_rsp->scan_response_data[1] = BT_GAP_LE_AD_TYPE_NAME_COMPLETE;

    memcpy(&adv_info->scan_rsp->scan_response_data[2], ble_name, len);

    // gen unique_id
    bt_bd_addr_t *edr_addr = bt_device_manager_get_local_address();
    ret = bt_app_common_generate_unique_id((const uint8_t *)edr_addr,
                                            BT_BD_ADDR_LEN,
                                            default_unique_id);
    if (BT_STATUS_SUCCESS != ret) {
        APPS_LOG_MSGID_E(LOG_AMA_TAG" AMA gen unique_id fail: %d", 1, ret);
    } else {
        adv_info->scan_rsp->scan_response_data[len + 2] = BT_APP_COMMON_UNIQUE_ID_MAX_LEN + 1;
        adv_info->scan_rsp->scan_response_data[len + 3] = BT_GAP_LE_AD_TYPE_MANUFACTURER_SPECIFIC;
        memcpy(adv_info->scan_rsp->scan_response_data + len + 4, default_unique_id, BT_APP_COMMON_UNIQUE_ID_MAX_LEN);
    }

    return MULTI_BLE_ADV_NEED_GEN_ADV_PARAM;
}


static const multi_va_manager_callbacks_t multi_cb =
{
    ama_voice_assistant_init,
    ama_type_switch,
    ama_on_get_ble_adv_data
};

void apps_register_ama_in_multi_voice(void)
{
    multi_voice_assistant_manager_register_instance(MULTI_VA_TYPE_AMA, &multi_cb);
}


void apps_ama_multi_va_set_inactive_done(void)
{

    multi_voice_assistant_manager_set_inactive_done(MULTI_VA_TYPE_AMA);

}

#endif
