/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "apps_config_event_list.h"
#include "apps_aws_sync_event.h"
#include "apps_config_vp_manager.h"
#include "battery_management.h"
#include "battery_management_core.h"
#include "apps_events_battery_event.h"
#include "apps_events_event_group.h"
#include "apps_config_key_remapper.h"
#include "apps_events_key_event.h"

#include "app_ama_activity.h"
#include "app_ama_audio.h"
#include "app_voice_prompt.h"
#include "apps_config_vp_index_list.h"
#include "multi_va_manager.h"
#include "bt_connection_manager.h"
#include "app_rho_idle_activity.h"

#ifdef MTK_AMA_ENABLE

static void app_ama_get_state_ind_handler(AMA_GET_STATE_IND_T * ind);
static void app_ama_media_control_handler(AMA_ISSUE_MEDIA_CONTROL_IND_T * ind);

ama_context_t *ama_activity_context = NULL;

/**
 * @brief Need configure the WWE voice data address according to the WWE mode
 * Need change by model manager.
 */
uint32_t wwe_flash_address = 0x00;
uint32_t wwe_flash_length = 0xC8C4;

static bool start_speech_with_wwd(bool from_agent, uint32_t stop_index)
{
    AMA_START_SPEECH_T *startSpeechParam = NULL;

    if (stop_index <= AMA_AUDIO_WWD_START_INDEX) {
        APPS_LOG_MSGID_I(APP_AMA_ACTI", [WWD] start_speech_with_wwd, Start recognition failed, Stop index error : %d", 1, stop_index);
        return false;
    }

    if (ama_activity_context->recognition_state != AMA_ACTIVITY_RECOGNITION_STATE_IDLE) {
        APPS_LOG_MSGID_I(APP_AMA_ACTI", [WWD] start_speech_with_wwd, Start recognition failed, Current speech state is not IDLE : %d", 1, ama_activity_context->recognition_state);
        // ama_audio_stop();
        /**
         * @brief Fix issue : BTA-7506
         * That when the start speech failed, need restart the WWE for next round.
         */
        ama_audio_restart(WWE_MODE_AMA, wwe_flash_address, wwe_flash_length);
        return false;
    }

    startSpeechParam = (AMA_START_SPEECH_T *)malloc(sizeof(AMA_START_SPEECH_T));
    if (startSpeechParam == NULL) {
        APPS_LOG_MSGID_I(APP_AMA_ACTI", [WWD] start_speech_with_wwd, Start recognition failed, cause allocate memory for start speech parameter failed", 0);
        return false;
    }

    APPS_LOG_MSGID_I(APP_AMA_ACTI", [WWD] start_speech_with_wwd, Start speech with stop index : %d", 1, stop_index);

    if (from_agent == true) {
        apps_config_set_vp(VP_INDEX_PRESS, true, 100, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
    }

    startSpeechParam->audioProfile = AMA_AUDIO_PROFILE_NEAR_FIELD;
    startSpeechParam->audioFormat = AMA_AUDIO_FORMAT_OPUS_16KHZ_32KBPS_CBR_0_20MS;
    startSpeechParam->audioSource = AMA_AUDIO_SOURCE_STREAM;
    startSpeechParam->type = AMA_SPEECH_INITIATOR_TYPE_WAKEWORD;
    // startSpeechParam->suppressEarcon = 0;
    startSpeechParam->suppressEndpointEarcon = 0;
    startSpeechParam->suppressStartEarcon = 0;
    startSpeechParam->wake_word.startIndexInSamples = AMA_AUDIO_WWD_START_INDEX;
    startSpeechParam->wake_word.endIndexInSamples = stop_index;
    startSpeechParam->wake_word.metadataLength = 0;
    startSpeechParam->wake_word.nearMiss = false;

    ama_activity_context->trigger_mode = AMA_SPEECH_INITIATOR_TYPE_WAKEWORD;
    ama_activity_context->recognition_state = AMA_ACTIVITY_RECOGNITION_STATE_STARTING;

    APPS_LOG_MSGID_I(APP_AMA_ACTI", [WWD] start_speech_with_wwd, Start speech : address 0x%x:%x:%x:%x:%x:%x", 6,
                            ama_activity_context->addr.addr[0], ama_activity_context->addr.addr[1], ama_activity_context->addr.addr[2],
                            ama_activity_context->addr.addr[3], ama_activity_context->addr.addr[4], ama_activity_context->addr.addr[5]);

    AMAStatus status = AMA_Target_StartSpeech(&(ama_activity_context->addr), startSpeechParam);

    free(startSpeechParam);
    startSpeechParam = NULL;

    APPS_LOG_MSGID_I(APP_AMA_ACTI", [WWD] start_speech_with_wwd, Start speech result : %d", 1, status);

    if (status == AMA_STATUS_OK) {
        return true;
    }
    ama_activity_context->recognition_state = AMA_ACTIVITY_RECOGNITION_STATE_IDLE;
    return false;
}

static bool ama_wwd_trigger_handler(uint32_t stop_index)
{

    APPS_LOG_MSGID_I(APP_AMA_ACTI", [WWD] ama_wwd_trigger_handler, Stop index : %d", 1, stop_index);

    if (stop_index <= AMA_AUDIO_WWD_START_INDEX) {
        return false;
    }

#ifdef MTK_AWS_MCE_ENABLE
    if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER) {
        apps_config_set_vp(VP_INDEX_PRESS, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);

        bt_status_t status = apps_aws_sync_event_send_extra(EVENT_GROUP_UI_SHELL_KEY,
                                                            KEY_AMA_START,
                                                            (void *)&stop_index,
                                                            sizeof(uint32_t));
        if (BT_STATUS_SUCCESS != status) {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", [WWD] ama_wwd_trigger_handler, Partner send KEY_AMA_START aws to agent failed", 0);
            return false;
        } else {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", [WWD] ama_wwd_trigger_handler, Partner send KEY_AMA_START aws to agent success", 0);
            return true;
        }
    } else
#endif /* MTK_AWS_MCE_ENABLE */
    {
        return start_speech_with_wwd(true, stop_index);
    }
}

static bool audio_recorder_stopped_callback_handler(void)
{
    AMAStatus status = AMA_Target_StopSpeech(&(ama_activity_context->addr));
    APPS_LOG_MSGID_I(APP_AMA_ACTI", audio_recorder_stopped_callback_handler, Stop speech result :%d", 1, status);
    if (status == AMA_STATUS_OK) {
        return true;
    }

    return false;
}

static bool _proc_ui_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    switch (event_id) {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE: {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", create", 0);

            if (extra_data == NULL || data_len == 0) {
                assert(false);
            }

            ama_activity_context = (ama_context_t *)malloc(sizeof(ama_context_t));
            if (ama_activity_context == NULL) {
                assert(false);
            }

            wwe_flash_address = LM_BASE + 0x10000;

            memset(ama_activity_context, 0, sizeof(ama_context_t));
            memcpy(ama_activity_context->addr.addr, extra_data, data_len);

            ama_activity_context->recognition_state = AMA_ACTIVITY_RECOGNITION_STATE_IDLE;
            ama_activity_context->trigger_mode = AMA_SPEECH_INITIATOR_TYPE_NONE;
            ama_activity_context->avrcp_override = false;

            multi_va_type_t type = multi_va_manager_get_current_va_type();
            APPS_LOG_MSGID_I(APP_AMA_ACTI", [Multi_AI] on_create_type: %x", 1, type);
            if (type == MULTI_VA_TYPE_AMA) {
                multi_voice_assistant_manager_notify_va_connected(MULTI_VA_TYPE_AMA);
            }

            /**
             * @brief Init the audio recorder for WWD enable.
             *
             */
            ama_audio_init(audio_recorder_stopped_callback_handler);

            if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true) {
                /**
                 * @brief Configure the WWD triggered callback
                 *
                 */
                ama_audio_set_wwd_trigger_callback(ama_wwd_trigger_handler);
                /**
                 * @brief Start recorder to trigger WWE, and detect the key word.
                 *
                 */
                ama_audio_start(WWE_MODE_AMA, wwe_flash_address, wwe_flash_length);
            }
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY: {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", destroy", 0);

            /**
             * @brief Fix issue
             * When the AMA connected, and disconnected in a short time, the recorder is not start,
             * So the stop execution in the disconnect indication is not useful.
             * Make sure the audio recorder execute stop operation.
             */
            ama_audio_stop();

            if (ama_activity_context != NULL) {
                free(ama_activity_context);
                ama_activity_context = NULL;
            }
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESUME: {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", resume", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE: {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", pause", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH: {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", refresh", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESULT: {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", result", 0);
            break;
        }
        default:
            break;
    }
    return ret;
}

static AMAStatus app_ama_start_speech_with_tap_or_hld()
{
    if (ama_activity_context->recognition_state != AMA_ACTIVITY_RECOGNITION_STATE_IDLE) {
        APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_start_speech_with_tap_or_hld, Start recognition failed, Current state error : %d", 1, ama_activity_context->recognition_state);
        return AMA_STATUS_ERROR;
    }
    if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true) {
        /**
         * @brief If current support WWD feature, need stop recorder firstly
         *
         */
        APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_start_speech_with_tap_or_hld, WWD enabled, stop audio recorder firstly", 0);
        ama_audio_stop();
    }

    AMA_START_SPEECH_T startSpeechParam;

    if (AMA_TRIGGER_MODE_PRESS_AND_HOLD_CONFIGURED == true) {
        startSpeechParam.audioProfile = AMA_AUDIO_PROFILE_NEAR_FIELD;
        startSpeechParam.type = AMA_SPEECH_INITIATOR_TYPE_PRESS_AND_HOLD;
    } else if (AMA_TRIGGER_MODE_TAP_CONFIGURED == true) {
        startSpeechParam.audioProfile = AMA_AUDIO_PROFILE_CLOSE_TALK;
        startSpeechParam.type = AMA_SPEECH_INITIATOR_TYPE_TAP;
    }

    startSpeechParam.audioFormat = AMA_AUDIO_FORMAT_OPUS_16KHZ_32KBPS_CBR_0_20MS;
    startSpeechParam.audioSource = AMA_AUDIO_SOURCE_STREAM;
    // startSpeechParam.suppressEarcon = 0;
    startSpeechParam.suppressEndpointEarcon = 0;
    startSpeechParam.suppressStartEarcon = 0;

    ama_activity_context->trigger_mode = AMA_SPEECH_INITIATOR_TYPE_TAP;
    ama_activity_context->recognition_state = AMA_ACTIVITY_RECOGNITION_STATE_STARTING;

    APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_start_speech_with_tap_or_hld, address : 0x%x:%x:%x:%x:%x:%x", 6,
                        ama_activity_context->addr.addr[0], ama_activity_context->addr.addr[1], ama_activity_context->addr.addr[2],
                        ama_activity_context->addr.addr[3], ama_activity_context->addr.addr[4], ama_activity_context->addr.addr[5]);

    AMAStatus status = AMA_Target_StartSpeech(&(ama_activity_context->addr), &startSpeechParam);

    APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_start_speech_with_tap_or_hld, Start recognition result : 0x%x", 1, status);

    if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true) {
        if (status != AMA_STATUS_OK) {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_start_speech_with_tap_or_hld, Start recognition failed, start WWE again", 0);
            ama_activity_context->recognition_state = AMA_ACTIVITY_RECOGNITION_STATE_IDLE;
            ama_audio_start(WWE_MODE_AMA, wwe_flash_address, wwe_flash_length);
        }
    }

    return status;
}

static bool _proc_key_event_group(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;

    uint16_t key_id = *(uint16_t *)extra_data;

    if (key_id == KEY_AMA_START) {
        APPS_LOG_MSGID_I(APP_AMA_ACTI", _proc_key_event_group, KEY_AMA_START Execution", 0);
#ifdef MTK_AWS_MCE_ENABLE
        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER) {
            uint32_t dummy_value = 0;
            /**
             * @brief Send the dummy data to agent which to indicate the event from key or WWD
             *
             */
            bt_status_t send_result = apps_aws_sync_event_send_extra(EVENT_GROUP_UI_SHELL_KEY, KEY_AMA_START, &dummy_value, sizeof(uint32_t));
            if (BT_STATUS_SUCCESS != send_result) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", _proc_key_event_group, Partner send KEY_AMA_START aws event to agent failed", 0);
            } else {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", _proc_key_event_group, Partner send KEY_AMA_START aws event to agent success", 0);
            }
        } else
#endif
        {
            app_ama_start_speech_with_tap_or_hld();
        }
        ret = true;
    } else if (KEY_AMA_START_NOTIFY == key_id) {
        APPS_LOG_MSGID_I(APP_AMA_ACTI", _proc_key_event_group, KEY_AMA_START_NOTIFY Execution", 0);
        apps_config_set_vp(VP_INDEX_PRESS, true, 100, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
        ret = true;
    }

#if 0
    else if (action == KEY_AMA_MEDIA_CONTROL)
    {
        AMA_ISSUE_MEDIA_CONTROL_IND_T ind;
        state = bt_sink_srv_get_state();
        contx = (ama_context_t *)(self->local_context);
        APPS_LOG_MSGID_I(APP_AMA_ACTI",state:%x", 1,state);
        memcpy(&ind.bdAddr, &contx->addr, sizeof(BD_ADDR_T));
        APPS_LOG_DUMP_I(APP_AMA_ACTI, &(ind.bdAddr), sizeof(BD_ADDR_T));
        if (state == BT_SINK_SRV_STATE_STREAMING)
        {

            ind.control= AMA_MEDIA_CONTROL_PAUSE;
            app_ama_media_control_handler(contx, &ind);

        } else if (state == BT_SINK_SRV_STATE_CONNECTED)
        {
            //memcpy(&ind.bdAddr, &contx->addr, sizeof(BD_ADDR_T));
            ind.control= AMA_MEDIA_CONTROL_PLAY;
            app_ama_media_control_handler(contx, &ind);
        }

    }
#endif
    return ret;
}

static void app_ama_get_state_ind_handler(AMA_GET_STATE_IND_T * ind)
{
    AMA_STATE_T state;
    AMA_ERROR_CODE_E errCode = AMA_ERROR_CODE_SUCCESS;

    APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_get_state_ind_handler : feature:%x", 1, ind->feature);

    state.feature = ind->feature;

    switch (ind->feature)
    {
        case AMA_STATE_FEATURE_BT_DISCOVERABLE:
        {
            state.valueType = AMA_VALUE_TYPE_BOOLEAN;
            state.value.integer = 1;
        }
        break;
        case AMA_STATE_FEATURE_A2DP_ENABLED:
        case AMA_STATE_FEATURE_HFP_ENABLED:
        {
            state.valueType = AMA_VALUE_TYPE_BOOLEAN;
            state.value.integer = 1;
            break;
        }
        case AMA_STATE_FEATURE_A2DP_CONNECTED:
        {
            bt_cm_profile_service_state_t profile_state = bt_cm_get_profile_service_state(ama_activity_context->addr.addr, BT_CM_PROFILE_SERVICE_A2DP_SINK);

            state.valueType = AMA_VALUE_TYPE_BOOLEAN;
            state.value.integer = (profile_state == BT_CM_PROFILE_SERVICE_STATE_CONNECTED ?  true : false);
            APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_get_state_ind_handler, A2DP : %x", 1, state.value.integer);
            break;
        }
        case AMA_STATE_FEATURE_HFP_CONNECTED:
        {
            bt_cm_profile_service_state_t profile_state = bt_cm_get_profile_service_state(ama_activity_context->addr.addr, BT_CM_PROFILE_SERVICE_HFP);

            state.valueType = AMA_VALUE_TYPE_BOOLEAN;
            state.value.integer = (profile_state == BT_CM_PROFILE_SERVICE_STATE_CONNECTED ? true : false);
            APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_get_state_ind_handler, HFP : %x", 1, state.value.integer);
            break;
        }
        default:
        {
            errCode = AMA_ERROR_CODE_UNSUPPORTED;
            state.valueType = AMA_VALUE_TYPE_BOOLEAN;
            state.value.integer = 0;
            break;
        }
    }
    APPS_LOG_MSGID_I(APP_AMA_ACTI",get_state_ind_handler : %x, %x,", 3, ind->feature, state.value.integer);
    AMA_Target_GetStateResponse(&ind->bdAddr, &state, errCode);
}

static void app_ama_media_control_handler(AMA_ISSUE_MEDIA_CONTROL_IND_T *ind)
{
    AMAStatus status;
    bt_sink_srv_action_t action = 0;

    if (ind == NULL)
    {
        return;
    }
    APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_media_control_handler, override: %x, control: %x", 2, ama_activity_context->avrcp_override, ind->control);
    if (ama_activity_context->avrcp_override)
    {
        if (ind->control >= AMA_MEDIA_CONTROL_PLAY && ind->control <= AMA_MEDIA_CONTROL_PLAY_PAUSE)
        {
            status = AMA_Target_IssueMediaControl(&(ind->bdAddr), ind->control);

            AMA_Target_IssueMediaControlResponse(&ind->bdAddr, AMA_ERROR_CODE_SUCCESS);

        } else {
            status = AMA_Target_IssueMediaControlResponse(&ind->bdAddr, AMA_ERROR_CODE_UNKNOWN);
        }

        APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_media_control_handler, status: %x", 1, status);
        return;
    }
    switch(ind->control)
    {
        case AMA_MEDIA_CONTROL_PLAY:
        {
            action = BT_SINK_SRV_ACTION_PLAY;
            break;
        }
        case AMA_MEDIA_CONTROL_PAUSE:
        {
            action = BT_SINK_SRV_ACTION_PAUSE;
            break;
        }
        case AMA_MEDIA_CONTROL_NEXT:
        {
            action = BT_SINK_SRV_ACTION_NEXT_TRACK;
            break;
        }
        case AMA_MEDIA_CONTROL_PREVIOUS:
        {
            action = BT_SINK_SRV_ACTION_PREV_TRACK;
            break;
        }
        default:
        {
            break;
        }
    }

    bt_status_t result = bt_sink_srv_send_action(action, NULL);
    APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_media_control_handler, Send action result : 0x%x", result);

    AMA_Target_IssueMediaControlResponse(&(ama_activity_context->addr), AMA_ERROR_CODE_SUCCESS);
}

static void app_ama_set_state_ind_handler(AMA_SET_STATE_IND_T *ind)
{
    AMAStatus status;
    if (ind == NULL) {
        APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_set_state_ind_handler, ind is NULL", 0);
        return;
    }

    APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_set_state_ind_handler : feature(0x%x), value:(0x%x)", 2, ind->state.feature, ind->state.value.boolean);

    switch(ind->state.feature) {
        case AMA_STATE_FEATURE_AVRCP_OVERRIDE:
            ama_activity_context->avrcp_override = ind->state.value.boolean;
            break;

        default:
            status = AMA_Target_SetStateResponse(&ind->bdAddr, AMA_ERROR_CODE_UNSUPPORTED);
            return;
    }

    status = AMA_Target_SetStateResponse(&(ama_activity_context->addr), AMA_ERROR_CODE_SUCCESS);
    APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_set_state_ind_handler, Response result : 0x%x", 1, status);
}

void notify_state_change(AMA_VALUE_TYPE_E type, AMA_STATE_FEATURE_E feature, uint32_t value)
{
    AMA_STATE_T state;
    state.valueType = type;
    state.feature = feature;

    APPS_LOG_MSGID_I(APP_AMA_ACTI", notify_state_change, value type : %d, feature : 0x%x, value : %d", 3, type, feature, value);

    if (state.valueType == AMA_VALUE_TYPE_BOOLEAN) {
        state.value.boolean = (value != 0 ? true : false);
    } else {
        state.value.integer = value;
    }
    AMA_Target_SynchronizeState(&(ama_activity_context->addr), &state);
}

bool app_ama_activity_proc_bt_event(ui_shell_activity_t *self,
                                    uint32_t event_id,
                                    void *extra_data,
                                    size_t data_len)
{
    AMAStatus status;
    bool ret = true;

    switch(event_id)
    {
        case BT_SINK_SRV_EVENT_STATE_CHANGE:
        {
            bt_sink_srv_event_param_t *event = (bt_sink_srv_event_param_t *)extra_data;
            if (event != NULL) {
                bt_sink_srv_state_t curr_state = event->state_change.current;
                bt_sink_srv_state_t pre_state = event->state_change.previous;

                if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true) {
                    APPS_LOG_MSGID_I(APP_AMA_ACTI", [BT_SINK_SRV_EVENT_STATE_CHANGE] prev: 0x%x, current: 0x%x", 2, pre_state, curr_state);
                    if ((pre_state >= BT_SINK_SRV_STATE_INCOMING) && (pre_state != curr_state) &&
                        (curr_state < BT_SINK_SRV_STATE_INCOMING)) {
                        APPS_LOG_MSGID_I(APP_AMA_ACTI", [BT_SINK_SRV_EVENT_STATE_CHANGE] HFP is finished, need start the WWE to monitor the AMA Wake Word", 0);
                        ama_audio_start(WWE_MODE_AMA, wwe_flash_address, wwe_flash_length);
                    } else if ((curr_state >= BT_SINK_SRV_STATE_INCOMING) &&
                                (pre_state < BT_SINK_SRV_STATE_INCOMING)) {
                        APPS_LOG_MSGID_I(APP_AMA_ACTI", [BT_SINK_SRV_EVENT_STATE_CHANGE] HFP is started, need stop Wake word engine", 0);
                        ama_audio_stop();
                    }
                }
            }
            ret = false;
        }
        break;
        default:
            break;
    }
    return ret;
}

bool app_ama_activity_proc_ama_event(ui_shell_activity_t *self,
                                    uint32_t event_id,
                                    void *extra_data,
                                    size_t data_len)
{
    AMAStatus status;
    bool ret = true;

    switch(event_id)
    {
        /* CONNECTION */
        case AMA_START_SERVICE_CFM:
        {
            AMA_START_SERVICE_CFM_T *start_cnf = (AMA_START_SERVICE_CFM_T *)extra_data;
            if (start_cnf)
            {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_START_SERVICE_CFM, Confirm result : 0x%x", 1, start_cnf->status);
            }
            break;
        }
        case AMA_STOP_SERVICE_CFM:
        {
            AMA_STOP_SERVICE_CFM_T *stop_cnf = (AMA_STOP_SERVICE_CFM_T *)extra_data;
            if (stop_cnf) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_STOP_SERVICE_CFM, Confirm result : 0x%x", 1, stop_cnf->status);
            }
            break;
        }
#if 0
        case AMA_ENABLE_ADV_CFM:
        {
        }
        break;
        case AMA_DISABLE_ADV_CFM:
        {
        }
        break;

        case AMA_CONNECT_CFM:
        {
            AMA_CONNECT_CFM_T *cnf = (AMA_CONNECT_CFM_T *)extra_data;
            if (cnf) {
                memcpy(&(ama_activity_context->addr), &(cnf->bdAddr), sizeof(BD_ADDR_T));

                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_CONNECT_CFM: btaddr(%x), (%x), (%x), (%x), (%x),(%x)", 6,
                        ama_activity_context->addr.addr[0], ama_activity_context->addr.addr[1], ama_activity_context->addr.addr[2],
                        ama_activity_context->addr.addr[3], ama_activity_context->addr.addr[4], ama_activity_context->addr.addr[5]);

                ama_activity_context->recognition_state = AMA_ACTIVITY_RECOGNITION_STATE_IDLE;

                multi_va_type_t type = multi_va_manager_get_current_va_type();
                APPS_LOG_MSGID_I(APP_AMA_ACTI", start_ama: %x", 1, type);
                if (type == MULTI_VA_TYPE_AMA || type == MULTI_VA_TYPE_UNKNOWN) {
                    multi_voice_assistant_manager_notify_va_connected(MULTI_VA_TYPE_AMA);
                }
            }
        }
        break;
#endif
        case AMA_DISCONNECT_IND:
        {
            AMA_DISCONNECT_IND_T *ind = (AMA_DISCONNECT_IND_T *)extra_data;
            if (ind) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_DISCONNECT_IND, status: 0x%x", 1, ind->status);
                if (memcmp(&ind->bdAddr, ama_activity_context->addr.addr, sizeof(BD_ADDR_T)) == 0) {
                    ama_audio_stop();
                    multi_voice_assistant_manager_notify_va_disconnected(MULTI_VA_TYPE_AMA);

                    /**
                     * @brief Finish activity
                     *
                     */
                    ui_shell_finish_activity(self, self);

                } else {
                    APPS_LOG_MSGID_I(APP_AMA_ACTI" AMA_DISCONNECT_IND, ind->addr: 0x%x:%x:%x:%x:%x:%x", 6,
                            ind->bdAddr.addr[0], ind->bdAddr.addr[1], ind->bdAddr.addr[2],
                            ind->bdAddr.addr[3], ind->bdAddr.addr[4], ind->bdAddr.addr[5]);
                    APPS_LOG_MSGID_I(APP_AMA_ACTI" AMA_DISCONNECT_IND, record_addr: 0x%x:%x:%x:%x:%x:%x", 6,
                            ama_activity_context->addr.addr[0], ama_activity_context->addr.addr[1], ama_activity_context->addr.addr[2],
                            ama_activity_context->addr.addr[3], ama_activity_context->addr.addr[4], ama_activity_context->addr.addr[5]);
                    APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_DISCONNECT_IND, The bdAddr is not match, ignore", 0);
                }
            }
        }
        break;
        case AMA_NOTIFY_DEVICE_CONFIG_IND:
        {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_NOTIFY_DEVICE_CONFIG_IND", 0);
            multi_voice_assistant_manager_set_inactive_done(MULTI_VA_TYPE_AMA);
            break;
        }
        case AMA_DISCONNECT_CFM:
        {
            AMA_DISCONNECT_CFM_T *cnf = (AMA_DISCONNECT_CFM_T *)extra_data;
            if (cnf) {
                if (memcmp(&cnf->bdAddr, ama_activity_context->addr.addr, sizeof(BD_ADDR_T)) == 0) {
                    APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_DISCONNECT_CFM", 0);
                    ama_audio_stop();
                    multi_voice_assistant_manager_notify_va_disconnected(MULTI_VA_TYPE_AMA);
                    /**
                     * @brief Finish activity
                     *
                     */
                    ui_shell_finish_activity(self, self);
                } else {
                    APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_DISCONNECT_CFM, cnf->addr: 0x%x:%x:%x:%x:%x:%x", 6,
                            cnf->bdAddr.addr[0], cnf->bdAddr.addr[1], cnf->bdAddr.addr[2],
                            cnf->bdAddr.addr[3], cnf->bdAddr.addr[4], cnf->bdAddr.addr[5]);
                    APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_DISCONNECT_CFM, record_addr: 0x%x:%x:%x:%x:%x:%x", 6,
                            ama_activity_context->addr.addr[0], ama_activity_context->addr.addr[1], ama_activity_context->addr.addr[2],
                            ama_activity_context->addr.addr[3], ama_activity_context->addr.addr[4], ama_activity_context->addr.addr[5]);
                    APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_DISCONNECT_CFM, The bdAddr is not match, ignore", 0);
                }
            }
        }
        break;
        /* SPEECH */
        case AMA_START_SPEECH_CFM:
        {
            AMA_START_SPEECH_CFM_T *cnf = (AMA_START_SPEECH_CFM_T *)extra_data;
            if (cnf == NULL) {
                break;
            }
            APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_START_SPEECH_CFM, err_code: 0x%x, trigger mode : %d", 2, cnf->errorCode, ama_activity_context->trigger_mode);
            if (cnf->errorCode == AMA_ERROR_CODE_SUCCESS) {
                if (ama_activity_context->trigger_mode == AMA_SPEECH_INITIATOR_TYPE_TAP
                    || ama_activity_context->trigger_mode == AMA_SPEECH_INITIATOR_TYPE_PRESS_AND_HOLD) {
                    ama_audio_start(WWE_MODE_NONE, 0, 0);
                } else if (ama_activity_context->trigger_mode == AMA_SPEECH_INITIATOR_TYPE_WAKEWORD) {
                    if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true) {
                        // ama_audio_start(WWE_MODE_NONE, 0);
                    }
                }
                ama_activity_context->recognition_state = AMA_ACTIVITY_RECOGNITION_STATE_RECORDING;
            }
        }
        break;
        case AMA_PROVIDE_SPEECH_IND:
        {
            AMA_PROVIDE_SPEECH_IND_T *ind = (AMA_PROVIDE_SPEECH_IND_T *)extra_data;
            if (ind != NULL) {
                AMA_PROVIDE_SPEECH_T speech_param;
                speech_param.audioFormat = AMA_AUDIO_FORMAT_OPUS_16KHZ_16KBPS_CBR_0_20MS;
                speech_param.audioProfile = AMA_AUDIO_PROFILE_NEAR_FIELD;
                speech_param.audioSource = AMA_AUDIO_SOURCE_STREAM;

                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_PROVIDE_SPEECH_IND, trigger mode : %d", 1, ama_activity_context->trigger_mode);

                ama_audio_restart(WWE_MODE_NONE, 0, 0);
                ama_activity_context->recognition_state = AMA_ACTIVITY_RECOGNITION_STATE_RECORDING;

                AMAStatus status = AMA_Target_ProvideSpeechResponse(&ind->bdAddr, &speech_param, AMA_ERROR_CODE_SUCCESS);
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_PROVIDE_SPEECH_IND, Response result : 0x%x", 1, status);
            }
        }
        break;
        case AMA_STOP_SPEECH_IND:
        {
            AMA_STOP_SPEECH_IND_T *ind = (AMA_STOP_SPEECH_IND_T *)extra_data;
            if (ind) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_STOP_SPEECH_IND , trigger mode : %d", 1, ama_activity_context->trigger_mode);
                if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true) {
                    ama_audio_restart(WWE_MODE_AMA, wwe_flash_address, wwe_flash_length);
                } else {
                    ama_audio_stop();
                }
                ama_activity_context->trigger_mode = AMA_SPEECH_INITIATOR_TYPE_NONE;
                ama_activity_context->recognition_state = AMA_ACTIVITY_RECOGNITION_STATE_IDLE;

                status = AMA_Target_StopSpeechResponse(&ind->bdAddr);
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_STOP_SPEECH_IND, Response result : 0x%x", 1, status);
            }
        }
        break;
        case AMA_STOP_SPEECH_CFM:
        {
            AMA_STOP_SPEECH_CFM_T *cnf = ((AMA_STOP_SPEECH_CFM_T *)extra_data);
            if(cnf) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_STOP_SPEECH_CFM, WWD Configured : %d", 1, AMA_TRIGGER_MODE_WWD_CONFIGURED);
                if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true) {
                    ama_audio_restart(WWE_MODE_AMA, wwe_flash_address, wwe_flash_length);
                } else {
                    ama_audio_stop();
                }
                ama_activity_context->trigger_mode = AMA_SPEECH_INITIATOR_TYPE_NONE;
                ama_activity_context->recognition_state = AMA_ACTIVITY_RECOGNITION_STATE_IDLE;
            }
        }
        break;
        case AMA_ENDPOINT_SPEECH_IND:
        {
            AMA_ENDPOINT_SPEECH_IND_T *ind = ((AMA_ENDPOINT_SPEECH_IND_T *)extra_data);
            if (ind == NULL) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_ENDPOINT_SPEECH_IND, there has some error, ind is NULL", 0);
                break;
            }
            if (memcmp(&ind->bdAddr, ama_activity_context->addr.addr, sizeof(BD_ADDR_T)) == 0) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_ENDPOINT_SPEECH_IND, WWD Configured : %d", 1, AMA_TRIGGER_MODE_WWD_CONFIGURED);
                if (AMA_TRIGGER_MODE_WWD_CONFIGURED == true) {
                    ama_audio_restart(WWE_MODE_AMA, wwe_flash_address, wwe_flash_length);
                } else {
                    ama_audio_stop();
                }

                ama_activity_context->trigger_mode = AMA_SPEECH_INITIATOR_TYPE_NONE;

                status = AMA_Target_EndpointSpeechResponse(&ind->bdAddr);
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_ENDPOINT_SPEECH_IND, Response result : 0x%x", 1, status);
            }
        }
        break;
        case AMA_ENDPOINT_SPEECH_CFM:
        {
            AMA_ENDPOINT_SPEECH_CFM_T * cnf = (AMA_ENDPOINT_SPEECH_CFM_T *)extra_data;
            if (cnf) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI",AMA_ENDPOINT_SPEECH_CFM: %x", 1, cnf->errorCode);
            }
        }
        break;
        case AMA_SPEECH_STATE_IND:
        {
            AMA_SPEECH_STATE_IND_T *ind = (AMA_SPEECH_STATE_IND_T *)extra_data;

            if (ind != NULL) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_SPEECH_STATE_IND, New Speech State : 0x%x", 1, ind->state);
                switch (ind->state) {
                    case AMA_SPEECH_STATE_IDLE:
                    ama_activity_context->recognition_state = AMA_ACTIVITY_RECOGNITION_STATE_IDLE;
                    break;
                    case AMA_SPEECH_STATE_LISTENING:
                    break;
                    case AMA_SPEECH_STATE_PROCESSING:
                    ama_activity_context->recognition_state = AMA_ACTIVITY_RECOGNITION_STATE_THINKING;
                    break;
                    case AMA_SPEECH_STATE_SPEAKING:
                    ama_activity_context->recognition_state = AMA_ACTIVITY_RECOGNITION_STATE_SPEAKING;
                    break;
                }
            }
        }
        break;
        /* STATE */
        case AMA_GET_STATE_CFM:
        {
            AMA_GET_STATE_CFM_T * cnf = (AMA_GET_STATE_CFM_T *)extra_data;
            if (cnf) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_GET_STATE_CFM, erro_code: 0x%x, feature : 0x%x, value: 0x%x, value_type : 0x%x",
                    4, cnf->errorCode, cnf->state.feature, cnf->state.value, cnf->state.valueType);
            }
        }
        break;
        case AMA_GET_STATE_IND:
        {
            app_ama_get_state_ind_handler((AMA_GET_STATE_IND_T *)extra_data);
        }
        break;
        case AMA_SET_STATE_IND:
        {
            app_ama_set_state_ind_handler((AMA_SET_STATE_IND_T *)extra_data);
        }
        break;
        case AMA_SYNCHRONIZE_STATE_CFM:
        {
            AMA_SYNCHRONIZE_STATE_CFM_T * cnf = ((AMA_SYNCHRONIZE_STATE_CFM_T *)extra_data);
            if (cnf) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_SYNCHRONIZE_STATE_CFM, error_code : 0x%x", 1, cnf->errorCode);
            }
           break;
        }
        case AMA_SYNCHRONIZE_STATE_IND:
        {
            AMA_SYNCHRONIZE_STATE_IND_T * ind = (AMA_SYNCHRONIZE_STATE_IND_T *)extra_data;
            AMA_Target_SynchronizeStateResponse(&ind->bdAddr);
        }
        break;
        /* MEDIA */
        case AMA_ISSUE_MEDIA_CONTROL_CFM:
        {
            AMA_ISSUE_MEDIA_CONTROL_CFM_T *cnf = (AMA_ISSUE_MEDIA_CONTROL_CFM_T *)extra_data;
            if (cnf) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_ISSUE_MEDIA_CONTROL_CFM, error_code : 0x%x", 1, cnf->errorCode);
            }
        }
        break;
        case AMA_ISSUE_MEDIA_CONTROL_IND:
        {
           app_ama_media_control_handler((AMA_ISSUE_MEDIA_CONTROL_IND_T *)extra_data);
        }
        break;
        /* Cellular Calling */
        case AMA_FORWARD_AT_COMMAND_IND:
        {
            //app_AmaForwardATCmdIndHandler((AMA_FORWARD_AT_COMMAND_IND_T *)extra_data);
        }
        break;
        case AMA_INCOMING_CALL_CFM: {
            //app_AmaIncomingCallCfmHandler((AMA_INCOMING_CALL_CFM_T *)extra_data);
            break;
        }
        /* MISC IND*/
        case AMA_SWITCH_TRANSPORT_IND:
        {
            AMA_SWITCH_TRANSPORT_IND_T *ind = (AMA_SWITCH_TRANSPORT_IND_T *)extra_data;
            if (ind) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_SWITCH_TRANSPORT_IND, old_addr %x, %x, %x,%x,%x,%x", 6,
                        ind->bdAddr.addr[0], ind->bdAddr.addr[1], ind->bdAddr.addr[2],
                        ind->bdAddr.addr[3], ind->bdAddr.addr[4], ind->bdAddr.addr[5]);

                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_SWITCH_TRANSPORT_IND, local_addr %x, %x, %x,%x,%x,%x", 6,
                        ama_activity_context->addr.addr[0], ama_activity_context->addr.addr[1], ama_activity_context->addr.addr[2],
                        ama_activity_context->addr.addr[3], ama_activity_context->addr.addr[4], ama_activity_context->addr.addr[5]);

                if (memcmp(&(ind->bdAddr), ama_activity_context->addr.addr, sizeof(BD_ADDR_T)) == 0) {
                    memcpy(ama_activity_context->addr.addr, &(ind->newBdAddr), sizeof(BD_ADDR_T));
                }
            }
        }
        break;

        case AMA_UPDATE_DEVICE_INFORMATION_IND:
        {
            //app_AmaUpdateDeviceInfoIndHandler((AMA_UPDATE_DEVICE_INFORMATION_IND_T *)extra_data);
        }
        break;
        case AMA_OVERRIDE_ASSISTANT_IND:
        {
            AMA_OVERRIDE_ASSISTANT_IND_T *ind = (AMA_OVERRIDE_ASSISTANT_IND_T*)extra_data;
            APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_OVERRIDE_ASSISTANT_IND, error code : %d", 1, ind->errorCode);
            if(ama_activity_context == NULL) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", ama_activity_context null", 0);
                break;
            };
            if (ama_activity_context->start_setup) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", is start_setup, wait setup complete", 0);
                break;
            }
            if (ind->errorCode == AMA_ERROR_CODE_SUCCESS) {
                multi_voice_assistant_manager_notify_va_connected(MULTI_VA_TYPE_AMA);
            }
        }
        break;
        /* MISC CFM*/
        case AMA_GET_CENTRAL_INFORMATION_CFM:
        {
            AMA_GET_CENTRAL_INFORMATION_CFM_T *cnf = (AMA_GET_CENTRAL_INFORMATION_CFM_T *)extra_data;
            if (cnf) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_GET_CENTRAL_INFORMATION_CFM, error_code : 0x%x, paltom : 0x%x", 2, cnf->errorCode, cnf->platform);
            }
        }
        break;
        case AMA_OVERRIDE_ASSISTANT_CFM:
        {
            AMA_OVERRIDE_ASSISTANT_CFM_T *cnf = ((AMA_OVERRIDE_ASSISTANT_CFM_T *)extra_data);
            if (cnf) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", AMA_OVERRIDE_ASSISTANT_CFM, error_code : 0x%x", 1, cnf->errorCode);
                multi_voice_assistant_manager_set_inactive_done(MULTI_VA_TYPE_AMA);
            }
        }
        break;
        case AMA_START_SETUP_IND:
        {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", start_setup", 0);
            if(ama_activity_context == NULL) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", ama_activity_context null", 0);
                break;
            }
            ama_activity_context->start_setup = true;
        }
        break;
        case AMA_COMPLETE_SETUP_IND:
        {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", complete_setup:error_code", 0);
            if(ama_activity_context == NULL) {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", ama_activity_context null", 0);
                break;
            }
            ama_activity_context->start_setup = false;
            multi_voice_assistant_manager_notify_va_connected(MULTI_VA_TYPE_AMA);
        }
        break;
        case AMA_FACTORY_RESET_CFM:
        {
            //app_AmaFactoryResetCfmHandler((AMA_FACTORY_RESET_CFM_T *)msg);
            break;
        }
        default:
            break;
    }
   return ret;
}

bool app_ama_activity_process_bt_cm_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;

    switch (event_id) {
        case BT_CM_EVENT_REMOTE_INFO_UPDATE:
        {
            bt_cm_remote_info_update_ind_t *remote_update = (bt_cm_remote_info_update_ind_t *)extra_data;
            if (remote_update == NULL) {
                APPS_LOG_MSGID_E(APP_AMA_ACTI", app_ama_activity_process_bt_cm_proc, remote_update null", 0);
                return false;
            } else if (ama_activity_context == NULL) {
                APPS_LOG_MSGID_E(APP_AMA_ACTI", app_ama_activity_process_bt_cm_proc, ama_activity_context null", 0);
                return false;
            }

            bool pre_hfp_conn = ((BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP) & remote_update->pre_connected_service) > 0);
            if (!pre_hfp_conn) {
                pre_hfp_conn = ((BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HSP) & remote_update->pre_connected_service) > 0);
            }
            bool pre_a2dp_conn = ((BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_A2DP_SINK) & remote_update->pre_connected_service) > 0);
            bool cur_hfp_conn = ((BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP)) & remote_update->connected_service) > 0;
            if (!cur_hfp_conn) {
                cur_hfp_conn = ((BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HSP) & remote_update->connected_service) > 0);
            }
            bool cur_a2dp_conn = ((BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_A2DP_SINK) & remote_update->connected_service) > 0);

            APPS_LOG_MSGID_I(APP_AMA_ACTI", bt_info_update acl=%d->%d srv=0x%08X->0x%08X hfp=%d->%d a2dp=%d->%d", 8,
                    remote_update->pre_acl_state, remote_update->acl_state,
                    remote_update->pre_connected_service, remote_update->connected_service,
                    pre_hfp_conn, cur_hfp_conn, pre_a2dp_conn, cur_a2dp_conn);

            if (!cur_hfp_conn && !cur_a2dp_conn && (pre_hfp_conn || pre_a2dp_conn)) {
                /**
                 * @brief If A2DP/HFP disconnected, disconnect AMA link
                 * For bug fix.
                 */
                APPS_LOG_MSGID_I(APP_AMA_ACTI", [BT_CM_EVENT_REMOTE_INFO_UPDATE] A2DP and HFP disconnected, Disconnect AMA link", 0);
                AMA_Target_DisconnectRequest(&(ama_activity_context->addr));
                return false;
            }

            if (!pre_a2dp_conn && cur_a2dp_conn) {
                notify_state_change(AMA_VALUE_TYPE_BOOLEAN, AMA_STATE_FEATURE_A2DP_CONNECTED, true);
            } else if (pre_a2dp_conn && !cur_a2dp_conn) {
                notify_state_change(AMA_VALUE_TYPE_BOOLEAN, AMA_STATE_FEATURE_A2DP_CONNECTED, false);
            }

            if (!pre_hfp_conn && cur_hfp_conn) {
                notify_state_change(AMA_VALUE_TYPE_BOOLEAN, AMA_STATE_FEATURE_HFP_CONNECTED, true);
            } else if (pre_hfp_conn && !cur_hfp_conn) {
                notify_state_change(AMA_VALUE_TYPE_BOOLEAN, AMA_STATE_FEATURE_HFP_CONNECTED, false);
            }
            break;
        }
        default:
            break;
    }
    return ret;
}

#ifdef MTK_AWS_MCE_ENABLE
bool app_ama_activity_process_aws_data_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    bt_aws_mce_report_info_t *aws_data_ind = (bt_aws_mce_report_info_t *)extra_data;
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();

    if (aws_data_ind->module_id == BT_AWS_MCE_REPORT_MODULE_APP_ACTION && role == BT_AWS_MCE_ROLE_AGENT) {

        uint32_t event_group;
        uint32_t action;

        void *stop_index_p = NULL;
        uint32_t stop_index = 0;
        uint32_t stop_index_len = 0;

        apps_aws_sync_event_decode_extra(aws_data_ind, &event_group, &action, &stop_index_p, &stop_index_len);

        if (event_group == EVENT_GROUP_UI_SHELL_KEY && action == KEY_AMA_START) {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_activity_process_aws_data_proc, Received start AMA_START from partner, stop index : 0x%x, len : %d", 2, stop_index_p, stop_index_len);
            if (stop_index_p != NULL && stop_index_len == sizeof(uint32_t)) {
                stop_index = (uint32_t)stop_index_p;

                if (stop_index == 0) {
                    APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_activity_process_aws_data_proc, Key trigger", 0);
                    app_ama_start_speech_with_tap_or_hld();
                } else {
                    APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_activity_process_aws_data_proc, WWD trigger stop_index : %d", 1, stop_index);
                    start_speech_with_wwd(false, stop_index);
                }
            } else {
                APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_activity_process_aws_data_proc, KEY_AMA_START key trigger on partner", 0);
                app_ama_start_speech_with_tap_or_hld();
            }
        } else if (event_group == EVENT_GROUP_UI_SHELL_KEY && action == KEY_AMA_START_NOTIFY) {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_activity_process_aws_data_proc, KEY_AMA_START_NOTIFY Execution", 0);
            apps_config_set_vp(VP_INDEX_PRESS, true, 100, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
        }
        ret = true;
    }
    return ret;
}
#endif

bool app_ama_multi_va_set_configuration(bool selected)
{
    if (selected == false) {
        if (ama_activity_context != NULL) {
            AMA_Target_SetDeviceConfiguration(&(ama_activity_context->addr), true, true);
            return true;
        } else {
            APPS_LOG_MSGID_I(APP_AMA_ACTI", app_ama_multi_va_set_configuration, ama_activity_context is NULL", 0);
            return false;
        }
    }
    return false;
}

bool app_ama_activity_proc(
            struct _ui_shell_activity *self,
            uint32_t event_group,
            uint32_t event_id,
            void *extra_data,
            size_t data_len)
{
    bool ret = false;
    // APPS_LOG_MSGID_I(APP_AMA_ACTI", event_group : %x, id : %x", 2, event_group, event_id);
    switch (event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM: {
            ret = _proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_KEY: {
            ret = _proc_key_event_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_SINK: {
            app_ama_activity_proc_bt_event(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_CONN_MANAGER: {
            ret = app_ama_activity_process_bt_cm_proc(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_AMA:
        {
            ret = app_ama_activity_proc_ama_event(self, event_id, extra_data, data_len);
            break;
        }
#ifdef MTK_AWS_MCE_ENABLE
        case EVENT_GROUP_UI_SHELL_AWS_DATA:
        {
            app_ama_activity_process_aws_data_proc(self, event_id, extra_data, data_len);
            break;
        }
#endif

#if defined(MTK_AWS_MCE_ENABLE) && defined(SUPPORT_ROLE_HANDOVER_SERVICE) && defined(BT_ROLE_HANDOVER_WITH_SPP_BLE)
        case EVENT_GROUP_UI_SHELL_APP_INTERACTION:
        {
            if (event_id == APPS_EVENTS_INTERACTION_RHO_END) {
                app_rho_result_t status = (app_rho_result_t) extra_data;
                if (status == APP_RHO_RESULT_SUCCESS) {
                    APPS_LOG_MSGID_I(APP_AMA_ACTI ", app_ama_activity_proc, APPS_EVENTS_INTERACTION_RHO_END, success, need send the address to new agent", 0);
                    AMA_CONNECT_CFM_T *cfm = (AMA_CONNECT_CFM_T *)pvPortMalloc(sizeof(AMA_CONNECT_CFM_T));
                    memset(cfm, 0, sizeof(AMA_CONNECT_CFM_T));
                    cfm->status = 0;
                    memcpy(cfm->bdAddr.addr, ama_activity_context->addr.addr, 6);
                    bt_status_t status = apps_aws_sync_event_send_extra(EVENT_GROUP_UI_SHELL_BT_AMA,
                                                                        AMA_CONNECT_CFM,
                                                                        (void *)cfm,
                                                                        sizeof(AMA_CONNECT_CFM_T));
                    if (status == BT_STATUS_SUCCESS) {
                        ama_audio_stop();
                        multi_voice_assistant_manager_notify_va_disconnected(MULTI_VA_TYPE_AMA);
                        ui_shell_finish_activity(self, self);
                    } else {
                        APPS_LOG_MSGID_I(APP_AMA_ACTI ", app_ama_activity_proc, APPS_EVENTS_INTERACTION_RHO_END, Send data to new agent failed, 0x%x", 1, status);
                    }
                    vPortFree(cfm);
                } else {
                    APPS_LOG_MSGID_I(APP_AMA_ACTI ", app_ama_activity_proc, APPS_EVENTS_INTERACTION_RHO_END, fail, DO NOTHING", 0);
                }
            }
        }
        break;
#endif
        default:
            break;
    }

    // APPS_LOG_MSGID_I(APP_AMA_ACTI", ret : %x", 1, ret);
    return ret;
}


#endif
