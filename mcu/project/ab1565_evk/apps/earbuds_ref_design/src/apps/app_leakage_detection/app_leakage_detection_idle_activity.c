/* Copyright Statement:
 *
 * (C) 2020  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifdef MTK_LEAKAGE_DETECTION_ENABLE

#include "app_leakage_detection_idle_activity.h"
#include "app_leakage_detection_utils.h"
#include "apps_events_event_group.h"
#include "apps_events_interaction_event.h"
#include "app_voice_prompt.h"
#include "bt_sink_srv.h"
#include "apps_config_event_list.h"
#include "apps_debug.h"
#include "ui_shell_manager.h"
#include "apps_config_vp_manager.h"
#include "airo_key_event.h"
#include "bt_sink_srv_ami.h"

static bool _proc_ui_shell_group(
        ui_shell_activity_t *self, 
        uint32_t event_id, 
        void *extra_data,
        size_t data_len);


static void stop_vp(app_leakage_detection_context_t* ctx){
    ui_shell_remove_event(EVENT_GROUP_UI_SHELL_APP_INTERACTION, APPS_EVENTS_INTERACTION_LEAKAGE_DETECTION_VP_TRIGGER);
    ui_shell_remove_event(EVENT_GROUP_UI_SHELL_APP_INTERACTION, APPS_EVENTS_INTERACTION_LEAKAGE_DETECTION_VP);
    //apps_config_stop_vp(APP_LEAKAGE_DETECTION_VP_INDEX, true, 0);
#ifndef APP_LEAKAGE_DETECTION_VP_REPEAT_INTERVAL
    apps_config_stop_voice(true, 0, true);
#else
    apps_config_stop_voice(true, 0, false);
#endif
    ctx->vp_cnt = 0;
}

static void vp_callback(uint32_t idx, vp_err_code err)
{
    if(idx == APP_LEAKAGE_DETECTION_VP_INDEX && err == VP_ERR_CODE_PREEMPTED){
        APPS_LOG_MSGID_I("app_leakage_detection_idle_activity_proc, vp stoped", 0);
        audio_anc_leakage_compensation_terminate();
    }
}

static bool _proc_ui_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data,size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    app_leakage_detection_context_t* ctx = NULL;

    switch (event_id) {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE: {
            APPS_LOG_MSGID_I("app_leakage_detection_idle_activity_proc, create", 0);
            self->local_context = (void*)pvPortMalloc(sizeof(app_leakage_detection_context_t));
            if(self->local_context){
                memset(self->local_context, 0, sizeof(app_leakage_detection_context_t));
                ctx = (app_leakage_detection_context_t*)self->local_context;
                ctx->in_idle = true;
            }
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY: {
            APPS_LOG_MSGID_I("app_leakage_detection_idle_activity_proc, destroy", 0);
            if(self->local_context){
                vPortFree(self->local_context);
            }
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESUME: {
            APPS_LOG_MSGID_I("app_leakage_detection_idle_activity_proc, resume", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE: {
            APPS_LOG_MSGID_I("app_leakage_detection_idle_activity_proc, pause", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH: {
            APPS_LOG_MSGID_I("app_leakage_detection_idle_activity_proc, refresh", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESULT: {
            APPS_LOG_MSGID_I("app_leakage_detection_idle_activity_proc, result", 0);
            break;
        }
        default:
            break;
    }
    return ret;
}


static bool _proc_apps_internal_events(
        struct _ui_shell_activity *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    app_leakage_detection_context_t* ctx = (app_leakage_detection_context_t*)self->local_context;

    switch (event_id) {
        case APPS_EVENTS_INTERACTION_LEAKAGE_DETECTION_VP_TRIGGER:
            ret = true;
            if(ctx && ctx->in_idle){
                bool* p_play_flag = (bool*)extra_data;
                APPS_LOG_MSGID_I("app_leakage_detection_idle_activity_proc, trigger vp play, flag:%d",
                                 1, *p_play_flag);
                if(*p_play_flag){
#ifndef APP_LEAKAGE_DETECTION_VP_REPEAT_INTERVAL
                    apps_config_set_voice(APP_LEAKAGE_DETECTION_VP_INDEX, true, 200, VOICE_PROMPT_PRIO_ULTRA, true, true, vp_callback);
                    ui_shell_remove_event(EVENT_GROUP_UI_SHELL_APP_INTERACTION, APPS_EVENTS_INTERACTION_LEAKAGE_DETECTION_VP);
                    ui_shell_send_event(false, EVENT_PRIORITY_MIDDLE, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                                        APPS_EVENTS_INTERACTION_LEAKAGE_DETECTION_VP,
                                        NULL, 0, NULL,
                                        APP_LEAKAGE_DETECTION_VP_PLAY_TIME);
#else
                    ctx->vp_cnt = APP_LEAKAGE_DETECTION_VP_REPEAT_TIMES - 1;
                    apps_config_set_voice(APP_LEAKAGE_DETECTION_VP_INDEX, true, 200, VOICE_PROMPT_PRIO_ULTRA, false, false, vp_callback);
                    ui_shell_remove_event(EVENT_GROUP_UI_SHELL_APP_INTERACTION, APPS_EVENTS_INTERACTION_LEAKAGE_DETECTION_VP);
                    ui_shell_send_event(false, EVENT_PRIORITY_MIDDLE, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                                        APPS_EVENTS_INTERACTION_LEAKAGE_DETECTION_VP,
                                        NULL, 0, NULL,
                                        APP_LEAKAGE_DETECTION_VP_REPEAT_INTERVAL);
#endif
                }else{
                    stop_vp(ctx);
                }
            }
            break;

        case APPS_EVENTS_INTERACTION_LEAKAGE_DETECTION_VP:
            ret = true;
#ifndef APP_LEAKAGE_DETECTION_VP_REPEAT_INTERVAL
            stop_vp(ctx);
#else
            if(ctx && ctx->in_idle && ctx->vp_cnt > 0){
                APPS_LOG_MSGID_I("app_leakage_detection_idle_activity_proc, vp play, vp_cnt:%d", 1, ctx->vp_cnt);
                ctx->vp_cnt--;
                apps_config_set_voice(APP_LEAKAGE_DETECTION_VP_INDEX, true, 200, VOICE_PROMPT_PRIO_ULTRA, false, false);

                if(ctx->vp_cnt > 0){
                    ui_shell_send_event(false, EVENT_PRIORITY_MIDDLE, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                                    APPS_EVENTS_INTERACTION_LEAKAGE_DETECTION_VP,
                                    NULL, 0, NULL,
                                    APP_LEAKAGE_DETECTION_VP_REPEAT_INTERVAL);
                }
            }
#endif
            break;
    }
    return ret;
}


static bool _proc_bt_state_event(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    app_leakage_detection_context_t* ctx = (app_leakage_detection_context_t*)self->local_context;

    if (event_id == BT_SINK_SRV_EVENT_STATE_CHANGE){
        bt_sink_srv_state_change_t *param = (bt_sink_srv_state_change_t*) extra_data;

        /* stop vp when in call or music status */
        if((param->previous < BT_SINK_SRV_STATE_INCOMING) && (param->current >= BT_SINK_SRV_STATE_INCOMING)){
            ctx->in_idle = false;
            stop_vp(ctx);
        }

        if(param->current < BT_SINK_SRV_STATE_INCOMING){
            ctx->in_idle = true;
        }
    }

    return false;
}

#if 0
static bool _proc_key_event_group(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    static bool flag = false;
    bool ret = false;
    uint8_t key_id;
    airo_key_event_t key_event;
    apps_config_key_action_t action = KEY_ACTION_INVALID;
    app_leakage_detection_context_t* ctx = (app_leakage_detection_context_t*)self->local_context;

    if (extra_data) {
        action = *(uint16_t *)extra_data;
    } else {
        action = apps_config_key_event_remapper_map_action(key_id, key_event);
    }

    if (action == KEY_TEST) {
        APPS_LOG_MSGID_I("app_leakage_detection_idle_activity_proc event_group key event", 0);
        if(!flag){
            ctx->in_idle = true;
            bool* p_play_flag = (bool *)pvPortMalloc(sizeof(bool));
            *p_play_flag = true;
            ui_shell_send_event(false, EVENT_PRIORITY_HIGH, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                            APPS_EVENTS_INTERACTION_LEAKAGE_DETECTION_VP_TRIGGER,
                            p_play_flag, sizeof(bool), NULL, 0);
        }else{
            ctx->in_idle = false;
            stop_vp(ctx);
        }
        flag = !flag;
    }
    return ret;

}
#endif

bool app_leakage_detection_idle_activity_proc(
            struct _ui_shell_activity *self,
            uint32_t event_group,
            uint32_t event_id,
            void *extra_data,
            size_t data_len)
{
    bool ret = false;
    APPS_LOG_MSGID_I("app_leakage_detection_idle_activity_proc event_group : %x, id: %x", 2, event_group, event_id);
    switch (event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM:
            ret = _proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;

        case EVENT_GROUP_UI_SHELL_APP_INTERACTION:
            ret = _proc_apps_internal_events(self, event_id, extra_data, data_len);
            break;

        case EVENT_GROUP_UI_SHELL_BT_SINK:
            ret = _proc_bt_state_event(self, event_id, extra_data, data_len);
            break;

#if 0
        /* ONLY FOR DEBUG */
        case EVENT_GROUP_UI_SHELL_KEY:
            ret = _proc_key_event_group(self, event_id, extra_data, data_len);
            break;
#endif

        default:
            break;
    }

    return ret;
}

#endif /*MTK_LEAKAGE_DETECTION_ENABLE*/
