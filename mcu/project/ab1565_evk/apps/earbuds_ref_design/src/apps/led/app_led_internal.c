/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
#include "hal_platform.h"
#include "hal.h"
#include "bsp_led.h"
#include "app_led_internal.h"
#include "led_control_style_cfg.h"

#define     MAX_LED_NUM     2
app_led_private_info_t  g_led_info[MAX_LED_NUM];

/* ******************************************************************************
 *                          Private API
 * *****************************************************************************
*/

static void     led_timer_callback(void *arg)
{
    app_led_private_info_t  *priv = (app_led_private_info_t *)arg;
    log_led_info("[app][led][iner] led%d t0 completed", 1, priv->led_id);
    if(priv != NULL){
        bsp_led_start(priv->led_id);
    }
}

static  void    led_ext_loop_callback(void *arg)
{
    app_led_private_info_t  *priv = (app_led_private_info_t *)arg;

    if(priv == NULL){
        return;
    }
    priv->counter += 1;
    if(priv->ext_lp_tms == priv->counter){
        log_led_info("[app][led][iner] ext loop completed,led%d stoped", 1, priv->led_id);
        bsp_led_stop(priv->led_id);
    }
}


/* ******************************************************************************
 *                          Public API
 * *****************************************************************************
*/

bool    app_led_internal_init(uint8_t  led)
{
    bsp_led_status_t        status;
    app_led_private_info_t  *priv;

    if(led > MAX_LED_NUM){
        return false;
    }
    priv = &g_led_info[led];
    priv->led_id = led;
    status = bsp_led_init(led, BSP_LED_BREATH_MODE);
    if(status != BSP_LED_STATUS_OK){
        log_led_error("[app][led][iner] init led failed:%d", 1, status);
        return false;
    }
    return true;
}


bool    app_led_internal_deinit(uint8_t led)
{
    app_led_private_info_t  *priv;

    priv = &g_led_info[led];
    bsp_led_deinit(led);
    if(priv->gpt_handle != 0){
        hal_gpt_sw_stop_timer_ms(priv->gpt_handle);
        hal_gpt_sw_free_timer(priv->gpt_handle);
        priv->gpt_handle = 0;
    }
    return true;
}


bool    app_led_internal_config(uint8_t led, one_led_style_t *config)
{
    bsp_led_status_t    status;
    bsp_led_config_t    bsp_cfg;
    app_led_private_info_t  *priv;

    if(config == NULL){
        log_led_error("[app][led][iner] para is null", 0);
        return NULL;
    }
    priv = &g_led_info[led];
    if(config->onoff == 0 && config->time_unit == 0){
        priv->on_off = 0;
        return true;
    }
    priv->on_off    = 1;
    priv->ext_lp_tms= config->repeat_ext;
    priv->dly_sta_tm= config->t0 * config->time_unit;

    bsp_cfg.bright     = config->brightness;
    bsp_cfg.call_back  = led_ext_loop_callback;
    bsp_cfg.user_data  = priv;
    bsp_cfg.cfg_timing.on_step_time = 0;
    bsp_cfg.cfg_timing.on_time = config->time_unit * config->t1;
    bsp_cfg.cfg_timing.off_step_time= 0;
    bsp_cfg.cfg_timing.off_time= config->time_unit * config->t2;
    bsp_cfg.cfg_timing.repeat_cycle_times = config->repeat_t1t2;
    bsp_cfg.cfg_timing.idle_time_for_loop = config->time_unit * config->t3;

    status = bsp_led_config(led, &bsp_cfg);
    if(status != BSP_LED_STATUS_OK){
        log_led_error("[app][led][iner] init led failed:%d", 1, status);
        return false;
    }
    return true;
}

extern  void hal_isink_dump_register();

bool    app_led_internal_start(uint8_t led, uint32_t sync_ms)
{
    app_led_private_info_t  *priv;
    hal_gpt_status_t    gpt_sta;
    bsp_led_status_t    bsp_sta;

    priv = &g_led_info[led];
    priv->counter = 0;
    priv->dly_sta_tm += sync_ms;
    if(priv->dly_sta_tm != 0) {
    	log_led_warn("[app][led][iner] delay start %d ms(sync %d)", 2, priv->dly_sta_tm, sync_ms);
        gpt_sta = hal_gpt_sw_get_timer(&(priv->gpt_handle));
        if(gpt_sta != HAL_GPT_STATUS_OK){
            log_led_error("[app][led][iner] start failed,get sw gpt:%d", 1, gpt_sta);
            return false;
        }
        gpt_sta = hal_gpt_sw_start_timer_ms(priv->gpt_handle, priv->dly_sta_tm, led_timer_callback, priv);
        if(gpt_sta != HAL_GPT_STATUS_OK){
            log_led_error("[app][led][iner] start failed,start sw gpt:%d", 1, gpt_sta);
            return false;
        }
    } else {
        bsp_sta = bsp_led_start(priv->led_id);
        if(bsp_sta != BSP_LED_STATUS_OK){
            log_led_error("[app][led][iner] bsp start failed%d", 1, bsp_sta);
            return false;
        }
    }
    hal_isink_dump_register();
    return true;
}

bool    app_led_internal_stop(uint8_t led)
{
    app_led_private_info_t  *priv;

    priv = &g_led_info[led];
    bsp_led_stop(led);
    if(priv->gpt_handle != 0){
        hal_gpt_sw_stop_timer_ms(priv->gpt_handle);
        hal_gpt_sw_free_timer(priv->gpt_handle);
        priv->gpt_handle = 0;
    }
    return true;
}






