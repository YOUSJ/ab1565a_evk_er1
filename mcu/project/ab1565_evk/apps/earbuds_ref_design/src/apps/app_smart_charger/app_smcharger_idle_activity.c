/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifdef MTK_SMART_CHARGER_ENABLE

#include "app_smcharger_idle_activity.h"

// start transient activity
#include "app_smcharger_startup_activity.h"

// battery event
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
#include "app_rho_idle_activity.h"
#endif
#include "apps_config_features_dynamic_setting.h"
#include "battery_management.h"
#include "battery_management_core.h"
#include "bt_power_on_config.h"
#include "apps_customer_config.h"
#if defined(MTK_AWS_MCE_ENABLE)
#include "bt_aws_mce_report.h"
#endif
#if defined(SUPPORT_ROLE_HANDOVER_SERVICE) && defined(MTK_FOTA_ENABLE)
#include "app_fota_idle_activity.h"
#endif

#define SMCHARGER_IDLE_ACTIVITY "[SMCharger]idle_activity"

static void app_smcharger_context_print(app_smcharger_context_t *ctx)
{
    APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", Print_SMCharger_Context role=0x%02X charger_exist=%d, charger_state=%d shutdown=%d "
            "battery_percent=%d [partner=%d] smcharger_state=%d [partner=%d] "
            "aws_state=0x%02X battery_state=%d", 10,
            bt_device_manager_aws_local_info_get_role(),
            ctx->charger_exist_state, ctx->charging_state, ctx->shutdown_state,
            ctx->battery_percent, ctx->partner_battery_percent,
            ctx->smcharger_state, ctx->partner_smcharger_state,
            ctx->aws_state, ctx->battery_state);
}

static app_battery_state_t app_smcharger_get_battery_state(app_smcharger_context_t *smcharger_ctx)
{
    app_battery_state_t new_state;
    app_smcharger_context_print(smcharger_ctx);
    if (smcharger_ctx->charger_exist_state) {
        if (smcharger_ctx->battery_percent == 100) {
            // Only use APP_BATTERY_STATE_CHARGING_FULL to show Charging_Full LED (LED_INDEX_IDLE)
            // no need charger_complete_event (EVENT_ID_SMCHARGER_CHARGER_COMPLETE_INTERRUPT)
            new_state = APP_BATTERY_STATE_CHARGING_FULL;
        } else if (smcharger_ctx->charging_state == CHARGER_STATE_THR) {
            new_state = APP_BATTERY_STATE_THR;
        } else {
            new_state = APP_BATTERY_STATE_CHARGING;
        }
    } else {
        if (smcharger_ctx->shutdown_state == APPS_EVENTS_BATTERY_SHUTDOWN_STATE_VOLTAGE_LOW) {
            new_state = APP_BATTERY_STATE_SHUTDOWN;
        } else if (smcharger_ctx->battery_percent < APPS_BATTERY_LOW_THRESHOLD) {
            new_state = APP_BATTERY_STATE_LOW_CAP;
        } else if (smcharger_ctx->battery_percent >= APPS_BATTERY_FULL_THRESHOLD) {
            new_state = APP_BATTERY_STATE_FULL;
        } else {
            new_state = APP_BATTERY_STATE_IDLE;
        }
    }

    if (smcharger_ctx->battery_state != new_state) {
        APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", battery_state %d->%d", 2, smcharger_ctx->battery_state, new_state);
        // Battery State Change -> Update LED_BG_PATTERN
        ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                APPS_EVENTS_INTERACTION_UPDATE_LED_BG_PATTERN, (void *)NULL, 0,
                NULL, 50);  // delay 50ms to wait smcharger activity switch
    } else {
        APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", same battery_state=%d", 1, new_state);
    }
    return new_state;
}

#ifdef APPS_AUTO_TRIGGER_RHO
static void smcharger_agent_check_and_do_rho(app_smcharger_context_t *smcharger_ctx)
{
    // Check partner battery is valid and higher than SWITCH_ROLE_PARTNER_BATTERY_LEVEL
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
    bool bt_edr_connected = (bt_cm_get_connected_devices(~BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS), NULL, 0) > 0);
    if (role == BT_AWS_MCE_ROLE_AGENT && smcharger_ctx->aws_state == BT_AWS_MCE_AGENT_STATE_ATTACHED
#if (defined(SUPPORT_ROLE_HANDOVER_SERVICE) && defined(MTK_FOTA_ENABLE)) || defined(MTK_VA_XIAOAI_ENABLE)
            && !smcharger_ctx->fota_doing
#endif
            && bt_edr_connected) {
        APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", AGENT & AWS ATTACHED & BT CONNECTED, check RHO: ", 0);
        app_smcharger_context_print(smcharger_ctx);
        if (smcharger_ctx->battery_state != APP_BATTERY_STATE_SHUTDOWN  // Shut down will ask home screen to do Power off and RHO
         && smcharger_ctx->battery_state < APP_BATTERY_STATE_CHARGING   // Agent not charging
         && smcharger_ctx->smcharger_state == STATE_SMCHARGER_OUT_OF_CASE   // Agent out_of_case
         && smcharger_ctx->partner_smcharger_state == STATE_SMCHARGER_OUT_OF_CASE // Partner out_of_case
         && smcharger_ctx->partner_battery_percent < PARTNER_BATTERY_CHARGING // Partner not charging
         && smcharger_ctx->battery_percent + APPS_DIFFERENCE_BATTERY_VALUE_FOR_RHO // diff 30
                 < smcharger_ctx->partner_battery_percent) {
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", trigger RHO due to low_battery", 0);
            // trigger RHO start
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_TRIGGER_RHO, NULL, 0, NULL, 0);
        } else {
            bool in_case = (smcharger_ctx->smcharger_state == STATE_SMCHARGER_LID_OPEN);
            bool partner_out_case = (smcharger_ctx->partner_smcharger_state == STATE_SMCHARGER_OUT_OF_CASE);
            if (in_case && partner_out_case) {
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", PREPARE_RHO due to in/out case, flag=%d",
                                        1, smcharger_ctx->agent_prepare_rho_flag);
                if (!smcharger_ctx->agent_prepare_rho_flag) {
                    smcharger_ctx->agent_prepare_rho_flag = TRUE;
                    // Start SMCHARGER PREPARE_RHO start after 1000ms
                    ui_shell_remove_event(EVENT_GROUP_UI_SHELL_CHARGER_CASE, EVENT_ID_SMCHARGER_PREPARE_RHO);
                    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_CHARGER_CASE,
                            EVENT_ID_SMCHARGER_PREPARE_RHO, NULL, 0, NULL, 1000);
                }
            } else {
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", cancel PREPARE_RHO due to in/out case", 0);
                smcharger_ctx->agent_prepare_rho_flag = FALSE;
                ui_shell_remove_event(EVENT_GROUP_UI_SHELL_CHARGER_CASE, EVENT_ID_SMCHARGER_PREPARE_RHO);
            }
        }
    } else {
        APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", check RHO fail: role=0x%02X aws=0x%02X bt_edr_connected=%d",
                3, role, smcharger_ctx->aws_state, bt_edr_connected);
    }
}
#endif

#if defined(MTK_AWS_MCE_ENABLE)
static void partner_notify_battery_level_to_agent(app_smcharger_context_t *smcharger_ctx)
{
    if (smcharger_ctx->aws_state != BT_AWS_MCE_AGENT_STATE_ATTACHED) {
        APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [Partner] AWS not connected, no need notify battery_level", 0);
    } else {
        uint8_t info_array[sizeof(bt_aws_mce_report_info_t)];
        uint8_t data_array[sizeof(smcharger_ctx->battery_percent)];
        bt_aws_mce_report_info_t *aws_data = (bt_aws_mce_report_info_t *)&info_array;
        aws_data->module_id = BT_AWS_MCE_REPORT_MODULE_BATTERY;
        aws_data->is_sync = FALSE;
        aws_data->sync_time = 0;
        aws_data->param_len = sizeof(smcharger_ctx->battery_percent);
        if (APP_BATTERY_STATE_CHARGING > smcharger_ctx->battery_state) {
            *(data_array) = smcharger_ctx->battery_percent;
        } else {
            *(data_array) = PARTNER_BATTERY_CHARGING | smcharger_ctx->battery_percent;
        }
        aws_data->param = (void *)data_array;
        APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [Partner] Send battery(%d) to agent", 1, *((uint8_t *)aws_data->param));
        bt_aws_mce_report_send_event(aws_data);
    }
}

static void partner_notify_smcharger_state_to_agent(app_smcharger_context_t *smcharger_ctx)
{
    if (smcharger_ctx->aws_state != BT_AWS_MCE_AGENT_STATE_ATTACHED) {
        APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [Partner] AWS not connected, no need notify smcharger_state", 0);
    }
    if (smcharger_ctx->aws_state == BT_AWS_MCE_AGENT_STATE_ATTACHED
            && bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER) {
        uint8_t info_array[sizeof(bt_aws_mce_report_info_t)] = {0};
        uint8_t data_array[sizeof(smcharger_ctx->smcharger_state)] = {0};
        bt_aws_mce_report_info_t *aws_data = (bt_aws_mce_report_info_t *)&info_array;
        aws_data->module_id = BT_AWS_MCE_REPORT_MODULE_SMCHARGER;
        aws_data->is_sync = FALSE;
        aws_data->sync_time = 0;
        aws_data->param_len = sizeof(smcharger_ctx->smcharger_state);
        memcpy((uint8_t *)data_array, &smcharger_ctx->smcharger_state, sizeof(smcharger_ctx->smcharger_state));
        aws_data->param = (void *)data_array;
        APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [Partner] Send smcharger_state(%d) to agent", 1, smcharger_ctx->smcharger_state);
        bt_aws_mce_report_send_event(aws_data);
    }
}
#endif

static bool smcharger_idle_proc_ui_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data,size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    switch (event_id) {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE: {
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", create", 0);
            app_smcharger_init();

            // Malloc & Init battery context
            self->local_context = pvPortMalloc(sizeof(app_smcharger_context_t));
            if (self->local_context != NULL) {
                app_smcharger_context_t *smcharger_ctx = (app_smcharger_context_t *)self->local_context;
                memset(self->local_context, 0, sizeof(app_smcharger_context_t));
                smcharger_ctx->battery_percent = battery_management_get_battery_property(BATTERY_PROPERTY_CAPACITY);
                smcharger_ctx->charging_state = battery_management_get_battery_property(BATTERY_PROPERTY_CHARGER_STATE);
                smcharger_ctx->charger_exist_state = battery_management_get_battery_property(BATTERY_PROPERTY_CHARGER_EXIST);
                smcharger_ctx->shutdown_state = calculate_shutdown_state(battery_management_get_battery_property(BATTERY_PROPERTY_VOLTAGE));
                smcharger_ctx->aws_state = BT_AWS_MCE_AGENT_STATE_INACTIVE;
                smcharger_ctx->partner_battery_percent = PARTNER_BATTERY_INVALID;
                smcharger_ctx->battery_state = APP_BATTERY_STATE_IDLE;
                smcharger_ctx->smcharger_state = STATE_SMCHARGER_NONE;
                smcharger_ctx->partner_smcharger_state = STATE_SMCHARGER_NONE;
                smcharger_ctx->case_battery_percent = 0;
                smcharger_ctx->agent_prepare_rho_flag = FALSE;

                smcharger_ctx->battery_state = app_smcharger_get_battery_state(smcharger_ctx);
                app_smcharger_set_context(smcharger_ctx);
            } else {
                APPS_LOG_MSGID_E(SMCHARGER_IDLE_ACTIVITY", malloc smcharger_ctx fail", 0);
                configASSERT(0);
            }

            // Start_Up (Start State Machine)
            ui_shell_start_activity(self, (ui_shell_proc_event_func_t)app_smcharger_startup_activity_proc,
                    ACTIVITY_PRIORITY_MIDDLE, (void *)NULL, 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY: {
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", destroy", 0);
            if (self->local_context) {
                vPortFree(self->local_context);
            }
            app_smcharger_set_context(NULL);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESUME: {
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", resume", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE: {
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", pause", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH: {
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", refresh", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESULT: {
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", result", 0);
            break;
        }
        default:
            break;
    }
    return ret;
}

static void smcharger_idle_update_battery_state(app_smcharger_context_t *smcharger_ctx, bool need_rho)
{
    app_battery_state_t new_state = app_smcharger_get_battery_state(smcharger_ctx);
    app_battery_state_t old_state = smcharger_ctx->battery_state;
    if (new_state == old_state) {
        // Do nothing when no change
    } else if (new_state == APP_BATTERY_STATE_SHUTDOWN
            && old_state != APP_BATTERY_STATE_SHUTDOWN) {
        APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", Start Power Off", 0);
    } else if (new_state == APP_BATTERY_STATE_LOW_CAP
            && old_state >= APP_BATTERY_STATE_IDLE) {
        APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", Start Low_Battery VP", 0);
        apps_config_set_vp(VP_INDEX_LOW_BATTERY, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
    }
    smcharger_ctx->battery_state = new_state;

    if (APP_BATTERY_STATE_SHUTDOWN == smcharger_ctx->battery_state) {
        app_smcharger_power_off(TRUE);
    } else {
#if defined(MTK_AWS_MCE_ENABLE)
        if (need_rho && bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER) {
            partner_notify_battery_level_to_agent(smcharger_ctx);
        } else if (need_rho && bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT) {
#ifdef APPS_AUTO_TRIGGER_RHO
            if (apps_config_features_is_auto_rho_enabled()) {
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [Agent] check_and_do_rho due to battery_event", 0);
                smcharger_agent_check_and_do_rho(smcharger_ctx);
            }
#endif
        }
#endif
    }
}

static bool smcharger_idle_battery_event_group(
        struct _ui_shell_activity *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    bool update_battery_state = true;
    bool need_rho = false;
    app_smcharger_context_t *smcharger_ctx = (app_smcharger_context_t *)self->local_context;
    switch (event_id) {
        case APPS_EVENTS_BATTERY_PERCENT_CHANGE:
            smcharger_ctx->battery_percent = (int32_t)extra_data;
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [DRV]Current battery_percent=%d", 1, (int32_t)extra_data);
        #ifdef MTK_VA_XIAOAI_ENABLE
            app_smartcharger_update_bat();
        #endif
            need_rho = true;
            break;
        case APPS_EVENTS_BATTERY_CHARGER_STATE_CHANGE:
            smcharger_ctx->charging_state = (int32_t)extra_data;
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [DRV]Current charging_state=%d", 1, smcharger_ctx->charging_state);
            break;
        case APPS_EVENTS_BATTERY_SHUTDOWN_STATE_CHANGE:
            smcharger_ctx->shutdown_state = (battery_event_shutdown_state_t)extra_data;
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [DRV]Current shutdown_state=%d", 1, smcharger_ctx->shutdown_state);
            break;
        default:
            // APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [DRV]Doesn't care battery event: %d", 1, event_id);
            update_battery_state = false;
            break;
    }

    if (update_battery_state) {
        smcharger_idle_update_battery_state(smcharger_ctx, need_rho);
    }
    return ret;
}

static bool smcharger_idle_bt_sink_event_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;

    return ret;
}

static bool smcharger_idle_bt_cm_event_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
#if defined(MTK_AWS_MCE_ENABLE)
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
    app_smcharger_context_t *smcharger_ctx = (app_smcharger_context_t *)self->local_context;
#endif
    switch (event_id) {
        case BT_CM_EVENT_POWER_STATE_UPDATE:
        {
            bt_cm_power_state_update_ind_t *power_updata = (bt_cm_power_state_update_ind_t *)extra_data;
            if (NULL == smcharger_ctx) {
                break;
            }

            if (BT_CM_POWER_STATE_OFF == power_updata->power_state) {
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", smcharger BT power off", 0);
            } else if (BT_CM_POWER_STATE_ON == power_updata->power_state) {
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", smcharger BT power on", 0);
            }
        }
            break;
        case BT_CM_EVENT_REMOTE_INFO_UPDATE:
        {
            bt_cm_remote_info_update_ind_t *remote_update = (bt_cm_remote_info_update_ind_t *)extra_data;
            if (NULL == remote_update || NULL == smcharger_ctx) {
                break;
            }
#if defined(MTK_AWS_MCE_ENABLE)
            if (BT_AWS_MCE_ROLE_AGENT == role || BT_AWS_MCE_ROLE_NONE == role) {
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", bt_info_update acl=%d->%d srv=0x%04X->0x%04X", 4,
                        remote_update->pre_acl_state, remote_update->acl_state,
                        remote_update->pre_connected_service, remote_update->connected_service);

                // Check Agent AWS connected
                if (!(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->pre_connected_service)
                        && (BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->connected_service)) {
                    smcharger_ctx->aws_state = BT_AWS_MCE_AGENT_STATE_ATTACHED;
                } else if ((BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->pre_connected_service)
                        && !(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->connected_service)) {
                    smcharger_ctx->partner_battery_percent = PARTNER_BATTERY_INVALID;
                    smcharger_ctx->partner_smcharger_state = STATE_SMCHARGER_NONE;
                    smcharger_ctx->aws_state = BT_AWS_MCE_AGENT_STATE_INACTIVE;
                }

                if (remote_update->connected_service > remote_update->pre_connected_service) {
                    APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [Agent] check_and_do_rho due to BT Service", 0);
                    smcharger_agent_check_and_do_rho(smcharger_ctx);
                }
            } else {
                // Check Partner AWS connected
                if (!(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->pre_connected_service)
                        && (BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->connected_service)) {
                    smcharger_ctx->aws_state = BT_AWS_MCE_AGENT_STATE_ATTACHED;
                    partner_notify_battery_level_to_agent(smcharger_ctx);
                    partner_notify_smcharger_state_to_agent(smcharger_ctx);
                } else if ((BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->pre_connected_service)
                        && !(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->connected_service)) {
                    smcharger_ctx->aws_state = BT_AWS_MCE_AGENT_STATE_INACTIVE;
                }
            }
#endif
        }
            break;
        default:
            break;
    }
    return ret;
}

#if defined(MTK_AWS_MCE_ENABLE)
static bool smcharger_idle_aws_event_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    bool need_check_rho = FALSE;
    bt_aws_mce_report_info_t *aws_data_ind = (bt_aws_mce_report_info_t *)extra_data;
    app_smcharger_context_t *smcharger_ctx = (app_smcharger_context_t *)self->local_context;
    if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT) {
        if (aws_data_ind->module_id == BT_AWS_MCE_REPORT_MODULE_BATTERY) {
            configASSERT(aws_data_ind->param_len == sizeof(smcharger_ctx->partner_battery_percent));
            memcpy(&smcharger_ctx->partner_battery_percent, aws_data_ind->param, sizeof(smcharger_ctx->partner_battery_percent));
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [Agent] Received partner battery=%d", 1, smcharger_ctx->partner_battery_percent);
            need_check_rho = TRUE;
        #ifdef MTK_VA_XIAOAI_ENABLE
            app_smartcharger_update_bat();
        #endif
        } else if (aws_data_ind->module_id == BT_AWS_MCE_REPORT_MODULE_SMCHARGER) {
            configASSERT(aws_data_ind->param_len == sizeof(smcharger_ctx->partner_smcharger_state));
            memcpy(&smcharger_ctx->partner_smcharger_state, aws_data_ind->param, sizeof(smcharger_ctx->partner_smcharger_state));
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [Agent] Received partner smcharger_state=%d", 1, smcharger_ctx->partner_smcharger_state);
            need_check_rho = TRUE;
        }
    }
#ifdef APPS_AUTO_TRIGGER_RHO
    if (need_check_rho && apps_config_features_is_auto_rho_enabled()) {
        smcharger_agent_check_and_do_rho(smcharger_ctx);
    }
#endif
    return ret;
}
#endif

static bool smcharger_idle_interaction_event_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;

    switch (event_id) {
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
        case APPS_EVENTS_INTERACTION_RHO_END: { // old Agent -> new Partner
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [new Partner] Received RHO end", 0);
            app_smcharger_context_t *smcharger_ctx = (app_smcharger_context_t *)self->local_context;
            smcharger_ctx->agent_prepare_rho_flag = FALSE;
            app_rho_result_t rho_ret = (app_rho_result_t)extra_data;
            if (APP_RHO_RESULT_SUCCESS == rho_ret) {
                // When agent switched to partner, set the partner_battery_percent to invalid value
                smcharger_ctx->partner_battery_percent = PARTNER_BATTERY_INVALID;
                smcharger_ctx->partner_smcharger_state = STATE_SMCHARGER_NONE;
                partner_notify_battery_level_to_agent(smcharger_ctx);
                partner_notify_smcharger_state_to_agent(smcharger_ctx);
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [new Partner] RHO success", 0);
            } else {
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [new Partner] RHO fail", 0);
            }
            break;
        }
        case APPS_EVENTS_INTERACTION_PARTNER_SWITCH_TO_AGENT: { // old Partner -> new Agent
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [new Agent] Received RHO end", 0);
            app_smcharger_context_t *smcharger_ctx = (app_smcharger_context_t *)self->local_context;
            smcharger_ctx->agent_prepare_rho_flag = FALSE;
            app_rho_result_t rho_ret = (app_rho_result_t)extra_data;
            if (APP_RHO_RESULT_SUCCESS == rho_ret) {
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [new Agent] RHO success", 0);
            } else {
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [new Agent] RHO fail", 0);
            }
            break;
        }
#endif
        default:
            // APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", Not supported event id = %d", 1, event_id);
            break;
    }
    return ret;
}

static bool smcharger_idle_fota_event_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
#if defined(SUPPORT_ROLE_HANDOVER_SERVICE) && defined(MTK_FOTA_ENABLE)
    app_smcharger_context_t *smcharger_ctx = (app_smcharger_context_t *)self->local_context;

    if (smcharger_ctx) {
        switch (event_id) {
            case RACE_EVENT_TYPE_FOTA_START:
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY"FOTA start", 0);
                smcharger_ctx->fota_doing = true;
                break;
            case RACE_EVENT_TYPE_FOTA_CANCEL:
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY"FOTA cancel", 0);
                smcharger_ctx->fota_doing = false;
                break;
        default:
            break;
        }
    }
#endif
    return ret;
}

static bool smcharger_idle_proc_key_event_group(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    return ret;
}

bool app_smcharger_idle_activity_proc(
            struct _ui_shell_activity *self,
            uint32_t event_group,
            uint32_t event_id,
            void *extra_data,
            size_t data_len)
{
    bool ret = false;
    switch (event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM:
            ret = smcharger_idle_proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_KEY:
            ret = smcharger_idle_proc_key_event_group(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_BATTERY:
            ret = smcharger_idle_battery_event_group(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_BT_SINK:
            ret = smcharger_idle_bt_sink_event_group(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_APP_INTERACTION:
            ret = smcharger_idle_interaction_event_group(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_FOTA:
            ret = smcharger_idle_fota_event_group(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_BT_CONN_MANAGER:
            ret = smcharger_idle_bt_cm_event_group(self, event_id, extra_data, data_len);
            break;
    #if defined(MTK_AWS_MCE_ENABLE)
        case EVENT_GROUP_UI_SHELL_AWS_DATA:
            ret = smcharger_idle_aws_event_group(self, event_id, extra_data, data_len);
            break;
    #endif
        case EVENT_GROUP_UI_SHELL_CHARGER_CASE: {
            APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", CHARGER_CASE group, event_id=%d", 1, event_id);
            app_smcharger_context_t *smcharger_ctx = (app_smcharger_context_t *)self->local_context;
            ret = true;
            if (event_id == EVENT_ID_SMCHARGER_SYNC_STATE) {
                smcharger_agent_check_and_do_rho(smcharger_ctx);
    #if defined(MTK_AWS_MCE_ENABLE)
                if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER){
                    partner_notify_smcharger_state_to_agent(smcharger_ctx);
                }
    #endif
            } else if (event_id == EVENT_ID_SMCHARGER_PREPARE_RHO) {
                ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                        APPS_EVENTS_INTERACTION_TRIGGER_RHO, NULL, 0, NULL, 0);
            } else if (event_id == EVENT_ID_SMCHARGER_CHARGER_OUT_PATTERN) {
                smcharger_ctx->charger_exist_state = 0;
                smcharger_ctx->battery_percent = battery_management_get_battery_property(BATTERY_PROPERTY_CAPACITY);
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", CHARGER_OUT = Charger_exist(0) battery_percent=%d", 1, smcharger_ctx->battery_percent);
                smcharger_idle_update_battery_state(smcharger_ctx, true);
            } else if (event_id == EVENT_ID_SMCHARGER_CHARGER_IN_INTERRUPT) {
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", CHARGER_IN = Charger_exist(1)", 0);
                smcharger_ctx->charger_exist_state = 1;
                uint8_t battery_percent = battery_management_get_battery_property(BATTERY_PROPERTY_CAPACITY);
                smcharger_ctx->shutdown_state = calculate_shutdown_state(battery_management_get_battery_property(BATTERY_PROPERTY_VOLTAGE));
                APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", [DRV]Current charger_exist=1 battery_percent=%d (%d) shutdown_state=%d",
                                3, smcharger_ctx->battery_percent, battery_percent, smcharger_ctx->shutdown_state);
                smcharger_idle_update_battery_state(smcharger_ctx, true);
            } else if (event_id == EVENT_ID_SMCHARGER_NOTIFY_PUBLIC_EVENT) {
                // Other APP should only use public event
                ret = false;
            } else {
                // APPS_LOG_MSGID_I(SMCHARGER_IDLE_ACTIVITY", unexpected smcharger event", 0);
            }
            break;
        }
        default:
            break;
    }
    return ret;
}

#endif
