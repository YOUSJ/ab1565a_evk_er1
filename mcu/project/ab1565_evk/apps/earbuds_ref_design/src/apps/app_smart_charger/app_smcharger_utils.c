/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifdef MTK_SMART_CHARGER_ENABLE

#define MTK_SMART_CHARGER_TEST

#include "app_smcharger_utils.h"
#include "apps_aws_sync_event.h"
#include "apps_config_event_list.h"
#include "hal_pmu.h"
#include "prompt_control.h"
#include "smartcharger.h"
#include "smartcase.h"
#include "bt_power_on_config.h"
#ifdef APPS_SLEEP_AFTER_NO_CONNECTION
#include "app_power_saving_utils.h"
#endif

#if MTK_AWS_MCE_ENABLE
#include "bt_sink_srv_ami.h"
#endif

#if 0//def MTK_ANC_ENABLE
#include "anc_control.h"
#endif

#ifdef MTK_VA_XIAOAI_ENABLE
#include "xiaoai.h"
#endif

#if !defined(MTK_BATTERY_MANAGEMENT_ENABLE)
#error "For SmartCharger feature, please enable MTK_BATTERY_MANAGEMENT_ENABLE"
#endif

#ifdef MTK_SMART_CHARGER_TEST
#include "atci.h"

static atci_status_t smcharger_atci_handler(atci_parse_cmd_param_t *parse_cmd);
static atci_cmd_hdlr_item_t smcharger_atci_cmd[] = {
    {
        .command_head = "AT+SMC",
        .command_hdlr = smcharger_atci_handler,
        .hash_value1 = 0,
        .hash_value2 = 0,
    },
};
#endif

#define SMCHARGER_UTILS "[SMCharger]Util"

static app_smcharger_context_t* g_smcharger_context = NULL;

static bool app_smcharger_ui_shell_event(int event_id, void *data, size_t data_len)
{
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", send smcharger event_id=%d", 1, event_id);
    ui_shell_status_t ret = ui_shell_send_event(FALSE, EVENT_PRIORITY_HIGH, EVENT_GROUP_UI_SHELL_CHARGER_CASE,
            event_id, data, data_len, NULL, 0);
    if (ret != UI_SHELL_STATUS_OK) {
        APPS_LOG_MSGID_E(SMCHARGER_UTILS", send smcharger event fail", 0);
    }
    return (ret == UI_SHELL_STATUS_OK);
}

static void app_smcharger_ui_shell_public_event(app_smcharger_action_t action, uint8_t data)
{
    app_smcharger_public_event_para_t *event_para = (app_smcharger_public_event_para_t *)
                                                    pvPortMalloc(sizeof(app_smcharger_public_event_para_t));
    event_para->action = action;
    event_para->data = data;
    bool ret = app_smcharger_ui_shell_event(EVENT_ID_SMCHARGER_NOTIFY_PUBLIC_EVENT,
                                (void *)event_para, sizeof(app_smcharger_public_event_para_t));
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", shell_public_event action=%d data=%d ret=%d", 3,
                     action, data, ret);
}

static const char* app_smcharger_state_string(int state)
{
    const char *state_str = "NULL";
    switch (state) {
        case STATE_SMCHARGER_STARTUP:
            state_str = "STARTUP";
            break;
        case STATE_SMCHARGER_LID_CLOSE:
            state_str = "LID_CLOSE";
            break;
        case STATE_SMCHARGER_LID_OPEN:
            state_str = "LID_OPEN";
            break;
        case STATE_SMCHARGER_OUT_OF_CASE:
            state_str = "OUT_OF_CASE";
            break;
        case STATE_SMCHARGER_OFF:
            state_str = "OFF";
            break;
    }
    return state_str;
}

#if 0//def MTK_ANC_ENABLE
static void app_smcharger_anc_callback(anc_control_event_t event_id,
                                       anc_control_result_t result)
{
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", anc_callback, event_id=%d result=%d", 2, event_id, result);
}
#endif

#define SMCHARGER_DATA_MASK_B01        0x03     // 0b0000 0011
#define SMCHARGER_DATA_MASK_B234       0x1C     // 0b0001 1100
#define SMCHARGER_DATA_MASK_B567       0xE0     // 0b1110 0000

#ifdef MTK_VA_XIAOAI_ENABLE
void app_smartcharger_update_bat()
{
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
    if (role == BT_AWS_MCE_ROLE_AGENT && g_smcharger_context != NULL) {
        unsigned char partner_battery_percent = g_smcharger_context->partner_battery_percent;
        if (g_smcharger_context->aws_state != BT_AWS_MCE_AGENT_STATE_ATTACHED) {
            partner_battery_percent = 0;
        }
        unsigned char left_bat = 0;
        unsigned char right_bat = 0;
    #if MTK_AWS_MCE_ENABLE
        audio_channel_t channel = ami_get_audio_channel();
        if (AUDIO_CHANNEL_L == channel) {
            left_bat = g_smcharger_context->battery_percent;
            right_bat = partner_battery_percent;
        } else if (AUDIO_CHANNEL_R == channel) {
            right_bat = g_smcharger_context->battery_percent;
            left_bat = partner_battery_percent;
        } else {
            APPS_LOG_MSGID_I(SMCHARGER_UTILS", [XIAOAI_VA] invalid L/R Channel", 0);
        }
    #endif
        xiaoai_set_battery(g_smcharger_context->battery_percent,
                        partner_battery_percent,
                        g_smcharger_context->case_battery_percent,
                        left_bat,
                        right_bat);
    }
}

void app_smartcharger_set_ota_state(bool ota_ongoing)
{
    if (g_smcharger_context != NULL) {
        g_smcharger_context->fota_doing = ota_ongoing;
    }
}
#endif

static void app_smcharger_handle_battery_level_report(uint32_t pattern_data)
{
    uint8_t mask = SMCHARGER_DATA_MASK_B01 + SMCHARGER_DATA_MASK_B234;
    if ((pattern_data & ~mask) != 0) {
        APPS_LOG_MSGID_E(SMCHARGER_UTILS", [DRV]callback, error battery_level_report data", 0);
        return;
    }
    uint8_t case_battery_percent = pattern_data;
    if (case_battery_percent == 31) {    // 0b11111
        case_battery_percent = 100;
    } else {
        case_battery_percent = (uint8_t)(case_battery_percent * 3.22);
    }
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", [DRV]callback, case_battery_percent %d->%d",
                        2,
                        (g_smcharger_context != NULL ? g_smcharger_context->case_battery_percent : 0),
                        case_battery_percent);
    if (g_smcharger_context != NULL
            && g_smcharger_context->case_battery_percent != case_battery_percent) {
        g_smcharger_context->case_battery_percent = case_battery_percent;
        app_smcharger_ui_shell_event(EVENT_ID_SMCHARGER_BATTERY_LEVEL_REPORT, (void *)(uint32_t)case_battery_percent, 0);
    #ifdef MTK_VA_XIAOAI_ENABLE
        app_smartcharger_update_bat();
    #endif
    }
}


static void app_smcharger_driver_callback(uint8_t drv_event, uint32_t pattern_data, uint16_t data_len)
{
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", [DRV]callback drv_event=%d pattern_data=%d data_len=%d",
                    3, drv_event, pattern_data, data_len);
    bool need_send_event = FALSE;
    uint8_t user_data = 0;
    uint8_t mask = 0;
    int event_id = EVENT_ID_SMCHARGER_NONE;
    switch (drv_event) {
        case DRV_CHARGER_EVENT_LID_OPEN_PATTERN: {
            event_id = EVENT_ID_SMCHARGER_LID_OPEN_PATTERN;
            if (g_smcharger_context != NULL && g_smcharger_context->smcharger_state == STATE_SMCHARGER_LID_OPEN) {
                APPS_LOG_MSGID_I(SMCHARGER_UTILS", [DRV]callback, ignore LID_OPEN Pattern in LID_OPEN State", 0);
                // For close_lid then open quickly
                app_smcharger_ui_shell_public_event(SMCHARGER_OPEN_LID_ACTION, 0);
            } else {
                need_send_event = TRUE;
            }
            if (data_len > 0) {
                app_smcharger_handle_battery_level_report(pattern_data);
            }
            break;
        }
        case DRV_CHARGER_EVENT_LID_CLOSE_PATTERN: {
            event_id = EVENT_ID_SMCHARGER_LID_CLOSE_PATTERN;
            if (g_smcharger_context != NULL && g_smcharger_context->smcharger_state == STATE_SMCHARGER_LID_CLOSE) {
                APPS_LOG_MSGID_I(SMCHARGER_UTILS", [DRV]callback, ignore LID_CLOSE Pattern in LID_CLOSE State", 0);
            } else {
                need_send_event = TRUE;
            }
            break;
        }
        case DRV_CHARGER_EVENT_CHARGER_OFF_PATTERN: {
            event_id = EVENT_ID_SMCHARGER_CHARGER_OFF_PATTERN;
            if (g_smcharger_context != NULL && g_smcharger_context->smcharger_state == STATE_SMCHARGER_OFF) {
                APPS_LOG_MSGID_I(SMCHARGER_UTILS", [DRV]callback, ignore OFF Pattern in OFF State", 0);
            } else {
                need_send_event = TRUE;
            }
            if (data_len > 0) {
                app_smcharger_handle_battery_level_report(pattern_data);
            }
            break;
        }
        case DRV_CHARGER_EVENT_CHARGER_OUT_PATTERN: {
            event_id = EVENT_ID_SMCHARGER_CHARGER_OUT_PATTERN;
            need_send_event = TRUE;
            ui_shell_remove_event(EVENT_GROUP_UI_SHELL_CHARGER_CASE, event_id);
            break;
        }
        case DRV_CHARGER_EVENT_CHARGER_KEY_PATTERN: {
            event_id = EVENT_ID_SMCHARGER_CHARGER_KEY_PATTERN;
            if (data_len == 3 && pattern_data <= 3) {
                user_data = pattern_data;
                need_send_event = TRUE;
            } else {
                APPS_LOG_MSGID_E(SMCHARGER_UTILS", [DRV]callback, error CHARGER_KEY data", 0);
            }
            break;
        }
        case DRV_CHARGER_EVENT_USER1_PATTERN: {
            event_id = EVENT_ID_SMCHARGER_USER_DATA1_PATTERN;
            mask = SMCHARGER_DATA_MASK_B01;
            if ((pattern_data & ~mask) == 0) {
                user_data = pattern_data;
                need_send_event = TRUE;
            } else {
                APPS_LOG_MSGID_E(SMCHARGER_UTILS", [DRV]callback, error USER_DATA1 data", 0);
            }
            break;
        }
        case DRV_CHARGER_EVENT_USER2_PATTERN: {
            event_id = EVENT_ID_SMCHARGER_USER_DATA2_PATTERN;
            mask = SMCHARGER_DATA_MASK_B234 + SMCHARGER_DATA_MASK_B567;
            if ((pattern_data & ~mask) == 0) {
                user_data = pattern_data;
                need_send_event = TRUE;
            } else {
                APPS_LOG_MSGID_E(SMCHARGER_UTILS", [DRV]callback, error USER_DATA2 data", 0);
            }
            break;
        }
        case DRV_CHARGER_EVENT_USER3_PATTERN: {
            event_id = EVENT_ID_SMCHARGER_USER_DATA3_PATTERN;
            mask = SMCHARGER_DATA_MASK_B01 + SMCHARGER_DATA_MASK_B234 + SMCHARGER_DATA_MASK_B567;
            if ((pattern_data & ~mask) == 0) {
                user_data = pattern_data;
                need_send_event = TRUE;
            } else {
                APPS_LOG_MSGID_E(SMCHARGER_UTILS", [DRV]callback, error USER_DATA3 data", 0);
            }
            break;
        }
        // Charger_in from battery_manager charger_exist=1 callback
        case DRV_CHARGER_EVENT_CHARGER_IN_PATTERN: {
            event_id = EVENT_ID_SMCHARGER_CHARGER_IN_INTERRUPT;
            need_send_event = TRUE;
            ui_shell_remove_event(EVENT_GROUP_UI_SHELL_CHARGER_CASE, event_id);
            break;
        }
        default: {
            APPS_LOG_MSGID_E(SMCHARGER_UTILS", [DRV]callback, error event", 0);
            break;
        }
    }
    if (need_send_event) {
        app_smcharger_ui_shell_event(event_id, (void *)(uint32_t)user_data, 0);
    }
}


static app_smcharger_action_status_t app_smcharger_handle_system_boot()
{
    uint8_t reason = pmu_get_power_on_reason();
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", handle_system_boot reason=0x%02X", 1, reason);
    if (reason & 0x04) {
        // STS_CHRIN, bit[2] == 1
        app_smcharger_ui_shell_event(EVENT_ID_SMCHARGER_CHARGER_IN_BOOT, NULL, 0);
    } else {
        // STS_PWRKEY, bit[0] == 1
        // STS_RBOOT STS_SPRA STS_RTCA as power_key
        app_smcharger_ui_shell_event(EVENT_ID_SMCHARGER_POWER_KEY_BOOT, NULL, 0);
        // Power_ON VP/LED
        apps_config_set_vp(VP_INDEX_POWER_ON, FALSE, 0, VOICE_PROMPT_PRIO_HIGH, FALSE, NULL);
        apps_config_set_foreground_led_pattern(LED_INDEX_POWER_ON, 30, FALSE);
    }
    return APP_SMCHARGER_OK;
}

static void app_smcharger_enable_discover()
{
    if (g_smcharger_context != NULL
            && g_smcharger_context->smcharger_state == STATE_SMCHARGER_LID_OPEN) {
        bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
        APPS_LOG_MSGID_I(SMCHARGER_UTILS", [Agent] enable_discover role=0x%02X bt_sink_state=0x%04X aws_state=0x%02X",
                        3, role, bt_sink_srv_get_state(), g_smcharger_context->aws_state);
        if (role == BT_AWS_MCE_ROLE_AGENT) {
            APPS_LOG_MSGID_I(SMCHARGER_UTILS", [Agent] enable_discover start", 0);
            bt_cm_discoverable(true);
        }
#ifdef MTK_AWS_MCE_ENABLE
        if (role == BT_AWS_MCE_ROLE_PARTNER) {
            if (g_smcharger_context->aws_state == BT_AWS_MCE_AGENT_STATE_ATTACHED) {
                if (BT_STATUS_SUCCESS == apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_KEY, KEY_DISCOVERABLE)) {
                    APPS_LOG_MSGID_I(SMCHARGER_UTILS"Partner send KEY_DISCOVERABLE to agent pass", 0);
                } else {
                    APPS_LOG_MSGID_E(SMCHARGER_UTILS"Partner send KEY_DISCOVERABLE to agent failed", 0);
                }
            } else {
                APPS_LOG_MSGID_I("[Partner] enable_discover start", 0);
                bt_cm_discoverable(true);
            }
        }
#endif
    } else {
        APPS_LOG_MSGID_I(SMCHARGER_UTILS", ignore enable_discover", 0);
    }
}

static app_smcharger_action_status_t app_smcharger_switch_bt(bool on)
{
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", switch_bt on=%d", 1, on);

    uint32_t delay_ms = 0;
#ifdef MTK_VA_XIAOAI_ENABLE
    if (on) {
        ui_shell_remove_event(EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                APPS_EVENTS_INTERACTION_REQUEST_ON_OFF_BT);
    } else {
        delay_ms = 2 * 1000;    // for sync LID_CLOSE state to agent & cancel pop-up window
    }
#endif

    ui_shell_status_t ret = UI_SHELL_STATUS_OK;
    if (BT_POWER_ON_NORMAL == bt_power_on_get_config_type()
            || BT_POWER_ON_DUT == bt_power_on_get_config_type()) {
        ret = ui_shell_send_event(FALSE, EVENT_PRIORITY_HIGNEST,
                                  EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                                  APPS_EVENTS_INTERACTION_REQUEST_ON_OFF_BT,
                                  (void *)on, 0, NULL, delay_ms);
    }
    return (ret == UI_SHELL_STATUS_OK ? APP_SMCHARGER_OK : APP_SMCHARGER_FAILURE);
}

static app_smcharger_action_status_t app_smcharger_mute_audio(bool is_mute)
{
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", mute_audio mute=%d", 1, is_mute);
    bt_status_t status = bt_sink_srv_music_set_mute(is_mute);
    prompt_control_set_mute(is_mute);
    return (status == BT_STATUS_SUCCESS ? APP_SMCHARGER_OK : APP_SMCHARGER_FAILURE);
}

static void app_smcharger_notify_state_to_app(int old_state, int state)
{
    uint32_t action = 0xFF;
    if (old_state == STATE_SMCHARGER_OUT_OF_CASE && state == STATE_SMCHARGER_LID_OPEN) {
        action = SMCHARGER_CHARGER_IN_ACTION;
    } else if (old_state == STATE_SMCHARGER_LID_CLOSE && state == STATE_SMCHARGER_OFF) {
        if (g_smcharger_context != NULL && g_smcharger_context->battery_percent == 100) {
            action = SMCHARGER_CHARGER_COMPLETE_ACTION;
        }
    } else if (state == STATE_SMCHARGER_LID_OPEN) {
        // lid_close->lid_open
        // startup->lid_open (power_off earbuds charger in, earbuds reboot in the lid_open case, open lid after earbuds power_off & lid_close)
        action = SMCHARGER_OPEN_LID_ACTION;
    } else if (old_state == STATE_SMCHARGER_LID_OPEN && state == STATE_SMCHARGER_LID_CLOSE) {
        action = SMCHARGER_CLOSE_LID_COMPLETE_ACTION;
    } else if (state == STATE_SMCHARGER_OUT_OF_CASE) {
        action = SMCHARGER_CHARGER_OUT_ACTION;
    }
    if (action != 0xFF) {
        // Send public event
        app_smcharger_ui_shell_public_event(action, 0);
    }
}

app_smcharger_action_status_t app_smcharger_power_off(bool normal_off)
{
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", power_off %d", 1, normal_off);
    if (normal_off) {
        apps_config_set_vp(VP_INDEX_POWER_OFF, FALSE, 0, VOICE_PROMPT_PRIO_EXTREME, TRUE, NULL);
        apps_config_set_foreground_led_pattern(LED_INDEX_POWER_OFF, 30, FALSE);
    }
    ui_shell_status_t ret = ui_shell_send_event(FALSE,
                                    EVENT_PRIORITY_HIGNEST,
                                    EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                                    (normal_off ? APPS_EVENTS_INTERACTION_REQUEST_POWER_OFF
                                            : APPS_EVENTS_INTERACTION_REQUEST_IMMEDIATELY_POWER_OFF),
                                    NULL, 0, NULL, 0);
    return (ret == UI_SHELL_STATUS_OK ? APP_SMCHARGER_OK : APP_SMCHARGER_FAILURE);
}

#ifdef APPS_SLEEP_AFTER_NO_CONNECTION
// need_power_saving when SmartCharger enable, two cases:
// 1. single(AWS no connected) + out_of_case
// 2. double(AWS connected) + agent + both out_of_case
static app_power_saving_target_mode_t app_smcharger_get_power_saving_target_mode(void)
{
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
    bool need_power_saving = FALSE;
    if (g_smcharger_context != NULL) {
        if (g_smcharger_context->aws_state != BT_AWS_MCE_AGENT_STATE_ATTACHED
                && g_smcharger_context->smcharger_state == STATE_SMCHARGER_OUT_OF_CASE) {
            need_power_saving = TRUE;
        } else if (g_smcharger_context->aws_state == BT_AWS_MCE_AGENT_STATE_ATTACHED
                && role == BT_AWS_MCE_ROLE_AGENT
                && g_smcharger_context->smcharger_state == STATE_SMCHARGER_OUT_OF_CASE
                && g_smcharger_context->partner_smcharger_state == STATE_SMCHARGER_OUT_OF_CASE) {
            need_power_saving = TRUE;
        }
        APPS_LOG_MSGID_I(SMCHARGER_UTILS", [0x%02X] aws=0x%02X need_power_saving=%d",
                3, role, g_smcharger_context->aws_state, need_power_saving);
    }

    if (need_power_saving) {
        return APP_POWER_SAVING_TARGET_MODE_SYSTEM_OFF;
    } else {
        return APP_POWER_SAVING_TARGET_MODE_NORMAL;
    }
}
#endif

app_smcharger_action_status_t app_smcharger_state_do_action(int state)
{
    if (g_smcharger_context == NULL) {
        APPS_LOG_MSGID_E(SMCHARGER_UTILS", app_smcharger do_action fail, invalid context", 0);
        return APP_SMCHARGER_FAILURE;
    }
    int old_state = g_smcharger_context->smcharger_state;
    g_smcharger_context->smcharger_state = state;
    APPS_LOG_I(SMCHARGER_UTILS", app_smcharger %s -> %s action",
                    app_smcharger_state_string(old_state), app_smcharger_state_string(state));

    // partner sync smcharger_state to agent for RHO
    if ((old_state == STATE_SMCHARGER_OUT_OF_CASE && state == STATE_SMCHARGER_LID_OPEN)
            || (old_state == STATE_SMCHARGER_LID_OPEN && state == STATE_SMCHARGER_OUT_OF_CASE)
            // partner sync close_state to agent
            || (old_state == STATE_SMCHARGER_LID_OPEN && state == STATE_SMCHARGER_LID_CLOSE)) {
        app_smcharger_ui_shell_event(EVENT_ID_SMCHARGER_SYNC_STATE, NULL, 0);
    }

#ifdef APPS_SLEEP_AFTER_NO_CONNECTION
    // need to determine "is_need_power_saving" when SmartCharger enable
    if (state == STATE_SMCHARGER_OUT_OF_CASE
            || state == STATE_SMCHARGER_LID_OPEN
            || state == STATE_SMCHARGER_LID_CLOSE) {

        app_power_saving_utils_notify_mode_changed(app_smcharger_get_power_saving_target_mode);
    }
#endif

    // SMCharger Notify State to other APP
    app_smcharger_notify_state_to_app(old_state, state);

    app_smcharger_action_status_t status = APP_SMCHARGER_OK;
    switch (state) {
        case STATE_SMCHARGER_STARTUP: {
            status += app_smcharger_handle_system_boot();
            break;
        }
        case STATE_SMCHARGER_LID_CLOSE: {
            status += app_smcharger_mute_audio(TRUE);
            status += app_smcharger_switch_bt(FALSE);
            break;
        }
        case STATE_SMCHARGER_LID_OPEN: {
            if (old_state == STATE_SMCHARGER_STARTUP) {
                if (BT_POWER_ON_DUT == bt_power_on_get_config_type()) {
                    APPS_LOG_MSGID_I(SMCHARGER_UTILS", BT_POWER_ON_DUT, No switch bt on", 0);
                } else {
                    status += app_smcharger_switch_bt(TRUE);
                }
            } else if (old_state == STATE_SMCHARGER_LID_CLOSE) {
                status += app_smcharger_switch_bt(TRUE);
            }
            status += app_smcharger_mute_audio(TRUE);
        #if 0//def MTK_ANC_ENABLE
            anc_control_result_t anc_ret = audio_anc_suspend(app_smcharger_anc_callback);
            APPS_LOG_MSGID_I(SMCHARGER_UTILS", audio_anc_suspend ret=%d", 1, anc_ret);
        #endif
            break;
        }
        case STATE_SMCHARGER_OUT_OF_CASE: {
            status += app_smcharger_switch_bt(TRUE);
            status += app_smcharger_mute_audio(FALSE);
        #if 0//def MTK_ANC_ENABLE
            anc_control_result_t anc_ret = audio_anc_resume(app_smcharger_anc_callback);
            APPS_LOG_MSGID_I(SMCHARGER_UTILS", audio_anc_resume ret=%d", 1, anc_ret);
        #endif
            break;
        }
        case STATE_SMCHARGER_OFF: {
            status += app_smcharger_power_off(FALSE);
            break;
        }
        default: {
            configASSERT(0);
            break;
        }
    }
    if (status != APP_SMCHARGER_OK) {
        APPS_LOG_MSGID_E(SMCHARGER_UTILS", app_smcharger do_action fail %d", 1, status);
    }
    return status;
}

void app_smcharger_init()
{
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", app_smcharger_init", 0);
    DrvCharger_RegisterSmartCase(app_smcharger_driver_callback);
#ifdef MTK_SMART_CHARGER_TEST
    atci_status_t ret = atci_register_handler(smcharger_atci_cmd, sizeof(smcharger_atci_cmd) / sizeof(atci_cmd_hdlr_item_t));
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", app_smcharger_init atci ret=%d", 1, ret);
#endif

#ifdef APPS_SLEEP_AFTER_NO_CONNECTION
    app_power_saving_utils_register_get_mode_callback(app_smcharger_get_power_saving_target_mode);
#endif
}

void app_smcharger_set_context(app_smcharger_context_t *smcharger_ctx)
{
    g_smcharger_context = smcharger_ctx;
}

app_smcharger_context_t* app_smcharger_get_context()
{
    return g_smcharger_context;
}

bool app_smcharger_show_led_bg_pattern()
{
    app_battery_state_t battery_state = (g_smcharger_context != NULL ?
                        g_smcharger_context->battery_state : APP_BATTERY_STATE_IDLE);
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", show BG_LED battery_state=%d", 1, battery_state);
    bool ret = FALSE;
    switch (battery_state) {
        case APP_BATTERY_STATE_IDLE:
        case APP_BATTERY_STATE_FULL:
            APPS_LOG_MSGID_I(SMCHARGER_UTILS", show BG_LED none", 0);
            ret = FALSE;
            break;
        case APP_BATTERY_STATE_CHARGING:
            APPS_LOG_MSGID_I(SMCHARGER_UTILS", show BG_LED CHARGING", 0);
            apps_config_set_background_led_pattern(LED_INDEX_CHARGING, FALSE, APPS_CONFIG_LED_AWS_SYNC_PRIO_HIGH);
            ret = TRUE;
            break;
        case APP_BATTERY_STATE_CHARGING_FULL:
            APPS_LOG_MSGID_I(SMCHARGER_UTILS", IDLE BG_LED for CHARGING_FULL", 0);
            apps_config_set_background_led_pattern(LED_INDEX_IDLE, FALSE, APPS_CONFIG_LED_AWS_SYNC_PRIO_LOWEST);
            ret = TRUE;
            break;
        case APP_BATTERY_STATE_LOW_CAP:
        case APP_BATTERY_STATE_SHUTDOWN:
            APPS_LOG_MSGID_I(SMCHARGER_UTILS", show BG_LED LOW_BATTERY/SHUTDOWN", 0);
            apps_config_set_background_led_pattern(LED_INDEX_LOW_BATTERY, FALSE, APPS_CONFIG_LED_AWS_SYNC_PRIO_HIGH);
            ret = TRUE;
            break;
        case APP_BATTERY_STATE_THR:
            APPS_LOG_MSGID_I(SMCHARGER_UTILS", no BG_LED APP_BATTERY_STATE_THR", 0);
            apps_config_set_background_led_pattern(LED_INDEX_CHARGING, FALSE, APPS_CONFIG_LED_AWS_SYNC_PRIO_HIGH);
            ret = TRUE;
            break;
    }
    return ret;
}

void app_smcharger_handle_battery_report(uint8_t battery_percent)
{
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", handle_battery_report battery_percent=%d", 1, battery_percent);
    if (battery_percent <= 100) {
        // Use SmartCharger Case battery_percent, such as send it by BLE ADV
        // Send public event
        app_smcharger_ui_shell_public_event(SMCHARGER_CASE_BATTERY_REPORT_ACTION, battery_percent);
    } else {
        APPS_LOG_MSGID_E(SMCHARGER_UTILS", handle_battery_report invalid battery_percent", 0);
    }
}

void app_smcharger_handle_key_event(int state, uint8_t key_value)
{
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", handle_key_event state=%d key=%d", 2, state, key_value);
    if ((state == STATE_SMCHARGER_LID_OPEN || state == STATE_SMCHARGER_LID_CLOSE)
            && key_value <= 3) {
        // Use key_value(0/1/2/3) according to Case State, such as Google Fast Pairing
        if (key_value == 0) {
            // trigger Google Fast Pairing by enable EDR discover
            app_smcharger_enable_discover();
        }

        // Send public event
        app_smcharger_ui_shell_public_event(SMCHARGER_CHARGER_KEY_ACTION, key_value);
    } else {
        APPS_LOG_MSGID_E(SMCHARGER_UTILS", handle_key_event invalid key_value", 0);
    }
}

void app_smcharger_handle_user_data(int state, int event_id, uint8_t user_data)
{
    APPS_LOG_MSGID_I(SMCHARGER_UTILS", handle_user_data state=%d event_id=%d user_data=0x%02X",
                                        3, state, event_id, user_data);

    // Send SMCHARGER_CLOSE_LID_ACTION to notify other APP
    if (state == STATE_SMCHARGER_LID_OPEN && event_id == EVENT_ID_SMCHARGER_USER_DATA1_PATTERN) {
        app_smcharger_ui_shell_public_event(SMCHARGER_CLOSE_LID_ACTION, 0);
    }

    if ((state == STATE_SMCHARGER_LID_OPEN || state == STATE_SMCHARGER_LID_CLOSE)
            && (event_id == EVENT_ID_SMCHARGER_USER_DATA1_PATTERN
                    || event_id == EVENT_ID_SMCHARGER_USER_DATA2_PATTERN
                    || event_id == EVENT_ID_SMCHARGER_USER_DATA3_PATTERN)) {
        // Use user_data according to Case State & User_Data1/2/3
        // user_data & SMCHARGER_DATA_MASK_B01/SMCHARGER_DATA_MASK_B234/SMCHARGER_DATA_MASK_B567

        // Send public event
        if (event_id == EVENT_ID_SMCHARGER_USER_DATA1_PATTERN) {
            app_smcharger_ui_shell_public_event(SMCHARGER_USER_DATA1_ACTION, user_data);
        } else if (event_id == EVENT_ID_SMCHARGER_USER_DATA2_PATTERN) {
            app_smcharger_ui_shell_public_event(SMCHARGER_USER_DATA2_ACTION, user_data);
        } else if (event_id == EVENT_ID_SMCHARGER_USER_DATA3_PATTERN) {
            app_smcharger_ui_shell_public_event(SMCHARGER_USER_DATA3_ACTION, user_data);
        }
    } else {
        APPS_LOG_MSGID_E(SMCHARGER_UTILS", handle_user_data invalid key_value", 0);
    }
}

/*
 * SmartCharger UT Code by using AT CMD
 * AT+SMC=pwr_key_boot/chg_in_boot/chg_in/chg_out/chg_off/lid_close/lid_open
 */
#ifdef MTK_SMART_CHARGER_TEST

char smcharger_context_str[200] = {0};

static void smcharger_atci_get_context_str(uint8_t *response_buf)
{
    memset(response_buf, 0, ATCI_UART_TX_FIFO_BUFFER_SIZE);
    snprintf((char *)response_buf, ATCI_UART_TX_FIFO_BUFFER_SIZE,
            "role=0x%02X battery_state=%d smcharger_state=%s[partner-%s] battery_percent=%d[partner-%d]\r\n",
            bt_device_manager_aws_local_info_get_role(),
            g_smcharger_context->battery_state,
            app_smcharger_state_string(g_smcharger_context->smcharger_state),
            app_smcharger_state_string(g_smcharger_context->partner_smcharger_state),
            (int)g_smcharger_context->battery_percent,
            (int)g_smcharger_context->partner_battery_percent);
}

static atci_status_t smcharger_atci_handler(atci_parse_cmd_param_t *parse_cmd)
{
    atci_response_t response = {{0}, 0};
    switch (parse_cmd->mode)
    {
        case ATCI_CMD_MODE_EXECUTION: {
          char *atcmd = parse_cmd->string_ptr + parse_cmd->name_len + 1;
          char cmd[20] = {0};
          uint8_t data = 0;
          memcpy(cmd, atcmd, strlen(atcmd) - 2);
          APPS_LOG_I(SMCHARGER_UTILS", smcharger AT CMD=%s", cmd);
          int event_id = EVENT_ID_SMCHARGER_NONE;
          if (strcmp(cmd, "pwr_key_boot") == 0) {
             event_id = EVENT_ID_SMCHARGER_POWER_KEY_BOOT;
          } else if (strcmp(cmd, "chg_in_boot") == 0) {
              event_id = EVENT_ID_SMCHARGER_CHARGER_IN_BOOT;
          } else if (strcmp(cmd, "chg_in") == 0) {
              event_id = EVENT_ID_SMCHARGER_CHARGER_IN_INTERRUPT;
          } else if (strcmp(cmd, "chg_out") == 0) {
              event_id = EVENT_ID_SMCHARGER_CHARGER_OUT_PATTERN;
          } else if (strcmp(cmd, "chg_off") == 0) {
              event_id = EVENT_ID_SMCHARGER_CHARGER_OFF_PATTERN;
              data = 0;    // battery level report
          } else if (strcmp(cmd, "key") == 0) {
              event_id = EVENT_ID_SMCHARGER_CHARGER_KEY_PATTERN;
              data = 0;     // key value
          } else if (strcmp(cmd, "lid_close") == 0) {
              event_id = EVENT_ID_SMCHARGER_USER_DATA1_PATTERN;
              ui_shell_send_event(FALSE, EVENT_PRIORITY_HIGH, EVENT_GROUP_UI_SHELL_CHARGER_CASE,
                      EVENT_ID_SMCHARGER_LID_CLOSE_PATTERN, NULL, 0, NULL, 3 * 1000);
          } else if (strcmp(cmd, "lid_open") == 0) {
              event_id = EVENT_ID_SMCHARGER_LID_OPEN_PATTERN;
              data = 80;    // battery level report
          } else if (strcmp(cmd, "user_data1") == 0) {
              event_id = EVENT_ID_SMCHARGER_USER_DATA1_PATTERN;
              data = 0x11;
          } else if (strcmp(cmd, "user_data2") == 0) {
              event_id = EVENT_ID_SMCHARGER_USER_DATA2_PATTERN;
              data = 0x22;
          } else if (strcmp(cmd, "user_data3") == 0) {
              event_id = EVENT_ID_SMCHARGER_USER_DATA3_PATTERN;
              data = 0x33;
          } else if (strcmp(cmd, "status") == 0) {
              smcharger_atci_get_context_str(response.response_buf);
              break;
          } else {
              APPS_LOG_I(SMCHARGER_UTILS", invalid SMCharger AT-CMD");
          }
          memset(response.response_buf, 0, ATCI_UART_TX_FIFO_BUFFER_SIZE);
          snprintf((char *)response.response_buf, ATCI_UART_TX_FIFO_BUFFER_SIZE, "OK - %s\r\n", atcmd);
          app_smcharger_ui_shell_event(event_id, (void *)(uint32_t)data, 0);
          response.response_flag = ATCI_RESPONSE_FLAG_APPEND_OK;
          break;
        }
        default:
            response.response_flag = ATCI_RESPONSE_FLAG_APPEND_ERROR;
            break;
    }
    response.response_len = strlen((char *)response.response_buf);
    atci_send_response(&response);
    return ATCI_STATUS_OK;
}
#endif

#endif
