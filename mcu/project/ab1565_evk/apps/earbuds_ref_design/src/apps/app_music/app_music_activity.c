/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

 

#include "app_music_activity.h"
#include "app_music_utils.h"
#include "apps_debug.h"
#include "bt_sink_srv.h"
#include "apps_events_event_group.h"
#include "apps_events_interaction_event.h"

#include "apps_config_key_remapper.h"
#ifdef MTK_IN_EAR_FEATURE_ENABLE
#include "app_in_ear_idle_activity.h"
#endif

static bool _proc_ui_shell_group(struct _ui_shell_activity *self, uint32_t event_id, void *extra_data,size_t data_len); 
static bool _proc_key_event_group(struct _ui_shell_activity *self, uint32_t event_id, void *extra_data, size_t data_len);



static bool _proc_ui_shell_group(struct _ui_shell_activity *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    switch (event_id) {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE: {
            APPS_LOG_MSGID_I("app_music_activity_proc create  current activity : %x", 1, (uint32_t)self);
            if (extra_data) {
                self->local_context = extra_data;
                apps_music_local_context_t *local_context = (apps_music_local_context_t *)self->local_context;
                local_context->avrcp_op_sta = AVRCP_OPERATION_STA_IDLE;
            }
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_UPDATE_LED_BG_PATTERN, NULL, 0,
                    NULL, 0);
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_UPDATE_MMI_STATE, NULL, 0,
                    NULL, 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY: {
            APPS_LOG_MSGID_I("app_music_activity_proc destroy", 0);
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_UPDATE_LED_BG_PATTERN, NULL, 0,
                    NULL, 0);
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_UPDATE_MMI_STATE, NULL, 0,
                    NULL, 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESUME: {
            APPS_LOG_MSGID_I("app_music_activity_proc resume", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE: {
            APPS_LOG_MSGID_I("app_music_activity_proc pause", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH: {
            APPS_LOG_MSGID_I("app_music_activity_proc refresh", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESULT: {
            APPS_LOG_MSGID_I("app_music_activity_proc result", 0);
            break;
        }
        default:
            break;
    }
    return ret;
}


static bool _proc_key_event_group(
        struct _ui_shell_activity *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    uint32_t key_num = event_id & 0x000000FF;
    uint32_t key_event = ((event_id >> 8) & 0x000000FF);
    apps_config_key_action_t action = KEY_ACTION_INVALID;
    apps_music_local_context_t *local_context = (apps_music_local_context_t *)self->local_context;

    APPS_LOG_MSGID_I("app_music_activity_proc key_num : 0x%x, key_event: 0x%x", 2, key_num, key_event);
    action = app_bt_music_proc_key_event(self, event_id, extra_data, data_len);
    
    if (action != KEY_ACTION_INVALID) {
        if (action == KEY_AVRCP_PAUSE){
            APPS_LOG_MSGID_I("is playing: 0x%x", 1, local_context->music_playing);
            /* don't finish here, should finish after state change from streaming to connect (BT_SINK_SRV_EVENT_STATE_CHANGE)
            if(local_context->music_playing) {
                ui_shell_finish_activity(self, self);
                local_context->music_playing = false;
                local_context->isAutoPaused = false;
                APPS_LOG_MSGID_I("ui_shell_finish_activity app_music_activity_proc, current activity : 0x%x, isAutoPaused: %d", 2, 
                    (uint32_t)self, local_context->isAutoPaused);
            }*/
        }
        ret = true;
    }

    return ret;
}

static bool _proc_bt_event_when_playing(
        struct _ui_shell_activity *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    if (event_id == BT_SINK_SRV_EVENT_STATE_CHANGE) {
        bt_sink_srv_state_change_t *param = (bt_sink_srv_state_change_t*)extra_data;
        apps_music_local_context_t *local_context = (apps_music_local_context_t *)self->local_context;

        APPS_LOG_MSGID_I("param->now = 0x%x, param->pre = 0x%x, is playing: 0x%x", 3, param->current, param->previous, local_context->music_playing);

        if ((param->previous == BT_SINK_SRV_STATE_STREAMING) && (param->current != BT_SINK_SRV_STATE_STREAMING)) {
            if(local_context->music_playing){
                ui_shell_finish_activity(self, self);
                local_context->music_playing = false;
                local_context->isAutoPaused = false;
                APPS_LOG_MSGID_I("ui_shell_finish_activity app_music_activity_proc, current activity : 0x%x, isAutoPaused:%d", 2, 
                    (uint32_t)self, local_context->isAutoPaused);
            }
        }
    }
    return false;
}

#ifdef MTK_IN_EAR_FEATURE_ENABLE
static bool check_and_ctrl_music(struct _ui_shell_activity *self, void *extra_data)
{
    apps_music_local_context_t* ctx = (apps_music_local_context_t*)self->local_context;
    app_in_ear_sta_info_t* sta_info = (app_in_ear_sta_info_t*)extra_data;
    
    APPS_LOG_MSGID_I(APP_MUSIC_UTILS", check and ctrl music, cur: %d, pre: %d, isAutoPaused: %d", 3, 
                     sta_info->current, sta_info->previous, ctx->isAutoPaused);

#ifdef IN_EAR_FEATURE_AIROHA
    if(sta_info->previous != APP_IN_EAR_STA_BOTH_OUT
       && sta_info->current == APP_IN_EAR_STA_BOTH_OUT
       && !ctx->isAutoPaused
       && ctx->music_playing)
#else
    if(sta_info->current != APP_IN_EAR_STA_BOTH_IN
       && sta_info->previous != APP_IN_EAR_STA_BOTH_OUT
       && !ctx->isAutoPaused
       && ctx->music_playing)
#endif
    {
        ctx->isAutoPaused = true;
        bt_status_t bt_status = bt_sink_srv_send_action(BT_SINK_SRV_ACTION_PAUSE, NULL);
        if (bt_status != BT_STATUS_SUCCESS) {
            APPS_LOG_MSGID_I(APP_MUSIC_UTILS", app_do_music_action fail, bt_status: 0x%x, isAutoPaused: %d", 2, 
                bt_status, ctx->isAutoPaused);
        }
        
        ui_shell_finish_activity(self, self);
        ctx->music_playing = false;
        APPS_LOG_MSGID_I("ui_shell_finish_activity app_music_activity_proc, paused by auto, current activity : 0x%x, isAutoPaused:%d",
           2, (uint32_t)self, ctx->isAutoPaused);
    }

    /* 
     * while the phone paused music, the sink server will report this event with at least 3 seconds delay.
     * so, it's possiable that this activity will receive the music playing request before the sink event.
     * In this case, may means that the old Agent send pause action and finished the shell activity, but
     * the new Agent not recv the SINK_SRV event about the music paused status.
     */
    if(sta_info->previous != APP_IN_EAR_STA_BOTH_IN && sta_info->current != APP_IN_EAR_STA_BOTH_OUT)
    {
        APPS_LOG_MSGID_I("app_music_activity_proc, do music playing action while the music playing flag is true, music_palying:%d, isAutoPaused:%d",
                         2, ctx->music_playing, ctx->isAutoPaused);
        if(!ctx->music_playing && ctx->isAutoPaused){
            bt_status_t bt_status = bt_sink_srv_send_action(BT_SINK_SRV_ACTION_PLAY, NULL);
            if (bt_status != BT_STATUS_SUCCESS) {
                APPS_LOG_MSGID_I(APP_MUSIC_UTILS", app_do_music_action fail, bt_status: 0x%x, isAutoPaused: %d", 2, 
                    bt_status, ctx->isAutoPaused);
            }else{
                ctx->isAutoPaused = false;
                ctx->music_playing = true;
            }
        }
    }

    if(sta_info->current != APP_IN_EAR_STA_BOTH_IN){
        ctx->isBoothInEar = false;
    }else{
        ctx->isBoothInEar = true;
    }

    if(sta_info->current != APP_IN_EAR_STA_BOTH_OUT){
        app_bt_music_checkAudioState(ctx);
    }

    return true;
}

#endif /*MTK_IN_EAR_FEATURE_ENABLE*/

static bool _proc_apps_internal_events(
        struct _ui_shell_activity *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;

    switch (event_id) {
        case APPS_EVENTS_INTERACTION_UPDATE_LED_BG_PATTERN:
            // Not need to display LED under A2DP playing music state, so dispatch the event to next activity to display LED
            break;
        case APPS_EVENTS_INTERACTION_UPDATE_MMI_STATE:
            apps_config_key_set_mmi_state(APP_A2DP_PLAYING);
            ret = true;
            break;
#ifdef GSOUND_LIBRARY_ENABLE
        case APPS_EVENTS_INTERACTION_GSOUND_ACTION_REJECTED:
            ret = app_bt_music_proc_gsound_reject_action(self, (bt_sink_srv_action_t)extra_data);
            break;
#endif

#ifdef MTK_IN_EAR_FEATURE_ENABLE
        case APPS_EVENTS_INTERACTION_IN_EAR_UPDATE_STA:
            ret = check_and_ctrl_music(self, extra_data);
            break;
#endif
        default:
            APPS_LOG_MSGID_I("Not supported event id = 0x%x", 1, event_id);
            break;
    }

    return ret;
}

bool app_music_activity_proc(
            struct _ui_shell_activity *self,
            uint32_t event_group,
            uint32_t event_id,
            void *extra_data,
            size_t data_len)
{
    bool ret = false;
    APPS_LOG_MSGID_I("app_music_activity_proc event_group: 0x%x, event_id: 0x%x", 2, event_group, event_id);
    switch (event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM: {
            ret = _proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_KEY: {
            ret = _proc_key_event_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_SINK: {
            ret = _proc_bt_event_when_playing(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_APP_INTERACTION: {
            ret = _proc_apps_internal_events(self, event_id, extra_data, data_len);
            break;
        }
        default:
            break;
    }
    return ret;
}


