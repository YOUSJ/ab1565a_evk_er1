/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "FreeRTOS.h"
#include "app_music_idle_activity.h"
#include "app_music_activity.h"
#include "app_music_utils.h"
#include "apps_events_interaction_event.h"


#include "apps_debug.h"
#include "apps_events_event_group.h"
#include "bt_sink_srv.h"

#include "apps_config_key_remapper.h"
#ifdef MTK_IN_EAR_FEATURE_ENABLE
#include "app_in_ear_idle_activity.h"
#endif
#include "bt_role_handover.h"

static bool _proc_ui_shell_group(struct _ui_shell_activity *self, uint32_t event_id, void *extra_data,size_t data_len); 
static bool _proc_key_event_group(struct _ui_shell_activity *self, uint32_t event_id, void *extra_data, size_t data_len);

#ifdef MTK_IN_EAR_FEATURE_ENABLE
static apps_music_local_context_t* g_pctx = NULL;

static uint8_t _role_handover_service_get_data_len_callback(const bt_bd_addr_t *addr)
{
    uint8_t len = sizeof(apps_music_local_context_t);
    APPS_LOG_MSGID_I("app_music_idle_activity get_data_len_callback: %d", 1, len);
    return len;

}

static bt_status_t _role_handover_get_data_callback(const bt_bd_addr_t *addr, void * data)
{
    apps_music_local_context_t *pdata = (apps_music_local_context_t *)data;
    memcpy(data, (void*)g_pctx, sizeof(apps_music_local_context_t));
    APPS_LOG_MSGID_I("app_music_idle_activity get_data_callback, isAutoPaused: %d", 1, pdata->isAutoPaused);
   return BT_STATUS_SUCCESS;
}

static bt_status_t _role_handover_update_data_callback(bt_role_handover_update_info_t *info)
{
    APPS_LOG_MSGID_I("app_music_idle_activity recv, role: %d", 1, info->role);

    if (info->role == BT_AWS_MCE_ROLE_PARTNER) {
        apps_music_local_context_t *pdata = (apps_music_local_context_t *)info->data;
        APPS_LOG_MSGID_I("app_music_idle_activity recv, role: %d, isAutoPaused: %d, isPlaying: %d", 3,
            info->role, pdata->isAutoPaused, g_pctx->music_playing);
        if(g_pctx->isAutoPaused != pdata->isAutoPaused){
            APPS_LOG_MSGID_I("app_music_idle_activity rho status, isAutoPaused: old: %d, new: %d", 2,
            g_pctx->isAutoPaused, pdata->isAutoPaused);
            g_pctx->isAutoPaused = pdata->isAutoPaused;
        }

        if(g_pctx->music_playing != pdata->music_playing){
            APPS_LOG_MSGID_I("app_music_idle_activity rho status, isPlaying: old: %d, new: %d", 2,
            g_pctx->music_playing, pdata->music_playing);
            g_pctx->music_playing = pdata->music_playing;
        }
    }
    return BT_STATUS_SUCCESS;
}


static void _role_handover_service_status_callback(
        const bt_bd_addr_t *addr,
        bt_aws_mce_role_t role,
        bt_role_handover_event_t event,
        bt_status_t status)
{
	if (BT_ROLE_HANDOVER_COMPLETE_IND == event)
    {
        if (BT_STATUS_SUCCESS == status)
        {
            /* allways set partner connecting flag to true while rho success. */
            g_pctx->isPartnerConnected = true;
        }
    }
}

#endif /*MTK_IN_EAR_FEATURE_ENABLE*/

static bool _proc_ui_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data,size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    switch (event_id) {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE: {
            APPS_LOG_MSGID_I("app_music_idle_activity_proc create current activity : 0x%x", 1, (uint32_t)self);
            self->local_context = pvPortMalloc(sizeof(apps_music_local_context_t));
            if (self->local_context) {
#ifdef MTK_IN_EAR_FEATURE_ENABLE
                g_pctx = self->local_context;
#endif
                ((apps_music_local_context_t *)self->local_context)->music_playing = false;
#ifdef MTK_AWS_MCE_ENABLE
                ((apps_music_local_context_t *)self->local_context)->isPartnerCharging = false;
                ((apps_music_local_context_t *)self->local_context)->isPartnerConnected = false;
                ((apps_music_local_context_t *)self->local_context)->isBoothInEar = false;
                ((apps_music_local_context_t *)self->local_context)->currMixState = MUSIC_STEREO;
#endif

#ifdef MTK_IN_EAR_FEATURE_ENABLE
                ((apps_music_local_context_t *)self->local_context)->isAutoPaused = false;
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
                bt_role_handover_callbacks_t role_callbacks={
                    NULL,
                    _role_handover_service_get_data_len_callback,
                    _role_handover_get_data_callback,
                    _role_handover_update_data_callback,
                    _role_handover_service_status_callback
                };
                bt_role_handover_register_callbacks(BT_ROLE_HANDOVER_MODULE_MUSIC, &role_callbacks);
#endif
#endif /*MTK_IN_EAR_FEATURE_ENABLE*/
                ((apps_music_local_context_t *)self->local_context)->avrcp_op_sta = AVRCP_OPERATION_STA_IDLE;
            }
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY: {
            APPS_LOG_MSGID_I("app_music_idle_activity_proc destroy", 0);
            if (self->local_context) {
                vPortFree(self->local_context);
            }
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESUME: {
            APPS_LOG_MSGID_I("app_music_idle_activity_proc resume", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE: {
            APPS_LOG_MSGID_I("app_music_idle_activity_proc pause", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH: {
            APPS_LOG_MSGID_I("app_music_idle_activity_proc refresh", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESULT: {
            APPS_LOG_MSGID_I("app_music_idle_activity_proc result", 0);
            break;
        }
        default:
            break;
    }
    return ret;
}


static bool _proc_key_event_group(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    apps_config_key_action_t action = KEY_ACTION_INVALID;
    apps_music_local_context_t *local_ctx = (apps_music_local_context_t *)self->local_context;

    if (local_ctx) {
        action = app_bt_music_proc_key_event(self, event_id, extra_data, data_len);
    }

    if (action != KEY_ACTION_INVALID) {
        if (action == KEY_AVRCP_PLAY) {
            /* start music app in case, for seldom issue: user want to play while device is waiting for SP suspend streaming rsp after pause,
            during waiting for the rsp, sink srv will keep streaming state. */
            if (local_ctx && !local_ctx->music_playing) {
                local_ctx->isAutoPaused = false;
                ui_shell_start_activity(self, app_music_activity_proc, ACTIVITY_PRIORITY_MIDDLE, local_ctx, 0);
                APPS_LOG_MSGID_I("ui_shell_start_activity app_music_activity_proc, current activity : %x, isAutoPaused:%d",
                    2, (uint32_t )self, local_ctx->isAutoPaused);
                local_ctx->music_playing = true;
            }
            ret = true;
        }
    }
    return ret;

}

#ifdef MTK_IN_EAR_FEATURE_ENABLE
static bool check_and_start_music(struct _ui_shell_activity *self, void *extra_data)
{
    apps_music_local_context_t* ctx = (apps_music_local_context_t*)self->local_context;
    app_in_ear_sta_info_t* sta_info = (app_in_ear_sta_info_t*)extra_data;
    
    APPS_LOG_MSGID_I("app_music_idle_activity check_and_start_music, cur: %d, pre: %d, isAutoPaused: %d, PartnerCharging:%d, connected:%d",
        5, sta_info->current, sta_info->previous, ctx->isAutoPaused, ctx->isPartnerCharging, ctx->isPartnerConnected);
    if(ctx->isAutoPaused && sta_info->previous != APP_IN_EAR_STA_BOTH_IN && sta_info->current != APP_IN_EAR_STA_BOTH_OUT)
    {
        ctx->isAutoPaused = false;
        if(!ctx->music_playing){
            bt_status_t bt_status = bt_sink_srv_send_action(BT_SINK_SRV_ACTION_PLAY, NULL);
            if (bt_status != BT_STATUS_SUCCESS) {
                APPS_LOG_MSGID_I(APP_MUSIC_UTILS", app_do_music_action fail, bt_status: 0x%x", 1, bt_status);
            }
            ui_shell_start_activity(self, app_music_activity_proc, ACTIVITY_PRIORITY_MIDDLE, ctx, 0);
            APPS_LOG_MSGID_I("ui_shell_start_activity app_music_activity_proc by auto, current activity : %x, isAutoPaused: %d", 2,
                (uint32_t)self, ctx->isAutoPaused);
            ctx->music_playing = true;
        }
    }

    /*
     * this is a sepcial case. it is means that the old Agent send the play action and start the shell activity, but the new Agent
     * still in pause status because of the SINK_SRV not report the event about the music to play status.
     */
#ifdef IN_EAR_FEATURE_AIROHA
    if(sta_info->previous != APP_IN_EAR_STA_BOTH_OUT && sta_info->current == APP_IN_EAR_STA_BOTH_OUT)
#else
    if(sta_info->current != APP_IN_EAR_STA_BOTH_IN && sta_info->previous != APP_IN_EAR_STA_BOTH_OUT)
#endif
    {
        APPS_LOG_MSGID_I("app_music_idle_activity recv pause request when it was paused. music_playing:%d", 1, ctx->music_playing);
        if(ctx->music_playing){
            bt_status_t bt_status = bt_sink_srv_send_action(BT_SINK_SRV_ACTION_PAUSE, NULL);
            if (bt_status != BT_STATUS_SUCCESS) {
                APPS_LOG_MSGID_I(APP_MUSIC_UTILS", app_do_music_action fail, bt_status: 0x%x", 1, bt_status);
            }else{
                ctx->music_playing = false;
                ctx->isAutoPaused = true;
            }
        }
    }

    if(sta_info->current != APP_IN_EAR_STA_BOTH_IN){
        ctx->isBoothInEar = false;
    }else{
        ctx->isBoothInEar = true;
    }
    
    return true;
}
#endif /*MTK_IN_EAR_FEATURE_ENABLE*/

static bool app_music_idle_proc_apps_internal_events(
        struct _ui_shell_activity *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;

    switch (event_id) {
#ifdef GSOUND_LIBRARY_ENABLE
        case APPS_EVENTS_INTERACTION_GSOUND_ACTION_REJECTED:
            ret = app_bt_music_proc_gsound_reject_action(self, (bt_sink_srv_action_t)extra_data);
            break;
#endif

#ifdef MTK_IN_EAR_FEATURE_ENABLE
        case APPS_EVENTS_INTERACTION_IN_EAR_UPDATE_STA:
            ret = check_and_start_music(self, extra_data);
            break;
#endif
        
        default:
            APPS_LOG_MSGID_I("Not supported event id = 0x%x", 1, event_id);
            break;
    }

    return ret;
}

bool app_music_idle_activity_proc(
            ui_shell_activity_t *self,
            uint32_t event_group,
            uint32_t event_id,
            void *extra_data,
            size_t data_len)
{
    bool ret = false;
    APPS_LOG_MSGID_I("app_music_idle_activity_proc event_group: 0x%x, event_id: 0x%x", 2, event_group, event_id);
    switch (event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM: {
            ret = _proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_KEY: {
            ret = _proc_key_event_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_SINK: {
            ret = app_bt_music_proc_basic_state_event(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_CONN_MANAGER:
            ret = app_music_util_bt_cm_event_proc(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_APP_INTERACTION:
            ret = app_music_idle_proc_apps_internal_events(self, event_id, extra_data, data_len);
            break;
#if defined(MTK_AWS_MCE_ENABLE)
        case EVENT_GROUP_UI_SHELL_AWS_DATA:
            ret = app_bt_music_proc_aws_data_event(self, event_id, extra_data, data_len);
            break;
#endif
        default:
            break;
    }
    return ret;
}


