/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "app_music_utils.h"
#include "app_music_activity.h"
#include "bt_sink_srv_music.h"
#include "bt_device_manager.h"
#include "apps_debug.h"
#include "bt_sink_srv.h"
#include "bt_sink_srv_ami.h"
#include "apps_events_key_event.h"
#ifdef GSOUND_LIBRARY_ENABLE
#include "app_gsound_idle_activity.h"
#endif
#if defined(MTK_AWS_MCE_ENABLE)
#include "bt_aws_mce_report.h"
#endif
#include "bt_connection_manager.h"

#ifdef MTK_SMART_CHARGER_ENABLE
#include "app_smcharger_idle_activity.h"
#else
#include "app_battery_transient_activity.h"
#endif

#if 0
apps_config_key_action_t app_get_config_status_by_sink_state(bt_sink_srv_state_t state)
{
    apps_config_key_action_t status = APP_TOTAL_STATE_NO;
    APPS_LOG_MSGID_I(APP_MUSIC_UTILS", sink_status: %x", 1, state);

    switch(state)
    {
        case BT_SINK_SRV_STATE_CONNECTED:          /**< BT connected. */
        {
            status = APP_CONNECTED;
            break;
        }
        case BT_SINK_SRV_STATE_STREAMING:          /**< A2DP playing. */
        {
            status = APP_A2DP_PLAYING;
            break;
        }
        default:
            break;
    }
    APPS_LOG_MSGID_I(APP_MUSIC_UTILS", music_status: %x", 1, status);
    return status;
}
#endif


static bool app_do_music_action(ui_shell_activity_t *self, apps_config_key_action_t action)
{
    bt_sink_srv_action_t sink_action = BT_SINK_SRV_ACTION_NONE;
    apps_music_local_context_t *local_context = (apps_music_local_context_t *)self->local_context;
    bt_status_t bt_status;
    bool op_valid = false;
    bt_sink_srv_avrcp_operation_state_t op;

    bool ret = true;

    APPS_LOG_MSGID_I(APP_MUSIC_UTILS", app_do_music_action app_action : 0x%x, avrcp op sta: %d", 2,
                     action, local_context->avrcp_op_sta);

    switch(action)
    {
        case KEY_AVRCP_PLAY:
        case KEY_AVRCP_PAUSE: {
            sink_action = BT_SINK_SRV_ACTION_PLAY_PAUSE;
            break;
        }
        case KEY_VOICE_UP: {
            sink_action = BT_SINK_SRV_ACTION_VOLUME_UP;
            break;
        }
        case KEY_VOICE_DN: {
            sink_action = BT_SINK_SRV_ACTION_VOLUME_DOWN;
            break;
        }
        case KEY_AVRCP_BACKWARD: {
            sink_action = BT_SINK_SRV_ACTION_PREV_TRACK;
            break;
        }
        case KEY_AVRCP_FORWARD: {
            sink_action = BT_SINK_SRV_ACTION_NEXT_TRACK;
            break;
        }
        case KEY_AVRCP_FAST_FORWARD_PRESS: {
            if (local_context->avrcp_op_sta == AVRCP_OPERATION_STA_IDLE) {
                op = BT_SINK_SRV_AVRCP_OPERATION_PRESS;
                op_valid = true;
                sink_action = BT_SINK_SRV_ACTION_FAST_FORWARD;
                local_context->avrcp_op_sta = AVRCP_OPERATION_STA_FAST_FORWARD_PRESS;
            }
            break;
        }
        case KEY_AVRCP_FAST_FORWARD_RELEASE: {
            if (local_context->avrcp_op_sta == AVRCP_OPERATION_STA_FAST_FORWARD_PRESS) {
                op = BT_SINK_SRV_AVRCP_OPERATION_RELEASE;
                op_valid = true;
                sink_action = BT_SINK_SRV_ACTION_FAST_FORWARD;
                local_context->avrcp_op_sta = AVRCP_OPERATION_STA_IDLE;
            }
            break;
        }
        case KEY_AVRCP_FAST_REWIND_PRESS: {
            if (local_context->avrcp_op_sta == AVRCP_OPERATION_STA_IDLE) {
                op = BT_SINK_SRV_AVRCP_OPERATION_PRESS;
                op_valid = true;
                sink_action = BT_SINK_SRV_ACTION_REWIND;
                local_context->avrcp_op_sta = AVRCP_OPERATION_STA_FAST_REWIND_PRESS;
            }
            break;
        }
        case KEY_AVRCP_FAST_REWIND_RELEASE: {
            if (local_context->avrcp_op_sta == AVRCP_OPERATION_STA_FAST_REWIND_PRESS) {
                op = BT_SINK_SRV_AVRCP_OPERATION_RELEASE;
                op_valid = true;
                sink_action = BT_SINK_SRV_ACTION_REWIND;
                local_context->avrcp_op_sta = AVRCP_OPERATION_STA_IDLE;
            }
            break;
        }
        default:
        {
            ret = false;
            break;
        }
    }

    APPS_LOG_MSGID_I(APP_MUSIC_UTILS", app_do_music_action, ret: 0x%x, sink_action : 0x%x", 2, ret, sink_action);

    if (ret) {

#ifdef GSOUND_LIBRARY_ENABLE
        bool gsound_ret = false;
        gsound_ret = app_gsound_idle_activity_proc_music_event(sink_action);
        if (gsound_ret) {
            ret = false;
        } else
#endif
        {
            if (op_valid) {
                APPS_LOG_MSGID_I(APP_MUSIC_UTILS", app_do_music_action, op: 0x%x, sink_action : 0x%x", 2, op, sink_action);
                bt_status = bt_sink_srv_send_action(sink_action, &op);
            } else {
                bt_status = bt_sink_srv_send_action(sink_action, NULL);
            }
            if (bt_status != BT_STATUS_SUCCESS) {
                APPS_LOG_MSGID_I(APP_MUSIC_UTILS", app_do_music_action fail, bt_status: 0x%x, sink_action : 0x%x", 2, bt_status, sink_action);
            }
        }
    }
    return ret;
}

#ifdef GSOUND_LIBRARY_ENABLE
bool app_bt_music_proc_gsound_reject_action(ui_shell_activity_t *self, bt_sink_srv_action_t sink_action)
{
    apps_music_local_context_t *local_ctx = (apps_music_local_context_t *)self->local_context;
    bool ret = false;
    bt_status_t bt_status;

    switch (sink_action) {
    case BT_SINK_SRV_ACTION_PLAY_PAUSE:
        APPS_LOG_MSGID_I(APP_MUSIC_UTILS", app_bt_music_proc_gsound_reject_action, sink_action : 0x%x", 1, sink_action);
        bt_status = bt_sink_srv_send_action(sink_action, NULL);
        if (bt_status != BT_STATUS_SUCCESS) {
            APPS_LOG_MSGID_W(APP_MUSIC_UTILS", app_bt_music_proc_gsound_reject_action fail, bt_status: 0x%x, sink_action : 0x%x", 2, bt_status, sink_action);
        }else{
            if(local_ctx->music_playing){
#if 0
                ui_shell_finish_activity(self, self);
                APPS_LOG_MSGID_I("ui_shell_finish_activity app_music_activity_proc, current activity : 0x%x", 1, (uint32_t)self);
                local_ctx->music_playing = false;
#endif
            }else{
                local_ctx->isAutoPaused = false;
                ui_shell_start_activity(self, app_music_activity_proc, ACTIVITY_PRIORITY_MIDDLE, local_ctx, 0);
                APPS_LOG_MSGID_I("ui_shell_start_activity app_music_activity_proc, current activity : %x, isAutoPaused: %d", 2, 
                    (uint32_t )self, local_ctx->isAutoPaused);
                local_ctx->music_playing = true;
            }
        }
        ret = true;
        break;
    default:
        break;
    }

    return ret;
}
#endif


apps_config_key_action_t app_bt_music_proc_key_event(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    uint8_t key_id;
    airo_key_event_t key_event;
    bool ret = false;

    app_event_key_event_decode(&key_id, &key_event, event_id);

    apps_config_key_action_t action;
   
    if (extra_data) {
        action = *(uint16_t *)extra_data;
    } else {
        action = apps_config_key_event_remapper_map_action(key_id, key_event);
    }

    APPS_LOG_MSGID_I(APP_MUSIC_UTILS", key_id: 0x%x, key_event: 0x%x, app_action : 0x%x", 3, key_id, key_event, action);

    if (KEY_ACTION_INVALID != action) {
        ret = app_do_music_action(self, action);
    }

    if (ret) {
        return action;
    } else {
        return KEY_ACTION_INVALID;
    }
}


bool app_bt_music_proc_basic_state_event(ui_shell_activity_t *self, uint32_t event_id,
        void *extra_data, size_t data_len) {
    bool ret = false;
    apps_music_local_context_t *local_context = (apps_music_local_context_t *)self->local_context;

    APPS_LOG_MSGID_I(APP_MUSIC_UTILS",   event_id : 0x%x", 1, event_id);

    if (event_id == BT_SINK_SRV_EVENT_STATE_CHANGE) {
        bt_sink_srv_state_change_t *param = (bt_sink_srv_state_change_t*) extra_data;
        APPS_LOG_MSGID_I("app_bt_music_proc_basic_state_event  param->now = 0x%x,  param->pre = 0x%x", 2, param->current, param->previous);
        if ((param->previous != BT_SINK_SRV_STATE_STREAMING) && (param->current == BT_SINK_SRV_STATE_STREAMING)) {
            // Invalid to start duplicate activity.
            if (local_context) {
                if (!local_context->music_playing) {
                    local_context->isAutoPaused = false;
                    ui_shell_start_activity(self, app_music_activity_proc, ACTIVITY_PRIORITY_MIDDLE, local_context, 0);
                    APPS_LOG_MSGID_I("ui_shell_start_activity app_music_activity_proc, current activity : %x, isAutoPaused: %d", 2, 
                        (uint32_t )self, local_context->isAutoPaused);
                    local_context->music_playing = true;
                }
            }
        }
    }

    return ret;
}

bool app_music_util_bt_cm_event_proc(ui_shell_activity_t *self, uint32_t event_id,
        void *extra_data, size_t data_len)
{
    bool ret = false;
    apps_music_local_context_t *local_context = (apps_music_local_context_t *)self->local_context;

#ifdef MTK_AWS_MCE_ENABLE
    bt_aws_mce_role_t role;
    role = bt_device_manager_aws_local_info_get_role();
#endif

    switch (event_id) {
        case BT_CM_EVENT_REMOTE_INFO_UPDATE:
        {
#ifdef MTK_AWS_MCE_ENABLE
            bt_cm_remote_info_update_ind_t *remote_update = (bt_cm_remote_info_update_ind_t *)extra_data;
            if (NULL == local_context || NULL == remote_update) {
                break;
            }
            
            if (BT_AWS_MCE_ROLE_AGENT == role || BT_AWS_MCE_ROLE_NONE == role) {
                if (BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->pre_connected_service
                        && !(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->connected_service)) {
                    local_context->isPartnerConnected = false;
                    local_context->isPartnerCharging = false;
                    app_bt_music_checkAudioState(local_context);
                    APPS_LOG_MSGID_I("app_music_activity_proc Partner Detached.", 0);
                } else if (!(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->pre_connected_service)
                        && BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->connected_service) {
                    local_context->isPartnerConnected = true;
                    APPS_LOG_MSGID_I("app_music_activity_proc Partner Attached.", 0);
                }
            } else if (BT_AWS_MCE_ROLE_PARTNER == role) {
                if (!(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->pre_connected_service)
                        && BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS) & remote_update->connected_service) {
                    am_dynamic_change_channel(AUDIO_CHANNEL_SELECTION_STEREO);
                    local_context->currMixState = MUSIC_STEREO;
                    APPS_LOG_MSGID_I("app_music_activity_proc set partner as stereo.", 0);
                }
            }
#endif
        }
            break;
        default:
            break;
    }
    return ret;
}

#ifdef MTK_AWS_MCE_ENABLE
bool app_bt_music_proc_aws_data_event(ui_shell_activity_t *self, uint32_t event_id,
        void *extra_data, size_t data_len)
{
    bool ret = false;
    bt_aws_mce_report_info_t *aws_data_ind = (bt_aws_mce_report_info_t *)extra_data;
    apps_music_local_context_t *local_context = (apps_music_local_context_t *)self->local_context;

    uint8_t battery = *(uint8_t*)aws_data_ind->param;
    if (aws_data_ind->module_id == BT_AWS_MCE_REPORT_MODULE_BATTERY
            && bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT) {
        APPS_LOG_MSGID_I("app_music_activity_proc Partner charge percent: %d.", 1, battery);

#ifdef MTK_SMART_CHARGER_ENABLE
        local_context->isPartnerCharging = battery & PARTNER_BATTERY_CHARGING ? true : false;
#else
        local_context->isPartnerCharging = battery & PARTNER_BATTERY_CHARGING ? true : false;
#endif
        app_bt_music_checkAudioState(local_context);
    }
    return ret;
}
void app_bt_music_checkAudioState(apps_music_local_context_t *cntx){

    APPS_LOG_I("currMixState: %s, partner conn: %d, chagring: %d",
        (cntx->currMixState == MUSIC_STEREO) ? "stereo":"mono", cntx->isPartnerConnected, cntx->isPartnerCharging);
    #ifdef MTK_IN_EAR_FEATURE_ENABLE
    if(cntx->isPartnerConnected && !cntx->isPartnerCharging && cntx->isBoothInEar)
    #else
        if(cntx->isPartnerConnected && !cntx->isPartnerCharging)
    #endif
    {//stereo state
        if(cntx->currMixState != MUSIC_STEREO){
            APPS_LOG_MSGID_I("app_music_activity_proc set to stereo", 0);
            am_dynamic_change_channel(AUDIO_CHANNEL_SELECTION_STEREO);
            cntx->currMixState = MUSIC_STEREO;
        }
    }else{//mono state
        if(cntx->currMixState != MUSIC_MONO){
            APPS_LOG_MSGID_I("app_music_activity_proc set to mono", 0);
            am_dynamic_change_channel(AUDIO_CHANNEL_SELECTION_MONO);
            cntx->currMixState = MUSIC_MONO;
        }
    }
}
#endif
