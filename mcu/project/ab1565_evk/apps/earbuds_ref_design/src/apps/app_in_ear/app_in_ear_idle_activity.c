/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#ifdef MTK_IN_EAR_FEATURE_ENABLE

#include "FreeRTOS.h"
#include "app_in_ear_idle_activity.h"
#include "app_in_ear_utils.h"
#include "apps_events_interaction_event.h"

#include "apps_events_key_event.h"

#include "apps_debug.h"
#include "apps_events_event_group.h"

#if 0
#include "apps_events_ap_event.h"
#endif

#include "bt_sink_srv.h"
#include "bt_role_handover.h"
#include "bt_aws_mce_report.h"

#include "apps_config_key_remapper.h"
#include "nvdm.h"

static apps_in_ear_local_context_t* g_local_context = NULL;


static apps_in_ear_local_context_t* _get_app_context()
{
    return g_local_context;
}


bool app_in_ear_is_out()
{
    apps_in_ear_local_context_t* ctx = _get_app_context();
    return (!ctx->isInEar);
}

/* only for debug */
static bool anlogInEar(bool set, bool flag)
{
    static bool isInEar = false;

    if(set){
        isInEar = flag;
    }
    
    return isInEar;
}


static uint8_t _role_handover_service_get_data_len_callback(const bt_bd_addr_t *addr)
{
    uint8_t len = sizeof(apps_in_ear_local_context_t);
    APPS_LOG_MSGID_I("app_in_ear get_data_len_callback: %d", 1, len);
    return len;

}


static bt_status_t _role_handover_get_data_callback(const bt_bd_addr_t *addr, void * data)
{
    apps_in_ear_local_context_t* ctx = _get_app_context();
    if (NULL == ctx)
    {
        APPS_LOG_MSGID_I("app_in_ear local context is NULL", 0);
        return BT_STATUS_FAIL;
    }

    apps_in_ear_local_context_t *pdata = (apps_in_ear_local_context_t*)data;
    memcpy(pdata, ctx, sizeof(apps_in_ear_local_context_t));
    APPS_LOG_MSGID_I("app_in_ear get_data_callback", 0);
    return BT_STATUS_SUCCESS;
}


static bt_status_t _role_handover_update_data_callback(bt_role_handover_update_info_t *info)
{
    bool needUpdate = false;
    app_in_ear_sta_t currentStaTemp;

    apps_in_ear_local_context_t* ctx = _get_app_context();

    APPS_LOG_MSGID_I("app_in_ear recv, role: %d", 1, info->role);
    if (NULL == ctx)
    {
        APPS_LOG_MSGID_I("app_in_ear local context is NULL", 0);
        return BT_STATUS_FAIL;
    }


    if (info->role == BT_AWS_MCE_ROLE_PARTNER)
    {
        apps_in_ear_local_context_t *pdata = (apps_in_ear_local_context_t *)info->data;

        APPS_LOG_MSGID_I("app_in_ear old agent: -inEar:%d, partnerIn:%d, -ps:%d, -cs:%d", 4,
                         pdata->isInEar, pdata->isPartnerInEar, pdata->previousSta, pdata->currentSta);
        APPS_LOG_MSGID_I("app_in_ear new agent: -inEar:%d, partnerIn:%d, -ps:%d, -cs:%d", 4,
                         ctx->isInEar, ctx->isPartnerInEar, ctx->previousSta, ctx->currentSta);

        ctx->isPartnerInEar = pdata->isInEar;
        ctx->previousSta = pdata->previousSta;
        ctx->currentSta = pdata->currentSta;
        ctx->eventDone = pdata->eventDone;
        ctx->eventOutEnable = pdata->eventOutEnable;
        ctx->rhoEnable = pdata->rhoEnable;

        /* check the old partner in-ear status */
        if (pdata->isPartnerInEar != ctx->isInEar)
        {
            needUpdate = true;
        }

        /* check the old agent in-ear status */
        if (pdata->isInEar &&
            pdata->currentSta != APP_IN_EAR_STA_AIN_POUT && 
            pdata->currentSta != APP_IN_EAR_STA_BOTH_IN)
        {
            needUpdate = true;
        }

        if (!pdata->isInEar &&
            pdata->currentSta != APP_IN_EAR_STA_AOUT_PIN &&
            pdata->currentSta != APP_IN_EAR_STA_BOTH_OUT)
        {
            needUpdate = true;
        }

        if (needUpdate)
        {
            APPS_LOG_MSGID_I("app_in_ear new agent do state update", 0);
            APPS_LOG_MSGID_I("app_in_ear old agent: -inEar:%d, partnerIn:%d, -ps:%d, -cs:%d", 4,
                             pdata->isInEar, pdata->isPartnerInEar, pdata->previousSta, pdata->currentSta);
            APPS_LOG_MSGID_I("app_in_ear new agent: -inEar:%d, partnerIn:%d, -ps:%d, -cs:%d", 4,
                             ctx->isInEar, ctx->isPartnerInEar, ctx->previousSta, ctx->currentSta);

            if (ctx->isInEar)
            {
                /* new agent in */
                if (ctx->isPartnerInEar)
                {
                    currentStaTemp = APP_IN_EAR_STA_BOTH_IN;
                }
                else
                {
                    currentStaTemp = APP_IN_EAR_STA_AIN_POUT;
                }
            }
            else
            {
                /* new agent out */
                if (ctx->isPartnerInEar)
                {
                    currentStaTemp = APP_IN_EAR_STA_AOUT_PIN;
                }
                else
                {
                    currentStaTemp = APP_IN_EAR_STA_BOTH_OUT;
                }
            }

            ctx->previousSta = ctx->currentSta;
            ctx->currentSta = currentStaTemp;
            ctx->eventDone = false;
        }
    }

    return BT_STATUS_SUCCESS;
}


static void _role_handover_service_status_callback(
             const bt_bd_addr_t *addr,
             bt_aws_mce_role_t role,
             bt_role_handover_event_t event,
             bt_status_t status)
{

    apps_in_ear_local_context_t* ctx = _get_app_context();
    if (NULL == ctx)
    {
        return;
    }

    if (BT_ROLE_HANDOVER_COMPLETE_IND == event)
    {
        ctx->isInRho = false;

        if (role == BT_AWS_MCE_ROLE_PARTNER && BT_STATUS_SUCCESS == status && !ctx->eventDone)
        {
            if((ctx->currentSta == APP_IN_EAR_STA_AOUT_PIN) && ctx->isInEar && !ctx->isPartnerInEar){
                ctx->currentSta = APP_IN_EAR_STA_AIN_POUT;
            }

            app_in_ear_sta_info_t* sta_info = (app_in_ear_sta_info_t*)pvPortMalloc(sizeof(sta_info));
            if(sta_info){
                sta_info->previous = ctx->previousSta;
                sta_info->current = ctx->currentSta;
#ifdef IN_EAR_DEBUG
                printStaToSysUART(ctx);
#endif
                ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST,
                                    EVENT_GROUP_UI_SHELL_APP_INTERACTION, APPS_EVENTS_INTERACTION_IN_EAR_UPDATE_STA,
                                    (void*)sta_info, sizeof(app_in_ear_sta_info_t),
                                    NULL, 0);

                APPS_LOG_MSGID_I("app_in_ear send interaction, present sta: %d, previous sta: %d", 2,
                             ctx->currentSta, ctx->previousSta);
            }

        }

        if (role == BT_AWS_MCE_ROLE_AGENT && BT_STATUS_SUCCESS == status)
        {
            sendAwsData(ctx, APP_IN_EAR_EVENT_UPDATE_STA);
        }

        if(BT_STATUS_FAIL == status){
            APPS_LOG_MSGID_E("app_in_ear rho failed.", 0);
        }
    }
}


static bool _proc_ui_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data,size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true

    switch (event_id)
    {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE:
        {
            APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc create current activity : 0x%x", 1, (uint32_t)self);
            self->local_context = pvPortMalloc(sizeof(apps_in_ear_local_context_t));
            if (self->local_context)
            {
                g_local_context = self->local_context;

                apps_in_ear_local_context_t* ctx = (apps_in_ear_local_context_t*)self->local_context;
                ctx->isInEar = anlogInEar(false, false); /* TODO */

#ifdef MTK_AWS_MCE_ENABLE
                if (ctx->isInEar)
                {
                    ctx->currentSta = APP_IN_EAR_STA_AIN_POUT;
                }
                else
                {
                    ctx->currentSta = APP_IN_EAR_STA_BOTH_OUT;
                }
                ctx->isPartnerInEar = false;
#else
                if (ctx->isInEar)
                {
                    ctx->currentSta = APP_IN_EAR_STA_BOTH_IN;
                }
                else
                {
                    ctx->currentSta = APP_IN_EAR_STA_BOTH_OUT;
                }
#endif
                ctx->previousSta = ctx->currentSta;
                ctx->isInRho = false;
                ctx->eventDone = true;
                ctx->eventOutEnable = true;
                ctx->rhoEnable = true;


#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
                bt_role_handover_callbacks_t role_callbacks = {
                    NULL,
                    _role_handover_service_get_data_len_callback,
                    _role_handover_get_data_callback,
                    _role_handover_update_data_callback,
                    _role_handover_service_status_callback
                };
                bt_role_handover_register_callbacks(BT_ROLE_HANDOVER_MODULE_IN_EAR, &role_callbacks);
#endif
            }
            break;
        }

        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY:
        {
            APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc destroy", 0);
            if (self->local_context)
            {
                vPortFree(self->local_context);
            }
            break;
        }

        case EVENT_ID_SHELL_SYSTEM_ON_RESUME:
        {
            APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc resume", 0);
            break;
        }

        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE:
        {
            APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc pause", 0);
            break;
        }

        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH:
        {
            APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc refresh", 0);
            break;
        }

        case EVENT_ID_SHELL_SYSTEM_ON_RESULT:
        {
            APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc result", 0);
            break;
        }

        default:
            break;
    }
    return ret;
}


static bool _proc_bt_sink_event(
             struct _ui_shell_activity *self,
             uint32_t event_id,
             void *extra_data,
             size_t data_len)
{
    apps_in_ear_local_context_t* ctx = (apps_in_ear_local_context_t*)self->local_context;
#ifdef MTK_AWS_MCE_ENABLE
    bt_sink_srv_event_param_t *event = (bt_sink_srv_event_param_t *)extra_data;
#endif

    if (event_id == BT_SINK_SRV_EVENT_STATE_CHANGE)
    {
        bt_sink_srv_state_change_t *param = (bt_sink_srv_state_change_t*)extra_data;

        /* unmute all earphone while music paused. */
        if ((param->previous == BT_SINK_SRV_STATE_STREAMING) && 
            (param->current != BT_SINK_SRV_STATE_STREAMING))
        {
            APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc try unmute all earphones.", 0);
            unMuteAllEarphones();
        }

        /*
        if ((param->previous >= BT_SINK_SRV_STATE_CONNECTED) &&
           (param->current < BT_SINK_SRV_STATE_CONNECTED))
        {
            ctx->isPartnerInEar = false;
            checkAndHandleStatusChanged(ctx);
        }
        */
    }

#ifdef MTK_AWS_MCE_ENABLE
    /* for agent, when partner disconnected, set partner status to "out" */
    else if (event_id == BT_SINK_SRV_EVENT_AWS_MCE_STATE_CHANGE)
    {
        bt_sink_srv_aws_mce_state_changed_ind_t *state_change = &(event->aws_state_change);
        if (state_change->aws_state == BT_AWS_MCE_AGENT_STATE_INACTIVE)
        {
            ctx->isPartnerInEar = false;
            checkAndHandleStatusChanged(ctx);
        }
    }
    /* for partner, when connected to agent, report in-ear status to agent */
    else if (event_id == BT_SINK_SRV_EVENT_PROFILE_CONNECTION_UPDATE)
    {
        bt_sink_srv_profile_connection_state_update_t *connect_info = (bt_sink_srv_profile_connection_state_update_t*)extra_data;
        if ((BT_SINK_SRV_PROFILE_AWS == connect_info->profile) &&
            (bt_connection_manager_device_local_info_get_aws_role() == BT_AWS_MCE_ROLE_PARTNER) &&
            (BT_SINK_SRV_PROFILE_CONNECTION_STATE_CONNECTED == connect_info->state))
        {
            APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc report status to agent, in-ear: %d", 1, ctx->isInEar);
            sendAwsData(ctx, APP_IN_EAR_EVENT_UPDATE_STA);
        }
     }
#endif

    return false;
}


static bool _proc_apps_internal_events(
             struct _ui_shell_activity *self,
             uint32_t event_id,
             void *extra_data,
             size_t data_len)
{
    bool ret = false;
    apps_in_ear_local_context_t* ctx = (apps_in_ear_local_context_t*)self->local_context;

    APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc internal event action: %d", 1, event_id);

    if(APPS_EVENTS_INTERACTION_RHO_END == event_id)
    {
        ctx->isInRho = false;
    }

    if (APPS_EVENTS_INTERACTION_UPDATE_IN_EAR_STA_EFFECT == event_id)
    {
        /* 
         * extra_data is not NULL means the event come from driver callback.
         * extra_data is NULL means the the status already recorded, just need to handle it.
         */
        if (extra_data != NULL)
        {
            bool* pstatus = (bool*)extra_data;
            ctx->isInEar = *pstatus;
        }

        if(ctx->isInRho){
            APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc isInRho: %d", 1, ctx->isInRho);
            return false;
        }

#ifdef MTK_ANC_ENABLE
        /* allways disable self's anc at first when taken out of ear. */
        if(!ctx->isInEar){
            //ctrlANC(false);
        }
#endif

        /* for agent, check and update status. for partner, report status to agent */
#ifdef MTK_AWS_MCE_ENABLE
        if (bt_connection_manager_device_local_info_get_aws_role() == BT_AWS_MCE_ROLE_PARTNER)
        {
            APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc report status to agent, in-ear: %d", 1, ctx->isInEar);
            sendAwsData(ctx, APP_IN_EAR_EVENT_UPDATE_STA);
        }
        else
#endif
        {
            checkAndHandleStatusChanged(ctx);
        }
    }
    return ret;
}


#ifdef MTK_AWS_MCE_ENABLE
static bool _proc_aws_data_event(
             ui_shell_activity_t *self,
             uint32_t event_id,
             void *extra_data,
             size_t data_len)
{
    bool ret = false;
    bt_aws_mce_report_info_t *aws_data_ind = (bt_aws_mce_report_info_t *)extra_data;
    APPS_LOG_MSGID_I("app_in_ear_idle_activity receive aws data, module_id: %d.", 1, aws_data_ind->module_id);

    apps_in_ear_local_context_t* ctx = (apps_in_ear_local_context_t*)self->local_context;

    if(aws_data_ind->module_id != BT_AWS_MCE_REPORT_MODULE_IN_EAR)
    {
        return false;
    }

    app_in_ear_aws_data_t data;
    memcpy(&data, aws_data_ind->param, sizeof(app_in_ear_aws_data_t));
    switch(data.event){
        case APP_IN_EAR_EVENT_UPDATE_STA:
            if(ctx->isInRho){
                APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc isInRho: %d", 1, ctx->isInRho);
                return false;
            }

            if (bt_connection_manager_device_local_info_get_aws_role() == BT_AWS_MCE_ROLE_AGENT)
            {
                APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc recv partner's status, in-ear: %d.", 1, data.isInEar);
                ctx->isPartnerInEar = data.isInEar;
                checkAndHandleStatusChanged(ctx);
            }
            break;
#ifdef MTK_ANC_ENABLE
        case APP_IN_EAR_EVENT_ANC_ENABLE:
            //ctrlANC(true);
            break;
        case APP_IN_EAR_EVENT_ANC_DISABLE:
            //ctrlANC(false);
            break;
#endif
    }

    /* 
     * this may never happen. in this case, old partner first send in-ear status, and then
     * the old agent start rho. when rho finished, old partner's data received by new partner.
     */
    if ((data.event == APP_IN_EAR_EVENT_UPDATE_STA) &&
        (bt_connection_manager_device_local_info_get_aws_role() == BT_AWS_MCE_ROLE_PARTNER))
    {
        APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc new partner received old partner's status, in-ear: %d",
                         1, data.isInEar);
    }
    return ret;
}
#endif

bool app_in_ear_activity_proc(
      struct _ui_shell_activity *self,
      uint32_t event_group,
      uint32_t event_id,
      void *extra_data,
      size_t data_len)
{
    bool ret = false;
    APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc event_group: 0x%x, event_id: 0x%x", 2, event_group, event_id);
    switch (event_group)
    {
        case EVENT_GROUP_UI_SHELL_SYSTEM:
            ret = _proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;
        
        case EVENT_GROUP_UI_SHELL_BT_SINK: 
            ret = _proc_bt_sink_event(self, event_id, extra_data, data_len);
            break;

        case EVENT_GROUP_UI_SHELL_APP_INTERACTION:
            ret = _proc_apps_internal_events(self, event_id, extra_data, data_len);
            break;

#if 0
        case EVENT_GROUP_UI_SHELL_APK_CONFIG:{
            ret = _proc_apk_config_event(self, event_id, extra_data, data_len);
            break;
        }
#endif

        /* only for debug */
        /*
        case EVENT_GROUP_UI_SHELL_KEY: {
            apps_config_key_action_t action;
            uint8_t key_id;
            airo_key_event_t key_event;
            app_event_key_event_decode(&key_id, &key_event, event_id);

            if (extra_data) {
                action = *(uint16_t *)extra_data;
            } else {
                action = apps_config_key_event_remapper_map_action(key_id, key_event);
            }
            
            APPS_LOG_MSGID_I("app_in_ear_idle_activity_proc key event action: %d", 1, action);
            if(action == KEY_ANLOG_IN_EAR)
            {
                bool* isInEar = (bool*)pvPortMalloc(sizeof(bool));
                *isInEar = anlogInEar(false, false);
                if(*isInEar)
                {
                    anlogInEar(true, false);
                    *isInEar = false;
                }
                else
                {
                    anlogInEar(true, true);
                    *isInEar = true;
                }
                ui_shell_remove_event(EVENT_GROUP_UI_SHELL_APP_INTERACTION, APPS_EVENTS_INTERACTION_UPDATE_IN_EAR_STA_EFFECT);
                ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                                                        APPS_EVENTS_INTERACTION_UPDATE_IN_EAR_STA_EFFECT, (void*)isInEar, sizeof(bool),
                                                        NULL, 0);
            }
            break;
        }
    */
        
#if defined(MTK_AWS_MCE_ENABLE)
        case EVENT_GROUP_UI_SHELL_AWS_DATA:
            ret = _proc_aws_data_event(self, event_id, extra_data, data_len);
            break;
#endif
        default:
            break;
    }
    return ret;
    
}

#endif /*MTK_IN_EAR_FEATURE_ENABLE*/
