/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifdef MTK_IN_EAR_FEATURE_ENABLE

#include "FreeRTOS.h"
#include "app_in_ear_idle_activity.h"
#include "app_in_ear_utils.h"
#include "apps_events_interaction_event.h"
#if 0
#include "apps_events_ap_event.h"
#ifdef MTK_ANC_ENABLE
#include "anc_control.h"
#endif
#endif
#include "apps_debug.h"
#include "apps_events_event_group.h"
#include "bt_sink_srv.h"
#include "bt_aws_mce_report.h"
#include "apps_config_key_remapper.h"

bool checkAndHandleStatusChanged(apps_in_ear_local_context_t* ctx)
{
    app_in_ear_sta_t currentStaTemp;

#ifdef MTK_AWS_MCE_ENABLE
    if (ctx->isInEar)
    {
        /* agent in */
        if (ctx->isPartnerInEar)
        {
            currentStaTemp = APP_IN_EAR_STA_BOTH_IN;
        }
        else
        {
            currentStaTemp = APP_IN_EAR_STA_AIN_POUT;
        }
    }else{
        /* agent out */
        if (ctx->isPartnerInEar)
        {
            currentStaTemp = APP_IN_EAR_STA_AOUT_PIN;
        }
        else
        {
            currentStaTemp = APP_IN_EAR_STA_BOTH_OUT;
        }
    }
#else
    if (ctx->isInEar)
    {
        currentStaTemp = APP_IN_EAR_STA_BOTH_IN;
    }
    else
    {
        currentStaTemp = APP_IN_EAR_STA_BOTH_OUT;
    }
#endif

    /* need to update state */
    if (ctx->currentSta != currentStaTemp)
    {
        ctx->previousSta = ctx->currentSta;
        ctx->currentSta = currentStaTemp;
        APPS_LOG_MSGID_I("app_in_ear status changed, present sta: %d, previous sta: %d", 2,
                                 ctx->currentSta, ctx->previousSta);

        if (ctx->isInRho)
        {
            ctx->eventDone = false;
        }
        else
        {
            ctx->eventDone = true;

            if (ctx->rhoEnable && ctx->currentSta == APP_IN_EAR_STA_AOUT_PIN)
            {
                ctx->isInRho = true;
                /* notify the new agent do actions. */
                ctx->eventDone = false;
                ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                                    APPS_EVENTS_INTERACTION_TRIGGER_RHO, NULL, 0,
                                    NULL, 0);
            }
            else
            {
                apps_events_report_in_ear(ctx->isInEar, ctx->isPartnerInEar);
                app_in_ear_sta_info_t* sta_info = (app_in_ear_sta_info_t*)pvPortMalloc(sizeof(sta_info));
                if(sta_info){
                    sta_info->previous = ctx->previousSta;
                    sta_info->current = ctx->currentSta;
#ifdef IN_EAR_DEBUG
                    printStaToSysUART(ctx);
#endif
                    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST,
                                    EVENT_GROUP_UI_SHELL_APP_INTERACTION, APPS_EVENTS_INTERACTION_IN_EAR_UPDATE_STA,
                                    (void*)sta_info, sizeof(app_in_ear_sta_info_t),
                                    NULL, 0);
                    APPS_LOG_MSGID_I("app_in_ear send interaction, present sta: %d, previous sta: %d", 2,
                                    ctx->currentSta, ctx->previousSta);
                }

            }
        }
    }

    return true;
}

void unMuteAllEarphones()
{
    
}

void sendAwsData(apps_in_ear_local_context_t * ctx, app_in_ear_event_t event)
{
    app_in_ear_aws_data_t data;
    data.event = event;
    data.isInEar = ctx->isInEar;
    
    bt_aws_mce_report_info_t aws_data;
    aws_data.module_id = BT_AWS_MCE_REPORT_MODULE_IN_EAR;
    aws_data.is_sync = false;
    aws_data.sync_time = 0;
    aws_data.param_len = sizeof(app_in_ear_aws_data_t);
    aws_data.param = (void*)&data;
    bt_status_t bst = bt_aws_mce_report_send_event(&aws_data);
    if(bst == BT_STATUS_FAIL)
    {
        APPS_LOG_MSGID_I("app_in_ear send aws data failed.", 0);
    }
    APPS_LOG_MSGID_I("app_in_ear send aws data, event:%d, isInEar:%d, module_id: %d", 3, data.event, data.isInEar, aws_data.module_id);
    
}


#ifdef IN_EAR_DEBUG
static char* sta0 = "BOTH_IN ";
static char* sta1 = "BOTH_OUT ";
static char* sta2 = "A_IN_P_OUT ";
static char* sta3 = "A_OUT_P_IN ";
void printStaToSysUART(apps_in_ear_local_context_t* ctx)
{
    atci_response_t response = {{0}, 0, ATCI_RESPONSE_FLAG_APPEND_OK};
    const char* p_sta = NULL;

    switch(ctx->currentSta){
        case APP_IN_EAR_STA_BOTH_IN:
            p_sta = sta0;
            break;
        case APP_IN_EAR_STA_BOTH_OUT:
            p_sta = sta1;
            break;
        case APP_IN_EAR_STA_AIN_POUT:
            p_sta = sta2;
            break;
        case APP_IN_EAR_STA_AOUT_PIN:
            p_sta = sta3;
            break;
        default:
            p_sta = "IN_EAR_UNKNOWN";
    }

    memcpy(response.response_buf, p_sta, strlen(p_sta));
    response.response_len = strlen(p_sta);
    atci_send_response(&response);
}
#endif

#if 0
static void app_in_ear_anc_control_callback(anc_control_event_t event_id, anc_control_result_t result)
{
    APPS_LOG_MSGID_I("app_in_ear, anc control callback %d ret : %d", 2,
                event_id, result);
}

#ifdef MTK_ANC_ENABLE
static bool g_anc_init_flag = false;
void ctrlANC(bool enable)
{
    if(!g_anc_init_flag){
        bool ret = apps_anc_control_register(app_in_ear_anc_control_callback);
        if(ret){
            g_anc_init_flag = true;
        }else{
            APPS_LOG_MSGID_I("app_in_ear apps_anc_control_register return failed.", 0);
        }
    }

    APPS_LOG_MSGID_I("app_in_ear anc control request:%d.", 1, enable);

    if(enable){
        apps_anc_control_set_resume(app_in_ear_anc_control_callback, false);
    }else{
        apps_anc_control_set_suspend(app_in_ear_anc_control_callback);
    }
}

void checkAndCtrlANC(apps_in_ear_local_context_t* ctx)
{
    if(ctx->currentSta != APP_IN_EAR_STA_BOTH_IN){
        ctrlANC(false);
        sendAwsData(ctx, APP_IN_EAR_EVENT_ANC_DISABLE);
    }else{
        ctrlANC(true);
        sendAwsData(ctx, APP_IN_EAR_EVENT_ANC_ENABLE);
    }
}
#endif

#endif

#endif
