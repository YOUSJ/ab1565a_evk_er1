/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include <stdint.h>
#include "FreeRTOS.h"
#include "portable.h"
#include "syslog.h"
#include "bt_fast_pair.h"
#include "apps_events_event_group.h"
#include "apps_config_features_dynamic_setting.h"
#include "app_fast_pair.h"
#include "bt_sink_srv.h"
#include "ui_shell_manager.h"
#include "bt_app_common.h"
#include "apps_debug.h"
#include "bt_init.h"
#include "bt_gap_le.h"
#include "bt_callback_manager.h"
#include "apps_events_bt_event.h"
#include "multi_ble_adv_manager.h"
#include "nvkey.h"
#include "nvdm.h"
#include "nvkey_id_list.h"
#ifdef MTK_AWS_MCE_ENABLE
#include "bt_aws_mce_report.h"
#include "bt_sink_srv_ami.h"
#endif
#include "app_fm_activity.h"
#include "apps_aws_sync_event.h"
#ifdef MTK_SMART_CHARGER_ENABLE
#include "app_smcharger_idle_activity.h"
#else
#include "app_battery_transient_activity.h"
#endif
#include "apps_events_battery_event.h"
#include "battery_management.h"
#include "nvdm_id_list.h"
#include "nvkey_id_list.h"

#define LOG_TAG     "[app_fast_pair] "

#define APP_FAST_PAIR_DISCOVER_MODE_ADV_INTERVAL    (0xA0)  /* 100ms */
#define APP_FAST_PAIR_NONDISCOVER_MODE_ADV_INTERVAL (0x190) /* 250ms */

#define APP_FAST_PAIR_MODEL_SET_MAX_NUMBER          (10)

#define FAST_PAIR_PRIVATE_PROTECTED
#ifdef FAST_PAIR_PRIVATE_PROTECTED
/* This key is used for fast pair related nvdm encrypt and decrypt, it's must as same as the config tool set.
 * The config tool while use the same key to encrypt fast pair related nvdm, because of the same key,
 * So the FW can decrypt it.
*/
const char app_fast_pair_nvdm_key[32] = {0x12, 0x34, 0x56, 0x78, 0x12, 0x34, 0x56, 0x78, \
                                         0x12, 0x34, 0x56, 0x78, 0x12, 0x34, 0x56, 0x78, \
                                         0x12, 0x34, 0x56, 0x78, 0x12, 0x34, 0x56, 0x78, \
                                         0x12, 0x34, 0x56, 0x78, 0x12, 0x34, 0x56, 0x78};
#endif

#ifdef MTK_AWS_MCE_ENABLE
#define APP_FAST_PAIR_COMPONENT_NUM                 (3)
#define APP_FAST_PAIR_AWS_CONTEXT_SYNC_ID           (0xE0)
#else
#define APP_FAST_PAIR_COMPONENT_NUM                 (1)
#endif

/* If Fast pair related nvkey is invalid, will use below config. */
#define APP_FAST_PAIR_DEFAULT_MAX_ACCOUNT           (5)
#define APP_FAST_PAIR_DEFAULT_TX_POWER_LEVEL        (0xEC)
#define APP_FAST_PAIR_AIROHA_TEST_MODEL_ID          (0x010100)
#ifdef MTK_AWS_MCE_ENABLE
#define APP_FAST_PAIR_PERSONALIZED_NAME             "Airoha_fast_pair_earbuds"
#else
#define APP_FAST_PAIR_PERSONALIZED_NAME             "Airoha_fast_pair_headset"
#endif
#define APP_FAST_PAIR_AIROHA_TEST_PRIVATE_KEY       {0x3C, 0x2D, 0xAB, 0x1F, 0x22, 0x04, 0xCF, 0xA9, \
                                                     0xD3, 0xB6, 0x5B, 0xE9, 0xBC, 0x0A, 0x45, 0x7C, \
                                                     0x00, 0x5C, 0xE9, 0xE4, 0xC5, 0x72, 0xF8, 0x09, \
                                                     0x55, 0xFF, 0xC8, 0x2C, 0x2B, 0x2D, 0xB1, 0xFD}

#define APP_FAST_PAIR_COMPONENT_LEFT                (0)
#define APP_FAST_PAIR_COMPONENT_RIGHT               (1)
#define APP_FAST_PAIR_COMPONENT_CASE                (2)

typedef struct {
    uint8_t     fps_enable;
    uint8_t     max_account;
    uint8_t     seleceted_set;
    uint8_t     tx_power_available;
    int8_t      tx_power_level;
    uint8_t     component_num;
    char        personalized_name[128];
    uint8_t     reserved[64];
} PACKED app_fast_pair_nvkey_t;

typedef struct{
    uint32_t    model_id;
    uint8_t     private_key[32];
} PACKED app_fast_pair_set_t;

typedef struct{
    app_fast_pair_set_t sets[APP_FAST_PAIR_MODEL_SET_MAX_NUMBER]; //10 as defalut.
    uint32_t            CRC;
} PACKED app_fast_pair_protected_t;

#define APP_FAST_PAIR_PROTECTED_CRY_LEN             (sizeof(app_fast_pair_protected_t))

typedef struct {
    bool                            is_adv_on;
    bool                            is_vis_on;
    uint8_t                         adv_stopping_flag;
    app_fast_pair_protected_t       protect_sets;
    app_fast_pair_nvkey_t           nvkey;
    bt_fast_pair_battery_t          battery;
    bt_fast_pair_account_key_list_t *account_key_list;
} app_fast_pair_local_context_t;

static app_fast_pair_local_context_t    *g_app_fast_pair_local_context = NULL;

static bool app_proc_fast_pair_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len);
static bool app_fast_pair_init(ui_shell_activity_t *self);
static void app_fast_pair_trigger_advertising(app_fast_pair_local_context_t *local_context);

#ifdef FAST_PAIR_PRIVATE_PROTECTED
extern int  nvkey_decrypt(unsigned char *p_key, unsigned char *p_in, int len, unsigned char *p_out);
#endif
static int8_t app_fast_pair_load_configure_info(app_fast_pair_local_context_t* context)
{
    nvkey_status_t status;
    if (NULL == context) {
        APPS_LOG_MSGID_E(LOG_TAG" wrongly parameter : 0x%x", 1, context);
        return -1;
    }
    uint32_t nvkey_id = NVKEYID_APP_FAST_PAIR_CONFIGURE;
    uint32_t size = sizeof(app_fast_pair_nvkey_t);
    if (NVKEY_STATUS_OK != (status = nvkey_read_data(nvkey_id, (uint8_t *)&(context->nvkey), &size))) {
        APPS_LOG_MSGID_E(LOG_TAG" load configure data failed : %d", 1, status);
        return -2;
    }
    APPS_LOG_MSGID_I(LOG_TAG" load configure data size %d", 1, size);
    APPS_LOG_DUMP_I(LOG_TAG" load configure data", &(context->nvkey), size);
    uint32_t protect_id = NVKEYID_APP_FAST_PAIR_PROTECTED;
    uint8_t  cry[APP_FAST_PAIR_PROTECTED_CRY_LEN] = {0};
    size = APP_FAST_PAIR_PROTECTED_CRY_LEN;
    if (NVKEY_STATUS_OK != (status = nvkey_read_data(protect_id, cry, &size))) {
        APPS_LOG_MSGID_E(LOG_TAG" load protected data failed : %d", 1, status);
        return -2;
    }
    APPS_LOG_MSGID_I(LOG_TAG" load protected data size %d", 1, size);
    APPS_LOG_DUMP_I(LOG_TAG" load protected data-before cry", cry, size);
 #ifdef FAST_PAIR_PRIVATE_PROTECTED
    int ret = nvkey_decrypt((unsigned char *)app_fast_pair_nvdm_key, cry, APP_FAST_PAIR_PROTECTED_CRY_LEN, (unsigned char *)&(context->protect_sets));
    APPS_LOG_MSGID_I(LOG_TAG" load configure decry ret %d", 1, ret);
    APPS_LOG_DUMP_I(LOG_TAG" load protected data-after cry", &(context->protect_sets), sizeof(app_fast_pair_protected_t));
 #else
    memcpy(&(context->protect_sets), cry, sizeof(app_fast_pair_protected_t));
 #endif
    return 0;
}

static void app_fast_pair_load_account_key(app_fast_pair_local_context_t* context)
{
    nvdm_status_t status;
    uint8_t saved_max_number = context->nvkey.max_account;
    uint32_t size = sizeof(bt_fast_pair_account_key_list_t) +
        sizeof(bt_fast_pair_account_key_t) * saved_max_number - sizeof(bt_fast_pair_account_key_t);
    memset(context->account_key_list, 0, size);
    status = nvdm_read_data_item(NVDM_GROUP_FAST_PAIR_APP, NVDM_GROUP_FAST_PAIR_ACCOUNT_KEY, (uint8_t *)(context->account_key_list), &size);
    if (NVDM_STATUS_OK != status) {
        context->account_key_list->max_key_number = context->nvkey.max_account;
        APPS_LOG_MSGID_E(LOG_TAG" account key load failed : %d", 1, status);
        return;
    }
    APPS_LOG_MSGID_I(LOG_TAG" account key load num %d, need num %d", 2, context->account_key_list->max_key_number, saved_max_number);
}

static void app_fast_pair_store_account_key(bt_fast_pair_account_key_list_t *account_key)
{
    nvdm_status_t status;
    uint32_t size = sizeof(bt_fast_pair_account_key_list_t) +
        sizeof(bt_fast_pair_account_key_t) * account_key->max_key_number - sizeof(bt_fast_pair_account_key_t);

    APPS_LOG_MSGID_E(LOG_TAG" Account key write nvdm size: %d", 1, size);
    status = nvdm_write_data_item(NVDM_GROUP_FAST_PAIR_APP, NVDM_GROUP_FAST_PAIR_ACCOUNT_KEY, NVDM_DATA_ITEM_TYPE_RAW_DATA,
        (uint8_t *)(account_key), size);
    if (NVDM_STATUS_OK != status) {
        APPS_LOG_MSGID_E(LOG_TAG" Account key write nvdm failed : %x", 1, status);
        return;
    }
}

static uint32_t app_fast_pair_get_adv_data(multi_ble_adv_info_t *adv_data)
{
    bt_fast_pair_advertising_data_t type;
    bt_fast_pair_status_t ret;
    uint16_t interval;

    if (NULL == g_app_fast_pair_local_context) {
        return 0;
    }
    if (g_app_fast_pair_local_context->is_vis_on) {
        type = BT_FAST_PAIR_ADVERTISING_DATA_MODEL_ID;
        interval = APP_FAST_PAIR_DISCOVER_MODE_ADV_INTERVAL;
    } else {
        //type = BT_FAST_PAIR_ADVERTISING_DATA_ACCOUNT;
        type = BT_FAST_PAIR_ADVERTISING_DATA_ACCOUNT_AND_BATTERY;
        interval = APP_FAST_PAIR_NONDISCOVER_MODE_ADV_INTERVAL;
    }

    adv_data->adv_param->advertising_interval_min = interval;
    /* Interval should be no larger than 100ms when discoverable */
    adv_data->adv_param->advertising_interval_max = interval;
    adv_data->adv_param->advertising_type = BT_HCI_ADV_TYPE_CONNECTABLE_UNDIRECTED;
    adv_data->adv_param->own_address_type = BT_ADDR_RANDOM_IDENTITY;
    adv_data->adv_param->advertising_channel_map = 7;
    adv_data->adv_param->advertising_filter_policy = 0;
    adv_data->adv_data->advertising_data_length = sizeof(adv_data->adv_data->advertising_data);

    ret = bt_fast_pair_get_advertising_data(adv_data->adv_data->advertising_data, &adv_data->adv_data->advertising_data_length, type);
    if (ret != BT_FAST_PAIR_STATUS_SUCCESS) {
        APPS_LOG_MSGID_E(LOG_TAG" adv get data failed %d", 1, ret);
        return 0;
    }
    /* The content of scan_rsp is the same as adv_data */
    //memcpy(adv_data->scan_rsp, adv_data->adv_data, sizeof(bt_hci_cmd_le_set_advertising_data_t));
    return MULTI_BLE_ADV_NEED_GEN_SCAN_RSP;
    // return 0;
}

static void app_fast_pair_trigger_advertising(app_fast_pair_local_context_t *local_context)
{
    /* For re-start ADV if connected by unexpected devices */
    APPS_LOG_MSGID_I(LOG_TAG" Trigger advertising visibility status: %d", 1, local_context->is_vis_on);

    if (apps_config_features_is_mp_test_mode()) {
        return;
    }
#if 0
    if (bt_sink_srv_get_connected_devices_number() >= 1) {
        /* Once connected one ACL link, then don't start ADV to avoid
         * conflict with other BLE module. */
        APPS_LOG_MSGID_W("[APP_FAST_PAIR]ACL link already exited: %d", 1, is_vis_on);
        return;
    }
#endif
    multi_ble_adv_manager_remove_ble_adv(app_fast_pair_get_adv_data);
    multi_ble_adv_manager_add_ble_adv(app_fast_pair_get_adv_data);
    multi_ble_adv_manager_notify_ble_adv_data_changed();
}

static void app_fast_pair_update_battery(app_fast_pair_local_context_t* context, bt_fast_pair_battery_t *battery)
{
    if (NULL == context || NULL == battery) {
        return;
    }
    bool need_update = false;
    bt_fast_pair_battery_t *cur_cntx = &(context->battery);

    if (cur_cntx->ui_show != battery->ui_show ||
        cur_cntx->battery[APP_FAST_PAIR_COMPONENT_RIGHT].charging != battery->battery[APP_FAST_PAIR_COMPONENT_RIGHT].charging ||
        cur_cntx->battery[APP_FAST_PAIR_COMPONENT_LEFT].charging != battery->battery[APP_FAST_PAIR_COMPONENT_LEFT].charging ||
        cur_cntx->battery[APP_FAST_PAIR_COMPONENT_CASE].charging != battery->battery[APP_FAST_PAIR_COMPONENT_CASE].charging ||
        cur_cntx->battery[APP_FAST_PAIR_COMPONENT_RIGHT].battery_value != battery->battery[APP_FAST_PAIR_COMPONENT_RIGHT].battery_value ||
        cur_cntx->battery[APP_FAST_PAIR_COMPONENT_LEFT].battery_value != battery->battery[APP_FAST_PAIR_COMPONENT_LEFT].battery_value ||
        cur_cntx->battery[APP_FAST_PAIR_COMPONENT_CASE].battery_value != battery->battery[APP_FAST_PAIR_COMPONENT_CASE].battery_value) {
        need_update = true;
    }
    if (true == need_update) {
        context->battery = *battery;
        bt_fast_pair_update_battery(cur_cntx);
        app_fast_pair_trigger_advertising(context);
    }
}

static bool app_fast_pair_bt_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;

    return ret;
}

static void app_fast_pair_ring_request_handle(bt_fast_pair_ring_request_t *ring)
{
    app_find_me_param_struct *find_self_param = (app_find_me_param_struct *)pvPortMalloc(sizeof(app_find_me_param_struct));
    bt_fast_pair_acknowledgements_t ack = {
        .action = BT_FAST_PAIR_MESSAGE_CODE_ACKNOWLEDGEMENT_ACK,
        .groupID = BT_FAST_PAIR_MESSAGE_GROUP_DEVICE_ACTION_EVENT,
        .codeID = BT_FAST_PAIR_MESSAGE_CODE_DEVICE_ACTION_EVENT_RING,
        .reason = BT_FAST_PAIR_ACKNOLEGEMENT_NAK_REASON_NOT_SUPPORTED
    };
    APPS_LOG_MSGID_I(LOG_TAG" Recived ring request from remote device L:%d, r:%d, duration:%d",
            3, ring->ring_status[0], ring->ring_status[1], ring->ring_duration);
    bt_fast_pair_send_acknowledgements(NULL, &ack);
    if (ring->ring_duration == 0xFF) {
        ring->ring_duration = 0;
    }
    if (NULL == find_self_param) {
        APPS_LOG_MSGID_E(LOG_TAG" Allocate ring req buffer fail", 0);
        return;
    }
    find_self_param->blink = 0;
    find_self_param->duration_seconds = ring->ring_duration;
#ifdef MTK_AWS_MCE_ENABLE
    app_find_me_param_struct peer_param = {
        .blink = 0,
        .duration_seconds = ring->ring_duration};
    uint8_t battery_item = (AUDIO_CHANNEL_R == ami_get_audio_channel() ? APP_FAST_PAIR_COMPONENT_RIGHT : APP_FAST_PAIR_COMPONENT_LEFT);
    if (APP_FAST_PAIR_COMPONENT_RIGHT == battery_item) {
        find_self_param->tone = ring->ring_status[APP_FAST_PAIR_COMPONENT_LEFT];
        peer_param.tone = ring->ring_status[APP_FAST_PAIR_COMPONENT_RIGHT];
    } else {
        find_self_param->tone = ring->ring_status[APP_FAST_PAIR_COMPONENT_RIGHT];
        peer_param.tone = ring->ring_status[APP_FAST_PAIR_COMPONENT_LEFT];
    }
    apps_aws_sync_event_send_extra(EVENT_GROUP_UI_SHELL_BT_FAST_PAIR,
                BT_FAST_PAIR_APP_EVENT_RING_REQUEST, &peer_param, sizeof(peer_param));
#else
    find_self_param->tone = ring->ring_status[0];
#endif
    ui_shell_send_event(false, EVENT_PRIORITY_MIDDLE, EVENT_GROUP_UI_SHELL_FINDME,
            APP_FIND_ME_EVENT_ID_TRIGGER, find_self_param, sizeof(app_find_me_param_struct), NULL, 0);
}

static void app_fast_pair_additional_data_handle(app_fast_pair_local_context_t *local_context, bt_fast_pair_additional_data_t *data, uint32_t data_len)
{
    APPS_LOG_MSGID_I(LOG_TAG" Additional data recived data_id %d, data_length %d.", 2, data->data_id, data->data_length);
    if (BT_FAST_PAIR_ADDITIONAL_DATA_ID_PERSONALIZED_NAME == data->data_id) {
        nvkey_status_t status = 0;
        memcpy(local_context->nvkey.personalized_name, data->data, data->data_length);
        local_context->nvkey.personalized_name[data->data_length] = 0;
        if (NVKEY_STATUS_OK != (status = nvkey_write_data(NVKEYID_APP_FAST_PAIR_CONFIGURE,
            (uint8_t *)&(local_context->nvkey), sizeof(app_fast_pair_nvkey_t)))) {
            APPS_LOG_MSGID_E(LOG_TAG" load configure write failed : 0x%x", 1, status);
        }
    }
#ifdef MTK_AWS_MCE_ENABLE
    apps_aws_sync_event_send_extra(EVENT_GROUP_UI_SHELL_BT_FAST_PAIR,
                    BT_FAST_PAIR_APP_EVENT_ADDITIONAL_DATA, data, data_len);
#endif
}

static bool app_fast_pair_app_event_callback(struct _ui_shell_activity *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    assert(NULL != self);
    app_fast_pair_local_context_t   *local_context = (app_fast_pair_local_context_t *)self->local_context;
    APPS_LOG_MSGID_I(LOG_TAG" Fast pair app event id:0x%x, len:%d.", 2, event_id, data_len);
    switch(event_id) {
        case BT_FAST_PAIR_APP_EVENT_NEED_STORE_ACCOUNT_KEY: {
            bt_fast_pair_account_key_list_t *real_key_list = local_context->account_key_list;
            bt_fast_pair_account_key_list_t *key_list = (bt_fast_pair_account_key_list_t *)extra_data;
            assert(key_list == real_key_list);
            APPS_LOG_MSGID_I(LOG_TAG" Account key update change fast pair data.", 0);
            if (!g_app_fast_pair_local_context->is_vis_on) {
                app_fast_pair_trigger_advertising(local_context);
            }
            app_fast_pair_store_account_key(key_list);
        #ifdef MTK_AWS_MCE_ENABLE
            uint32_t size_account = sizeof(bt_fast_pair_account_key_list_t) +
                    sizeof(bt_fast_pair_account_key_t) * (local_context->account_key_list->max_key_number - 1);
            apps_aws_sync_event_send_extra(EVENT_GROUP_UI_SHELL_BT_FAST_PAIR,
                BT_FAST_PAIR_APP_EVENT_NEED_STORE_ACCOUNT_KEY, local_context->account_key_list, size_account);
        #endif
        }
            break;
        case BT_FAST_PAIR_APP_EVENT_SERVICE_CONNECTED: {
            APPS_LOG_MSGID_I(LOG_TAG" Spp service connected.", 0);
        }
            break;
        case BT_FAST_PAIR_APP_EVENT_SERVICE_DISCONNECTED: {
            APPS_LOG_MSGID_I(LOG_TAG" Spp service disconnected.", 0);
        }
            break;
        case BT_FAST_PAIR_APP_EVENT_RING_REQUEST: {
            app_fast_pair_ring_request_handle(extra_data);
        }
            break;
        case BT_FAST_PAIR_APP_EVENT_MESSAGE_STREAM: {
            APPS_LOG_MSGID_I(LOG_TAG" Message stream recived.", 0);
        }
            break;
        case BT_FAST_PAIR_APP_EVENT_ADDITIONAL_DATA: {
            app_fast_pair_additional_data_handle(local_context, extra_data, data_len);
        }
            break;
        default:
            break;
    }
    return false;
}

#ifdef MTK_SMART_CHARGER_ENABLE
static bool app_fast_pair_proc_charger_case_group(
        struct _ui_shell_activity *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    app_fast_pair_local_context_t *local_context = (app_fast_pair_local_context_t *)self->local_context;
    uint8_t battery_item = (AUDIO_CHANNEL_R == ami_get_audio_channel() ? APP_FAST_PAIR_COMPONENT_RIGHT : APP_FAST_PAIR_COMPONENT_LEFT);
    bt_fast_pair_battery_t update_battery = local_context->battery;

    switch (event_id) {
        case EVENT_ID_SMCHARGER_NOTIFY_PUBLIC_EVENT: {
            app_smcharger_public_event_para_t *event_para = (app_smcharger_public_event_para_t *)extra_data;
            if (event_para->action == SMCHARGER_CHARGER_IN_ACTION) {
                APPS_LOG_MSGID_I(LOG_TAG", SMCharger Charger_in action", 0);
                update_battery.battery[battery_item].charging = 1;
            } else if (event_para->action == SMCHARGER_OPEN_LID_ACTION) {
                APPS_LOG_MSGID_I(LOG_TAG", SMCharger Open_lid action", 0);
                update_battery.ui_show = true;
            } else if (event_para->action == SMCHARGER_CLOSE_LID_ACTION) {
                APPS_LOG_MSGID_I(LOG_TAG", SMCharger Close_lid action", 0);
                update_battery.ui_show = false;
            } else if (event_para->action == SMCHARGER_CHARGER_OUT_ACTION) {
                APPS_LOG_MSGID_I(LOG_TAG", SMCharger Charger_out action", 0);
                update_battery.battery[battery_item].charging = 0;
            } else if (event_para->action == SMCHARGER_CASE_BATTERY_REPORT_ACTION) {
                APPS_LOG_MSGID_I(LOG_TAG", BATTERY_LEVEL_REPORT = %d", 1, (uint32_t)event_para->data);
                update_battery.battery[APP_FAST_PAIR_COMPONENT_CASE].battery_value= (uint32_t)event_para->data;
            }
        }
        break;

        default:
            return ret;
    }
    app_fast_pair_update_battery(local_context, &update_battery);
    return ret;
}
#endif

#ifdef MTK_AWS_MCE_ENABLE
#ifdef MTK_SMART_CHARGER_ENABLE
static void app_fast_pair_aws_mce_data_smart_charge_callback(app_fast_pair_local_context_t* context,  bt_aws_mce_report_info_t *report)
{
    uint8_t received_charger_exist = 0;
    app_smcharger_context_t smcharger_context;
    uint8_t battery_item = (AUDIO_CHANNEL_R == ami_get_audio_channel() ? APP_FAST_PAIR_COMPONENT_LEFT : APP_FAST_PAIR_COMPONENT_RIGHT);

    memcpy(&smcharger_context.partner_smcharger_state, report->param, sizeof(smcharger_context.partner_smcharger_state));
    APPS_LOG_MSGID_I(LOG_TAG", [Agent] Received partner smcharger_state=%d", 1, smcharger_context.partner_smcharger_state);
    received_charger_exist = (smcharger_context.partner_smcharger_state != STATE_SMCHARGER_OUT_OF_CASE) ? 1 : 0;
#if 0
    context->battery.battery[battery_item].charging = received_charger_exist;
    if (!(context->battery.battery[APP_FAST_PAIR_COMPONENT_RIGHT].charging) &&
        !(context->battery.battery[APP_FAST_PAIR_COMPONENT_LEFT].charging)) {
        context->battery.ui_show = false;
    } else {
        context->battery.ui_show = true;
    }
    bt_fast_pair_update_battery(&(context->battery));
    app_fast_pair_trigger_advertising(context);
#endif
}
#endif

static void app_fast_pair_aws_mce_data_battery_callback(app_fast_pair_local_context_t* context, bt_aws_mce_report_info_t *report)
{
    uint8_t received_charger_exist = 0;
    uint8_t received_bat_percent = 0;
    uint8_t battery_item = (AUDIO_CHANNEL_R == ami_get_audio_channel() ? APP_FAST_PAIR_COMPONENT_LEFT : APP_FAST_PAIR_COMPONENT_RIGHT);
    bt_fast_pair_battery_t update_battery = context->battery;

#ifdef MTK_SMART_CHARGER_ENABLE
    app_smcharger_context_t smcharger_context;
    memcpy(&smcharger_context.partner_battery_percent, report->param, sizeof(smcharger_context.partner_battery_percent));
    APPS_LOG_MSGID_I(LOG_TAG", [Agent] Received partner_bat_percent = %d", 1, smcharger_context.partner_battery_percent);
    received_bat_percent = smcharger_context.partner_battery_percent & (~ PARTNER_BATTERY_CHARGING);
    received_charger_exist = (smcharger_context.partner_battery_percent & PARTNER_BATTERY_CHARGING) ? 1 : 0;
#else
    battery_local_context_type_t battery_context;
    memcpy(&battery_context.partner_battery_percent, report->param, sizeof(battery_context.partner_battery_percent));
    APPS_LOG_MSGID_I(LOG_TAG", [Agent] Received partner_bat_percent = %d", 1, battery_context.partner_battery_percent);
    received_bat_percent = battery_context.partner_battery_percent & (~ PARTNER_BATTERY_CHARGING);
    received_charger_exist = (battery_context.partner_battery_percent & PARTNER_BATTERY_CHARGING) ? 1 : 0;
#endif
    update_battery.battery[battery_item].charging = received_charger_exist;
    update_battery.battery[battery_item].battery_value = received_bat_percent;
    app_fast_pair_update_battery(context, &update_battery);
}

static void app_fast_pair_aws_mce_data_app_action_callback(app_fast_pair_local_context_t* context, bt_aws_mce_report_info_t *report)
{
    uint32_t    event_group = 0, event_id = 0, extra_aws_len = 0;
    void        *extra_aws = NULL;
    apps_aws_sync_event_decode_extra(report, &event_group, &event_id, &extra_aws, &extra_aws_len);
    APPS_LOG_MSGID_I(LOG_TAG", report group_id:0x%x, event_id:0x%x, data_length:%d, data_buffer:0x%x", 4, event_group, event_id, extra_aws_len, extra_aws);
    if (EVENT_GROUP_UI_SHELL_BT_FAST_PAIR != event_group || NULL == extra_aws) {
        return;
    }
    switch (event_id) {
        case BT_FAST_PAIR_APP_EVENT_NEED_STORE_ACCOUNT_KEY: {
            bt_fast_pair_account_key_list_t *new_account = (bt_fast_pair_account_key_list_t *)extra_aws;
            if (context->account_key_list->max_key_number != new_account->max_key_number) {
                APPS_LOG_MSGID_E(LOG_TAG", Agent(%d) & Partner(%d) max key number are not same! Stop update flow!", 2,
                    context->account_key_list->max_key_number, new_account->max_key_number);
                return;
            }
            memcpy(context->account_key_list, new_account, extra_aws_len);
            app_fast_pair_store_account_key(context->account_key_list);
        }
            break;
        case BT_FAST_PAIR_APP_EVENT_RING_REQUEST: {
            if (extra_aws_len == sizeof(app_find_me_param_struct)) {
                app_find_me_param_struct *find_me_param =
                        (app_find_me_param_struct *)pvPortMalloc(sizeof(app_find_me_param_struct));
                if (find_me_param) {
                    memcpy(find_me_param, extra_aws, sizeof(app_find_me_param_struct));
                    ui_shell_send_event(false, EVENT_PRIORITY_MIDDLE,
                            EVENT_GROUP_UI_SHELL_FINDME,
                            APP_FIND_ME_EVENT_ID_TRIGGER,
                            find_me_param,
                            sizeof(app_find_me_param_struct),
                            NULL, 0);
                }
            }
        }
            break;
        case BT_FAST_PAIR_APP_EVENT_ADDITIONAL_DATA: {
            bt_fast_pair_additional_data_t *data = (bt_fast_pair_additional_data_t *)extra_aws;
            APPS_LOG_MSGID_I(LOG_TAG" Aws packet additional data recived data_id %d, data_length %d.", 2, data->data_id, data->data_length);
            if (BT_FAST_PAIR_ADDITIONAL_DATA_ID_PERSONALIZED_NAME == data->data_id) {
                nvkey_status_t status = 0;
                bt_fast_pair_update_parameters_t params;
                memcpy(context->nvkey.personalized_name, data->data, data->data_length);
                context->nvkey.personalized_name[data->data_length] = 0;
                params.tx_power_level = NULL;
                if (0 != context->nvkey.tx_power_available) {
                    params.tx_power_level = &(context->nvkey.tx_power_level);
                }
                params.addr_type = BT_ADDR_RANDOM;
                params.personalized_name = context->nvkey.personalized_name;
                if (NVKEY_STATUS_OK != (status = nvkey_write_data(NVKEYID_APP_FAST_PAIR_CONFIGURE,
                    (uint8_t *)&(context->nvkey), sizeof(app_fast_pair_nvkey_t)))) {
                    APPS_LOG_MSGID_E(LOG_TAG" load configure write failed : 0x%x", 1, status);
                }
                bt_fast_pair_update_parameters(&params);
            }
        }
            break;
        case APP_FAST_PAIR_AWS_CONTEXT_SYNC_ID: {
            app_fast_pair_nvkey_t *nvkey_cntx = (app_fast_pair_nvkey_t *)extra_aws;
            APPS_LOG_MSGID_I(LOG_TAG" Aws cntext sync data recived selected_set %d, component_num %d.", 2, nvkey_cntx->seleceted_set, nvkey_cntx->component_num);
            memcpy(&(context->nvkey), nvkey_cntx, (sizeof(context->nvkey) - sizeof(context->nvkey.reserved)));
        }
            break;
        default:
            break;
    }
}

static bool app_fast_pair_aws_mce_data_callback(struct _ui_shell_activity *self,  uint32_t event_id, void *extra_data, size_t data_len)
{
    assert(NULL != self);
    app_fast_pair_local_context_t *local_context = (app_fast_pair_local_context_t *)self->local_context;
    bt_aws_mce_report_info_t *report = (bt_aws_mce_report_info_t *)extra_data;
    if (NULL == report || NULL == local_context) {
        APPS_LOG_MSGID_E(LOG_TAG", report data or context is NULL", 0);
        return false;
    }
    switch (report->module_id) {
        case BT_AWS_MCE_REPORT_MODULE_BATTERY:
            app_fast_pair_aws_mce_data_battery_callback(local_context, report);
            break;
    #ifdef MTK_SMART_CHARGER_ENABLE
        case BT_AWS_MCE_REPORT_MODULE_SMCHARGER:
            app_fast_pair_aws_mce_data_smart_charge_callback(local_context, report);
            break;
    #endif
        case BT_AWS_MCE_REPORT_MODULE_APP_ACTION:
            app_fast_pair_aws_mce_data_app_action_callback(local_context, report);
            break;
        default:
            APPS_LOG_MSGID_I(LOG_TAG", report moudle id:0x%x not support.", 1, report->module_id);
            return false;
    }
    return false;
}
#endif

static bool app_fast_pair_init(ui_shell_activity_t *self)
{
    bt_fast_pair_status_t           ret;
    app_fast_pair_local_context_t   *local_context = (app_fast_pair_local_context_t *)self->local_context;
    uint32_t    app_fast_pair_model_id = APP_FAST_PAIR_AIROHA_TEST_MODEL_ID;
    uint8_t     app_fast_pair_private_key[32] = APP_FAST_PAIR_AIROHA_TEST_PRIVATE_KEY;
    if (0 == app_fast_pair_load_configure_info(local_context)) {
        if (local_context->nvkey.seleceted_set >= APP_FAST_PAIR_MODEL_SET_MAX_NUMBER) {
            local_context->nvkey.seleceted_set = APP_FAST_PAIR_MODEL_SET_MAX_NUMBER - 1;
        }
        app_fast_pair_model_id = local_context->protect_sets.sets[local_context->nvkey.seleceted_set].model_id;
        memcpy(app_fast_pair_private_key, &(local_context->protect_sets.sets[local_context->nvkey.seleceted_set].private_key), 32 * sizeof(uint8_t));
        APPS_LOG_MSGID_W(LOG_TAG" init model id 0x%x, max account %d, tx_power_exist %d, tx_power %d, component num %d select %d!",
            6, app_fast_pair_model_id, local_context->nvkey.max_account, local_context->nvkey.tx_power_available,
            local_context->nvkey.tx_power_level, local_context->nvkey.component_num, local_context->nvkey.seleceted_set);
        if (!local_context->nvkey.fps_enable) {
            APPS_LOG_MSGID_W(LOG_TAG" init disable!", 0);
            return false;
        }
    } else {
        /* load fail must stop init process */
        //return false;
        app_fast_pair_nvkey_t           temp_cnt = {
            .fps_enable = 1,
            .max_account = APP_FAST_PAIR_DEFAULT_MAX_ACCOUNT,
            .seleceted_set = 0,
            .tx_power_available = 1,
            .tx_power_level = APP_FAST_PAIR_DEFAULT_TX_POWER_LEVEL,
            .component_num = APP_FAST_PAIR_COMPONENT_NUM,
            .personalized_name = APP_FAST_PAIR_PERSONALIZED_NAME
        };
        memcpy(&(local_context->nvkey), &temp_cnt, sizeof(app_fast_pair_nvkey_t));
        local_context->protect_sets.sets[0].model_id = app_fast_pair_model_id;
        memcpy(&(local_context->protect_sets.sets[0].private_key), app_fast_pair_private_key, sizeof(app_fast_pair_private_key));
    }
    local_context->account_key_list = (bt_fast_pair_account_key_list_t *)pvPortMalloc(sizeof(bt_fast_pair_account_key_list_t) +
        (local_context->nvkey.max_account - 1) * sizeof(bt_fast_pair_account_key_t));
    if (NULL == local_context->account_key_list) {
        APPS_LOG_MSGID_E(LOG_TAG" Init allocate failed", 0);
        return false;
    }
    app_fast_pair_load_account_key(local_context);
    APPS_LOG_MSGID_I(LOG_TAG" Init account ", 0);
    bt_fast_pair_init_parameters_t init_param;
    init_param.model_id = app_fast_pair_model_id;
    init_param.private_key = app_fast_pair_private_key;
    init_param.tx_power_level = NULL;
    init_param.addr_type = BT_ADDR_RANDOM;
    init_param.component_num = local_context->nvkey.component_num;
    init_param.personalized_name = local_context->nvkey.personalized_name;
    if (0 != local_context->nvkey.tx_power_available) {
        init_param.tx_power_level = &(local_context->nvkey.tx_power_level);
    }
    ret = bt_fast_pair_init(&init_param);
    local_context->battery.ui_show = false;
    local_context->battery.component_num = APP_FAST_PAIR_COMPONENT_NUM;
    local_context->battery.remaining_time = 0xFFFF;
    for (uint8_t i = 0; i < APP_FAST_PAIR_COMPONENT_NUM; i++) {
        local_context->battery.battery[i].charging = 0;
        local_context->battery.battery[i].battery_value = 0xFF;
    }
    if (ret != BT_FAST_PAIR_STATUS_SUCCESS) {
        APPS_LOG_MSGID_E(LOG_TAG" fast pair service init failed %d", 1, ret);
        vPortFree(local_context->account_key_list);
        local_context->account_key_list = NULL;
        return false;
    }
    return true;
}

static bool app_proc_fast_pair_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    switch (event_id) {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE: {
            self->local_context = pvPortMalloc(sizeof(app_fast_pair_local_context_t));
            if (self->local_context == NULL) {
                ret = false;
            } else {
                memset(self->local_context, 0, sizeof(app_fast_pair_local_context_t));
                ret = app_fast_pair_init(self);
                if (ret == true) {
                    /* BT event is not suitable for UI shell scheme.
                                 * Instead, choose bt callback manager to handle bt related event. */
                    uint8_t battery_item = (AUDIO_CHANNEL_R == ami_get_audio_channel() ? APP_FAST_PAIR_COMPONENT_RIGHT : APP_FAST_PAIR_COMPONENT_LEFT);
                    g_app_fast_pair_local_context = self->local_context;
                    bt_fast_pair_battery_t update_battery = g_app_fast_pair_local_context->battery;
                    update_battery.battery[battery_item].charging = battery_management_get_battery_property(BATTERY_PROPERTY_CHARGER_EXIST);
                    update_battery.battery[battery_item].battery_value = battery_management_get_battery_property(BATTERY_PROPERTY_CAPACITY);
                    app_fast_pair_update_battery(g_app_fast_pair_local_context, &update_battery);
                    extern void bt_fast_pair_at_cmd_init();
                    bt_fast_pair_at_cmd_init();
                } else {
                    vPortFree(self->local_context);
                    self->local_context = NULL;
                }
            }
            APPS_LOG_MSGID_I(LOG_TAG" activity: %d", 1, ret);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY: {
            app_fast_pair_local_context_t *local_context = (app_fast_pair_local_context_t *)self->local_context;
            if (local_context) {
                if (local_context->account_key_list) {
                    vPortFree(local_context->account_key_list);
                    local_context->account_key_list = NULL;
                }
                vPortFree(self->local_context);
                self->local_context = NULL;
                g_app_fast_pair_local_context = NULL;
            }
            APPS_LOG_MSGID_I(LOG_TAG" destory", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESUME: {
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE: {
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH: {
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESULT: {
            break;
        }
        default:
            break;
    }
    return ret;
}

static bool app_fast_pair_proc_battery_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    assert(NULL != self);
    app_fast_pair_local_context_t *local_context = (app_fast_pair_local_context_t *)self->local_context;
    uint8_t battery_item = (AUDIO_CHANNEL_R == ami_get_audio_channel() ? APP_FAST_PAIR_COMPONENT_RIGHT : APP_FAST_PAIR_COMPONENT_LEFT);
    bt_fast_pair_battery_t update_battery = local_context->battery;
    switch (event_id) {
        case APPS_EVENTS_BATTERY_PERCENT_CHANGE: {
            APPS_LOG_MSGID_I(LOG_TAG"APPS_EVENTS_BATTERY_PERCENT_CHANGE : %d", 1, (uint32_t)extra_data);
            update_battery.battery[battery_item].battery_value = (uint32_t)extra_data;
        }
            break;
        case APPS_EVENTS_BATTERY_CHARGER_EXIST_CHANGE: {
#if 0
            APPS_LOG_MSGID_I(LOG_TAG"APPS_EVENTS_BATTERY_CHARGER_EXIST_CHANGE : %d", 1, (uint32_t)extra_data);
            local_context->battery.battery[battery_item].charging = (uint32_t)extra_data;
            if (!(local_context->battery.battery[APP_FAST_PAIR_COMPONENT_RIGHT].charging) &&
                !(local_context->battery.battery[APP_FAST_PAIR_COMPONENT_LEFT].charging)) {
                local_context->battery.ui_show = false;
            } else {
                local_context->battery.ui_show = true;
            }
#endif
        }
            return ret;
        default:
            return ret;
    }
    app_fast_pair_update_battery(local_context, &update_battery);
    return ret;
}

static bool app_fast_pair_proc_bt_cm_group(struct _ui_shell_activity *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    APPS_LOG_MSGID_I("[APP_FAST_PAIR], bt cm event_id : 0x%x", 1, event_id);
    app_fast_pair_local_context_t *local_context = (app_fast_pair_local_context_t *)self->local_context;
    if (local_context == NULL) {
        return ret;
    }
#ifdef MTK_AWS_MCE_ENABLE
    /* Only handle bt cm related event in Agent role */
    bt_aws_mce_role_t aws_role = bt_connection_manager_device_local_info_get_aws_role();
    if ((BT_AWS_MCE_ROLE_PARTNER | BT_AWS_MCE_ROLE_CLINET) & aws_role) {
        APPS_LOG_MSGID_W("[APP_FAST_PAIR] Don't init on wrongly role: %d", 1, aws_role);
        return ret;
    }
#endif    
    switch (event_id) {
        case BT_CM_EVENT_VISIBILITY_STATE_UPDATE: {
            bt_cm_visibility_state_update_ind_t *visible_update = (bt_cm_visibility_state_update_ind_t *)extra_data;
            local_context->is_vis_on = visible_update->visibility_state;
            APPS_LOG_MSGID_I("[APP_FAST_PAIR] Visibilty update chage fast pair data.", 0);
            app_fast_pair_trigger_advertising(local_context);
        }
            break;
        case BT_CM_EVENT_POWER_STATE_UPDATE: {
            bt_cm_power_state_update_ind_t *power_updata = (bt_cm_power_state_update_ind_t *)extra_data;
            if (BT_CM_POWER_STATE_ON != power_updata->power_state && !apps_config_features_is_mp_test_mode()) {
                local_context->is_vis_on = false;
                multi_ble_adv_manager_remove_ble_adv(app_fast_pair_get_adv_data);
            } else if (BT_CM_POWER_STATE_ON == power_updata->power_state && !apps_config_features_is_mp_test_mode()) {
                APPS_LOG_MSGID_I("[APP_FAST_PAIR] Power on trigger fast pair advertising.", 0);
                app_fast_pair_trigger_advertising(local_context);
            }
        }
            break;
        case BT_CM_EVENT_REMOTE_INFO_UPDATE: {
            bt_cm_remote_info_update_ind_t *remote_update = (bt_cm_remote_info_update_ind_t *)extra_data;
#ifdef MTK_AWS_MCE_ENABLE
            if (BT_CM_ACL_LINK_CONNECTED <= remote_update->acl_state &&
                !(remote_update->pre_connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS)) &&
                (remote_update->connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS))) {
                uint32_t size_account = sizeof(bt_fast_pair_account_key_list_t) +
                    sizeof(bt_fast_pair_account_key_t) * (local_context->account_key_list->max_key_number - 1);
                /* Only agent role will enable this module, so don't need judge the role
                           * in event handle. */
                APPS_LOG_MSGID_I("[APP_FAST_PAIR]AWS attached.", 0);
                apps_aws_sync_event_send_extra(EVENT_GROUP_UI_SHELL_BT_FAST_PAIR,
                    BT_FAST_PAIR_APP_EVENT_NEED_STORE_ACCOUNT_KEY, local_context->account_key_list, size_account);
                apps_aws_sync_event_send_extra(EVENT_GROUP_UI_SHELL_BT_FAST_PAIR,
                    APP_FAST_PAIR_AWS_CONTEXT_SYNC_ID, &(local_context->nvkey), sizeof(local_context->nvkey) - sizeof(local_context->nvkey.reserved));
            }
#endif
        }
            break;
        default:
            break;
    }
    return ret;
}

bool app_fast_pair_idle_activity_proc(struct _ui_shell_activity *self, uint32_t event_group, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    APPS_LOG_MSGID_I("[APP_FAST_PAIR]APP_FAST_PAIR_IDLE_ACT, event_group : %x, evt: %x", 2, event_group, event_id);
    switch(event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM: {
            ret = app_proc_fast_pair_shell_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_BATTERY: {
            ret = app_fast_pair_proc_battery_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_CONN_MANAGER: {
            /* Trigger fast pair behavior */
            ret = app_fast_pair_proc_bt_cm_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT:
            ret = app_fast_pair_bt_event_proc(self, event_id, extra_data, data_len);
            break;
#ifdef MTK_SMART_CHARGER_ENABLE
        case EVENT_GROUP_UI_SHELL_CHARGER_CASE:
            ret = app_fast_pair_proc_charger_case_group(self, event_id, extra_data, data_len);
            break;
#endif
        case EVENT_GROUP_UI_SHELL_BT_FAST_PAIR: {
            ret = app_fast_pair_app_event_callback(self, event_id, extra_data, data_len);
            break;
        }
#ifdef MTK_AWS_MCE_ENABLE
        case EVENT_GROUP_UI_SHELL_AWS_DATA: {
            /* Just group currently */
             ret = app_fast_pair_aws_mce_data_callback(self, event_id, extra_data, data_len);
            break;
        }
#endif
        default:
            break;
    }

    APPS_LOG_MSGID_I("[APP_FAST_PAIR]APP_FAST_PAIR_IDLE_ACT, ret : %x", 1, ret);
    return ret;
}

bt_fast_pair_account_key_list_t *bt_fast_pair_get_account_key_list()
{
    if (NULL == g_app_fast_pair_local_context) {
        return NULL;
    }
    return g_app_fast_pair_local_context->account_key_list;
}

void        app_fast_pair_set_tx_power_level(int8_t tx_power)
{
    if (NULL != g_app_fast_pair_local_context) {
        nvkey_status_t status = 0;
        g_app_fast_pair_local_context->nvkey.tx_power_level = tx_power;
        if (NVKEY_STATUS_OK != (status = nvkey_write_data(NVKEYID_APP_FAST_PAIR_CONFIGURE,
            (uint8_t *)&(g_app_fast_pair_local_context->nvkey), sizeof(app_fast_pair_nvkey_t)))) {
            APPS_LOG_MSGID_E("load configure write failed : 0x%x", 1, status);
        }
    }
}


