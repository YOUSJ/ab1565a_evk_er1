/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifdef GSOUND_LIBRARY_ENABLE

#include "app_gsound_idle_activity.h"
#include "app_gsound_multi_va.h"
#include "app_home_screen_idle_activity.h"
#include "app_bt_conn_componet_in_homescreen.h"
#include "apps_events_key_event.h"
#include "apps_config_event_list.h"
#include "gsound_port_config.h"
#include "gsound_port_interface.h"
#include "bt_sink_srv.h"
#include "bt_device_manager.h"
#include "apps_events_event_group.h"
#include "apps_events_interaction_event.h"
#include "ui_shell_activity.h"
#include "ui_shell_manager.h"
#include "apps_config_key_remapper.h"
#include "apps_config_vp_index_list.h"
#include "apps_config_vp_manager.h"
#include "apps_aws_sync_event.h"
#include "apps_debug.h"
#include "battery_management.h"
#include "bt_sink_srv_ami.h"
#include "gsound_service.h"
#if defined(MTK_AWS_MCE_ENABLE)
#include "bt_aws_mce_report.h"
#endif
#ifndef MULTI_VA_SUPPORT_COMPETITION
#include "multi_va_manager.h"
#endif

#define UI_SHELL_GSOUND_ACTIVITY    "[app_gsound_idle_activity]"

enum
{
    KEY_NOT_PREPARED,
    KEY_PREPARED,
    KEY_RELEASE,
    KEY_PTT,
};

static uint32_t app_gsound_is_key_prepared;

static void app_gsound_idle_activity_update_battery(void)
{
    int32_t capacity, charger_status;
    capacity = battery_management_get_battery_property(BATTERY_PROPERTY_CAPACITY);
    charger_status = battery_management_get_battery_property(BATTERY_PROPERTY_CHARGER_EXIST);

    gsound_port_send_battery_info(charger_status, capacity, NULL);
}

static bool gsound_idle_proc_ui_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data,size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    switch (event_id) {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE: {
            APPS_LOG_MSGID_I(UI_SHELL_GSOUND_ACTIVITY", create", 0);
            #ifndef MULTI_VA_SUPPORT_COMPETITION
            if (multi_va_manager_get_current_va_type() == MULTI_VA_TYPE_GSOUND)
            #endif
            {
                #ifdef GSOUND_SUPPORT_TWS
                GSoundServiceInitAsTws();
                #else
                GSoundServiceInitAsStereo();
                #endif

                #ifdef GSOUND_HOTWORD_ENABLE
                GSoundServiceInitHotwordExternal();
                #endif
            }
            app_gsound_idle_activity_update_battery();
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY: {
            APPS_LOG_MSGID_I(UI_SHELL_GSOUND_ACTIVITY", destroy", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESUME: {
            APPS_LOG_MSGID_I(UI_SHELL_GSOUND_ACTIVITY", resume", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE: {
            APPS_LOG_MSGID_I(UI_SHELL_GSOUND_ACTIVITY", pause", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH: {
            APPS_LOG_MSGID_I(UI_SHELL_GSOUND_ACTIVITY", refresh", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESULT: {
            APPS_LOG_MSGID_I(UI_SHELL_GSOUND_ACTIVITY", result", 0);
            break;
        }
        default:
            break;
    }
    return ret;
}

/*
 * return true means the key event valid on gsound, else invalid
 */
static GSOUND_PORT_ACTION_EVENT app_gsound_component_map_keyevent_action(apps_config_key_action_t key_event)
{
    GSOUND_PORT_ACTION_EVENT action = GSOUND_PORT_ACTION_NON;
    switch(key_event)
    {
        case KEY_GSOUND_PRESS:
            if (bt_sink_srv_ami_get_current_scenario() != HFP) {
                app_gsound_is_key_prepared = KEY_PREPARED;
                action = (GSOUND_PORT_ACTION_GA_WILL_PAUSE | GSOUND_PORT_ACTION_GA_VOICE_PREPARE | GSOUND_PORT_ACTION_GA_FETCH_PREPARE);
            }
            break;
        case KEY_GSOUND_VOICE_QUERY:
            if (app_gsound_is_key_prepared == KEY_PREPARED) {
                gsound_port_send_action_event((GSOUND_PORT_ACTION_GA_WILL_PAUSE | GSOUND_PORT_ACTION_GA_VOICE_PREPARE | GSOUND_PORT_ACTION_GA_FETCH_PREPARE), NULL);
#ifdef GSOUND_SUPPORT_REDUCE_GESTURE
                gsound_port_send_action_event(GSOUND_PORT_ACTION_GA_VOICE_PTT_OR_FETCH, NULL);
#else
                gsound_port_send_action_event(GSOUND_PORT_ACTION_GA_VOICE_PTT, NULL);
#endif
                app_gsound_is_key_prepared = KEY_PTT;
                action = (GSOUND_PORT_ACTION_GA_VOICE_PTT);
            }
            break;
        case KEY_GSOUND_NOTIFY:
            if (app_gsound_is_key_prepared == KEY_RELEASE) {
                gsound_port_send_action_event((GSOUND_PORT_ACTION_GA_WILL_PAUSE | GSOUND_PORT_ACTION_GA_VOICE_PREPARE | GSOUND_PORT_ACTION_GA_FETCH_PREPARE), NULL);
                gsound_port_send_action_event(GSOUND_PORT_ACTION_GA_VOICE_DONE, NULL);
                gsound_port_send_action_event((GSOUND_PORT_ACTION_GA_FETCH | GSOUND_PORT_ACTION_GA_VOICE_CONFIRM), NULL);
                app_gsound_is_key_prepared = KEY_NOT_PREPARED;
                action = (GSOUND_PORT_ACTION_GA_FETCH | GSOUND_PORT_ACTION_GA_VOICE_CONFIRM);
            }
            break;
        case KEY_GSOUND_CANCEL:
            if (app_gsound_is_key_prepared == KEY_RELEASE) {
                gsound_port_send_action_event((GSOUND_PORT_ACTION_GA_WILL_PAUSE | GSOUND_PORT_ACTION_GA_VOICE_PREPARE | GSOUND_PORT_ACTION_GA_FETCH_PREPARE), NULL);
                gsound_port_send_action_event(GSOUND_PORT_ACTION_GA_VOICE_DONE, NULL);
                gsound_port_send_action_event(GSOUND_PORT_ACTION_GA_STOP_ASSISTANT, NULL);
                app_gsound_is_key_prepared = KEY_NOT_PREPARED;
                action = (GSOUND_PORT_ACTION_GA_STOP_ASSISTANT);
            }
            break;
        case KEY_GSOUND_RELEASE:
            if (app_gsound_is_key_prepared == KEY_PREPARED) {
                app_gsound_is_key_prepared = KEY_RELEASE;
                action = GSOUND_PORT_ACTION_GA_VOICE_DONE;
            } else if (app_gsound_is_key_prepared == KEY_PTT) {
                gsound_port_send_action_event(GSOUND_PORT_ACTION_GA_VOICE_DONE, NULL);
                app_gsound_is_key_prepared = KEY_NOT_PREPARED;
                action = GSOUND_PORT_ACTION_GA_VOICE_DONE;
            }
            break;
        case KEY_AVRCP_PLAY:
        case KEY_AVRCP_PAUSE:
            if (gsound_port_send_action_event(GSOUND_PORT_ACTION_TOGGLE_PLAY_PAUSE, NULL) == 0) {
                action = GSOUND_PORT_ACTION_TOGGLE_PLAY_PAUSE;
            }
            break;
        default:
            break;
    }
    return action;
}

/*
 * return true means the event is used by gsound, so it will not be send to next app
 */
static bool app_gsound_component_proc_key_event_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    uint8_t key_id;
    airo_key_event_t key_event;
    GSOUND_PORT_ACTION_EVENT gsound_event;

    app_event_key_event_decode(&key_id, &key_event, event_id);

    apps_config_key_action_t action;
    if (extra_data) {
        action = *(uint16_t *)extra_data;
    } else {
        action = apps_config_key_event_remapper_map_action(key_id, key_event);
    }

    g_port_log("[APP_GSOUND] component_proc_key_event:%x,%x,%x,%x,%x \n\r", 5, event_id, key_id, key_event, action, bt_sink_srv_ami_get_current_scenario());
    if ((action >= KEY_GSOUND_PRESS && action <= KEY_GSOUND_CANCEL) || action == KEY_AVRCP_PLAY || action == KEY_AVRCP_PAUSE) {
#ifdef MTK_AWS_MCE_ENABLE
        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER) {
            if (app_home_screen_idle_activity_is_aws_connected()) {
                if (BT_STATUS_SUCCESS != apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_KEY, action)) {
                    APPS_LOG_MSGID_I(UI_SHELL_GSOUND_ACTIVITY", Partner send gsound action %d aws to agent failed", 1, action);
                }
            } else {
                apps_config_set_vp(VP_INDEX_FAILED, false, 100, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                APPS_LOG_MSGID_I(UI_SHELL_GSOUND_ACTIVITY", Partner aws disconnected for GSOUND action %d", 1, action);
            }
        } else
#endif
        {
            if ((gsound_event = app_gsound_component_map_keyevent_action(action)) == GSOUND_PORT_ACTION_NON) {
                APPS_LOG_MSGID_I(UI_SHELL_GSOUND_ACTIVITY", gsound_port_send_action_event failed", 0);
                if (KEY_AVRCP_PLAY == action || KEY_AVRCP_PAUSE == action) {
                    gsound_port_action_rejected(GSOUND_PORT_ACTION_TOGGLE_PLAY_PAUSE);
                }
            }
        }

        ret = true;
    }

    return ret;
}

/*
 * return true means the event is used by gsound, so it will not be processed by music
 */
bool app_gsound_idle_activity_proc_music_event(bt_sink_srv_action_t sink_action)
{
    bool ret = false;
    if((sink_action == BT_SINK_SRV_ACTION_PLAY_PAUSE)
            && GSOUND_PORT_ACTION_EVENT_MAX != gsound_port_remap_action(sink_action))
    {
        ret = true;
    }

    return ret;
}

/*
 * return 0 means the action reject handle success, else is error.
 */
uint8_t gsound_port_action_rejected(GSOUND_PORT_ACTION_EVENT action)
{
    uint8_t result = 0;
    bt_sink_srv_action_t sink_action = gsound_port_remap_action_reverse(action);

    if (sink_action != -1) {
        ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                APPS_EVENTS_INTERACTION_GSOUND_ACTION_REJECTED, (void *)sink_action, 0,
                NULL, 0);
    }

    return result;
}

#ifdef MTK_AWS_MCE_ENABLE
static bool gsound_idle_aws_report_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    bt_aws_mce_report_info_t *aws_data_ind = (bt_aws_mce_report_info_t *)extra_data;
    GSOUND_PORT_ACTION_EVENT gsound_action;

    if (aws_data_ind->module_id == BT_AWS_MCE_REPORT_MODULE_APP_ACTION) {
        uint32_t aws_event_group;
        uint32_t aws_event_id;
        apps_aws_sync_event_decode(aws_data_ind, &aws_event_group, &aws_event_id);
        switch (aws_event_group) {
            case EVENT_GROUP_UI_SHELL_KEY:
            APPS_LOG_MSGID_I(UI_SHELL_GSOUND_ACTIVITY", received aws sync key action : %x,%x", 2, aws_event_id, app_gsound_is_key_prepared);
            if((aws_event_id >= KEY_GSOUND_PRESS && aws_event_id <= KEY_GSOUND_CANCEL) || aws_event_id == KEY_AVRCP_PLAY || aws_event_id == KEY_AVRCP_PAUSE) {
                if ((gsound_action = app_gsound_component_map_keyevent_action(aws_event_id)) == GSOUND_PORT_ACTION_NON) {
                    APPS_LOG_MSGID_I(UI_SHELL_GSOUND_ACTIVITY", gsound_port_send_action_event failed", 0);
                    if (KEY_AVRCP_PLAY == aws_event_id || KEY_AVRCP_PAUSE == aws_event_id) {
                        gsound_port_action_rejected(GSOUND_PORT_ACTION_TOGGLE_PLAY_PAUSE);
                    }
                }
            }
                ret = true;

            break;
            default:
            break;
        }
    }
    return ret;
}
#endif

bool app_gsound_idle_activity_proc(
            struct _ui_shell_activity *self,
            uint32_t event_group,
            uint32_t event_id,
            void *extra_data,
            size_t data_len)
{
    bool ret = false;

    APPS_LOG_MSGID_I(UI_SHELL_GSOUND_ACTIVITY", event_group:%x id:%x", 2, event_group, event_id);

    switch (event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM: {
            ret = gsound_idle_proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_APP_INTERACTION: {
            home_screen_local_context_type_t *local_ctx = (home_screen_local_context_type_t *)self->local_context;
            if (event_id == APPS_EVENTS_INTERACTION_UPDATE_MMI_STATE && local_ctx->connection_state == BT_SINK_SRV_STATE_POWER_ON) {
                app_gsound_idle_activity_update_battery();
            }
            break;
        }
        case EVENT_GROUP_UI_SHELL_KEY: {
            ret = app_gsound_component_proc_key_event_group(self, event_id, extra_data, data_len);
            break;
        }
#if defined(MTK_AWS_MCE_ENABLE)
        case EVENT_GROUP_UI_SHELL_AWS_DATA: {
            ret = gsound_idle_aws_report_event_proc(self, event_id, extra_data, data_len);
            break;
        }
#endif

        case EVENT_GROUP_UI_SHELL_BATTERY: {
            app_gsound_idle_activity_update_battery();
            break;
        }

        case EVENT_GROUP_UI_SHELL_GSOUND: {
            switch (event_id) {
                case APPS_EVENTS_INTERACTION_GSOUND_ENABLED:
                case APPS_EVENTS_INTERACTION_GSOUND_CONNECTED: {
                    app_gsound_multi_va_notify_connected();
                    break;
                }

                case APPS_EVENTS_INTERACTION_GSOUND_DISABLED: {
                    app_gsound_multi_va_notify_disconnected(true);
                    break;
                }

                case APPS_EVENTS_INTERACTION_GSOUND_DISCONNECTED: {
                    app_gsound_multi_va_notify_disconnected(false);
                    break;
                }
                default:
                    break;
            }

            ret = true;
            break;
        }

        default:
            break;
    }

    APPS_LOG_MSGID_I(UI_SHELL_GSOUND_ACTIVITY", ret : %x", 1, ret);
    return ret;
}

#endif
