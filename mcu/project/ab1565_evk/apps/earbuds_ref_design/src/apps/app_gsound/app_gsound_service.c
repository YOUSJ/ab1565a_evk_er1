/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifdef GSOUND_LIBRARY_ENABLE
#include "bt_callback_manager.h"
#if defined MTK_GSOUND_AIR_SPP_ENABLE
#include "gsound_spp_air_interface.h"
#endif
#include "FreeRTOS.h"
#include "app_gsound_idle_activity.h"
#include "gsound_port_interface.h"
#include "gsound_service.h"
#include "ble_bms.h"
#include "bt_sink_srv_gsound.h"
#include "ui_shell_manager.h"
#include "apps_events_event_group.h"
#include "apps_events_interaction_event.h"
#include "ble_ancs_app.h"
#include "apps_config_vp_index_list.h"
#include "apps_config_vp_manager.h"
#include "multi_va_manager.h"
#include "nvdm.h"
#include "ble_ams.h"
#include "nvkey_id_list.h"
#include "nvkey.h"
#include "nvdm_id_list.h"

/**************************************************************************************************
* Define
**************************************************************************************************/
#define APP_GSOUND_VP_SYNC_TIME         100

#define APP_GSOUND_CHANNEL_CONTROL      0x01
#define APP_GSOUND_CHANNEL_AUDIO        0x02
#define APP_GSOUND_CHANNEL_BITMASK      0x03

#define GSOUND_INITIAL_STATE_DISABLED   0
#define GSOUND_INITIAL_STATE_ENABLED    1

#define MODULE_SN_LENGTH_SIZE           5
#define VER_LENGTH_SIZE                 1

typedef struct {
    uint32_t module_id;
    uint8_t  serial_num_length;
    uint8_t  serial_num[1];
    uint8_t  version_length;
    uint8_t  version[1];
} __attribute__((packed)) app_gsound_info_nvkey_t;

/**************************************************************************************************
* Prototype
**************************************************************************************************/
static void callback_gsound_enabled(void);
static void callback_gsound_disabled(void);
static void app_gsound_reset_parameter(void);
static uint32_t app_gsound_map_vp_index(uint8_t clip);
static void app_gsound_write_initial_state(uint8_t state);

/**************************************************************************************************
* Variable
**************************************************************************************************/
static GSoundServiceObserver observer = {
    callback_gsound_enabled,
    callback_gsound_disabled
};

static GSoundServiceConfig gsound_config;
static bool gsound_ignore_false_alarm = true;
static uint8_t gsound_on_connected_channel = 0;
static uint8_t gsound_on_disconnected_channel = 0;

/**************************************************************************************************
* Static function
**************************************************************************************************/
static void callback_gsound_enabled(void)
{
    g_port_log("[APP_GSOUND] GSound Enabled cb, false_alarm:%x\n\r", 1, gsound_ignore_false_alarm);

    if (gsound_ignore_false_alarm) {
        /* While GSound init (gsound_enabled = true), callback_gsound_enabled() will be executed. Ignore the 1st callback. */
        gsound_ignore_false_alarm = false;
        return;
    }

    app_gsound_write_initial_state(GSOUND_INITIAL_STATE_ENABLED);
    app_gsound_reset_parameter();

#ifdef MTK_AWS_MCE_ENABLE
    if (BT_AWS_MCE_ROLE_AGENT != bt_device_manager_aws_local_info_get_role()) {
        return;
    }
#endif

    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_GSOUND,
                        APPS_EVENTS_INTERACTION_GSOUND_ENABLED,
                        NULL, 0, NULL, 0);
}

static void callback_gsound_disabled(void)
{
    g_port_log("[APP_GSOUND] GSound Disabled cb, false_alarm:%x\n\r", 1, gsound_ignore_false_alarm);

    if (gsound_ignore_false_alarm) {
        /* While GSound init (gsound_enabled = false), callback_gsound_disabled() will be executed. Ignore the 1st callback. */
        gsound_ignore_false_alarm = false;
        return;
    }

    app_gsound_write_initial_state(GSOUND_INITIAL_STATE_DISABLED);
    app_gsound_reset_parameter();

#ifdef MTK_AWS_MCE_ENABLE
    if (BT_AWS_MCE_ROLE_AGENT != bt_device_manager_aws_local_info_get_role()) {
        return;
    }
#endif

    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_GSOUND,
                        APPS_EVENTS_INTERACTION_GSOUND_DISABLED,
                        NULL, 0, NULL, 0);
}

void gsound_bt_on_connected_callback(void)
{
#ifdef MTK_AWS_MCE_ENABLE
    if ((MULTI_VA_TYPE_GSOUND != multi_va_manager_get_current_va_type()) ||
        (BT_AWS_MCE_ROLE_AGENT != bt_device_manager_aws_local_info_get_role()))
#else
    if (MULTI_VA_TYPE_GSOUND != multi_va_manager_get_current_va_type())
#endif
    {
        return;
    }

    g_port_log("[APP_GSOUND] BT Connected cb\n\r", 0);

    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_GSOUND,
                        APPS_EVENTS_INTERACTION_GSOUND_CONNECTED,
                        NULL, 0, NULL, 0);
}

void gsound_bt_on_disconnected_callback(uint8_t channel)
{
#ifdef MTK_AWS_MCE_ENABLE
    if ((MULTI_VA_TYPE_GSOUND != multi_va_manager_get_current_va_type()) ||
        (BT_AWS_MCE_ROLE_AGENT != bt_device_manager_aws_local_info_get_role()))
#else
    if (MULTI_VA_TYPE_GSOUND != multi_va_manager_get_current_va_type())
#endif
    {
        //g_port_log("[APP_GSOUND] BT Disconnected cb NOT GSOUND channel:%x\n\r", 1, gsound_on_disconnected_channel);
        return;
    }

    g_port_log("[APP_GSOUND] BT Disconnected cb channel:%x\n\r", 1, gsound_on_disconnected_channel);

    gsound_on_disconnected_channel |= ((channel == GSOUND_CHANNEL_CONTROL) ? APP_GSOUND_CHANNEL_CONTROL : APP_GSOUND_CHANNEL_AUDIO);

    if (gsound_on_disconnected_channel == APP_GSOUND_CHANNEL_BITMASK) {
        gsound_on_disconnected_channel = 0;
        ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_GSOUND,
                            APPS_EVENTS_INTERACTION_GSOUND_DISCONNECTED,
                            NULL, 0, NULL, 0);
    }
}

void gsound_ble_on_connected_callback(uint8_t channel)
{
#ifdef MTK_AWS_MCE_ENABLE
    if ((MULTI_VA_TYPE_GSOUND != multi_va_manager_get_current_va_type()) ||
        (BT_AWS_MCE_ROLE_AGENT != bt_device_manager_aws_local_info_get_role()))
#else
    if (MULTI_VA_TYPE_GSOUND != multi_va_manager_get_current_va_type())
#endif
    {
        //g_port_log("[APP_GSOUND] BLE Disconnected cb NOT GSOUND channel:%x\n\r", 1, gsound_on_disconnected_channel);
        return;
    }

    g_port_log("[APP_GSOUND] BLE Connected cb channel:%x\n\r", 1, gsound_on_connected_channel);

    gsound_on_connected_channel |= ((channel == GSOUND_CHANNEL_CONTROL) ? APP_GSOUND_CHANNEL_CONTROL : APP_GSOUND_CHANNEL_AUDIO);

    if (gsound_on_connected_channel == APP_GSOUND_CHANNEL_BITMASK) {
        gsound_on_connected_channel = 0;
        ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_GSOUND,
                            APPS_EVENTS_INTERACTION_GSOUND_CONNECTED,
                            NULL, 0, NULL, 0);
    }
}

void gsound_ble_on_disconnected_callback(uint8_t channel)
{
    //g_port_log("[APP_GSOUND] BLE Disconnected cb channel:%x\n\r", 1, channel);
    gsound_bt_on_disconnected_callback(channel);
}

void gsound_rho_send_prepare_ready(void)
{
    g_port_log("[APP_GSOUND] gsound_rho_send_prepare_ready \n\r", 0);
#if defined(MTK_AWS_MCE_ENABLE) && defined(SUPPORT_ROLE_HANDOVER_SERVICE)
    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_GSOUND,
                            APPS_EVENTS_INTERACTION_GSOUND_RHO_PREPARE_READY,
                            NULL, 0, NULL, 0);
#endif
}

static void app_gsound_reset_parameter(void)
{
    gsound_on_connected_channel = 0;
    gsound_on_disconnected_channel = 0;
}

static uint32_t app_gsound_map_vp_index(uint8_t clip)
{
    uint32_t vp_index = 0xFFFFFFFF;
    switch(clip)
    {
        case GSOUND_PORT_AUDIO_OUT_CLIP_PREPARE_MIC_OPEN:
        case GSOUND_PORT_AUDIO_OUT_CLIP_FETCH:
        case GSOUND_PORT_AUDIO_OUT_CLIP_PTT_QUERY:
        case GSOUND_PORT_AUDIO_OUT_CLIP_REMOTE_MIC_OPEN:
            vp_index = VP_INDEX_BISTO_MIC_OPEN;
            break;
        case GSOUND_PORT_AUDIO_OUT_CLIP_QUERY_INTERRUPTED:
        case GSOUND_PORT_AUDIO_OUT_CLIP_PTT_REJECTED:
        case GSOUND_PORT_AUDIO_OUT_CLIP_REMOTE_MIC_CLOSE:
        case GSOUND_PORT_AUDIO_OUT_CLIP_PTT_MIC_CLOSE:
            vp_index = VP_INDEX_BISTO_MIC_CLOSE;
            break;
        case GSOUND_PORT_AUDIO_OUT_CLIP_GSOUND_NC:
            vp_index = VP_INDEX_BISTO_MIC_NOT_CONNECTED;
            break;
        default:
            break;
    }
    return vp_index;
}

static bool app_gsound_read_initial_state(bool selected)
{
#ifdef MTK_NVDM_ENABLE
    nvdm_status_t status;
    uint32_t size = 1;
    uint8_t initial_state = GSOUND_INITIAL_STATE_DISABLED;
    bool update_nvdm = false;
#endif

    /* GSound default initial state: Disable */
    /* [selected]-->true: GSound has been chosen before last power off. */

#ifdef MTK_NVDM_ENABLE
    status = nvdm_read_data_item(NVDM_GROUP_GSOUND, NVDM_GSOUND_ITEM_INIT_STATE, (uint8_t *)&initial_state, &size);

    g_port_log("[APP_GSOUND] NVDM Read status:%x, initial_state:%x", 2, status, initial_state);

    if (selected) {
        if ((status == NVDM_STATUS_ITEM_NOT_FOUND) ||
            ((status == NVDM_STATUS_OK) && (initial_state == GSOUND_INITIAL_STATE_DISABLED))) {
            initial_state = GSOUND_INITIAL_STATE_ENABLED;
            update_nvdm = true;
        }

    } else {
        if ((status == NVDM_STATUS_OK) && (initial_state == GSOUND_INITIAL_STATE_ENABLED)) {
            initial_state = GSOUND_INITIAL_STATE_DISABLED;
            update_nvdm = true;
        }
    }

    if (update_nvdm) {
        app_gsound_write_initial_state(initial_state);
    }
#endif

    g_port_log("[APP_GSOUND] Init! (selected:%x)", 1, selected);

    return selected;
}

static void app_gsound_write_initial_state(uint8_t initial_state)
{
#ifdef MTK_NVDM_ENABLE
    nvdm_status_t status;
    uint32_t size = 1;
    status = nvdm_write_data_item(NVDM_GROUP_GSOUND, NVDM_GSOUND_ITEM_INIT_STATE, NVDM_DATA_ITEM_TYPE_RAW_DATA,
                                  (uint8_t *)&initial_state, size);

    if(status != NVDM_STATUS_OK) {
        g_port_log("[APP_GSOUND] NVDM Write error:%x", 1, status);
    }
#endif
}

/**************************************************************************************************
* Public function
**************************************************************************************************/
void gsound_port_ota_reboot(void)
{
    g_port_log("[APP_GSOUND] gsound_port_ota_reboot \n\r", 0);

    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                        APPS_EVENTS_INTERACTION_REQUEST_REBOOT, NULL, 0,
                        NULL, 0);
}

// return 0 means VP enable success, else fail
uint32_t gsound_port_audio_out_start(uint8_t clip)
{
    uint32_t result = 0;
    uint32_t vp_index;

    g_port_log("[APP_GSOUND] gsound_port_audio_out_start:%d \n\r", 1, clip);

    if ((vp_index = app_gsound_map_vp_index(clip)) != 0xFFFFFFFF) {
        g_port_log("[APP_GSOUND] app_gsound_map_vp_index:%x \n\r", 1, vp_index);
        apps_config_set_vp(app_gsound_map_vp_index(clip), true, APP_GSOUND_VP_SYNC_TIME, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
    }

    return result;
}

void app_gsound_enable(void)
{
    GSoundStatus status;

    status = GSoundServiceEnable();
    g_port_log("[APP_GSOUND] GSound Enable, status:%x \n\r", 1, status);
}

void app_gsound_disable(void)
{
    GSoundStatus status;

    status = GSoundServiceDisable();
    g_port_log("[APP_GSOUND] GSound Disable, status:%x \n\r", 1, status);
}

bt_status_t gsound_port_info_get_serial(uint8_t *serial_num, uint32_t max_len)
{
    uint32_t nvkey_size = 0;
    uint8_t *sn = NULL, sn_length;
    app_gsound_info_nvkey_t *data;
    nvkey_status_t result;

    if (nvkey_data_item_length(NVKEYID_APP_GSOUND_INFO, &nvkey_size) != NVKEY_STATUS_OK) {
        g_port_log("[APP_GSOUND] get_serial length error \n\r", 0);
        return BT_STATUS_FAIL;
    }

    if ((data = pvPortMalloc(nvkey_size)) != NULL && (result = nvkey_read_data(NVKEYID_APP_GSOUND_INFO, data, &nvkey_size)) == NVKEY_STATUS_OK) {
        sn_length = data->serial_num_length;
        sn = data;

        g_port_log("[APP_GSOUND] gsound_port_info_get_serial:%x,%x \n\r", 2, data->module_id, sn_length);

        memcpy(serial_num, &sn[0 + MODULE_SN_LENGTH_SIZE], (sn_length > max_len) ? max_len : sn_length);
    }

    if(data != NULL) {
        vPortFree(data);
    }

    return (result == NVKEY_STATUS_OK) ? BT_STATUS_SUCCESS : BT_STATUS_FAIL;
}

bt_status_t gsound_port_info_get_app_version(uint8_t *version, uint32_t max_len)
{
    uint32_t nvkey_size = 0;
    uint8_t *ver = NULL, ver_length, sn_length;
    app_gsound_info_nvkey_t *data;
    nvkey_status_t result;

    if (nvkey_data_item_length(NVKEYID_APP_GSOUND_INFO, &nvkey_size) != NVKEY_STATUS_OK) {
        g_port_log("[APP_GSOUND] get_app_version length error \n\r", 0);
        return BT_STATUS_FAIL;
    }

    if ((data = pvPortMalloc(nvkey_size)) != NULL && (result = nvkey_read_data(NVKEYID_APP_GSOUND_INFO, data, &nvkey_size)) == NVKEY_STATUS_OK) {
        sn_length = data->serial_num_length;
        ver = data;
        ver_length = ver[0 + MODULE_SN_LENGTH_SIZE + sn_length];
        g_port_log("[APP_GSOUND] gsound_port_info_get_app_version:%x,%x,%x \n\r", 3, data->module_id, sn_length, ver_length);

        memcpy(version, &ver[0 + MODULE_SN_LENGTH_SIZE + sn_length + VER_LENGTH_SIZE], (ver_length > max_len) ? max_len : ver_length);

    }

    if(data != NULL) {
        vPortFree(data);
    }

    return (result == NVKEY_STATUS_OK) ? BT_STATUS_SUCCESS : BT_STATUS_FAIL;
}

bt_status_t gsound_port_info_get_device_id(uint32_t *device_id)
{
    uint32_t nvkey_size = 0;
    app_gsound_info_nvkey_t *data;
    nvkey_status_t result;

    if (nvkey_data_item_length(NVKEYID_APP_GSOUND_INFO, &nvkey_size) != NVKEY_STATUS_OK) {
        g_port_log("[APP_GSOUND] get_device_id length error \n\r", 0);
        return BT_STATUS_FAIL;
    }

    if ((data = pvPortMalloc(nvkey_size)) != NULL && (result = nvkey_read_data(NVKEYID_APP_GSOUND_INFO, data, &nvkey_size)) == NVKEY_STATUS_OK) {
        *device_id = data->module_id;
        g_port_log("[APP_GSOUND] gsound_port_info_get_device_id:%x \n\r", 1, data->module_id);
    }

    if(data != NULL) {
        vPortFree(data);
    }

    return (result == NVKEY_STATUS_OK) ? BT_STATUS_SUCCESS : BT_STATUS_FAIL;
}

void app_gsound_init(bool va_selected)
{
#ifdef MTK_GSOUND_AIR_SPP_ENABLE
    bt_gsound_spp_air_main();
    ble_bms_init();
#endif

    gsound_config.gsound_enabled = app_gsound_read_initial_state(va_selected);
    GSoundServiceInit(GSOUND_BUILD_ID, &gsound_config, &observer);

    gsound_port_register_callback();

    ble_ancs_app_init();
    ble_ams_init();

}

//temp, different project will have different value, should be got from Changjiang's API
#include "memory_map.h"
#ifndef LM_BASE
    #define LM_BASE 0x1233
#endif
uint32_t gsound_get_language_model_length(void)
{
    uint32_t len = 0xB99C;
    g_port_log("[APP_GSOUND] gsound_get_language_mode_length:%x \n\r", 1, len);
    return len;
}
uint32_t gsound_get_language_model_address(void)
{
    g_port_log("[APP_GSOUND] gsound_get_language_mode_address:%x \n\r", 1, LM_BASE);
    return LM_BASE;
}

#endif

