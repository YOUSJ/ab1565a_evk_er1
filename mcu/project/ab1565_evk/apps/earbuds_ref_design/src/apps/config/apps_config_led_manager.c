/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "apps_config_led_manager.h"
#include "apps_events_event_group.h"
#include "apps_events_interaction_event.h"
#include "ui_shell_manager.h"
#include "ui_shell_activity.h"

#ifdef MTK_AWS_MCE_ENABLE
#include "bt_connection_manager.h"
#include "bt_device_manager.h"
#include "bt_sink_srv.h"
#include "bt_aws_mce_srv.h"
#endif

#include "app_led_control.h"
#include "apps_debug.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "semphr.h"
#include <limits.h>

#define LOG_TAG     "[LED_MANAGER] "
#define LED_FG_TIMER_NAME       "LED_FG"
#define LED_FG_TIMEOUT_UNIT_MS  (100)

#define LED_PATTERN_DEFAULT_SYNC_TIME   (600)
#define LED_PATTERN_INVALID_INDEX   (0xFF)

static bool fg_display = false;
static TimerHandle_t s_timer_handle = NULL;

static uint8_t s_playing_bg_index = LED_PATTERN_INVALID_INDEX;

#ifdef MTK_AWS_MCE_ENABLE

static uint8_t background_self_index = LED_PATTERN_INVALID_INDEX;
static uint8_t s_background_self_priority = APPS_CONFIG_LED_AWS_SYNC_PRIO_INVALID;

static struct {
    bool need_sync_bg;
    bool background_synced;
    bool foreground_synced;
    uint8_t background_sync_index;
    uint8_t background_sync_priority;
} s_led_sync_info = {
    false,
    false,
    false,
    LED_PATTERN_INVALID_INDEX,
    APPS_CONFIG_LED_AWS_SYNC_PRIO_INVALID
};

typedef struct {
    uint8_t pattern_type;
    uint8_t pattern_index;
    uint8_t sync_priority;
    uint16_t timeout;   // Only for LED_PATTERN_FG patterns
} apps_config_led_manager_pattern_info_t;


static bool _partner_send_resync_bg_request(void)
{
    apps_config_led_manager_pattern_info_t pattern_info = {
        .pattern_type = LED_PATTERN_NONE,
    };
    bt_aws_mce_report_info_t mce_repo_info = {
        .module_id = BT_AWS_MCE_REPORT_MODULE_LED,
        .is_sync = false,
        .sync_time = 0,
        .param_len = sizeof(apps_config_led_manager_pattern_info_t),
        .param = &pattern_info
    };
    return bt_aws_mce_report_send_event(&mce_repo_info) == BT_STATUS_SUCCESS;
}
#endif

bool _process_background_led_pattern(uint8_t index, uint32_t sync_time, bool force_update)
{
    APPS_LOG_MSGID_I(LOG_TAG"BG LED pattern = %d, sync_time = %d, ", 2, index, sync_time);

    // If s_playing_bg_index == index, it's the same pattern, don't update it.
    if (force_update || s_playing_bg_index != index) {
        s_playing_bg_index = index;
#if LED_ENABLE
        return app_led_control_enable_1(LED_PATTERN_BG, index, false, sync_time);
#else
        return true;
#endif
    } else {
        return true;
    }
}

#ifdef MTK_AWS_MCE_ENABLE
static bool _sync_background_led_pattern(uint8_t index, uint8_t priority)
{
    bt_status_t send_result; 
    apps_config_led_manager_pattern_info_t pattern_info = {
        .pattern_type = LED_PATTERN_BG,
        .pattern_index = index,
        .sync_priority = priority,
        .timeout = 0
    };
    bt_aws_mce_report_info_t mce_repo_info = {
        .module_id = BT_AWS_MCE_REPORT_MODULE_LED,
        .is_sync = true,
        .sync_time = LED_PATTERN_DEFAULT_SYNC_TIME,
        .param_len = sizeof(apps_config_led_manager_pattern_info_t),
        .param = &pattern_info
    };

    send_result = bt_aws_mce_report_send_sync_event(&mce_repo_info);
    if (BT_STATUS_SUCCESS != send_result) {
        APPS_LOG_MSGID_I(LOG_TAG"_sync_background_led_pattern fail, error = %x",
                1, send_result);
        return false;
    } else{
        return true;
    }
}
#endif

bool apps_config_set_background_led_pattern(uint8_t index,
                                            bool need_sync,
                                            apps_config_led_manager_aws_sync_priority_t priority)
{
    bool need_update_pattern = true;
    bool force_update = false; /* Update even the last time is the same pattern. */
    bool ret;
    uint32_t delay_time = 0;

    APPS_LOG_MSGID_I(LOG_TAG"apps_config_set_background_led_pattern, index: %d, need_sync: %d",
            2, index, need_sync);

#ifdef MTK_AWS_MCE_ENABLE
    if (BT_AWS_MCE_ROLE_AGENT == bt_device_manager_aws_local_info_get_role()) {
        if (!need_sync) {
            // If last time background_synced and this time doesn't need sync, notify partner.
            if (s_led_sync_info.background_synced) {
                apps_config_led_manager_pattern_info_t pattern_info = {
                    .pattern_type = LED_PATTERN_BG,
                    .pattern_index = LED_PATTERN_INVALID_INDEX,
                    .sync_priority = APPS_CONFIG_LED_AWS_SYNC_PRIO_INVALID,
                    .timeout = 0
                };
                bt_aws_mce_report_info_t mce_repo_info = {
                    .module_id = BT_AWS_MCE_REPORT_MODULE_LED,
                    .is_sync = false,
                    .sync_time = 0,
                    .param_len = sizeof(apps_config_led_manager_pattern_info_t),
                    .param = &pattern_info
                };
                if (BT_STATUS_FAIL == bt_aws_mce_report_send_event(&mce_repo_info)) {
                    APPS_LOG_MSGID_E(LOG_TAG"Fail to notify partner to disable sync bg pattern", 0);
                } else {
                    APPS_LOG_MSGID_I(LOG_TAG"Success to notify partner to disable sync bg pattern", 0);
                }
            }

            delay_time = 0;
            s_led_sync_info.background_synced = false;
            s_led_sync_info.background_sync_index = LED_PATTERN_INVALID_INDEX;
        } else {
            if (s_led_sync_info.background_synced && index == s_led_sync_info.background_sync_index) {
                APPS_LOG_MSGID_E(LOG_TAG"The same bg index need sync, ignore", 0);
            } else {
                if (BT_AWS_MCE_SRV_LINK_NONE != bt_aws_mce_srv_get_link_type()
                        && _sync_background_led_pattern(index, priority)) {
                    APPS_LOG_MSGID_E(LOG_TAG"Success to sync new bg pattern to partner", 0);
                    s_led_sync_info.background_synced = true;
                    s_led_sync_info.background_sync_index = index;
                    s_led_sync_info.background_sync_priority = priority;
                    force_update = true;
                    delay_time = LED_PATTERN_DEFAULT_SYNC_TIME;
                } else {
                    APPS_LOG_MSGID_E(LOG_TAG"Fail to sync new bg pattern to partner", 0);
                    delay_time = 0;
                    s_led_sync_info.background_synced = false;
                    s_led_sync_info.background_sync_index = LED_PATTERN_INVALID_INDEX;
                }
            }
        }
    } else { // Partner
        if (!need_sync) {
            if (s_led_sync_info.background_synced && priority <= s_led_sync_info.background_sync_priority) {
                if (BT_AWS_MCE_SRV_LINK_NONE != bt_aws_mce_srv_get_link_type()) {
                    APPS_LOG_MSGID_I(LOG_TAG"Partner ignore new bg pattern, because background_synced", 0);
                    need_update_pattern = false;
                    // If last time playing self LED pattern but this time need play sync pattern
                    if (!s_led_sync_info.need_sync_bg
                            && s_background_self_priority > s_led_sync_info.background_sync_priority) {
                        APPS_LOG_MSGID_I(LOG_TAG"partner notify re-sync when self BG changed", 0);
                        _partner_send_resync_bg_request();
                    }
                } else {
                    APPS_LOG_MSGID_E(LOG_TAG"Partner state error, why background_synced but aws not connected", 0);
                }
            }
        } else {
            if (BT_AWS_MCE_SRV_LINK_NONE != bt_aws_mce_srv_get_link_type()) {
                if (s_led_sync_info.background_synced
                        && s_led_sync_info.background_sync_index != APPS_CONFIG_LED_AWS_SYNC_PRIO_INVALID) {
                    APPS_LOG_MSGID_I(LOG_TAG"partner ignore need sync bg pattern when attached", 0);
                    need_update_pattern = false;
                    if (!s_led_sync_info.need_sync_bg
                            && s_background_self_priority > s_led_sync_info.background_sync_priority
                            && s_led_sync_info.background_synced
                            && s_led_sync_info.background_sync_index != APPS_CONFIG_LED_AWS_SYNC_PRIO_INVALID) {
                        APPS_LOG_MSGID_I(LOG_TAG"partner notify re-sync when self BG changed nosync->sync", 0);
                        _partner_send_resync_bg_request();
                    }
                } else {
                    APPS_LOG_MSGID_I(LOG_TAG"partner display self sync pattern when agent never sync", 0);
                    delay_time = 0;
                }
            }
        }
    }
    s_led_sync_info.need_sync_bg = need_sync;
    s_background_self_priority = priority;
    background_self_index = index;
#endif
    if (need_update_pattern) {
        ret = _process_background_led_pattern(index, delay_time, force_update);
    } else {
        ret = true;
    }

    return ret;
}

static void _led_fg_time_out_callback( TimerHandle_t xTimer )
{
    APPS_LOG_MSGID_I(LOG_TAG"FG LED pattern timeout", 0);
    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_LED_MANAGER,
            APPS_EVENTS_LED_FG_PATTERN_TIMEOUT, NULL, 0, NULL, 0);
    fg_display = false;
}

void apps_config_check_foreground_led_pattern(void)
{
    if (!fg_display) {
        APPS_LOG_MSGID_I(LOG_TAG"Disable FG LED pattern", 0);
#if LED_ENABLE
        app_led_control_disable(LED_PATTERN_FG, false);
#endif
#ifdef MTK_AWS_MCE_ENABLE
        if (BT_AWS_MCE_SRV_LINK_NONE != bt_aws_mce_srv_get_link_type()) {
            bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
            if (BT_AWS_MCE_ROLE_AGENT == role) {
                if (s_led_sync_info.need_sync_bg) {
                    // When fg pattern timeout, re-sync bg pattern
                    APPS_LOG_MSGID_I(LOG_TAG"Agent re-sync bg pattern when fg timeout", 0);
                    if (_sync_background_led_pattern(background_self_index, s_background_self_priority)) {
                        _process_background_led_pattern(background_self_index, LED_PATTERN_DEFAULT_SYNC_TIME, true);
                        s_led_sync_info.background_synced = true;
                        s_led_sync_info.background_sync_index = background_self_index;
                        s_led_sync_info.background_sync_priority = s_background_self_priority;
                    } else {
                        s_led_sync_info.background_synced = false;
                        s_led_sync_info.background_sync_index = LED_PATTERN_INVALID_INDEX;
                    }
                }
            } else {
                // Send re-sync bg pattern request to agent.
                // If foreground_synced, consider the agent also timeout at the same time, no need send re-sync request
                if (!s_led_sync_info.foreground_synced && s_led_sync_info.background_synced
                        && s_background_self_priority <= s_led_sync_info.background_sync_priority) {
                    APPS_LOG_MSGID_I(LOG_TAG"Partner send re-sync request when FG end", 0);
                    _partner_send_resync_bg_request();
                }
            }
        } else {
            APPS_LOG_MSGID_I(LOG_TAG"NOT attached, when fg timeout", 0);
        }
        s_led_sync_info.foreground_synced = false;
#endif
    }
}

static bool _process_foreground_led_pattern(uint8_t index, uint16_t timeout, uint32_t sync_time)
{
    bool ret = false;
    xTimerStop(s_timer_handle, 0);
    xTimerChangePeriod(s_timer_handle, timeout * LED_FG_TIMEOUT_UNIT_MS / portTICK_PERIOD_MS + sync_time, 0);
    APPS_LOG_MSGID_I(LOG_TAG"FG LED pattern = %d, timeout = %d * 100ms, sync_time = %d",
            3, index, timeout, sync_time);

    fg_display = true;
#if LED_ENABLE
    ret = app_led_control_enable_1(LED_PATTERN_FG, index, false, sync_time);
#else
    ret = true;
#endif
    xTimerStart(s_timer_handle, 0);

    return ret;
}

bool apps_config_set_foreground_led_pattern(uint8_t index, uint16_t timeout, bool need_sync)
{
    uint32_t delay_time = 0;
    bool ret;

    APPS_LOG_MSGID_I(LOG_TAG"apps_config_set_foreground_led_pattern, index: %d, timeout: %d, need_sync: %d",
            3, index, timeout, need_sync);

#ifdef MTK_AWS_MCE_ENABLE
    if (need_sync
            && BT_AWS_MCE_SRV_LINK_NONE != bt_aws_mce_srv_get_link_type()) {
        bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
        if (BT_AWS_MCE_ROLE_CLINET != role && BT_AWS_MCE_ROLE_PARTNER != role) {
            apps_config_led_manager_pattern_info_t pattern_info = {
                .pattern_type = LED_PATTERN_FG,
                .pattern_index = index,
                .timeout = timeout
            };
            bt_aws_mce_report_info_t mce_repo_info = {
                .module_id = BT_AWS_MCE_REPORT_MODULE_LED,
                .is_sync = true,
                .sync_time = LED_PATTERN_DEFAULT_SYNC_TIME,
                .param_len = sizeof(apps_config_led_manager_pattern_info_t),
                .param = &pattern_info
            };
            if (BT_STATUS_FAIL != bt_aws_mce_report_send_sync_event(&mce_repo_info)) {
                delay_time = mce_repo_info.sync_time;
                s_led_sync_info.foreground_synced = true;
            } else {
                s_led_sync_info.foreground_synced = false;
                APPS_LOG_MSGID_W(LOG_TAG"apps_config_set_foreground_led_pattern, failed to sync", 0);
            }
        } else {
            // If AWS connected, pattern doesn't display sync LED pattern from itself.
            APPS_LOG_MSGID_I(LOG_TAG"apps_config_set_foreground_led_pattern, partner ignore", 0);
            return true;
        }
    } else {
        APPS_LOG_MSGID_I(LOG_TAG"apps_config_set_foreground_led_pattern, not need sync", 0);
        s_led_sync_info.foreground_synced = false;
    }
#endif
    ret = _process_foreground_led_pattern(index, timeout, delay_time);

    return ret;
}

#ifdef MTK_AWS_MCE_ENABLE
void apps_config_led_manager_on_aws_attached(bt_aws_mce_role_t role, bool attached)
{
    APPS_LOG_MSGID_E(LOG_TAG"apps_config_led_manager_on_aws_attached, role = %x, attached = %d",
            2, role, attached);

    if (BT_AWS_MCE_ROLE_AGENT == role && s_led_sync_info.need_sync_bg) {
        if (attached) {
            if (_sync_background_led_pattern(background_self_index, s_background_self_priority)) {
                _process_background_led_pattern(background_self_index, LED_PATTERN_DEFAULT_SYNC_TIME, true);
                s_led_sync_info.background_synced = true;
                s_led_sync_info.background_sync_index = background_self_index;
                s_led_sync_info.background_sync_priority = s_background_self_priority;
            } else {
                s_led_sync_info.background_synced = false;
                s_led_sync_info.background_sync_index = LED_PATTERN_INVALID_INDEX;
            }
        } else {
            s_led_sync_info.background_synced = false;
            s_led_sync_info.background_sync_index = LED_PATTERN_INVALID_INDEX;
        }
    } else if (BT_AWS_MCE_ROLE_PARTNER == role && !attached) {
        _process_background_led_pattern(background_self_index, 0, false);
        s_led_sync_info.background_synced = false;
        s_led_sync_info.background_sync_index = LED_PATTERN_INVALID_INDEX;
    }

}

void app_config_led_sync(bt_aws_mce_report_info_t *info, uint32_t tick_ms)
{
    if (info && info->param && info->param_len) {
        apps_config_led_manager_pattern_info_t *p_pattern_info =
                (apps_config_led_manager_pattern_info_t *)info->param;
        if (info->sync_time > INT_MAX) {
            info->sync_time = 0;
        }
        if (LED_PATTERN_FG == p_pattern_info->pattern_type) {
            APPS_LOG_MSGID_I(LOG_TAG"app_config_led_sync, sync fg index %d from agent", 1, p_pattern_info->pattern_index);
            uint32_t cost_time_ms = xTaskGetTickCount() * portTICK_PERIOD_MS - tick_ms;
            uint32_t sync_time_ms = 0;
            if (cost_time_ms < info->sync_time) {
                APPS_LOG_MSGID_I(LOG_TAG"app_config_led_sync, sync_time:%d ms, cost tik: %d ms", 2, info->sync_time, cost_time_ms);
                sync_time_ms = info->sync_time - cost_time_ms;
            }
            APPS_LOG_MSGID_I(LOG_TAG"app_config_led_sync, remain sync time:%d ms", 1, sync_time_ms);
            _process_foreground_led_pattern(p_pattern_info->pattern_index, p_pattern_info->timeout, sync_time_ms);
        } else if (LED_PATTERN_BG == p_pattern_info->pattern_type) {
            APPS_LOG_MSGID_I(LOG_TAG"app_config_led_sync, sync bg index %d from agent", 1, p_pattern_info->pattern_index);
            if (LED_PATTERN_INVALID_INDEX == p_pattern_info->pattern_index) {
                _process_background_led_pattern(background_self_index, 0, false);
                s_led_sync_info.background_synced = false;
            } else {
                s_led_sync_info.background_synced = true;
                if (p_pattern_info->sync_priority >= s_background_self_priority) {
                    uint32_t cost_time_ms = xTaskGetTickCount() * portTICK_PERIOD_MS - tick_ms;
                    uint32_t sync_time_ms = 0;
                    if (cost_time_ms < info->sync_time) {
                        sync_time_ms = info->sync_time - cost_time_ms;
                        APPS_LOG_MSGID_I(LOG_TAG"app_config_led_sync, sync_time:%d ms, cost tik: %d ms", 2, info->sync_time, cost_time_ms);
                    }
                    APPS_LOG_MSGID_I(LOG_TAG"app_config_led_sync, remain sync time:%d ms", 1, sync_time_ms);
                    _process_background_led_pattern(p_pattern_info->pattern_index, sync_time_ms, true);
                }
            }
            s_led_sync_info.background_sync_index = p_pattern_info->pattern_index;
            s_led_sync_info.background_sync_priority = p_pattern_info->sync_priority;
        } else if (LED_PATTERN_NONE == p_pattern_info->pattern_type) {
            // Agent receive re-sync request from partner.
            bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
            if (BT_AWS_MCE_ROLE_AGENT == role && s_led_sync_info.need_sync_bg) {
                APPS_LOG_MSGID_I(LOG_TAG"app_config_led_sync, receive re-sync", 0);
                if (_sync_background_led_pattern(background_self_index, s_background_self_priority)) {
                    s_led_sync_info.background_synced = true;
                    s_led_sync_info.background_sync_index = background_self_index;
                    s_led_sync_info.background_sync_priority = s_background_self_priority;
                    APPS_LOG_MSGID_I(LOG_TAG"success to re-sync", 0);
                    _process_background_led_pattern(background_self_index, LED_PATTERN_DEFAULT_SYNC_TIME, true);
                } else {
                    s_led_sync_info.background_synced = false;
                    s_led_sync_info.background_sync_index = LED_PATTERN_INVALID_INDEX;
                    APPS_LOG_MSGID_E(LOG_TAG"fail to re-sync", 0);
                }
            }
        }
    }
}


void app_led_sync_callback(bt_aws_mce_report_info_t *para)
{
    APPS_LOG_MSGID_I(LOG_TAG"app_led_sync_callback: send aws report to app: module 0x%0x", 1, para->module_id);
    apps_config_led_sync_info_t *extra_data = pvPortMalloc(sizeof(apps_config_led_sync_info_t) + para->param_len);
    if (extra_data == NULL) {
        APPS_LOG_MSGID_I(LOG_TAG"app_led_sync_callback: malloc fail", 0);
        return;
    }

    memcpy(&(extra_data->aws_info), para, sizeof(bt_aws_mce_report_info_t));
    extra_data->tick_ms = xTaskGetTickCount() * portTICK_PERIOD_MS;
    extra_data->aws_info.param = (void *)extra_data + sizeof(apps_config_led_sync_info_t);
    memcpy(extra_data->aws_info.param, para->param, para->param_len);

    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_LED_MANAGER,
            APPS_EVENTS_LED_SYNC_LED_PATTERN, extra_data, sizeof(apps_config_led_sync_info_t) + para->param_len,
                    NULL, 0);
}
#endif

void apps_config_led_manager_init(void)
{
    s_timer_handle = xTimerCreate(LED_FG_TIMER_NAME, 100, pdFALSE, 0, _led_fg_time_out_callback);
#ifdef MTK_AWS_MCE_ENABLE
    bt_aws_mce_report_register_callback(BT_AWS_MCE_REPORT_MODULE_LED, app_led_sync_callback);
#endif
}
