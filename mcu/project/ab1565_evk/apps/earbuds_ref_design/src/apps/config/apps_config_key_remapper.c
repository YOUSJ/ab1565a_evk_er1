/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include <stddef.h>

#include "apps_config_key_remapper.h"
#include "apps_debug.h"
#include "FreeRTOS.h"
#include "nvkey.h"
#include "nvkey_id_list.h"
#ifdef MTK_AWS_MCE_ENABLE
#include "bt_sink_srv_ami.h"
#endif
#include "hal_pmu.h"

#include <string.h>

#define LOG_TAG "[Key_remap] "
static apps_config_state_t s_mmi_state = APP_BT_OFF;

static void _init_default_configurable_table(void);

typedef enum {
        APPS_CONFIG_KEY_RELEASE = 0,        /* AIRO_KEY_RELEASE */
        APPS_CONFIG_KEY_SHORT_CLICK,        /* AIRO_KEY_SHORT_CLICK */
        APPS_CONFIG_KEY_DOUBLE_CLICK,       /* AIRO_KEY_DOUBLE_CLICK */
        APPS_CONFIG_KEY_TRIPLE_CLICK,       /* AIRO_KEY_TRIPLE_CLICK */
        APPS_CONFIG_KEY_LONG_PRESS_1,       /* AIRO_KEY_LONG_PRESS_1 */
        APPS_CONFIG_KEY_LONG_PRESS_2,       /* AIRO_KEY_LONG_PRESS_2 */
        APPS_CONFIG_KEY_LONG_PRESS_3,       /* AIRO_KEY_LONG_PRESS_3 */
        APPS_CONFIG_KEY_SLONG,         /* AIRO_KEY_SLONG */
        APPS_CONFIG_KEY_REPEAT,             /* AIRO_KEY_REPEAT */
        APPS_CONFIG_KEY_PRESS,              /* AIRO_KEY_PRESS */
        APPS_CONFIG_KEY_LONG_PRESS_RELEASE_1,   /* AIRO_KEY_LONG_PRESS_RELEASE_1 */
        APPS_CONFIG_KEY_LONG_PRESS_RELEASE_2,   /* AIRO_KEY_LONG_PRESS_RELEASE_2 */
        APPS_CONFIG_KEY_LONG_PRESS_RELEASE_3,   /* AIRO_KEY_LONG_PRESS_RELEASE_3 */
        APPS_CONFIG_KEY_SLONG_RELEASE,          /* AIRO_KEY_SLONG_RELEASE */
        APPS_CONFIG_KEY_INVALID,            /* AIRO_KEY_INVALID */
} apps_config_serialized_event_t;

static apps_config_configurable_table_t *s_configurable_table = NULL;
static uint8_t s_configurable_table_len = 0;
static bool s_configurable_table_ready = false;

// Add table in a customerized folder
#include "customerized_key_config.c"

typedef struct {
    uint32_t map_size;
    const apps_config_key_event_map_t *key_map;
} apps_config_key_event_map_list_t;

const static apps_config_key_event_map_list_t const s_map_list[APPS_CONFIG_KEY_INVALID] = {
        { sizeof(temp_key_release_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_release_configs },
        { sizeof(temp_key_short_click_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_short_click_configs },
        { sizeof(temp_key_double_click_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_double_click_configs },
        { sizeof(temp_key_triple_click_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_triple_click_configs },
        { sizeof(temp_key_long_press1_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_long_press1_configs },
        { sizeof(temp_key_long_press2_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_long_press2_configs },
        { sizeof(temp_key_long_press3_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_long_press3_configs },
        { sizeof(temp_key_slong_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_slong_configs },
        { sizeof(temp_key_repeat_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_repeat_configs },
        { sizeof(temp_key_press_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_press_configs },
        { sizeof(temp_key_long_press_release1_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_long_press_release1_configs },
        { sizeof(temp_key_long_press_release2_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_long_press_release2_configs },
        { sizeof(temp_key_long_press_release3_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_long_press_release3_configs },
        { sizeof(temp_key_slong_release_configs) / sizeof(apps_config_key_event_map_t),
                temp_key_slong_release_configs },
};


#ifdef MTK_AWS_MCE_ENABLE
// If it's AWS project, the left earbuds use this map
const static apps_config_key_event_map_list_t const s_left_map_list[APPS_CONFIG_KEY_INVALID] = {
        { sizeof(temp_left_key_release_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_release_configs},
        { sizeof(temp_left_key_short_click_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_short_click_configs},
        { sizeof(temp_left_key_double_click_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_double_click_configs},
        { sizeof(temp_left_key_triple_click_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_triple_click_configs},
        { sizeof(temp_left_key_long_press1_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_long_press1_configs},
        { sizeof(temp_left_key_long_press2_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_long_press2_configs},
        { sizeof(temp_left_key_long_press3_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_long_press3_configs},
        { sizeof(temp_left_key_slong_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_slong_configs},
        { sizeof(temp_left_key_repeat_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_repeat_configs},
        { sizeof(temp_left_key_press_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_press_configs},
        { sizeof(temp_left_key_long_press_release1_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_long_press_release1_configs},
        { sizeof(temp_left_key_long_press_release2_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_long_press_release2_configs},
        { sizeof(temp_left_key_long_press_release3_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_long_press_release3_configs},
        { sizeof(temp_left_key_slong_release_configs) / sizeof(apps_config_key_event_map_t),
                temp_left_key_slong_release_configs},
};
#endif

apps_config_key_action_t apps_config_key_event_remapper_map_action(
        uint8_t key_id,
        airo_key_event_t key_event)
{
    apps_config_key_action_t action_id = KEY_ACTION_INVALID;
    size_t i;
    apps_config_serialized_event_t serialized_event_id = APPS_CONFIG_KEY_INVALID;
    const apps_config_key_event_map_list_t *mapped_list = NULL;

    switch (key_event) {
    case AIRO_KEY_RELEASE:
        serialized_event_id = APPS_CONFIG_KEY_RELEASE;
        break;
    case AIRO_KEY_SHORT_CLICK:
        serialized_event_id = APPS_CONFIG_KEY_SHORT_CLICK;
        break;
    case AIRO_KEY_DOUBLE_CLICK:
        serialized_event_id = APPS_CONFIG_KEY_DOUBLE_CLICK;
        break;
    case AIRO_KEY_TRIPLE_CLICK:
        serialized_event_id = APPS_CONFIG_KEY_TRIPLE_CLICK;
        break;
    case AIRO_KEY_LONG_PRESS_1:
        serialized_event_id = APPS_CONFIG_KEY_LONG_PRESS_1;
        break;
    case AIRO_KEY_LONG_PRESS_2:
        serialized_event_id = APPS_CONFIG_KEY_LONG_PRESS_2;
        break;
    case AIRO_KEY_LONG_PRESS_3:
        serialized_event_id = APPS_CONFIG_KEY_LONG_PRESS_3;
        break;
    case AIRO_KEY_SLONG:
        serialized_event_id = APPS_CONFIG_KEY_SLONG;
        break;
    case AIRO_KEY_REPEAT:
        serialized_event_id = APPS_CONFIG_KEY_REPEAT;
        break;
    case AIRO_KEY_PRESS:
        serialized_event_id = APPS_CONFIG_KEY_PRESS;
        break;
    case AIRO_KEY_LONG_PRESS_RELEASE_1:
        serialized_event_id = APPS_CONFIG_KEY_LONG_PRESS_RELEASE_1;
        break;
    case AIRO_KEY_LONG_PRESS_RELEASE_2:
        serialized_event_id = APPS_CONFIG_KEY_LONG_PRESS_RELEASE_2;
        break;
    case AIRO_KEY_LONG_PRESS_RELEASE_3:
        serialized_event_id = APPS_CONFIG_KEY_LONG_PRESS_RELEASE_3;
        break;
    case AIRO_KEY_SLONG_RELEASE:
        serialized_event_id = APPS_CONFIG_KEY_SLONG_RELEASE;
        break;
    case AIRO_KEY_INVALID:
        serialized_event_id = APPS_CONFIG_KEY_INVALID;
        break;
    default:
        break;
    }

    if (serialized_event_id < APPS_CONFIG_KEY_INVALID) {
        if (!s_configurable_table && !s_configurable_table_ready) {
            _init_default_configurable_table();
        }
        if (s_configurable_table) {
            for (i = 0; i < s_configurable_table_len; i++) {
                if (serialized_event_id == s_configurable_table[i].key_event
                        && key_id == s_configurable_table[i].key_id
                        && ((1 << s_mmi_state) & s_configurable_table[i].supported_states)) {
                    action_id = s_configurable_table[i].app_key_event;
                    APPS_LOG_MSGID_I(LOG_TAG"Current used key_id from configuration_table = 0x%x, key_event = 0x%x, state = %d, action_id = 0x%x", 4, key_id, key_event, s_mmi_state, action_id);
                    return action_id;
                }
            }
        }

#if MTK_AWS_MCE_ENABLE
        // When device is left, use the left mapping tables
        if (AUDIO_CHANNEL_L == ami_get_audio_channel()) {
            mapped_list = &s_left_map_list[serialized_event_id];     
        } else
#endif
        {
            // Use the default mapping tables
            mapped_list = &s_map_list[serialized_event_id];
        }
        /* For debug
        APPS_LOG_MSGID_I(LOG_TAG"Current used key map [%d]", 1, serialized_event_id);
        for (j = 0; j < s_map_list[serialized_event_id].map_size; j++) {
            APPS_LOG_MSGID_I(LOG_TAG"%d, key_id = %x,  app_key_event = %x, states = %x", 4, j, s_map_list[serialized_event_id].key_map[j].key_id,
            s_map_list[serialized_event_id].key_map[j].app_key_event,
            s_map_list[serialized_event_id].key_map[j].supported_states);
        }
        */

        for (i = 0; i < mapped_list->map_size && mapped_list->key_map; i++) {
            if (key_id == mapped_list->key_map[i].key_id
                        && ((1 << s_mmi_state) & mapped_list->key_map[i].supported_states)) {
                action_id = mapped_list->key_map[i].app_key_event;
                break;
            }
        }
    }
    APPS_LOG_MSGID_I(LOG_TAG"Current used key_id = 0x%x, key_event = 0x%x, state = %d, action_id = 0x%x", 4, key_id, key_event, s_mmi_state, action_id);

    return action_id;
}

void apps_config_key_set_mmi_state(apps_config_state_t state)
{
    s_mmi_state = state;
    APPS_LOG_MSGID_I(LOG_TAG"apps_config_key_set_mmi_state = %d", 1, s_mmi_state);
}

apps_config_state_t apps_config_key_get_mmi_state(void)
{
    return s_mmi_state;
}

static void _init_default_configurable_table(void)
{
    const apps_config_configurable_table_t *hard_code_table = NULL;
    uint32_t hard_code_table_size = 0;
    nvkey_status_t nvkey_ret;
#if MTK_AWS_MCE_ENABLE
    audio_channel_t channel = ami_get_audio_channel();
    if (AUDIO_CHANNEL_L == channel) {
        hard_code_table = left_configurable_table;
        hard_code_table_size = sizeof(left_configurable_table);
    } else if (AUDIO_CHANNEL_R == channel) {
        hard_code_table = default_configurable_table;
        hard_code_table_size = sizeof(default_configurable_table);
    } else {
        APPS_LOG_MSGID_E(LOG_TAG"_init_default_configurable_table current audio channel not correct", 0);
    }
#else
    hard_code_table = default_configurable_table;
    hard_code_table_size = sizeof(default_configurable_table);
#endif
    if (hard_code_table) {
        if (hard_code_table_size) {
            s_configurable_table = (apps_config_configurable_table_t *)pvPortMalloc(hard_code_table_size);
            if (s_configurable_table) {
                memcpy(s_configurable_table, hard_code_table, hard_code_table_size);
                s_configurable_table_len = hard_code_table_size / sizeof(apps_config_configurable_table_t);
                nvkey_ret = nvkey_write_data(NVKEYID_CONFIGURABLE_KEY_ACTION_TABLE,
                                            (uint8_t *)s_configurable_table,
                                            hard_code_table_size);
                APPS_LOG_MSGID_E(LOG_TAG"_init_default_configurable_table init ret = %d, size = %d",
                                2, nvkey_ret, s_configurable_table_len);
            }
        }
        s_configurable_table_ready = true;
    }
}

void apps_config_key_remaper_init_configurable_table(void)
{
    uint32_t size = 0;
    bool push_to_work = false;
    uint32_t i;
    nvkey_status_t ret = nvkey_data_item_length(NVKEYID_CONFIGURABLE_KEY_ACTION_TABLE, &size);
    
    APPS_LOG_MSGID_I(LOG_TAG"apps_config_key_remaper_init_configurable_table", 0);
    if (NVKEY_STATUS_ITEM_NOT_FOUND == ret || size < sizeof(apps_config_configurable_table_t)) {
        _init_default_configurable_table();
    } else if (NVKEY_STATUS_OK == ret) {
        if (s_configurable_table
                && size != s_configurable_table_len * sizeof(apps_config_configurable_table_t)) {
            vPortFree(s_configurable_table);
            s_configurable_table = NULL;
        }
        if (!s_configurable_table) {
            s_configurable_table = (apps_config_configurable_table_t *)pvPortMalloc(size);
        }
        s_configurable_table_len = 0;
        if (s_configurable_table) {
            ret = nvkey_read_data(NVKEYID_CONFIGURABLE_KEY_ACTION_TABLE,
                    (uint8_t *)s_configurable_table, &size);
            if (NVKEY_STATUS_OK == ret) {
                s_configurable_table_len = size / (sizeof(apps_config_configurable_table_t));
                APPS_LOG_MSGID_I(LOG_TAG"apps_config_key_remaper_init_configurable_table success, s_configurable_table_len = %d",
                        1, s_configurable_table_len);
            } else {
                APPS_LOG_MSGID_E(LOG_TAG"apps_config_key_remaper_init_configurable_table, read data fail : %d", 1, ret);
                vPortFree(s_configurable_table);
                s_configurable_table = NULL;
            }
        } else {
            APPS_LOG_MSGID_E(LOG_TAG"apps_config_key_remaper_init_configurable_table, malloc fail", 0);
        }
    } else {
        APPS_LOG_MSGID_W(LOG_TAG"apps_config_key_remaper_init_configurable_table, read size ret = %d", 1, ret);
        if (s_configurable_table) {
            vPortFree(s_configurable_table);
            s_configurable_table = NULL;
            s_configurable_table_len = 0;
        }
    }

    // Special code. Because GSOUND need push to talk, must disable HW power off when press power key.
    for (i = 0; i < s_configurable_table_len; i++)
    {
        if (APPS_CONFIG_KEY_RELEASE == s_configurable_table[i].key_event) {
            push_to_work = true;
            break;
        }
    }
    if (push_to_work) {
        pmu_pwrkey_enable(PMU_OFF);
    } else {
        pmu_pwrkey_enable(PMU_ON);
    }
}

#ifdef MULTI_VA_SUPPORT_COMPETITION
void apps_config_key_remapper_set_va_type(multi_va_type_t old_va_type,  multi_va_type_t new_va_type)
{
    const apps_config_configurable_table_t *target_table = NULL;
    uint32_t table_size = 0;
    APPS_LOG_MSGID_E(LOG_TAG"set_va_type, %x->%x", 2, old_va_type, new_va_type);
    switch (new_va_type) {
#ifdef MTK_AMA_ENABLE
        case MULTI_VA_TYPE_AMA:
            target_table = ama_configurable_table;
            table_size = sizeof(ama_configurable_table);
            break;
#endif
#ifdef GSOUND_LIBRARY_ENABLE
        case MULTI_VA_TYPE_GSOUND:
            target_table = gsound_configurable_table;
            table_size = sizeof(gsound_configurable_table);
            break;
#endif
        case MULTI_VA_TYPE_UNKNOWN:
            _init_default_configurable_table();
            break;
        default:
            APPS_LOG_MSGID_E(LOG_TAG"set_va_type, param error : %x", 1, new_va_type);
            break;
    }

    if (target_table) {
        nvkey_status_t ret = nvkey_write_data(NVKEYID_CONFIGURABLE_KEY_ACTION_TABLE, (uint8_t *)target_table, table_size);
        APPS_LOG_MSGID_E(LOG_TAG"set_va_type, table : %x, size: %d", 2, target_table, table_size);
        if (NVKEY_STATUS_OK == ret) {
            apps_config_key_remaper_init_configurable_table();
        } else {
            APPS_LOG_MSGID_E(LOG_TAG"set_va_type, write nvkey fail : %d", 1, ret);
        }
    }
}
#endif