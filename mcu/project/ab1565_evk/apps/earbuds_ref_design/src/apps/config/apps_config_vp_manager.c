/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "apps_config_vp_manager.h"
#include "app_voice_prompt.h"
#include "apps_debug.h"
#include "bt_device_manager.h"

uint16_t apps_config_set_vp(uint32_t vp_index, bool need_sync, uint32_t sync_delay_time,
    app_vp_prio_t level, bool cleanup, app_vp_play_callback_t callback)
{
    APPS_LOG_MSGID_I("Voice prompt index = 0x%x", 1, vp_index);
#ifdef MTK_PROMPT_SOUND_ENABLE
#ifdef MTK_AWS_MCE_ENABLE
    uint32_t role = bt_device_manager_aws_local_info_get_role();
    if (role == BT_AWS_MCE_ROLE_NONE || (!need_sync)) {
        return app_voice_prompt_play(vp_index, false, sync_delay_time, level, cleanup, callback);
    } else {
        if (role == BT_AWS_MCE_ROLE_AGENT) {
            return app_voice_prompt_play(vp_index, true, sync_delay_time, level, cleanup, callback);
        } else {
            // Partner ignores the VP and wait for sync from agent
            return 0;
        }
    }
#else
     return app_voice_prompt_play(vp_index, false, sync_delay_time, level, cleanup, callback);
#endif
#else
    return 0;
#endif
}

bool apps_config_stop_vp(uint16_t id, bool need_sync, uint32_t sync_delay_time)
{
#ifdef MTK_PROMPT_SOUND_ENABLE
#ifdef MTK_AWS_MCE_ENABLE
        uint32_t role = bt_device_manager_aws_local_info_get_role();
        if (role == BT_AWS_MCE_ROLE_NONE || (!need_sync)) {
            return app_voice_prompt_stop(id, false, 0);
        } else {
            if (role == BT_AWS_MCE_ROLE_AGENT) {
                return app_voice_prompt_stop(id, true, sync_delay_time);
            } else {
                return 0;
            }
        }
#else
         return app_voice_prompt_stop(id, false, 0);
#endif
#else
    return true;
#endif
}

uint16_t apps_config_set_voice(uint32_t vp_index, bool need_sync, uint32_t sync_delay_time,
    app_vp_prio_t level, bool ringtone, bool repeat, app_vp_play_callback_t callback)
{
#ifdef MTK_PROMPT_SOUND_ENABLE
#ifdef MTK_AWS_MCE_ENABLE
        uint32_t role = bt_device_manager_aws_local_info_get_role();
        if (role == BT_AWS_MCE_ROLE_NONE || (!need_sync)) {
            return app_voice_prompt_voice_play(vp_index, false, sync_delay_time, level, ringtone, repeat, callback);
        } else {
            if (role == BT_AWS_MCE_ROLE_AGENT) {
                return app_voice_prompt_voice_play(vp_index, true, sync_delay_time, level, ringtone, repeat, callback);
            } else {
                // Partner ignores the VP and wait for sync from agent
                return 0;
            }
        }
#else
         return app_voice_prompt_voice_play(vp_index, false, sync_delay_time, level, ringtone, repeat, callback);
#endif
#else
    return 0;
#endif
}

bool apps_config_stop_voice(bool need_sync, uint32_t sync_delay_time,  bool ringtone)
{
#ifdef MTK_PROMPT_SOUND_ENABLE
#ifdef MTK_AWS_MCE_ENABLE
        uint32_t role = bt_device_manager_aws_local_info_get_role();
        if (role == BT_AWS_MCE_ROLE_NONE || (!need_sync)) {
            return app_voice_prompt_voice_stop( false, 0, ringtone);
        } else {
            if (role == BT_AWS_MCE_ROLE_AGENT) {
                return app_voice_prompt_voice_stop( need_sync, sync_delay_time, ringtone);
            } else {
                // Partner ignores the VP and wait for sync from agent
                return 0;
            }
        }
#else
         return app_voice_prompt_voice_stop( false, 0, ringtone);
#endif
#else
    return false;
#endif
}

bool apps_config_set_vp_lang(uint8_t index, bool need_sync)
{
#ifdef MTK_PROMPT_SOUND_ENABLE
   return app_voice_prompt_setLang(index, need_sync);
#else
    return false;
#endif
}

