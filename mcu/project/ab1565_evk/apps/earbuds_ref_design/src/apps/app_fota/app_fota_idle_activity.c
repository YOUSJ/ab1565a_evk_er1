/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
#include "app_fota_idle_activity.h"
#include "apps_config_led_manager.h"
#include "apps_config_led_index_list.h"
#include "apps_config_vp_manager.h"
#include "apps_config_vp_index_list.h"
#include "apps_events_event_group.h"
#include "apps_events_interaction_event.h"
#include "race_app_race_event_hdl.h"
#include "race_app_bt_event_hdl.h"
#include "race_app_aws_event_hdl.h"
#include "bt_device_manager.h"


#define APP_IDLE_FOTA_ACTI "app_idle_fota_activity"

typedef struct {
    fota_state_t pre_state;
    fota_state_t curr_state;
}fota_context_t;


static bool _proc_ui_shell_group(struct _ui_shell_activity *self, uint32_t event_id, void *extra_data,size_t data_len);
static bool _proc_key_event_group(struct _ui_shell_activity *self, uint32_t event_id, void *extra_data, size_t data_len);

static void ui_shell_show_activity(struct _ui_shell_activity *self, fota_state_t state)
{
    APPS_LOG_MSGID_I("ui shell fota state :%d ", 1, state);
    fota_context_t *local_context =  (fota_context_t*)self->local_context;
    local_context->pre_state = local_context->curr_state;
    local_context->curr_state = state;
    switch (state) {
        case FOTA_IN_IDLE: {
            APPS_LOG_MSGID_I("FOTA IN IDLE", 0);
            break;
        }
        case  FOTA_IN_START: {
            APPS_LOG_MSGID_I("FOTA STARTING", 0);
            break;
        }
        case FOTA_IN_CANCELLING: {
            APPS_LOG_MSGID_I("FOTA CANCELLING", 0);
            break;
        }
        case FOTA_IN_CANCELLED: {
            APPS_LOG_MSGID_I("FOTA CANCELLED", 0);
            break;
        }
        case FOTA_IN_END: {
            APPS_LOG_MSGID_I("FOTA END", 0);
            break;
        }
        default:
            break;
     }
}


static bool _proc_ui_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data,size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    switch (event_id) {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE: {
            APPS_LOG_MSGID_I(APP_IDLE_FOTA_ACTI": create", 0);
            if (self) {
                fota_context_t *local_context = NULL;
                self->local_context = pvPortMalloc(sizeof(fota_context_t));
                if (self->local_context) {
                    local_context =  (fota_context_t*)self->local_context;
                    if (local_context) {
                        local_context->pre_state = FOTA_IN_IDLE;
                        local_context->curr_state = FOTA_IN_IDLE;
                    }
                    ui_shell_show_activity(self, FOTA_IN_IDLE);
                }
            }
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY: {
            APPS_LOG_MSGID_I(APP_IDLE_FOTA_ACTI": destroy", 0);
            if (self->local_context) {
                vPortFree(self->local_context);
            }
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESUME: {
            APPS_LOG_MSGID_I(APP_IDLE_FOTA_ACTI": resume", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE: {
            APPS_LOG_MSGID_I(APP_IDLE_FOTA_ACTI": pause", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH: {
            APPS_LOG_MSGID_I(APP_IDLE_FOTA_ACTI": refresh", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESULT: {
            APPS_LOG_MSGID_I(APP_IDLE_FOTA_ACTI": result", 0);
            break;
        }
        default:
            ret = false;
            break;
    }
    return ret;
}


static bool _proc_key_event_group(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    return ret;
}


static bool ui_shell_pro_fota_state(
        struct _ui_shell_activity *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
#ifdef MTK_AWS_MCE_ENABLE
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
    APPS_LOG_MSGID_I(APP_IDLE_FOTA_ACTI", event_id : %d, role = %x", 2, event_id, role);
#endif
    switch (event_id) {
        case RACE_EVENT_TYPE_FOTA_START: {
            ui_shell_show_activity(self, FOTA_IN_START);
#ifdef MTK_AWS_MCE_ENABLE
            if (role == BT_AWS_MCE_ROLE_AGENT)
#endif
            {
                apps_config_set_vp(VP_INDEX_SUCCESSED, true, 200, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                apps_config_set_foreground_led_pattern(LED_INDEX_FOTA_START, 30, false);
            }
            break;
        }
        case RACE_EVENT_TYPE_FOTA_CANCELLING: {
            ui_shell_show_activity(self, FOTA_IN_CANCELLING);
            break;
        }
        case RACE_EVENT_TYPE_FOTA_CANCEL: {
            // Need to do
            ui_shell_show_activity(self, FOTA_IN_CANCELLED);
#ifdef MTK_AWS_MCE_ENABLE
            if (role == BT_AWS_MCE_ROLE_AGENT)
#endif
            {
                apps_config_set_foreground_led_pattern(LED_INDEX_FOTA_CANCELLED, 30, false);
            }
            break;
        }
        case RACE_EVENT_TYPE_FOTA_NEED_REBOOT: {
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_REQUEST_REBOOT, NULL, 0,
                    NULL, 0);
            break;
        }
    }
    return ret;
}


bool app_fota_idle_activity_proc(
            struct _ui_shell_activity *self,
            uint32_t event_group,
            uint32_t event_id,
            void *extra_data,
            size_t data_len)
{
    bool ret = false;

    APPS_LOG_MSGID_I(APP_IDLE_FOTA_ACTI", event_group : %d, id : %x", 2, event_group, event_id);

    switch (event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM: {
            ret = _proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;
        }

        case EVENT_GROUP_UI_SHELL_KEY: {
            ret = _proc_key_event_group(self, event_id, extra_data, data_len);
            break;
        }

        case EVENT_GROUP_UI_SHELL_FOTA: {
            ret = ui_shell_pro_fota_state(self, event_id, extra_data, data_len);
            break;
        }

        case EVENT_GROUP_UI_SHELL_BT_SINK: {
            APPS_LOG_MSGID_I(APP_IDLE_FOTA_ACTI", event_id : %x", 1, APP_FOTA_ACTI, event_id);
            race_app_bt_sink_event_handler(event_id, extra_data);
            break;
        }

        case EVENT_GROUP_UI_SHELL_BT_CONN_MANAGER: {
            race_app_bt_cm_event_handler(event_id, extra_data);
#ifdef RACE_AWS_ENABLE
            race_app_aws_cm_event_handler(event_id, extra_data);
#endif
        }

        default: {
            break;
        }
    }

    return ret;
}


