/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "airo_key_event.h"
#include "ui_shell_manager.h"
#include "apps_debug.h"
#include "apps_events_event_group.h"
#include "apps_events_interaction_event.h"
#include "FreeRTOS.h"

static bool s_press_from_power_on = false; /* If user press the key from power on, need do special things*/

static void _apps_key_airo_key_callback(airo_key_event_t event, uint8_t key_data, void *user_data)
{
    APPS_LOG_MSGID_I("key event  = 0x%x, key_data = 0x%x", 2, event, key_data);
    uint16_t *p_key_action = (uint16_t *)pvPortMalloc(sizeof(uint16_t));
    ui_shell_status_t status;

    if (p_key_action) {
        *p_key_action = s_press_from_power_on;
#ifdef MTK_IN_EAR_FEATURE_ENABLE
        if((key_data == EINT_KEY_2) && (event == AIRO_KEY_PRESS || event == AIRO_KEY_RELEASE))
        {
            bool* isInEar = (bool*)pvPortMalloc(sizeof(bool));
            if(isInEar != NULL)
            {
                *isInEar = (event == AIRO_KEY_PRESS) ? true : false;
                status = ui_shell_remove_event(EVENT_GROUP_UI_SHELL_APP_INTERACTION, APPS_EVENTS_INTERACTION_UPDATE_IN_EAR_STA_EFFECT);
                if(UI_SHELL_STATUS_OK != status){
                    APPS_LOG_MSGID_I("touch sensor event remove failed., err = %d", 1, status);
                }

                status = ui_shell_send_event(true, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                                            APPS_EVENTS_INTERACTION_UPDATE_IN_EAR_STA_EFFECT, (void*)isInEar, sizeof(bool),
                                            NULL, 500);
                if(UI_SHELL_STATUS_OK != status){
                    vPortFree(isInEar);
                    APPS_LOG_MSGID_I("touch sensor event send failed., err = %d", 1, status);
                }
            }else{
                APPS_LOG_MSGID_E("touch sensor malloc fail.", 0);
            }

        }
#endif
        status = ui_shell_send_event(true, EVENT_PRIORITY_MIDDLE, EVENT_GROUP_UI_SHELL_KEY,
                (key_data & 0xFF) | ((event & 0xFF) << 8),
                p_key_action, sizeof(uint16_t), NULL, 0);
        if (UI_SHELL_STATUS_OK != status) {
            vPortFree(p_key_action);
            if (UI_SHELL_STATUS_INVALID_STATE == status) {
                s_press_from_power_on = true;
            }
            APPS_LOG_MSGID_I("key press from power on, err = %d", 1, status);
        }
    } else {
        APPS_LOG_MSGID_E("key callback malloc fail", 0);
    }
    if (s_press_from_power_on && AIRO_KEY_RELEASE == event) {
        s_press_from_power_on = false;
        APPS_LOG_MSGID_I("key press from power on end", 0);
    }
}

void apps_event_key_event_init(void)
{
    bool status;
    status = airo_key_event_init();
    if (status != true) {
        APPS_LOG_MSGID_E("sct key init fail, status:%d", 1, status);
    }

#ifdef AB1565
    uint8_t reason = pmu_get_power_on_reason();
    if (reason & 0x01) {
        // Power key
        s_press_from_power_on = true;
    }
    APPS_LOG_MSGID_I("inti key power on by reason : %x", 1, reason);
#endif
    status = airo_key_register_callback(_apps_key_airo_key_callback, NULL);
    if (status != true) {
        APPS_LOG_MSGID_E("register callback fail, status:%d", 1, status);
    }
}

void app_event_key_event_decode(uint8_t *key_id, airo_key_event_t *key_event, uint32_t event_id)
{
    if (key_id) {
        *key_id = event_id & 0xFF;
    }
    if (key_event) {
        *key_event = (event_id >> 8) & 0xFF;
    }
}
