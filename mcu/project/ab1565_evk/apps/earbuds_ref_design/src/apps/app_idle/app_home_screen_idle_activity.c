/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "app_home_screen_idle_activity.h"
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
#include "app_rho_idle_activity.h"
#endif
#include "app_bt_conn_componet_in_homescreen.h"
#include "apps_config_led_manager.h"
#include "apps_config_led_index_list.h"
#include "apps_config_vp_manager.h"
#include "apps_config_vp_index_list.h"
#include "apps_config_key_remapper.h"
#include "apps_config_features_dynamic_setting.h"
#include "apps_aws_sync_event.h"
#include "project_config.h"
#include "apps_customer_config.h"

#include "FreeRTOS.h"
#include "timers.h"
#include "ui_shell_manager.h"
#include "apps_events_event_group.h"
#include "apps_events_key_event.h"
#include "apps_events_bt_event.h"
#include "apps_config_event_list.h"
#include "nvdm_config_factory_reset.h"
#include "at_command_bt.h"
#include "bt_sink_srv.h"
#include "bt_app_common.h"
#include "bt_power_on_config.h"
#include "apps_events_interaction_event.h"
#include "bt_sink_srv_ami.h"
#include "bt_device_manager.h"
#include "nvdm.h"
#include "nvdm_id_list.h"
#include "nvkey.h"
#include "nvkey_id_list.h"
#if 0//def MTK_ANC_ENABLE
#include "anc_control.h"
#endif
#include "hal_rtc.h"
#include "hal_wdt.h"
#ifdef MTK_AWS_MCE_ENABLE
#include "bt_aws_mce_report.h"
#include "bt_aws_mce_srv.h"
#endif
#include "apps_debug.h"

#define UI_SHELL_IDLE_BT_CONN_ACTIVITY  "[TK_Home]app_home_screen_idle_activity"

// Use a timeout before power off, to show LED and play VP
#define POWER_OFF_TIMER_NAME       "POWER_OFF"
#define WAIT_TIME_BEFORE_POWER_OFF  (3 * 1000)

static ui_shell_activity_t *s_homescreen_self = NULL;

#define RETRY_ENTER_RTC_MODE_TIMES      (10)
static void app_home_screen_check_and_do_power_off(home_screen_local_context_type_t *local_context)
{
    if (local_context->bt_power_state == APP_HOME_SCREEN_BT_POWER_STATE_DISABLED) {
        if (APP_HOME_SCREEN_STATE_POWERING_OFF == local_context->state
                && !local_context->power_off_waiting_time_out
                && local_context->power_off_waiting_release_key == DEVICE_KEY_NONE) {
            uint32_t i;
            for (i = 0; ; i++) {
                if (HAL_RTC_STATUS_ERROR == hal_rtc_enter_rtc_mode()) {
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Enter RTC mode fail !!!", 0);
                    if (i > RETRY_ENTER_RTC_MODE_TIMES) {
                        configASSERT(0 && "Enter RTC mode fail !!!");
                    }
                } else {
                    break;
                }
            }
        } else if (APP_HOME_SCREEN_STATE_REBOOT == local_context->state) {
            hal_wdt_status_t ret;
            hal_wdt_config_t wdt_config;

            wdt_config.mode = HAL_WDT_MODE_RESET;
            wdt_config.seconds = 1;
            hal_wdt_init(&wdt_config);
            ret = hal_wdt_software_reset();
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_home_screen_check_and_do_power_off reboot ret = %d", 1, ret);
        }
    }
}

static void app_home_screen_check_and_do_bt_enable_disable(struct _ui_shell_activity *self, bool try_rho)
{
    home_screen_local_context_type_t *local_context = (home_screen_local_context_type_t *)self->local_context;

    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_home_screen_check_and_do_bt_enable_disable target status ? %d, current statue: %d",
            2, local_context->target_bt_power_state, local_context->bt_power_state);
    if (APP_HOME_SCREEN_BT_POWER_STATE_ENABLED == local_context->target_bt_power_state) {
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_home_screen_check_and_do_bt_enable_disable start set BT enable: %d", 1, local_context->target_bt_power_state);
        bt_cm_power_active();
        // Enable BT
    } else if (APP_HOME_SCREEN_BT_POWER_STATE_DISABLED == local_context->target_bt_power_state) {
        // Disable BT
#ifdef APPS_AUTO_TRIGGER_RHO
        if (apps_config_features_is_auto_rho_enabled()
                && bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT
                && local_context->aws_connected
                && try_rho) {
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_home_screen_check_and_do_bt_enable_disable do RHO before disable bt", 0);

            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_TRIGGER_RHO, NULL, 0,
                    NULL, 0);
        }
        else
#endif
        {
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_home_screen_check_and_do_bt_enable_disable start set BT enable: %d", 1, local_context->target_bt_power_state);
            bt_cm_power_standby(false);
        }
    }
}

static void app_home_screen_trigger_bt_enable_disable(struct _ui_shell_activity *self, bool enable_bt, bool need_try_rho)
{
    home_screen_local_context_type_t *local_context = (home_screen_local_context_type_t *)self->local_context;

    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_home_screen_trigger_bt_enable_disable ? %d, try_rho : %d", 2, enable_bt, need_try_rho);

    if (APP_HOME_SCREEN_STATE_IDLE != local_context->state || !enable_bt) {
        local_context->target_bt_power_state = APP_HOME_SCREEN_BT_POWER_STATE_DISABLED;
    } else {
        local_context->target_bt_power_state = APP_HOME_SCREEN_BT_POWER_STATE_ENABLED;
        need_try_rho = false;
#if APPS_AUTO_SET_BT_DISCOVERABLE
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", set flag auto_start_visiable when need power on", 0);
        local_context->auto_start_visiable = true;
#endif
    }
    app_home_screen_check_and_do_bt_enable_disable(self, need_try_rho);
}

static void _trigger_power_off_flow(struct _ui_shell_activity *self, bool need_wait)
{
    home_screen_local_context_type_t *local_context = (home_screen_local_context_type_t *)(self->local_context);

    if (local_context->state != APP_HOME_SCREEN_STATE_IDLE) {
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", _trigger_power_off_flow, already prepared to power off, just wait", 0);
        return;
    } else {
        local_context->state = APP_HOME_SCREEN_STATE_POWERING_OFF;
    }

#if 0//def MTK_ANC_ENABLE
    anc_control_result_t anc_ret = audio_anc_suspend(NULL);
    APPS_LOG_MSGID_I("audio_anc_suspend after when power off in ret = %d", 1, anc_ret);
#endif
    ami_audio_power_off_flow(); // Disable Audio before power off to avoid pop sound.
    app_home_screen_trigger_bt_enable_disable(self, false, true);


    if (need_wait) {
        local_context->power_off_waiting_time_out = true;
        ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                APPS_EVENTS_INTERACTION_POWER_OFF_WAIT_TIMEOUT, NULL, 0,
                NULL, WAIT_TIME_BEFORE_POWER_OFF);
    } else {
        local_context->power_off_waiting_time_out = false;
        app_home_screen_check_and_do_power_off(local_context);
    }

    /*
    TimerHandle_t timer_handle = xTimerCreate(POWER_OFF_TIMER_NAME, WAIT_TIME_BEFORE_POWER_OFF / portTICK_PERIOD_MS,
            pdFALSE, 0, _power_off_wait_timeout_callback);
    xTimerStart(timer_handle, 0);
    */
}

static void _trigger_reboot_flow(struct _ui_shell_activity *self)
{
    home_screen_local_context_type_t *local_context = (home_screen_local_context_type_t *)(self->local_context);

    if (local_context->state != APP_HOME_SCREEN_STATE_IDLE) {
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", reboot flow, already prepared to power off when request reboot, just wait", 0);
    } else {
        local_context->state = APP_HOME_SCREEN_STATE_REBOOT;
#if 0//def MTK_ANC_ENABLE
        anc_control_result_t anc_ret = audio_anc_suspend(NULL);
        APPS_LOG_MSGID_I("audio_anc_suspend after when reboot in ret = %d", 1, anc_ret);
#endif
        ami_audio_power_off_flow(); // Disable Audio before power off to avoid pop sound.
        app_home_screen_trigger_bt_enable_disable(self, false, false);
    }

    app_home_screen_check_and_do_power_off(local_context);
}

#if 0//def MTK_ANC_ENABLE
static void app_home_screen_anc_control_callback(anc_control_event_t event_id, anc_control_result_t result)
{
    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_homescreen_idle_activity, anc control callback %d ret : %d", 2,
                event_id, result);
}

#ifdef MTK_ANC_HOWLING_TURN_OFF_ANC 
static void app_home_screen_disable_anc_when_howling(ui_shell_activity_t *self)
{
    home_screen_local_context_type_t *local_ctx = (home_screen_local_context_type_t *)self->local_context;
    anc_control_result_t control_ret;

    if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER
            && local_ctx && local_ctx->aws_connected) {
            if (BT_STATUS_SUCCESS != apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_AUDIO_ANC, ANC_CONTROL_EVENT_HOWLING)) {
                control_ret = audio_anc_disable(app_home_screen_anc_control_callback);
                apps_config_set_vp(VP_INDEX_FAILED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Partner send ANC_CONTROL_EVENT_HOWLING to agent failed, disable self ret : %d",
                        1, control_ret);
            } else {
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Partner send ANC_CONTROL_EVENT_HOWLING to agent success", 0);
            }
    } else {
        control_ret = audio_anc_disable(app_home_screen_anc_control_callback);
        apps_config_set_vp(VP_INDEX_FAILED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", disable anc filter when ANC_CONTROL_EVENT_HOWLING, ret: %d",
                1, control_ret);
    }
}
#endif

static bool app_home_screen_process_anc_and_pass_through(ui_shell_activity_t *self, apps_config_key_action_t key_action)
{
    bool ret = false;
    uint8_t anc_enable;
    uint32_t runtime_info;
    uint8_t hybrid_enable;
    anc_control_result_t control_ret;
    anc_filter_type_t target_filter_type;
    home_screen_local_context_type_t *local_ctx = (home_screen_local_context_type_t *)self->local_context;

    if (!local_ctx) {
        return ret;
    }
    anc_get_status(&anc_enable, &runtime_info, &hybrid_enable);
    if (KEY_PASS_THROUGH == key_action) {
        target_filter_type = FILTER_5;
        ret = true;
    } else if (KEY_ANC == key_action) {
        target_filter_type = FILTER_1;
        ret = true;
    } else if (KEY_SWITCH_ANC_AND_PASSTHROUGH == key_action) {
        // switch loop is OFF->PassThrough->ANC->OFF
        if (!anc_enable) {
            // When last is OFF, next state is PassThrough
            target_filter_type = FILTER_5;
        } else {
            /* If current filter is Filter_1(ANC), must set to OFF, target is FILTER_1
            and if current filter is FILTER_5(PassThrough), target is Filter_1 */
            target_filter_type = FILTER_1;
        }
        ret = true;
    } else if (KEY_BETWEEN_ANC_PASSTHROUGH == key_action) {
        if (anc_enable && (FILTER_5 == (runtime_info & ANC_FILTER_TYPE_MASK))) {
            //set as anc
            target_filter_type = FILTER_1;
        } else {
            //set as passthrough
            target_filter_type = FILTER_5;
        }
        ret = true;
    } else {
        return false;
    }

#ifdef MTK_AWS_MCE_ENABLE
    if (local_ctx && local_ctx->aws_connected) {
        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER) {
            if (BT_STATUS_SUCCESS != apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_KEY, key_action)) {
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Partner send KEY_ANC or KEY_PASS_THROUGH aws to agent failed : %d", 1, key_action);
                apps_config_set_vp(VP_INDEX_FAILED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
            } else {
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Partner send KEY_ANC or KEY_PASS_THROUGH aws to agent success : %d", 1, key_action);
            }
            return ret;
        }
    } else {
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_home_screen_process_anc_and_pass_through, aws_connected is false", 0);
        apps_config_set_vp(VP_INDEX_FAILED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
        return ret;
    }
#endif
    // Agent and aws connected.
    if (anc_enable && (target_filter_type == (runtime_info & ANC_FILTER_TYPE_MASK)) && (KEY_BETWEEN_ANC_PASSTHROUGH != key_action)) {
        control_ret = audio_anc_disable(app_home_screen_anc_control_callback);
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_homescreen_idle_activity, disable anc filter : %d ret: %d", 2,
                target_filter_type, control_ret);
    } else {
        control_ret = audio_anc_enable(target_filter_type, ANC_UNASSIGNED_GAIN,
                app_home_screen_anc_control_callback);
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_homescreen_idle_activity, enable anc filter %d ret: %d", 2,
                target_filter_type, control_ret);
    }
    apps_config_set_vp(VP_INDEX_SUCCESSED, true, 200, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
    return ret;
}
#endif

static nvdm_status_t app_home_screen_fact_rst_nvdm_flag(void)
{
    uint8_t factrst_flag;
    nvdm_status_t status;

    APPS_LOG_MSGID_I("Write Factory reset flag to NVDM", 0);

    factrst_flag = FACTORY_RESET_FLAG;
    status = nvdm_write_data_item(NVDM_GROUP_FACTORY_RESET, NVDM_FACTORY_RESET_ITEM_FACTORY_RESET_FLAG,
            NVDM_DATA_ITEM_TYPE_RAW_DATA, &factrst_flag, 1);

    return status;
}

static nvdm_status_t app_home_screen_fact_rst_link_key_nvdm_flag(void)
{
    uint8_t factrst_flag;
    nvdm_status_t status;

    APPS_LOG_MSGID_I("Write Factory reset flag to NVDM", 0);

    factrst_flag = FACTORY_RESET_LINK_KEY;
    status = nvdm_write_data_item(NVDM_GROUP_FACTORY_RESET, NVDM_FACTORY_RESET_ITEM_FACTORY_RESET_FLAG,
            NVDM_DATA_ITEM_TYPE_RAW_DATA, &factrst_flag, 1);

    return status;
}

static bool _proc_ui_shell_group(
        struct _ui_shell_activity *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    home_screen_local_context_type_t *local_ctx = (home_screen_local_context_type_t *)self->local_context;

    switch (event_id) {
    case EVENT_ID_SHELL_SYSTEM_ON_CREATE:
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", home_screen create", 0);
        self->local_context = pvPortMalloc(sizeof(home_screen_local_context_type_t));
        if (self->local_context) {
            memset(self->local_context, 0, sizeof(home_screen_local_context_type_t));
            local_ctx = (home_screen_local_context_type_t *)self->local_context;
            local_ctx->state = APP_HOME_SCREEN_STATE_IDLE;
            local_ctx->bt_power_state = APP_HOME_SCREEN_BT_POWER_STATE_DISABLED;
            local_ctx->target_bt_power_state = APP_HOME_SCREEN_BT_POWER_STATE_DISABLED;
            local_ctx->connection_state = false;
            local_ctx->power_off_waiting_release_key = DEVICE_KEY_NONE;
            local_ctx->is_bt_visiable = false;
            s_homescreen_self = self;
        }
        break;
    case EVENT_ID_SHELL_SYSTEM_ON_DESTROY:
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_homescreen_idle_activity destroy", 0);
        if (self->local_context) {
            vPortFree(self->local_context);
        }
        break;
    case EVENT_ID_SHELL_SYSTEM_ON_RESUME:
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_homescreen_idle_activity resume", 0);
        break;
    case EVENT_ID_SHELL_SYSTEM_ON_PAUSE:
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_homescreen_idle_activity pause", 0);
        break;
    case EVENT_ID_SHELL_SYSTEM_ON_REFRESH:
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_homescreen_idle_activity refresh", 0);
        break;
    case EVENT_ID_SHELL_SYSTEM_ON_RESULT:
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_homescreen_idle_activity result", 0);
        if (extra_data) {
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", extra data for app_homescreen_idle_activity result", 0);
        }
        break;
    default:
        break;
    }
    return ret;
}

static bool _proc_key_event_group(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    home_screen_local_context_type_t *local_context = (home_screen_local_context_type_t *)(self->local_context);

    uint8_t key_id;
    airo_key_event_t key_event;

    app_event_key_event_decode(&key_id, &key_event, event_id);

    // Do not power off before key released
    if (local_context->power_off_waiting_release_key == key_id && key_event == AIRO_KEY_RELEASE) {
        local_context->power_off_waiting_release_key = DEVICE_KEY_NONE;
        app_home_screen_check_and_do_power_off(local_context);
    }

    if (APP_HOME_SCREEN_STATE_POWERING_OFF == local_context->state) {
        return true;
    }
    apps_config_key_action_t action;
    if (extra_data) {
        action = *(uint16_t *)extra_data;
    } else {
        action = apps_config_key_event_remapper_map_action(key_id, key_event);
    }

    switch (action) {
    case KEY_POWER_OFF:
        apps_config_set_vp(VP_INDEX_POWER_OFF, false, 0, VOICE_PROMPT_PRIO_EXTREME, true, NULL);
        apps_config_set_foreground_led_pattern(LED_INDEX_POWER_OFF, 30, false);
        _trigger_power_off_flow(self, true);
        // When use long press to power off, must wait the key release
        if (key_event >= AIRO_KEY_LONG_PRESS_1 && key_event <= AIRO_KEY_SLONG) {
            local_context->power_off_waiting_release_key = key_id;
        }
        ret = true;
        break;
    case KEY_POWER_ON:
        // power on BT
        if (local_context->bt_power_state == APP_HOME_SCREEN_BT_POWER_STATE_DISABLED) {
            app_home_screen_trigger_bt_enable_disable(self, true, true);
            apps_config_set_vp(VP_INDEX_POWER_ON, false, 0, VOICE_PROMPT_PRIO_HIGH, false, NULL);
            apps_config_set_foreground_led_pattern(LED_INDEX_POWER_ON, 30, false);
        }
        break;
    case KEY_SYSTEM_REBOOT:
        _trigger_reboot_flow(self);
        ret = true;
        break;
    case KEY_DISCOVERABLE:
#ifdef MTK_AWS_MCE_ENABLE
        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER) {
            if (local_context->aws_connected) {
                if (BT_STATUS_SUCCESS != apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_KEY, KEY_DISCOVERABLE)) {
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Partner send KEY_DISCOVERABLE aws to agent failed", 0);
                    apps_config_set_vp(VP_INDEX_FAILED, false, 100, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                } else {
                    apps_config_set_vp(VP_INDEX_SUCCESSED, false, 100, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                }
            } else {
                bt_cm_discoverable(true);
                apps_config_set_vp(VP_INDEX_PAIRING, false, 100, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Partner aws disconnected for KEY_DISCOVERABLE", 0);
            }
        } else if (bt_device_manager_aws_local_info_get_role() != BT_AWS_MCE_ROLE_CLINET)
#endif
        {
            bt_cm_discoverable(true);
            apps_config_set_vp(VP_INDEX_PAIRING, false, 100, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
        }
        ret = true;
        break;
    case KEY_RECONNECT_LAST_DEVICE:
#ifdef MTK_AWS_MCE_ENABLE
        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER) {
            if (local_context->aws_connected) {
                if (BT_STATUS_SUCCESS != apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_KEY, KEY_RECONNECT_LAST_DEVICE)) {
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Partner send KEY_RECONNECT_LAST_DEVICE aws to agent failed", 0);
                    apps_config_set_vp(VP_INDEX_FAILED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                } else {
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Partner send KEY_RECONNECT_LAST_DEVICE aws to agent success", 0);
                }
            } else {
                apps_config_set_vp(VP_INDEX_FAILED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Partner aws disconnected for KEY_RECONNECT_LAST_DEVICE", 0);
            }
        } else if (bt_device_manager_aws_local_info_get_role() != BT_AWS_MCE_ROLE_CLINET)
#endif
        {
            bt_bd_addr_t *p_bd_addr = bt_device_manager_remote_get_dev_by_seq_num(1);
            bt_cm_connect_t connect_param = { {0},
                                BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS)
                                | BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP)
                                | BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_A2DP_SINK)
                                | BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AVRCP) };
            if (p_bd_addr) {
                memcpy(connect_param.address, *p_bd_addr, sizeof(bt_bd_addr_t));
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_homescreen_idle_activity receive KEY_RECONNECT_LAST_DEVICE", 0);
                bt_cm_connect(&connect_param);
                apps_config_set_vp(VP_INDEX_SUCCESSED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
            }
        }
        ret = true;
        break;
    case KEY_RESET_PAIRED_DEVICES:
#ifdef MTK_AWS_MCE_ENABLE
        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER) {
            if (local_context->aws_connected) {
                if (BT_STATUS_SUCCESS != apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_KEY, KEY_RESET_PAIRED_DEVICES)) {
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Partner send KEY_RESET_PAIRED_DEVICES aws to agent failed", 0);
                    apps_config_set_vp(VP_INDEX_FAILED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                } else {
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Partner send KEY_RESET_PAIRED_DEVICES aws to agent success", 0);
                }
            } else {
                apps_config_set_vp(VP_INDEX_FAILED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Partner aws disconnected for KEY_RESET_PAIRED_DEVICES", 0);
            }
        } else if (bt_device_manager_aws_local_info_get_role() != BT_AWS_MCE_ROLE_CLINET)
#endif
        {
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", app_homescreen_idle_activity receive KEY_RESET_PAIRED_DEVICES", 0);
            bt_device_manager_unpair_all();
            apps_config_set_vp(VP_INDEX_SUCCESSED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
        }
        ret = true;
        break;
#if 0//def MTK_ANC_ENABLE
    case KEY_PASS_THROUGH:
    case KEY_ANC:
    case KEY_SWITCH_ANC_AND_PASSTHROUGH:
    case KEY_BETWEEN_ANC_PASSTHROUGH:
        ret = app_home_screen_process_anc_and_pass_through(self, action);
        break;
#endif
    case KEY_FACTORY_RESET:
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Receive key to do factory reset", 0);
        app_home_screen_fact_rst_nvdm_flag();
#ifdef MTK_AWS_MCE_ENABLE
        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT) {
            if (BT_STATUS_SUCCESS != apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_KEY, action)) {
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Fail to send KEY_FACTORY_RESET to partner", 0);
            }
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_REQUEST_REBOOT, NULL, 0,
                    NULL, 100);
        } else
#else
        {
            _trigger_reboot_flow(self);
        }
#endif
        ret = true;
        break;
    case KEY_FACTORY_RESET_AND_POWEROFF:
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Receive key to do factory reset and power off", 0);
        app_home_screen_fact_rst_nvdm_flag();
#ifdef MTK_AWS_MCE_ENABLE
        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT) {
            if (BT_STATUS_SUCCESS != apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_KEY, action)) {
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Fail to send KEY_FACTORY_RESET_AND_POWEROFF to partner", 0);
            }
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_REQUEST_IMMEDIATELY_POWER_OFF, NULL, 0,
                    NULL, 100);
        } else
#else
        {
            _trigger_power_off_flow(self, false);
        }
#endif
        ret = true;
        break;
    case KEY_RESET_LINK_KEY:
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Receive key to do reset link key", 0);
        app_home_screen_fact_rst_link_key_nvdm_flag();
#ifdef MTK_AWS_MCE_ENABLE
        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT) {
            if (BT_STATUS_SUCCESS != apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_KEY, action)) {
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Fail to send KEY_RESET_LINK_KEY to partner", 0);
            }
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_REQUEST_REBOOT, NULL, 0,
                    NULL, 100);
        } else
#else
        {
            _trigger_reboot_flow(self);
        }
#endif
        ret = true;
        break;
#ifdef MTK_AWS_MCE_ENABLE
    case KEY_AIR_PAIRING:
    {
        bt_aws_mce_srv_air_pairing_t air_pairing_data = {
                .duration = APPS_AIR_PAIRING_DURATION,
                .default_role = 0,
                .air_pairing_key = APPS_AIR_PAIRING_KEY,
                .air_pairing_info = APPS_AIR_PAIRING_INFO,
                .rssi_threshold = AIR_PAIRING_RSSI_THRESHOLD,
                .audio_ch = ami_get_audio_channel(),
        };


        if (BT_STATUS_SUCCESS == bt_aws_mce_srv_air_pairing_start(&air_pairing_data)) {
            apps_config_set_vp(VP_INDEX_PAIRING, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
            apps_config_set_foreground_led_pattern(LED_INDEX_AIR_PAIRING, APPS_AIR_PAIRING_DURATION * 10, false);
            local_context->in_air_pairing = true;
        } else {
            apps_config_set_vp(VP_INDEX_FAILED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
        }

        ret = true;
        break;
    }
#endif
#ifdef APPS_TRIGGER_RHO_BY_KEY
    case KEY_RHO_TO_AGENT:
    {
        if (!apps_config_features_is_auto_rho_enabled()) {
            if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER) {
                if (local_context->aws_connected
                        && BT_STATUS_SUCCESS == apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_KEY, KEY_RHO_TO_AGENT)) {
                    apps_config_set_vp(VP_INDEX_PRESS, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                    apps_config_set_foreground_led_pattern(LED_INDEX_TRIGGER_RHO, 30, false);
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Send RHO request to agent", 0);
                } else {
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Current AWS not connected", 0);
                    apps_config_set_vp(VP_INDEX_FAILED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                }
                local_context->key_trigger_waiting_rho = true;
            } else {
                apps_config_set_vp(VP_INDEX_SUCCESSED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                apps_config_set_foreground_led_pattern(LED_INDEX_TRIGGER_RHO, 30, false);
            }

            ret = true;
        }
        break;
    }
#endif
    case KEY_TEST_MODE_ENTER_DUT_MODE:
    {
        bool dut_config = false;
        uint32_t dut_size = sizeof(dut_config);
        nvkey_status_t nvkey_ret = nvkey_read_data(NVKEYID_BT_DUT_ENABLE, (uint8_t *)(&dut_config), &dut_size);
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY"ENTER_DUT_MODE nvkey_read:%d, dut_config:%d\r\n", 2, nvkey_ret, dut_config);
        if (NVKEY_STATUS_OK == nvkey_ret) {
            dut_config = !dut_config;
        } else {
            dut_config = true;
        }
        dut_size = sizeof(dut_config);
        nvkey_ret = nvkey_write_data(NVKEYID_BT_DUT_ENABLE, (uint8_t *)(&dut_config), dut_size);
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY"ENTER_DUT_MODE nvkey_write:%d", 1, nvkey_ret);
        _trigger_reboot_flow(self);
    }
        break;
    case KEY_TEST_MODE_ENTER_RELAY_MODE:
    {
        bt_power_on_relay_config_t relay_config = {
            .relay_enable = false,
            .port_number = 0,
        };
        uint32_t relay_size = sizeof(relay_config);
        nvkey_status_t nvkey_ret = nvkey_read_data(NVKEYID_BT_RELAY_ENABLE, (uint8_t *)(&relay_config), &relay_size);
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY"ENTER_RELAY_MODE nvkey_read:%d, relay_enable:%d, port_number:%d\r\n", 3, nvkey_ret, relay_config.relay_enable, relay_config.port_number);
        if (NVKEY_STATUS_OK == nvkey_ret) {
            relay_config.relay_enable = !relay_config.relay_enable;
        } else {
            relay_config.relay_enable = true;
            relay_config.port_number = 0;
        }
        relay_size = sizeof(relay_config);
        nvkey_ret = nvkey_write_data(NVKEYID_BT_RELAY_ENABLE, (uint8_t *)(&relay_config), relay_size);
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY"ENTER_RELAY_MODE nvkey_write:%d", 3, nvkey_ret);
        _trigger_reboot_flow(self);
    }
        break;
    }
    return ret;
}

#ifdef MTK_AWS_MCE_ENABLE
static bool homescreen_app_aws_data_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    bt_aws_mce_report_info_t *aws_data_ind = (bt_aws_mce_report_info_t *)extra_data;

    ret = bt_conn_component_aws_data_proc(self, event_id, extra_data, data_len);

    if (ret) {
        return ret;
    }

    if (aws_data_ind->module_id == BT_AWS_MCE_REPORT_MODULE_APP_ACTION)
    {
        uint32_t event_group;
        uint32_t event_id;
        apps_aws_sync_event_decode(aws_data_ind, &event_group, &event_id);
        switch (event_group)
        {
        case EVENT_GROUP_UI_SHELL_KEY:
            switch (event_id)
            {
#ifdef APPS_TRIGGER_RHO_BY_KEY
            case KEY_RHO_TO_AGENT:
            if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT)
            {
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Received event = %x to do RHO", 1, event_id);
                ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                        APPS_EVENTS_INTERACTION_TRIGGER_RHO, NULL, 0,
                        NULL, 0);
            }
            ret = true;
            break;
#endif
            case KEY_DISCOVERABLE:
                if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT)
                {
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Received event = %x to do discover", 1, event_id);
                    bt_cm_discoverable(true);
                    apps_config_set_vp(VP_INDEX_PAIRING, false, 100,
                            VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                }
                ret = true;
                break;
            case KEY_RECONNECT_LAST_DEVICE:
                if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT)
                {
                    bt_bd_addr_t *p_bd_addr = bt_device_manager_remote_get_dev_by_seq_num(1);
                    bt_cm_connect_t connect_param = { { 0 },
                                BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS)
                                | BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP)
                                | BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_A2DP_SINK)
                                | BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AVRCP) };
                    if (p_bd_addr) {
                        memcpy(connect_param.address, *p_bd_addr, sizeof(p_bd_addr));
                        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Agent received KEY_RECONNECT_LAST_DEVICE from Partner", 0);
                        bt_cm_connect(&connect_param);
                        apps_config_set_vp(VP_INDEX_SUCCESSED, false, 0,
                                VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                    }
                }
                ret = true;
                break;
            case KEY_RESET_PAIRED_DEVICES:
                if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT)
                {
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Agent received KEY_RESET_PAIRED_DEVICES from Partner", 0);
                    bt_device_manager_unpair_all();
                    apps_config_set_vp(VP_INDEX_SUCCESSED, false, 0,
                            VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                }
                ret = true;
                break;
#if 0//def MTK_ANC_ENABLE
                case KEY_PASS_THROUGH:
                case KEY_ANC:
                case KEY_SWITCH_ANC_AND_PASSTHROUGH:
                case KEY_BETWEEN_ANC_PASSTHROUGH:
                ret = app_home_screen_process_anc_and_pass_through(self, event_id);
                break;
#endif
            case KEY_FACTORY_RESET:
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Receive aws KEY_FACTORY_RESET", 0);
                app_home_screen_fact_rst_nvdm_flag();
                _trigger_reboot_flow(self);
                ret = true;
                break;
            case KEY_FACTORY_RESET_AND_POWEROFF:
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Receive aws KEY_FACTORY_RESET_AND_POWEROFF", 0);
                app_home_screen_fact_rst_nvdm_flag();
                _trigger_power_off_flow(self, false);
                ret = true;
                break;
            case KEY_RESET_LINK_KEY:
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Receive aws KEY_RESET_LINK_KEY", 0);
                app_home_screen_fact_rst_link_key_nvdm_flag();
                _trigger_reboot_flow(self);
                ret = true;
                break;
            default:
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Received unprocessed event = %x=%x", 2, event_group, event_id);
                break;
            }
            break;
#if 0//def MTK_ANC_ENABLE
        case EVENT_GROUP_UI_SHELL_AUDIO_ANC:
            switch (event_id) {
            case ANC_CONTROL_EVENT_HOWLING:
#ifdef MTK_ANC_HOWLING_TURN_OFF_ANC 
                app_home_screen_disable_anc_when_howling(self);
#endif
                break;
            default:
                break;
            }
            break;
#endif
        default:
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Received unprocessed event group = %x", 1, event_group);
            break;
        }
    }
    return ret;
}
#endif

static bool homescreen_app_bt_sink_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;

    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", BT event = %x", 1, event_id);

    ret = bt_conn_component_bt_sink_event_proc(self, event_id, extra_data, data_len);

    return ret;
}

static bool homescreen_app_bt_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;

    ret = bt_conn_component_bt_event_proc(self, event_id, extra_data, data_len);

    return ret;
}

static apps_config_state_t homescreen_app_get_mmi_state(ui_shell_activity_t *self)
{
    home_screen_local_context_type_t *local_context = (home_screen_local_context_type_t *)(self->local_context);
    apps_config_state_t mmi_state = APP_BT_OFF;

    if (!local_context) {
        return mmi_state;
    }
    if (APP_HOME_SCREEN_BT_POWER_STATE_DISABLED == local_context->bt_power_state) {
        mmi_state = APP_BT_OFF;
    } else {
        if (local_context->connection_state) {
            mmi_state = APP_CONNECTED;
        } else {
            mmi_state = local_context->is_bt_visiable ? APP_CONNECTABLE : APP_DISCONNECTED;
        }
    }

    return mmi_state;
}

static bool homescreen_app_bt_connection_manager_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    apps_config_state_t old_mmi_state;
    apps_config_state_t new_mmi_state;
    bt_cm_remote_info_update_ind_t *remote_update = NULL;
    home_screen_local_context_type_t *local_context = (home_screen_local_context_type_t *)(self->local_context);
#ifdef MTK_AWS_MCE_ENABLE
    bool old_aws_connected = false;
    bt_aws_mce_role_t role;

    role = bt_device_manager_aws_local_info_get_role();
#endif
    if (!local_context) {
        return ret;
    }

#ifdef MTK_AWS_MCE_ENABLE
    old_aws_connected = local_context->aws_connected;
#endif
    old_mmi_state = homescreen_app_get_mmi_state(self);

    ret = bt_conn_component_bt_cm_event_proc(self, event_id, extra_data, data_len);

    new_mmi_state = homescreen_app_get_mmi_state(self);

    switch (event_id) {
        case BT_CM_EVENT_REMOTE_INFO_UPDATE:
            remote_update = (bt_cm_remote_info_update_ind_t *)extra_data;
            break;
        default:
            break;
    }
#if APPS_AUTO_SET_BT_DISCOVERABLE
#ifdef MTK_AWS_MCE_ENABLE
    // Disconnected from Smart phone, set the flag to prepare start BT discoverable
    // If user refused pairing on Smart phone, must restart discoverable
    if (((APP_BT_OFF == old_mmi_state || APP_CONNECTED == old_mmi_state)
            && (APP_DISCONNECTED == new_mmi_state || APP_CONNECTABLE == new_mmi_state))
            || (APP_DISCONNECTED == new_mmi_state && remote_update && BT_CM_ACL_LINK_DISCONNECTED != remote_update->pre_acl_state 
            && BT_CM_ACL_LINK_DISCONNECTED == remote_update->acl_state
            && bt_cm_get_connected_devices(~BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS), NULL, 0) == 0)) {
        if (local_context->aws_connected && BT_AWS_MCE_ROLE_AGENT == role) {
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", since aws is connected, directly auto_start_visiable", 0);
            local_context->auto_start_visiable = false;
            bt_cm_discoverable(true);
        } else {
            local_context->auto_start_visiable = true;
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", set flag auto_start_visiable when connected", 0);
        }
    } else if ((APP_DISCONNECTED == old_mmi_state || APP_CONNECTABLE == old_mmi_state)
            && (APP_BT_OFF == new_mmi_state || APP_CONNECTED == new_mmi_state)) {
        if (local_context->auto_start_visiable) {
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", un-set flag auto_start_visiable when disconnected", 0);
            local_context->auto_start_visiable = false;
        }
    } else if (!old_aws_connected && local_context->aws_connected && local_context->auto_start_visiable) {
        // When AWS connected, auto set BT discoverable.
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", aws connected and need auto_start_visiable, role = %x",
                1, role);
        local_context->auto_start_visiable = false;
        if (BT_AWS_MCE_ROLE_AGENT == role && APP_DISCONNECTED == new_mmi_state) {
            bt_cm_discoverable(true);
        }
    }
#else
    if (((APP_BT_OFF == old_mmi_state || APP_CONNECTED == old_mmi_state)
            && (APP_DISCONNECTED == new_mmi_state || APP_CONNECTABLE == new_mmi_state))
            || (APP_DISCONNECTED == new_mmi_state && remote_update && BT_CM_ACL_LINK_DISCONNECTED != remote_update->pre_acl_state 
            && BT_CM_ACL_LINK_DISCONNECTED == remote_update->acl_state)) {
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", auto_start_visiable when status changed from %d to POWER_ON",
                1, old_mmi_state);
        bt_cm_discoverable(true);
    }
#endif
#endif

    if (APP_HOME_SCREEN_STATE_IDLE != local_context->state) {
        app_home_screen_check_and_do_power_off(local_context);
    }

    return ret;
}

static bool homescreen_app_aws_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    switch (event_id) {
#ifdef MTK_AWS_MCE_ENABLE
    case BT_AWS_MCE_SRV_EVENT_AIR_PAIRING_COMPLETE:
    {
        if (extra_data) {
            home_screen_local_context_type_t *local_context = (home_screen_local_context_type_t *)(self->local_context);
            bt_aws_mce_srv_air_pairing_complete_ind_t *air_pairing_ind = (bt_aws_mce_srv_air_pairing_complete_ind_t *)extra_data;
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Received BT_AWS_MCE_SRV_EVENT_AIR_PAIRING_COMPLETE result= %d", 1, air_pairing_ind->result);
            local_context->in_air_pairing = false;

            if (APP_HOME_SCREEN_BT_POWER_STATE_DISABLED == local_context->target_bt_power_state) {
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Need disable BT after air pairing stopped", 0);
            } else {
                if (air_pairing_ind->result) {
#if APPS_AUTO_SET_BT_DISCOVERABLE
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", set flag auto_start_visiable when air pairing successfully", 0);
                    local_context->auto_start_visiable = true;
#endif
                    apps_config_set_foreground_led_pattern(LED_INDEX_AIR_PAIRING_SUCCESS, 30, false);
                    apps_config_set_vp(VP_INDEX_SUCCESSED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                } else {
                    apps_config_set_foreground_led_pattern(LED_INDEX_AIR_PAIRING_FAIL, 30, false);
                    apps_config_set_vp(VP_INDEX_FAILED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                }
            }
        }
        ret = true;
        break;
    }
#endif
    default:
        break;
    }
    return ret;
}

static bool _app_interaction_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    home_screen_local_context_type_t *local_ctx = (home_screen_local_context_type_t *)self->local_context;
    bool ret = false;
#ifdef MTK_AWS_MCE_ENABLE
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
#endif

    switch (event_id) {
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
    case APPS_EVENTS_INTERACTION_RHO_STARTED:
    {
        bt_aws_mce_role_t role = (uint32_t)extra_data;
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", homescreen received RHO started, role = %x", 1, role);
        if (BT_AWS_MCE_ROLE_AGENT == role) {
            local_ctx->rho_doing = true;
        }
    }
        break;
    case APPS_EVENTS_INTERACTION_RHO_END:
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", homescreen received RHO end", 0);
        local_ctx->rho_doing = false;
        {
            //app_rho_result_t rho_ret = (app_rho_result_t)extra_data;
#ifdef APPS_AUTO_TRIGGER_RHO
            if (apps_config_features_is_auto_rho_enabled()) {
                app_home_screen_check_and_do_bt_enable_disable(self, false);
            }
#endif
        }
        break;
    case APPS_EVENTS_INTERACTION_PARTNER_SWITCH_TO_AGENT:
#ifdef APPS_TRIGGER_RHO_BY_KEY
        if (!apps_config_features_is_auto_rho_enabled()) {
            if (local_ctx->key_trigger_waiting_rho) {
                app_rho_result_t result = (app_rho_result_t)extra_data;
                if (APP_RHO_RESULT_SUCCESS == result) {
                    apps_config_set_vp(VP_INDEX_SUCCESSED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                } else {
                    apps_config_set_vp(VP_INDEX_FAILED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                }
            }
        }
#endif
        break;
#endif
    case APPS_EVENTS_INTERACTION_POWER_OFF_WAIT_TIMEOUT:
    {
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Timeout before power off", 0);

        if (APP_HOME_SCREEN_STATE_IDLE != local_ctx->state) {
            local_ctx->power_off_waiting_time_out = false;
            app_home_screen_check_and_do_power_off(local_ctx);
            ret = true;
        }
    }
    break;
    case APPS_EVENTS_INTERACTION_REQUEST_POWER_OFF:
    {
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Receive power off request", 0);
        _trigger_power_off_flow(self, true);
        ret = true;
    }
    break;
    case APPS_EVENTS_INTERACTION_REQUEST_IMMEDIATELY_POWER_OFF:
    {
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Receive immediately power off request", 0);
        _trigger_power_off_flow(self, false);
        ret = true;
    }
    break;
    case APPS_EVENTS_INTERACTION_REQUEST_REBOOT:
    {
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Receive sys reboot request", 0);
        _trigger_reboot_flow(self);
        ret = true;
    }
        break;
    case APPS_EVENTS_INTERACTION_REQUEST_ON_OFF_BT:
    {
        bool enable_bt = (bool)extra_data;
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Receive BT on/off request : %d", 1, enable_bt);
        app_home_screen_trigger_bt_enable_disable(self, enable_bt, true);
    }
        break;
    case APPS_EVENTS_INTERACTION_UPDATE_LED_BG_PATTERN:
        ret = true;
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Connection state = %x, is_bt_visiable = %d", 2, local_ctx->connection_state, local_ctx->is_bt_visiable);
        if (local_ctx->is_bt_visiable) {
            apps_config_set_background_led_pattern(LED_INDEX_CONNECTABLE, true, APPS_CONFIG_LED_AWS_SYNC_PRIO_MIDDLE);
        } else {
            apps_config_state_t app_state = homescreen_app_get_mmi_state(self);
            switch (app_state) {
                case APP_BT_OFF:
                    apps_config_set_background_led_pattern(LED_INDEX_IDLE, false, APPS_CONFIG_LED_AWS_SYNC_PRIO_LOWEST);
                    break;
                case APP_DISCONNECTED:
                    apps_config_set_background_led_pattern(LED_INDEX_DISCONNECTED, true, APPS_CONFIG_LED_AWS_SYNC_PRIO_LOW);
                    break;
                case APP_CONNECTED:
#ifdef MTK_AWS_MCE_ENABLE
                    if (role == BT_AWS_MCE_ROLE_PARTNER || role == BT_AWS_MCE_ROLE_CLINET) {
                        if (local_ctx->aws_connected) {
                            apps_config_set_background_led_pattern(LED_INDEX_IDLE, false, APPS_CONFIG_LED_AWS_SYNC_PRIO_LOWEST);
                        } else {
                            apps_config_set_background_led_pattern(LED_INDEX_DISCONNECTED, true, APPS_CONFIG_LED_AWS_SYNC_PRIO_LOW);
                        }
                    } else
#endif
                    {
                        apps_config_set_background_led_pattern(LED_INDEX_IDLE, false, APPS_CONFIG_LED_AWS_SYNC_PRIO_LOWEST);
                    }
                    break;
                default:
                    ret = false;
                    APPS_LOG_MSGID_W(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Not supported state = %x", 1, local_ctx->connection_state);
                    break;
            }
        }
        break;
    case APPS_EVENTS_INTERACTION_UPDATE_MMI_STATE:
        ret = true;
        apps_config_key_set_mmi_state(homescreen_app_get_mmi_state(self));
        break;
    default:
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", Not supported event id = %d", 1, event_id);
        break;
    }

    return ret;
}

static bool homescreen_app_audio_anc_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;

    switch (event_id) {
#ifdef MTK_ANC_HOWLING_TURN_OFF_ANC
        case ANC_CONTROL_EVENT_HOWLING:
            app_home_screen_disable_anc_when_howling(self);
            break;
#endif
        default:
            break;
    }
    return ret;
}

bool app_home_screen_idle_activity_proc(
            ui_shell_activity_t *self,
            uint32_t event_group,
            uint32_t event_id,
            void *extra_data,
            size_t data_len)
{
    bool ret = false;
    
    APPS_LOG_MSGID_I(UI_SHELL_IDLE_BT_CONN_ACTIVITY", home screen receive event_group : %d, id: %x", 2, event_group, event_id);
    
    switch (event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM: {
            ret = _proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_KEY: {
            ret = _proc_key_event_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_SINK: {
            ret = homescreen_app_bt_sink_event_proc(self, event_id, extra_data, data_len);
            break;

        }
        case EVENT_GROUP_UI_SHELL_BT: {
            ret = homescreen_app_bt_event_proc(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_CONN_MANAGER: {
            ret = homescreen_app_bt_connection_manager_event_proc(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_AWS: {
            ret = homescreen_app_aws_event_proc(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_APP_INTERACTION:
            ret = _app_interaction_event_proc(self, event_id, extra_data, data_len);
            break;
#if defined(MTK_AWS_MCE_ENABLE)
        case EVENT_GROUP_UI_SHELL_AWS_DATA:
            ret = homescreen_app_aws_data_proc(self, event_id, extra_data, data_len);
            break;
#endif
        case EVENT_GROUP_UI_SHELL_AUDIO_ANC:
            ret = homescreen_app_audio_anc_event_proc(self, event_id, extra_data, data_len);
            break;
        default:
            break;
    }
    return ret;
}

bool app_home_screen_idle_activity_is_aws_connected(void)
{
    if (s_homescreen_self) {
        return ((home_screen_local_context_type_t *)s_homescreen_self->local_context)->aws_connected;
    } else {
        return false;
    }
}


