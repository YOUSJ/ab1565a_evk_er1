/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
#include "app_fm_activity.h"
#include "timers.h"
#include "apps_config_led_manager.h"
#include "apps_config_led_index_list.h"
#include "apps_config_vp_manager.h"
#include "apps_config_vp_index_list.h"
#include "apps_config_key_remapper.h"
#include "apps_events_event_group.h"
#include "app_bt_conn_componet_in_homescreen.h"
#ifdef RACE_FIND_ME_ENABLE
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
#include "race_cmd_find_me.h"
#endif
#endif

#define UI_SHELL_FM_ACTIVITY "[FIND_ME_APP]ui_shell_ut_fm_activity"
#define FINDME_EVENT_TIMER_NAME        "find_me_event"
#define FINDME_EVENT_TIMER_ID          0
#define FINDME_EVENT_TIMER_INTERVAL    (4 * 1000)
#define FINDME_TOTAL_TIME              (30 * 1000)

static bool _proc_key_event_group(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len);
static bool _proc_ui_shell_group(
        ui_shell_activity_t *self, 
        uint32_t event_id, 
        void *extra_data,
        size_t data_len);

static void apps_set_find_me(ui_shell_activity_t *self);
//static void findme_timer_callback_function(TimerHandle_t xTimer);
static void apps_stop_find_me(void);
static void apps_do_find_me(ui_shell_activity_t *self, void *extra_data);

static bool _proc_ui_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data,size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    switch (event_id) {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE: {
            APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", create", 0);
#ifdef RACE_FIND_ME_ENABLE
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
            race_cmd_find_me_init();
#endif
#endif
            self->local_context = extra_data;
            app_find_me_param_struct find_me_param = {
                .blink = ((app_find_me_context_t *)self->local_context)->blink,
                .tone = ((app_find_me_context_t *)self->local_context)->tone,
                .duration_seconds = ((app_find_me_context_t *)self->local_context)->duration_seconds
            };
            memset(self->local_context, 0, sizeof(app_find_me_context_t));
            apps_do_find_me(self, &find_me_param);//the first find me 

            // LED will display in apps_do_find_me(), so only trigger MMI updating
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_UPDATE_MMI_STATE, NULL, 0, NULL, 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY: {
            APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", destroy", 0);
#ifdef RACE_FIND_ME_ENABLE
            //apps_stop_find_me();
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
            race_cmd_find_me_deinit();
#endif
#endif
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_UPDATE_LED_BG_PATTERN, NULL, 0, NULL, 0);
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_UPDATE_MMI_STATE, NULL, 0, NULL, 0);

            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESUME: {
            APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", resume", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE: {
            APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", pause", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH: {
            APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", refresh", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESULT: {
            APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", result", 0);
            break;
        }
        default:
            break;
    }
    return ret;
}


static bool _proc_key_event_group(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    uint8_t key_id;
    airo_key_event_t key_event;
    app_event_key_event_decode(&key_id, &key_event, event_id);
    apps_config_key_action_t action;
    if (extra_data) {
        action = *(uint16_t *)extra_data;
    } else {
        action = apps_config_key_event_remapper_map_action(key_id, key_event);
    }
    APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", key_event: key_id : %x, key_event : %x, action :%d", 3, key_id, key_event, action);
    if (action == KEY_STOP_FIND_ME) {
        apps_stop_find_me();
        ui_shell_finish_activity(self, self);
        ret = true;
    }
    return ret;
}


static void apps_do_find_me(ui_shell_activity_t *self, void *extra_data)
{
    app_find_me_param_struct *param = (app_find_me_param_struct*)extra_data;
    app_find_me_context_t *local_context = (app_find_me_context_t *)self->local_context;
    if (extra_data) {
        APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", apps_do_find_me: start...: %x, which_one : %x", 2, param->blink, param->tone);
        uint8_t old_blink = local_context->blink;
        APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", old blink state: %x", 1, old_blink);
        local_context->blink = param->blink;
        local_context->tone = param->tone;
        local_context->duration_seconds = param->duration_seconds;
        local_context->count = 0;
        if (!local_context->blink && !local_context->tone) {
            ui_shell_finish_activity(self, self);
        } else {
#if 0
        APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", find_me_timer : %x", 1, s_find_me_timer);
        if (s_find_me_timer == NULL) {
            s_find_me_timer = xTimerCreate(FINDME_EVENT_TIMER_NAME, FINDME_EVENT_TIMER_INTERVAL / portTICK_PERIOD_MS, pdTRUE, FINDME_EVENT_TIMER_ID, findme_timer_callback_function);
        } else {
            APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", timer is in, stop first", 0);
            apps_stop_find_me();
        }
        APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", find_me_timer : %x", 1, s_find_me_timer);
        if (s_find_me_timer != NULL) {
            APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", timer start ...", 0);
            xTimerStart(s_find_me_timer, 0);
        }
#endif
            if (old_blink && !local_context->blink) {
                ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST,
                        EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                        APPS_EVENTS_INTERACTION_UPDATE_LED_BG_PATTERN, NULL, 0, NULL, 0);
            }
            apps_set_find_me(self);
        }
    }
}

static void apps_stop_find_me(void)
{
#if 0
    if (s_find_me_timer) {
        APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", stop timer...", 0);
        xTimerStop(s_find_me_timer, 0);
    }
#endif
    ui_shell_remove_event(EVENT_GROUP_UI_SHELL_APP_INTERACTION, APPS_EVENTS_INTERACTION_PLAY_FIND_ME_TONE);
}

static void apps_set_find_me(ui_shell_activity_t *self)
{
    app_find_me_context_t *local_context = (app_find_me_context_t *)self->local_context;
    APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", blink: %d, tone : %d", 2, local_context->tone, local_context->tone);
    if (local_context->tone) {
        apps_config_set_vp(VP_INDEX_DOORBELL, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
    }
    if (local_context->blink) {
        apps_config_set_background_led_pattern(LED_INDEX_FIND_ME, false, APPS_CONFIG_LED_AWS_SYNC_PRIO_HIGH);
    }
    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
            APPS_EVENTS_INTERACTION_PLAY_FIND_ME_TONE, NULL, 0,
            NULL, FINDME_EVENT_TIMER_INTERVAL);
}

static void apps_fm_play_tone(ui_shell_activity_t *self)
{
    app_find_me_context_t *local_context = (app_find_me_context_t *)self->local_context;
    APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", apps_fm_play_tone: count: %x", 1, local_context->count);

    local_context->count++;
    if (local_context->count >= ((local_context->duration_seconds > 0
            ? (local_context->duration_seconds * 1000)
            : FINDME_TOTAL_TIME) / FINDME_EVENT_TIMER_INTERVAL)) {
        //ui_shell_finish_activity
        APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", play tone is finished", 0);
        /*ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
        APPS_EVENTS_INTERACTION_FINISH_FIND_ME, NULL, 0,NULL);*/
        ui_shell_finish_activity(self, self);
    } else {
        apps_set_find_me(self);
    }
}

#if 0
static void findme_timer_callback_function(TimerHandle_t xTimer)
{
    APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", timer_callback: count: %x, timer : %x", 2, s_find_me.count, s_find_me_timer);
    apps_set_find_me();
    s_find_me.count++;
    if (s_find_me.count == (FINDME_TOTAL_TIME/FINDME_EVENT_TIMER_INTERVAL)) {
        apps_stop_find_me();
        //ui_shell_finish_activity
        APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", timer is finished", 0);
        ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
        APPS_EVENTS_INTERACTION_FINISH_FIND_ME, NULL, 0, NULL, 0);
    }
}
#endif

bool app_find_me_activity_proc(
            struct _ui_shell_activity *self,
            uint32_t event_group,
            uint32_t event_id,
            void *extra_data,
            size_t data_len)
{
    bool ret = false;
    APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", event_group : %x, id: %x", 2, event_group, event_id);
    switch (event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM: {
            ret = _proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_KEY: {
            ret = _proc_key_event_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_FINDME: {
            APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", event_id : %x", 1, event_id);
            if (event_id == APP_FIND_ME_EVENT_ID_TRIGGER) {//exist find me
                apps_stop_find_me();
                apps_do_find_me(self, extra_data);
                ret = true;
            }
            break;
        }
        case EVENT_GROUP_UI_SHELL_APP_INTERACTION: {
            APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", interaction event_id : %x", 1, event_id);
            if (event_id == APPS_EVENTS_INTERACTION_UPDATE_LED_BG_PATTERN) { //other app trigger
                app_find_me_context_t *local_context = (app_find_me_context_t *)self->local_context;
                APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY",[reset_state] timer is in , find me doing", 0);
                if (local_context->blink) {
                    apps_config_set_background_led_pattern(LED_INDEX_FIND_ME, false, APPS_CONFIG_LED_AWS_SYNC_PRIO_HIGH);
                    ret = true;
                }
            } else if (event_id == APPS_EVENTS_INTERACTION_UPDATE_MMI_STATE) {
                apps_config_key_set_mmi_state(APP_STATE_FIND_ME);
                ret = true;
            } else if (event_id == APPS_EVENTS_INTERACTION_PLAY_FIND_ME_TONE) {
                apps_fm_play_tone(self);
                ret = true; 
            }
            break;
        }
        default:
            break;
    }

    APPS_LOG_MSGID_I(UI_SHELL_FM_ACTIVITY", ret : %x", 1, ret);
    return ret;
}


