/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "multi_ble_adv_manager.h"
#include "multi_va_manager.h"
#include "multi_va_event_id.h"
#include "bt_app_common.h"
#include "bt_sink_srv.h"
#include "bt_device_manager.h"
#include "ui_shell_manager.h"
#include "apps_events_event_group.h"
#include "apps_events_bt_event.h"
#include "apps_events_interaction_event.h"
#ifdef MTK_AWS_MCE_ENABLE
#include "app_rho_idle_activity.h"
#endif
#include "bt_device_manager_le.h"
#include "apps_debug.h"

#define LOG_TAG     "[MULTI_ADV]"

#define MAX_BLE_DATA                (4)
#define SWITCH_ADV_TYPE_MS          (2000)
#define ADV_POWER_SAVING_TIMEOUT    (30 * 1000)
#define TRIGGER_ADV_AFTER_RESOLVING_LIST_TIMEOUT    (1000)

typedef enum {
    MULTI_ADV_STATE_STOPPED      = 0,
    MULTI_ADV_STATE_STOPPING,
    MULTI_ADV_STATE_STARTING,
    MULTI_ADV_STATE_STARTED
} multi_adv_state_t;

typedef enum {
    MULTI_ADV_INTERVAL_NORMAL,
    MULTI_ADV_INTERVAL_POWER_SAVING,
} multi_adv_s_interval_mode_t;

multi_adv_s_interval_mode_t s_interval_mode = MULTI_ADV_INTERVAL_NORMAL;

multi_adv_state_t s_current_adv_state;
multi_adv_state_t s_target_adv_state;

get_ble_adv_data_func_t s_adv_func_array[MAX_BLE_DATA] = { NULL };
get_ble_adv_data_func_t s_default_adv_func = NULL;
uint32_t s_current_adv_id = 0;
uint32_t s_adv_func_count = 0;
bool s_adv_changed = false;
uint8_t s_pause_adv = 0;
/* For GVA-16046. Work around problem for starting BLE adv when BLE pairing */
typedef enum {
    MULTI_ADV_LE_ADD_RESOLVING_LIST_IDLE,
    MULTI_ADV_LE_ADD_RESOLVING_LIST_NEED_CALL,
    MULTI_ADV_LE_ADD_RESOLVING_LIST_WAIT_CALL_CFM,
} multi_adv_le_add_resolving_list_state_t;
multi_adv_le_add_resolving_list_state_t s_check_resolving_list_state = MULTI_ADV_LE_ADD_RESOLVING_LIST_IDLE;

static get_ble_adv_data_func_t multi_ble_adv_manager_get_adv_data(void)
{
    get_ble_adv_data_func_t temp_func = NULL;
    size_t i;
    int start_id;
    if (0 == s_adv_func_count) {
        APPS_LOG_MSGID_E(LOG_TAG"get_adv_data, no adv data", 0);
        return NULL;
    } else if (s_adv_func_count == 1) {
        start_id = s_current_adv_id; // Search from current id. Probely it is the same one.
    } else {
        start_id = s_current_adv_id + 1; // Search from next id.
    }
    for (i = 0; i < MAX_BLE_DATA; i++) {
        temp_func = s_adv_func_array[(start_id + i) % MAX_BLE_DATA];
        if (temp_func) {
            s_current_adv_id = (start_id + i) % MAX_BLE_DATA;
            break;
        }
    }

    APPS_LOG_MSGID_I(LOG_TAG"get_adv_data, info[%d]func(%x)", 2,
            s_current_adv_id, temp_func);
    return temp_func;
}

static void multi_ble_adv_manager_do_start_ble_adv(void)
{
    bt_app_common_ble_adv_status_t status = bt_app_common_get_advertising_status();
#ifdef MTK_AWS_MCE_ENABLE
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
#endif

    if ((s_target_adv_state == MULTI_ADV_STATE_STARTED && !s_pause_adv)
#ifdef MTK_AWS_MCE_ENABLE
            && (BT_AWS_MCE_ROLE_AGENT == role || BT_AWS_MCE_ROLE_NONE == role)
#endif
            ) {
        if (BT_APP_COMMON_BLE_ADV_STARTED ==  status) {
            bt_status_t status = bt_app_common_advtising_stop();
            // If BLE adv is started, stop it first
            if (status == BT_STATUS_SUCCESS) {
                APPS_LOG_MSGID_I(LOG_TAG"start adv, Successed to stop BLE adv", 0);
                if (s_adv_func_count > 0 || s_default_adv_func) {
                    s_current_adv_state = MULTI_ADV_STATE_STARTING;
                } else {
                    s_current_adv_state = MULTI_ADV_STATE_STOPPED;
                }
            } else {
                APPS_LOG_MSGID_W(LOG_TAG"start adv, Fail to stop BLE adv", 0);
            }
        } else if (BT_APP_COMMON_BLE_ADV_STOPPED == status) {
            /* For GVA-16046. Work around problem for starting BLE adv when BLE pairing */
            if (MULTI_ADV_LE_ADD_RESOLVING_LIST_NEED_CALL == s_check_resolving_list_state) {
                bt_device_manager_le_update_resolving_list();
                /* Need waiting , send MULTI_VA_EVENT_CHANGE_BLE_ADV_TYPE to restart adv */
                ui_shell_remove_event(EVENT_GROUP_UI_SHELL_MULTI_VA,
                            MULTI_VA_EVENT_CHANGE_BLE_ADV_TYPE);
                ui_shell_send_event(false, EVENT_PRIORITY_MIDDLE,
                        EVENT_GROUP_UI_SHELL_MULTI_VA,
                        MULTI_VA_EVENT_CHANGE_BLE_ADV_TYPE,
                        NULL, 0, NULL, TRIGGER_ADV_AFTER_RESOLVING_LIST_TIMEOUT);
                APPS_LOG_MSGID_W(LOG_TAG"delay to start ble adv for resolving list", 0);
                s_check_resolving_list_state = MULTI_ADV_LE_ADD_RESOLVING_LIST_WAIT_CALL_CFM;
                return;
            }
            // If BLE adv is not started, start ble adv
            bt_status_t status;
            get_ble_adv_data_func_t ble_adv_func = multi_ble_adv_manager_get_adv_data();
            bt_hci_cmd_le_set_advertising_parameters_t default_adv_param;
            bt_hci_cmd_le_set_advertising_data_t default_adv_data;
            bt_hci_cmd_le_set_scan_response_data_t default_scan_rsp;
            multi_ble_adv_info_t adv_info = {
                &default_adv_param, &default_adv_data, &default_scan_rsp};
            memset(&default_adv_param, 0, sizeof(default_adv_param));
            memset(&default_adv_data, 0, sizeof(default_adv_data));
            memset(&default_scan_rsp, 0, sizeof(default_scan_rsp));

            // Variables for fill adv data
            bt_hci_cmd_le_set_advertising_parameters_t *temp_adv_param = NULL;
            bt_hci_cmd_le_set_advertising_data_t *temp_adv_data = NULL;
            bt_hci_cmd_le_set_scan_response_data_t *temp_scan_rsp = NULL;

            if (!ble_adv_func) {
                ble_adv_func = s_default_adv_func;
            }
            if (ble_adv_func) {
                uint32_t gen_ret = ble_adv_func(&adv_info);

                if (gen_ret & MULTI_BLE_ADV_NEED_GEN_ADV_PARAM)  {
                    APPS_LOG_MSGID_I(LOG_TAG"adv_info.adv_param need gen default", 0);
                    temp_adv_param = adv_info.adv_param;
                }
                if (gen_ret & MULTI_BLE_ADV_NEED_GEN_ADV_DATA)  {
                    APPS_LOG_MSGID_I(LOG_TAG"adv_info.adv_data need gen default", 0);
                    temp_adv_data = adv_info.adv_data;
                }
                if (gen_ret & MULTI_BLE_ADV_NEED_GEN_SCAN_RSP)  {
                    APPS_LOG_MSGID_I(LOG_TAG"adv_info.scan_rsp need gen default", 0);
                    temp_scan_rsp = adv_info.scan_rsp;
                }
                bt_app_common_generate_default_adv_data(
                        temp_adv_param, temp_adv_data, temp_scan_rsp, NULL, 0);
                if (adv_info.adv_param->advertising_interval_max == 0) {
                    if (MULTI_ADV_INTERVAL_NORMAL == s_interval_mode) {
                        default_adv_param.advertising_interval_min = 0xA0;
                        default_adv_param.advertising_interval_max = 0xA0;
                    } else {
                        default_adv_param.advertising_interval_min = 0x29C;
                        default_adv_param.advertising_interval_max = 0x29C;
                    }
                } else {
                    if (s_adv_func_count == 1) {
                        // When current adv is fixed interval, not necessary to change the adv interval
                        ui_shell_remove_event(EVENT_GROUP_UI_SHELL_MULTI_VA,
                                MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL);
                    }
                }

                status = bt_app_common_advertising_start_ex(
                        adv_info.adv_param,
                        adv_info.adv_data,
                        adv_info.scan_rsp);
                if (status == BT_STATUS_SUCCESS) {
                    APPS_LOG_MSGID_I(LOG_TAG"start adv, Successed to start BLE adv", 0);
                    ui_shell_remove_event(EVENT_GROUP_UI_SHELL_MULTI_VA,
                            MULTI_VA_EVENT_CHANGE_BLE_ADV_TYPE);
                    if (s_adv_func_count > 1) {
                        ui_shell_send_event(false, EVENT_PRIORITY_MIDDLE,
                                EVENT_GROUP_UI_SHELL_MULTI_VA,
                                MULTI_VA_EVENT_CHANGE_BLE_ADV_TYPE,
                                NULL, 0, NULL, SWITCH_ADV_TYPE_MS);
                    }
                } else {
                    APPS_LOG_MSGID_W(LOG_TAG"start adv, Fail to start BLE adv", 0);
                }
                s_current_adv_state = MULTI_ADV_STATE_STARTED;
            } else {
                APPS_LOG_MSGID_W(LOG_TAG"Cannot start adv, because ble_adv_info is NULL", 0);
                s_current_adv_state = MULTI_ADV_STATE_STOPPED;
                ui_shell_remove_event(EVENT_GROUP_UI_SHELL_MULTI_VA,
                        MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL);
            }
        } else {
            APPS_LOG_MSGID_W(LOG_TAG"start adv, start BLE adv in busy status", 0);
            s_current_adv_state = MULTI_ADV_STATE_STARTING;
        }
    } else {
        s_current_adv_state = MULTI_ADV_STATE_STOPPED;
        APPS_LOG_MSGID_W(LOG_TAG"start adv, fail, current target is : %d", 1, s_target_adv_state);
    }
}

static void multi_ble_adv_manager_do_stop_ble_adv(void)
{
    bt_app_common_ble_adv_status_t status = bt_app_common_get_advertising_status();
#ifdef MTK_AWS_MCE_ENABLE
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
#endif

    if ((s_target_adv_state == MULTI_ADV_STATE_STOPPED || s_pause_adv)
#ifdef MTK_AWS_MCE_ENABLE
            && (BT_AWS_MCE_ROLE_AGENT == role || BT_AWS_MCE_ROLE_NONE == role)
#endif
            ) {
        if (BT_APP_COMMON_BLE_ADV_STARTED == status) {
            // If BLE adv is started, stop it directly
            bt_status_t status = bt_app_common_advtising_stop();
            if (status == BT_STATUS_SUCCESS) {
                APPS_LOG_MSGID_I(LOG_TAG"stop adv, Successed to stop BLE adv", 0);
            } else {
                APPS_LOG_MSGID_W(LOG_TAG"stop adv, Fail to stop BLE adv", 0);
            }
            s_current_adv_state = MULTI_ADV_STATE_STOPPED;
        } else if (BT_APP_COMMON_BLE_ADV_STOPPED == status) {
            APPS_LOG_MSGID_W(LOG_TAG"stop adv, already stopped", 0);
            s_current_adv_state = MULTI_ADV_STATE_STOPPED;
        } else {
            APPS_LOG_MSGID_W(LOG_TAG"stop adv, stop BLE adv in busy status", 0);
            // May be stopped by Fast pairing
            s_current_adv_state = MULTI_ADV_STATE_STOPPED;
        }
    } else {
        s_current_adv_state = MULTI_ADV_STATE_STOPPED;
        APPS_LOG_MSGID_W(LOG_TAG"stop adv, fail, current target is : %d", 1, s_target_adv_state);
    }
}

void multi_ble_adv_manager_start_ble_adv(void)
{
#ifdef MTK_AWS_MCE_ENABLE
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
#endif
    s_target_adv_state = MULTI_ADV_STATE_STARTED;
    s_interval_mode = MULTI_ADV_INTERVAL_NORMAL;
    ui_shell_remove_event(EVENT_GROUP_UI_SHELL_MULTI_VA,
            MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL);

#ifdef MTK_AWS_MCE_ENABLE
    if (BT_AWS_MCE_ROLE_AGENT == role || BT_AWS_MCE_ROLE_NONE == role)
#endif
    {
        ui_shell_send_event(false, EVENT_PRIORITY_MIDDLE,
                EVENT_GROUP_UI_SHELL_MULTI_VA,
                MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL,
                NULL, 0, NULL, ADV_POWER_SAVING_TIMEOUT);
    }

    multi_ble_adv_manager_do_start_ble_adv();
}

void multi_ble_adv_manager_stop_ble_adv(void)
{
    s_target_adv_state = MULTI_ADV_STATE_STOPPED;
    s_current_adv_state = MULTI_ADV_STATE_STOPPED;
    ui_shell_remove_event(EVENT_GROUP_UI_SHELL_MULTI_VA,
            MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL);
    ui_shell_remove_event(EVENT_GROUP_UI_SHELL_MULTI_VA,
            MULTI_VA_EVENT_CHANGE_BLE_ADV_TYPE);
    s_check_resolving_list_state = MULTI_ADV_LE_ADD_RESOLVING_LIST_IDLE;
    multi_ble_adv_manager_do_stop_ble_adv();
}

multi_adv_pause_result_t multi_ble_adv_manager_pause_ble_adv(void)
{
    multi_adv_pause_result_t ret = MULTI_ADV_PAUSE_RESULT_HAVE_STOPPED;
    bt_app_common_ble_adv_status_t adv_state = bt_app_common_get_advertising_status();
    APPS_LOG_MSGID_I(LOG_TAG"pause adv, current = %d, state = %d, s_pause_adv = %d", 3,
            s_current_adv_state, adv_state, s_pause_adv);
    if (s_current_adv_state == MULTI_ADV_STATE_STOPPED
            && BT_APP_COMMON_BLE_ADV_STOPPED == adv_state) {
        ret = MULTI_ADV_PAUSE_RESULT_HAVE_STOPPED;
    } else {
        s_pause_adv++;
        if (s_pause_adv <= 1) {
            multi_ble_adv_manager_do_stop_ble_adv();
        }
        ret = MULTI_ADV_PAUSE_RESULT_WAITING;
    }
    return ret;
}

void multi_ble_adv_manager_resume_ble_adv(void)
{
    bt_app_common_ble_adv_status_t adv_state = bt_app_common_get_advertising_status();
    APPS_LOG_MSGID_I(LOG_TAG"resume adv, current = %d, state = %d, s_pause_adv = %d", 3,
            s_current_adv_state, adv_state, s_pause_adv);
    if (s_pause_adv) {
        s_pause_adv--;
        if (s_pause_adv == 0) {
            multi_ble_adv_manager_do_start_ble_adv();
        }
    }
}

bool multi_ble_adv_manager_add_ble_adv(get_ble_adv_data_func_t get_ble_adv_func)
{
    size_t i;
    size_t duplicate_id = 0xFF;
    size_t new_id = 0xFF;
    bool ret = true;

    for (i = 0; i < MAX_BLE_DATA; i++) {
        if (NULL == s_adv_func_array[i] && new_id >= MAX_BLE_DATA) {
            new_id = i;
        } else if (s_adv_func_array[i] == get_ble_adv_func) {
            duplicate_id = i;
            break;
        }
    }

    if (duplicate_id < MAX_BLE_DATA) {
        APPS_LOG_MSGID_I(LOG_TAG"add_ble_adv is duplicate at [%d] for %x",
                2, duplicate_id, get_ble_adv_func);
    } else if (new_id < MAX_BLE_DATA) {
        APPS_LOG_MSGID_I(LOG_TAG"add_ble_adv new at [%d] for %x",
                2, new_id, get_ble_adv_func);
        s_adv_func_array[new_id] = get_ble_adv_func;
        s_adv_func_count++;
        s_adv_changed = true;
    } else {
        APPS_LOG_MSGID_E(LOG_TAG"add_ble_adv fail, current s_adv_func_count = %d",
                1, s_adv_func_count);
        ret = false;
    }

    return ret;
}

bool multi_ble_adv_manager_remove_ble_adv(get_ble_adv_data_func_t get_ble_adv_func)
{
    size_t i;
    bool ret = false;
    for (i = 0; i < MAX_BLE_DATA; i++) {
        if (s_adv_func_array[i] == get_ble_adv_func) {
            s_adv_func_array[i] = NULL;
            s_adv_func_count--;
            ret = true;
            s_adv_changed = true;
            APPS_LOG_MSGID_I(LOG_TAG"remove_ble_adv success, id = %d for %x, s_adv_func_count = %d",
                    3, i, get_ble_adv_func, s_adv_func_count);
            break;
        }
    }

    if (!ret) {
        APPS_LOG_MSGID_E(LOG_TAG"remove_ble_adv fail, current s_adv_func_count = %d",
                1, s_adv_func_count);
    }

    return ret;
}

void multi_ble_adv_manager_notify_ble_adv_data_changed(void)
{
    if (MULTI_ADV_STATE_STARTED == s_target_adv_state) {
        if (!s_adv_changed) {
            APPS_LOG_MSGID_I(LOG_TAG "notify_ble_adv_data_changed but not changed", 0);
            return;
        }
#ifdef MTK_AWS_MCE_ENABLE
        bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
        if (BT_AWS_MCE_ROLE_AGENT == role || BT_AWS_MCE_ROLE_NONE == role)
#endif
        {
            s_interval_mode = MULTI_ADV_INTERVAL_NORMAL;
            ui_shell_remove_event(EVENT_GROUP_UI_SHELL_MULTI_VA,
                    MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL);

            ui_shell_send_event(false, EVENT_PRIORITY_MIDDLE,
                    EVENT_GROUP_UI_SHELL_MULTI_VA,
                    MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL,
                    NULL, 0, NULL, ADV_POWER_SAVING_TIMEOUT);
            multi_ble_adv_manager_do_start_ble_adv();
        }
    }
}

void multi_ble_adv_manager_set_default_adv_function(get_ble_adv_data_func_t func)
{
    if (func != s_default_adv_func) {
        APPS_LOG_MSGID_I(LOG_TAG "set_default_adv_function, s_adv_func_count = %d",
                1, s_adv_func_count);
        if (s_adv_func_count == 0) {
            s_adv_changed = true;
        }
        s_default_adv_func = func;
    }
}

void multi_ble_adv_manager_bt_event_proc(uint32_t event_id, void *extra_data, size_t data_len)
{
    apps_bt_event_data_t *bt_event_data = (apps_bt_event_data_t *)extra_data;

    switch(event_id) {
        case BT_GAP_LE_SET_ADVERTISING_CNF: {
            if (bt_event_data->status == BT_STATUS_SUCCESS) {
                APPS_LOG_MSGID_I(LOG_TAG "BT_GAP_LE_SET_ADVERTISING_CNF", 0);
                if (MULTI_ADV_STATE_STARTING == s_current_adv_state) {
                    APPS_LOG_MSGID_I(LOG_TAG "BT_GAP_LE_SET_ADVERTISING_CNF and need start adv", 0);
                    multi_ble_adv_manager_do_start_ble_adv();
                }
            } else {
                APPS_LOG_MSGID_I(LOG_TAG "BT_GAP_LE_SET_ADVERTISING_CNF is fail : %x ", 1, bt_event_data->status);
                if (MULTI_ADV_STATE_STARTING == s_current_adv_state) {
                    APPS_LOG_MSGID_I(LOG_TAG "BT_GAP_LE_SET_ADVERTISING_CNF and need start adv", 0);
                    multi_ble_adv_manager_do_start_ble_adv();
                }
            }
            break;
        }
        case BT_GAP_LE_DISCONNECT_IND:
            APPS_LOG_MSGID_I(LOG_TAG"BLE disconnected, s_target_adv_state = %d", 1, s_target_adv_state);
            s_check_resolving_list_state = MULTI_ADV_LE_ADD_RESOLVING_LIST_IDLE;
            if (MULTI_ADV_STATE_STARTED == s_target_adv_state) {
#ifdef MTK_AWS_MCE_ENABLE
                bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
                if (BT_AWS_MCE_ROLE_AGENT == role || BT_AWS_MCE_ROLE_NONE == role)
#endif
                {
                    s_interval_mode = MULTI_ADV_INTERVAL_NORMAL;
                    ui_shell_remove_event(EVENT_GROUP_UI_SHELL_MULTI_VA,
                            MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL);
                    ui_shell_send_event(false, EVENT_PRIORITY_MIDDLE,
                            EVENT_GROUP_UI_SHELL_MULTI_VA,
                            MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL,
                            NULL, 0, NULL, ADV_POWER_SAVING_TIMEOUT);
                    multi_ble_adv_manager_do_start_ble_adv();
                }
#ifdef MTK_AWS_MCE_ENABLE
                else {
                    APPS_LOG_MSGID_I(LOG_TAG"Now should not restart ble adv: current role = %x", 1,
                            bt_device_manager_aws_local_info_get_role());
                }
#endif
            }
            break;
        case BT_GAP_LE_CONNECT_IND:
            APPS_LOG_MSGID_I(LOG_TAG"BLE connected, s_target_adv_state = %d", 1, s_target_adv_state);
            s_check_resolving_list_state = MULTI_ADV_LE_ADD_RESOLVING_LIST_NEED_CALL;
            ui_shell_remove_event(EVENT_GROUP_UI_SHELL_MULTI_VA,
                    MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL);
            break;
        default:
            break;
    }
}

void multi_ble_adv_manager_multi_va_proc(uint32_t event_id, void *extra_data, size_t data_len)
{
#ifdef MTK_AWS_MCE_ENABLE
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
#endif

    switch(event_id) {
        case MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL:
            APPS_LOG_MSGID_I(LOG_TAG"MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL", 0);
            if (MULTI_ADV_STATE_STARTED == s_target_adv_state
#ifdef MTK_AWS_MCE_ENABLE
                    && (BT_AWS_MCE_ROLE_AGENT == role || BT_AWS_MCE_ROLE_NONE == role)
#endif
                    ) {
                s_interval_mode = MULTI_ADV_INTERVAL_POWER_SAVING;
                if (s_adv_func_count > 1
                        || MULTI_ADV_LE_ADD_RESOLVING_LIST_WAIT_CALL_CFM == s_check_resolving_list_state) {
                    APPS_LOG_MSGID_I(LOG_TAG"MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL, have multi adv, wait for it switch", 0);
                } else {
                    multi_ble_adv_manager_do_start_ble_adv();
                }
            } else {
                APPS_LOG_MSGID_E(LOG_TAG"MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL, why s_target_adv_state is not started", 0);
            }
            break;
        case MULTI_VA_EVENT_CHANGE_BLE_ADV_TYPE:
            APPS_LOG_MSGID_I(LOG_TAG"MULTI_VA_EVENT_CHANGE_BLE_ADV_TYPE", 0);
            if (MULTI_ADV_STATE_STARTED == s_target_adv_state
#ifdef MTK_AWS_MCE_ENABLE
                    && (BT_AWS_MCE_ROLE_AGENT == role || BT_AWS_MCE_ROLE_NONE == role)
#endif
                    ) {
                if (MULTI_ADV_LE_ADD_RESOLVING_LIST_WAIT_CALL_CFM == s_check_resolving_list_state) {
                    s_check_resolving_list_state = MULTI_ADV_LE_ADD_RESOLVING_LIST_IDLE;
                }
                multi_ble_adv_manager_do_start_ble_adv();
            } else {
                APPS_LOG_MSGID_E(LOG_TAG"MULTI_VA_EVENT_CHANGE_BLE_ADV_TYPE, why s_target_adv_state is not started", 0);
            }
            break;
        default:
            break;
    }
}

void multi_ble_adv_manager_interaction_proc(uint32_t event_id, void *extra_data, size_t data_len)
{
    switch(event_id) {
#if defined(MTK_AWS_MCE_ENABLE) && defined(SUPPORT_ROLE_HANDOVER_SERVICE)
        case APPS_EVENTS_INTERACTION_RHO_STARTED: {
            bt_aws_mce_role_t role = (uint32_t)extra_data;
            if (BT_AWS_MCE_ROLE_AGENT == role || BT_AWS_MCE_ROLE_NONE == role) {
                APPS_LOG_MSGID_I(LOG_TAG"agent role switch, stop ble adv", 0);
                s_target_adv_state = MULTI_ADV_STATE_STOPPED;
                s_current_adv_state = MULTI_ADV_STATE_STOPPED;
            }
        }
            break;
        case APPS_EVENTS_INTERACTION_RHO_END: {
            app_rho_result_t status = (app_rho_result_t)extra_data;
            if (APP_RHO_RESULT_SUCCESS == status) {
                APPS_LOG_MSGID_I(LOG_TAG"agent -> partner, stop ble adv", 0);
                s_target_adv_state = MULTI_ADV_STATE_STOPPED;
                s_current_adv_state = MULTI_ADV_STATE_STOPPED;
                ui_shell_remove_event(EVENT_GROUP_UI_SHELL_MULTI_VA,
                        MULTI_VA_EVENT_CHANGE_BLE_ADV_INTERVAL);
                ui_shell_remove_event(EVENT_GROUP_UI_SHELL_MULTI_VA,
                        MULTI_VA_EVENT_CHANGE_BLE_ADV_TYPE);
                s_check_resolving_list_state = MULTI_ADV_LE_ADD_RESOLVING_LIST_IDLE;
            } else {
                APPS_LOG_MSGID_E(LOG_TAG"agent rho failed, restart ble adv", 0);
                multi_ble_adv_manager_start_ble_adv();
            }
        }
            break;
        case APPS_EVENTS_INTERACTION_PARTNER_SWITCH_TO_AGENT:
        {
            app_rho_result_t status = (app_rho_result_t)extra_data;
            if (APP_RHO_RESULT_SUCCESS == status) {
                APPS_LOG_MSGID_I(LOG_TAG"partner -> agent, start ble adv", 0);
                multi_ble_adv_manager_start_ble_adv();
            } else {
                APPS_LOG_MSGID_E(LOG_TAG"partner rho failed, ignore", 0);
            }
        }
            break;
#endif
        default:
            break;
    }
}
