/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "multi_va_manager.h"
#include "multi_va_event_id.h"
#include "apps_events_interaction_event.h"
#include "FreeRTOS.h"
#include "apps_debug.h"
#include "nvkey.h"
#include "nvkey_id_list.h"
#include "apps_aws_sync_event.h"
#include "apps_events_event_group.h"
#include "ui_shell_manager.h"
#include "apps_config_key_remapper.h"
#include "apps_customer_config.h"
#include "bt_device_manager.h"
#include <string.h>
#ifdef MTK_AWS_MCE_ENABLE
#include "bt_aws_mce_srv.h"
#endif

#define LOG_TAG     "[Multi_VA] "

typedef struct {
    multi_va_manager_callbacks_t *p_callbacks;
} multi_va_register_item_t;

static uint8_t s_va_type;
static uint8_t removing_va_type;
static uint8_t s_va_connected = false;
static bool s_rebooting = false;
static bool s_enable_adv = false;

multi_va_register_item_t va_info_array[MULTI_VA_TYPE_MAX_NUM];

void multi_voice_assistant_manager_register_instance(
        multi_va_type_t va_type,
        const multi_va_manager_callbacks_t *p_callbacks)
{
    if (va_type < MULTI_VA_TYPE_MAX_NUM && p_callbacks) {
        va_info_array[va_type].p_callbacks = (multi_va_manager_callbacks_t *)pvPortMalloc(sizeof(multi_va_manager_callbacks_t));
        if (va_info_array[va_type].p_callbacks) {
            memcpy(va_info_array[va_type].p_callbacks, p_callbacks,
                    sizeof(multi_va_manager_callbacks_t));
        } else {
            APPS_LOG_MSGID_E(LOG_TAG"register_instance malloc fail", 0);
        }
    } else {
        APPS_LOG_MSGID_E(LOG_TAG"register_instance parameter error va_type: %d", 1, va_type);
    }
}

void multi_va_manager_start(void)
{
    uint32_t read_size = sizeof(s_va_type);
    nvkey_status_t read_status;

    s_va_type = MULTI_VA_TYPE_UNKNOWN;
    removing_va_type = MULTI_VA_TYPE_UNKNOWN;
    read_status = nvkey_read_data(NVKEYID_VA_SWITH, &s_va_type, &read_size);
    APPS_LOG_MSGID_I(LOG_TAG"start va_type is: %d, read_status = %d", 2, s_va_type, read_status);
    if (NVKEY_STATUS_OK != read_status) {
        s_va_type = APPS_DEFAULT_VA_TYPE;
    }

#ifdef MULTI_VA_SUPPORT_COMPETITION
    size_t i;
    for (i = 0; i < MULTI_VA_TYPE_MAX_NUM; i++) {
        if (va_info_array[i].p_callbacks) {
            va_info_array[i].p_callbacks->voice_assistant_initialize(s_va_type == i);
        }
    }
#else
    // Because not support MULTI_VA_SUPPORT_COMPETITION, only start one VA
    if (s_va_type < MULTI_VA_TYPE_MAX_NUM && va_info_array[s_va_type].p_callbacks) {
        va_info_array[s_va_type].p_callbacks->voice_assistant_initialize(true);
    } else {
        APPS_LOG_MSGID_E(LOG_TAG"Cannot init the va type: %d", 1, s_va_type);
    }
#endif
    // Because in system starting, not necessary to call notify function
    // multi_ble_adv_manager_notify_ble_adv_data_changed();
}

multi_va_type_t multi_va_manager_get_current_va_type(void)
{
    return s_va_type;
}

#if defined(MTK_AWS_MCE_ENABLE) && defined(MULTI_VA_SUPPORT_COMPETITION)
void multi_va_manager_send_va_type_to_partner(void)
{
    bt_status_t send_ret = apps_aws_sync_event_send_extra(EVENT_GROUP_UI_SHELL_MULTI_VA,
            MULTI_VA_EVENT_SYNC_VA_TYPE_TO_PARTNER, &s_va_type, sizeof(s_va_type));
    APPS_LOG_MSGID_I(LOG_TAG"send_va_type_to_partner(), type = %d", 1, s_va_type);
    if (BT_STATUS_SUCCESS != send_ret) {
        APPS_LOG_MSGID_W(LOG_TAG"send_va_type_to_partner(), fail = %x", 1, send_ret);
    }
}

void multi_va_manager_receive_va_change_from_agent(void *p_va_type)
{
    nvkey_status_t write_status;
    uint8_t va_type = *(uint8_t *)p_va_type;
    size_t i;
    APPS_LOG_MSGID_I(LOG_TAG"_receive_va_change_from_agent(), type = %d, s_va_type = %d",
            2, va_type, s_va_type);
    if (s_va_type != va_type) {
        if (s_va_type >= MULTI_VA_TYPE_MAX_NUM) {
            if (va_type < MULTI_VA_TYPE_MAX_NUM) {
                for (i = 0; i < MULTI_VA_TYPE_MAX_NUM; i++) {
                    if (va_type != i && va_info_array[i].p_callbacks) {
                        multi_ble_adv_manager_remove_ble_adv(va_info_array[i].p_callbacks->on_get_ble_adv_data);
                    }
                }
            } else {
                // Not change really.
            }
        } else {
            // Remove the old adv
            if (va_info_array[s_va_type].p_callbacks) {
                va_info_array[s_va_type].p_callbacks->on_voice_assistant_type_switch(false);
                multi_ble_adv_manager_remove_ble_adv(va_info_array[s_va_type].p_callbacks->on_get_ble_adv_data);
            }
            // Add new adv
            if (va_type < MULTI_VA_TYPE_MAX_NUM) {
                if (va_info_array[va_type].p_callbacks) {
                    multi_ble_adv_manager_add_ble_adv(va_info_array[va_type].p_callbacks->on_get_ble_adv_data);
                }
            } else {
                for (i = 0; i < MULTI_VA_TYPE_MAX_NUM; i++) {
                    if (s_va_type != i && va_info_array[i].p_callbacks) {
                        multi_ble_adv_manager_add_ble_adv(va_info_array[i].p_callbacks->on_get_ble_adv_data);
                    }
                }
            }
        }

        if (va_type < MULTI_VA_TYPE_MAX_NUM) {
            if (va_info_array[va_type].p_callbacks) {
                va_info_array[va_type].p_callbacks->on_voice_assistant_type_switch(true);
            }
        }

        apps_config_key_remapper_set_va_type(s_va_type, va_type);
        s_va_type = va_type;
        write_status = nvkey_write_data(NVKEYID_VA_SWITH, &s_va_type, sizeof(s_va_type));
        if (NVKEY_STATUS_OK != write_status) {
            APPS_LOG_MSGID_I(LOG_TAG"_receive_va_change_from_agent(), Write NVKEYID_VA_SWITH failed %d", 1, write_status);
        }
    }
}

void multi_va_manager_receive_reboot_from_agent(void)
{
    APPS_LOG_MSGID_I(LOG_TAG"multi_va_manager_receive_reboot_from_agent(), type = %d",
            1, s_va_type);
    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST,
            EVENT_GROUP_UI_SHELL_APP_INTERACTION, APPS_EVENTS_INTERACTION_REQUEST_ON_OFF_BT,
            (void *)0, 0, NULL, 0);
    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST,
            EVENT_GROUP_UI_SHELL_APP_INTERACTION, APPS_EVENTS_INTERACTION_REQUEST_REBOOT,
            NULL, 0, NULL, 100);
}

void multi_va_manager_on_partner_detached(void)
{
    if (s_rebooting){
        APPS_LOG_MSGID_I(LOG_TAG"multi_va_manager_partner_detached()", 0);
        ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST,
                EVENT_GROUP_UI_SHELL_APP_INTERACTION, APPS_EVENTS_INTERACTION_REQUEST_REBOOT,
                NULL, 0, NULL, 0);
    }
}
#endif

static void multi_va_manager_reboot(void)
{
    s_rebooting = true;
#if defined(MTK_AWS_MCE_ENABLE) && defined(MULTI_VA_SUPPORT_COMPETITION)
    bt_status_t send_ret = BT_STATUS_SUCCESS;
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();

    APPS_LOG_MSGID_I(LOG_TAG"multi_va_manager_reboot(), type = %d, role = %x",
            2, s_va_type, role);
    if (BT_AWS_MCE_ROLE_AGENT == role || BT_AWS_MCE_ROLE_NONE == role) {
        send_ret = apps_aws_sync_event_send_extra(EVENT_GROUP_UI_SHELL_MULTI_VA,
                MULTI_VA_EVENT_SYNC_REBOOT, NULL, 0);
    } else if (BT_AWS_MCE_SRV_LINK_NONE != bt_aws_mce_srv_get_link_type()) {
        // When set VA type from Smart phone, Partner should wait agent sending reboot request.
        APPS_LOG_MSGID_I(LOG_TAG"multi_va_manager_reboot(), partner connected, just wait", 0);
    } else {
        send_ret = BT_STATUS_UNSUPPORTED;
        APPS_LOG_MSGID_I(LOG_TAG"multi_va_manager_reboot(), partner disconnected reboot now", 0);
    }
 
    if (BT_STATUS_SUCCESS != send_ret)
#endif
    {
        APPS_LOG_MSGID_W(LOG_TAG"multi_va_manager_reboot, directly_reboot", 0);
        ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST,
                EVENT_GROUP_UI_SHELL_APP_INTERACTION, APPS_EVENTS_INTERACTION_REQUEST_REBOOT,
                NULL, 0, NULL, 0);
    }
}

void multi_voice_assistant_manager_notify_va_connected(multi_va_type_t va_type)
{
#ifdef MULTI_VA_SUPPORT_COMPETITION
    nvkey_status_t write_status;
    APPS_LOG_MSGID_I(LOG_TAG"notify_va_connected, va_type: (%d)->(%d)", 2, s_va_type, va_type);
    if (s_va_type < MULTI_VA_TYPE_MAX_NUM && s_va_type != va_type) {
        // s_va_type changed, it's va changed need reboot
        multi_va_switch_off_return_t switch_off_result = MULTI_VA_SWITCH_OFF_SET_INACTIVE_DONE;
        if (va_info_array[s_va_type].p_callbacks) {
            switch_off_result =
                    va_info_array[s_va_type].p_callbacks->on_voice_assistant_type_switch(false);
            APPS_LOG_MSGID_I(LOG_TAG"notify_va_connected(), switch off result : %d",
                    1, switch_off_result);
        }
        if (MULTI_VA_SWITCH_OFF_WAIT_INACTIVE == switch_off_result) {
            removing_va_type = s_va_type;
        }

        apps_config_key_remapper_set_va_type(s_va_type, va_type);
        s_va_type = va_type;
        write_status = nvkey_write_data(NVKEYID_VA_SWITH, &s_va_type, sizeof(s_va_type));
        if (NVKEY_STATUS_OK != write_status) {
            APPS_LOG_MSGID_E(LOG_TAG"notify_va_connected(), Write NVKEYID_VA_SWITH failed %d", 1, write_status);
        }
#if defined(MTK_AWS_MCE_ENABLE)
        // Sync to partner
        multi_va_manager_send_va_type_to_partner();
#endif
        if (MULTI_VA_SWITCH_OFF_SET_INACTIVE_DONE == switch_off_result) {
            multi_va_manager_reboot();
        }

    } else {
        if (s_va_type >= MULTI_VA_TYPE_MAX_NUM && s_va_type != va_type) {
            apps_config_key_remapper_set_va_type(s_va_type, va_type);
            s_va_type = va_type;
            APPS_LOG_MSGID_I(LOG_TAG"notify_va_connected(), first time switch %d", 1, va_type);
            write_status = nvkey_write_data(NVKEYID_VA_SWITH, &s_va_type, sizeof(s_va_type));
            if (NVKEY_STATUS_OK != write_status) {
                APPS_LOG_MSGID_E(LOG_TAG"notify_va_connected(), Write NVKEYID_VA_SWITH failed %d", 1, write_status);
            }
#if defined(MTK_AWS_MCE_ENABLE)
            // sync to partner
            multi_va_manager_send_va_type_to_partner();
#endif
            if (s_enable_adv) {
                if (va_info_array[va_type].p_callbacks) {
                    multi_ble_adv_manager_remove_ble_adv(va_info_array[va_type].p_callbacks->on_get_ble_adv_data);
                    multi_ble_adv_manager_notify_ble_adv_data_changed();
                }
            }
        } else if (s_va_type == va_type) {
            APPS_LOG_MSGID_I(LOG_TAG"notify_va_connected(), va_type_not change", 0);
            if (s_enable_adv) {
                size_t i;
                if (va_info_array[va_type].p_callbacks) {
                    multi_ble_adv_manager_remove_ble_adv(va_info_array[va_type].p_callbacks->on_get_ble_adv_data);
                }
                for (i = 0; i < MULTI_VA_TYPE_MAX_NUM; i++) {
                    if (va_type != i && va_info_array[i].p_callbacks) {
                        multi_ble_adv_manager_add_ble_adv(va_info_array[i].p_callbacks->on_get_ble_adv_data);
                    }
                }
                multi_ble_adv_manager_notify_ble_adv_data_changed();
            }
        }
    }
#else
    if (s_va_type != va_type) {
        APPS_LOG_MSGID_E(LOG_TAG"notify_va_connected, not support MULTI_VA_SUPPORT_COMPETITION, but va type not correct: %d", 1, va_type);
    } else {
        APPS_LOG_MSGID_I(LOG_TAG"notify_va_connected, not support MULTI_VA_SUPPORT_COMPETITION, va_type = %d", 1, va_type);
    }
#endif
    s_va_connected = true;
}

void multi_voice_assistant_manager_set_inactive_done(multi_va_type_t va_type)
{
    APPS_LOG_MSGID_I(LOG_TAG"set_inactive_done, va_type: %d, removing_va_type: %d, s_va_type: %d",
            3, va_type, removing_va_type, s_va_type);
    if (removing_va_type == va_type) {
        removing_va_type = MULTI_VA_TYPE_UNKNOWN;
#ifdef MULTI_VA_SUPPORT_COMPETITION
        // reboot both side
        multi_va_manager_reboot();
#endif
    } else if (s_va_type == va_type) {
#ifdef MULTI_VA_SUPPORT_COMPETITION
        size_t i;
        nvkey_status_t write_status;
        if (s_enable_adv) {
            for (i = 0; i < MULTI_VA_TYPE_MAX_NUM; i++) {
                if (va_info_array[i].p_callbacks) {
                    multi_ble_adv_manager_add_ble_adv(
                            va_info_array[i].p_callbacks->on_get_ble_adv_data);
                }
            }
            multi_ble_adv_manager_notify_ble_adv_data_changed();
        }
        apps_config_key_remapper_set_va_type(s_va_type, MULTI_VA_TYPE_UNKNOWN);
        s_va_type = MULTI_VA_TYPE_UNKNOWN;
        write_status = nvkey_write_data(NVKEYID_VA_SWITH, &s_va_type, sizeof(s_va_type));
        if (NVKEY_STATUS_OK != write_status) {
            APPS_LOG_MSGID_I(LOG_TAG"_receive_va_change_from_agent(), Write NVKEYID_VA_SWITH failed %d", 1, write_status);
        }
#if defined(MTK_AWS_MCE_ENABLE) && defined(MULTI_VA_SUPPORT_COMPETITION)
        multi_va_manager_send_va_type_to_partner();
#endif
#endif
        APPS_LOG_MSGID_I(LOG_TAG"set_inactive_done, wait for disconnected and set adv", 0);

    }
}

void multi_voice_assistant_manager_notify_va_disconnected(multi_va_type_t va_type)
{
    if (s_va_type == va_type) {
        s_va_connected = false;
        APPS_LOG_MSGID_I(LOG_TAG"va_disconnect, need trigger BLE adv", 1, va_type);
        // TO-DO set BLE adv
#ifdef MULTI_VA_SUPPORT_COMPETITION
        if (s_enable_adv) {
            size_t i;
            for (i = 0; i < MULTI_VA_TYPE_MAX_NUM; i++) {
                if (va_type != i && va_info_array[i].p_callbacks) {
                    multi_ble_adv_manager_remove_ble_adv(va_info_array[i].p_callbacks->on_get_ble_adv_data);
                }
            }
            if (va_info_array[s_va_type].p_callbacks) {
                multi_ble_adv_manager_add_ble_adv(va_info_array[s_va_type].p_callbacks->on_get_ble_adv_data);
            }
            multi_ble_adv_manager_notify_ble_adv_data_changed();
        }
#endif
    } else {
        APPS_LOG_MSGID_I(LOG_TAG"va_disconnect, but s_va_type(%d) != va_type(%d)",
                2, s_va_type, va_type);
    }
}

bool multi_voice_assistant_manager_enable_adv(bool enable)
{
    bool have_va_adv = false;
    APPS_LOG_MSGID_I(LOG_TAG"enable_adv %d -> %d, va_type: %x, connected : %d",
            4, s_enable_adv, enable, s_va_type, s_va_connected);
    if (!s_enable_adv && enable) {
#ifdef MULTI_VA_SUPPORT_COMPETITION
        if (s_va_connected) {
            size_t i;
            for (i = 0; i < MULTI_VA_TYPE_MAX_NUM; i++) {
                if (s_va_type != i && va_info_array[i].p_callbacks) {
                    multi_ble_adv_manager_add_ble_adv(va_info_array[i].p_callbacks->on_get_ble_adv_data);
                    have_va_adv = true;
                }
            }
        } else {
            size_t i;
            for (i = 0; i < MULTI_VA_TYPE_MAX_NUM; i++) {
                if ((s_va_type >= MULTI_VA_TYPE_MAX_NUM || s_va_type == i)
                        && va_info_array[i].p_callbacks) {
                    multi_ble_adv_manager_add_ble_adv(va_info_array[i].p_callbacks->on_get_ble_adv_data);
                    have_va_adv = true;
                }
            }
        }
#else
        if (s_va_type < MULTI_VA_TYPE_MAX_NUM) {
            if (va_info_array[s_va_type].p_callbacks) {
                multi_ble_adv_manager_add_ble_adv(va_info_array[s_va_type].p_callbacks->on_get_ble_adv_data);
                have_va_adv = true;
            }
        }
#endif
    } else if (s_enable_adv && !enable) {
#ifdef MULTI_VA_SUPPORT_COMPETITION
        if (s_va_connected) {
            size_t i;
            for (i = 0; i < MULTI_VA_TYPE_MAX_NUM; i++) {
                if (s_va_type != i && va_info_array[i].p_callbacks) {
                    multi_ble_adv_manager_remove_ble_adv(va_info_array[i].p_callbacks->on_get_ble_adv_data);
                    have_va_adv = true;
                }
            }
        } else {
            size_t i;
            for (i = 0; i < MULTI_VA_TYPE_MAX_NUM; i++) {
                if ((s_va_type >= MULTI_VA_TYPE_MAX_NUM || s_va_type == i)
                        && va_info_array[i].p_callbacks) {
                    multi_ble_adv_manager_remove_ble_adv(va_info_array[i].p_callbacks->on_get_ble_adv_data);
                    have_va_adv = true;
                }
            }
        }
#else
        if (s_va_type < MULTI_VA_TYPE_MAX_NUM) {
            if (va_info_array[s_va_type].p_callbacks) {
                multi_ble_adv_manager_remove_ble_adv(va_info_array[s_va_type].p_callbacks->on_get_ble_adv_data);
                have_va_adv = true;
            }
        }
#endif
    }
    s_enable_adv = enable;

    return have_va_adv;
}

void multi_voice_assistant_manager_va_config_changed(void) {
    uint8_t va_type = MULTI_VA_TYPE_UNKNOWN;
    uint32_t va_type_len = sizeof(s_va_type);
    nvkey_status_t read_status = nvkey_read_data(NVKEYID_VA_SWITH, &va_type, &va_type_len);
    if (NVKEY_STATUS_OK != read_status || va_type_len != sizeof(s_va_type)) {
        APPS_LOG_MSGID_E(LOG_TAG"_set_va_type(), Read NVKEYID_VA_SWITH failed %d", 1, read_status);
    }
    if (va_type != s_va_type) {
        multi_va_switch_off_return_t switch_off_result = MULTI_VA_SWITCH_OFF_SET_INACTIVE_DONE;

        APPS_LOG_MSGID_I(LOG_TAG"_set_va_type(), type = %d", 1, va_type);
        
#ifdef MULTI_VA_SUPPORT_COMPETITION
        if (s_va_type < MULTI_VA_TYPE_MAX_NUM) {
#ifdef MTK_AWS_MCE_ENABLE
            bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
            if (BT_AWS_MCE_ROLE_AGENT == role || BT_AWS_MCE_ROLE_NONE == role)
#endif
            {
                if (va_info_array[s_va_type].p_callbacks) {
                    switch_off_result =
                            va_info_array[s_va_type].p_callbacks->on_voice_assistant_type_switch(false);
                    APPS_LOG_MSGID_I(LOG_TAG"set_va_type(), switch off[%d] result : %d",
                            2, s_va_type, switch_off_result);
                }
                if (MULTI_VA_SWITCH_OFF_WAIT_INACTIVE == switch_off_result) {
                    removing_va_type = s_va_type;
                }
            }
        } else {
            APPS_LOG_MSGID_I(LOG_TAG"set_va_type(), old type is unknown", 0);
        }
#endif
        s_va_type = va_type;
        if (MULTI_VA_SWITCH_OFF_SET_INACTIVE_DONE == switch_off_result) {
            // TO-DO: reboot
            multi_va_manager_reboot();
        }
    } else {
        APPS_LOG_MSGID_W(LOG_TAG"_set_va_type(), va_type not change %d", 1, va_type);
    }
}
