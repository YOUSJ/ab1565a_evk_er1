# Copyright Statement:
#
# (C) 2017  Airoha Technology Corp. All rights reserved.
#
# This software/firmware and related documentation ("Airoha Software") are
# protected under relevant copyright laws. The information contained herein
# is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
# Without the prior written permission of Airoha and/or its licensors,
# any reproduction, modification, use or disclosure of Airoha Software,
# and information contained herein, in whole or in part, shall be strictly prohibited.
# You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
# if you have agreed to and been bound by the applicable license agreement with
# Airoha ("License Agreement") and been granted explicit permission to do so within
# the License Agreement ("Permitted User").  If you are not a Permitted User,
# please cease any access or use of Airoha Software immediately.
# BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
# THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
# ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
# WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
# NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
# SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
# SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
# THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
# THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
# CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
# SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
# STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
# CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
# AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
# OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
# AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
#

APPS_SRC = $(APP_PATH)/src/apps
APPS_INC = $(APP_PATH)/inc/apps

C_FILES += $(APPS_SRC)/apps_init.c
# Apps C files
#C_FILES += $(APPS_SRC)/app_battery/app_battery_low_capacity_activity.c
ifeq ($(MTK_FOTA_ENABLE),y)
C_FILES += $(APPS_SRC)/app_fota/app_fota_idle_activity.c
#C_FILES += $(APPS_SRC)/app_fota/app_fota_activity.c
endif

# Pre-proc activity
C_FILES += $(APPS_SRC)/app_preproc/app_preproc_activity.c
#C_FILES += $(APPS_SRC)/app_preproc/app_preproc_sys_pwr.c

# Battery app
C_FILES += $(APPS_SRC)/app_battery/app_battery_idle_activity.c
C_FILES += $(APPS_SRC)/app_battery/app_battery_transient_activity.c

ifeq ($(APPS_SLEEP_AFTER_NO_CONNECTION),y)
# Power saving
C_FILES += $(APPS_SRC)/app_power_saving/app_power_saving_idle_activity.c
C_FILES += $(APPS_SRC)/app_power_saving/app_power_saving_timeout_activity.c
C_FILES += $(APPS_SRC)/app_power_saving/app_power_saving_waiting_activity.c
C_FILES += $(APPS_SRC)/app_power_saving/app_power_saving_utils.c
endif

# multi va
C_FILES += $(APPS_SRC)/app_multi_va/app_multi_va_idle_activity.c
C_FILES += $(APPS_SRC)/app_multi_va/multi_va_manager.c
C_FILES += $(APPS_SRC)/app_multi_va/multi_ble_adv_manager.c

# Some utils app and home screen app
ifeq ($(SUPPORT_ROLE_HANDOVER_SERVICE),y)
C_FILES += $(APPS_SRC)/app_idle/app_rho_idle_activity.c
endif
C_FILES += $(APPS_SRC)/app_idle/app_home_screen_idle_activity.c
C_FILES += $(APPS_SRC)/app_idle/app_bt_conn_componet_in_homescreen.c

#in-ear part
C_FILES += $(APPS_SRC)/app_in_ear/app_in_ear_idle_activity.c
C_FILES += $(APPS_SRC)/app_in_ear/app_in_ear_utils.c

#music part
C_FILES += $(APPS_SRC)/app_music/app_music_idle_activity.c
C_FILES += $(APPS_SRC)/app_music/app_music_activity.c
C_FILES += $(APPS_SRC)/app_music/app_music_utils.c

#hfp part

C_FILES += $(APPS_SRC)/app_hfp/app_hfp_idle_activity.c
C_FILES += $(APPS_SRC)/app_hfp/app_hfp_utils.c
C_FILES += $(APPS_SRC)/app_hfp/app_hfp_activity.c

# FIND ME
C_FILES += $(APPS_SRC)/app_fm/app_fm_idle_activity.c
C_FILES += $(APPS_SRC)/app_fm/app_fm_activity.c

# App state report
C_FILES += $(APPS_SRC)/app_state_report/apps_state_report.c

# Events folder
C_FILES += $(APPS_SRC)/events/apps_events_key_event.c
C_FILES += $(APPS_SRC)/events/apps_events_battery_event.c
C_FILES += $(APPS_SRC)/events/apps_events_bt_event.c
C_FILES += $(APPS_SRC)/events/apps_events_audio_event.c

# Config folder
C_FILES += $(APPS_SRC)/config/apps_config_key_remapper.c
C_FILES += $(APPS_SRC)/config/apps_config_led_manager.c
C_FILES += $(APPS_SRC)/config/apps_config_vp_manager.c
C_FILES += $(APPS_SRC)/config/apps_config_features_dynamic_setting.c

# LED
C_FILES += $(APPS_SRC)/led/app_led_control.c
C_FILES += $(APPS_SRC)/led/app_led_internal.c
C_FILES += $(APPS_SRC)/led/led_control_style_cfg.c

# FAST_PAIR
ifeq ($(MTK_BT_FAST_PAIR_ENABLE),y)
C_FILES += $(APPS_SRC)/app_fast_pair/app_fast_pair_idle_activity.c
endif

# SMART_CHARGER
ifeq ($(MTK_SMART_CHARGER_ENABLE),y)
C_FILES += $(APPS_SRC)/app_smart_charger/app_smcharger_idle_activity.c
C_FILES += $(APPS_SRC)/app_smart_charger/app_smcharger_lid_close_activity.c
C_FILES += $(APPS_SRC)/app_smart_charger/app_smcharger_lid_open_activity.c
C_FILES += $(APPS_SRC)/app_smart_charger/app_smcharger_off_activity.c
C_FILES += $(APPS_SRC)/app_smart_charger/app_smcharger_out_of_case_activity.c
C_FILES += $(APPS_SRC)/app_smart_charger/app_smcharger_startup_activity.c
C_FILES += $(APPS_SRC)/app_smart_charger/app_smcharger_utils.c
endif

ifeq ($(GSOUND_LIBRARY_ENABLE),y)
C_FILES += $(APPS_SRC)/app_gsound/app_gsound_idle_activity.c
endif


# Utils
C_FILES += $(APPS_SRC)/utils/apps_debug.c
ifeq ($(MTK_AWS_MCE_ENABLE),y)
C_FILES += $(APPS_SRC)/utils/apps_aws_sync_event.c
endif

#voice_prompt
ifeq ($(MTK_PROMPT_SOUND_ENABLE),y)
C_FILES +=   $(APPS_SRC)/vp/app_voice_prompt.c
C_FILES +=   $(APPS_SRC)/vp/app_voice_prompt_nvdm.c
endif

#ama
ifeq ($(MTK_AMA_DEVELOPMENT),y)
C_FILES +=   $(APPS_SRC)/app_ama/app_ama_idle_activity.c
C_FILES +=   $(APPS_SRC)/app_ama/app_ama_activity.c
C_FILES +=   $(APPS_SRC)/app_ama/app_ama_audio.c
C_FILES +=   $(APPS_SRC)/app_ama/app_ama_multi_va.c
CFLAGS  += -DMTK_AMA_ENABLE

# Add AMA WWD support
ifeq ($(AMA_WWD_ENABLE),y)
CFLAGS  += -DAMA_WWD_ENABLE
endif
endif

# Add xiaowei VA activity support
ifeq ($(MTK_VA_XIAOWEI_ENABLE), y)
C_FILES += $(APPS_SRC)/app_va_xiaowei/app_va_xiaowei_activity.c
C_FILES += $(APPS_SRC)/app_va_xiaowei/app_va_xiaowei_multi_support.c
C_FILES += $(APPS_SRC)/app_va_xiaowei/app_va_xiaowei_transient_activity.c
endif

# Add xiaoai VA activity support
ifeq ($(MTK_VA_XIAOAI_ENABLE), y)
C_FILES += $(APPS_SRC)/app_va_xiaoai/app_va_xiaoai_activity.c
C_FILES += $(APPS_SRC)/app_va_xiaoai/app_va_xiaoai_ble_adv.c
endif

# leakage detection
ifeq ($(MTK_LEAKAGE_DETECTION_ENABLE), y)
C_FILES += $(APPS_SRC)/app_leakage_detection/app_leakage_detection_idle_activity.c
endif

# Include bt sink path
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_battery
ifeq ($(MTK_FOTA_ENABLE),y)
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_fota
endif
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_gsound
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_hfp
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_idle
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_multi_va
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/config
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/events
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/utils
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_music
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/led
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_power_saving
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_preproc
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_fm
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_fast_pair
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/vp
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_smart_charger
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_ama
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_in_ear
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_leakage_detection

# Add xiaowei VA activity support
ifeq ($(MTK_VA_XIAOWEI_ENABLE), y)
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_va_xiaowei
CFLAGS += -I$(SOURCE_DIR)/middleware/MTK/xiaowei/inc
endif

# Add xiaoai VA activity support
ifeq ($(MTK_VA_XIAOAI_ENABLE), y)
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_va_xiaoai
CFLAGS += -I$(SOURCE_DIR)/middleware/MTK/xiaoai/inc
endif
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_state_report

#APPs features

##
## APPS_DISABLE_BT_WHEN_CHARGING
## Brief:       This option is used to enable the feature that disabling BT if device is charging.
## Usage:       Enable the feature by configuring it as y.
## Path:        project
## Dependency:  None
## Notice:      Do not enable the feature if CCASE_ENABLE is y
## Relative doc:None
##
ifeq ($(APPS_DISABLE_BT_WHEN_CHARGING), y)
CFLAGS += -DAPPS_DISABLE_BT_WHEN_CHARGING
endif

##
## APPS_AUTO_TRIGGER_RHO
## Brief:       This option is used to enable the feature that triggers RHO before power off, disable BT and battery level of agent is lower than partner.
## Usage:       Enable the feature by configuring it as y.
## Path:        project
## Dependency:  SUPPORT_ROLE_HANDOVER_SERVICE
## Notice:      Do not enable APPS_TRIGGER_RHO_BY_KEY and APPS_AUTO_TRIGGER_RHO in one project
## Relative doc:None
##
ifeq ($(APPS_AUTO_TRIGGER_RHO), y)
# Do RHO when agent low battery, power off or disable BT
CFLAGS += -DAPPS_AUTO_TRIGGER_RHO
endif

##
## APPS_TRIGGER_RHO_BY_KEY
## Brief:       This option is used to enable the feature trigger RHO user presses key.
## Usage:       Enable the feature by configuring it as y.
## Path:        project
## Dependency:  SUPPORT_ROLE_HANDOVER_SERVICE
## Notice:      Do not enable APPS_TRIGGER_RHO_BY_KEY and APPS_AUTO_TRIGGER_RHO in one project
## Relative doc:None
##
ifeq ($(APPS_TRIGGER_RHO_BY_KEY), y)
# Do RHO press key on partner
CFLAGS += -DAPPS_TRIGGER_RHO_BY_KEY
endif

##
## APPS_SLEEP_AFTER_NO_CONNECTION
## Brief:       This option is used to enable the feature start a timer to sleep when device doesn't connect to smart phone.
## Usage:       Enable the feature by configuring it as y.
## Path:        project
## Dependency:  None
## Notice:      None
## Relative doc:None
##
ifeq ($(APPS_SLEEP_AFTER_NO_CONNECTION), y)
# Press key to do air pairing
CFLAGS += -DAPPS_SLEEP_AFTER_NO_CONNECTION
endif

# app gsound
ifeq ($(GSOUND_LIBRARY_ENABLE), y)
CFLAGS += -I$(SOURCE_DIR)/$(APPS_INC)/app_gsound
C_FILES += $(APPS_SRC)/app_gsound/app_gsound_service.c
C_FILES += $(APPS_SRC)/app_gsound/app_gsound_multi_va.c
endif

# Multi Voice assistant
ifeq ($(MULTI_VA_SUPPORT_COMPETITION), y)
CFLAGS += -DMULTI_VA_SUPPORT_COMPETITION
endif

ifeq ($(MTK_IN_EAR_FEATURE_ENABLE), y)
CFLAGS += -DMTK_IN_EAR_FEATURE_ENABLE
endif

ifeq ($(MTK_LEAKAGE_DETECTION_ENABLE), y)
CFLAGS += -DMTK_LEAKAGE_DETECTION_ENABLE
endif
