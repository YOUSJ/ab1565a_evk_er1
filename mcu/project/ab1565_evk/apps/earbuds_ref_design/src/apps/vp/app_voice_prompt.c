/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */


#ifdef MTK_PROMPT_SOUND_ENABLE
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "hal.h"
#include "hal_audio_internal.h"
#include "bt_connection_manager.h"
#include "bt_sink_srv_utils.h"

#include "app_voice_prompt.h"
#include "app_voice_prompt_nvdm.h"
#include "prompt_control.h"
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
#include "bt_role_handover.h"
#endif
#include "bt_connection_manager_internal.h"
#include "bt_connection_manager.h"
#include "bt_gap.h"
#include "rofs.h"
#ifdef MTK_AWS_MCE_ENABLE
#include "bt_aws_mce_report.h"
#include "bt_aws_mce_srv.h"
#ifdef __CONN_VP_SYNC_STYLE_ONE__
#include "apps_config_vp_index_list.h"
#endif
#endif
#include "bt_callback_manager.h"
#include "bt_device_manager.h"

typedef struct {
    /* for vp */
    vp_queue *vp_list_low;
    vp_queue *vp_list_medium;
    vp_queue *vp_list_high;
    vp_queue *vp_list_extrem;

    /* for voice boardcast and ringtone */
    vp_queue *vp_list_ultra;

    SemaphoreHandle_t vp_semaphore_handle;

#ifdef MTK_AWS_MCE_ENABLE
    /* keep tick when play a tone, it need will be used compensate time delay in peer device*/
    TickType_t tickAtPlay;
#endif

    /* a id for stop procedure */
    uint16_t count;

    /* if flag set, will empry all vp who are queuing and reject any vp from now on, this flag should be set by power off vp only*/
    bool isCleanup;

    /*a falg for waiting for confirm after stop a tone*/
    bool isToneStopping;

    /* keep current bt state, sniff, exiting sniff and normal */
    app_vp_bt_state bt_state;

    /* a flag to keep init vp status */
    bool isInit;

    /* a flag to indicate that vp is playing*/
    bool isPlaying;

    /* keep delay from caller*/
    uint32_t voiceDelay;
} app_voice_prompt_context_t;
static volatile app_voice_prompt_context_t vp_ctx={0};
extern bool g_app_voice_prompt_test_off;

log_create_module(VOICE_PROMPT_APP, PRINT_LEVEL_INFO);

/********************************************************
 * Function
 *
 ********************************************************/
static void app_voice_prompt_init();
static void app_voice_prompt_callback(prompt_control_event_t event_id);
static vp_queue *app_voice_prompt_get_queue_by_prio(app_vp_prio_t level);
static void app_voice_prompt_set_queue_by_prio(app_vp_prio_t level, vp_queue * queue);
static vp_queue *app_voice_prompt_get_current_queue();
static void app_voice_prompt_stop_and_play_next();
static app_voice_prompt_list_status_t app_voice_prompt_delete_item_by_id(uint16_t id);
static app_voice_prompt_list_status_t app_voice_prompt_get_item_by_id(uint16_t id, DataType *item);
static app_voice_prompt_list_status_t app_voice_prompt_delete_all_item_excp_curr(vp_queue * queue);
#ifdef MTK_AWS_MCE_ENABLE
static bool app_voice_prompt_aws_play(uint32_t tone_idx, uint32_t delay_time, app_vp_type type, app_vp_prio_t level, bool isCleanup, bool repeat, app_vp_play_callback_t callback);
#endif
char* app_voice_prompt_get_type_str(app_vp_prio_t level);
#ifdef MTK_AWS_MCE_ENABLE
static void app_voice_prompt_sync_callback(bt_aws_mce_report_info_t *info);

static bt_status_t app_voice_prompt_get_SP_bd_addr(bt_bd_addr_t *addr);
static bt_status_t app_voice_gap_evt_callback(bt_msg_type_t msg, bt_status_t status, void *buffer);
static void app_voice_exit_sniff_cnf(bt_status_t status, bt_gap_sniff_mode_changed_ind_t* ind);
static void app_voice_enable_sniff_cnf(bt_status_t status, bt_gap_write_link_policy_cnf_t *cnf);
static bt_status_t app_voice_prompt_enable_sniff(bool enable);
#endif

#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
static void app_voice_prompt_rho_cb(const bt_bd_addr_t *addr,bt_aws_mce_role_t role,bt_role_handover_event_t event,bt_status_t status);
#endif
static bool app_voice_prompt_stop_internal(uint16_t id, bool need_sync, uint32_t delay_time, bool preempted);
static bool app_voice_prompt_voice_stop_internal(bool need_sync, uint32_t delay_time, bool isRingtone, bool preempted);
static inline void app_voice_prompt_noti(app_vp_play_callback_t callback, uint32_t index, vp_err_code err);

static void app_vp_mutex_create(void)
{
    if ( vp_ctx.vp_semaphore_handle == NULL ) {
        vSemaphoreCreateBinary(vp_ctx.vp_semaphore_handle); /*In New FreeRTOS version It would assert if Mutex take & give are different task.*/
    }
}

static void app_vp_mutex_lock(void)
{
    if ( vp_ctx.vp_semaphore_handle != NULL ) {
        xSemaphoreTake(vp_ctx.vp_semaphore_handle, portMAX_DELAY);
    }
}

static void app_vp_mutex_unlock(void)
{
    if ( vp_ctx.vp_semaphore_handle != NULL ) {
        xSemaphoreGive(vp_ctx.vp_semaphore_handle);
    }
}

static vp_queue *app_voice_prompt_create_queue()
{
    vp_queue * q = (vp_queue *)bt_sink_srv_memory_alloc(sizeof(vp_queue));
    if(q==NULL){
        app_voice_prompt_msgid_error("create vp queue fail-memory malloc fail!.", 0);
        return NULL;
    }
    q->front = NULL;
    q->rear = NULL;
    q->length = 0;
    return q;
}

static app_voice_prompt_list_status_t app_voice_prompt_insert_item(vp_queue * q, DataType *item)
{
    if (q->length >= APP_VOICE_PROMPT_QUEUE_MAX_LEN) {
        app_voice_prompt_report("app_voice_prompt VP queue full", 0);
        return VOICE_PROMPT_LIST_FAIL;
    }

    vp_node *node = (vp_node *)pvPortMalloc(sizeof(vp_node));
    app_voice_prompt_report("insert 0x%08x, current queue length %d", node, q->length);
    if(node==NULL){
        app_voice_prompt_msgid_error("add vp queue node fail-memory malloc fail!", 0);
        return VOICE_PROMPT_LIST_FAIL;
    }
    memcpy(&(node->data), item, sizeof(DataType));
    node->next = NULL;
    if(q->front == NULL){
        q->front = node;
    }

    if(q->rear == NULL){
        q->rear = node;
    }else{
        q->rear->next = node;
        q->rear = node;
    }

    q->length ++;

    return VOICE_PROMPT_LIST_SUCCESS;
}

static app_voice_prompt_list_status_t app_voice_prompt_delete_item(vp_queue * q)
{
    vp_node *node;
    if(q==NULL ||  q->front ==NULL)
    {
        return VOICE_PROMPT_LIST_FAIL;
    }else{
        node = q->front;
        if(q->front == q->rear){
            q->front = NULL;
            q->rear = NULL;
        }else{
            q->front = q->front->next;
        }
        app_voice_prompt_report("delete 0x%08x", node);
        bt_sink_srv_memory_free(node);
        q->length --;
    }
    return VOICE_PROMPT_LIST_SUCCESS;
}

static app_voice_prompt_list_status_t app_voice_prompt_delete_all_item(vp_queue * q)
{
    vp_node *curr = NULL, *temp;

    if(q == NULL || q->front == NULL){
        return VOICE_PROMPT_LIST_FAIL;
    }

    curr = q->front;
    while(curr){
        temp = curr;
        curr = curr->next;
        app_voice_prompt_report("delete 0x%08x", temp);
        bt_sink_srv_memory_free(temp);
    }
    q->front = NULL;
    q->rear = q->front;
    q->length = 0;

    return VOICE_PROMPT_LIST_SUCCESS;
}

static app_voice_prompt_list_status_t app_voice_prompt_delete_all_item_excp_curr(vp_queue * q)
{
    vp_node *curr = NULL, *temp;

    if(q == NULL || q->front == NULL){
        return VOICE_PROMPT_LIST_FAIL;
    }

    if(q->front == q->rear){
        return VOICE_PROMPT_LIST_SUCCESS;
    }

    curr = q->front->next;
    while(curr){
        temp = curr;
        curr = curr->next;
        app_voice_prompt_report("delete 0x%08x", temp);
        bt_sink_srv_memory_free(temp);
    }
    q->rear = q->front;
    q->length = 1;

    return VOICE_PROMPT_LIST_SUCCESS;

}

static app_voice_prompt_list_status_t app_voice_prompt_delete_item_by_id(uint16_t id)
{
    vp_queue *queue = NULL;
    vp_node *curr = NULL, *prev = NULL;
    bool found = false;

    for(int i=VOICE_PROMPT_PRIO_EXTREME; i >= VOICE_PROMPT_PRIO_LOW; i--){
        queue = app_voice_prompt_get_queue_by_prio(i);

        //empty queue
        if(queue == NULL || queue->front == NULL){
            continue;
        }else{
            curr = queue->front;
            while(curr){
                if(curr->data.id == id){
                    if(curr == queue->rear && curr == queue->front){//just one vp queuing,
                        queue->front = NULL;
                        queue->rear = NULL;
                    }else{
                        if(prev != NULL){//for Coverity warning only
                            prev->next = curr->next;
                            if(curr == queue->rear){//last one will be remove. update rear
                                queue->rear = prev;
                            }
                        }else{
                            configASSERT(0);
                        }
                    }
                    app_voice_prompt_report("delete 0x%08x", curr);
                    bt_sink_srv_memory_free(curr);
                    queue->length --;
                    found = true;
                    break;
                }
                prev = curr;
                curr = curr->next;
            }
        }

        if(found){
            break;
        }
    }

    return (found ? VOICE_PROMPT_LIST_SUCCESS : VOICE_PROMPT_LIST_FAIL);
}

static app_voice_prompt_list_status_t app_voice_prompt_get_item_by_id(uint16_t id, DataType *item)
{
    vp_queue *queue = NULL;
    vp_node *curr = NULL;
    bool found = false;

    for(int i=VOICE_PROMPT_PRIO_EXTREME; i >= VOICE_PROMPT_PRIO_LOW; i--){
        queue = app_voice_prompt_get_queue_by_prio(i);

        //empty queue
        if(queue == NULL || queue->front == NULL){
            continue;
        }else{
            curr = queue->front;
            while(curr){
                if(curr->data.id == id){
                    memcpy(item, &(curr->data), sizeof(DataType));
                    found = true;
                    break;
                }
                curr = curr->next;
            }
        }

        if(found){
            break;
        }
    }

    return (found ? VOICE_PROMPT_LIST_SUCCESS : VOICE_PROMPT_LIST_FAIL);
}

static app_voice_prompt_list_status_t app_voice_prompt_copy_item(vp_queue * q, DataType *item)
{
    if(q==NULL ||  q->front ==NULL)
    {
        item=NULL;
        return VOICE_PROMPT_LIST_FAIL;
    }else{
        memcpy(item, &(q->front->data), sizeof(DataType));
    }
    return VOICE_PROMPT_LIST_SUCCESS;
}

static void app_voice_prompt_set_queue_by_prio(app_vp_prio_t level, vp_queue * queue)
{
    switch(level){

    case VOICE_PROMPT_PRIO_LOW:
        vp_ctx.vp_list_low = queue;
        break;

    case VOICE_PROMPT_PRIO_MEDIUM:
        vp_ctx.vp_list_medium = queue;
        break;

    case VOICE_PROMPT_PRIO_HIGH:
        vp_ctx.vp_list_high = queue;
        break;

    case VOICE_PROMPT_PRIO_ULTRA:
        vp_ctx.vp_list_ultra = queue;
        break;

    case VOICE_PROMPT_PRIO_EXTREME:
        vp_ctx.vp_list_extrem = queue;
        break;

    default:
        return ;
    }

}

static vp_queue *app_voice_prompt_get_queue_by_prio(app_vp_prio_t level){
    switch(level){

    case VOICE_PROMPT_PRIO_LOW:
        return vp_ctx.vp_list_low;

    case VOICE_PROMPT_PRIO_MEDIUM:
        return vp_ctx.vp_list_medium;

    case VOICE_PROMPT_PRIO_HIGH:
        return vp_ctx.vp_list_high;

    case VOICE_PROMPT_PRIO_ULTRA:
        return vp_ctx.vp_list_ultra;

    case VOICE_PROMPT_PRIO_EXTREME:
        return vp_ctx.vp_list_extrem;

    default:
        return NULL;
    }
}

char* app_voice_prompt_get_type_str(app_vp_prio_t level)
{
    switch(level){

    case VP_TYPE_RT:
        return "rt";

    case VP_TYPE_VB:
        return "vb";

    case VP_TYPE_VP:
        return "vp";

    default:
        return NULL;
    }

}

static vp_queue *app_voice_prompt_get_current_queue()
{
    vp_queue *queue = NULL;

    for(int i=VOICE_PROMPT_PRIO_EXTREME; i >= VOICE_PROMPT_PRIO_LOW; i--){
        queue = app_voice_prompt_get_queue_by_prio(i);

        //empty queue
        if(queue == NULL || queue->front == NULL){
            continue;
        }else{
            break;
        }
    }

    return queue;
}

#ifdef MTK_AWS_MCE_ENABLE
static bt_status_t app_voice_prompt_get_SP_bd_addr(bt_bd_addr_t *addr)
{
    bt_bd_addr_t addr_list[2];
    uint32_t count = 0;
    uint32_t i;

    count = bt_cm_get_connected_devices(BT_CM_PROFILE_SERVICE_NONE, addr_list, sizeof(addr_list) / sizeof(bt_bd_addr_t));
    
    for (i = 0; i < count; i++) {
        if (memcmp(addr_list[i], bt_device_manager_aws_local_info_get_peer_address(), sizeof(bt_bd_addr_t) != 0)) {
            memcpy(*addr, addr_list[i], sizeof(bt_bd_addr_t));
            return BT_STATUS_SUCCESS;
        }
    }

    app_voice_prompt_msgid_error("Error! cann't get sp addr,",0);
    return BT_STATUS_FAIL;
}

static bt_status_t app_voice_prompt_enable_sniff(bool enable)
{
    bt_bd_addr_t addr={0};
    bt_gap_connection_handle_t conn_handle = NULL;
    bt_status_t ret = BT_STATUS_SUCCESS;

    app_voice_prompt_get_SP_bd_addr(&addr);
    conn_handle = bt_cm_get_gap_handle(addr);

    if (0 != conn_handle) {
        bt_gap_link_policy_setting_t setting;
        if (enable) {
            setting.sniff_mode = BT_GAP_LINK_POLICY_ENABLE;
        } else {
            setting.sniff_mode = BT_GAP_LINK_POLICY_DISABLE;
        }
        ret = bt_gap_write_link_policy(conn_handle, &setting);
    } else {
        ret = BT_STATUS_FAIL;
    }

    if(ret != BT_STATUS_SUCCESS){
        app_voice_prompt_report("handle 0x%x, %s bt sniff fail. ret: 0x%08x", conn_handle, enable ? "enable" : "disable", ret);
    }else{
        app_voice_prompt_report("handle 0x%x, %s bt sniff success.", conn_handle, enable ? "enable" : "disable");
    }
    return ret;
}

static void app_voice_exit_sniff_cnf(bt_status_t status, bt_gap_sniff_mode_changed_ind_t* ind)
{
    app_voice_prompt_list_data_t item={0};

    app_vp_mutex_lock();

    app_voice_prompt_copy_item(app_voice_prompt_get_current_queue(), (DataType *)&item);
    if(ind == NULL || status != BT_STATUS_SUCCESS){
        /* exit sniff fail, treat as sniff, play anyway */
        vp_ctx.bt_state = VP_BT_STATE_SNIFF;
    }else{
        if(ind->sniff_status == BT_GAP_LINK_SNIFF_TYPE_ACTIVE){
            vp_ctx.bt_state = VP_BT_STATE_ACTIVE;
            app_voice_prompt_enable_sniff(false); //disable sniff mode while vp playing
        }else{
            vp_ctx.bt_state = VP_BT_STATE_SNIFF;
        }
    }
    if(item.id != 0 && item.need_sync){
        app_voice_prompt_aws_play(item.index, item.delay_time, item.type, item.level, vp_ctx.isCleanup, item.repeat, item.callback);
    }

    app_vp_mutex_unlock();
}

static void app_voice_enable_sniff_cnf(bt_status_t status, bt_gap_write_link_policy_cnf_t *cnf)
{
    app_voice_prompt_msgid_report("sniff status change cnf, status: 0x%x, sniff mode: 0x%x", 2, status, cnf->sniff_mode);
}

static bt_status_t app_voice_gap_evt_callback(bt_msg_type_t msg, bt_status_t status, void *buffer)
{
    switch(msg){
        case BT_GAP_SNIFF_MODE_CHANGE_IND:
            {
                bt_gap_sniff_mode_changed_ind_t* ind = (bt_gap_sniff_mode_changed_ind_t *)buffer;
                app_voice_prompt_msgid_report("sniff mode change ind status: 0x%x, bt_state: %d, sniff status: %d", 3, status, vp_ctx.bt_state, ind ? ind->sniff_status:0xff);
                if(vp_ctx.bt_state == VP_BT_STATE_SNIFF_EXITING){
                    app_voice_exit_sniff_cnf(status, ind);
                }
            }
            break;

        case BT_GAP_WRITE_LINK_POLICY_CNF:
            {
                app_voice_enable_sniff_cnf(status, (bt_gap_write_link_policy_cnf_t *)buffer);
            }
            break;

        default:
            ;
    }

    return BT_STATUS_SUCCESS;
}
#endif

#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
static void app_voice_prompt_rho_cb(const bt_bd_addr_t *addr,bt_aws_mce_role_t role,bt_role_handover_event_t event,bt_status_t status)
{
    app_voice_prompt_msgid_report("voice prompt rho, role: 0x%x, state: %d, playing: %d", 3, role, vp_ctx.bt_state, vp_ctx.isPlaying);

    if (BT_ROLE_HANDOVER_COMPLETE_IND == event && status == BT_STATUS_SUCCESS && vp_ctx.isPlaying){
        /* rho will update sniff status to partner, so just update bt_status */
        if(role == BT_AWS_MCE_ROLE_AGENT){
            vp_ctx.bt_state = VP_BT_STATE_SNIFF;
        }else if(role == BT_AWS_MCE_ROLE_PARTNER){
            vp_ctx.bt_state = VP_BT_STATE_ACTIVE;
        }
    }
}
#endif

static bool app_voice_prompt_toneIdx2FileId(uint32_t tone_idx, uint16_t *fileId)
{
    return app_voice_prompt_VpId2FileId(tone_idx, fileId);
}

static void app_voice_prompt_init()
{
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
    bt_role_handover_callbacks_t role_callbacks = {NULL,NULL,NULL,NULL,app_voice_prompt_rho_cb};
#endif

    if(vp_ctx.isInit){
        return;
    }
    vp_ctx.isInit = true;
    vp_ctx.bt_state = VP_BT_STATE_SNIFF;
    app_voice_prompt_VpInit();
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
    bt_role_handover_register_callbacks(BT_ROLE_HANDOVER_MODULE_VP_APP, &role_callbacks);
#endif
#ifdef MTK_AWS_MCE_ENABLE
    bt_aws_mce_report_register_callback(BT_AWS_MCE_REPORT_MODULE_VP, app_voice_prompt_sync_callback);
    bt_callback_manager_register_callback(bt_callback_type_app_event, MODULE_MASK_GAP, (void *)app_voice_gap_evt_callback);
#endif
}

static bool app_voice_prompt_local_play(uint32_t tone_idx, uint32_t sync_time, app_vp_play_callback_t callback)
{
    uint8_t *tone_buf = NULL;
    uint32_t tone_size = 0;
    uint16_t file_id = 0;
    bool ret = true;
    ROFS_FILEINFO_T *pMediaFile;
    TickType_t currTick, compensate;

#ifdef MTK_AWS_MCE_ENABLE
    currTick = xTaskGetTickCount();
    if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER && sync_time > 0) {
        /* tick overflow*/
        if(vp_ctx.tickAtPlay > currTick){
            compensate = portMAX_DELAY - vp_ctx.tickAtPlay + currTick;
        }else{
            compensate = currTick - vp_ctx.tickAtPlay;
        }
        compensate = compensate * portTICK_PERIOD_MS;
        app_voice_prompt_msgid_report("voice prompt sync time compensate: 0x%x", 1, compensate);
        if(sync_time > compensate){
            sync_time -= compensate;
        }
    }
#endif
    app_voice_prompt_init();
    app_voice_prompt_msgid_report("voice prompt play index: 0x%x, synctime: 0x%x", 2, tone_idx, sync_time);
#ifndef MTK_AUDIO_AT_CMD_PROMPT_SOUND_ENABLE
    if(app_voice_prompt_toneIdx2FileId(tone_idx, &file_id)){
        pMediaFile=ROFS_fopen((unsigned short)file_id);

        if(pMediaFile){
            tone_buf =(uint8_t *)ROFS_address(pMediaFile);
            tone_size =(uint32_t) pMediaFile->BasicInfo.ulFileSize;
            app_voice_prompt_report("voice prompt: fn: %s, file id: %d, buf: 0x%08x, size: 0x%x", pMediaFile->szFileName, file_id, tone_buf, tone_size);
        }else{
            app_voice_prompt_msgid_report("voice prompt: file non found with file id: 0x%x", 1, file_id);
            ret = false;
        }
    }else{
        app_voice_prompt_msgid_report("voice prompt: get file id fail with tone id: 0x%x", 1, tone_idx);
        ret = false;
    }

    /* app_voice_prompt_callback(PROMPT_CONTROL_MEDIA_END);*/
    if(tone_size != 0 && tone_buf != NULL){
        app_voice_prompt_msgid_report("prompt_control_play_sync_tone +", 0);
#ifndef MTK_MP3_TASK_DEDICATE
        prompt_control_play_tone(VPC_MP3, tone_buf, tone_size, app_voice_prompt_callback);
#else
        prompt_control_play_sync_tone(VPC_MP3, tone_buf, tone_size, sync_time,app_voice_prompt_callback);
#endif
        app_voice_prompt_msgid_report("prompt_control_play_sync_tone -", 0);
    }
#endif
    vp_ctx.isPlaying = true;

    if(!ret){/* current play fail, remove and play next */
        app_voice_prompt_list_data_t item={0};
        app_voice_prompt_noti(callback, tone_idx, VP_ERR_CODE_FILE_NO_FOUND);
        app_voice_prompt_delete_item(app_voice_prompt_get_current_queue());
        app_voice_prompt_copy_item(app_voice_prompt_get_current_queue(), (DataType *)&item);
        if(item.id != 0){
            if(item.need_sync){
                ret = app_voice_prompt_aws_play(item.index, item.delay_time, item.type, item.level, vp_ctx.isCleanup, item.repeat, item.callback);
            }else{
                ret = app_voice_prompt_local_play(item.index, item.delay_time, item.callback);
            }
        }
    }

    app_voice_prompt_msgid_report("voice prompt play result: %d", 1, ret);

    return ret;
}

static bool app_voice_prompt_local_stop(uint32_t id, uint32_t sync_time, app_vp_type type, bool preempted)
{
    app_voice_prompt_list_data_t item={0};
    app_voice_prompt_list_status_t ret=VOICE_PROMPT_LIST_SUCCESS;

    app_voice_prompt_msgid_report("voice prompt local stop, id: 0x%d, type: 0x%x", 2, id, type);

    app_voice_prompt_copy_item(app_voice_prompt_get_current_queue(), &item);
    if(item.id == 0){
        return false;
    }

    if(type == VP_TYPE_VP){
        if(item.id == id){/* stopping current VP and play next one */
            ret = app_voice_prompt_delete_item(app_voice_prompt_get_queue_by_prio(item.level));
            if(ret == VOICE_PROMPT_LIST_SUCCESS){
                app_voice_prompt_stop_and_play_next();
            }
            app_voice_prompt_noti(item.callback, item.index, preempted ? VP_ERR_CODE_PREEMPTED : VP_ERR_CODE_STOP);
        }else{/* stopping vp is queuing, just remove form queue */
            app_voice_prompt_get_item_by_id(id, &item);
            ret = app_voice_prompt_delete_item_by_id(id);
            if(ret == VOICE_PROMPT_LIST_SUCCESS){
                app_voice_prompt_noti(item.callback, item.index, preempted ? VP_ERR_CODE_PREEMPTED : VP_ERR_CODE_STOP);
            }
        }
    }else if(type == VP_TYPE_VB || type == VP_TYPE_RT){
        switch(item.type){
            case VP_TYPE_RT:
                if(type == VP_TYPE_RT){/* current is ringtone and want to stop it, so just stop it and play next */
                    app_voice_prompt_msgid_report("stop curr ringtone", 0);
                    ret = app_voice_prompt_delete_item(app_voice_prompt_get_queue_by_prio(VOICE_PROMPT_PRIO_ULTRA));
                    if(ret == VOICE_PROMPT_LIST_SUCCESS){
                        app_voice_prompt_stop_and_play_next();
                    }
                    app_voice_prompt_noti(item.callback, item.index, preempted ? VP_ERR_CODE_PREEMPTED : VP_ERR_CODE_STOP);
                }else{/* current is ringtone but want to stop voice boardcast, that's anormal, stop fail */
                    app_voice_prompt_msgid_error("ringtone playing, stop ringtone only!", 0);
                }

                break;
            case VP_TYPE_VB:
                /* stop voice boardcast/ringtone and play next */
                if(type == VP_TYPE_VB){
                    app_voice_prompt_msgid_report("stop all voice boardcast", 0);
                    ret = app_voice_prompt_delete_all_item(app_voice_prompt_get_queue_by_prio(VOICE_PROMPT_PRIO_ULTRA));
                    if(ret == VOICE_PROMPT_LIST_SUCCESS){
                        app_voice_prompt_stop_and_play_next();
                    }
                    app_voice_prompt_noti(item.callback, item.index,  preempted ? VP_ERR_CODE_PREEMPTED : VP_ERR_CODE_STOP);
                }else{
                    app_voice_prompt_msgid_error("vc playing, stop vc only!", 0);
                }
                break;

            case VP_TYPE_VP:
                /* just remove all ringtone and voice boardcast. */
                app_voice_prompt_msgid_report("higher vp is playing idx: 0x%x, remove vc and ringtone quite", 1, item.index);
                ret = app_voice_prompt_delete_all_item(app_voice_prompt_get_queue_by_prio(VOICE_PROMPT_PRIO_ULTRA));
                app_voice_prompt_noti(item.callback, item.index,  preempted ? VP_ERR_CODE_PREEMPTED : VP_ERR_CODE_STOP);
                break;

            default:
                break;

        }
    }

    if(ret == VOICE_PROMPT_LIST_SUCCESS){
        app_voice_prompt_msgid_report("voice prompt remove success", 0);
    }else{
        app_voice_prompt_msgid_report("voice prompt remove fail", 0);
    }
    return (ret == VOICE_PROMPT_LIST_SUCCESS) ? true : false;
}

#ifdef MTK_AWS_MCE_ENABLE
static bt_status_t app_voice_prompt_exit_sniff_mode(bt_bd_addr_t remote_addr)
{
    bt_status_t status;
    bt_gap_connection_handle_t bt_handle = bt_cm_get_gap_handle(remote_addr);

    status = bt_gap_exit_sniff_mode(bt_handle);
    app_voice_prompt_msgid_report("[BT_CM][E] Exit sniff mode fail status %x !!!", 1, status);
    return status;
}

static bool app_voice_prompt_aws_play(uint32_t tone_idx, uint32_t delay_time, app_vp_type type, app_vp_prio_t level, bool isCleanup, bool repeat, app_vp_play_callback_t callback)
{
    //bt_bd_addr_t *device_p = NULL;
    //device_p = bt_sink_srv_cm_get_aws_connected_device();
    bool ret = false;

    if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT ){
        app_voice_prompt_msgid_report("aws play, state: %d", 1, vp_ctx.bt_state);
        if(vp_ctx.bt_state == VP_BT_STATE_SNIFF){
            bt_bd_addr_t bd_addr={0};
            bt_status_t status = BT_STATUS_SUCCESS;

            app_voice_prompt_get_SP_bd_addr(&bd_addr);
            status = app_voice_prompt_exit_sniff_mode(bd_addr);
            if(status == BT_STATUS_SUCCESS){// waiting for exit sniff mode
                app_voice_prompt_msgid_report("exiting sniff mode...", 0);
                vp_ctx.bt_state = VP_BT_STATE_SNIFF_EXITING;
                return true;
            }else if(status == BT_CONNECTION_MANAGER_STATUS_STATE_ALREADY_EXIST){
                vp_ctx.bt_state = VP_BT_STATE_ACTIVE;
                app_voice_prompt_enable_sniff(false); //disable sniff mode while vp playing
            }else{
                app_voice_prompt_msgid_report("exiting sniff mode fail, 0x%x, play anyway", 1, status);
                vp_ctx.bt_state = VP_BT_STATE_SNIFF;
            }
        }else if(vp_ctx.bt_state == VP_BT_STATE_SNIFF_EXITING){
            app_voice_prompt_msgid_report("bt is exiting sniff mode, waitting", 0);
            return true;
        }
    }
    if (BT_AWS_MCE_SRV_LINK_NONE != bt_aws_mce_srv_get_link_type()) {
        //bt_bd_addr_t connected_dev = {0};
        //bt_clock_t play_clock = {0};

        //bt_sink_srv_memcpy(&connected_dev, device_p, sizeof(bt_bd_addr_t));
        bt_aws_mce_report_info_t info;
        uint8_t *param = bt_sink_srv_memory_alloc( sizeof(app_voice_prompt_play_sync_t) +1 );
        app_voice_prompt_play_sync_t *sync_context =( app_voice_prompt_play_sync_t *) (param +1);

        info.module_id = BT_AWS_MCE_REPORT_MODULE_VP;
        info.sync_time=delay_time;
        info.param_len=sizeof(app_voice_prompt_play_sync_t) +1;
        info.param =(void *)param;

        param[0] = APP_VOICE_PROMPT_SYNC_PLAY;
        sync_context->play_index = tone_idx;
        sync_context->level = level;
        sync_context->isCleanup= isCleanup;
        sync_context->type = type;
        sync_context->repeat = repeat;
        sync_context->callback = callback;
        sync_context->count = vp_ctx.count-1;/* local has incresed, but partner not yet */
        bt_aws_mce_report_send_sync_event(&info);

#ifdef __CONN_VP_SYNC_STYLE_ONE__
        if (tone_idx != VP_INDEX_CONNECTED) {
#endif
        ret = app_voice_prompt_local_play(tone_idx, delay_time, callback);
#ifdef __CONN_VP_SYNC_STYLE_ONE__
        }
#endif

        bt_sink_srv_memory_free(param);
        app_voice_prompt_msgid_report("voice promt delay_time: %d, vp index=%d", 2, delay_time,sync_context->play_index);
    }else{
        app_voice_prompt_msgid_report("voice promt has no connect aws device,delay_time: 0ms", 0);  
#ifdef __CONN_VP_SYNC_STYLE_ONE__
        if (tone_idx != VP_INDEX_CONNECTED) {
#endif
        ret = app_voice_prompt_local_play(tone_idx, delay_time, callback);
#ifdef __CONN_VP_SYNC_STYLE_ONE__
        }
#endif

    }

    return ret;
}

static bool app_voice_prompt_aws_stop(uint16_t id, uint32_t delay_time, app_vp_type type, bool preempted)
{
    bool ret = false;

    if (BT_AWS_MCE_SRV_LINK_NONE != bt_aws_mce_srv_get_link_type()) {
        bt_clock_t stop_clock = {0};
        bt_aws_mce_report_info_t info;

        uint8_t *param = bt_sink_srv_memory_alloc( sizeof(app_voice_prompt_stop_sync_t) +1 );
        app_voice_prompt_stop_sync_t *sync_context =( app_voice_prompt_stop_sync_t *) (param +1);
 
        info.module_id = BT_AWS_MCE_REPORT_MODULE_VP;
        info.sync_time=delay_time;
        info.param_len=sizeof(app_voice_prompt_stop_sync_t) +1;
        info.param =(void *)param;

        param[0] = APP_VOICE_PROMPT_SYNC_STOP;
        sync_context->id = id;
        sync_context->type = type;
        sync_context->preempted = preempted;

        bt_sink_srv_bt_clock_addition(&stop_clock,0,delay_time*1000);
        memcpy(&(sync_context->stop_time), &stop_clock, sizeof(bt_clock_t));

        app_voice_prompt_msgid_report("voice promt stop id: 0x%x, type: 0x%x delay_time: %d", 3,
             id, type, delay_time);

        bt_aws_mce_report_send_sync_event(& info);

        ret = app_voice_prompt_local_stop(id, delay_time, type, preempted);
        bt_sink_srv_memory_free(param);
    }else{
        app_voice_prompt_msgid_report("voice promt has no connect aws device,delay_time: 0ms", 0);
        ret = app_voice_prompt_local_stop(id, 0, type, preempted);
    }

    return ret;
}

void app_voice_prompt_sync_callback(bt_aws_mce_report_info_t *info)
{
    bt_aws_mce_report_module_id_t owner=info->module_id;
    if(owner!=BT_AWS_MCE_REPORT_MODULE_VP){
        app_voice_prompt_msgid_report("[app_voice_prompt] voice promt owner not correct: 0x%2x", 1,owner);
        return;
    }

    app_voice_prompt_play_sync_t *play_packet = (app_voice_prompt_play_sync_t *)(info->param +1);
    uint8_t *param = (uint8_t *)info->param;
    if(param[0] == APP_VOICE_PROMPT_SYNC_PLAY){
        int32_t duration=info->sync_time;
        uint32_t play_time =0;
        bool need_sync = false;
        app_voice_prompt_play_sync_t sync_index;

        memcpy(&sync_index, play_packet,sizeof(app_voice_prompt_play_sync_t));

        app_voice_prompt_msgid_report("[app_voice_prompt] play vp, duration: %d",1, duration);

        //if(duration > 2000) {
        play_time = duration;// /1000;

        app_voice_prompt_sync_count(sync_index.count);
        if(sync_index.type == VP_TYPE_VP){
            app_voice_prompt_play(sync_index.play_index, need_sync, play_time, sync_index.level, sync_index.isCleanup, sync_index.callback);
        }else if(sync_index.type == VP_TYPE_VB || sync_index.type == VP_TYPE_RT ){
            app_voice_prompt_voice_play(sync_index.play_index, need_sync, play_time,
                sync_index.level, (sync_index.type == VP_TYPE_RT) ? true: false, sync_index.repeat, sync_index.callback);
        }
        //}
        // This not need send event
    }else if(param[0] == APP_VOICE_PROMPT_SYNC_STOP){
        app_voice_prompt_stop_sync_t sync_index;
        int32_t duration=info->sync_time;
        uint32_t stop_time =0;

        memcpy(&sync_index, play_packet,sizeof(app_voice_prompt_stop_sync_t));
        stop_time = duration;// /1000;

        app_voice_prompt_msgid_report("[app_voice_prompt]  stop, id: 0x%x, type: 0x%x", 2, sync_index.id, sync_index.type);

        if(sync_index.preempted){
            app_vp_mutex_lock();
            if(sync_index.type == VP_TYPE_VP){
                app_voice_prompt_stop_internal(sync_index.id, false, stop_time, true);
            }else if(sync_index.type == VP_TYPE_VB || sync_index.type == VP_TYPE_RT ){
                app_voice_prompt_voice_stop_internal(false, stop_time, (sync_index.type == VP_TYPE_RT) ? true: false, true);
            }
            app_vp_mutex_unlock();
        }else{
            if(sync_index.type == VP_TYPE_VP){
                app_voice_prompt_stop(sync_index.id, false, stop_time);
            }else if(sync_index.type == VP_TYPE_VB || sync_index.type == VP_TYPE_RT ){
                app_voice_prompt_voice_stop(false, stop_time, (sync_index.type == VP_TYPE_RT) ? true: false);
            }
        }
    }else if(param[0] == APP_VOICE_PROMPT_SYNC_LANG){
        app_voice_prompt_lang_sync_t sync_index;
        memcpy(&sync_index, play_packet,sizeof(app_voice_prompt_lang_sync_t));
        app_voice_prompt_msgid_report("[app_voice_prompt]  vp lang: 0x%x", 1, sync_index.lang_index);
        app_voice_prompt_setLang(sync_index.lang_index,false);
    }
    return;
}
#endif

//stop current, than play next
static void app_voice_prompt_stop_and_play_next()
{
    //app_voice_prompt_list_data_t item={0};

    /* Get comfirm evt in callback, and play next one */
    prompt_control_stop_tone();

    vp_ctx.isToneStopping = true;

}

static void app_voice_prompt_callback(prompt_control_event_t event_id)
{
    vp_queue *queue = NULL;
    app_voice_prompt_list_data_t item={0};

    app_voice_prompt_msgid_report("voice prompt callback event=%d\n", 1, event_id);
    app_vp_mutex_lock();

    if (event_id == PROMPT_CONTROL_MEDIA_END) {
        //prompt_control_stop_tone();   //will call in prompt_control.c
        queue = app_voice_prompt_get_current_queue();
        app_voice_prompt_copy_item(queue, (DataType *)&item);

        app_voice_prompt_report("voice prompt play %s idx 0x%x end, level: 0x%x\n",
            app_voice_prompt_get_type_str(item.type), item.index, item.level);

        if(vp_ctx.isToneStopping){
            //prev vp has been removed in stop procedure, so no need to remove here
            app_voice_prompt_msgid_report("stop success, play next one", 0);
            vp_ctx.isToneStopping = false;
        }else{
            if(item.id != 0 && item.type == VP_TYPE_RT && item.repeat
                    && bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT){//loop play if ringtone
                app_voice_prompt_msgid_report("ringtone loop play..... %d, %d", 2, item.delay_time, vp_ctx.voiceDelay);
                if(VP_VOICE_DEFAULT_DELAY == item.delay_time){
                    //update caller delay after fisrt play.
                    queue->front->data.delay_time = vp_ctx.voiceDelay;
                    //vp_ctx.voiceDelay = 0;
                }
                vTaskDelay(50/portTICK_PERIOD_MS);
            }else{
                app_voice_prompt_noti(item.callback, item.index,  VP_ERR_CODE_SUCCESS);
                app_voice_prompt_delete_item(queue);
                queue = app_voice_prompt_get_current_queue();
            }
        }

        if(queue != NULL && queue->front){
            bool isPlayed = false;
            app_voice_prompt_copy_item(queue, (DataType *)&item);
#ifdef MTK_AWS_MCE_ENABLE
            if(item.need_sync){
                isPlayed = app_voice_prompt_aws_play(item.index, item.delay_time, item.type, item.level, vp_ctx.isCleanup, item.repeat, item.callback);
            }
            else
#endif
            {
                isPlayed = app_voice_prompt_local_play(item.index, item.delay_time, item.callback);
            }

            if(!isPlayed){
                app_voice_prompt_msgid_report("next one play fail, have to end vp play, bt state: %d", 1, vp_ctx.bt_state);
                vp_ctx.isPlaying = false;
#ifdef MTK_AWS_MCE_ENABLE
                if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT && vp_ctx.bt_state != VP_BT_STATE_SNIFF){
                    app_voice_prompt_enable_sniff(true);
                    vp_ctx.bt_state = VP_BT_STATE_SNIFF;
                }
#endif
            }
        }else{// nothing need to play, bt will enter sniff.
            app_voice_prompt_msgid_report("queue is empty, have to end vp play, bt state: %d", 1, vp_ctx.bt_state);
            vp_ctx.isPlaying = false;
#ifdef MTK_AWS_MCE_ENABLE
            if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT && vp_ctx.bt_state != VP_BT_STATE_SNIFF){
                app_voice_prompt_enable_sniff(true);
                vp_ctx.bt_state = VP_BT_STATE_SNIFF;
            }
#endif
        }

    }

    app_vp_mutex_unlock();
}

static inline void app_voice_prompt_noti(app_vp_play_callback_t callback, uint32_t index, vp_err_code err)
{
    if(callback != NULL){
        callback(index, err);
    }
}

uint16_t app_voice_prompt_play(uint32_t tone_idx, bool need_sync, uint32_t delay_time, app_vp_prio_t level, bool cleanup, app_vp_play_callback_t callback)
{
    //vp_node *node_front=NULL;
    vp_node *node_rear=NULL;
    app_voice_prompt_list_data_t item, cur_item={0};
    app_voice_prompt_list_status_t ret = VOICE_PROMPT_LIST_SUCCESS;
    uint16_t id=0;
    vp_queue *queue = NULL;
    bool stop_success = true;

    app_vp_mutex_create();
    app_vp_mutex_lock();

#ifdef MTK_AWS_MCE_ENABLE
    vp_ctx.tickAtPlay = xTaskGetTickCount();
#endif

#ifdef __CONN_VP_SYNC_STYLE_ONE__
    if (tone_idx == VP_INDEX_CONNECTED && need_sync) {
        app_voice_prompt_aws_play(tone_idx, delay_time, VP_TYPE_VP, level, cleanup, false, callback);
        need_sync = false;
    }
#endif

    do{
        if(level == VOICE_PROMPT_PRIO_ULTRA || /* voice boradcast and ringtone are invalid in this func, please use app_voice_prompt_voice_play() */
            level < VOICE_PROMPT_PRIO_LOW || level > VOICE_PROMPT_PRIO_EXTREME){
            app_voice_prompt_msgid_error("Error, invaild level 0x%x, play vp fail.", 1, level);
            break;
        }

        if(g_app_voice_prompt_test_off){
            app_voice_prompt_msgid_warning("Warning, VP testing runing, skp idx: 0x%x.", 1, tone_idx);
            break;
        }

        queue = app_voice_prompt_get_queue_by_prio(level);
        if(queue == NULL){
            queue = app_voice_prompt_create_queue();
            if(queue == NULL){
                app_voice_prompt_msgid_error("Error, can no create vp queue, play fail.", 0);
                break;
            }
            app_voice_prompt_set_queue_by_prio(level, queue);
        }

        if(vp_ctx.isCleanup){/* only power off vp can cleanup. */
            app_voice_prompt_msgid_warning("Warning, power off..., skip idx: 0x%x", 1, tone_idx);
            break;
        }

        /*  get current */
        app_voice_prompt_copy_item(app_voice_prompt_get_current_queue(), &cur_item);
        if(cur_item.id != 0){
            app_voice_prompt_msgid_report("incoming vp idx: 0x%x, level: 0x%x, current vp: 0x%x, level: 0x%x", 4,
                tone_idx, level, cur_item.index, cur_item.level);
        }else{
            app_voice_prompt_msgid_report("incoming vp idx: 0x%x, level: 0x%x", 2, tone_idx, level);
        }

        //node_front = queue->front;
        node_rear = queue->rear;
        if(node_rear && (node_rear->data.index == tone_idx)){
            app_voice_prompt_msgid_warning("Warning, queue the same vp, skip idx: 0x%x", 1, tone_idx);
            break;
        }

        /* for uint16_t count overflow */
        if(vp_ctx.count == 0){
            vp_ctx.count++;
        }

        if(cleanup){/* empty all queue. caution: after this, all nodes from queue will be free */
            if(level != VOICE_PROMPT_PRIO_EXTREME){
                app_voice_prompt_msgid_report(" clean up fail due to not extreme level, cur level: 0x%x", 1, level);
                break;
            }
            vp_ctx.isCleanup = cleanup;/* keep cleanup flag once set */
            for(int i=VOICE_PROMPT_PRIO_EXTREME; i >= VOICE_PROMPT_PRIO_LOW; i--){
                if(i == cur_item.level){
                    /* curr will be delete in stop procedure*/
                    app_voice_prompt_delete_all_item_excp_curr(app_voice_prompt_get_queue_by_prio(cur_item.level));
                }else{
                    app_voice_prompt_delete_all_item(app_voice_prompt_get_queue_by_prio(i));
                }
            }
        }

        item.need_sync = need_sync;
        item.index = tone_idx;
        item.delay_time = delay_time;
        item.id = ++vp_ctx.count;
        item.level = level;
        item.callback = callback;
        item.type = VP_TYPE_VP;
        item.repeat = false;

        if(cur_item.id != 0 ){/* some on is playing */
#ifdef __CONN_VP_SYNC_STYLE_ONE__
            if(level > cur_item.level || tone_idx == VP_INDEX_CONNECTED){/* higher priority vp incoming, stop current */
#else
            if(level > cur_item.level){/* higher priority vp incoming, stop current */
#endif
                app_voice_prompt_report("stop current %s and play idx: 0x%x", app_voice_prompt_get_type_str(cur_item.type), tone_idx);
                if(cur_item.level == VOICE_PROMPT_PRIO_ULTRA){
                    stop_success = app_voice_prompt_voice_stop_internal(cur_item.need_sync,cur_item.delay_time, (cur_item.type == VP_TYPE_RT) ? true : false, true);
                }else{
                    stop_success = app_voice_prompt_stop_internal(cur_item.id, cur_item.need_sync, cur_item.delay_time, true);
                }
                ret = app_voice_prompt_insert_item(queue, &item);
                if(VOICE_PROMPT_LIST_SUCCESS != ret){
                    app_voice_prompt_msgid_report("queuing vp fail, id: 0x%x", 1, tone_idx);
                    break;
                }
                id = item.id;

                /* just in case, play tone if stop fail */
                if(!stop_success){
                    bool isPlayed = false;
                    app_voice_prompt_msgid_report("stop current vp fail, just play", 0);
#ifdef MTK_AWS_MCE_ENABLE
                    if(need_sync){
                        isPlayed = app_voice_prompt_aws_play(tone_idx, delay_time, VP_TYPE_VP, level, cleanup, item.repeat, item.callback);
                    }
                    else
#endif
                    {
                        isPlayed = app_voice_prompt_local_play(tone_idx, delay_time, item.callback);
                    }
                    if(!isPlayed){
                        vp_ctx.isPlaying = false;
                        id = 0;
#ifdef MTK_AWS_MCE_ENABLE
                        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT && vp_ctx.bt_state != VP_BT_STATE_SNIFF){
                            app_voice_prompt_enable_sniff(true);
                            vp_ctx.bt_state = VP_BT_STATE_SNIFF;
                        }
#endif
                    }
                }
            }else{
                app_voice_prompt_msgid_report("just queue idx: 0x%x", 1, tone_idx);
                ret = app_voice_prompt_insert_item(queue, &item);
                if(VOICE_PROMPT_LIST_SUCCESS != ret){
                    app_voice_prompt_msgid_report("queuing vp fail, id: 0x%x", 1, tone_idx);
                    break;
                }
                id = item.id;
            }
        }else{
            ret = app_voice_prompt_insert_item(queue, &item);
            if(VOICE_PROMPT_LIST_SUCCESS != ret){
                app_voice_prompt_msgid_report("queuing vp fail, id: 0x%x", 1, tone_idx);
                break;
            }
            id = item.id;
            
            if(vp_ctx.isToneStopping){
                app_voice_prompt_msgid_report("someone is stopping, skip play now.",0);
                id = 0;
            }else{
                bool isPlayed = false;
#ifdef MTK_AWS_MCE_ENABLE
                if(need_sync){
                    isPlayed = app_voice_prompt_aws_play(tone_idx, delay_time, VP_TYPE_VP, level, cleanup, item.repeat, item.callback);
                }
                else
#endif
                {
                    isPlayed = app_voice_prompt_local_play(tone_idx, delay_time, item.callback);
                }
                if(!isPlayed){
                    vp_ctx.isPlaying = false;
                    id = 0;
#ifdef MTK_AWS_MCE_ENABLE
                    if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT && vp_ctx.bt_state != VP_BT_STATE_SNIFF){
                        app_voice_prompt_enable_sniff(true);
                        vp_ctx.bt_state = VP_BT_STATE_SNIFF;
                    }
#endif
                }

            }
        }
        app_voice_prompt_msgid_report("play id: 0x%x", 1, id);
    }while(0);

    app_vp_mutex_unlock();

    return id;
}

bool app_voice_prompt_stop(uint16_t id, bool need_sync, uint32_t delay_time)
{
    bool ret = false;

    app_vp_mutex_lock();

    ret = app_voice_prompt_stop_internal(id, need_sync, delay_time, false);

    app_vp_mutex_unlock();

    return ret;
}

static bool app_voice_prompt_stop_internal(uint16_t id, bool need_sync, uint32_t delay_time, bool preempted)
{
    bool ret = false;
    app_voice_prompt_list_data_t item={0};

    app_voice_prompt_msgid_report("voice prompt stop internal, id: 0x%x, preempt: %d", 2, id, preempted);

    if(id == 0){
        app_voice_prompt_msgid_warning("voice prompt stop invalid id, stop fail.", 0);
        return true;
    }

    app_voice_prompt_copy_item(app_voice_prompt_get_current_queue(), &item);
    if(item.id == 0 || item.type != VP_TYPE_VP){
        app_voice_prompt_msgid_warning("voice prompt not vp playing or invaild type", 0);
        return false;
    }

#ifdef MTK_AWS_MCE_ENABLE
    if(need_sync){
        ret = app_voice_prompt_aws_stop(id, delay_time, VP_TYPE_VP, preempted);
    }
    else
#endif
    {
        ret = app_voice_prompt_local_stop(id, delay_time, VP_TYPE_VP, preempted);
    }

    app_voice_prompt_msgid_report("voice prompt stop internal done.", 0);

    return ret;
}

uint16_t app_voice_prompt_voice_play(uint32_t tone_idx, bool need_sync, uint32_t delay_time, app_vp_prio_t level, bool isRingtone, bool repeat, app_vp_play_callback_t callback)
{
    uint16_t id = 0;
    vp_queue *queue = NULL;
    app_voice_prompt_list_data_t item, cur_item={0};
    app_voice_prompt_list_status_t ret = VOICE_PROMPT_LIST_SUCCESS;
    bool stop_success = true;

    app_vp_mutex_create();
    app_vp_mutex_lock();

#ifdef MTK_AWS_MCE_ENABLE
    vp_ctx.tickAtPlay = xTaskGetTickCount();
#endif

    do{
        if(level != VOICE_PROMPT_PRIO_ULTRA){//only voice boardcast and ringtone come here
           app_voice_prompt_msgid_error("Error, invaild level 0x%x, play voice fail.", 1, level);
           break;
        }

        if(g_app_voice_prompt_test_off){
            app_voice_prompt_msgid_warning("Warning, vp testing runing, skp idx: 0x%x.", 1, tone_idx);
            break;
        }
        queue = app_voice_prompt_get_queue_by_prio(level);
        if(queue == NULL){
           queue = app_voice_prompt_create_queue();
           if(queue == NULL){
               app_voice_prompt_msgid_error("Error, can no create voice queue, play fail.", 0);
               break;
           }
           app_voice_prompt_set_queue_by_prio(level, queue);
        }

        if(vp_ctx.isCleanup){//only power off vp can cleanup.
           app_voice_prompt_msgid_warning("Warning, power off..., skip idx: 0x%x", 1, tone_idx);
           break;
        }

        //for uint16_t count overflow
        if(vp_ctx.count == 0){
            vp_ctx.count++;
        }

        //get current
        app_voice_prompt_copy_item(app_voice_prompt_get_current_queue(), &cur_item);
        if(cur_item.id != 0){
            app_voice_prompt_report("incoming %s id: 0x%x, level: 0x%x, current %s: 0x%x, level: 0x%x", 
               (isRingtone ? "rt" : "vb"), tone_idx, level, app_voice_prompt_get_type_str(cur_item.type), cur_item.index, cur_item.level);

            if(isRingtone && cur_item.type == VP_TYPE_VB){//if current is vc and ringtone coming, remove all vb
                //app_voice_prompt_delete_all_item(app_voice_prompt_get_queue_by_prio(cur_item.level));
                app_voice_prompt_delete_all_item_excp_curr(app_voice_prompt_get_queue_by_prio(cur_item.level));
            }else if(!isRingtone && cur_item.type == VP_TYPE_RT){//if current is ringtone and vc coming, reject it
                app_voice_prompt_msgid_warning("ringtone is playing, reject vb ", 0);
                break;
            }else if(isRingtone && cur_item.type == VP_TYPE_RT && tone_idx == cur_item.index){
                vp_node *node = app_voice_prompt_get_current_queue()->front;
                node->data.repeat = repeat;
                node->data.need_sync = need_sync;
                app_voice_prompt_msgid_warning("the same ringtone is playing, just update flag", 0);
                break;
            }
        }else{
            app_voice_prompt_report("incoming %s id: 0x%x, level: 0x%x",(isRingtone ? "rt" : "vb"), tone_idx, level);
        }

        item.need_sync = need_sync;
        item.index = tone_idx;
        if(repeat && isRingtone){
            item.delay_time = VP_VOICE_DEFAULT_DELAY;
            vp_ctx.voiceDelay = delay_time;
        }else{
            item.delay_time = delay_time;
        }
        item.id = ++vp_ctx.count;
        item.level = level;
        item.type = (isRingtone ? VP_TYPE_RT : VP_TYPE_VB);
        item.repeat = repeat;
        item.callback = callback;

        if(cur_item.id != 0 ){//some on is playing
            if(level > cur_item.level){//higher priority vp incoming, stop current
                app_voice_prompt_report("stop current %s: 0x%x than play id: 0x%x",app_voice_prompt_get_type_str(cur_item.type), cur_item.index,  tone_idx);
                stop_success = app_voice_prompt_stop_internal(cur_item.id, cur_item.need_sync, cur_item.delay_time, true);
                ret = app_voice_prompt_insert_item(queue, &item);
                if(VOICE_PROMPT_LIST_SUCCESS != ret){
                    app_voice_prompt_msgid_report("queuing voice fail, id: 0x%x", 1, tone_idx);
                    break;
                }
                id = item.id;

                if(!stop_success){
                    bool isPlayed = false;
                    app_voice_prompt_msgid_report("stop current rt or vb fail, just play", 0);
#ifdef MTK_AWS_MCE_ENABLE
                    if(need_sync){
                        isPlayed = app_voice_prompt_aws_play(tone_idx, item.delay_time, isRingtone ? VP_TYPE_RT : VP_TYPE_VB, level, false, repeat, item.callback);
                    }
                    else
#endif
                    {
                        isPlayed = app_voice_prompt_local_play(tone_idx, item.delay_time, item.callback);
                    }
                    if(!isPlayed){
                        vp_ctx.isPlaying = false;
#ifdef MTK_AWS_MCE_ENABLE
                        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT && vp_ctx.bt_state != VP_BT_STATE_SNIFF){
                            app_voice_prompt_enable_sniff(true);
                            vp_ctx.bt_state = VP_BT_STATE_SNIFF;
                        }
#endif
                    }

                }
            }else{
                app_voice_prompt_msgid_report("just queue id: 0x%x", 1, tone_idx);
                ret = app_voice_prompt_insert_item(queue, &item);
                if(VOICE_PROMPT_LIST_SUCCESS != ret){
                    app_voice_prompt_msgid_report("queuing voice fail, id: 0x%x", 1, tone_idx);
                    break;
                }
                id = item.id;
            }
        }else{
            ret = app_voice_prompt_insert_item(queue, &item);
            if(VOICE_PROMPT_LIST_SUCCESS != ret){
                app_voice_prompt_msgid_report("queuing voice fail, id: 0x%x", 1, tone_idx);
                break;
            }
            id = item.id;

            if(vp_ctx.isToneStopping){
                app_voice_prompt_msgid_report("someone is stopping, skip play now.",0);
            }else{
                bool isPlayed = false;
#ifdef MTK_AWS_MCE_ENABLE
                if(need_sync){
                    isPlayed = app_voice_prompt_aws_play(tone_idx, item.delay_time, isRingtone ? VP_TYPE_RT : VP_TYPE_VB, level, false, repeat, item.callback);
                }
                else
#endif
                {
                    isPlayed = app_voice_prompt_local_play(tone_idx, item.delay_time, item.callback);
                }
                if(!isPlayed){
                    vp_ctx.isPlaying = false;
#ifdef MTK_AWS_MCE_ENABLE
                    if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT && vp_ctx.bt_state != VP_BT_STATE_SNIFF){
                        app_voice_prompt_enable_sniff(true);
                        vp_ctx.bt_state = VP_BT_STATE_SNIFF;
                    }
#endif
                }
            }
        }
    }while(0);

    app_vp_mutex_unlock();

    return id;
}

bool app_voice_prompt_voice_stop(bool need_sync, uint32_t delay_time, bool isRingtone)
{
    bool ret = false;

    app_vp_mutex_lock();

    ret = app_voice_prompt_voice_stop_internal(need_sync, delay_time, isRingtone, false);

    app_vp_mutex_unlock();

    return ret;
}

static bool app_voice_prompt_voice_stop_internal(bool need_sync, uint32_t delay_time, bool isRingtone, bool preempted)
{
    bool ret = false;
    app_voice_prompt_list_data_t item={0};

    app_voice_prompt_copy_item(app_voice_prompt_get_current_queue(), &item);
    if(item.id != 0 && (item.type == VP_TYPE_VB || item.type == VP_TYPE_RT)){
        app_voice_prompt_report("stop internal %s: curr type: %s, idx 0x%x, level: %d, preempt: %d", isRingtone?"rt":"vb", 
            app_voice_prompt_get_type_str(item.type),item.index, item.level, preempted);
    }else{
        app_voice_prompt_report("stop internal %s, no one playing or invalid type, curr idx: 0x%x type: %s",
            isRingtone?"rt":"vb", item.index, app_voice_prompt_get_type_str(item.type)) ;
        return false;
    }

#ifdef MTK_AWS_MCE_ENABLE
    if(need_sync){
        ret = app_voice_prompt_aws_stop(item.id, delay_time, isRingtone ? VP_TYPE_RT : VP_TYPE_VB, preempted);
    }
    else
#endif
    {
        ret = app_voice_prompt_local_stop(item.id, delay_time, isRingtone ? VP_TYPE_RT : VP_TYPE_VB, preempted);
    }

    return ret;
}

app_vp_type app_voice_prompt_get_current_type()
{
    app_voice_prompt_list_data_t item={0};

    app_voice_prompt_copy_item(app_voice_prompt_get_current_queue(), &item);

    if(item.id == 0){
        return VP_TYPE_NONE;
    }else{
        return item.type;
    }
}


uint16_t app_voice_prompt_get_current_index()
{
    app_voice_prompt_list_data_t item={0};

    app_voice_prompt_copy_item(app_voice_prompt_get_current_queue(), &item);
    if(item.id != 0){
        return item.index;
    }else{
        return 0xFFFF;
    }
}

uint16_t app_voice_prompt_get_current_id()
{
    app_voice_prompt_list_data_t item={0};

    app_voice_prompt_copy_item(app_voice_prompt_get_current_queue(), &item);
    return item.id;
}

void app_voice_prompt_sync_count(uint16_t count){
    vp_ctx.count= count;
    app_voice_prompt_msgid_report("current count: 0x%x", 1, count);
}


bool app_voice_prompt_setLang(uint8_t index, bool need_sync){
#ifdef MTK_AWS_MCE_ENABLE
    if(need_sync){
        if (BT_AWS_MCE_SRV_LINK_NONE != bt_aws_mce_srv_get_link_type()) {
          bt_aws_mce_report_info_t info;
          uint8_t *param = bt_sink_srv_memory_alloc( sizeof(app_voice_prompt_lang_sync_t) +1 );
          app_voice_prompt_lang_sync_t *lang_context =( app_voice_prompt_lang_sync_t *) (param +1);
			
          info.module_id = BT_AWS_MCE_REPORT_MODULE_VP;
          info.sync_time=0;
          info.is_sync=false;
          info.param_len=sizeof(app_voice_prompt_lang_sync_t) +1;
          info.param =(void *)param;
          param[0] = APP_VOICE_PROMPT_SYNC_LANG;
	lang_context->lang_index = index;
           bt_aws_mce_report_send_event(&info);
	bt_sink_srv_memory_free(param);

        }

        return app_voice_prompt_VPSetLang(index);
    }else
#endif
    {
        return app_voice_prompt_VPSetLang(index);
    }
}

uint8_t app_voice_prompt_getLang()
{
    uint8_t index = 0xFF;
    
    app_voice_prompt_VPGetLang(&index);
    return index;
}


uint16_t app_voice_prompt_getLangCount()
{
    uint16_t count = 0;
    
    app_voice_prompt_GetSupportLangCnt(&count);
    return count;
}


bool app_voice_prompt_getSupportLang(uint16_t *buffer)
{
    if(buffer == NULL){
        return false;
    }else{
        return app_voice_prompt_GetSupportLang(buffer);
    }
}

void app_voice_prompt_dump_all(){
    vp_queue *queue = NULL;
    vp_node *node;
    app_vp_mutex_lock();

    app_voice_prompt_report("-----------------------dump queuing vp begin-------------------------");
    for(int i=VOICE_PROMPT_PRIO_EXTREME; i >= VOICE_PROMPT_PRIO_LOW; i--){
        queue = app_voice_prompt_get_queue_by_prio(i);

        //empty queue
        if(queue == NULL || queue->front == NULL){
            continue;
        }else{
            node = queue->front;
            while(node){
                app_voice_prompt_msgid_report("queue: 0x%08x, id: 0x%x, vp idx: 0x%x, level: %d, type: %d", 5, 
                    queue, node->data.id, node->data.index, node->data.level, node->data.type);
                node = node->next;
            }
        }
    }
    app_voice_prompt_report("-----------------------dump queuing vp end---------------------------");
    app_vp_mutex_unlock();
}

#endif /*MTK_PROMPT_SOUND_ENABLE*/

