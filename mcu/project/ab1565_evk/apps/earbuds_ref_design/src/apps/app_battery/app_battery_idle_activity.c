/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include <string.h>

#include "app_battery_idle_activity.h"
#include "app_battery_transient_activity.h"
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
#include "app_rho_idle_activity.h"
#endif
#include "apps_config_led_manager.h"
#include "apps_config_led_index_list.h"
#include "apps_config_vp_manager.h"
#include "apps_config_vp_index_list.h"
#include "apps_config_features_dynamic_setting.h"
#include "apps_events_interaction_event.h"
#include "apps_events_event_group.h"
#include "apps_config_key_remapper.h"
#include "battery_management.h"
#include "battery_management_core.h"
#include "apps_debug.h"
#include "ui_shell_manager.h"
#include "FreeRTOS.h"
#include "bt_power_on_config.h"
#include "bt_device_manager.h"
#include "apps_customer_config.h"
#if 0//def MTK_ANC_ENABLE
#include "anc_control.h"
#endif
#if defined(MTK_AWS_MCE_ENABLE)
#include "bt_aws_mce_report.h"
#endif
#ifdef MTK_IN_EAR_FEATURE_ENABLE
#include "app_in_ear_idle_activity.h"
#endif

#ifdef APPS_SLEEP_AFTER_NO_CONNECTION
#include "app_power_saving_utils.h"
#endif
#if defined(SUPPORT_ROLE_HANDOVER_SERVICE) && defined(MTK_FOTA_ENABLE)
#include "app_fota_idle_activity.h"
#endif

#define LOG_TAG     "[app_bat]"

static battery_local_context_type_t *s_battery_local_context = NULL;

static void _shutdown_when_low_battery(struct _ui_shell_activity *self)
{
    APPS_LOG_MSGID_I("_shutdown_when_low_battery", 0);
    apps_config_set_vp(VP_INDEX_POWER_OFF, false, 0, VOICE_PROMPT_PRIO_EXTREME, true, NULL);
    apps_config_set_foreground_led_pattern(LED_INDEX_POWER_OFF, 30, false);
    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
            APPS_EVENTS_INTERACTION_REQUEST_POWER_OFF, NULL, 0,
            NULL, 0);
}

static void battery_update_led_background_pattern(ui_shell_activity_t *self, app_battery_state_t new_state)
{
    battery_local_context_type_t *local_ctx = (battery_local_context_type_t *)self->local_context;
    if ((local_ctx->state > APP_BATTERY_STATE_LOW_CAP && local_ctx->state < APP_BATTERY_STATE_CHARGING)
            && (new_state <= APP_BATTERY_STATE_LOW_CAP || new_state >= APP_BATTERY_STATE_CHARGING)) {
        // Old state doesn't need show BG LED, but new state need show BG LED
        APPS_LOG_MSGID_I("need start battery transient activity to display BG LED", 0);
        ui_shell_start_activity(self, app_battery_transient_activity_proc, ACTIVITY_PRIORITY_LOW, local_ctx, 0);
    }
    if (local_ctx->state != new_state) {
        APPS_LOG_MSGID_I("old state = %d, new state = %d", 2, local_ctx->state, new_state);
        ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                APPS_EVENTS_INTERACTION_UPDATE_LED_BG_PATTERN, NULL, 0,
                NULL, 0);
    }
}

#ifdef APPS_SLEEP_AFTER_NO_CONNECTION
static app_power_saving_target_mode_t app_battery_get_power_saving_target_mode(void)
{
    app_power_saving_target_mode_t target_mode = APP_POWER_SAVING_TARGET_MODE_SYSTEM_OFF;

    if (s_battery_local_context && s_battery_local_context->charger_exist_state) {
#ifdef APPS_DISABLE_BT_WHEN_CHARGING
        // If support APPS_DISABLE_BT_WHEN_CHARGING, other APP will disable BT
        target_mode = APP_POWER_SAVING_TARGET_MODE_NORMAL;
#else
        // When charging in, cannot system off.
        target_mode = APP_POWER_SAVING_TARGET_MODE_BT_OFF;
#endif
    }
    return target_mode;
}
#endif

static app_battery_state_t _get_battery_state(struct _ui_shell_activity *self)
{
    battery_local_context_type_t *local_ctx = (battery_local_context_type_t *)self->local_context;
    app_battery_state_t new_state;

    APPS_LOG_MSGID_I("charger_exist_state %d, charger state %d", 3, local_ctx->charger_exist_state, local_ctx->charging_state);
    if (local_ctx->charger_exist_state) {
        if (local_ctx->charging_state == CHARGER_STATE_CHR_OFF || local_ctx->charging_state == CHARGER_STATE_EOC) {
            new_state = APP_BATTERY_STATE_CHARGING_FULL;
        } else if (local_ctx->charging_state == CHARGER_STATE_THR) {
            new_state = APP_BATTERY_STATE_THR;
        } else {
            new_state = APP_BATTERY_STATE_CHARGING;
        }
    } else {
        if (local_ctx->shutdown_state == APPS_EVENTS_BATTERY_SHUTDOWN_STATE_VOLTAGE_LOW) {
            new_state = APP_BATTERY_STATE_SHUTDOWN;
        } else if (local_ctx->battery_percent < APPS_BATTERY_LOW_THRESHOLD) {
            new_state = APP_BATTERY_STATE_LOW_CAP;
        } else if (local_ctx->battery_percent >= APPS_BATTERY_FULL_THRESHOLD) {
            new_state = APP_BATTERY_STATE_FULL;
        } else {
            new_state = APP_BATTERY_STATE_IDLE;
        }
    }

    battery_update_led_background_pattern(self, new_state);

    return new_state;
}

#ifdef APPS_AUTO_TRIGGER_RHO
static void check_and_do_rho(struct _ui_shell_activity *self) {
    battery_local_context_type_t *local_ctx = (battery_local_context_type_t *)self->local_context;

    if (local_ctx->aws_state != BT_AWS_MCE_AGENT_STATE_ATTACHED) {
        APPS_LOG_MSGID_I("AWS not connected, cannot send aws data", 0);
        goto exit;
    }
    // Check partner battery is valid and higher than SWITCH_ROLE_PARTNER_BATTERY_LEVEL
    if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT
            && local_ctx->state != APP_BATTERY_STATE_SHUTDOWN // Shut down will ask home screen to do Power off and RHO
            && local_ctx->state < APP_BATTERY_STATE_CHARGING
            && local_ctx->partner_battery_percent < PARTNER_BATTERY_CHARGING
            && local_ctx->battery_percent + APPS_DIFFERENCE_BATTERY_VALUE_FOR_RHO < local_ctx->partner_battery_percent
            #ifdef MTK_IN_EAR_FEATURE_ENABLE
			&& app_in_ear_is_out()
            #endif
            )
    {
        APPS_LOG_MSGID_I("after checked need do RHO", 0);

        // trigger RHO start
        ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                APPS_EVENTS_INTERACTION_TRIGGER_RHO, NULL, 0,
                NULL, 0);
    }
exit:
    return;
}
#endif

#if defined(MTK_AWS_MCE_ENABLE)
static void partner_notify_battery_level_to_agent(struct _ui_shell_activity *self)
{
    battery_local_context_type_t *local_ctx = (battery_local_context_type_t *)self->local_context;

    if (local_ctx->aws_state != BT_AWS_MCE_AGENT_STATE_ATTACHED) {
        APPS_LOG_MSGID_I("AWS not connected, cannot send aws data", 0);
    } else {
        uint8_t info_array[sizeof(bt_aws_mce_report_info_t)];
        uint8_t data_array[sizeof(local_ctx->battery_percent)];
        bt_aws_mce_report_info_t *aws_data = (bt_aws_mce_report_info_t *)&info_array;
        aws_data->module_id = BT_AWS_MCE_REPORT_MODULE_BATTERY;
        aws_data->is_sync = FALSE;
        aws_data->sync_time = 0;
        aws_data->param_len = sizeof(local_ctx->battery_percent);
        if (APP_BATTERY_STATE_CHARGING > local_ctx->state) {
            *(data_array) = local_ctx->battery_percent;
        } else {
            *(data_array) = PARTNER_BATTERY_CHARGING | local_ctx->battery_percent;
        }
        aws_data->param = (void *)data_array;
        APPS_LOG_MSGID_I("Send battery value to agent : %d", 1, *((int32_t *)aws_data->param));
        bt_aws_mce_report_send_event(aws_data);
    }
}
#endif

static bool _proc_ui_shell_group(
        struct _ui_shell_activity *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    battery_local_context_type_t *local_ctx = (battery_local_context_type_t *)self->local_context;

    switch (event_id) {
    case EVENT_ID_SHELL_SYSTEM_ON_CREATE:
        APPS_LOG_MSGID_I("app_battery_idle_activity create", 0);
        self->local_context = pvPortMalloc(sizeof(battery_local_context_type_t));
        if (self->local_context) {
            local_ctx = (battery_local_context_type_t *)self->local_context;
            s_battery_local_context = local_ctx;
            app_battery_state_t temp_state;
            memset(self->local_context, 0, sizeof(battery_local_context_type_t));
            local_ctx->battery_percent = battery_management_get_battery_property(BATTERY_PROPERTY_CAPACITY);
            local_ctx->charging_state = battery_management_get_battery_property(BATTERY_PROPERTY_CHARGER_STATE);
            local_ctx->charger_exist_state = battery_management_get_battery_property(BATTERY_PROPERTY_CHARGER_EXIST);
            local_ctx->shutdown_state = calculate_shutdown_state(battery_management_get_battery_property(BATTERY_PROPERTY_VOLTAGE));
            local_ctx->aws_state = BT_AWS_MCE_AGENT_STATE_INACTIVE;
            local_ctx->partner_battery_percent = PARTNER_BATTERY_INVALID;
            local_ctx->state = APP_BATTERY_STATE_IDLE;
#ifndef CCASE_ENABLE
            if(BT_POWER_ON_NORMAL == bt_power_on_get_config_type() || BT_POWER_ON_DUT == bt_power_on_get_config_type()) {
#ifdef APPS_DISABLE_BT_WHEN_CHARGING
                // If support Charger case, enable BT dynamically when charger out
                if (local_ctx->charger_exist_state == 0)
#endif
                {
#if 0//def MTK_ANC_ENABLE
                    anc_control_result_t anc_ret = audio_anc_resume(NULL);
                    APPS_LOG_MSGID_I("audio_anc_resume ret = %d", 1, anc_ret);
#endif
                    ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                            APPS_EVENTS_INTERACTION_REQUEST_ON_OFF_BT, (void *)true, 0,
                            NULL, 0);
                }
            }
#endif

            temp_state = _get_battery_state(self);

            if (temp_state == APP_BATTERY_STATE_SHUTDOWN) {
                _shutdown_when_low_battery(self);
            } else {
#if !defined(CCASE_ENABLE) && defined(APPS_DISABLE_BT_WHEN_CHARGING)
                if (local_ctx->charger_exist_state == 0) {
#endif
                    apps_config_set_vp(VP_INDEX_POWER_ON, false, 0, VOICE_PROMPT_PRIO_HIGH, false, NULL);
                    apps_config_set_foreground_led_pattern(LED_INDEX_POWER_ON, 30, false);
                    if (temp_state == APP_BATTERY_STATE_LOW_CAP) {
                        apps_config_set_vp(VP_INDEX_LOW_BATTERY, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                    }
#if !defined(CCASE_ENABLE) && defined(APPS_DISABLE_BT_WHEN_CHARGING)
                }
#endif
            }

            local_ctx->state = temp_state;
#ifdef APPS_SLEEP_AFTER_NO_CONNECTION
            app_power_saving_utils_register_get_mode_callback(app_battery_get_power_saving_target_mode);
#endif
        } else {
            APPS_LOG_MSGID_E("Cannot alloc app_battery_idle_activity local_context", 0);
            ret = false;
        }
        break;
    case EVENT_ID_SHELL_SYSTEM_ON_DESTROY:
        APPS_LOG_MSGID_I("app_battery_idle_activity destroy", 0);
        if (self->local_context) {
            vPortFree(self->local_context);
        }
        break;
    case EVENT_ID_SHELL_SYSTEM_ON_RESUME:
        APPS_LOG_MSGID_I("app_battery_idle_activity resume", 0);
        break;
    case EVENT_ID_SHELL_SYSTEM_ON_PAUSE:
        APPS_LOG_MSGID_I("app_battery_idle_activity pause", 0);
        break;
    case EVENT_ID_SHELL_SYSTEM_ON_REFRESH:
        APPS_LOG_MSGID_I("app_battery_idle_activity refresh", 0);
        break;
    case EVENT_ID_SHELL_SYSTEM_ON_RESULT:
        APPS_LOG_MSGID_I("app_battery_idle_activity result", 0);
        if (extra_data) {
            APPS_LOG_MSGID_I("extra data for app_battery_idle_activity result", 0);
        }
        break;
    default:
        break;
    }
    return ret;
}

static bool _proc_battery_event_group(
        struct _ui_shell_activity *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    app_battery_state_t bat_state;
    app_battery_state_t old_state;
    battery_local_context_type_t *local_ctx = (battery_local_context_type_t *)self->local_context;

    switch (event_id) {
    case APPS_EVENTS_BATTERY_PERCENT_CHANGE:
        local_ctx->battery_percent = (int32_t)extra_data;
        APPS_LOG_MSGID_I("Current capacity : %d", 1, (int32_t)extra_data);
        break;
    case APPS_EVENTS_BATTERY_CHARGER_STATE_CHANGE:
        if (CHARGER_STATE_CHR_OFF == (int32_t)extra_data && local_ctx->charger_exist_state) {
            /* When charger out, to avoid the state become charging full,
                need keep the state of charging_state util timeout. */
            APPS_LOG_MSGID_I("Temporarily keep the charging_state to CHARGER_STATE_CHR_OFF", 0);
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST,
                                EVENT_GROUP_UI_SHELL_BATTERY,
                                APPS_EVENTS_BATTERY_CHARGER_FULL_CHANGE,
                                NULL, 0, NULL, 2000);
        } else {
            local_ctx->charging_state = (int32_t)extra_data;
            // To remove the event sending in case APPS_EVENTS_BATTERY_CHARGER_EXIST_CHANGE
            ui_shell_remove_event(EVENT_GROUP_UI_SHELL_BATTERY,
                                  APPS_EVENTS_BATTERY_CHARGER_FULL_CHANGE);
        }
        APPS_LOG_MSGID_I("Current charging_state : %d", 1, local_ctx->charging_state);
        break;
    case APPS_EVENTS_BATTERY_CHARGER_EXIST_CHANGE:
    {
        int32_t old_charger_exist_state = local_ctx->charger_exist_state;
        local_ctx->charger_exist_state = (int32_t)extra_data;
        if (local_ctx->charger_exist_state && !old_charger_exist_state) {
            if (CHARGER_STATE_CHR_OFF == local_ctx->charging_state) {
                /* When charger in, to avoid the state become charging full,
                    need set charge_state to CHARGER_STATE_FASTCC. */
                local_ctx->charging_state = CHARGER_STATE_FASTCC;
                ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST,
                                    EVENT_GROUP_UI_SHELL_BATTERY,
                                    APPS_EVENTS_BATTERY_CHARGER_FULL_CHANGE,
                                    NULL, 0, NULL, 2000);
                APPS_LOG_MSGID_I("Charger exist, but need wait APPS_EVENTS_BATTERY_CHARGER_STATE_CHANGE", 0);
            } else {
                APPS_LOG_MSGID_I("Charger exist, And the charging_state = %d", 1, local_ctx->charging_state);
            }
        } else if (!local_ctx->charger_exist_state
                && CHARGER_STATE_CHR_OFF != local_ctx->charging_state) {
            // To remove the event sending in case APPS_EVENTS_BATTERY_CHARGER_STATE_CHANGE
            ui_shell_remove_event(EVENT_GROUP_UI_SHELL_BATTERY,
                                  APPS_EVENTS_BATTERY_CHARGER_FULL_CHANGE);
            local_ctx->charging_state = CHARGER_STATE_CHR_OFF;
            APPS_LOG_MSGID_I("Set charging_state to CHARGER_STATE_CHR_OFF when charger out", 0);
        }
        local_ctx->battery_percent = battery_management_get_battery_property(BATTERY_PROPERTY_CAPACITY);
        local_ctx->shutdown_state = calculate_shutdown_state(battery_management_get_battery_property(BATTERY_PROPERTY_VOLTAGE));
        APPS_LOG_MSGID_I("Current charger_exist_state : %d", 1, local_ctx->charger_exist_state);

#ifdef APPS_SLEEP_AFTER_NO_CONNECTION
        app_power_saving_utils_notify_mode_changed(app_battery_get_power_saving_target_mode);
#endif
        break;
    }
    case APPS_EVENTS_BATTERY_SHUTDOWN_STATE_CHANGE:
        local_ctx->shutdown_state = (battery_event_shutdown_state_t)extra_data;
        break;
    case APPS_EVENTS_BATTERY_CHARGER_FULL_CHANGE:
        APPS_LOG_MSGID_I("Received APPS_EVENTS_BATTERY_CHARGER_FULL_CHANGE delay message, set charging_state to CHARGER_STATE_CHR_OFF", 0);
        local_ctx->charging_state = CHARGER_STATE_CHR_OFF;
        break;
    default:
        APPS_LOG_MSGID_I("Doesn't care battery event: %d", 1, event_id);
        break;
    }

    bat_state = _get_battery_state(self);
    old_state = local_ctx->state;

    if (bat_state == APP_BATTERY_STATE_LOW_CAP) {
        apps_config_set_vp(VP_INDEX_LOW_BATTERY, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
    } else if (bat_state == old_state) {
        // Do nothing when no change
    } else if ((bat_state == APP_BATTERY_STATE_SHUTDOWN)
            && old_state != APP_BATTERY_STATE_SHUTDOWN) {
        APPS_LOG_MSGID_I("Start Power Off", 0);
    } else if (bat_state == APP_BATTERY_STATE_CHARGING_FULL || old_state == APP_BATTERY_STATE_CHARGING)  {
        apps_config_set_foreground_led_pattern(LED_INDEX_CHARGING_FULL, 50, false);
    }

#ifdef APPS_DISABLE_BT_WHEN_CHARGING
    if(BT_POWER_ON_NORMAL == bt_power_on_get_config_type() || BT_POWER_ON_DUT == bt_power_on_get_config_type()) {
        if (APP_BATTERY_STATE_CHARGING <= bat_state
                && APP_BATTERY_STATE_CHARGING > local_ctx->state) {
#if 0//def MTK_ANC_ENABLE
            anc_control_result_t anc_ret = audio_anc_suspend(NULL);
            APPS_LOG_MSGID_I("audio_anc_suspend after charger in ret = %d", 1, anc_ret);
#endif
            APPS_LOG_MSGID_I("send request to do APPS_EVENTS_INTERACTION_REQUEST_ON_OFF_BT, enable : %d", 1, false);
            apps_config_set_foreground_led_pattern(LED_INDEX_POWER_OFF, 30, false);
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_REQUEST_ON_OFF_BT, (void *)false, 0,
                    NULL, 0);
        } else if (APP_BATTERY_STATE_CHARGING > bat_state
                && APP_BATTERY_STATE_CHARGING <= local_ctx->state) {
            // If support Charger case, enable BT dynamically when charger out
#if 0//def MTK_ANC_ENABLE
            anc_control_result_t anc_ret = audio_anc_resume(NULL);
            APPS_LOG_MSGID_I("audio_anc_resume after charger out ret = %d", 1, anc_ret);
#endif
            apps_config_set_vp(VP_INDEX_POWER_ON, false, 0, VOICE_PROMPT_PRIO_HIGH, false, NULL);
            apps_config_set_foreground_led_pattern(LED_INDEX_POWER_ON, 30, false);
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_REQUEST_ON_OFF_BT, (void *)true, 0,
                    NULL, 0);
        }
    }
#endif

    local_ctx->state = bat_state;

    if (APP_BATTERY_STATE_SHUTDOWN == local_ctx->state) {
        _shutdown_when_low_battery(self);
    } else {
#if defined(MTK_AWS_MCE_ENABLE)
        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER) {
            partner_notify_battery_level_to_agent(self);
        } else if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT) {
#ifdef APPS_AUTO_TRIGGER_RHO
            if (apps_config_features_is_auto_rho_enabled()) {
                check_and_do_rho(self);
            }
#endif
        }
#endif
    }

    return ret;
}

static bool battery_app_bt_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;

    return ret;
}

static bool battery_app_bt_cm_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
#if defined(MTK_AWS_MCE_ENABLE)
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
#endif

    switch (event_id) {
#if defined(MTK_AWS_MCE_ENABLE)
        case BT_CM_EVENT_REMOTE_INFO_UPDATE:
        {
            // Check Partner AWS connected
            bt_cm_remote_info_update_ind_t *remote_update = (bt_cm_remote_info_update_ind_t *)extra_data;
            battery_local_context_type_t *local_ctx = (battery_local_context_type_t *)self->local_context;
            if (NULL == remote_update || NULL == local_ctx) {
                break;
            }

            if (BT_AWS_MCE_ROLE_PARTNER == role) {
                if (!(remote_update->pre_connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS))
                        && (remote_update->connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS))) {
                    local_ctx->aws_state = BT_AWS_MCE_AGENT_STATE_ATTACHED;
                    partner_notify_battery_level_to_agent(self);
                } else if ((remote_update->pre_connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS))
                        && !(remote_update->connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS))) {
                    local_ctx->aws_state = BT_AWS_MCE_AGENT_STATE_INACTIVE;
                }
            } else if (BT_AWS_MCE_ROLE_AGENT == role) {
                if (!(remote_update->pre_connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS))
                        && (remote_update->connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS))) {
                    local_ctx->partner_battery_percent = PARTNER_BATTERY_INVALID;
                    local_ctx->aws_state = BT_AWS_MCE_AGENT_STATE_ATTACHED;
                } else if ((remote_update->pre_connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS))
                        && !(remote_update->connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS))) {
                    local_ctx->aws_state = BT_AWS_MCE_AGENT_STATE_INACTIVE;
                }
            }
        }
            break;
#endif
        default:
            break;
    }
    return ret;
}

#if defined(MTK_AWS_MCE_ENABLE)
static bool _proc_aws_report_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    bt_aws_mce_report_info_t *aws_data_ind = (bt_aws_mce_report_info_t *)extra_data;
    battery_local_context_type_t *local_ctx = (battery_local_context_type_t *)self->local_context;
    APPS_LOG_MSGID_I("battery_proc_aws_report:module 0x%0x", 1, aws_data_ind->module_id);
    if (aws_data_ind->module_id == BT_AWS_MCE_REPORT_MODULE_BATTERY
            && bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_AGENT) {
        configASSERT(aws_data_ind->param_len == sizeof(local_ctx->partner_battery_percent));
        memcpy(&local_ctx->partner_battery_percent, aws_data_ind->param, sizeof(local_ctx->partner_battery_percent));
        APPS_LOG_MSGID_I("Received partner battery = %d", 1, local_ctx->partner_battery_percent);
    #ifdef APPS_AUTO_TRIGGER_RHO
                    if (apps_config_features_is_auto_rho_enabled()) {
                        check_and_do_rho(self);
                    }
    #endif
                }
    return ret;
}
#endif

static bool _app_interaction_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;

    switch (event_id) {
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
        case APPS_EVENTS_INTERACTION_RHO_END:
            APPS_LOG_MSGID_I("battery received RHO end", 0);
            {
                battery_local_context_type_t *local_ctx = (battery_local_context_type_t *)self->local_context;
                app_rho_result_t rho_ret = (app_rho_result_t)extra_data;
                if (APP_RHO_RESULT_SUCCESS == rho_ret) {
                    // When agent switched to partner, set the partner_battery_percent to invaid value
                    local_ctx->partner_battery_percent = PARTNER_BATTERY_INVALID;
                    APPS_LOG_MSGID_I("RHO success", 0);
                }
            }
            break;
#endif
        default:
            APPS_LOG_MSGID_I("Not supported event id = %d", 1, event_id);
            break;
    }

    return ret;
}

static bool battery_idle_fota_event_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
#if defined(SUPPORT_ROLE_HANDOVER_SERVICE) && defined(MTK_FOTA_ENABLE)
    battery_local_context_type_t *local_ctx = (battery_local_context_type_t *)self->local_context;
    if (local_ctx) {
        switch (event_id) {
            case RACE_EVENT_TYPE_FOTA_START:
                APPS_LOG_MSGID_I(LOG_TAG"FOTA start", 0);
                local_ctx->fota_doing = true;
                break;
            case RACE_EVENT_TYPE_FOTA_CANCEL:
                APPS_LOG_MSGID_I(LOG_TAG"FOTA cancel", 0);
                local_ctx->fota_doing = false;
                break;
        default:
            break;
        }
    }
#endif
    return ret;    
}

bool app_battery_idle_activity_proc(
            struct _ui_shell_activity *self,
            uint32_t event_group,
            uint32_t event_id,
            void *extra_data,
            size_t data_len)
{
    bool ret = false;
    APPS_LOG_MSGID_I("app_battery_idle_activity_proc receive event_group : %d, id: %x", 2, event_group, event_id);
    switch (event_group) {
    case EVENT_GROUP_UI_SHELL_SYSTEM:
        ret = _proc_ui_shell_group(self, event_id, extra_data, data_len);
        break;
    case EVENT_GROUP_UI_SHELL_BATTERY:
        ret = _proc_battery_event_group(self, event_id, extra_data, data_len);
        break;
    case EVENT_GROUP_UI_SHELL_BT_SINK:
        ret = battery_app_bt_event_proc(self, event_id, extra_data, data_len);
        break;
    case EVENT_GROUP_UI_SHELL_BT_CONN_MANAGER:
        ret = battery_app_bt_cm_event_proc(self, event_id, extra_data, data_len);
        break;
    case EVENT_GROUP_UI_SHELL_APP_INTERACTION:
        ret = _app_interaction_event_proc(self, event_id, extra_data, data_len);
        break;
    case EVENT_GROUP_UI_SHELL_FOTA:
        ret = battery_idle_fota_event_proc(self, event_id, extra_data, data_len);
        break;
#if defined(MTK_AWS_MCE_ENABLE)
    case EVENT_GROUP_UI_SHELL_AWS_DATA:
        ret = _proc_aws_report_group(self, event_id, extra_data, data_len);
        break;
#endif
    default:
        break;
    }
    return ret;
}

