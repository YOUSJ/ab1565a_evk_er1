/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "app_hfp_activity.h"
#include "apps_events_event_group.h"
#include "apps_config_vp_manager.h"
#include "apps_config_vp_index_list.h"
#include "apps_config_key_remapper.h"
#include "bt_aws_mce_srv.h"
#include "bt_device_manager.h"

#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
#include "app_rho_idle_activity.h"
#endif
#include "bt_device_manager.h"

static bool _proc_ui_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data,size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    switch (event_id) {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE: {
            APPS_LOG_MSGID_I(APP_HFP_ACTI", create : %x", 1, extra_data);
            self->local_context = extra_data;
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_UPDATE_LED_BG_PATTERN, NULL, 0, NULL, 0);
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_UPDATE_MMI_STATE, NULL, 0, NULL, 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY: {
            APPS_LOG_MSGID_I(APP_HFP_ACTI", destroy", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESUME: {
            
            APPS_LOG_MSGID_I(APP_HFP_ACTI", resume", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE: {
            
            APPS_LOG_MSGID_I(APP_HFP_ACTI", pause", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH: {
            APPS_LOG_MSGID_I(APP_HFP_ACTI", refresh", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESULT: {
            
            APPS_LOG_MSGID_I(APP_HFP_ACTI", result", 0);
            break;
        }
        default:
            ret = false;
            break;
    }
    return ret;
}


static bool _proc_key_event_group(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    hfp_context_t *hfp_context = (hfp_context_t *)(self->local_context);
    if (hfp_context == NULL) {
        APPS_LOG_MSGID_I(APP_HFP_ACTI", key_event_group: hfp_context is null", 0);
        return ret;
    }
    bt_sink_srv_state_t pre = hfp_context->pre_state;
    bt_sink_srv_state_t cur = hfp_context->curr_state;

    APPS_LOG_MSGID_I(APP_HFP_ACTI", key_event_group: pre = %x, curr = %x ", 2, pre, cur);

    ret = app_bt_hfp_proc_key_event(cur, event_id, extra_data, data_len);

    return ret;
}


bool ui_shell_bt_hfp_event_proc( ui_shell_activity_t *self,
        uint32_t event_group,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    APPS_LOG_MSGID_I(APP_HFP_ACTI", event_id : %x", 1, event_id);

    switch(event_id) {
        case BT_SINK_SRV_EVENT_STATE_CHANGE:
        {
            bt_sink_srv_state_change_t *param = (bt_sink_srv_state_change_t*)extra_data;
            if (param == NULL) {
                APPS_LOG_MSGID_I(APP_HFP_ACTI", state_change :param is null ", 0);
                return ret;
            }
            APPS_LOG_MSGID_I(APP_HFP_ACTI", state_change: param_previous: %x, param_now: %x", 2, param->previous, param->current);
            if (param->previous == BT_SINK_SRV_STATE_INCOMING && param->current != BT_SINK_SRV_STATE_INCOMING) {
                APPS_LOG_MSGID_I(APP_HFP_ACTI", state_change: stop_vp:",0);
               // if (hfp_context->is_vp) {
                    apps_config_stop_voice(true, 200, true);
                    app_bt_hfp_set_vp_flag(0);
                //}
            }
            if (param->current <= BT_SINK_SRV_STATE_CONNECTED)
            {
                if (param->previous == BT_SINK_SRV_STATE_ACTIVE || param->previous == BT_SINK_SRV_STATE_HELD_REMAINING
                        || param->previous == BT_SINK_SRV_STATE_HELD_ACTIVE || param->previous == BT_SINK_SRV_STATE_MULTIPARTY) {
                    /*call is end*/
#ifdef MTK_AWS_MCE_ENABLE
                    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
                    if (BT_AWS_MCE_ROLE_PARTNER == role || BT_AWS_MCE_ROLE_CLINET == role) {
                        if (bt_aws_mce_srv_get_link_type() == BT_AWS_MCE_SRV_LINK_NORMAL) {
                            //apps_config_set_vp(VP_INDEX_CALL_ENDED, true, 200, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                        }
                    } else
#endif
                    {
                        if (0 != bt_cm_get_connected_devices(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP), NULL, 0)) {
                            //apps_config_set_vp(VP_INDEX_CALL_ENDED, true, 200, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                        }
                    }
                }
                ui_shell_finish_activity(self, self);

            } else {
                apps_config_state_t state = app_get_config_status_by_state(param->current);
                APPS_LOG_MSGID_I(APP_HFP_ACTI", state_change:is_hfp: %x", 1, state);
                if (state != APP_TOTAL_STATE_NO) {
                    APPS_LOG_MSGID_I(APP_HFP_ACTI", enter self", 0);
                    ((hfp_context_t *)(self->local_context))->pre_state = param->previous;
                    ((hfp_context_t *)(self->local_context))->curr_state = param->current;
                }
            }
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_UPDATE_LED_BG_PATTERN, NULL, 0, NULL, 0);
            ui_shell_send_event(false, EVENT_PRIORITY_HIGNEST, EVENT_GROUP_UI_SHELL_APP_INTERACTION,
                    APPS_EVENTS_INTERACTION_UPDATE_MMI_STATE, NULL, 0, NULL, 0);
            break;
        }
        case BT_SINK_SRV_EVENT_HF_SPEAKER_VOLUME_CHANGE: {
            break;
        }
        case BT_SINK_SRV_EVENT_HF_CALLER_INFORMATION:
        case BT_SINK_SRV_EVENT_HF_MISSED_CALL: {
            APPS_LOG_MSGID_I(APP_HFP_ACTI", event_id : %d", 1, event_id);
            bt_sink_srv_caller_information_t *call_info = (bt_sink_srv_caller_information_t *)extra_data;
            APPS_LOG_I("Caller info: number:%s, name:%s", call_info->number,call_info->name);
            break;
        }
        case BT_SINK_SRV_EVENT_HF_VOICE_RECOGNITION_CHANGED: {
            bt_sink_srv_voice_recognition_status_t *voice_re = (bt_sink_srv_voice_recognition_status_t *)extra_data;
            APPS_LOG_MSGID_I("Voice_recogn: enable:%d", 1, voice_re->enable);
            break;
        }
        case BT_SINK_SRV_EVENT_HF_CALL_LIST_INFORMATION: { // app need query// ignore
            break;
        }
        case BT_SINK_SRV_EVENT_HF_SCO_STATE_UPDATE: {
            APPS_LOG_MSGID_I(APP_HFP_ACTI" SCO_STATE", 0);
            bt_sink_srv_sco_state_update_t *sco_state = (bt_sink_srv_sco_state_update_t *)extra_data;
            if (sco_state) {
                hfp_context_t *ctx = (hfp_context_t *)(self->local_context);
                ctx->esco_connected = sco_state->state == BT_SINK_SRV_SCO_CONNECTION_STATE_CONNECTED ? true : false;
                APPS_LOG_MSGID_I(APP_HFP_ACTI", SCO_STATE: %d", 1, sco_state->state);
            }

            break;
        }
        case BT_SINK_SRV_EVENT_HF_RING_IND: {
            hfp_context_t *hfp_context = (hfp_context_t *)(self->local_context);
            APPS_LOG_MSGID_I(APP_HFP_ACTI", pre_state: %x, cur_state : %x", 2, hfp_context->pre_state, hfp_context->curr_state);
            if (hfp_context->curr_state == BT_SINK_SRV_STATE_INCOMING && hfp_context->pre_state != BT_SINK_SRV_STATE_TWC_INCOMING) {
                app_bt_hfp_set_vp_flag(1);
                apps_config_set_voice(VP_INDEX_INCOMING_CALL, true, 600, VOICE_PROMPT_PRIO_ULTRA, true, true, NULL);
            }
            break;
        }
        default:{
            APPS_LOG_MSGID_I(APP_HFP_ACTI", hfp_event_proc:Don't care", 0);
            ret = false;
            break;
        }
    }
    APPS_LOG_MSGID_I(APP_HFP_ACTI", hfp_event_proc:ret = %x", 1, ret);
    return ret;
}

static bool app_hfp_bt_cm_event_proc(ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
    switch (event_id) {
        case BT_CM_EVENT_REMOTE_INFO_UPDATE:
        {
            bt_cm_remote_info_update_ind_t *remote_update = (bt_cm_remote_info_update_ind_t *)extra_data;
            if (remote_update
                    && remote_update->pre_connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP)
                    && !(remote_update->connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP))) {
                APPS_LOG_MSGID_I(APP_HFP_ACTI", hfp_event_proc:Don't care", 0);
                apps_config_stop_voice(true, 200, true);
                app_bt_hfp_set_vp_flag(0);
                ui_shell_finish_activity(self, self);
            }
        }
            break;
        default:
            break;

    }
    return ret;
}

bool ui_shell_ut_hfp_activity_proc(
            struct _ui_shell_activity *self,
            uint32_t event_group,
            uint32_t event_id,
            void *extra_data,
            size_t data_len)
{
    bool ret = false;

    APPS_LOG_MSGID_I(APP_HFP_ACTI", event_group : %d, id : %x", 2, event_group, event_id);
    
    switch (event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM: {
            
            ret = _proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_KEY: {
            
            ret = _proc_key_event_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_SINK: {
            
            ret = ui_shell_bt_hfp_event_proc(self, event_group, event_id, extra_data, data_len);

            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_CONN_MANAGER:
            ret = app_hfp_bt_cm_event_proc(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_APP_INTERACTION:
        {
            apps_config_state_t app_state;
            hfp_context_t *hfp_context = (hfp_context_t *)(self->local_context);
            
            APPS_LOG_MSGID_I(APP_HFP_ACTI",APP_INTERACTION: event_id : %d", 1, event_id);
            if (hfp_context) {
                app_state = app_get_config_status_by_state(hfp_context->curr_state);
                if (event_id == APPS_EVENTS_INTERACTION_UPDATE_LED_BG_PATTERN) {
                    APPS_LOG_MSGID_I(APP_HFP_ACTI",APP_INTERACTION, state : %d, app_state = %d", 2, hfp_context->curr_state, app_state);
                    ret = true;
                    switch (hfp_context->curr_state)
                    {
                        case BT_SINK_SRV_STATE_INCOMING: {
                            APPS_LOG_MSGID_I("incoming", 0);
                            apps_config_set_background_led_pattern(LED_INDEX_INCOMING_CALL, true, APPS_CONFIG_LED_AWS_SYNC_PRIO_MIDDLE);
                            break;
                        }
                        case BT_SINK_SRV_STATE_OUTGOING: {
                            APPS_LOG_MSGID_I("outgoing", 0);
                            apps_config_set_background_led_pattern(LED_INDEX_OUTGOING_CALL, true, APPS_CONFIG_LED_AWS_SYNC_PRIO_MIDDLE);
                            break;
                        }
                        case BT_SINK_SRV_STATE_ACTIVE:
                        {
                            APPS_LOG_MSGID_I(APP_HFP_ACTI", active", 0, APP_HFP_UTILS);
                            apps_config_set_background_led_pattern(LED_INDEX_CALL_ACTIVE, true, APPS_CONFIG_LED_AWS_SYNC_PRIO_MIDDLE);
                            break;
                        }
                        case BT_SINK_SRV_STATE_HELD_REMAINING:
                        {
                            APPS_LOG_MSGID_I(APP_HFP_ACTI", HELD_REMAINING", 0, APP_HFP_UTILS);
                            apps_config_set_background_led_pattern(LED_INDEX_HOLD_CALL, true, APPS_CONFIG_LED_AWS_SYNC_PRIO_MIDDLE);
                            break;
                        }
                        case BT_SINK_SRV_STATE_HELD_ACTIVE:
                        {
                            APPS_LOG_MSGID_I(APP_HFP_ACTI", HELD_ACTIVE", 0, APP_HFP_UTILS);
                            apps_config_set_background_led_pattern(LED_INDEX_CALL_ACTIVE, true, APPS_CONFIG_LED_AWS_SYNC_PRIO_MIDDLE);
                            break;
                        }
                        case BT_SINK_SRV_STATE_TWC_INCOMING:
                        {
                            APPS_LOG_MSGID_I(APP_HFP_ACTI", twc_incoming", 0, APP_HFP_UTILS);
                            apps_config_set_background_led_pattern(LED_INDEX_INCOMING_CALL, true, APPS_CONFIG_LED_AWS_SYNC_PRIO_MIDDLE);
                            break;
                        }
                        case BT_SINK_SRV_STATE_TWC_OUTGOING:
                        {
                            APPS_LOG_MSGID_I(APP_HFP_ACTI", outgoing", 0, APP_HFP_UTILS);
                            apps_config_set_background_led_pattern(LED_INDEX_OUTGOING_CALL, true, APPS_CONFIG_LED_AWS_SYNC_PRIO_MIDDLE);
                            break;
                        }
                        case BT_SINK_SRV_STATE_MULTIPARTY: {/**< There is a conference call. */
                            APPS_LOG_MSGID_I("multiparty", 0);
                            apps_config_set_background_led_pattern(LED_INDEX_CALL_ACTIVE, true, APPS_CONFIG_LED_AWS_SYNC_PRIO_MIDDLE);
                            break;
                        }
                        default:
                        {
                            ret = false;
                            break;
                        }
                    }

                } else if (APPS_EVENTS_INTERACTION_UPDATE_MMI_STATE == event_id) {
                    if (APP_TOTAL_STATE_NO != app_state) {
                        apps_config_key_set_mmi_state(app_state);
                        ret = true;
                    } else {
                        ret = false;
                    }
                }
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
                else if (APPS_EVENTS_INTERACTION_RHO_END == event_id || APPS_EVENTS_INTERACTION_PARTNER_SWITCH_TO_AGENT == event_id) {
                    app_rho_result_t rho_ret = (app_rho_result_t)extra_data;

                    APPS_LOG_MSGID_I(APP_HFP_ACTI", RHO result : 0x%x, cur_state : 0x%x, pre_state : 0x%x", 3, rho_ret, hfp_context->curr_state, hfp_context->pre_state);
                    uint8_t flag = app_bt_hfp_get_vp_flag();
                    if (!flag){
                        break;
                    }
                    if (APP_RHO_RESULT_SUCCESS == rho_ret) {
                        if (BT_SINK_SRV_STATE_INCOMING == hfp_context->curr_state && hfp_context->pre_state != BT_SINK_SRV_STATE_TWC_INCOMING) {
                            bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
                            APPS_LOG_MSGID_I(APP_HFP_ACTI", curr RHO: 0x%x", 1, role);
                            if (BT_AWS_MCE_ROLE_AGENT == role) {
                                apps_config_set_voice(VP_INDEX_INCOMING_CALL, true, 600, VOICE_PROMPT_PRIO_ULTRA, true, true, NULL);
                            } else if (BT_AWS_MCE_ROLE_PARTNER == role){
                                apps_config_set_voice(VP_INDEX_INCOMING_CALL, false, 600, VOICE_PROMPT_PRIO_ULTRA, true, false, NULL);
                            }
                        }
                     }
                 }
#endif
            }
            break;
        }
        default:
            break;
    }
    APPS_LOG_MSGID_I(APP_HFP_ACTI", connected_activity_proc: ret = %d", 1, ret);
    return ret;
}

