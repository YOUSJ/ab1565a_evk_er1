/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "app_hfp_idle_activity.h"
#include "apps_config_event_list.h"
#include "apps_aws_sync_event.h"
#include "battery_management.h"
#include "battery_management_core.h"
#include "apps_events_battery_event.h"
#include "apps_events_event_group.h"
#include "apps_config_key_remapper.h"
#include "apps_events_key_event.h"
#include "bt_device_manager.h"

#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
#include "app_rho_idle_activity.h"
#endif

#ifdef MTK_AWS_MCE_ENABLE
#include "bt_aws_mce_report.h"
#endif

#ifdef MTK_AWS_MCE_ENABLE
bool app_hfp_aws_data_proc(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    bt_aws_mce_report_info_t *aws_data_ind = (bt_aws_mce_report_info_t *)extra_data;
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
    if (aws_data_ind->module_id == BT_AWS_MCE_REPORT_MODULE_APP_ACTION) {
        uint32_t event_group;
        uint32_t action;

        apps_aws_sync_event_decode(aws_data_ind, &event_group, &action);
        APPS_LOG_MSGID_I("Received action = %x to paly reject call, role = %x, event_group = %x", 3, action, role, event_group);
        if (event_group == EVENT_GROUP_UI_SHELL_KEY && action == KEY_REJCALL) {

            if (role == BT_AWS_MCE_ROLE_AGENT) 
            {

                apps_config_set_vp(VP_INDEX_CALL_REJECTED, true, 200, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
            }
        } else if (event_group == EVENT_GROUP_UI_SHELL_KEY && action == KEY_WAKE_UP_VOICE_ASSISTANT) {
            if (role == BT_AWS_MCE_ROLE_AGENT)
            {
                hfp_context_t *local_context = (hfp_context_t*)self->local_context;
                bool active = !local_context->voice_assistant;
                if (BT_STATUS_SUCCESS == bt_sink_srv_send_action(BT_SINK_SRV_ACTION_VOICE_RECOGNITION_ACTIVATE, &active)) {
                    local_context->voice_assistant = active;
                } else {
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", Agent send BT_SINK_SRV_ACTION_VOICE_RECOGNITION_ACTIVATE fail", 0);
                }
            }
        }
    }

    return ret;
}
#endif

static void app_report_battery_to_remote(int32_t bat_val, int32_t pre_val)
{
    bat_val = bat_val / 10; /*% need transfer, bt sink action level is 0~9*/
    if (bat_val == 10) { /*bal_val is 100*/
        bat_val = 9;
    }

    pre_val = pre_val /10;
    if (pre_val == 10) { /*bal_val is 100*/
        pre_val = 9;
    }
    
    APPS_LOG_MSGID_I(APP_HFP_ACTI", bat_val : %d, pre_val : %d", 2, bat_val, pre_val);
    if (pre_val != bat_val) {
        bt_status_t status = bt_sink_srv_send_action(BT_SINK_SRV_ACTION_REPORT_BATTERY, &bat_val);//BT_SINK_SRV_ACTION_REPORT_BATTERY
        APPS_LOG_MSGID_I(APP_HFP_ACTI", status : %x", 1, status);
    }
}

bool app_start_hfp_activity(ui_shell_activity_t *self, uint32_t event_id, void *extra_data, size_t data_len)
{
    bool ret = false;
    bt_sink_srv_state_change_t *param = (bt_sink_srv_state_change_t*)extra_data;
    if (param == NULL) {
        return ret;
    }
    APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", hfp_state: param->now = %x, param->pre = %x", 2, param->current, param->previous);
    
    hfp_context_t *hfp_context = (hfp_context_t*)self->local_context;
    if (hfp_context == NULL) {
        return ret;
    }
    
    hfp_context->pre_state = param->previous;
    hfp_context->curr_state = param->current;
    
    apps_config_state_t pre_state = app_get_config_status_by_state(param->previous);
    apps_config_state_t state = app_get_config_status_by_state(param->current);

    APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", hfp_activity: state = %x", 1, state);
    if (APP_TOTAL_STATE_NO == pre_state && state != APP_TOTAL_STATE_NO) {
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", hfp_activity: pre_state = %x, curr_state = %x", 2, hfp_context->pre_state, hfp_context->curr_state);
        ui_shell_start_activity(NULL, ui_shell_ut_hfp_activity_proc, ACTIVITY_PRIORITY_MIDDLE, (void*)hfp_context, 0);
    }
    return false;
}


static bool _proc_ui_shell_group(ui_shell_activity_t *self, uint32_t event_id, void *extra_data,size_t data_len)
{
    bool ret = true; // UI shell internal event must process by this activity, so default is true
    switch (event_id) {
        case EVENT_ID_SHELL_SYSTEM_ON_CREATE: {
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", create", 0);
            self->local_context = pvPortMalloc(sizeof(hfp_context_t));
            if(self->local_context){
                memset(self->local_context, 0, sizeof(hfp_context_t));
                hfp_context_t *local_context = (hfp_context_t*)self->local_context;
                local_context->esco_connected = false;
            }
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_DESTROY: {
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", destroy", 0);
             if (self->local_context) {
                vPortFree(self->local_context);
            }
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESUME: {
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", resume", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_PAUSE: {
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", pause", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_REFRESH: {
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", refresh", 0);
            break;
        }
        case EVENT_ID_SHELL_SYSTEM_ON_RESULT: {
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", result", 0);
            break;
        }
        default:
            break;
    }
    return ret;
}


static bool _proc_key_event_group(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;

    hfp_context_t *local_context = (hfp_context_t*)self->local_context;
    uint8_t key_id;
    airo_key_event_t key_event;

    app_event_key_event_decode(&key_id, &key_event, event_id);

    apps_config_key_action_t action;
    if (extra_data) {
        action = *(uint16_t *)extra_data;
    } else {
        action = apps_config_key_event_remapper_map_action(key_id, key_event);
    }

    switch (action) {
    case KEY_WAKE_UP_VOICE_ASSISTANT_NOTIFY: /* Only notify user long press time is up */
        apps_config_set_vp(VP_INDEX_PRESS, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
        ret = true;
        break;
    case KEY_WAKE_UP_VOICE_ASSISTANT:
    case KEY_WAKE_UP_VOICE_ASSISTANT_CONFIRM: /* If user release the key after long press, trigger voice assistant */
#ifdef MTK_AWS_MCE_ENABLE
        if (bt_device_manager_aws_local_info_get_role() == BT_AWS_MCE_ROLE_PARTNER) {
            if (BT_STATUS_SUCCESS != apps_aws_sync_event_send(EVENT_GROUP_UI_SHELL_KEY, KEY_WAKE_UP_VOICE_ASSISTANT)) {
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", Partner send KEY_WAKE_UP_VOICE_ASSISTANT aws to agent failed", 0);
                if (KEY_WAKE_UP_VOICE_ASSISTANT == action) {
                    apps_config_set_vp(VP_INDEX_FAILED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                }
            } else {
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", Partner send KEY_WAKE_UP_VOICE_ASSISTANT aws to agent success", 0);
                if (KEY_WAKE_UP_VOICE_ASSISTANT == action) {
                    apps_config_set_vp(VP_INDEX_PRESS, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
                }
            }
        } else
#endif
        {
            bool active = !local_context->voice_assistant;
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", Agent set voice_recognition to %d", 1, active);
            if (BT_STATUS_SUCCESS == bt_sink_srv_send_action(BT_SINK_SRV_ACTION_VOICE_RECOGNITION_ACTIVATE, &active)) {
                local_context->voice_assistant = active;
            } else {
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", Agent send BT_SINK_SRV_ACTION_VOICE_RECOGNITION_ACTIVATE fail", 0);
            }
            if (KEY_WAKE_UP_VOICE_ASSISTANT == action) {
                apps_config_set_vp(VP_INDEX_PRESS, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
            }
        }
        ret = true;
        break;
    case KEY_REDIAL_LAST_CALL:
        APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", receive KEY_REDIAL_LAST_CALL", 0);
        apps_config_set_vp(VP_INDEX_SUCCESSED, false, 0, VOICE_PROMPT_PRIO_MEDIUM, false, NULL);
        bt_sink_srv_send_action(BT_SINK_SRV_ACTION_DIAL_LAST, NULL);
        ret = true;
        break;
    case KEY_VOICE_UP:
        if (local_context->esco_connected) {
            bt_sink_srv_send_action(BT_SINK_SRV_ACTION_CALL_VOLUME_UP, NULL);
        }
        break;
    case KEY_VOICE_DN:
        if (local_context->esco_connected) {
            bt_sink_srv_send_action(BT_SINK_SRV_ACTION_CALL_VOLUME_DOWN, NULL);
        }
        break;
    }

    return ret;
}

static bool app_hfp_idle_proc_bt_cm_event_group(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len)
{
    bool ret = false;
#ifdef MTK_AWS_MCE_ENABLE
    bt_aws_mce_role_t role = bt_device_manager_aws_local_info_get_role();
#endif

    switch(event_id) {
        case BT_CM_EVENT_REMOTE_INFO_UPDATE:
        {
            bt_cm_remote_info_update_ind_t *remote_update = (bt_cm_remote_info_update_ind_t *)extra_data;
            hfp_context_t *hfp_context = (hfp_context_t*)self->local_context;
            if (remote_update && hfp_context) {
                if (!(remote_update->pre_connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP))
                        && remote_update->connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP)) {
                    int32_t bat_val = battery_management_get_battery_property(BATTERY_PROPERTY_CAPACITY);
                    hfp_context->battery_level = bat_val;
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", bat_val: %d", 1, bat_val);
                    app_report_battery_to_remote(hfp_context->battery_level, 0xFFFF);
                } else if (remote_update->pre_connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP)
                        && !(remote_update->connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_HFP))) {
                    hfp_context->voice_assistant = false;
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", Set voice_recognition to false when HFP disconnected", 0);
                }
#ifdef MTK_AWS_MCE_ENABLE
                if (BT_AWS_MCE_ROLE_PARTNER == role || BT_AWS_MCE_ROLE_CLINET == role) {
                    if (remote_update->pre_connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS)
                        && !(remote_update->connected_service & BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS))) {
                            hfp_context->voice_assistant = false;
                            APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", Partner disconnect", 0);
                        }
                }
#endif
            }
        }
            break;
        default:
            break;
    }

    return ret;
}

bool app_hfp_idle_activity_proc(
            struct _ui_shell_activity *self,
            uint32_t event_group,
            uint32_t event_id,
            void *extra_data,
            size_t data_len)
{
    bool ret = false;
    APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", event_group : %x, id : %x", 2, event_group, event_id);
    switch (event_group) {
        case EVENT_GROUP_UI_SHELL_SYSTEM: {
            ret = _proc_ui_shell_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_KEY: {
            ret = _proc_key_event_group(self, event_id, extra_data, data_len);
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_SINK: {
            APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", BT_SINK : %x", 1, event_id);
            if (BT_SINK_SRV_EVENT_HF_VOICE_RECOGNITION_CHANGED == event_id) {
                hfp_context_t *hfp_context = (hfp_context_t*)self->local_context;
                bt_sink_srv_event_param_t *event = (bt_sink_srv_event_param_t *)extra_data;
                hfp_context->voice_assistant = event->voice_recognition.enable;
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", voice_recognition changed : %d", 1, hfp_context->voice_assistant);
            } else if(BT_SINK_SRV_EVENT_STATE_CHANGE == event_id) {
                ret = app_start_hfp_activity(self, event_id, extra_data, data_len);
                bt_sink_srv_state_change_t *param = (bt_sink_srv_state_change_t*)extra_data;
                if (param->current <= BT_SINK_SRV_STATE_POWER_ON && param->previous >= BT_SINK_SRV_STATE_CONNECTED) {
                    hfp_context_t *hfp_context = (hfp_context_t*)self->local_context;
                    hfp_context->voice_assistant = false;
                    APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", Set voice_recognition to false when BT sink state = %d", 1, param->current);
                }
            } else if (BT_SINK_SRV_EVENT_HF_SCO_STATE_UPDATE == event_id) {
                APPS_LOG_MSGID_I(APP_HFP_ACTI" SCO_STATE", 0);
                bt_sink_srv_sco_state_update_t *sco_state = (bt_sink_srv_sco_state_update_t *)extra_data;
                if (sco_state) {
                    hfp_context_t *ctx = (hfp_context_t *)(self->local_context);
                    ctx->esco_connected = sco_state->state == BT_SINK_SRV_SCO_CONNECTION_STATE_CONNECTED ? true : false;
                    APPS_LOG_MSGID_I(APP_HFP_ACTI", SCO_STATE: %d", 1, sco_state->state);
                }
            }
            break;
        }
        case EVENT_GROUP_UI_SHELL_BT_CONN_MANAGER:
            ret = app_hfp_idle_proc_bt_cm_event_group(self, event_id, extra_data, data_len);
            break;
        case EVENT_GROUP_UI_SHELL_BATTERY: {
            if (event_id ==  APPS_EVENTS_BATTERY_PERCENT_CHANGE) {
                hfp_context_t *hfp_context = (hfp_context_t*)self->local_context;
                if (hfp_context == NULL) {
                    return ret;
                }
                int32_t pre_battery = hfp_context->battery_level;
                int32_t curr_battery = (int32_t)extra_data;
                APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", pre_bat: %d, curr_bat = %d, cur_state = %x", 3, pre_battery, curr_battery, hfp_context->curr_state);
                if (hfp_context->curr_state >= BT_SINK_SRV_STATE_CONNECTED && curr_battery != pre_battery) {
                    app_report_battery_to_remote(curr_battery, pre_battery);
                    hfp_context->battery_level = curr_battery;
                }
            }
            break;
        }

        case EVENT_GROUP_UI_SHELL_APP_INTERACTION: {
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
            if (APPS_EVENTS_INTERACTION_RHO_END == event_id || APPS_EVENTS_INTERACTION_PARTNER_SWITCH_TO_AGENT == event_id) {
                app_rho_result_t rho_ret = (app_rho_result_t)extra_data;
                hfp_context_t *hfp_context = (hfp_context_t*)self->local_context;
                int32_t bat_val = battery_management_get_battery_property(BATTERY_PROPERTY_CAPACITY);
                hfp_context->battery_level = bat_val;
                if (APP_RHO_RESULT_SUCCESS == rho_ret) {
                    bt_aws_mce_role_t role = bt_connection_manager_device_local_info_get_aws_role();
                    if (BT_AWS_MCE_ROLE_AGENT == role) {
                        app_report_battery_to_remote(hfp_context->battery_level, 0xFFFF);
                    }
                }
            }
#endif
            break;
        }

#if defined(MTK_AWS_MCE_ENABLE)
        case EVENT_GROUP_UI_SHELL_AWS_DATA:
            ret = app_hfp_aws_data_proc(self, event_id, extra_data, data_len);
            break;
#endif
        default:
            break;
    }

    APPS_LOG_MSGID_I(UI_SHELL_IDLE_HFP_ACTIVITY", ret : %x", 1, ret);
    return ret;
}

