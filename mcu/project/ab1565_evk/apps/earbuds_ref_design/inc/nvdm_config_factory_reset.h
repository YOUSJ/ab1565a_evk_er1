/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __NVDM_CONFIG_FACTORY_RST_H__
#define __NVDM_CONFIG_FACTORY_RST_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifdef MTK_NVDM_ENABLE

#define FACTORY_RESET_FLAG      (0x55)
#define FACTORY_RESET_LINK_KEY  (0x56)

/*
 * 1. The user should put the reserved NVDM items in factory reset here with group name and item name.
 * 2. Those reserved NVDM items should be reviewed carefully before put here.
 */
#define FACTORY_RST_RESERVED_NVDM_ITEM_LIST \
{                                    \
    /* NVDM internal use */ \
    {"nvdm", "user_setting"},\
    /* BT relative */        \
    {"BT", "local_addr"}, \
    {"BT_APP", "fix_addr"},  \
    {"BT_APP", "name"}, \
    {"context_info", "index"}, \
    {"context_info", "info_0"}, \
    {"context_info", "info_1"}, \
    {"context_info", "info_2"}, \
    {"hci_log_switch_group", "hci_log_switch_item"}, \
    {"port_service", "port_assign"}, \
    {"port_service", "port_config"}, \
    /* NVDM version */ \
    {"AB15", "1001"}, \
    {"AB15", "1814"}, \
    /* FT FW K */ \
    {"AB15", "2000"}, \
    {"AB15", "200C"}, \
    {"AB15", "2002"}, \
    {"AB15", "2003"}, \
    {"AB15", "2004"}, \
    {"AB15", "2005"}, \
    {"AB15", "2006"}, \
    {"AB15", "2007"}, \
    {"AB15", "2008"}, \
    {"AB15", "2009"}, \
    {"AB15", "200A"}, \
    {"AB15", "200B"}, \
    {"AB15", "2010"}, \
    {"AB15", "2011"}, \
    {"AB15", "2012"}, \
    {"AB15", "2013"}, \
    {"AB15", "2014"}, \
    {"AB15", "2015"}, \
    {"AB15", "2016"}, \
    {"AB15", "2017"}, \
    {"AB15", "2018"}, \
    {"AB15", "2019"}, \
    {"AB15", "201A"}, \
    {"AB15", "201B"}, \
    {"AB15", "201C"}, \
    {"AB15", "201D"}, \
    {"AB15", "201E"}, \
    {"AB15", "2020"}, \
    {"AB15", "2021"}, \
    /* Battery related */ \
    {"AB15", "2001"}, \
    {"AB15", "2022"}, \
    /* MP_CAL_XO_26M */ \
    {"AB15", "2045"}, \
    /* BLE device name */ \
    {"AB15", "3901"}, \
    /* Key timing configuration */  \
    {"AB15", "D000"}, \
    {"AB15", "D001"}, \
    {"AB15", "D002"}, \
    {"AB15", "D003"}, \
    {"AB15", "D004"}, \
    {"AB15", "D005"}, \
    {"AB15", "D006"}, \
    {"AB15", "D007"}, \
    {"AB15", "D008"}, \
    {"AB15", "D009"}, \
    {"AB15", "D00A"}, \
    {"AB15", "D00B"}, \
    {"AB15", "D00C"}, \
    {"AB15", "D00D"}, \
    {"AB15", "D00E"}, \
    {"AB15", "D00F"}, \
    /* Captouch calibration data */  \
    {"AB15", "D100"}, \
    {"AB15", "E001"}, \
    /* Mix ratio of DL1 and DL2 parameters */ \
    {"AB15", "E005"}, \
    /* Used for DSP audio driver parmeter (Gain Table) */  \
    {"AB15", "E010"}, \
    {"AB15", "E011"}, \
    {"AB15", "E012"}, \
    {"AB15", "E013"}, \
    {"AB15", "E014"}, \
    {"AB15", "E015"}, \
    {"AB15", "E016"}, \
    {"AB15", "E017"}, \
    {"AB15", "E020"}, \
    {"AB15", "E021"}, \
    {"AB15", "E022"}, \
    {"AB15", "E023"}, \
    {"AB15", "E024"}, \
    {"AB15", "E025"}, \
    {"AB15", "E026"}, \
    {"AB15", "E027"}, \
    {"AB15", "E030"}, \
    {"AB15", "E031"}, \
    {"AB15", "E032"}, \
    {"AB15", "E033"}, \
    {"AB15", "E034"}, \
    {"AB15", "E035"}, \
    {"AB15", "E036"}, \
    {"AB15", "E040"}, \
    {"AB15", "E041"}, \
    {"AB15", "E042"}, \
    {"AB15", "E043"}, \
    {"AB15", "E044"}, \
    {"AB15", "E045"}, \
    {"AB15", "E046"}, \
    {"AB15", "E047"}, \
    {"AB15", "E050"}, \
    {"AB15", "E051"}, \
    {"AB15", "E052"}, \
    {"AB15", "E056"}, \
    {"AB15", "E060"}, \
    {"AB15", "E061"}, \
    {"AB15", "E062"}, \
    {"AB15", "E063"}, \
    {"AB15", "E064"}, \
    {"AB15", "E065"}, \
    {"AB15", "E066"}, \
    {"AB15", "E100"}, \
    /* eSCO DSP Algorithm Parameters*/ \
    {"AB15", "E101"}, \
    {"AB15", "E102"}, \
    {"AB15", "E103"}, \
    {"AB15", "E104"}, \
    {"AB15", "E105"}, \
    {"AB15", "E106"}, \
    {"AB15", "E107"}, \
    {"AB15", "E109"}, \
    {"AB15", "E110"}, \
    {"AB15", "E120"}, \
    {"AB15", "E130"}, \
    {"AB15", "E140"}, \
    {"AB15", "E150"}, \
    {"AB15", "E161"}, \
    {"AB15", "E162"}, \
    {"AB15", "E163"}, \
    {"AB15", "E164"}, \
    {"AB15", "E165"}, \
    {"AB15", "E166"}, \
    {"AB15", "E167"}, \
    {"AB15", "E168"}, \
    /* Used for ANC parameters */ \
    {"AB15", "E180"}, \
    {"AB15", "E181"}, \
    {"AB15", "E182"}, \
    {"AB15", "E183"}, \
    {"AB15", "E184"}, \
    {"AB15", "E185"}, \
    {"AB15", "E186"}, \
    {"AB15", "E187"}, \
    {"AB15", "E188"}, \
    {"AB15", "E189"}, \
    {"AB15", "E18A"}, \
    {"AB15", "E18B"}, \
    {"AB15", "E18C"}, \
    {"AB15", "E18D"}, \
    {"AB15", "E18E"}, \
    {"AB15", "E1B8"}, \
    {"AB15", "E1B9"}, \
    {"AB15", "E1BA"}, \
    {"AB15", "E1BB"}, \
    /* Used for Config Tool paramater(PEQ) */ \
    {"AB15", "EE05"}, \
    {"AB15", "EE06"}, \
    {"AB15", "EE07"}, \
    {"AB15", "EE08"}, \
    {"AB15", "EE09"}, \
    {"AB15", "EE0A"}, \
    {"AB15", "EE0B"}, \
    {"AB15", "EE0C"}, \
    {"AB15", "EE0D"}, \
    {"AB15", "EE0E"}, \
    {"AB15", "EE0F"}, \
    {"AB15", "EE13"}, \
    {"AB15", "EE14"}, \
    {"AB15", "EE15"}, \
    {"AB15", "EE16"}, \
    {"AB15", "EE17"}, \
    {"AB15", "EE18"}, \
    {"AB15", "EE19"}, \
    {"AB15", "EE1A"}, \
    {"AB15", "EE1B"}, \
    {"AB15", "EE1C"}, \
    {"AB15", "EE1D"}, \
    {"AB15", "EE1E"}, \
    {"AB15", "EE1F"}, \
    {"AB15", "EE21"}, \
    {"AB15", "EE22"}, \
    {"AB15", "EE23"}, \
    {"AB15", "EE24"}, \
    /* Music Latency Setting*/ \
    {"AB15", "E004"}, \
    /* Exception dump mode */ \
	{"AB15", "F080"}, \
    /* BT name */ \
    {"AB15", "F202"}, \
    {"AB15", "F230"}, \
    /* Turn Key APP Use (PEQ) */  \
    {"AB15", "F232"}, \
    {"AB15", "F233"}, \
    {"AB15", "F234"}, \
    {"AB15", "F235"}, \
    {"AB15", "F236"}, \
    {"AB15", "F237"}, \
    {"AB15", "F238"}, \
    {"AB15", "F239"}, \
    /* Turn Key APP Use(Volumn) */ \
    {"AB15", "F23A"}, \
    {"AB15", "F23B"}, \
    {"AB15", "F23C"}, \
    {"AB15", "F23D"}, \
    {"AB15", "F23E"}, \
    {"AB15", "F23F"}, \
    {"AB15", "F260"}, \
    {"AB15", "F261"}, \
    {"AB15", "F262"}, \
    {"AB15", "F263"}, \
    {"AB15", "F264"}, \
    {"AB15", "F265"}, \
    {"AB15", "F266"}, \
    {"AB15", "F267"}, \
    {"AB15", "F268"}, \
    {"AB15", "F269"}, \
    {"AB15", "F26A"}, \
    {"AB15", "F26B"}, \
    {"AB15", "F26C"}, \
    {"AB15", "F26D"}, \
    {"AB15", "F26E"}, \
    {"AB15", "F26F"}, \
    {"AB15", "F270"}, \
    {"AB15", "F271"}, \
    {"AB15", "F272"}, \
    {"AB15", "F273"}, \
    {"AB15", "F274"}, \
    {"AB15", "F275"}, \
    {"AB15", "F276"}, \
    {"AB15", "F277"}, \
    {"AB15", "F278"}, \
    {"AB15", "F279"}, \
    {"AB15", "F27A"}, \
    {"AB15", "F27B"}, \
    /* LED pattern */ \
    {"AB15", "F280"}, \
    {"AB15", "F283"}, \
    /* Turn Key APP Use(HW I/O Configure)*/ \
    {"AB15", "F28F"}, \
    {"AB15", "F292"}, \
    {"AB15", "F293"}, \
    /* BT AWS config related */ \
    {"AB15", "F2B0"}, \
    /* Turn Key APP Use(Audio Channel)*/ \
    {"AB15", "F2B5"}, \
    {"AB15", "F2E1"}, \
    {"AB15", "F2E4"}, \
    /* Config a2dp 3M */ \
    {"AB15", "F2E6"}, \
    /* NVKEYID for AMA configuration */ \
    {"AB15", "F2E9"}, \
    {"AB15", "F2EB"}, \
    {"AB15", "F2EC"}, \
    {"AB15", "F300"}, \
    /*Fast pair configuration. */ \
    {"AB15", "F301"}, \
    {"AB15", "F302"}, \
    /* Gsound Info record */ \
    {"AB15", "F303"}, \
    /* Customer Information */ \
    {"AB15", "F500"}, \
};

void factory_rst_reserved_nvdm_item_list_check(void);


#endif

#ifdef __cplusplus
}
#endif

#endif /* __NVDM_CONFIG_FACTRST_H__ */
