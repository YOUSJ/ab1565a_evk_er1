/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __NVDM_CONFIG_H__
#define __NVDM_CONFIG_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifdef MTK_NVDM_ENABLE

/* This macro defines max count of data items */
#define NVDM_PORT_DAT_ITEM_COUNT (400)

/* This macro defines size of PEB, normally it is size of flash block */
#define NVDM_PORT_PEB_SIZE (4096)

/* This macro defines max size of data item during all user defined data items.
 * 1. Must not define it greater than 2048 bytes.
 * 2. Define it as smaller as possible to enhance the utilization rate of NVDM region.
 * 2. Try your best to store small data less than 256 bytes.
 */
#define NVDM_PORT_MAX_DATA_ITEM_SIZE (1024)

/* This macro defines start address and PEB count of the NVDM region */
#include "memory_map.h"
#define NVDM_PORT_REGION_ADDRESS    ROM_NVDM_BASE
#define NVDM_PORT_REGION_SIZE       ROM_NVDM_LENGTH

/* This macro defines max length of group name of data item */
#define NVDM_PORT_GROUP_NAME_MAX_LENGTH (16)

/* This macro defines max length of data item name of data item */
#define NVDM_PORT_DATA_ITEM_NAME_MAX_LENGTH (32)

#define FOTA_NVDM_ITEM_CLEAR_LIST   \
{                                       \
    {"features",    "AUTORHO"},\
    {"factrst",     "factrst_flag"},\
    {"fota",        "fota_mark"},\
    {"fota",        "ver"},\
    {"gsound_data", "data"},\
    {"AB15",        "F2E0"},\
    {"AB15",        "F2E2"},\
    {"AB15",        "F2E5"},\
};


#ifdef MTK_FOTA_ENABLE
#if 0
/*
 * 1. The user should put the reserved NVDM items here with group name and item name.
 * 2. Those reserved NVDM items should be reviewed carefully before put here.
 */
#define FOTA_RESERVED_NVDM_ITEM_LIST \
{                                    \
    /* NVDM internal use */ \
    {"nvdm", "user_setting"},\
    /* BT relative */        \
    {"BT", "local_addr"},  \
    {"BT", "music_volume"},  \
    {"BT_APP", "irk"},  \
    {"BT_APP", "address"},  \
    {"BT_APP", "fix_addr"},  \
    {"BT_APP", "name"},  \
    {"BT_DM", "bonded_info_00"},  \
    {"BT_DM", "bonded_info_01"},  \
    {"BT_DM", "bonded_info_02"},  \
    {"BT_DM", "bonded_info_03"},  \
    {"common", "audio_path"},  \
    {"context_info", "index"},  \
    {"context_info", "info_0"},  \
    {"context_info", "info_1"},  \
    {"context_info", "info_2"},  \
    {"AB15", "account_key_list"},  \
    {"GSOUND", "init_state"},  \
    {"hci_log_switch_group", "hci_log_switch_item"}, \
    {"speaker", "firmware_type"},  \
    /* Port service port setting */  \
    {"port_service", "port_assign"},  \
    {"port_service", "port_config"},  \
    {"SYSLOG", "cpu_filter"},  \
    {"SYSLOG", "dump"},  \
    {"SYSLOG", "module_filter"},  \
    /* NVDM version */ \
    {"AB15", "1001"}, \
    /* Paired list */   \
    {"AB15", "1800"}, \
    {"AB15", "1801"}, \
    {"AB15", "1802"}, \
    {"AB15", "1803"}, \
    {"AB15", "1804"}, \
    {"AB15", "1805"}, \
    {"AB15", "1806"}, \
    {"AB15", "1807"}, \
    {"AB15", "1808"}, \
    {"AB15", "1809"}, \
    {"AB15", "180A"}, \
    {"AB15", "180B"}, \
    {"AB15", "180C"}, \
    {"AB15", "180D"}, \
    {"AB15", "180E"}, \
    {"AB15", "180F"}, \
    {"AB15", "1810"}, \
    {"AB15", "1813"}, \
    {"AB15", "1814"}, \
    /* Battery related */ \
    {"AB15", "2001"}, \
    {"AB15", "2022"}, \
    /* MP_CAL_XO_26M */ \
    {"AB15", "2045"}, \
    /* BLE device name */ \
    {"AB15", "3901"}, \
    /* Key timing configuration */  \
    {"AB15", "D000"}, \
    {"AB15", "D001"}, \
    {"AB15", "D002"}, \
    {"AB15", "D003"}, \
    {"AB15", "D004"}, \
    {"AB15", "D005"}, \
    {"AB15", "D006"}, \
    {"AB15", "D007"}, \
    {"AB15", "D008"}, \
    {"AB15", "D009"}, \
    {"AB15", "D00A"}, \
    {"AB15", "D00B"}, \
    {"AB15", "D00C"}, \
    {"AB15", "D00D"}, \
    {"AB15", "D00E"}, \
    {"AB15", "D00F"}, \
    /* Captouch calibration data */  \
    {"AB15", "D100"}, \
    {"AB15", "E001"}, \
    {"AB15", "E004"}, \
    /* Mix ratio of DL1 and DL2 parameters */ \
    {"AB15", "E005"}, \
    /* Used for DSP audio driver parmeter (Gain Table) */  \
    {"AB15", "E010"}, \
    {"AB15", "E011"}, \
    {"AB15", "E012"}, \
    {"AB15", "E013"}, \
    {"AB15", "E014"}, \
    {"AB15", "E015"}, \
    {"AB15", "E016"}, \
    {"AB15", "E017"}, \
    {"AB15", "E018"}, \
    {"AB15", "E020"}, \
    {"AB15", "E021"}, \
    {"AB15", "E022"}, \
    {"AB15", "E023"}, \
    {"AB15", "E024"}, \
    {"AB15", "E025"}, \
    {"AB15", "E026"}, \
    {"AB15", "E027"}, \
    {"AB15", "E028"}, \
    {"AB15", "E030"}, \
    {"AB15", "E031"}, \
    {"AB15", "E032"}, \
    {"AB15", "E033"}, \
    {"AB15", "E034"}, \
    {"AB15", "E035"}, \
    {"AB15", "E036"}, \
    {"AB15", "E040"}, \
    {"AB15", "E041"}, \
    {"AB15", "E042"}, \
    {"AB15", "E043"}, \
    {"AB15", "E044"}, \
    {"AB15", "E045"}, \
    {"AB15", "E046"}, \
    {"AB15", "E047"}, \
    {"AB15", "E050"}, \
    {"AB15", "E051"}, \
    {"AB15", "E052"}, \
    {"AB15", "E056"}, \
    {"AB15", "E060"}, \
    {"AB15", "E061"}, \
    {"AB15", "E062"}, \
    {"AB15", "E063"}, \
    {"AB15", "E064"}, \
    {"AB15", "E065"}, \
    {"AB15", "E066"}, \
    {"AB15", "E100"}, \
    /* eSCO DSP Algorithm Parameters*/ \
    {"AB15", "E101"}, \
    {"AB15", "E102"}, \
    {"AB15", "E103"}, \
    {"AB15", "E104"}, \
    {"AB15", "E105"}, \
    {"AB15", "E106"}, \
    {"AB15", "E107"}, \
    {"AB15", "E109"}, \
    {"AB15", "E110"}, \
    {"AB15", "E120"}, \
    {"AB15", "E130"}, \
    {"AB15", "E140"}, \
    {"AB15", "E150"}, \
    {"AB15", "E161"}, \
    {"AB15", "E162"}, \
    {"AB15", "E163"}, \
    {"AB15", "E164"}, \
    {"AB15", "E165"}, \
    {"AB15", "E166"}, \
    {"AB15", "E167"}, \
    {"AB15", "E168"}, \
    /* Used for ANC parameters */ \
    {"AB15", "E180"}, \
    {"AB15", "E181"}, \
    {"AB15", "E182"}, \
    {"AB15", "E183"}, \
    {"AB15", "E184"}, \
    {"AB15", "E185"}, \
    {"AB15", "E186"}, \
    {"AB15", "E187"}, \
    {"AB15", "E188"}, \
    {"AB15", "E189"}, \
    {"AB15", "E18A"}, \
    {"AB15", "E18B"}, \
    {"AB15", "E18C"}, \
    {"AB15", "E18D"}, \
    {"AB15", "E18E"}, \
    {"AB15", "E1B8"}, \
    {"AB15", "E1B9"}, \
    {"AB15", "E1BA"}, \
    {"AB15", "E1BB"}, \
    /* Used for Config Tool paramater(PEQ) */ \
    {"AB15", "EE05"}, \
    {"AB15", "EE06"}, \
    {"AB15", "EE07"}, \
    {"AB15", "EE08"}, \
    {"AB15", "EE09"}, \
    {"AB15", "EE0A"}, \
    {"AB15", "EE0B"}, \
    {"AB15", "EE0C"}, \
    {"AB15", "EE0D"}, \
    {"AB15", "EE0E"}, \
    {"AB15", "EE0F"}, \
    {"AB15", "EE13"}, \
    {"AB15", "EE14"}, \
    {"AB15", "EE15"}, \
    {"AB15", "EE16"}, \
    {"AB15", "EE17"}, \
    {"AB15", "EE18"}, \
    {"AB15", "EE19"}, \
    {"AB15", "EE1A"}, \
    {"AB15", "EE1B"}, \
    {"AB15", "EE1C"}, \
    {"AB15", "EE1D"}, \
    {"AB15", "EE1E"}, \
    {"AB15", "EE1F"}, \
    {"AB15", "EE21"}, \
    {"AB15", "EE22"}, \
    {"AB15", "EE23"}, \
    {"AB15", "EE24"}, \
    /* Used for APP parameter(PEQ) */ \
    {"AB15", "EF00"}, \
    {"AB15", "EF01"}, \
    {"AB15", "EF02"}, \
    {"AB15", "EF03"}, \
    /* BT name */ \
    {"AB15", "F202"}, \
    {"AB15", "F203"}, \
    {"AB15", "F230"}, \
    /* Turn Key APP Use (PEQ) */  \
    {"AB15", "F232"}, \
    {"AB15", "F233"}, \
    {"AB15", "F234"}, \
    {"AB15", "F235"}, \
    {"AB15", "F236"}, \
    {"AB15", "F237"}, \
    {"AB15", "F238"}, \
    {"AB15", "F239"}, \
    /* Turn Key APP Use(Volumn) */ \
    {"AB15", "F23A"}, \
    {"AB15", "F23B"}, \
    {"AB15", "F23C"}, \
    {"AB15", "F23D"}, \
    {"AB15", "F23E"}, \
    {"AB15", "F23F"}, \
    {"AB15", "F240"}, \
    {"AB15", "F260"}, \
    {"AB15", "F261"}, \
    {"AB15", "F262"}, \
    {"AB15", "F263"}, \
    {"AB15", "F264"}, \
    {"AB15", "F265"}, \
    {"AB15", "F266"}, \
    {"AB15", "F267"}, \
    {"AB15", "F268"}, \
    {"AB15", "F269"}, \
    {"AB15", "F26A"}, \
    {"AB15", "F26B"}, \
    {"AB15", "F26C"}, \
    {"AB15", "F26D"}, \
    {"AB15", "F26E"}, \
    {"AB15", "F26F"}, \
    {"AB15", "F270"}, \
    {"AB15", "F271"}, \
    {"AB15", "F272"}, \
    {"AB15", "F273"}, \
    {"AB15", "F274"}, \
    {"AB15", "F275"}, \
    {"AB15", "F276"}, \
    {"AB15", "F277"}, \
    {"AB15", "F278"}, \
    {"AB15", "F279"}, \
    {"AB15", "F27A"}, \
    {"AB15", "F27B"}, \
    {"AB15", "F27C"}, \
    {"AB15", "F27D"}, \
    {"AB15", "F27E"}, \
    {"AB15", "F27F"}, \
    /* LED pattern */ \
    {"AB15", "F280"}, \
    {"AB15", "F283"}, \
    /* Turn Key APP Use(HW I/O Configure)*/ \
    {"AB15", "F28F"}, \
    {"AB15", "F292"}, \
    {"AB15", "F293"}, \
    /* BT AWS config related */ \
    {"AB15", "F2B0"}, \
    /* Turn Key APP Use(Audio Channel)*/ \
    {"AB15", "F2B5"}, \
    /* NVKEYID_ENABLE_MP_TEST_MODE */ \
    {"AB15", "F2C4"}, \
    /* Voice prompt */ \
    {"AB15", "F2DA"}, \
    {"AB15", "F2E1"}, \
    /* BT DUT Test Mode enable */ \
    {"AB15", "F2E3"}, \
    {"AB15", "F2E4"}, \
    /* Config a2dp 3M */ \
    {"AB15", "F2E6"}, \
    /* Configurable key action mapping table */ \
    {"AB15", "F2E7"}, \
    /* NVKEYID for AMA Data variation */ \
    {"AB15", "F2E8"}, \
    /* NVKEYID for AMA configuration */ \
    {"AB15", "F2E9"}, \
    /* NVKEYID for VA switch */ \
    {"AB15", "F2EA"}, \
    {"AB15", "F2EB"}, \
    {"AB15", "F2EC"}, \
    /* NVKEYID for Xiaowei to store proper value */ \
    {"AB15", "F2ED"}, \
    {"AB15", "F2EE"}, \
    {"AB15", "F300"}, \
    /*Fast pair configuration. */ \
    {"AB15", "F301"}, \
    {"AB15", "F302"}, \
    /* Gsound Info record */ \
    {"AB15", "F303"}, \
    /*Easy pair configuration. */ \
    {"AB15", "F304"}, \
    /* Customer Information */ \
    {"AB15", "F500"}, \
};
#endif
/* This API is used to mark in NVDM when FOTA is begin to run, and is called in FOTA side. */
void reserved_nvdm_item_list_ask_check(void);
/*
 * This API is used to check whether all non-reserved nvdm items should be delete after FOTA done.
 * It should be called during system initial phase.
 */
void reserved_nvdm_item_list_check(void);
/*
 * This API is used to trigger user's customize modification for reserved item list in NVDM region after FOTA done.
 * The user should implement it when the reserved nvdm items is needed to be modified after FOTA.
 */
void reserved_nvdm_item_list_modify(void);

#endif

#endif

#ifdef __cplusplus
}
#endif

#endif /* __NVDM_CONFIG_H__ */
