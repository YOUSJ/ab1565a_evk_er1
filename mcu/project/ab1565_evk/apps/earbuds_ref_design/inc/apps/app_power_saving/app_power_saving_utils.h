/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __APP_POWER_SAVING_UTILS__
#define __APP_POWER_SAVING_UTILS__

#ifdef __cplusplus
extern "C" {
#endif

#include "ui_shell_activity.h"
#include "bt_sink_srv.h"

typedef enum {
    POWER_SAVING_STATE_IDLE = 0,
    POWER_SAVING_STATE_WAITING_TIMEOUT,
    POWER_SAVING_STATE_TIMEOUT,
} app_power_saving_state_t;

typedef enum {
    APP_POWER_SAVING_BT_OFF = 0,
    APP_POWER_SAVING_BT_DISCONNECTED,
    APP_POWER_SAVING_BT_CONNECTED,
} app_power_saving_bt_state_t;

typedef struct {
    app_power_saving_state_t app_state;
    app_power_saving_bt_state_t bt_sink_srv_state;
    bool chargering;
#ifdef MTK_ANC_ENABLE
    uint8_t anc_enable;
#endif
} app_power_saving_context_t;

typedef enum {
    APP_POWER_SAVING_TARGET_MODE_NORMAL = 0,
    APP_POWER_SAVING_TARGET_MODE_BT_OFF,
    APP_POWER_SAVING_TARGET_MODE_SYSTEM_OFF,
} app_power_saving_target_mode_t;

typedef enum {
    APP_POWER_SAVING_EVENT_TIMEOUT,
    APP_POWER_SAVING_EVENT_REFRESH_TIME,
    APP_POWER_SAVING_EVENT_NEED_DISABLE_BT,
    APP_POWER_SAVING_EVENT_NEED_SYSTEM_OFF,
    APP_POWER_SAVING_EVENT_NOTIFY_CHANGE,
} app_power_saving_event_t;

typedef app_power_saving_target_mode_t (*get_power_saving_target_mode_func_t)(void);

/* internal use */
bool app_power_saving_utils_add_new_callback_func(get_power_saving_target_mode_func_t callback_func);

app_power_saving_target_mode_t app_power_saving_utils_get_tagert_mode(
        ui_shell_activity_t *self);

bool app_power_saving_utils_notify_mode_changed(get_power_saving_target_mode_func_t callback_func);

bool app_power_saving_utils_register_get_mode_callback(get_power_saving_target_mode_func_t callback_func);

void app_power_saving_utils_update_bt_state(ui_shell_activity_t *self,
        uint32_t event_id, void *extra_data, size_t data_len);

#ifdef __cplusplus
            }
#endif

#endif /* __APP_POWER_SAVING_UTILS__ */
