/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __APP_SMCHARGER_UTILS_H__
#define __APP_SMCHARGER_UTILS_H__

#ifdef MTK_SMART_CHARGER_ENABLE

#include "ui_shell_manager.h"
#include "ui_shell_activity.h"
#include "apps_debug.h"
#include "FreeRTOS.h"
#include "apps_events_battery_event.h"
#include "apps_events_event_group.h"
#include "apps_events_interaction_event.h"
#include "apps_config_led_manager.h"
#include "apps_config_led_index_list.h"
#include "apps_config_vp_manager.h"
#include "apps_config_vp_index_list.h"
#include "bt_sink_srv.h"
#include "bt_power_on_config.h"
#include "bt_device_manager.h"

// SMCharger battery state
typedef enum {
    APP_BATTERY_STATE_SHUTDOWN,
    APP_BATTERY_STATE_LOW_CAP,
    APP_BATTERY_STATE_IDLE,
    APP_BATTERY_STATE_FULL,
    APP_BATTERY_STATE_CHARGING,
    APP_BATTERY_STATE_CHARGING_FULL,
    APP_BATTERY_STATE_THR
} app_battery_state_t;

// SMCharger State Machine
typedef enum {
    STATE_SMCHARGER_NONE = 0,
    STATE_SMCHARGER_STARTUP,
    STATE_SMCHARGER_LID_CLOSE,
    STATE_SMCHARGER_LID_OPEN,
    STATE_SMCHARGER_OUT_OF_CASE,
    STATE_SMCHARGER_OFF
} app_smcharger_state;

// SMCharger Special Flag (extra data)
#define SMCHARGER_BOOT_OUT_FLAG           0xFFF1

typedef struct {
    uint8_t battery_percent;
    uint8_t partner_battery_percent;
    int32_t charging_state;
    int32_t charger_exist_state;
    battery_event_shutdown_state_t shutdown_state;
    bt_aws_mce_agent_state_type_t aws_state;
    app_battery_state_t     battery_state;
    int                     smcharger_state;
    int                     partner_smcharger_state;
    int                     case_battery_percent;
    bool                    agent_prepare_rho_flag;
#if (defined(SUPPORT_ROLE_HANDOVER_SERVICE) && defined(MTK_FOTA_ENABLE)) || defined(MTK_VA_XIAOAI_ENABLE)
    bool                    fota_doing;
#endif
} app_smcharger_context_t;

// Public Event: SMCharger Notify app_smcharger_public_event_para_t to other APP
typedef enum {
    SMCHARGER_CHARGER_IN_ACTION = 0,
    SMCHARGER_CHARGER_COMPLETE_ACTION,
    SMCHARGER_OPEN_LID_ACTION,
    SMCHARGER_CLOSE_LID_ACTION,
    SMCHARGER_CLOSE_LID_COMPLETE_ACTION,
    SMCHARGER_CHARGER_OUT_ACTION,               // 5
    SMCHARGER_CHARGER_KEY_ACTION,
    SMCHARGER_USER_DATA1_ACTION,
    SMCHARGER_USER_DATA2_ACTION,
    SMCHARGER_USER_DATA3_ACTION,
    SMCHARGER_CASE_BATTERY_REPORT_ACTION        // 10
} app_smcharger_action_t;

typedef struct {
    app_smcharger_action_t action;
    uint8_t                data;
} app_smcharger_public_event_para_t;

// SMCharger Event
enum {
    EVENT_ID_SMCHARGER_NONE = 0,
    EVENT_ID_SMCHARGER_POWER_KEY_BOOT,
    EVENT_ID_SMCHARGER_CHARGER_IN_BOOT,
    EVENT_ID_SMCHARGER_CHARGER_IN_INTERRUPT,
    EVENT_ID_SMCHARGER_CHARGER_COMPLETE_INTERRUPT,  // Unused
    EVENT_ID_SMCHARGER_CHARGER_OUT_PATTERN,
    EVENT_ID_SMCHARGER_CHARGER_OFF_PATTERN,
    EVENT_ID_SMCHARGER_CHARGER_KEY_PATTERN,
    EVENT_ID_SMCHARGER_LID_CLOSE_PATTERN,
    EVENT_ID_SMCHARGER_LID_OPEN_PATTERN,
    EVENT_ID_SMCHARGER_USER_DATA1_PATTERN,
    EVENT_ID_SMCHARGER_USER_DATA2_PATTERN,
    EVENT_ID_SMCHARGER_USER_DATA3_PATTERN,
    EVENT_ID_SMCHARGER_BATTERY_LEVEL_REPORT,
    EVENT_ID_SMCHARGER_SYNC_STATE,
    EVENT_ID_SMCHARGER_PREPARE_RHO,
    // Public Event: SMCharger Notify app_smcharger_public_event_para_t to other APP
    // Non-SMCharger APP should only use public event
    EVENT_ID_SMCHARGER_NOTIFY_PUBLIC_EVENT
};

// SMCharger Status
typedef enum {
    APP_SMCHARGER_FAILURE = -1,
    APP_SMCHARGER_OK = 0,
} app_smcharger_action_status_t;

app_smcharger_action_status_t app_smcharger_power_off(bool normal_off);

app_smcharger_action_status_t app_smcharger_state_do_action(int state);

void app_smcharger_init();

void app_smcharger_set_context(app_smcharger_context_t *smcharger_ctx);
app_smcharger_context_t* app_smcharger_get_context();

bool app_smcharger_show_led_bg_pattern();

void app_smcharger_handle_battery_report(uint8_t battery_percent);
void app_smcharger_handle_key_event(int state, uint8_t key_value);
void app_smcharger_handle_user_data(int state, int event_id, uint8_t user_data);

#ifdef MTK_VA_XIAOAI_ENABLE
void app_smartcharger_update_bat();
void app_smartcharger_set_ota_state(bool ota_ongoing);
#endif

#endif
#endif
