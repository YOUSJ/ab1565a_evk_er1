/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */


#ifndef APP_VOICE_PROMPT_H
#define APP_VOICE_PROMPT_H

#include "bt_sink_srv.h"
#include "bt_sink_srv_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/********************************************************
 * Macro & Define
 *
 ********************************************************/

#define app_voice_prompt_report(_message,...)       LOG_I(VOICE_PROMPT_APP, (_message), ##__VA_ARGS__)
#define app_voice_prompt_msgid_report(_message,...) LOG_MSGID_I(VOICE_PROMPT_APP, _message, ##__VA_ARGS__)
#define app_voice_prompt_msgid_warning(_message,...) LOG_MSGID_W(VOICE_PROMPT_APP, _message, ##__VA_ARGS__)
#define app_voice_prompt_msgid_error(_message,...) LOG_MSGID_E(VOICE_PROMPT_APP, _message, ##__VA_ARGS__)

#define app_vp_assert configASSERT
#define VP_VOICE_DEFAULT_DELAY 600
#define VP_VOICE_SPECIAL_LINK_DELAY 60  //60ms delay for special link
//define lang code string and lang code id
#define VP_LANG_CODEC_EN_US 0x409
#define VP_LANG_CODEC_ZH_TW 0x404
#define VP_LANG_CODEC_FR_FR 0x40C
typedef uint16_t app_vp_lang_codec_t;

/********************************************************
 * Enum & Structures
 *
 ********************************************************/
/** @brief This enum define tone of voice prompt */
typedef enum {
    VOICE_PROMPT_TONE_POWER_ON,                   /**<  0  */
    VOICE_PROMPT_TONE_POWER_OFF,                 /**<  1  */
    VOICE_PROMPT_TONE_RESET_PAIRED_LIST,  /**< 2 */
    VOICE_PROMPT_TONE_PAIRING,                            /**<  3  */
    VOICE_PROMPT_TONE_CONNECTED,                  /**<  4  */
    VOICE_PROMPT_TONE_DISCONNECTED,           /**< 5 */
    VOICE_PROMPT_TONE_VOLUME_DOWM,     /**<  6 */
    VOICE_PROMPT_TONE_VOLUME_UP,     /**<  7 */
    VOICE_PROMPT_TONE_CALL_END,                       /**<  8*/
    VOICE_PROMPT_TONE_CALL_REFUSED,             /**<  9 */
    VOICE_PROMPT_TONE_CALL_REDIALING,           /**<  10 */
    VOICE_PROMPT_TONE_LOW_BATTERY,              /**<   11 */
    VOICE_PROMPT_TONE_BT_MUSIC,                      /**< 12 */
    VOICE_PROMPT_TONE_INCOMING_CALL,            /**< 13 */
    VOICE_PROMPT_TONE_INCOMING_CALL_ENDED,            /**< 14 */
    VOICE_PROMPT_TONE_NUM                   /**< The total number of voice prompt tone. */
} app_voice_prompt_tone_t;

typedef enum {
    VOICE_PROMPT_PRIO_LOW,    //common vp
    VOICE_PROMPT_PRIO_MEDIUM, //common vp
    VOICE_PROMPT_PRIO_HIGH,   //common vp
    VOICE_PROMPT_PRIO_ULTRA,  //voice boardcast & ringtone
    VOICE_PROMPT_PRIO_EXTREME,//power off vp
    VOICE_PROMPT_PRIO_TOTAL
}app_vp_prio_t;

typedef enum {
    VP_TYPE_VP=0,
    VP_TYPE_VB, //vioce boardcast
    VP_TYPE_RT, //ringtone
    VP_TYPE_NONE
}app_vp_type;

typedef enum {
    VP_ERR_CODE_SUCCESS=0,
    VP_ERR_CODE_FAIL,
    VP_ERR_CODE_FILE_NO_FOUND,
    VP_ERR_CODE_STOP,
    VP_ERR_CODE_PREEMPTED
}vp_err_code;

typedef enum {
    VP_BT_STATE_SNIFF,
    VP_BT_STATE_SNIFF_EXITING,
    VP_BT_STATE_ACTIVE,
}app_vp_bt_state;


typedef enum {
    VOICE_PROMPT_LIST_SUCCESS,                   /**<  0  */
    VOICE_PROMPT_LIST_FAIL,                 /**<  1  */
    VOICE_PROMPT_LIST_NUM                   /**< The total number of voice prompt tone. */
} app_voice_prompt_list_status_t;

typedef void (*app_vp_play_callback_t)(uint32_t idx, vp_err_code err);

typedef struct {
    const uint8_t *buf;
    uint32_t size;
}app_voice_prompt_buf_t;

typedef struct {
    uint32_t            play_index;
    bt_clock_t          play_time;
    app_vp_play_callback_t callback;
    uint16_t            count;
    app_vp_prio_t       level;
    app_vp_type         type;
    bool                isCleanup;
    bool                repeat;
} app_voice_prompt_play_sync_t;

typedef struct {
    uint16_t            id;
    app_vp_type         type;
    bool                preempted;
    bt_clock_t          stop_time;
} app_voice_prompt_stop_sync_t;

typedef struct {
    uint8_t lang_index;
} app_voice_prompt_lang_sync_t;


typedef struct {
    app_vp_prio_t level;
    app_vp_type   type;
    bool          need_sync;
    bool          repeat;
    uint16_t      id;
    uint32_t      index;
    uint32_t      delay_time;
    app_vp_play_callback_t callback;
}app_voice_prompt_list_data_t;

typedef app_voice_prompt_list_data_t DataType;

typedef struct node{
    DataType data;
    struct node* next;
}vp_node;

typedef struct{
    vp_node* front;
    vp_node* rear;
    uint16_t length;
}vp_queue;

#define APP_VOICE_PROMPT_QUEUE_MAX_LEN  10

#define APP_VOICE_PROMPT_SYNC_PLAY 0x01
#define APP_VOICE_PROMPT_SYNC_STOP 0x02
#define APP_VOICE_PROMPT_SYNC_LANG 0x03

/********************************************************
 * External Function
 *
 ********************************************************/
/**
* @brief      This function require play a vp, there will occur preemption according with priority, otherwire, the vp will queue after a vp who the priority the same with.
* @param[in]  tone_idx is a VP index which reqiere play, see apps_config_vp_index_list.h
* @param[in]  need_sync, it mean that vp need sync to partner at the same time
* @param[in]  delay_time, aftre the time, agent and partner play at the same time
* @param[in]  level, see app_vp_prio_t
* @param[in]  cleanup, if ture, will empty all queueing vp and reject any new vp from now on, this flag should use by power off only!!!
* @return     If the operation completed successfully, the return value is ID(should > 0) which use in stop procedure, otherwire 0 is play fail.
*/
uint16_t app_voice_prompt_play(uint32_t tone_idx, bool need_sync, uint32_t delay_time, app_vp_prio_t level, bool cleanup, app_vp_play_callback_t callback);

/**
* @brief      This function require stop a vp by idx, no matter the vp is playing or not,
* @param[in]  id, a id return by app_voice_prompt_play
* @param[in]  need_sync, it mean that vp need sync to partner at the same time
* @param[in]  delay_time, aftre the time, agent and partner play at the same time
* @return     If the operation completed successfully, return true, otherwise return false.
*/
bool app_voice_prompt_stop(uint16_t id, bool need_sync, uint32_t delay_time);

/**
* @brief      This function require play a voice number(incoming call number) or ringtone.
* @param[in]  tone_idx is a voice number or ringtone index which reqiere play, see apps_config_vp_index_list.h
* @param[in]  need_sync, it mean that vp need sync to partner at the same time
* @param[in]  delay_time, aftre the time, agent and partner play at the same time
* @param[in]  level, see app_vp_prio_t
* @param[in]  ringtone, requirement is ringtone if true and loop play, otherwire it is voice number
* @param[in]  repeat, ringtone or vocie boardcast need to repeat play or not
* @return     If the operation completed successfully, the return value is ID which use in stop procedure.
*/
uint16_t app_voice_prompt_voice_play(uint32_t tone_idx, bool need_sync, uint32_t delay_time, app_vp_prio_t level, bool ringtone, bool repeat, app_vp_play_callback_t callback);

/**
* @brief      This function require stop a voice number or ringtone,
* @param[in]  need_sync, it mean that vp need sync to partner at the same time
* @param[in]  delay_time, aftre the time, agent and partner play at the same time
* @param[in]  ringtone, stop ringtone if true, other stop current voice number and empty all voice number which are queuqing.
* @return     If the operation completed successfully, return true, otherwise return false.
*/
bool app_voice_prompt_voice_stop(bool need_sync, uint32_t delay_time, bool ringtone);

/**
* @brief      This function get current play type. vp vc or ringtone
* @return     ref VP_TYPE_VP, VP_TYPE_VOICE, VP_TYPE_RINGTONE or VP_TYPE_NONE
*/
app_vp_type app_voice_prompt_get_current_type();

/**
* @brief      This function get current play index
* @return     ref apps_config_vp_index_list.h, if fail, return 0xFFFF
*/
uint16_t app_voice_prompt_get_current_index();

/**
* @brief      This function get current play id,
* @return     a id for stop procedure, 0x0 for nothing playing
*/
uint16_t app_voice_prompt_get_current_id();

/**
* @brief      This function require set language for VP,
* @param[in]  langCodec, a language codec which want to set (app_vp_lang_codec_t), ref https://docs.microsoft.com/en-us/previous-versions/ms776294(v=vs.85)
* @param[in]  need_sync, it mean that vp need sync to partner at the same time
* @return     If the operation completed successfully, return true, otherwise return false.
*/
bool app_voice_prompt_setLangByLCID(app_vp_lang_codec_t langCodec, bool need_sync);


/**
* @brief      This function require set language for VP,
* @param[in]  lang index, a index in languange support array
* @param[in]  need_sync, it mean that vp need sync to partner at the same time
* @return     If the operation completed successfully, return true, otherwise return false.
*/
bool app_voice_prompt_setLang(uint8_t index, bool need_sync);

/**
* @brief      This function require get language idx for VP,
* @return     If the operation completed successfully, return language inedx, otherwise return 0xFF.
*/
uint8_t app_voice_prompt_getLang();

/**
* @brief      This function require get count which support,
* @return     If the operation completed successfully, return language count, otherwise return 0x0.
*/
uint16_t app_voice_prompt_getLangCount();

/**
* @brief      This function require get count which support,
* @param[out] a buffer to storage language LICD
* @return     If the operation completed successfully, return true, otherwise return false.
*/
bool app_voice_prompt_getSupportLang(uint16_t *buffer);


/**
* @brief      This function require set language for VP,
* @param[in]  sync count form agent
* @return     void.
*/
void app_voice_prompt_sync_count(uint16_t);


/**
* @brief      This function require dump all queuing VP, VC or ringtone
* @return     void.
*/
void app_voice_prompt_dump_all();

#ifdef __cplusplus
    }
#endif

#endif /* BT_SINK_APP_VOICE_PROMPT_H */

