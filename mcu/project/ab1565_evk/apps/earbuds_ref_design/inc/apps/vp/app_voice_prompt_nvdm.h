/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

 #ifndef __APP_VOICE_PROMPT_NVDM_H__
 #define __APP_VOICE_PROMPT_NVDM_H__

#include "rofs.h"

#define MAX_VP_LANGUAGE      5

#ifndef PACKED
#define PACKED  __attribute__((packed))
#endif

 /**************************************************************************************************
 * Structure
 **************************************************************************************************/
 /*
 ---------------------------------------------------------------------------------
 |   LCnt  |   Codec   |   NVKeyID   |   Codec   |   NVKeyID   |...
 ---------------------------------------------------------------------------------
 */
 typedef struct
 {
     uint16_t langCodec;
     uint16_t rofsFileId;
 }PACKED APP_VP_LANG_INFO_NVKEY_STRU;
 
 typedef struct
 {
     uint8_t langCnt;
     //VP_LANG_INFO_NVKEY_STRU lang[langCnt];
 }PACKED APP_VP_LANG_NVKEY_STRU;

 /*
 ----------------------------------------------------------------------------------------------------
 | EvtNum | EvtID  |FileCnt |	  FileID	 | EvtID  |FileCnt |	 FileID 	|	  FileID	 |...
 ----------------------------------------------------------------------------------------------------
 */
 typedef struct
 {
     uint8_t eventId;
     uint8_t fileCnt;
     //U16 fileId, ...
 }PACKED APP_VP_EVENT_INFO_NVKEY_STRU;

 typedef struct
 {
     uint8_t eventNum;
     //VP_EVENT_INFO_NVKEY_STRU evt[VP_EVENT_INFO_NVKEY_STRU];
 }PACKED APP_VP_EVENT_NVKEY_STRU;

 //NVKey: 0xF2DA
 typedef struct
 {
     uint8_t langId;
     uint8_t selectRound;
     uint8_t selectTime;  //unit: sec, please longer than the media event
 }PACKED APP_VP_SEL_PARA_STRU;


 typedef struct
 {
     uint16_t langCodec;
     uint8_t eventNum;
     uint8_t *pEventTable;
     uint8_t *pBuffer;
 }APP_VP_LANG_INFO_STRU;

 typedef struct
 {
    uint16_t langCnt;
    uint8_t *pLangBuffer;
    APP_VP_LANG_INFO_STRU langInfo[MAX_VP_LANGUAGE];
    APP_VP_SEL_PARA_STRU *pSelPara;
 }APP_VP_CTL_STRU;

 void app_voice_prompt_VpInit();
 bool app_voice_prompt_VpId2FileId(uint8_t VpId, uint16_t *fileId);
 void app_voice_prompt_VpDumpAllTable();
 bool app_voice_prompt_VPGetCurrentLangId(uint8_t *langId);
 bool app_voice_prompt_VPSetLang(uint8_t langIdx);
 bool app_voice_prompt_VPGetLang(uint8_t *langIdx);
 bool app_voice_prompt_GetSupportLangCnt(uint16_t *langCnt);
 bool app_voice_prompt_GetSupportLang(uint16_t *buffer);
#endif

