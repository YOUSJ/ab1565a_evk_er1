/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
#ifndef  _LED_CONTROL_STYLE_CFG_H_
#define  _LED_CONTROL_STYLE_CFG_H_

#ifdef __cplusplus
extern "C" {
#endif
#include "syslog.h"
#include "app_led_internal.h"


#define log_led(fmt, ...)            LOG_I(led_log, fmt, ##__VA_ARGS__)
#define log_led_info(fmt, cnt, ...)  LOG_MSGID_I(led_log, fmt, cnt, ##__VA_ARGS__)
#define log_led_warn(fmt, cnt, ...)  LOG_MSGID_W(led_log, fmt, cnt, ##__VA_ARGS__)
#define log_led_error(fmt, cnt, ...) LOG_MSGID_E(led_log, fmt, cnt, ##__VA_ARGS__)

#define     LED_SYSTEM_MODE_NORMAL          0
#define     LED_SYSTEM_MODE_LP_TEST         1


enum {
    LED0 = 0,
    LED1,
    LED2,
    LED3,
    LED4,
    LED5,
    LED_NUM,
};

enum {
    LED_OFF = 0,
    LED_ON,
    LED_ALT_FIRST,
    LED_ALT_SECOND,
    LED_ALT_THIRD,
    LED_ALT_FORTH,
    LED_ALT_FIFTH,
    LED_ALT_SIXTH,
    LED_DONT_CARE,
};

typedef enum {
    LED_PATTERN_BG = 0,
    LED_PATTERN_FG,
    LED_PATTERN_FILTER,
    LED_PATTERN_NONE,
}led_pattern_type_t;


typedef struct {
    uint8_t                  led_num;
    one_led_style_t         *led_setting;
}led_style_config_t;



void                  led_style_config_init(void);
void                  led_style_config_deinit(void);
led_style_config_t   *led_style_config_read(led_pattern_type_t    pattern, uint8_t  style_no);
uint8_t               led_style_config_sys_mode(void);


#ifdef __cplusplus
}
#endif
#endif /*END _LED_CONTROL_STYLE_CFG_H_*/


