/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#ifndef __APP_IN_EAR_UTILS_H__
#define __APP_IN_EAR_UTILS_H__

#ifdef MTK_IN_EAR_FEATURE_ENABLE

#include "bt_sink_srv.h"
#include "ui_shell_manager.h"
#include "ui_shell_activity.h"
#include "apps_config_key_remapper.h"
#include "app_in_ear_utils.h"
#include "atci.h"


typedef struct {
    bool isInEar;
    
#ifdef MTK_AWS_MCE_ENABLE
    bool isPartnerInEar;
    bool isInRho;
#endif
    
    app_in_ear_sta_t previousSta;
    app_in_ear_sta_t currentSta;
    bool eventDone;
    bool eventOutEnable;
    bool rhoEnable;
} apps_in_ear_local_context_t;


typedef enum{
    APP_IN_EAR_EVENT_UPDATE_STA,
    APP_IN_EAR_EVENT_ANC_ENABLE,
    APP_IN_EAR_EVENT_ANC_DISABLE
} app_in_ear_event_t;

/*
 * usually, the structure send from agent to partner for report in-ear state.
 * when rho is running, the old agent will send it to new agent.
*/
typedef struct{
    app_in_ear_event_t event;
    bool isInEar;
} app_in_ear_aws_data_t;

/**/
bool checkAndHandleStatusChanged(apps_in_ear_local_context_t* ctx);
void unMuteAllEarphones(void);
void sendAwsData(apps_in_ear_local_context_t * ctx, app_in_ear_event_t event);

//#define IN_EAR_DEBUG
#ifdef IN_EAR_DEBUG
void printStaToSysUART(apps_in_ear_local_context_t* ctx);
#endif

#ifdef MTK_ANC_ENABLE
void ctrlANC(bool enable);
void checkAndCtrlANC(apps_in_ear_local_context_t* ctx);
#endif

#define apps_events_report_in_ear(a,b)

#endif /*MTK_IN_EAR_FEATURE_ENABLE*/

#endif /*__APP_MUSIC_UTILS_H__*/

