/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __APPS_CONFIG_KEY_REMAPPER_H__
#define __APPS_CONFIG_KEY_REMAPPER_H__

#include "apps_config_event_list.h"
#include "apps_config_state_list.h"
#include "airo_key_event.h"
#ifdef MULTI_VA_SUPPORT_COMPETITION
#include "multi_va_manager.h"
#endif

#define APP_CFG_KEY_ID_1        (0x7E)
#define APP_CFG_KEY_ID_2        (0x7F)
#define APP_CFG_KEY_ID_3        (0x03)


#define APP_CFG_KEY_ID_FUNC              (0x01)/**< Function key. */
#define APP_CFG_KEY_ID_NEXT              (0x02) /**< Next key. */
#define APP_CFG_KEY_ID_PREV              (0x03)/**< Previous key. */

typedef struct {
    uint8_t key_id;
    uint16_t app_key_event;
    uint32_t supported_states;
} __attribute__((__packed__)) apps_config_key_event_map_t;

typedef struct {
    uint8_t key_event;
    uint8_t key_id;
    uint16_t app_key_event;
    uint32_t supported_states;
} __attribute__((__packed__)) apps_config_configurable_table_t;

apps_config_key_action_t apps_config_key_event_remapper_map_action(
        uint8_t key_id,
        airo_key_event_t key_event);

void apps_config_key_set_mmi_state(apps_config_state_t state);

apps_config_state_t apps_config_key_get_mmi_state(void);

void apps_config_key_remaper_init_configurable_table(void);

#ifdef MULTI_VA_SUPPORT_COMPETITION
void apps_config_key_remapper_set_va_type(multi_va_type_t old_va_type,  multi_va_type_t new_va_type);
#endif

#endif /* __APPS_CONFIG_KEY_REMAPPER_H__ */
