/* Copyright Statement:
 *
 * (C) 2018  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __APPS_EVENTS_INTERACTION_EVENT_H__
#define __APPS_EVENTS_INTERACTION_EVENT_H__

enum {
    /* The events below is used to support RHO features */
    APPS_EVENTS_INTERACTION_TRIGGER_RHO,    /* Any APPs can send the event to app_rho_idle_activity to trigger RHO */
    APPS_EVENTS_INTERACTION_RHO_STARTED,    /* app_rho_idle_activity send the event when RHO started */
    APPS_EVENTS_INTERACTION_RHO_END,        /* app_rho_idle_activity send the event when RHO ended */
    APPS_EVENTS_INTERACTION_PARTNER_SWITCH_TO_AGENT,    /* app_home_screen_idle_activity in Partner send the event to Agent for triggering RHO */

    /* The events below is used to support power off, reboot or disable/enable BT */
    APPS_EVENTS_INTERACTION_POWER_OFF_WAIT_TIMEOUT,     /* app_home_screen_idle_activity send the event to itself with a short delay time when do power off */
    APPS_EVENTS_INTERACTION_REQUEST_POWER_OFF,      /* Any APPs can send the event to app_home_screen_idle_activity to trigger power off */
    APPS_EVENTS_INTERACTION_REQUEST_IMMEDIATELY_POWER_OFF,  /* Any APPs can send the event to app_home_screen_idle_activity to trigger power off */

    APPS_EVENTS_INTERACTION_REQUEST_REBOOT,         /* Any APPs can send the event to app_home_screen_idle_activity to trigger reboot */
    APPS_EVENTS_INTERACTION_REQUEST_ON_OFF_BT,      /* Any APPs can send the event to app_home_screen_idle_activity to enable or disable BT */

    /* The events below is used to support sleep after no connection and wait long enough */
    APPS_EVENTS_INTERACTION_REFRESH_SLEEP_TIME,         /* app_home_screen_idle_activity in Partner send the event to Agent for trigger it reset sleep timer */
    APPS_EVENTS_INTERACTION_GO_TO_SLEEP,        /* app_home_screen_idle_activity send the event to itself with a delay time to trigger sleep flow.
                                                   The agent sends the event to partner to make partner sleep at the same time. */
    APPS_EVENTS_INTERACTION_SLEEP_WAIT_TIMEOUT, /* app_home_screen_idle_activity send the event to itself with a short delay time to sleep. */

    /* The events below is used to support update states */
    APPS_EVENTS_INTERACTION_UPDATE_MMI_STATE,   /* Activities update mmi state when it receive the event. If an activity want to set MMI state, it should
                                                   returns true when receives the event to avoid next activity receives the event. */
    APPS_EVENTS_INTERACTION_UPDATE_LED_BG_PATTERN,  /* Activities update LED background pattern when it receive the event. If an activity want to set LED background
                                                       pattern, it should returns true when receives the event to avoid next activity receives the event. */
    /* The events below is used to support find me feature */
    APPS_EVENTS_INTERACTION_FINISH_FIND_ME, /* Reserved, not useful now. */
    APPS_EVENTS_INTERACTION_PLAY_FIND_ME_TONE,  /* To play find me tone continually, app_fm_activity send the event to itself to trigger play voice prompt with a delay */
    /* The events below is used to support GSOUND feature */
    APPS_EVENTS_INTERACTION_GSOUND_ACTION_REJECTED, /* Some key actions (e.g. KEY_AVRCP_PLAY) need be processed by GSOUND lib. If GSOUND lib ignore the events, it sends
                                                       the event. */
    APPS_EVENTS_INTERACTION_INCREASE_BLE_ADV_INTERVAL, /* Use a small interval in first 30 seconds, and bt_app_common sends the interval to ask APPs to stop ble adv
                                                        when timeout. */
    APPS_EVENTS_INTERACTION_RELOAD_KEY_ACTION_FROM_NVKEY,   /* Read NVkey to refresh the key-action table */
    APPS_EVENTS_INTERACTION_BT_VISIBLE_STATE_CHANGE,    /* Agent send current bt visible state to partner */
    
    APPS_EVENTS_INTERACTION_UPDATE_IN_EAR_STA_EFFECT,  /* The driver callbac will send this event when in-ear status changed. */
    APPS_EVENTS_INTERACTION_IN_EAR_UPDATE_STA, /* The in-ear app will send this event when in-ear status changed. */

    APPS_EVENTS_INTERACTION_LEAKAGE_DETECTION_VP_TRIGGER, /* This event come from race cmd handler */
    APPS_EVENTS_INTERACTION_LEAKAGE_DETECTION_VP,  /* This event is used for vp */
};

#endif /* __APPS_EVENTS_INTERACTION_EVENT_H__ */
