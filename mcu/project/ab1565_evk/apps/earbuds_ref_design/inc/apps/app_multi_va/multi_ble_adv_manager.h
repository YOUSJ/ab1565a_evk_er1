/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __MULTI_BLE_ADV_MANAGER_H__
#define __MULTI_BLE_ADV_MANAGER_H__

#include "bt_gap_le.h"
#include <stdbool.h>

typedef struct {
    bt_hci_cmd_le_set_advertising_parameters_t *adv_param;
    bt_hci_cmd_le_set_advertising_data_t *adv_data;
    bt_hci_cmd_le_set_scan_response_data_t *scan_rsp;
} multi_ble_adv_info_t;

typedef enum {
    MULTI_ADV_PAUSE_RESULT_HAVE_STOPPED,
    MULTI_ADV_PAUSE_RESULT_WAITING,
} multi_adv_pause_result_t;

#define MULTI_BLE_ADV_NEED_GEN_ADV_PARAM    (1)
#define MULTI_BLE_ADV_NEED_GEN_ADV_DATA     (1 << 1)
#define MULTI_BLE_ADV_NEED_GEN_SCAN_RSP     (1 << 2)

typedef uint32_t (*get_ble_adv_data_func_t)(multi_ble_adv_info_t *);

void multi_ble_adv_manager_start_ble_adv(void);

void multi_ble_adv_manager_stop_ble_adv(void);

multi_adv_pause_result_t multi_ble_adv_manager_pause_ble_adv(void);

void multi_ble_adv_manager_resume_ble_adv(void);

bool multi_ble_adv_manager_add_ble_adv(get_ble_adv_data_func_t get_ble_adv_data);

bool multi_ble_adv_manager_remove_ble_adv(get_ble_adv_data_func_t get_ble_adv_data);

void multi_ble_adv_manager_notify_ble_adv_data_changed(void);

void multi_ble_adv_manager_set_default_adv_function(get_ble_adv_data_func_t func);

void multi_ble_adv_manager_bt_event_proc(uint32_t event_id, void *extra_data, size_t data_len);

void multi_ble_adv_manager_multi_va_proc(uint32_t event_id, void *extra_data, size_t data_len);

void multi_ble_adv_manager_interaction_proc(uint32_t event_id, void *extra_data, size_t data_len);


#endif /* __MULTI_BLE_ADV_MANAGER_H__ */
