/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __MULTI_VA_MANAGER_H__
#define __MULTI_VA_MANAGER_H__

#include "multi_ble_adv_manager.h"
#include <stdint.h>

#define MULTI_VA_TYPE_AMA       (0)
#define MULTI_VA_TYPE_GSOUND    (1)
#define MULTI_VA_XIAOWEI        (2)
#define MULTI_VA_XIAOAI         (3)
#define MULTI_VA_TYPE_MAX_NUM   (4)
#define MULTI_VA_TYPE_UNKNOWN   (0xFF)
typedef uint32_t                multi_va_type_t;

typedef enum {
    // MULTI_VA_SWITCH_OFF_NEVER_CONNECTED,
    MULTI_VA_SWITCH_OFF_WAIT_INACTIVE,
    MULTI_VA_SWITCH_OFF_SET_INACTIVE_DONE,
} multi_va_switch_off_return_t;

typedef struct {
    void (*voice_assistant_initialize)(bool selected);
    multi_va_switch_off_return_t (*on_voice_assistant_type_switch)(bool selected);
    get_ble_adv_data_func_t on_get_ble_adv_data;
} multi_va_manager_callbacks_t;

void multi_va_manager_start(void);

multi_va_type_t multi_va_manager_get_current_va_type(void);

void multi_voice_assistant_manager_register_instance(
        multi_va_type_t va_type,
        const multi_va_manager_callbacks_t *p_callbacks);

void multi_voice_assistant_manager_notify_va_connected(multi_va_type_t va_type);

void multi_voice_assistant_manager_notify_va_disconnected(multi_va_type_t va_type);

void multi_voice_assistant_manager_set_inactive_done(multi_va_type_t va_type);

bool multi_voice_assistant_manager_enable_adv(bool enable);

/*
* internal use.
*/
#if defined(MULTI_VA_SUPPORT_COMPETITION) && defined(MTK_AWS_MCE_ENABLE)
void multi_va_manager_send_va_type_to_partner(void);

void multi_va_manager_receive_va_change_from_agent(void *p_va_type);

void multi_va_manager_receive_reboot_from_agent(void);

void multi_va_manager_on_partner_detached(void);
#endif

void multi_voice_assistant_manager_va_config_changed(void);

#endif /* __MULTI_VA_MANAGER_H__ */
