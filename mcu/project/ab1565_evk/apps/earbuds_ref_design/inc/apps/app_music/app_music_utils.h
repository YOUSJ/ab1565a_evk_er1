/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#ifndef __APP_MUSIC_UTILS_H__
#define __APP_MUSIC_UTILS_H__

#include "bt_sink_srv.h"
#include "ui_shell_manager.h"
#include "ui_shell_activity.h"
#include "apps_config_key_remapper.h"


#define APP_MUSIC_UTILS "app_music_utils.h"
#define AVRCP_OPERATION_STA_IDLE 0
#define AVRCP_OPERATION_STA_FAST_FORWARD_PRESS 1
#define AVRCP_OPERATION_STA_FAST_REWIND_PRESS 2

#ifdef MTK_AWS_MCE_ENABLE
typedef enum {
    MUSIC_STEREO = 0,
    MUSIC_MONO
}music_mix_state_t;
#endif

typedef struct {
    bool music_playing;
#ifdef MTK_AWS_MCE_ENABLE
    bool isPartnerCharging;
    bool isPartnerConnected;
    music_mix_state_t currMixState;
    bool isBoothInEar;                  /* is booth agent and partner in ear */
#endif
    bt_sink_srv_state_t bt_state;
    bool isAutoPaused;                  /* the music paused because of no earphone in ear */
    uint32_t avrcp_op_sta;
} apps_music_local_context_t;

apps_config_key_action_t app_get_config_status_by_sink_state(bt_sink_srv_state_t state);
#ifdef MTK_AWS_MCE_ENABLE
void app_bt_music_checkAudioState(apps_music_local_context_t *cntx);
bool app_bt_music_proc_aws_data_event(
            ui_shell_activity_t *self,
            uint32_t event_id,
            void *extra_data,
            size_t data_len);
#endif

#ifdef GSOUND_LIBRARY_ENABLE
bool app_bt_music_proc_gsound_reject_action(ui_shell_activity_t *self, bt_sink_srv_action_t sink_action);
#endif

apps_config_key_action_t app_bt_music_proc_key_event(
            ui_shell_activity_t *self,
            uint32_t event_id,
            void *extra_data,
            size_t data_len);

bool app_bt_music_proc_basic_state_event(
            ui_shell_activity_t *self,
            uint32_t event_id,
            void *extra_data,
            size_t data_len);

bool app_music_util_bt_cm_event_proc(
        ui_shell_activity_t *self,
        uint32_t event_id,
        void *extra_data,
        size_t data_len);

#endif /*__APP_MUSIC_UTILS_H__*/

