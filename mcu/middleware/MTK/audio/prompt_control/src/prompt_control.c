/* Copyright Statement:
 *
 * (C) 2005-2017  MediaTek Inc. All rights reserved.
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. ("MediaTek") and/or its licensors.
 * Without the prior written permission of MediaTek and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) MediaTek Software
 * if you have agreed to and been bound by the applicable license agreement with
 * MediaTek ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of MediaTek Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT MEDIATEK SOFTWARE RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 */

/* Includes ------------------------------------------------------------------*/
#ifdef MTK_PROMPT_SOUND_ENABLE

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "FreeRTOS.h"
#include "timers.h"
#include "audio_mixer.h"
#include "audio_codec.h"
#include "bt_sink_srv.h"
#include "mp3_codec.h"
#include "mp3_codec_internal.h"
#include "bt_sink_srv_audio_setting.h"
#include "bt_sink_srv_ami.h"
#if defined(MTK_AVM_DIRECT)
    #include "hal_audio_cm4_dsp_message.h"
    #include "hal_audio_internal.h"
    #include "hal_dvfs_internal.h"
#else
    #include "hal_audio_internal_service.h"
#endif

#include "prompt_control.h"

#include "bt_sink_srv_ami.h"

#define UNUSED(x)  ((void)(x))

log_create_module(VPC, PRINT_LEVEL_INFO);

/********************************************************
 * Macro & Define
 *
 ********************************************************/
#define PROMPT_CONTROL_MAIN_TRACK_GAIN    0x2000 /*weight 0.25*/
#define PROMPT_CONTROL_SIDE_TRACK_GAIN    0x8000 /*weight 1.0 */

#ifdef MTK_AVM_DIRECT
#ifdef MTK_PROMPT_SOUND_USING_CONFIG_DEFAULT_GAIN_LEVEL
#define PROMPT_CONTROL_DEFAULT_GAIN_LEVEL bt_sink_srv_ami_get_vp_default_volume_level()
#else
#define PROMPT_CONTROL_DEFAULT_GAIN_LEVEL 11
#endif
#ifndef MTK_MP3_TASK_DEDICATE
#define VP_MASK_VP_HAPPENING       0x00000004
#define VP_MASK_DL1_HAPPENING      0x00000008
#endif
#endif

#ifdef  HAL_DVFS_MODULE_ENABLED
#ifdef HAL_DVFS_416M_SOURCE
#define VP_DVFS_DEFAULT_SPEED  HAL_DVFS_FULL_SPEED_104M
#elif defined(HAL_DVFS_312M_SOURCE)
#define VP_DVFS_DEFAULT_SPEED  DVFS_78M_SPEED
#endif
#endif
/********************************************************
 * Global variable
 *
 ********************************************************/
static uint16_t mixer_main_track_gain = PROMPT_CONTROL_MAIN_TRACK_GAIN;
static uint16_t mixer_side_track_gain = PROMPT_CONTROL_SIDE_TRACK_GAIN;
static audio_codec_media_handle_t *g_codec_handle = NULL;
static mp3_codec_media_handle_t   *g_mp3_codec_handle = NULL;
prompt_control_callback_t  g_app_callback = NULL;
prompt_control_callback_t  g_app_internal_callback = NULL;

#ifdef MTK_AVM_DIRECT
static bool       g_prompt_mute_lock = false;
#ifndef MTK_PROMPT_SOUND_USING_CONFIG_DEFAULT_GAIN_LEVEL
static uint8_t    g_prompt_gain_level = PROMPT_CONTROL_DEFAULT_GAIN_LEVEL;
#endif
SemaphoreHandle_t g_prompt_semaphore_handle = NULL;
#ifndef MTK_MP3_TASK_DEDICATE
#else
uint32_t vp_gpt_timer_handle = 0;
volatile uint32_t g_vp_task_mask = 0;
extern volatile mp3_codec_media_handle_t   *g_mp3_codec_task_handle;
#endif
#endif
extern volatile uint8_t g_audio_dl_suspend_by_user;

/********************************************************
 * Function
 *
 ********************************************************/
#ifdef MTK_MP3_TASK_DEDICATE
extern void mp3_codec_event_send_from_isr(mp3_codec_queue_event_id_t id, void *parameter);
#endif

#ifdef MTK_AVM_DIRECT
prompt_control_status_t prompt_control_mutex_lock(void)
{
    prompt_control_status_t status = PROMPT_CONTROL_RETURN_OK;

    if ( g_prompt_semaphore_handle != NULL ) {
        LOG_MSGID_I(VPC, "[VPC] prompt_control_mutex_lock() +\r\n", 0);
        xSemaphoreTake(g_prompt_semaphore_handle, portMAX_DELAY);
        LOG_MSGID_I(VPC, "[VPC] prompt_control_mutex_lock() -\r\n", 0);
    }
    else {
        status = PROMPT_CONTROL_RETURN_ERROR;
    }

    return status;
}

prompt_control_status_t prompt_control_mutex_unlock(void)
{
    prompt_control_status_t status = PROMPT_CONTROL_RETURN_OK;

    if ( g_prompt_semaphore_handle != NULL ) {
        LOG_MSGID_I(VPC, "[VPC] prompt_control_mutex_unlock()\r\n", 0);
        xSemaphoreGive(g_prompt_semaphore_handle);
    }
    else {
        status = PROMPT_CONTROL_RETURN_ERROR;
    }

    return status;
}
#endif

#ifdef MTK_MP3_TASK_DEDICATE
bool prompt_control_query_state(void)
{
    if(g_mp3_codec_task_handle != NULL){
        if(g_mp3_codec_task_handle->state == MP3_CODEC_STATE_PLAY){
            return true;
        } else {
            return false;
        }
    }
    return false;
}
#endif

#if !defined(MTK_AVM_DIRECT)
static void prompt_control_set_mixer_volume(void)
{
#if defined(MTK_AVM_DIRECT)
    uint32_t data32 = (mixer_main_track_gain<<16) | mixer_side_track_gain;

    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_PROMPT_SET_VOLUME, 0, data32, false);
#else
    audio_mixer_status_t status;

    status = audio_mixer_set_volume(mixer_main_track_gain, mixer_side_track_gain);
    if (status != AUDIO_MIXER_STATUS_OK) {
        LOG_MSGID_I(VPC, "[VPC]Set mixer volume failed : status=%d\r\n",1, (int32_t)status);
    }
#endif
}
#endif //!MTK_AVM_DIRECT

#ifdef MTK_AVM_DIRECT
void prompt_control_set_volume(void)
{
    #ifdef __BT_SINK_SRV_AUDIO_SETTING_SUPPORT__
    bt_sink_srv_audio_setting_vol_info_t vol_info;
    bt_sink_srv_audio_setting_vol_t vol;
    bt_sink_srv_am_type_t current_audio_type;
    uint32_t digital = 0;
    uint32_t analog = 0;

    vol_info.type = VOL_VP;
#ifndef MTK_PROMPT_SOUND_USING_CONFIG_DEFAULT_GAIN_LEVEL
    vol_info.vol_info.def_vol_info.lev = g_prompt_gain_level;
#else
    vol_info.vol_info.def_vol_info.lev = PROMPT_CONTROL_DEFAULT_GAIN_LEVEL;
#endif
    memset(&vol, 0, sizeof(bt_sink_srv_audio_setting_vol_t));

    bt_sink_srv_audio_setting_get_vol(&vol_info, &vol);
    digital = vol.vol.vp_vol.vol.digital;
    analog = vol.vol.vp_vol.vol.analog_L;

    current_audio_type = bt_sink_srv_ami_get_current_scenario();
    if(current_audio_type != NONE) {
        analog = 0x00007FFF; //don't care
    }

    hal_audio_set_stream_out_dl2_volume(digital, analog);

    LOG_MSGID_I(VPC, "[VPC]Set Prompt Volume Scenario:%d Level[%d] D_Gain:0x%x A_Gain:0x%x\n",4,
        current_audio_type, vol_info.vol_info.def_vol_info.lev, digital, analog);
    #endif
}
#endif

#ifdef MTK_AVM_DIRECT
void prompt_control_set_mute(bool mute)
{
    bt_sink_srv_am_result_t ret = 0;
    g_prompt_mute_lock = mute;
    ret = bt_sink_srv_ami_audio_set_mute(FEATURE_NO_NEED_ID, mute, STREAM_OUT_2);
    LOG_MSGID_I(VPC, "[VPC]prompt_control_set_mute(0x%x), ret:0x%x", 2, mute, ret);
    UNUSED(ret);
}
#endif

#ifdef __AUDIO_WAV_ENABLE__
static void prompt_control_wav_callback(audio_codec_media_handle_t *handle, audio_codec_event_t event)
{
    LOG_MSGID_I(VPC, "[VPC]wav tone callback:%d\r\n",1, (uint32_t)event);
    switch (event) {
        case AUDIO_CODEC_MEDIA_OK:
            prompt_control_stop_tone();
#ifndef MTK_MP3_TASK_DEDICATE
            if (g_app_callback) {
                g_app_callback(PROMPT_CONTROL_MEDIA_END);
            }
            //bt_sink_srv_event_callback(BT_SINK_SRV_EVENT_MIXER_TONE_STOP, NULL);
#endif
            break;
        default:
            break;
    }
}
#endif

static bool prompt_control_play_wav_tone(uint8_t *tone_buf, uint32_t tone_size)
{
#ifdef __AUDIO_WAV_ENABLE__
    audio_codec_media_handle_t *handle;

    LOG_MSGID_I(VPC, "[VPC] Play wav tone (0x%x, %d)",2, tone_buf, tone_size);

    if (tone_size <= 0) {
        LOG_MSGID_E(VPC, "[VPC]Error found:tone size=%d",1, tone_size);
        return false;
    }

    handle = audio_codec_wav_codec_open(prompt_control_wav_callback, NULL);
    if (handle != NULL) {
        g_codec_handle = handle;

        handle->set_track_role(handle, AUDIO_MIXER_TRACK_ROLE_SIDE);

        handle->set_bitstream_buffer(handle, tone_buf, tone_size);

        if (handle->play(handle) != AUDIO_CODEC_RETURN_OK) {
            LOG_MSGID_I(VPC, "[VPC]play WAV codec failed.\r\n", 0);
            prompt_control_stop_tone();
            return false;
        }
    } else {
        LOG_MSGID_I(VPC, "[VPC]open WAV codec error\r\n", 0);
        return false;
    }
    return true;
#else
    return false;
#endif
}

void prompt_control_mp3_callback(mp3_codec_media_handle_t *handle, mp3_codec_event_t event)
{
    LOG_MSGID_I(VPC, "[VPC]mp3 tone callback:%d\r\n",1, event);
    switch (event) {
        case MP3_CODEC_MEDIA_BITSTREAM_END: {
            prompt_control_stop_tone();
#ifndef MTK_MP3_TASK_DEDICATE
            if (g_app_callback) {
                g_app_callback(PROMPT_CONTROL_MEDIA_END);
            }
            //bt_sink_srv_event_callback(BT_SINK_SRV_EVENT_MIXER_TONE_STOP, NULL);
#endif
        }
        break;
        default:
            break;
    }
}

static bool prompt_control_play_mp3_tone(uint8_t *tone_buf, uint32_t tone_size)
{
#ifndef MTK_MP3_TASK_DEDICATE
    mp3_codec_media_handle_t *handle;

    LOG_MSGID_I(VPC, "[VPC] Play mp3 tone (0x%x, %d)",2, tone_buf, tone_size);

    if (tone_size <= 0) {
        LOG_MSGID_E(VPC, "[VPC]Error found:tone size=%d",1, tone_size);
        return false;
    }

    handle = mp3_codec_open(prompt_control_mp3_callback);
    if (handle != NULL) {
        g_mp3_codec_handle = handle;

        handle->set_bitstream_buffer(handle, tone_buf, tone_size);

        mp3_codec_set_memory2(handle);

        handle->set_track_role(handle, AUDIO_MIXER_TRACK_ROLE_SIDE);

        if (handle->play(handle) != MP3_CODEC_RETURN_OK) {
            LOG_MSGID_I(VPC, "[VPC]play mp3 codec failed.\r\n", 0);
            prompt_control_stop_tone();
            return false;
        }
        handle->flush(handle, 1);
    } else {
        LOG_MSGID_I(VPC, "[VPC]open mp3 codec error\r\n", 0);
        return false;
    }

    return true;
#else
    g_mp3_codec_task_handle->set_bitstream_buffer((mp3_codec_media_handle_t *)g_mp3_codec_task_handle, tone_buf, tone_size);
    mp3_codec_event_send_from_isr(MP3_CODEC_QUEUE_EVENT_PLAY, (void *)g_mp3_codec_task_handle);

    return true;
#endif
}

void prompt_control_set_mixer_gain(uint16_t main_gain, uint16_t side_gain)
{
    LOG_MSGID_I(VPC, "[VPC]set main_gain=0x%x, side_gain=0x%x\n",2, main_gain, side_gain);
    mixer_main_track_gain = main_gain;
    mixer_side_track_gain = side_gain;
}

#ifdef MTK_AVM_DIRECT
uint8_t prompt_control_get_level()
{
#ifndef MTK_PROMPT_SOUND_USING_CONFIG_DEFAULT_GAIN_LEVEL
    LOG_MSGID_I(VPC, "[VPC]get level:%d\n",1, g_prompt_gain_level);
    return g_prompt_gain_level;
#else
    LOG_MSGID_I(VPC, "[VPC]get level:%d\n",1, PROMPT_CONTROL_DEFAULT_GAIN_LEVEL);
    return PROMPT_CONTROL_DEFAULT_GAIN_LEVEL;
#endif
}

#ifndef MTK_PROMPT_SOUND_USING_CONFIG_DEFAULT_GAIN_LEVEL
void prompt_control_set_level(uint8_t vol_level)
{
    LOG_MSGID_I(VPC, "[VPC]set level:%d\n",1, vol_level);
    g_prompt_gain_level = vol_level;
}
#endif
#endif

void prompt_control_stop_tone_active(void);
static void prompt_control_set_aws_sync(bool aws_sync_request, uint32_t aws_sync_time);
void prompt_control_gpt_callback(void *user_data);
bool prompt_control_play_tone_internal(prompt_control_tone_type_t tone_type, uint8_t *tone_buf, uint32_t tone_size, uint32_t sync_time, prompt_control_callback_t callback)
{
    bool ret = false;
    g_app_callback = callback;

#ifdef MTK_MP3_TASK_DEDICATE
    if(!g_prompt_mute_lock){
        if(AUD_EXECUTION_FAIL == bt_sink_srv_ami_audio_set_mute(FEATURE_NO_NEED_ID, false, STREAM_OUT_2)){
            LOG_MSGID_I(VPC, "[VPC]set un-mute fail\n",0);
        }
    }
    g_vp_task_mask &= ~VP_LOCAL_MASK_MUTE_SHARED_BUF;
#endif

    if (tone_type == VPC_WAV) {
        ret = prompt_control_play_wav_tone(tone_buf, tone_size);
    } else if (tone_type == VPC_MP3) {
        ret = prompt_control_play_mp3_tone(tone_buf, tone_size);
    }
    return ret;
}

bool prompt_control_play_tone(prompt_control_tone_type_t tone_type, uint8_t *tone_buf, uint32_t tone_size, prompt_control_callback_t callback)
{
    bool ret = false;

#if defined(MTK_AVM_DIRECT)
#ifndef MTK_MP3_TASK_DEDICATE
    ami_set_audio_mask(VP_MASK_VP_HAPPENING, true);
    if ( g_prompt_semaphore_handle == NULL ) {
       g_prompt_semaphore_handle = xSemaphoreCreateMutex();
    }
    prompt_control_mutex_lock();
#else
    audio_codec_mutex_unlock();
#endif
#endif

    LOG_MSGID_I(VPC, "[VPC]mix play internal type=%d, buf=%x, size=%d\n",3, tone_type, tone_buf, tone_size);

#if defined(__HAL_AUDIO_AWS_SUPPORT__) && !defined(MTK_AVM_DIRECT)
    audio_service_aws_skip_clock_skew(1);
#endif  /* __HAL_AUDIO_AWS_SUPPORT__ */

    if (g_codec_handle || g_mp3_codec_handle) {
#if defined(MTK_AVM_DIRECT)
#ifndef MTK_MP3_TASK_DEDICATE
        prompt_control_stop_tone_active();
        for (uint16_t i=0; ; i++) {
            if( !(g_codec_handle || g_mp3_codec_handle) ){
                break;
            }
            if (i == 1000) {
                LOG_MSGID_E(VPC, "[VPC]Active stop VP wait too long.", 0);
                LOG_MSGID_E(VPC, "VP_active_stop_wait", 0);
                configASSERT(0);
            }
            vTaskDelay(2 / portTICK_RATE_MS);
        }
#endif
#else
        return ret;
#endif
    }
#if defined(MTK_AVM_DIRECT)
#ifndef MTK_MP3_TASK_DEDICATE
#ifdef HAL_DVFS_MODULE_ENABLED
    hal_dvfs_lock_control(VP_DVFS_DEFAULT_SPEED, HAL_DVFS_LOCK);
#endif
    ami_hal_audio_status_set_running_flag(AUDIO_MESSAGE_TYPE_PROMPT, true);
    prompt_control_set_volume();
#endif
#else
    prompt_control_set_mixer_volume();
#endif

    if( am_prompt_control_play_sync_tone(tone_type, tone_buf, tone_size, 0, callback) == AUD_EXECUTION_SUCCESS ){
        ret = true;
    } else {
        ret = false;
    }

#if defined(MTK_AVM_DIRECT)
#ifndef MTK_MP3_TASK_DEDICATE
    prompt_control_mutex_unlock();
#else
    audio_codec_mutex_unlock();
#endif
#endif
    return ret;
}

#ifdef MTK_MP3_TASK_DEDICATE
bool prompt_control_play_sync_tone(prompt_control_tone_type_t tone_type, uint8_t *tone_buf, uint32_t tone_size, uint32_t sync_time, prompt_control_callback_t callback)
{
    bool ret = false;
    bool sync_request = false;
    // uint32_t savedmask = 0;
    uint32_t curr_cnt = 0;
#if defined(MTK_AVM_DIRECT)
    audio_codec_mutex_unlock();
#endif

    LOG_MSGID_I(VPC, "[VPC][SYNC]mix play internal type=%d, buf=%x, size=%d, sync time=%u\n",4, tone_type, tone_buf, tone_size, sync_time);

#if defined(__HAL_AUDIO_AWS_SUPPORT__) && !defined(MTK_AVM_DIRECT)
    audio_service_aws_skip_clock_skew(1);
#endif  /* __HAL_AUDIO_AWS_SUPPORT__ */

    if (g_codec_handle || g_mp3_codec_handle) {
#if !defined(MTK_AVM_DIRECT)
        return ret;
#endif
    }

    if(sync_time != 0) {
        sync_request = true;
        prompt_control_set_aws_sync(true, sync_time);
        if(sync_request){
            if(sync_time <= 80) { // less than 80 ms should be assert, the time of dsp handling CCNI Msg will be more than 80 ms
                LOG_MSGID_I(VPC, "[VPC][SYNC]mix play internal type=%d, buf=%x, size=%d, sync time=%u\n",4, tone_type, tone_buf, tone_size, sync_time);
                LOG_MSGID_E(VPC, "[VPC][SYNC]VP_SYNC Time expired before playing!!! ", 0);
                //configASSERT(0);
            }
        }
        hal_gpt_status_t gpt_status = hal_gpt_sw_get_timer(&vp_gpt_timer_handle);
        if (HAL_GPT_STATUS_OK != gpt_status) {
            LOG_MSGID_I(VPC, "[VPC][SYNC]VP have create one_shot Timer error!!, error id = %d\n", 1, gpt_status);
        } else {
            /*Mask interrupt for set timer.*/
            // hal_nvic_save_and_set_interrupt_mask(&savedmask);
            hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_32K, &curr_cnt);
            if(sync_request){
            //     gpt_status = hal_gpt_sw_start_timer_us(vp_gpt_timer_handle, bt_sync_time - (curr_cnt - bt_cur_gpt_time), prompt_control_gpt_callback, NULL);
            // }else{
                gpt_status = hal_gpt_sw_start_timer_us(vp_gpt_timer_handle, sync_time * 1000, prompt_control_gpt_callback, NULL);
            }
            /*Restore interrupt when set timer end.*/
            // hal_nvic_restore_interrupt_mask(savedmask);
            if(HAL_GPT_STATUS_OK != gpt_status)
            {
                LOG_MSGID_I(VPC, "[VPC][SYNC]VP start one_shot Timer error!!, error id = %d.\n", 1, gpt_status);
            } else {
                LOG_MSGID_I(VPC, "[VPC][SYNC]VP start one_shot Timer pass, sync_time  = %d\n", 1, sync_time);
            }
        }

    }else {
        prompt_control_set_aws_sync(false, sync_time);
    }

    am_prompt_control_play_sync_tone(tone_type, tone_buf, tone_size, sync_time, callback);

#if defined(MTK_AVM_DIRECT)
    audio_codec_mutex_unlock();
#endif
    return ret;
}
#endif

void prompt_control_stop_tone_internal(void)
{
#ifndef MTK_MP3_TASK_DEDICATE
    if (g_codec_handle) {
        if (g_codec_handle->stop(g_codec_handle) != AUDIO_CODEC_RETURN_OK ) {
            LOG_MSGID_I(VPC, "[VPC]stop WAV codec fail.\n", 0);
        }
        if (audio_codec_wav_codec_close(g_codec_handle) != AUDIO_CODEC_RETURN_OK ) {
            LOG_MSGID_I(VPC, "[VPC]close WAV codec fail.\n", 0);
        }
        g_codec_handle = NULL;
    } else if (g_mp3_codec_handle) {
        if (g_mp3_codec_handle->stop(g_mp3_codec_handle) != MP3_CODEC_RETURN_OK ) {
            LOG_MSGID_I(VPC, "[VPC]stop mp3 codec fail.\n", 0);
        }
        if (mp3_codec_close(g_mp3_codec_handle) != MP3_CODEC_RETURN_OK ) {
            LOG_MSGID_I(VPC, "[VPC]close mp3 codec fail.\n", 0);
        }
        g_mp3_codec_handle = NULL;
    }
#if defined(MTK_AVM_DIRECT)
        ami_set_audio_mask(VP_MASK_VP_HAPPENING, false);
        if(g_audio_dl_suspend_by_user == false) {
            am_audio_dl_resume();
        }
        am_audio_ul_resume();

#ifdef HAL_DVFS_MODULE_ENABLED
        hal_dvfs_lock_control(VP_DVFS_DEFAULT_SPEED, HAL_DVFS_UNLOCK);
#endif
        ami_hal_audio_status_set_running_flag(AUDIO_MESSAGE_TYPE_PROMPT, false);
#endif
    g_app_callback = NULL;
#else  /*MTK_MP3_TASK_DEDICATE*/
    if(!g_prompt_mute_lock){
        if(AUD_EXECUTION_FAIL == bt_sink_srv_ami_audio_set_mute(FEATURE_NO_NEED_ID, true, STREAM_OUT_2)){
            LOG_MSGID_I(VPC, "[VPC]set mute fail\n",0);
        }
    }
    g_vp_task_mask |= VP_LOCAL_MASK_MUTE_SHARED_BUF;
    mp3_codec_event_send_from_isr(MP3_CODEC_QUEUE_EVENT_STOP, (void *)g_mp3_codec_task_handle);
    if (g_codec_handle) {
        g_codec_handle = NULL;
    } else if (g_mp3_codec_handle) {
        g_mp3_codec_handle = NULL;
    }
#endif /*MTK_MP3_TASK_DEDICATE*/
}

void prompt_control_stop_tone(void)
{
    LOG_MSGID_I(VPC, "[VPC]stop codec internal done (0x%x,0x%x).\n",2, g_codec_handle, g_mp3_codec_handle);
    audio_codec_mutex_lock();
#if defined(__HAL_AUDIO_AWS_SUPPORT__) && !defined(MTK_AVM_DIRECT)
    audio_service_aws_skip_clock_skew(0);
#endif  /* __HAL_AUDIO_AWS_SUPPORT__ */
    am_prompt_control_stop_tone();
    audio_codec_mutex_unlock();
}

void prompt_control_stop_tone_active(void)
{
#if defined(MTK_AVM_DIRECT)
    LOG_MSGID_I(VPC, "[VPC]Try to stop codec. (0x%x,0x%x).\n",2, g_codec_handle, g_mp3_codec_handle);

    audio_codec_mutex_lock();
    if (g_codec_handle) {
    } else if (g_mp3_codec_handle) {
        mp3_codec_internal_handle_t *internal_handle = mp3_handle_ptr_to_internal_ptr(g_mp3_codec_handle);
        internal_handle->media_bitstream_end = 1;
    }
    audio_codec_mutex_unlock();
#endif
}

static void prompt_control_set_aws_sync(bool aws_sync_request, uint32_t aws_sync_time)
{
#ifdef MTK_MP3_TASK_DEDICATE
    audio_codec_mutex_lock();
    uint32_t savedmask;
    /*Mask interrupt for set timer.*/
    hal_nvic_save_and_set_interrupt_mask(&savedmask);
    if (aws_sync_request){
        TickType_t xTimeNow;
        xTimeNow = xTaskGetTickCount();
        g_mp3_codec_task_handle->aws_sync_request = aws_sync_request;
        g_mp3_codec_task_handle->aws_sync_time    = xTimeNow + aws_sync_time;
    } else {
        g_mp3_codec_task_handle->aws_sync_request = aws_sync_request;
        g_mp3_codec_task_handle->aws_sync_time    = 0;
    }
    /*Restore interrupt when set timer end.*/
    hal_nvic_restore_interrupt_mask(savedmask);
    audio_codec_mutex_unlock();
    LOG_MSGID_I(VPC, "[VPC]Set VP Sync time(%d) tick(%d).\n", 2, aws_sync_time, g_mp3_codec_task_handle->aws_sync_time);
#endif
}

extern TickType_t xTimeOUT_VP;
void prompt_control_gpt_callback(void *user_data)
{
#ifdef MTK_MP3_TASK_DEDICATE
    xTimeOUT_VP = xTaskGetTickCount();
    mp3_codec_event_send_from_isr(MP3_CODEC_QUEUE_EVENT_SYNC_TRIGGER, (void *)g_mp3_codec_task_handle);
#endif
    hal_gpt_status_t gpt_status = hal_gpt_sw_free_timer(vp_gpt_timer_handle);
    if(HAL_GPT_STATUS_OK != gpt_status)
    {
        LOG_MSGID_I(VPC, "[VPC][SYNC]VP free one_shot Timer error!!, error id = %d.\n", 1, gpt_status);
    }
    vp_gpt_timer_handle = 0;
}

#endif

