/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "battery_management_core.h"
int *voltage_table=NULL;
uint8_t gauge_timer_count = 0;                              /*Data : For gauge calculation*/
uint8_t gauge_regular_second_time;                          /*Data : For gauge calculation*/
extern battery_basic_data bm_cust;                          /*Data : restore battery basic data*/
extern battery_managerment_control_info_t bm_ctrl_info;     /*Data : restore battery basic data*/

extern TaskHandle_t battery_regular_task_t;
/*==========[Battery Management:Gague]==========*/
void battery_calibration_gauge_tolerance(void){
    if (pmu_get_charger_state() == 0x2) {
        gauge_timer_count++;
        if (gauge_timer_count == gauge_regular_second_time) {
            bm_ctrl_info.gauge_calibration += 1;
            gauge_timer_count = 0;
        }
        if (bm_ctrl_info.gauge_calibration >= 5) {
            bm_ctrl_info.gauge_calibration = 5;
        }
    } else if (pmu_get_charger_state() != 0x2 && bm_ctrl_info.gauge_calibration > 0) {
        gauge_timer_count++;
        if((bm_ctrl_info.feature.charger_option==1)&&(bm_ctrl_info.isChargerExist==1)){
            bm_ctrl_info.gauge_calibration =5;
        }else if (gauge_timer_count == gauge_regular_second_time) {
            bm_ctrl_info.gauge_calibration -= 1;
            gauge_timer_count = 0;
        }
        if (bm_ctrl_info.gauge_calibration <= 0) {
            bm_ctrl_info.gauge_calibration = 0;
        }
    }
}
/* ste 0:Vbus plug-out
 * ste 1:Vbus plug-in */
void battery_switch_calibration_state(uint8_t ste){
    if(ste){
        if (pmu_auxadc_get_channel_value(PMU_AUX_BATSNS) > (bm_cust.full_bat - bm_cust.full_bat_offset)) {
            bm_ctrl_info.gauge_calibration = ((pmu_auxadc_get_channel_value(PMU_AUX_BATSNS) - (bm_cust.full_bat - bm_cust.full_bat_offset)) / 10) + 1;
        } else {
            bm_ctrl_info.gauge_calibration = 0;
        }
    }else{
        if (pmu_auxadc_get_channel_value(PMU_AUX_BATSNS) > (bm_cust.full_bat - bm_cust.full_bat_offset) && bm_ctrl_info.gauge_calibration == 0) {
            bm_ctrl_info.gauge_calibration = ((pmu_auxadc_get_channel_value(PMU_AUX_BATSNS) - (bm_cust.full_bat - bm_cust.full_bat_offset)) / 10);
        } else if (pmu_auxadc_get_channel_value(PMU_AUX_BATSNS) < (bm_cust.full_bat - bm_cust.full_bat_offset)) {
            vTaskSuspend(battery_regular_task_t);
        }
        gauge_timer_count = 0;
    }
}
/*How many seconds will be divided into six times to reaching 100% */
void battery_set_calibration_time(void) {
    if((CHARGER_TOLERANCE_TIME * 60)<=CHARGER_REGULAR_TIME){
        gauge_regular_second_time = 1;
    }else{
        gauge_regular_second_time = ((CHARGER_TOLERANCE_TIME * 60) / CHARGER_REGULAR_TIME) / 6;
    }
}

uint32_t battery_get_linear_gauge_percent(void) {
    uint32_t interpolationVoltage = 0;
    interpolationVoltage = battery_core_gauge_function(pmu_auxadc_get_channel_value(PMU_AUX_BATSNS));
    return interpolationVoltage;
}
uint32_t battery_get_linear_gauge_percent_level(void){
    return (int)(battery_get_linear_gauge_percent()/10);
}
unsigned char battery_gauge_get_refernece_index(signed short vBatSnsValue) {
    unsigned char index;
    for (index = 0; index < BATTERY_VOLTAGE_REFERENCE_POINTS; index++) {
        if (vBatSnsValue < voltage_table[index]) {
            break;
        }
    }
    return index;
}

uint32_t battery_core_gauge_function(signed short vBatSnsValue)
{
    unsigned char index = 0;
    float slope;
    uint32_t interPolationValue = 0;
    short xAxisDiff = 0;
    voltage_table = bm_cust.check_point;
    index = battery_gauge_get_refernece_index(vBatSnsValue);
    slope = battery_core_gauge_indexCalc(index);
    if (index == 0) {
        xAxisDiff = (float) (vBatSnsValue - bm_cust.shutdown_bat);
        interPolationValue = 0 + slope * xAxisDiff;
    } else if (index == BATTERY_VOLTAGE_REFERENCE_POINTS) {
        xAxisDiff = (float) (vBatSnsValue - voltage_table[BATTERY_VOLTAGE_REFERENCE_POINTS - 1]);
        interPolationValue = 90 + slope * xAxisDiff;
        if (pmu_auxadc_get_channel_value(PMU_AUX_BATSNS) > (bm_cust.full_bat - bm_cust.full_bat_offset)) {
            interPolationValue = GAUGE_TOLERANCE_PERCENT+bm_ctrl_info.gauge_calibration;
        }
        if(interPolationValue>100){
            interPolationValue = 100;
        }
    } else {
        xAxisDiff = (float) (vBatSnsValue - voltage_table[index - 1]);
        interPolationValue = (index * 10) + slope * xAxisDiff;
    }
    return interPolationValue;
}

float battery_core_gauge_indexCalc(unsigned char index) {
    short dataX0, dataX1 = 0;
    short dataY0, dataY1 = 0;
    float slope;

    if (index == 0) {
        dataX0 = bm_cust.shutdown_bat;
        dataX1 = voltage_table[0];

        dataY0 = 0;
        dataY1 = 10;
    } else if (index == BATTERY_VOLTAGE_REFERENCE_POINTS) {
        dataX0 = voltage_table[BATTERY_VOLTAGE_REFERENCE_POINTS - 1];
        dataX1 = bm_cust.full_bat;

        dataY0 = BATTERY_VOLTAGE_REFERENCE_POINTS * 10;
        dataY1 = 100;
    } else {
        dataX0 = voltage_table[index - 1];
        dataX1 = voltage_table[index];
        dataY0 = index * 10;
        dataY1 = (index + 1) * 10;
    }

    if (dataX1 == dataX0)
        return 0.0;

    slope = ((float) (dataY1 - dataY0)) / ((float) (dataX1 - dataX0));
    return slope;
}


