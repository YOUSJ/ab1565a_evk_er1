/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef USBAUDIO_DRV_H
#define USBAUDIO_DRV_H

#include "usb.h"
#include "usb_comm.h"
#include "usb_custom.h"
#include "usb_custom_def.h"
#include "bmd.h"

/***********************************************
    audio class specific command definition
************************************************/
#if defined(MTK_USB_AUDIO_V2_ENABLE)
#define USB_AUDIO_DEVICE_CODE              0xEF
#define USB_AUDIO_SUBCLASS_CODE            0x02
#define USB_AUDIO_PROTOCOL_CODE            0x01
#else
#define USB_AUDIO_DEVICE_CODE              0x00
#define USB_AUDIO_SUBCLASS_CODE            0x00
#define USB_AUDIO_PROTOCOL_CODE            0x00
#endif

/* Class-Specfic Codes ,EP0*/
#define USB_AUDIO_GET_CUR	0x81
#define USB_AUDIO_GET_MIN	0x82
#define USB_AUDIO_GET_MAX	0x83
#define USB_AUDIO_GET_RES	0x84
#define USB_AUDIO_SET_CUR	0x01

typedef enum {
    USB_AUDIO_MUTE_OFF = 0,
    USB_AUDIO_MUTE_ON  = 1
} usb_audio_mute_t;

typedef void (*AUDIO_RX_FUNC)(void);
typedef void (*AUDIO_TX_FUNC)(void);
typedef void (*AUDIO_SETINTERFACE_FUNC)(uint8_t bInterfaceNumber, uint8_t bAlternateSetting);
typedef void (*AUDIO_SETSAMPLINGRATE_FUNC)(uint8_t ep_number, uint32_t sampling_rate);
typedef void (*AUDIO_UNPLUG_FUNC)(void);
typedef void (*AUDIO_VOLUMECHANGE_FUNC)(uint8_t ep_number, uint8_t channel, uint32_t volume);
typedef void (*AUDIO_MUTE_FUNC)(uint8_t ep_number, usb_audio_mute_t mute);

/* Audio device structure */
typedef struct
{
    uint8_t             stream_interface_id;
    Usb_EpBOut_Status	*rxpipe; /* bulk out EP, Data interface */
    Usb_Ep_Info	        *stream_ep_out_info;
    Usb_Interface_Info	*control_if_info;
    Usb_Interface_Info	*stream_if_info;
    Usb_IAD_Dscr        *iad_dscr;
    /* buffer for read DMA operation*/
    uint8_t             *rx_dma_buffer;
    uint32_t            rx_dma_buffer_len;
    volatile uint32_t   rx_dma_buffer_is_full;
    volatile uint32_t   rx_dma_buffer_read;
    volatile uint32_t   rx_dma_buffer_write;
    volatile uint32_t   msg_notify;
#ifdef MTK_USB_AUDIO_MICROPHONE
    uint8_t             stream_microphone_interface_id;
    Usb_EpBOut_Status   *txpipe; /* bulk in EP, Data interface */
    Usb_Ep_Info         *stream_ep_in_info;
    Usb_Interface_Info	*stream_microphone_if_info;
    /* buffer for read DMA operation*/
    uint8_t             *tx_dma_buffer;
    uint32_t            tx_dma_buffer_len;
    volatile uint32_t   tx_dma_buffer_is_full;
    volatile uint32_t   tx_dma_buffer_read;
    volatile uint32_t   tx_dma_buffer_write;
#endif
}UsbAudio_Struct;

typedef struct
{
    /* Customizable variables */
    const USB_AUDIO_PARAM	*audio_param;
}UsbAudio_Common_Struct;

typedef struct
{
    bool                        initialized;
    AUDIO_RX_FUNC               rx_cb;
    AUDIO_TX_FUNC               tx_cb;
    AUDIO_SETINTERFACE_FUNC     setinterface_cb;
    AUDIO_SETSAMPLINGRATE_FUNC  setsamplingrate_cb;
    AUDIO_UNPLUG_FUNC           unplug_cb;
    AUDIO_VOLUMECHANGE_FUNC     volumechange_cb;
    AUDIO_MUTE_FUNC             mute_cb;
#ifdef MTK_USB_AUDIO_MICROPHONE
    AUDIO_SETINTERFACE_FUNC     setinterface_cb_mic;
    AUDIO_SETSAMPLINGRATE_FUNC  setsamplingrate_cb_mic;
    AUDIO_UNPLUG_FUNC           unplug_cb_mic;
    AUDIO_VOLUMECHANGE_FUNC     volumechange_cb_mic;
    AUDIO_MUTE_FUNC             mute_cb_mic;
#endif
}UsbAudioStruct;

/***********************************************
	function and global variable
************************************************/
extern UsbAudio_Common_Struct g_UsbAudio_Comm;
extern UsbAudioStruct USB_Audio;
extern UsbAudio_Struct g_UsbAudio;

void USB_Audio_ControlIf_Create(void *ifname);
void USB_Audio_ControlIf_Reset(void);
void USB_Audio_ControlIf_Enable(void);
void USB_Audio_ControlIf_Speed_Reset(bool b_other_speed);
void USB_Audio_StreamIf_Create(void *ifname);
void USB_Audio_StreamIf_Reset(void);
void USB_Audio_StreamIf_Enable(void);
void USB_Audio_StreamIf_Speed_Reset(bool b_other_speed);
#ifdef MTK_USB_AUDIO_MICROPHONE
void USB_Audio_StreamIf_Microphone_Create(void *ifname);
void USB_Audio_StreamIf_Microphone_Reset(void);
void USB_Audio_StreamIf_Microphone_Enable(void);
void USB_Audio_StreamIf_Microphone_Speed_Reset(bool b_other_speed);
#endif
void USB_Audio_Ep0_Command(Usb_Ep0_Status *pep0state, Usb_Command *pcmd);
void USB_Audio_IsoOut_Hdr(void);
void USB_Audio_IsoOut_Reset(void);
#ifdef MTK_USB_AUDIO_MICROPHONE
void USB_Audio_IsoIn_Hdr(void);
void USB_Audio_IsoIn_Reset(void);
#endif
void USB_Init_Audio_Status(void);
void USB_Release_Audio_Status(void);
void USB_Audio_Register_Rx_Callback(AUDIO_RX_FUNC rx_cb);
void USB_Audio_Register_Tx_Callback(AUDIO_TX_FUNC tx_cb);
void USB_Audio_Register_SetInterface_Callback(AUDIO_SETINTERFACE_FUNC setinterface_cb);
void USB_Audio_Register_SetSamplingRate_Callback(AUDIO_SETSAMPLINGRATE_FUNC setsamplingrate_cb);
void USB_Audio_Register_Unplug_Callback(AUDIO_UNPLUG_FUNC unplug_cb);
void USB_Audio_Register_VolumeChange_Callback(AUDIO_VOLUMECHANGE_FUNC volumechange_cb);
void USB_Audio_Register_Mute_Callback(AUDIO_MUTE_FUNC mute_cb);
#ifdef MTK_USB_AUDIO_MICROPHONE
void USB_Audio_Register_Mic_SetInterface_Callback(AUDIO_SETINTERFACE_FUNC setinterface_cb);
void USB_Audio_Register_Mic_SetSamplingRate_Callback(AUDIO_SETSAMPLINGRATE_FUNC setsamplingrate_cb);
void USB_Audio_Register_Mic_Unplug_Callback(AUDIO_UNPLUG_FUNC unplug_cb);
void USB_Audio_Register_Mic_VolumeChange_Callback(AUDIO_VOLUMECHANGE_FUNC volumechange_cb);
void USB_Audio_Register_Mic_Mute_Callback(AUDIO_MUTE_FUNC mute_cb);
#endif
uint32_t USB_Audio_Get_CurSamplingRate(void);
#ifdef MTK_USB_AUDIO_MICROPHONE
uint32_t USB_Audio_Get_CurSamplingRate_Microphone(void);
#endif
void* USB_Audio_Rx_Buffer_Get_Read_Address(void);
uint32_t USB_Audio_Rx_Buffer_Get_Bytes(void);
void USB_Audio_Rx_Buffer_Drop_Bytes(uint32_t bytes);
uint32_t USB_Audio_Get_Len_Received_Data(void);
uint32_t USB_Audio_Read_Data(void* dst, uint32_t len);

#endif /* USBAUDIO_DRV_H */

