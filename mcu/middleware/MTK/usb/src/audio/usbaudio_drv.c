/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifdef MTK_USB_DEMO_ENABLED

#include <stdint.h>
#include <stdio.h>
#include <string.h>

/* Kernel includes. */
#include "hal.h"
#include "memory_attribute.h"
#include "hal_usb.h"
#include "usb_comm.h"
#include "usb.h"
#include "usb_resource.h"
#include "usbaudio_drv.h"
#include "syslog.h"

#if defined(MTK_USB_AUDIO_V1_ENABLE) && defined(MTK_USB_AUDIO_V2_ENABLE)
    #error "MTK_USB_AUDIO_V1_ENABLE and MTK_USB_AUDIO_V2_ENABLE can't be defined at the same time."
#endif

#define USB_AUDIO_RX_BUFFER_SIZE 2048
#define USB_AUDIO_TX_BUFFER_SIZE 2048

/* Global variables */
UsbAudio_Common_Struct g_UsbAudio_Comm;
UsbAudioStruct USB_Audio;
UsbAudio_Struct g_UsbAudio;

#if defined(MTK_USB_AUDIO_V2_ENABLE)
#define USB_AUDIO_INTERFACE_PROTOCOL 0x20
#else
#define USB_AUDIO_INTERFACE_PROTOCOL 0x00
#endif

#if defined(MTK_USB_AUDIO_V2_ENABLE)
const uint8_t audio_iad_dscr[] = {
    USB_IAD_LENGTH, //bLength;
    USB_INTERFACE_ASSOCIATION, //bDescriptorType;
    0x00, //bFirstInterface;
#ifndef MTK_USB_AUDIO_MICROPHONE
    0x02, //bInterfaceCount;
#else
    0x03, //bInterfaceCount;
#endif
    0x01, //bFunctionClass; AUDIO_FUNCTION
    0x00, //bFunctionSubClass;
    0x20, //bFunctionProtocol; AF_VERSION_02_00
    0x00, //iFunction;
};
#endif

#ifdef MTK_USB_AUDIO_V2_ENABLE
static const uint8_t audio_control_if_dscr[] = {
    USB_IFDSC_LENGTH, //bLength;
    USB_INTERFACE, //bDescriptorType;
    0x00, //bInterfaceNumber;
    0x00, //bAlternateSetting;
    0x00, //bNumEndpoints;
    0x01, //bInterfaceClass;
    0x01, //bInterfaceSubClass;
    USB_AUDIO_INTERFACE_PROTOCOL, //bInterfaceProtocol; IP_VERSION_02_00: UAC2 or 0x00 UAC1
    0x00, //iInterface;

    0x09, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x01, //bDescriptorSubtype; Header
    0x00, //bcdADC; LSB
    0x02, //bcdADC; MSB
    0x00, //bCategory
#ifndef MTK_USB_AUDIO_MICROPHONE
    0x40, //wTotalLength; LSB = 0x09+0x11+0x12+0x0C+0x08=0x40
#else
    0x77, //wTotalLength; LSB = 0x09+(0x11+0x12+0x0C+0x08)*2=0x77
#endif
    0x00, //wTotalLength; MSB
    0x00, //bmControls

    0x11, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x02, //bDescriptorSubtype; Input Terminal
    0x01, //bTerminalID
    0x01, //wTerminalType; USB streaming
    0x01,
    0x00, //bAssocTerminal
    0x04, //bCSourceID
    0x02, //bNrChannels; 2 channels
    0x03, //bmChannelConfig; (L, R)
    0x00,
    0x00,
    0x00,
    0x00, //iChannelNames
    0x00, //bmControls
    0x00,
    0x00, //iTerminal

    0x12, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x06, //bDescriptorSubtype; Feature Unit
    0x02, //bUnitID
    0x01, //bSourceID
    0x00, //bmaControls(0); Master Channel (set to 0x03 for supporting Mute)
    0x00,
    0x00,
    0x00,
    0x00, //bmaControls(1); Logical Channel 1 (set to 0x0C for supporting Volume)
    0x00,
    0x00,
    0x00,
    0x00, //bmaControls(2); Logical Channel 2 (set to 0x0C for supporting Volume)
    0x00,
    0x00,
    0x00,
    0x00, //iFeature */

    0x0C, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x03, //bDescriptorSubtype; Output Terminal
    0x03, //bTerminalID
    0x01, //wTerminalType; Speaker
    0x03,
    0x00, //bAssocTerminal
    0x02, //bSourceID
    0x04, //bCSourceID
    0x00, //bmControls
    0x00,
    0x00, //iTerminal

    0x08, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x0A, //bDescriptorSubtype; Clock Source
    0x04, //bClockID
    0x00, //bmAttributes
    0x03, //bmControls; Clock Frequency Control Read / Write;
    0x01, //bAssocTerminal
    0x00, //iClockSource

#ifdef MTK_USB_AUDIO_MICROPHONE
    0x11, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x02, //bDescriptorSubtype; Input Terminal
    0x05, //bTerminalID
    0x01, //wTerminalType; Microphone (LSB)
    0x02, //wTerminalType; Microphone (MSB)
    0x00, //bAssocTerminal
    0x08, //bCSourceID
    0x02, //bNrChannels; 2 channels
    0x03, //bmChannelConfig; (L, R)
    0x00,
    0x00,
    0x00,
    0x00, //iChannelNames
    0x00, //bmControls
    0x00,
    0x00, //iTerminal

    0x12, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x06, //bDescriptorSubtype; Feature Unit
    0x06, //bUnitID
    0x05, //bSourceID
    0x00, //bmaControls(0); Master Channel (set to 0x03 for supporting Mute)
    0x00,
    0x00,
    0x00,
    0x00, //bmaControls(1); Logical Channel 1 (set to 0x0C for supporting Volume)
    0x00,
    0x00,
    0x00,
    0x00, //bmaControls(2); Logical Channel 2 (set to 0x0C for supporting Volume)
    0x00,
    0x00,
    0x00,
    0x00, //iFeature */

    0x0C, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x03, //bDescriptorSubtype; Output Terminal
    0x07, //bTerminalID
    0x01, //wTerminalType; USB streaming
    0x01,
    0x00, //bAssocTerminal
    0x06, //bSourceID
    0x08, //bCSourceID
    0x00, //bmControls
    0x00,
    0x00, //iTerminal

    0x08, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x0A, //bDescriptorSubtype; Clock Source
    0x08, //bClockID
    0x00, //bmAttributes
    0x03, //bmControls; Clock Frequency Control Read / Write;
    0x07, //bAssocTerminal
    0x00, //iClockSource
#endif // MTK_USB_AUDIO_MICROPHONE
};
#endif

#ifdef MTK_USB_AUDIO_V1_ENABLE
static const uint8_t audio_control_if_dscr[] = {
    USB_IFDSC_LENGTH, //bLength;
    USB_INTERFACE, //bDescriptorType;
    0x00, //bInterfaceNumber;
    0x00, //bAlternateSetting;
    0x00, //bNumEndpoints;
    0x01, //bInterfaceClass;
    0x01, //bInterfaceSubClass;
    USB_AUDIO_INTERFACE_PROTOCOL, //bInterfaceProtocol; IP_VERSION_02_00: UAC2 or 0x00 UAC1
    0x00, //iInterface;

#ifndef MTK_USB_AUDIO_MICROPHONE
    0x09, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x01, //bDescriptorSubtype; Header
    0x00, //bcdADC; LSB
    0x01, //bcdADC; MSB
    0x28, //wTotalLength; LSB = 0x09+0x0C+0x0A+0x09=40=0x28
    0x00, //wTotalLength; MSB
    0x01, //bInCollection
    0x01, //baInterfaceNr[0]
#else
    0x0A, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x01, //bDescriptorSubtype; Header
    0x00, //bcdADC; LSB
    0x01, //bcdADC; MSB
    0x48, //wTotalLength; LSB = 0x0A+(0x0C+0x0A+0x09)*2=72=0x48
    0x00, //wTotalLength; MSB
    0x02, //bInCollection
    0x01, //baInterfaceNr[0]
    0x02, //baInterfaceNr[1]
#endif

    0x0C, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x02, //bDescriptorSubtype; Input Terminal
    0x01, //bTerminalID
    0x01, //wTerminalType; USB streaming
    0x01,
    0x00, //bAssocTerminal
    0x02, //bNrChannels; 2 channels
    0x03, //wChannelConfig; (L, R)
    0x00,
    0x00, //iChannelNames
    0x00, //iTerminal

    0x0A, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x06, //bDescriptorSubtype; Feature Unit
    0x02, //bUnitID
    0x01, //bSourceID
    0x01, //bControlSize
    0x01, //bmaControls(0); Master Channel support Mute
    0x02, //bmaControls(1); Logical Channel 1 support Volume
    0x02, //bmaControls(2); Logical Channel 2 support Volume
    0x00, //iFeature */

    0x09, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x03, //bDescriptorSubtype; Output Terminal
    0x03, //bTerminalID
    0x01, //wTerminalType; Speaker
    0x03,
    0x00, //bAssocTerminal
    0x02, //bSourceID
    0x00, //iTerminal

#ifdef MTK_USB_AUDIO_MICROPHONE
    0x0C, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x02, //bDescriptorSubtype; Input Terminal
    0x05, //bTerminalID
    0x01, //wTerminalType; Microphone (LSB)
    0x02, //wTerminalType; Microphone (MSB)
    0x00, //bAssocTerminal
    0x02, //bNrChannels; 2 channels
    0x03, //wChannelConfig; (L, R)
    0x00,
    0x00, //iChannelNames
    0x00, //iTerminal

    0x0A, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x06, //bDescriptorSubtype; Feature Unit
    0x06, //bUnitID
    0x05, //bSourceID
    0x01, //bControlSize
    0x01, //bmaControls(0); Master Channel support Mute
    0x02, //bmaControls(1); Logical Channel 1 support Volume
    0x02, //bmaControls(2); Logical Channel 2 support Volume
    0x00, //iFeature */

    0x09, //bLength
    0x24, //bDescriptorType; Audio Interface Descriptor
    0x03, //bDescriptorSubtype; Output Terminal
    0x07, //bTerminalID
    0x01, //wTerminalType; USB streaming
    0x01,
    0x00, //bAssocTerminal
    0x06, //bSourceID
    0x00, //iTerminal
#endif // MTK_USB_AUDIO_MICROPHONE
};
#endif

const uint8_t audio_stream_if_dscr[] = {
    USB_IFDSC_LENGTH, //bLength;
    USB_INTERFACE, //bDescriptorType;
    0x01, //bInterfaceNumber;
    0x00, //bAlternateSetting;
    0x00, //bNumEndpoints;
    0x01, //bInterfaceClass;
    0x02, //bInterfaceSubClass;
    USB_AUDIO_INTERFACE_PROTOCOL, //bInterfaceProtocol; IP_VERSION_02_00: UAC2 or 0x00 UAC1
    0x00, //iInterface;
};

#ifdef MTK_USB_AUDIO_MICROPHONE
const uint8_t audio_stream_microphone_if_dscr[] = {
    USB_IFDSC_LENGTH, //bLength;
    USB_INTERFACE, //bDescriptorType;
    0x02, //bInterfaceNumber;
    0x00, //bAlternateSetting;
    0x00, //bNumEndpoints;
    0x01, //bInterfaceClass;
    0x02, //bInterfaceSubClass;
    USB_AUDIO_INTERFACE_PROTOCOL, //bInterfaceProtocol; IP_VERSION_02_00: UAC2 or 0x00 UAC1
    0x00, //iInterface;
};
#endif

const uint8_t audio_stream_alt_if_dscr[] = {
    USB_IFDSC_LENGTH, //bLength;
    USB_INTERFACE, //bDescriptorType;
    0x01, //bInterfaceNumber;
    0x01, //bAlternateSetting;
    0x01, //bNumEndpoints;
    0x01, //bInterfaceClass;
    0x02, //bInterfaceSubClass;
    USB_AUDIO_INTERFACE_PROTOCOL, //bInterfaceProtocol; IP_VERSION_02_00: UAC2 or 0x00 UAC1
    0x00, //iInterface;

#ifdef MTK_USB_AUDIO_V2_ENABLE
    0x10, //bLength
    0x24, //bDescriptorType;
    0x01, //bDescriptorSubtype;
    0x01, //bTerminalLink;
    0x00, //bmControls;
    0x01, //bFormatType; FORMAT_TYPE_I
    0x01, //bmFormats; PCM
    0x00,
    0x00,
    0x00,
    0x02, //bNrChannels; 2 channels
    0x03, //bmChannelConfig
    0x00,
    0x00,
    0x00,
    0x00,
#else
    0x07, //bLength;
    0x24, //bDescriptorType;
    0x01, //bDescriptorSubtype;
    0x01, //bTerminalLink;
    0x01, //bDelay;
    0x01, //wFormatTag;LSB PCM
    0x00, //wFormatTag;MSB PCM
#endif

#ifdef MTK_USB_AUDIO_V2_ENABLE
    0x06, //bLength;
    0x24, //bDescriptorType; Audio
    0x02, //bDescriptorSubtype; Format
    0x01, //bFormatType; FORMAT_TYPE_I
    0x02, //bSubslotSize; 2 bytes
    0x10, //bBitResolution; 16 bites
#else
#if 1
    0x0B, //bLength
#else
    0x0E, //bLength;
#endif
    0x24, //bDescriptorType; Audio
    0x02, //bDescriptorSubtype; Format
    0x01, //bFormatType; FORMAT_TYPE_I
    0x02, //bNrChannels; 2 channels
    0x02, //nSubframeSize; 2 bytes per subframe
    0x10, //bBitResolution; 16 bits per sample
#if 1
    0x01, //bSamFreqType; supports 1 sample frequencies
    0x80, //tSamFreq[1]; 48000 Hz
    0xBB,
    0x00,
#else
    0x02, //bSamFreqType; supports 2 sample frequencies
    0x80, //tSamFreq[1]; 48000 Hz
    0xBB,
    0x00,
    0x44, //tSamFreq[1]; 44100 Hz
    0xAC,
    0x00,
#endif
#endif
};

#ifdef MTK_USB_AUDIO_MICROPHONE
const uint8_t audio_stream_microphone_alt_if_dscr[] = {
    USB_IFDSC_LENGTH, //bLength;
    USB_INTERFACE, //bDescriptorType;
    0x02, //bInterfaceNumber;
    0x01, //bAlternateSetting;
    0x01, //bNumEndpoints;
    0x01, //bInterfaceClass;
    0x02, //bInterfaceSubClass;
    USB_AUDIO_INTERFACE_PROTOCOL, //bInterfaceProtocol; IP_VERSION_02_00: UAC2 or 0x00 UAC1
    0x00, //iInterface;

#ifdef MTK_USB_AUDIO_V2_ENABLE
    0x10, //bLength
    0x24, //bDescriptorType;
    0x01, //bDescriptorSubtype;
    0x07, //bTerminalLink;
    0x00, //bmControls;
    0x01, //bFormatType; FORMAT_TYPE_I
    0x01, //bmFormats; PCM
    0x00,
    0x00,
    0x00,
    0x02, //bNrChannels; 2 channels
    0x03, //bmChannelConfig
    0x00,
    0x00,
    0x00,
    0x00,
#else
    0x07, //bLength;
    0x24, //bDescriptorType;
    0x01, //bDescriptorSubtype;
    0x07, //bTerminalLink;
    0x01, //bDelay;
    0x01, //wFormatTag;LSB PCM
    0x00, //wFormatTag;MSB PCM
#endif

#ifdef MTK_USB_AUDIO_V2_ENABLE
    0x06, //bLength;
    0x24, //bDescriptorType; Audio
    0x02, //bDescriptorSubtype; Format
    0x01, //bFormatType; FORMAT_TYPE_I
    0x02, //bSubslotSize; 2 bytes
    0x10, //bBitResolution; 16 bites
#else
#if 1
    0x0B, //bLength;
#else
    0x0E, //bLength;
#endif
    0x24, //bDescriptorType; Audio
    0x02, //bDescriptorSubtype; Format
    0x01, //bFormatType; FORMAT_TYPE_I
    0x02, //bNrChannels; 2 channels
    0x02, //nSubframeSize; 2 bytes per subframe
    0x10, //bBitResolution; 16 bits per sample
#if 1
    0x01, //bSamFreqType; supports 1 sample frequencies
    0x80, //tSamFreq[1]; 16000 Hz
    0x3E,
    0x00,
#else
    0x02, //bSamFreqType; supports 2 sample frequencies
    0x80, //tSamFreq[1]; 48000 Hz
    0xBB,
    0x00,
    0x44, //tSamFreq[1]; 44100 Hz
    0xAC,
    0x00,
#endif
#endif
};
#endif

const uint8_t audio_stream_ep_out_dscr[] = { // speaker usage
    0x09, //bLength;
    USB_ENDPOINT, //bDescriptorType;
    0x00, //bEndpointAddress;
    USB_EP_ISO | (USB_ISO_SYNCHRONOUS << 2), //bmAttributes;
    0xC8, //wMaxPacketSize[2]; // Will be replaced by HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_HIGH_SPEED &
    0x00, //wMaxPacketSize[2]; // HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_FULL_SPEED.
    0x01, //bInterval; // Will be changed to 0x04 if high-speed @ USB_Audio_StreamIf_Speed_Reset().
    0x00, //bRefresh;
    0x00, //bSynchAddress;

#ifdef MTK_USB_AUDIO_V2_ENABLE
    0x08, //bLength
    0x25, //bDescriptorType; Audio Endpoint Descriptor
    0x01, //bDescriptorSubtype; General
    0x00, //bmAttributes;
    0x00, //bmControls;
    0x00, //bLockDelayUnits
    0x00, //wLockDelay; LSB
    0x00, //wLockDelay; MSB
#else
    0x07, //bLength
    0x25, //bDescriptorType; Audio Endpoint Descriptor
    0x01, //bDescriptorSubtype; General
    0x01, //bmAttributes; Sampling Frequency
    0x00, //bLockDelayUnits
    0x00, //wLockDelay; LSB
    0x00, //wLockDelay; MSB
#endif
};

#ifdef MTK_USB_AUDIO_MICROPHONE
const uint8_t audio_stream_ep_in_dscr[] = { // microphone usage
    0x09, //bLength;
    USB_ENDPOINT, //bDescriptorType;
    0x00, //bEndpointAddress;
    USB_EP_ISO | (USB_ISO_SYNCHRONOUS << 2), //bmAttributes;
    0xC8, //wMaxPacketSize[2]; // Will be replaced by HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_HIGH_SPEED &
    0x00, //wMaxPacketSize[2]; // HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_FULL_SPEED.
    0x01, //bInterval; // Will be changed to 0x04 if high-speed @ USB_Audio_StreamIf_Speed_Reset().
    0x00, //bRefresh;
    0x00, //bSynchAddress;

#ifdef MTK_USB_AUDIO_V2_ENABLE
    0x08, //bLength
    0x25, //bDescriptorType; Audio Endpoint Descriptor
    0x01, //bDescriptorSubtype; General
    0x00, //bmAttributes;
    0x00, //bmControls;
    0x00, //bLockDelayUnits
    0x00, //wLockDelay; LSB
    0x00, //wLockDelay; MSB
#else
    0x07, //bLength
    0x25, //bDescriptorType; Audio Endpoint Descriptor
    0x01, //bDescriptorSubtype; General
    0x01, //bmAttributes; Sampling Frequency
    0x00, //bLockDelayUnits
    0x00, //wLockDelay; LSB
    0x00, //wLockDelay; MSB
#endif
};
#endif

#ifdef MTK_USB_AUDIO_V2_ENABLE
#if 1 // support 16K only for now
const uint8_t uac2_range_sampling_freqency[]={
    0x01,
    0x00,
    0x80, //Max 16000 Hz
    0x3E,
    0x00,
    0x00,
    0x80, //Min 16000 Hz
    0x3E,
    0x00,
    0x00,
    0x00, //Res 0
    0x00,
    0x00,
    0x00,
};
#else
const uint8_t uac2_range_sampling_freqency[]={
    0x04,
    0x00,
    0x40, //Max 8000 Hz
    0x1F,
    0x00,
    0x00,
    0x40, //Min 8000 Hz
    0x1F,
    0x00,
    0x00,
    0x00, //Res 0
    0x00,
    0x00,
    0x00,
    0x80, //Max 16000 Hz
    0x3E,
    0x00,
    0x00,
    0x80, //Min 16000 Hz
    0x3E,
    0x00,
    0x00,
    0x00, //Res 0
    0x00,
    0x00,
    0x00,
    0x44, //Max 44100 Hz
    0xAC,
    0x00,
    0x00,
    0x44, //Min 44100 Hz
    0xAC,
    0x00,
    0x00,
    0x00, //Res 0
    0x00,
    0x00,
    0x00,
    0x80, //Max 48000 Hz
    0xBB,
    0x00,
    0x00,
    0x80, //Min 48000 Hz
    0xBB,
    0x00,
    0x00,
    0x00, //Res 0
    0x00,
    0x00,
    0x00,
};
#endif

const uint8_t uac2_range_volume[]={
    0x01,
    0x00,
    0x00,
    0x00,
    0xFF,
    0x00,
    0x01,
    0x00,
};

#ifdef MTK_USB_AUDIO_MICROPHONE
const uint8_t uac2_microphone_range_sampling_freqency[]={
    0x01,
    0x00,
    0x80, //Max 16000 Hz
    0x3E,
    0x00,
    0x00,
    0x80, //Min 16000 Hz
    0x3E,
    0x00,
    0x00,
    0x00, //Res 0
    0x00,
    0x00,
    0x00,
};
#endif

const uint8_t uac2_microphone_range_volume[]={
    0x01,
    0x00,
    0x00,
    0x00,
    0xFF,
    0x00,
    0x01,
    0x00,
};
#endif

hal_usb_status_t hal_usb_configure_rx_endpoint_type(uint32_t ep_num, hal_usb_endpoint_transfer_type_t ep_type, bool b_is_use_dma);

static unsigned int USB_Audio_Stream_Speed_Reset_Packet_Size(bool b_other_speed)
{
    unsigned int  max_packet;

    if (hal_usb_is_high_speed() == true) {

        if (b_other_speed == false) {
            max_packet = HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_HIGH_SPEED;
        } else {
            max_packet = HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_FULL_SPEED;
        }
    } else {
        if (b_other_speed == false) {
            max_packet = HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_FULL_SPEED;
        } else {
            max_packet = HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_HIGH_SPEED;
        }
    }

    return max_packet;
}

/************************************************************
    Interface initialization functions
*************************************************************/
/* Audio control interface create function, prepare descriptor */
void USB_Audio_ControlIf_Create(void *ifname)
{
    uint8_t if_id;

    /* Get resource number and register to gUsbDevice */
    g_UsbAudio.control_if_info = USB_Get_Interface_Number(&if_id);

    /* Record interface name and interface descriptor length */
    g_UsbAudio.control_if_info->interface_name_ptr = (char *)ifname;
    g_UsbAudio.control_if_info->ifdscr_size = sizeof(audio_control_if_dscr);

    /* Related endpoint info and class specific command handler */
    g_UsbAudio.control_if_info->if_class_specific_hdlr = USB_Audio_Ep0_Command;

    memcpy(&(g_UsbAudio.control_if_info->ifdscr.stdif), audio_control_if_dscr, sizeof(audio_control_if_dscr));

    /* Standard interface descriptor */
    ((Usb_If_Dscr *)g_UsbAudio.control_if_info->ifdscr.classif)->bInterfaceNumber = if_id;

#if defined(MTK_USB_AUDIO_V2_ENABLE)
    g_UsbAudio.iad_dscr = USB_Get_IAD_Number();
    memcpy(g_UsbAudio.iad_dscr, audio_iad_dscr, sizeof(audio_iad_dscr));
#endif
}

/* Audio stream interface reset function, enable EP */
void USB_Audio_ControlIf_Reset(void)
{
}

void USB_Audio_ControlIf_Enable(void)
{
}

/* Audio stream interface speed reset function, enable EP's speed-specific descriptor */
void USB_Audio_ControlIf_Speed_Reset(bool b_other_speed)
{
}

/* Audio stream interface create function, prepare descriptor */
void USB_Audio_StreamIf_Create(void *ifname)
{
    uint8_t ep_rx_id;
    uint8_t if_id;

    LOG_MSGID_I(common, "USB_Audio_StreamIf_Create()\n", 0);

    /* Get resource number and register to gUsbDevice */
    if (g_UsbAudio.stream_interface_id == 0xFF) { //check has already register or not
        g_UsbAudio.stream_if_info = USB_Get_Interface_Number(&if_id);
        g_UsbAudio.stream_interface_id = if_id;
    }

    g_UsbAudio.stream_ep_out_info = USB_Get_Iso_Rx_Ep(&ep_rx_id);
    g_UsbAudio.stream_if_info->alternate_if_info[0].ep_info[0] = g_UsbAudio.stream_ep_out_info;

    /* Record interface name and interface descriptor length */
    g_UsbAudio.stream_if_info->interface_name_ptr = (char *)ifname;
    g_UsbAudio.stream_if_info->ifdscr_size = sizeof(audio_stream_if_dscr);
    g_UsbAudio.stream_if_info->alternate_if_info[0].ifdscr_size = sizeof(audio_stream_alt_if_dscr);
    g_UsbAudio.stream_if_info->alternate_if_info[0].ep_info[0]->epdscr_size = sizeof(audio_stream_ep_out_dscr);

    /* Related endpoint info and class specific command handler*/
    g_UsbAudio.stream_if_info->alternate_if_info[0].ep_info[0] = g_UsbAudio.stream_ep_out_info;
    g_UsbAudio.stream_if_info->if_class_specific_hdlr = USB_Audio_Ep0_Command; // Command: "SetCur" for "Sampling freq".

    memcpy(&(g_UsbAudio.stream_if_info->ifdscr.stdif), audio_stream_if_dscr, sizeof(audio_stream_if_dscr));
    memcpy(&(g_UsbAudio.stream_if_info->alternate_if_info[0].ifdscr.stdif), audio_stream_alt_if_dscr, sizeof(audio_stream_alt_if_dscr));
    memcpy(&(g_UsbAudio.stream_ep_out_info->epdesc.stdep), audio_stream_ep_out_dscr, sizeof(audio_stream_ep_out_dscr));

    /* Standard interface descriptor */
    g_UsbAudio.stream_if_info->ifdscr.stdif.bInterfaceNumber = g_UsbAudio.stream_interface_id;
    g_UsbAudio.stream_ep_out_info->ep_reset = USB_Audio_IsoOut_Reset;

    /* RX Endpoint handler */
    hal_usb_register_driver_callback(HAL_USB_DRV_HDLR_EP_RX, ep_rx_id, USB_Audio_IsoOut_Hdr);

    /* Endpoint descriptor */
    g_UsbAudio.stream_ep_out_info->epdesc.stdep.bEndpointAddress = (USB_EP_DIR_OUT | ep_rx_id); /*OutPipe*/
    g_UsbAudio.stream_ep_out_info->ep_status.epout_status.byEP = ep_rx_id;

    if(g_UsbAudio.rx_dma_buffer == NULL){
        LOG_MSGID_I(common, "USB_Audio_StreamIf_Create() rx_dma_buffer size=%d\n", 1, HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_HIGH_SPEED);
        g_UsbAudio.rx_dma_buffer = (uint8_t*)USB_Get_Memory(USB_AUDIO_RX_BUFFER_SIZE);
        g_UsbAudio.rx_dma_buffer_len = USB_AUDIO_RX_BUFFER_SIZE;
    }

    hal_usb_get_dma_channel(0, ep_rx_id, HAL_USB_EP_DIRECTION_RX, false);
}

/* Audio stream interface reset function, enable EP */
void USB_Audio_StreamIf_Reset(void)
{
    LOG_MSGID_I(common, "USB_Audio_StreamIf_Reset()\n", 0);

    g_UsbAudio.rxpipe = &g_UsbAudio.stream_ep_out_info->ep_status.epout_status;

    /* Stop DMA */
    hal_usb_stop_dma_channel(g_UsbAudio.rxpipe->byEP, HAL_USB_EP_DIRECTION_RX);
}

void USB_Audio_StreamIf_Enable(void)
{
    LOG_MSGID_I(common, "USB_Audio_StreamIf_Enable()\n", 0);

    /* Stop DMA */
    /* Compliance test tool will set configuration , but no reset */
    hal_usb_stop_dma_channel(g_UsbAudio.rxpipe->byEP, HAL_USB_EP_DIRECTION_RX);

    /*Non-DMA*/
    hal_usb_enable_rx_endpoint(g_UsbAudio.rxpipe->byEP, HAL_USB_EP_TRANSFER_ISO, HAL_USB_EP_USE_NO_DMA, true);
}

/* Audio stream interface speed reset function, enable EP's speed-specific descriptor */
void USB_Audio_StreamIf_Speed_Reset(bool b_other_speed)
{
#if !defined(MTK_USB11)
    uint32_t temp_max_size;
    temp_max_size = USB_Audio_Stream_Speed_Reset_Packet_Size((bool)b_other_speed);

    g_UsbAudio.stream_ep_out_info->epdesc.stdep.wMaxPacketSize[0] = temp_max_size & 0xff;
    g_UsbAudio.stream_ep_out_info->epdesc.stdep.wMaxPacketSize[1] = (temp_max_size >> 8) & 0xff;

    if (hal_usb_is_high_speed() == true) {
        g_UsbAudio.stream_ep_out_info->epdesc.stdep.bInterval = 4; // (2^(4-1))*0.125us=1ms
    }
    else{
        g_UsbAudio.stream_ep_out_info->epdesc.stdep.bInterval = 1; // (2^(1-1))*1ms=1ms
    }

    LOG_MSGID_I(common, "USB_Audio_StreamIf_Speed_Reset() wMaxPacketSize=%d b_other_speed=%d\n", 2, temp_max_size, b_other_speed);
#endif
}

#ifdef MTK_USB_AUDIO_MICROPHONE
/* Audio stream interface create function, prepare descriptor */
void USB_Audio_StreamIf_Microphone_Create(void *ifname)
{
    uint8_t ep_tx_id;
    uint8_t if_id;

    LOG_MSGID_I(common, "USB_Audio_StreamIf_Microphone_Create()\n", 0);

    /* Get resource number and register to gUsbDevice */
    if (g_UsbAudio.stream_microphone_interface_id == 0xFF) { //check has already register or not
        g_UsbAudio.stream_microphone_if_info = USB_Get_Interface_Number(&if_id);
        g_UsbAudio.stream_microphone_interface_id = if_id;
    }

    g_UsbAudio.stream_ep_in_info = USB_Get_Iso_Tx_Ep(&ep_tx_id);
    g_UsbAudio.stream_microphone_if_info->alternate_if_info[0].ep_info[0] = g_UsbAudio.stream_ep_in_info;

    /* Record interface name and interface descriptor length */
    g_UsbAudio.stream_microphone_if_info->interface_name_ptr = (char *)ifname;
    g_UsbAudio.stream_microphone_if_info->ifdscr_size = sizeof(audio_stream_microphone_if_dscr);
    g_UsbAudio.stream_microphone_if_info->alternate_if_info[0].ifdscr_size = sizeof(audio_stream_microphone_alt_if_dscr);
    g_UsbAudio.stream_microphone_if_info->alternate_if_info[0].ep_info[0]->epdscr_size = sizeof(audio_stream_ep_in_dscr);

    /* Related endpoint info and class specific command handler*/
    g_UsbAudio.stream_microphone_if_info->alternate_if_info[0].ep_info[0] = g_UsbAudio.stream_ep_in_info;
    g_UsbAudio.stream_microphone_if_info->if_class_specific_hdlr = USB_Audio_Ep0_Command; // Command: "SetCur" for "Sampling freq".

    memcpy(&(g_UsbAudio.stream_microphone_if_info->ifdscr.stdif), audio_stream_microphone_if_dscr, sizeof(audio_stream_microphone_if_dscr));
    memcpy(&(g_UsbAudio.stream_microphone_if_info->alternate_if_info[0].ifdscr.stdif), audio_stream_microphone_alt_if_dscr, sizeof(audio_stream_microphone_alt_if_dscr));
    memcpy(&(g_UsbAudio.stream_ep_in_info->epdesc.stdep), audio_stream_ep_in_dscr, sizeof(audio_stream_ep_in_dscr));

    /* Standard interface descriptor */
    g_UsbAudio.stream_microphone_if_info->ifdscr.stdif.bInterfaceNumber = g_UsbAudio.stream_microphone_interface_id;
    g_UsbAudio.stream_ep_in_info->ep_reset = USB_Audio_IsoIn_Reset;

    /* TX Endpoint handler */
    hal_usb_register_driver_callback(HAL_USB_DRV_HDLR_EP_TX, ep_tx_id, USB_Audio_IsoIn_Hdr);

    /* Endpoint descriptor */
    g_UsbAudio.stream_ep_in_info->epdesc.stdep.bEndpointAddress = (USB_EP_DIR_IN | ep_tx_id); /*InPipe*/
    g_UsbAudio.stream_ep_in_info->ep_status.epout_status.byEP = ep_tx_id;

    if(g_UsbAudio.tx_dma_buffer == NULL){
        LOG_MSGID_I(common, "USB_Audio_StreamIf_Microphone_Create() tx_dma_buffer size=%d\n", 1, HAL_USB_MAX_PACKET_SIZE_ENDPOINT_ISOCHRONOUS_HIGH_SPEED);
        g_UsbAudio.tx_dma_buffer = (uint8_t*)USB_Get_Memory(USB_AUDIO_TX_BUFFER_SIZE);
        g_UsbAudio.tx_dma_buffer_len = USB_AUDIO_TX_BUFFER_SIZE;
    }

    hal_usb_get_dma_channel(ep_tx_id, 0, HAL_USB_EP_DIRECTION_TX, false);
}

/* Audio stream interface reset function, enable EP */
void USB_Audio_StreamIf_Microphone_Reset(void)
{
    LOG_MSGID_I(common, "USB_Audio_StreamIf_Microphone_Reset()\n", 0);

    g_UsbAudio.txpipe = &g_UsbAudio.stream_ep_in_info->ep_status.epout_status;

    /* Stop DMA */
    hal_usb_stop_dma_channel(g_UsbAudio.txpipe->byEP, HAL_USB_EP_DIRECTION_TX);
}

void USB_Audio_StreamIf_Microphone_Enable(void)
{
    LOG_MSGID_I(common, "USB_Audio_StreamIf_Microphone_Enable()\n", 0);

    /* Stop DMA */
    /* Compliance test tool will set configuration , but no reset */
    hal_usb_stop_dma_channel(g_UsbAudio.txpipe->byEP, HAL_USB_EP_DIRECTION_TX);

    /*Non-DMA*/
    hal_usb_enable_tx_endpoint(g_UsbAudio.txpipe->byEP, HAL_USB_EP_TRANSFER_ISO, HAL_USB_EP_USE_NO_DMA, true);
}

/* Audio stream interface speed reset function, enable EP's speed-specific descriptor */
void USB_Audio_StreamIf_Microphone_Speed_Reset(bool b_other_speed)
{
#if !defined(MTK_USB11)
    uint32_t temp_max_size;
    temp_max_size = USB_Audio_Stream_Speed_Reset_Packet_Size((bool)b_other_speed);

    g_UsbAudio.stream_ep_in_info->epdesc.stdep.wMaxPacketSize[0] = temp_max_size & 0xff;
    g_UsbAudio.stream_ep_in_info->epdesc.stdep.wMaxPacketSize[1] = (temp_max_size >> 8) & 0xff;

    if (hal_usb_is_high_speed() == true) {
        g_UsbAudio.stream_ep_in_info->epdesc.stdep.bInterval = 4; // (2^(4-1))*0.125us=1ms
    }
    else{
        g_UsbAudio.stream_ep_in_info->epdesc.stdep.bInterval = 1; // (2^(1-1))*1ms=1ms
    }

    LOG_MSGID_I(common, "USB_Audio_StreamIf_Speed_Microphone_Reset() wMaxPacketSize=%d b_other_speed=%d\n", 2, temp_max_size, b_other_speed);
#endif
}
#endif // MTK_USB_AUDIO_MICROPHONE

/************************************************************
    global variable g_UsbAudio initialize and release functions
*************************************************************/
/* Initialize global variable g_UsbAudio */
void USB_Init_Audio_Status(void)
{
    LOG_MSGID_I(common, "USB_Init_Audio_Status()\n", 0);

    g_UsbAudio.rxpipe = NULL;
    g_UsbAudio.stream_interface_id = 0xFF;
#ifdef MTK_USB_AUDIO_MICROPHONE
    g_UsbAudio.txpipe = NULL;
    g_UsbAudio.stream_microphone_interface_id = 0xFF;
#endif

    USB_Audio.initialized = true;
}

/* Release global variable g_UsbAudio */
void USB_Release_Audio_Status(void)
{
    LOG_MSGID_I(common, "USB_Release_Audio_Status()\n", 0);

    if (g_UsbAudio.rxpipe != NULL) {
        hal_usb_release_dma_channel(0, g_UsbAudio.rxpipe->byEP, HAL_USB_EP_DIRECTION_RX, false);
    }

#ifdef MTK_USB_AUDIO_MICROPHONE
    if (g_UsbAudio.txpipe != NULL) {
        hal_usb_release_dma_channel(g_UsbAudio.txpipe->byEP, 0, HAL_USB_EP_DIRECTION_TX, false);
    }
#endif

    if(USB_Audio.unplug_cb){
        USB_Send_Message(USB_AUDIO_UNPLUG, NULL);
    }

    USB_Init_Audio_Status();

    USB_Free_Memory();
}

/************************************************************
    EP0 handle functions
************************************************************/
/* Parse class specific request */
static uint8_t g_usb_audio_mute;
#if defined(MTK_USB_AUDIO_V2_ENABLE)
static uint16_t g_usb_audio_volume_cur[2]={0x0000, 0x0000};
#else
static uint16_t g_usb_audio_volume_cur[2]={0x0032, 0x0032};
#endif
static const uint16_t g_usb_audio_volume_min[2]={0x0000, 0x0000};
static const uint16_t g_usb_audio_volume_max[2]={0x0065, 0x0065};
static const uint16_t g_usb_audio_volume_res[2]={0x0001, 0x0001};
#if 1
static uint32_t g_usb_audio_sampling_frequency = 48000;
#else
static uint32_t g_usb_audio_sampling_frequency = 44100;
#endif
#ifdef MTK_USB_AUDIO_MICROPHONE
static uint8_t g_usb_audio_microphone_mute;
static uint16_t g_usb_audio_microphone_volume_cur[2]={0xF600, 0xF600};
static const uint16_t g_usb_audio_microphone_volume_min[2]={0xDB00, 0xDB00};
static const uint16_t g_usb_audio_microphone_volume_max[2]={0x0000, 0x0000};
static const uint16_t g_usb_audio_microphone_volume_res[2]={0x0100, 0x0100};
#if 1
static uint32_t g_usb_audio_microphone_sampling_frequency = 16000;
#else
static uint32_t g_usb_audio_microphone_sampling_frequency = 44100;
#endif
#endif
static uint16_t g_usb_audio_set_cur_wIndex;
static uint16_t g_usb_audio_set_cur_wValue;

static void USB_Audio_Ep0_DataReceived(void *data)
{
    bool stall = false;

#if defined(MTK_USB_AUDIO_V2_ENABLE) // UAC2
    if(g_usb_audio_set_cur_wValue == 0x0100 && g_usb_audio_set_cur_wIndex == 0x0400){
        hal_usb_read_endpoint_fifo(0, 4, &g_usb_audio_sampling_frequency);
#else // UAC1
    if(g_usb_audio_set_cur_wValue == 0x0100 && g_usb_audio_set_cur_wIndex == 0x01){
        hal_usb_read_endpoint_fifo(0, 3, &g_usb_audio_sampling_frequency);
#endif
        LOG_MSGID_I(common, "USB_Audio_Ep0_SetCurUnit() change sampling freq to %d Hz\n", 1, g_usb_audio_sampling_frequency);
        USB_Send_Message(USB_AUDIO_SET_SAMPLING_FREQ, (void*)(g_usb_audio_sampling_frequency + (0x01<<24)));
    }
#ifdef MTK_USB_AUDIO_MICROPHONE
#if defined(MTK_USB_AUDIO_V2_ENABLE) // UAC2
    else if(g_usb_audio_set_cur_wValue == 0x0100 && g_usb_audio_set_cur_wIndex == 0x0800){
        hal_usb_read_endpoint_fifo(0, 4, &g_usb_audio_microphone_sampling_frequency);
#else // UAC1
    else if(g_usb_audio_set_cur_wValue == 0x0100 && g_usb_audio_set_cur_wIndex == 0x81){
        hal_usb_read_endpoint_fifo(0, 3, &g_usb_audio_microphone_sampling_frequency);
#endif
        LOG_MSGID_I(common, "USB_Audio_Ep0_SetCurUnit() change microphone sampling freq to %d Hz\n", 1, g_usb_audio_microphone_sampling_frequency);
        USB_Send_Message(USB_AUDIO_SET_SAMPLING_FREQ, (void*)(g_usb_audio_microphone_sampling_frequency + (0x81 << 24)));
    }
#endif
    else if(g_usb_audio_set_cur_wValue == 0x0100 && g_usb_audio_set_cur_wIndex == 0x0200){
        hal_usb_read_endpoint_fifo(0, 1, &g_usb_audio_mute);
        LOG_MSGID_I(common, "USB_Audio_Ep0_SetCurUnit() change mute to %d\n", 1, g_usb_audio_mute);
        USB_Send_Message(USB_AUDIO_SET_MUTE, (void*)(g_usb_audio_mute + (0x01<<24)));
    }
    else if((g_usb_audio_set_cur_wValue == 0x0201 || g_usb_audio_set_cur_wValue == 0x0202) && g_usb_audio_set_cur_wIndex == 0x0200){
        hal_usb_read_endpoint_fifo(0, 2, &g_usb_audio_volume_cur[g_usb_audio_set_cur_wValue - 0x0201]);
        LOG_MSGID_I(common, "USB_Audio_Ep0_SetCurUnit() change channel %d volume to %d\n", 2, g_usb_audio_set_cur_wValue - 0x0200, g_usb_audio_volume_cur[g_usb_audio_set_cur_wValue - 0x0201]);
        USB_Send_Message(USB_AUDIO_SET_VOLUME, (void*)(g_usb_audio_volume_cur[g_usb_audio_set_cur_wValue - 0x0201] + ((g_usb_audio_set_cur_wValue - 0x0200) << 16) + (0x01<<24)));
    }
#ifdef MTK_USB_AUDIO_MICROPHONE
    else if(g_usb_audio_set_cur_wValue == 0x0100 && g_usb_audio_set_cur_wIndex == 0x0600){
        hal_usb_read_endpoint_fifo(0, 1, &g_usb_audio_microphone_mute);
        LOG_MSGID_I(common, "USB_Audio_Ep0_SetCurUnit() change microphone mute to %d\n", 1, g_usb_audio_microphone_mute);
        USB_Send_Message(USB_AUDIO_SET_MUTE, (void*)(g_usb_audio_microphone_mute + (0x81<<24)));
    }
    else if((g_usb_audio_set_cur_wValue == 0x0201 || g_usb_audio_set_cur_wValue == 0x0202) && g_usb_audio_set_cur_wIndex == 0x0600){
        hal_usb_read_endpoint_fifo(0, 2, &g_usb_audio_microphone_volume_cur[g_usb_audio_set_cur_wValue - 0x0201]);
        LOG_MSGID_I(common, "USB_Audio_Ep0_SetCurUnit() change microphone channel %d volume to %d\n", 2, g_usb_audio_set_cur_wValue - 0x0200, g_usb_audio_microphone_volume_cur[g_usb_audio_set_cur_wValue - 0x0201]);
        USB_Send_Message(USB_AUDIO_SET_VOLUME, (void*)(g_usb_audio_microphone_volume_cur[g_usb_audio_set_cur_wValue - 0x0201] + ((g_usb_audio_set_cur_wValue - 0x0200) << 16) + (0x81<<24)));
    }
#endif
    else{
        stall = true;
    }

    gUsbDevice.ep0_state = USB_EP0_RX_STATUS;
    hal_usb_update_endpoint_0_state(HAL_USB_EP0_DRV_STATE_READ_END, stall, true);
}

#ifdef MTK_USB_AUDIO_V2_ENABLE
void USB_Audio_Ep0_Command(Usb_Ep0_Status *pep0state, Usb_Command *pcmd)
{
    bool  bError = false;

    switch (pcmd->bmRequestType) {
        case 0xA1: // Get
            if(pcmd->bRequest == 0x02 && pcmd->wValue == 0x0100 && pcmd->wIndex == 0x0400){ // Get Range / Sampling Frequency Range / Clock Source
                LOG_MSGID_I(common, "USB_Audio_Ep0_Command() get sampling frequency range for speaker\n", 0);
                USB_Generate_EP0Data(pep0state, pcmd, (void*)uac2_range_sampling_freqency, pcmd->wLength < sizeof(uac2_range_sampling_freqency)?pcmd->wLength:sizeof(uac2_range_sampling_freqency));
            }
#ifdef MTK_USB_AUDIO_MICROPHONE
            else if(pcmd->bRequest == 0x02 && pcmd->wValue == 0x0100 && pcmd->wIndex == 0x0800){ // Get Range / Sampling Frequency Range / Clock Source for Microphone path
                LOG_MSGID_I(common, "USB_Audio_Ep0_Command() get sampling frequency range for microphone\n", 0);
                USB_Generate_EP0Data(pep0state, pcmd, (void*)uac2_microphone_range_sampling_freqency, pcmd->wLength < sizeof(uac2_microphone_range_sampling_freqency)?pcmd->wLength:sizeof(uac2_microphone_range_sampling_freqency));
            }
#endif
            else if(pcmd->bRequest == 0x01 && pcmd->wValue == 0x0100 && pcmd->wIndex == 0x0400){ // Get Cur / Sampling Frequency Range / Clock Source
                USB_Generate_EP0Data(pep0state, pcmd, &g_usb_audio_sampling_frequency, pcmd->wLength);
            }
#ifdef MTK_USB_AUDIO_MICROPHONE
            else if(pcmd->bRequest == 0x01 && pcmd->wValue == 0x0100 && pcmd->wIndex == 0x0800){ // Get Cur / Sampling Frequency Range / Clock Source
                USB_Generate_EP0Data(pep0state, pcmd, &g_usb_audio_microphone_sampling_frequency, pcmd->wLength);
            }
#endif
            else if(pcmd->bRequest == 0x02 && (pcmd->wValue == 0x0201 || pcmd->wValue == 0x0202) && pcmd->wIndex == 0x0200){ // Get Range / Volume / Channel 1 or 2 / Feature Unit
                USB_Generate_EP0Data(pep0state, pcmd, (void*)uac2_range_volume, pcmd->wLength);
            }
#ifdef MTK_USB_AUDIO_MICROPHONE
            else if(pcmd->bRequest == 0x02 && (pcmd->wValue == 0x0201 || pcmd->wValue == 0x0202) && pcmd->wIndex == 0x0600){ // Get Range / Volume / Channel 1 or 2 / Feature Unit
                USB_Generate_EP0Data(pep0state, pcmd, (void*)uac2_microphone_range_volume, pcmd->wLength);
            }
#endif
            else if(pcmd->bRequest == 0x01 && (pcmd->wValue == 0x0201 || pcmd->wValue == 0x0202) && pcmd->wIndex == 0x0200){ // Get Cur / Volume / Channel 1 or 2 / Feature Unit
                USB_Generate_EP0Data(pep0state, pcmd, &g_usb_audio_volume_cur[pcmd->wValue - 0x0201], pcmd->wLength);
            }
#ifdef MTK_USB_AUDIO_MICROPHONE
            else if(pcmd->bRequest == 0x01 && (pcmd->wValue == 0x0201 || pcmd->wValue == 0x0202) && pcmd->wIndex == 0x0600){ // Get Cur / Volume / Channel 1 or 2 / Feature Unit
                USB_Generate_EP0Data(pep0state, pcmd, &g_usb_audio_microphone_volume_cur[pcmd->wValue - 0x0201], pcmd->wLength);
            }
#endif
            else if(pcmd->bRequest == 0x01 && pcmd->wValue == 0x0100 && pcmd->wIndex == 0x0200){ // Get Cur / Mute / Channel 0 / Feature Unit
                USB_Generate_EP0Data(pep0state, pcmd, &g_usb_audio_mute, pcmd->wLength);
            }
#ifdef MTK_USB_AUDIO_MICROPHONE
            else if(pcmd->bRequest == 0x01 && pcmd->wValue == 0x0100 && pcmd->wIndex == 0x0600){ // Get Cur / Mute / Channel 0 / Feature Unit
                USB_Generate_EP0Data(pep0state, pcmd, &g_usb_audio_microphone_mute, pcmd->wLength);
            }
#endif
            else{
                bError = true;
            }
            break;
        case 0x21:
            if(pcmd->bRequest == 0x01 && (((pcmd->wValue == 0x0201 || pcmd->wValue == 0x0202 || pcmd->wValue == 0x0100) && pcmd->wIndex == 0x0200) ||
                (pcmd->wValue == 0x0100 && pcmd->wIndex == 0x0400) ||
                (pcmd->wValue == 0x0100 && pcmd->wIndex == 0x0800))){
                g_usb_audio_set_cur_wValue = pcmd->wValue;
                g_usb_audio_set_cur_wIndex = pcmd->wIndex;
                gUsbDevice.ep0_rx_handler = (usb_ep0_rx_ptr)USB_Audio_Ep0_DataReceived;
                gUsbDevice.ep0_state = USB_EP0_RX;
            }
            else{
                bError = true;
            }
            break;
        default:
            bError = true;
            break;
    }

    if(bError == true){
        LOG_MSGID_E(common, "USB_Audio_Ep0_Command() not handled bmRequestType=0x%x bRequest=0x%x wValue=0x%x wIndex=0x%x wLength=0x%x\n",
            5, pcmd->bmRequestType, pcmd->bRequest, pcmd->wValue, pcmd->wIndex, pcmd->wLength);
    }

    /* Stall command if an error occured */
    USB_EP0_Command_Hdlr(bError);
}
#else
void USB_Audio_Ep0_Command(Usb_Ep0_Status *pep0state, Usb_Command *pcmd)
{
    bool  bError = false;

    switch (pcmd->bRequest) {
        case USB_AUDIO_GET_CUR:
            /* tell host the current setting */
            if(pcmd->wValue == 0x0100 && pcmd->wIndex == 0x01){ // USB out EP for speaker
                USB_Generate_EP0Data(pep0state, pcmd, &g_usb_audio_sampling_frequency, pcmd->wLength);
            }
#ifdef MTK_USB_AUDIO_MICROPHONE
            else if(pcmd->wValue == 0x0100 && pcmd->wIndex == 0x81){ // USB in EP for microphone
                USB_Generate_EP0Data(pep0state, pcmd, &g_usb_audio_microphone_sampling_frequency, pcmd->wLength);
            }
#endif
            else if(pcmd->wValue == 0x0100 && pcmd->wIndex == 0x0200){ // HSB 0x01: Mute Control; LSB 0x00: Master Channel. HSB 0x02: Feature Unit ID; LSB 0x00: Interface #
                USB_Generate_EP0Data(pep0state, pcmd, &g_usb_audio_mute, pcmd->wLength);
            }
#ifdef MTK_USB_AUDIO_MICROPHONE
            else if(pcmd->wValue == 0x0100 && pcmd->wIndex == 0x0600){ // HSB 0x01: Mute Control; LSB 0x00: Master Channel. HSB 0x06: Feature Unit ID; LSB 0x00: Interface #
                USB_Generate_EP0Data(pep0state, pcmd, &g_usb_audio_microphone_mute, pcmd->wLength);
            }
#endif
            else if((pcmd->wValue == 0x0201 || pcmd->wValue == 0x0202) && (pcmd->wIndex == 0x0200)){ // HSB 0x02: Volume Control; LSB 0x01: Channel 1
                USB_Generate_EP0Data(pep0state, pcmd, &g_usb_audio_volume_cur[pcmd->wValue - 0x0201], pcmd->wLength);
            }
#ifdef MTK_USB_AUDIO_MICROPHONE
            else if((pcmd->wValue == 0x0201 || pcmd->wValue == 0x0202) && (pcmd->wIndex == 0x0600)){ // HSB 0x02: Volume Control; LSB 0x01: Channel 1
                USB_Generate_EP0Data(pep0state, pcmd, &g_usb_audio_microphone_volume_cur[pcmd->wValue - 0x0201], pcmd->wLength);
            }
#endif
            else{
                bError = true;
            }
            break;
        case USB_AUDIO_GET_MIN:
            /* tell host the current setting */
            if((pcmd->wValue == 0x0201 || pcmd->wValue == 0x0202) && (pcmd->wIndex == 0x0200)){ // HSB 0x02: Volume Control; LSB 0x01: Channel 1
                USB_Generate_EP0Data(pep0state, pcmd, (void*)(&g_usb_audio_volume_min[pcmd->wValue - 0x0201]), pcmd->wLength);
            }
#ifdef MTK_USB_AUDIO_MICROPHONE
            else if((pcmd->wValue == 0x0201 || pcmd->wValue == 0x0202) && (pcmd->wIndex == 0x0600)){ // HSB 0x02: Volume Control; LSB 0x01: Channel 1
                USB_Generate_EP0Data(pep0state, pcmd, (void*)(&g_usb_audio_microphone_volume_min[pcmd->wValue - 0x0201]), pcmd->wLength);
            }
#endif
            else{
                bError = true;
            }
            break;
        case USB_AUDIO_GET_MAX:
            /* tell host the current setting */
            if((pcmd->wValue == 0x0201 || pcmd->wValue == 0x0202) && (pcmd->wIndex == 0x0200)){ // HSB 0x02: Volume Control; LSB 0x01: Channel 1
                USB_Generate_EP0Data(pep0state, pcmd, (void*)(&g_usb_audio_volume_max[pcmd->wValue - 0x0201]), pcmd->wLength);
            }
#ifdef MTK_USB_AUDIO_MICROPHONE
            else if((pcmd->wValue == 0x0201 || pcmd->wValue == 0x0202) && (pcmd->wIndex == 0x0600)){ // HSB 0x02: Volume Control; LSB 0x01: Channel 1
                USB_Generate_EP0Data(pep0state, pcmd, (void*)(&g_usb_audio_microphone_volume_max[pcmd->wValue - 0x0201]), pcmd->wLength);
            }
#endif
            else{
                bError = true;
            }
            break;
        case USB_AUDIO_GET_RES:
            /* tell host the current setting */
            if((pcmd->wValue == 0x0201 || pcmd->wValue == 0x0202) && (pcmd->wIndex == 0x0200)){ // HSB 0x02: Volume Control; LSB 0x01: Channel 1
                USB_Generate_EP0Data(pep0state, pcmd, (void*)(&g_usb_audio_volume_res[pcmd->wValue - 0x0201]), pcmd->wLength);
            }
#ifdef MTK_USB_AUDIO_MICROPHONE
            else if((pcmd->wValue == 0x0201 || pcmd->wValue == 0x0202) && (pcmd->wIndex == 0x0600)){ // HSB 0x02: Volume Control; LSB 0x01: Channel 1
                USB_Generate_EP0Data(pep0state, pcmd, (void*)(&g_usb_audio_microphone_volume_res[pcmd->wValue - 0x0201]), pcmd->wLength);
            }
#endif
            else{
                bError = true;
            }
            break;
        case USB_AUDIO_SET_CUR:
            g_usb_audio_set_cur_wValue = pcmd->wValue;
            g_usb_audio_set_cur_wIndex = pcmd->wIndex;
            gUsbDevice.ep0_rx_handler = (usb_ep0_rx_ptr)USB_Audio_Ep0_DataReceived;
            gUsbDevice.ep0_state = USB_EP0_RX;
            break;
        default:
            bError = true;
            break;
    }

    if(bError){
        LOG_MSGID_E(common, "USB_Audio_Ep0_Command() not handled bmRequestType: 0x%02X, bRequest: 0x%02X, wValue: 0x%04X, wIndex: 0x%04X, wLength: 0x%04X \r\n",
            5, pcmd->bmRequestType, pcmd->bRequest, pcmd->wValue, pcmd->wIndex, pcmd->wLength);
    }

    /* Stall command if an error occured */
    USB_EP0_Command_Hdlr(bError);
}
#endif

/************************************************************
    Iso EP OUT handle functions
*************************************************************/
static uint32_t USB_Audio_Buffer_Space(void)
{
    if(g_UsbAudio.rx_dma_buffer_is_full)
        return 0;
    else{
        if(g_UsbAudio.rx_dma_buffer_read <= g_UsbAudio.rx_dma_buffer_write){
            return g_UsbAudio.rx_dma_buffer_read + (g_UsbAudio.rx_dma_buffer_len - g_UsbAudio.rx_dma_buffer_write);
        }
        else{
            return g_UsbAudio.rx_dma_buffer_read - g_UsbAudio.rx_dma_buffer_write;
        }
    }
}

void* USB_Audio_Rx_Buffer_Get_Read_Address(void)
{
    return g_UsbAudio.rx_dma_buffer + g_UsbAudio.rx_dma_buffer_read;
}

uint32_t USB_Audio_Rx_Buffer_Get_Bytes(void)
{
    uint32_t write_temp, read_temp, rc;

    write_temp = g_UsbAudio.rx_dma_buffer_write;
    read_temp =g_UsbAudio.rx_dma_buffer_read;

    if(write_temp > read_temp){
        rc = write_temp - read_temp;
    }
    else if(write_temp < read_temp || g_UsbAudio.rx_dma_buffer_is_full == 1){
        rc = g_UsbAudio.rx_dma_buffer_len - read_temp;
    }
    else{
        rc = 0;
    }

    return rc;
}

void USB_Audio_Rx_Buffer_Drop_Bytes(uint32_t bytes)
{
    uint32_t read_temp;
    
    if(g_UsbAudio.rx_dma_buffer_len > g_UsbAudio.rx_dma_buffer_read + bytes){
        read_temp = g_UsbAudio.rx_dma_buffer_read + bytes;
    }
    else{
        read_temp = bytes - (g_UsbAudio.rx_dma_buffer_len - g_UsbAudio.rx_dma_buffer_read);
    }

    g_UsbAudio.rx_dma_buffer_read = read_temp;
    g_UsbAudio.rx_dma_buffer_is_full = 0;

    return;
}
static uint32_t usb_audio_count = 0;
/* EP Iso Out interrupt handler, called by EP interrupt */
void USB_Audio_IsoOut_Hdr(void)
{
    uint32_t nCount;
    uint32_t max_pkt_size;
    uint32_t temp;

    if (USB_Audio.initialized == false) {
        return;
    }

    usb_audio_count++;
    if((usb_audio_count % 1000) == 0){
        LOG_MSGID_I(common, "usb_audio_count audio_cnt[%d]\r\n", 1, usb_audio_count);
    }else if(usb_audio_count >= 0xFFFFFFF){
        usb_audio_count = 0;
    }
    
    /* Check current packet's data length */
    nCount = hal_usb_get_rx_packet_length(g_UsbAudio.rxpipe->byEP);

    if (nCount != 0) {
        max_pkt_size = USB_Iso_Max_Packet_Size();

        if ((nCount > max_pkt_size)) {
            LOG_MSGID_I(common, "usb error USB_Audio_IsoOut_Hdr() nCount=%d > max_pkt_size=%d\n", 2, nCount, max_pkt_size);
        }

        if(g_UsbAudio.rx_dma_buffer_is_full)
        {
            // Don't log in ISR level.
            LOG_MSGID_I(common, "usb error USB_Audio_IsoOut_Hdr() rx_dma_buffer_is_full = 1\n", 0);
        }
        else{
            uint32_t nSpace = USB_Audio_Buffer_Space();

            if(nSpace < nCount){
                LOG_MSGID_I(common, "usb error USB_Audio_IsoOut_Hdr() no enough space\n", 0);
                nCount = nSpace;
            }
            if(g_UsbAudio.rx_dma_buffer_write >= g_UsbAudio.rx_dma_buffer_read){
                if(g_UsbAudio.rx_dma_buffer_len - g_UsbAudio.rx_dma_buffer_write < nCount){
                    temp = g_UsbAudio.rx_dma_buffer_len - g_UsbAudio.rx_dma_buffer_write;
                    hal_usb_read_endpoint_fifo(g_UsbAudio.rxpipe->byEP, temp,
                        g_UsbAudio.rx_dma_buffer + g_UsbAudio.rx_dma_buffer_write);
                    nCount -= temp;
                    g_UsbAudio.rx_dma_buffer_write = 0;
                }
            }
            hal_usb_read_endpoint_fifo(g_UsbAudio.rxpipe->byEP, nCount, g_UsbAudio.rx_dma_buffer + g_UsbAudio.rx_dma_buffer_write);

            g_UsbAudio.rx_dma_buffer_write += nCount;
            if(g_UsbAudio.rx_dma_buffer_write == g_UsbAudio.rx_dma_buffer_len)
                g_UsbAudio.rx_dma_buffer_write=0;
            if(g_UsbAudio.rx_dma_buffer_write == g_UsbAudio.rx_dma_buffer_read)
                g_UsbAudio.rx_dma_buffer_is_full = 1;
        }
#if 1
        if(USB_Audio.rx_cb){
            USB_Audio.rx_cb();
        }else{
            LOG_MSGID_I(common, "usb error callback drop[%d]\n", 1, nCount);
            USB_Audio_Rx_Buffer_Drop_Bytes(nCount);
        }
#else
        if(g_UsbAudio.msg_notify <= 2){
            g_UsbAudio.msg_notify++;
            USB_Send_Message(USB_AUDIO_RX_DATA, NULL);
        }
#endif
        /* Clear RxPktRdy */
        hal_usb_set_endpoint_rx_ready(g_UsbAudio.rxpipe->byEP);
    }
}

/* EP Iso Out reset handler */
void USB_Audio_IsoOut_Reset(void)
{
    g_UsbAudio.rxpipe = &g_UsbAudio.stream_ep_out_info->ep_status.epout_status;
}

#ifdef MTK_USB_AUDIO_MICROPHONE
/* EP Iso In interrupt handler, called by EP interrupt */
void USB_Audio_IsoIn_Hdr(void)
{
    if(USB_Audio.tx_cb){
        USB_Audio.tx_cb();
    }
}

/* EP Iso Out reset handler */
void USB_Audio_IsoIn_Reset(void)
{
    g_UsbAudio.txpipe = &g_UsbAudio.stream_ep_in_info->ep_status.epout_status;
}
#endif

void USB_Audio_Register_Rx_Callback(AUDIO_RX_FUNC rx_cb)
{
    USB_Audio.rx_cb = rx_cb;
}

void USB_Audio_Register_Tx_Callback(AUDIO_TX_FUNC tx_cb)
{
    USB_Audio.tx_cb = tx_cb;
}

void USB_Audio_Register_SetInterface_Callback(AUDIO_SETINTERFACE_FUNC setinterface_cb)
{
    USB_Audio.setinterface_cb = setinterface_cb;
}

void USB_Audio_Register_SetSamplingRate_Callback(AUDIO_SETSAMPLINGRATE_FUNC setsamplingrate_cb)
{
    USB_Audio.setsamplingrate_cb = setsamplingrate_cb;
}

void USB_Audio_Register_Unplug_Callback(AUDIO_UNPLUG_FUNC unplug_cb)
{
    USB_Audio.unplug_cb = unplug_cb;
}

void USB_Audio_Register_VolumeChange_Callback(AUDIO_VOLUMECHANGE_FUNC volumechange_cb)
{
    USB_Audio.volumechange_cb = volumechange_cb;
}

void USB_Audio_Register_Mute_Callback(AUDIO_MUTE_FUNC mute_cb)
{
    USB_Audio.mute_cb = mute_cb;
}

#ifdef MTK_USB_AUDIO_MICROPHONE
void USB_Audio_Register_Mic_SetInterface_Callback(AUDIO_SETINTERFACE_FUNC setinterface_cb)
{
    USB_Audio.setinterface_cb_mic = setinterface_cb;
}

void USB_Audio_Register_Mic_SetSamplingRate_Callback(AUDIO_SETSAMPLINGRATE_FUNC setsamplingrate_cb)
{
    USB_Audio.setsamplingrate_cb_mic = setsamplingrate_cb;
}

void USB_Audio_Register_Mic_Unplug_Callback(AUDIO_UNPLUG_FUNC unplug_cb)
{
    USB_Audio.unplug_cb_mic = unplug_cb;
}

void USB_Audio_Register_Mic_VolumeChange_Callback(AUDIO_VOLUMECHANGE_FUNC volumechange_cb)
{
    USB_Audio.volumechange_cb_mic = volumechange_cb;
}

void USB_Audio_Register_Mic_Mute_Callback(AUDIO_MUTE_FUNC mute_cb)
{
    USB_Audio.mute_cb_mic = mute_cb;
}
#endif

uint32_t USB_Audio_Get_CurSamplingRate(void)
{
    return g_usb_audio_sampling_frequency;
}

#ifdef MTK_USB_AUDIO_MICROPHONE
uint32_t USB_Audio_Get_CurSamplingRate_Microphone(void)
{
    return g_usb_audio_microphone_sampling_frequency;
}
#endif

uint32_t USB_Audio_Get_Len_Received_Data(void)
{
    uint32_t write_temp, read_temp, rc;

    write_temp = g_UsbAudio.rx_dma_buffer_write;
    read_temp =g_UsbAudio.rx_dma_buffer_read;

    if(write_temp > read_temp){
        rc = write_temp - read_temp;
    }
    else if(g_UsbAudio.rx_dma_buffer_is_full == 1){
        rc = g_UsbAudio.rx_dma_buffer_len;
    }
    else if(write_temp < read_temp){
        rc = g_UsbAudio.rx_dma_buffer_len - read_temp + write_temp;
    }
    else{
        rc = 0;
    }

    return rc;
}

uint32_t USB_Audio_Read_Data(void* dst, uint32_t len)
{
    uint32_t read_bytes=0;
    uint32_t data_bytes;

    data_bytes = USB_Audio_Rx_Buffer_Get_Bytes();
    if(data_bytes >= len){
        memcpy(dst,  USB_Audio_Rx_Buffer_Get_Read_Address(), len);
        USB_Audio_Rx_Buffer_Drop_Bytes(len);
        read_bytes = len;
    }
    else{
        memcpy(dst,  USB_Audio_Rx_Buffer_Get_Read_Address(), data_bytes);
        USB_Audio_Rx_Buffer_Drop_Bytes(data_bytes);
        read_bytes = data_bytes;
        data_bytes = USB_Audio_Rx_Buffer_Get_Bytes();
        if(data_bytes >= len-read_bytes){
            memcpy(dst + read_bytes,  USB_Audio_Rx_Buffer_Get_Read_Address(), len-read_bytes);
            USB_Audio_Rx_Buffer_Drop_Bytes(len-read_bytes);
            read_bytes = len;
        }
        else{
            memcpy(dst + read_bytes, USB_Audio_Rx_Buffer_Get_Read_Address(), data_bytes);
            USB_Audio_Rx_Buffer_Drop_Bytes(data_bytes);
            read_bytes += data_bytes;
        }
    }

    return read_bytes;
}

#endif //MTK_USB_DEMO_ENABLED

