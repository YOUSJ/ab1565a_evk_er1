/* Copyright Statement:
 *
 * (C) 2005-2016  MediaTek Inc. All rights reserved.
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. ("MediaTek") and/or its licensors.
 * Without the prior written permission of MediaTek and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) MediaTek Software
 * if you have agreed to and been bound by the applicable license agreement with
 * MediaTek ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of MediaTek Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT MEDIATEK SOFTWARE RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 */

#ifdef MTK_USB_DEMO_ENABLED

#include <stdint.h>
#include <stdio.h>
#include <string.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"


/* hal includes */
#include "hal_cache.h"
//#include "hal_pmu_wrap_interface.h"
#include "hal_nvic_internal.h"
#include "hal_sleep_manager.h"
#include "usbacm_drv.h"
#include "memory_attribute.h"
#include "usbacm_adap.h"
#include "usb.h"
#include "usb_comm.h"
#include "usb_resource.h"

#include "usbms_drv.h"
#include "usbms_adap.h"
#include "task_def.h"
#include "syslog.h"
#include "usbhid_drv.h"
#include "usbhid_adap.h"
#include "usbaudio_drv.h"
#include "hal_gpio.h"
#include "hal_pmu.h"
#include "hal_usb_internal.h"
#if (PRODUCT_VERSION == 2822) || (PRODUCT_VERSION == 1565)
#ifdef MTK_BATTERY_MANAGEMENT_ENABLE
#include "battery_management.h"
#endif
#endif

#if defined(AB1568)
#include "hal_pmu_charger_2568.h"
#endif

#if defined(AB1565)
#include "hal_pmu_charger_2565.h"
#endif

#if ((PRODUCT_VERSION == 1552) || defined(AM255X))
#ifdef MTK_BATTERY_MANAGEMENT_ENABLE
#include "battery_management_bc12.h"
#endif
#endif
#ifdef  __USB_MASS_STORAGE_ENABLE__
#include "usbms_state.h"
#endif

#ifdef MTK_PORT_SERVICE_ENABLE
#include "serial_port.h"
extern serial_port_register_callback_t g_serial_port_usb_callback[MAX_USB_PORT_NUM];
#endif

#ifdef  __USB_MASS_STORAGE_ENABLE__
msc_register_callback_t g_msc_usb_callback = NULL;
#endif

#ifdef USB_DETECT_LED
#include "hal.h"
#include "hal_pmu.h"
#include "hal_platform.h"
#endif

#define BOOT_TIMEOUT_CNT 900000
#define BOOT_ENU_TIMEOUT_CNT 300000

volatile bool usb_initial = false;

volatile uint8_t usb_sleep_handler = 0;

const USB_DEVICE_PARAM *USB_GetDeviceParam(void);
const USB_ACM_PARAM *USB_GetAcmParam(void);
const USB_MS_PARAM *USB_GetMsParam(void);
const USB_HID_PARAM *USB_GetHidParam(void);
const USB_AUDIO_PARAM *USB_GetAudioParam(void);

static TaskHandle_t usb_task_handle = NULL;
static QueueHandle_t usb_queue_handle = NULL;
extern UsbHid_Common_Struct g_UsbHID_Comm;

extern void usb_hisr(void);
extern hal_usb_status_t hal_usb_drv_create_isr(void);

bool is_vusb_ready(void)
{
#ifdef MTK_USB11
    bool ret;
    hal_gpio_status_t status;
    hal_gpio_data_t input_gpio_data = HAL_GPIO_DATA_LOW;

    status = hal_gpio_get_input((hal_gpio_pin_t)HAL_GPIO_36, &input_gpio_data);

    if (status == HAL_GPIO_STATUS_OK) {
        if (input_gpio_data == HAL_GPIO_DATA_HIGH) {
            ret = true;
            //log_hal_info("usb cable in \n");
        } else {
            ret = false;
            //log_hal_info("usb cable out \n");
        }
    } else {
        ret = false;
    }

    return ret;
#else
    bool ret = TRUE;
#if (PRODUCT_VERSION != 3335)
    ret = (bool)pmu_get_usb_input_status();
#endif
    return ret;
#endif
}

void USB_Send_Message(usb_msg_type_t msg, void *data)
{
    usb_msg_t msgs;
    BaseType_t xHigherPriorityTaskWoken;
    BaseType_t ret;

    if (usb_queue_handle == NULL) {
        return;
    }

    // We have not woken a task at the start of the ISR.
    xHigherPriorityTaskWoken = pdFALSE;

    msgs.msg_id = msg;
    msgs.data = data;

    if (0 == HAL_NVIC_QUERY_EXCEPTION_NUMBER) {
        ret = xQueueSend(usb_queue_handle, &msgs, 0);
        LOG_D(common, "[USB] Send Queue in Task !! id = %d ", msgs.msg_id);
    } else {
        ret = xQueueSendFromISR(usb_queue_handle, &msgs, &xHigherPriorityTaskWoken);
        LOG_D(common, "[USB] Send Queue in ISR !! id = %d ", msgs.msg_id);
    }

    if (ret != pdTRUE) {
        LOG_MSGID_E(common, "[USB] Send Queue fail!! Queue size = %d ", 1, USB_QUEUE_LENGTH);
    }
    // Now the buffer is empty we can switch context if necessary.
    if (xHigherPriorityTaskWoken) {
        // Actual macro used here is port specific.
        portYIELD_FROM_ISR(pdTRUE);
    }
}


static void usb_task_main(void *pvParameters)
{
    usb_msg_t msgs;

    static uint32_t stack_max = 0;
    uint32_t stack_cur = 0;
#if defined(MTK_USB_AUDIO_V1_ENABLE) || defined(MTK_USB_AUDIO_V2_ENABLE)
    uint32_t temp;
    uint8_t bInterfaceNumber;
    uint8_t bAlternateSetting;
#endif

    while (1) {
        if (usb_queue_handle != NULL) {
            if (xQueueReceive(usb_queue_handle, &msgs, portMAX_DELAY)) {

                LOG_D(common, "QueueReceive ID  = %d", msgs.msg_id);

                switch (msgs.msg_id) {
                    case USB_ACM_MSG:
                        LOG_D(common, "USB_ACM_MSG");
                        break;
#ifdef  __USB_MASS_STORAGE_ENABLE__
                    case USB_MSC_RX_MSG:
                        USB_Ms_State_Main(MSG_ID_USB_MSDRV_REC_DONE_CONF, (usb_ms_rec_done_conf_struct *) msgs.data);
                        LOG_D(common, "USB_MSC_RX_MSG: 0x%x", msgs.data);
                        break;
                    case USB_MSC_TX_MSG:
                        USB_Ms_State_Main(MSG_ID_USB_MSDRV_TRX_DONE_CONF, NULL);
                        LOG_D(common, "USB_MSC_TX_MSG");
                        break;
                    case USB_MSC_CLR_STALL_MSG:
                        USB_Ms_State_Main(MSG_ID_USB_MSDRV_CLEAR_STALL_REQ, NULL);
                        LOG_D(hal, "USB_MSC_TX_MSG");
                        break;
                    case USB_MSC_RESET_IND_MSG:
                        USB_Ms_State_Main(MSG_ID_USB_MSDRV_RESET_IND, NULL);
                        LOG_D(common, "USB_MSC_TX_MSG");
                        break;
#endif
#if defined(MTK_USB_AUDIO_V1_ENABLE) || defined(MTK_USB_AUDIO_V2_ENABLE)
                    case USB_AUDIO_SET_INTERFACE:
                        bInterfaceNumber=(uint8_t)(((uint32_t)(msgs.data)) >> 16);
                        bAlternateSetting=(uint8_t)(((uint32_t)(msgs.data)) & 0x0000FFFF);

                        /* Reset buffer related variables. */
                        g_UsbAudio.rx_dma_buffer_is_full = 0;
                        g_UsbAudio.rx_dma_buffer_read = 0;
                        g_UsbAudio.rx_dma_buffer_write = 0;

                        if(bInterfaceNumber == 2 && bAlternateSetting == 0){
                            hal_usb_disable_tx_endpoint(1);
                            hal_usb_clear_tx_endpoint_fifo(1, HAL_USB_EP_TRANSFER_ISO, false);
                        }
                        else if(bInterfaceNumber == 2 && bAlternateSetting == 1){
                            hal_usb_enable_tx_endpoint(1, HAL_USB_EP_TRANSFER_ISO, HAL_USB_EP_USE_NO_DMA, false);
                        }

                        if(USB_Audio.setinterface_cb){
                            USB_Audio.setinterface_cb(bInterfaceNumber, bAlternateSetting);
                        }
#ifdef MTK_USB_AUDIO_MICROPHONE
                        if(USB_Audio.setinterface_cb_mic){
                            USB_Audio.setinterface_cb_mic(bInterfaceNumber, bAlternateSetting);
                        }
#endif
                        break;
                    case USB_AUDIO_SET_SAMPLING_FREQ:
                        if(USB_Audio.setsamplingrate_cb){
                            USB_Audio.setsamplingrate_cb(((uint32_t)(msgs.data)) >> 24, ((uint32_t)(msgs.data)) & 0x00FFFFFF);
                        }
#ifdef MTK_USB_AUDIO_MICROPHONE
                        if(USB_Audio.setsamplingrate_cb_mic){
                            USB_Audio.setsamplingrate_cb_mic(((uint32_t)(msgs.data)) >> 24, ((uint32_t)(msgs.data)) & 0x00FFFFFF);
                        }
#endif
                        break;
                    case USB_AUDIO_RX_DATA:
                        if(USB_Audio.rx_cb){
                            USB_Audio.rx_cb();
                        }
                        temp=g_UsbAudio.msg_notify;
                        if(temp>0){
                            temp--;
                            g_UsbAudio.msg_notify=temp;
                        }
                        break;
                    case USB_AUDIO_UNPLUG:
                        if(USB_Audio.unplug_cb){
                            USB_Audio.unplug_cb();
                        }
#ifdef MTK_USB_AUDIO_MICROPHONE
                        if(USB_Audio.unplug_cb_mic){
                            USB_Audio.unplug_cb_mic();
                        }
#endif
                        break;
                    case USB_AUDIO_SET_MUTE:
                        if(USB_Audio.mute_cb){
                            USB_Audio.mute_cb(((uint32_t)(msgs.data)) >> 24, ((uint32_t)(msgs.data)) & 0x000000FF);
                        }
#ifdef MTK_USB_AUDIO_MICROPHONE
                        if(USB_Audio.mute_cb_mic){
                            USB_Audio.mute_cb_mic(((uint32_t)(msgs.data)) >> 24, ((uint32_t)(msgs.data)) & 0x000000FF);
                        }
#endif
                        break;
                    case USB_AUDIO_SET_VOLUME:
                        if(USB_Audio.volumechange_cb){
                            USB_Audio.volumechange_cb(((uint32_t)(msgs.data)) >> 24, (((uint32_t)(msgs.data)) >> 16) & 0x000000FF, ((uint32_t)(msgs.data)) & 0x0000FFFF);
                        }
#ifdef MTK_USB_AUDIO_MICROPHONE
                        if(USB_Audio.volumechange_cb_mic){
                            USB_Audio.volumechange_cb_mic(((uint32_t)(msgs.data)) >> 24, (((uint32_t)(msgs.data)) >> 16) & 0x000000FF, ((uint32_t)(msgs.data)) & 0x0000FFFF);
                        }
#endif
                        break;
#endif
                    default:
                        break;
                }

                stack_cur = (USB_TASK_STACKSIZE - uxTaskGetStackHighWaterMark(usb_task_handle) * sizeof(portSTACK_TYPE));
                if (stack_cur > stack_max) {
                    stack_max = stack_cur;
                    LOG_MSGID_I(common, "USB task max-usage:%d \r\n", 1, stack_max);
                }

            }
        }
    }
}


bool usb_task_init(void)
{
    portBASE_TYPE xReturn = pdFALSE;

    if ((usb_queue_handle != NULL) || (usb_task_handle != NULL)) {
        LOG_MSGID_I(common, "usb task have inited, return !", 0);
        return true;
    }

    /* Queue creation */
    usb_queue_handle = xQueueCreate(USB_QUEUE_LENGTH, sizeof(usb_msg_t));

    if (usb_queue_handle == NULL) {
        LOG_MSGID_E(common, "usb_queue_handle create fail!", 0);
        return false;
    }

    xReturn = xTaskCreate(usb_task_main, USB_TASK_NAME, USB_TASK_STACKSIZE / sizeof(portSTACK_TYPE), NULL, USB_TASK_PRIO, &usb_task_handle);

    if (xReturn == pdFALSE) {
        vQueueDelete(usb_queue_handle);
        usb_queue_handle = NULL;
        LOG_MSGID_E(common, "usb_task_main task create fail!", 0);
        return false;
    }

    return true;
}

void usb_task_deinit(void)
{
    if (usb_queue_handle != NULL) {
        vQueueDelete(usb_queue_handle);
        usb_queue_handle = NULL;
    }

    if (usb_task_handle != NULL) {
        vTaskDelete(usb_task_handle);
        usb_task_handle = NULL;
    }
}


void usb_dev_app_init_cdc_acm()
{
#ifdef USB_CDC_ACM_ENABLE
            g_UsbACM_Comm.acm_param = USB_GetAcmParam();
            USB_Register_CreateFunc("ACM COMMU1",
                                    USB_Acm1_CommIf_Create,
                                    USB_Acm1_CommIf_Reset,
                                    USB_Acm1_CommIf_Enable,
                                    (usb_speed_if_func_ptr)USB_Acm1_CommIf_Speed_Reset,
                                    NULL);

            USB_Register_CreateFunc("ACM DATA1",
                                    USB_Acm1_DataIf_Create,
                                    USB_Acm1_DataIf_Reset,
                                    USB_Acm1_DataIf_Enable,
                                    (usb_speed_if_func_ptr)USB_Acm1_DataIf_Speed_Reset,
                                    NULL);

            USB_Register_CreateFunc("ACM COMMU2",
                                    USB_Acm2_CommIf_Create,
                                    USB_Acm2_CommIf_Reset,
                                    USB_Acm2_CommIf_Enable,
                                    (usb_speed_if_func_ptr)USB_Acm2_CommIf_Speed_Reset,
                                    NULL);

            USB_Register_CreateFunc("ACM DATA2",
                                    USB_Acm2_DataIf_Create,
                                    USB_Acm2_DataIf_Reset,
                                    USB_Acm2_DataIf_Enable,
                                    (usb_speed_if_func_ptr)USB_Acm2_DataIf_Speed_Reset,
                                    NULL);

            USB_Register_Device_Code(0x00, 0x00, 0x00, 0x0023/*g_UsbACM_Comm.acm_param->desc_product_dual_port*/);

            USB_Init(USB_CDC_ACM);
#endif  
}

void usb_dev_app_init_msc()
{
#ifdef  __USB_MASS_STORAGE_ENABLE__
            g_UsbMS.ms_param = USB_GetMsParam();

            //USB_Ms_Register_DiskDriver(&USB_RAM_drv);
            USB_Ms_Register_DiskDriver(&usbms_msdc_driver);

#ifdef MTK_FATFS_ON_SERIAL_NAND
            USB_Ms_Register_DiskDriver(&usbms_spinand_driver);
#endif

            USB_Register_CreateFunc("MASS STORAGE", USB_Ms_If_Create, USB_Ms_If_Reset,
                                    USB_Ms_If_Enable, (usb_speed_if_func_ptr)USB_Ms_If_Speed_Reset, USB_Ms_If_Resume);
            USB_Register_Device_Code(USB_MS_DEVICE_CODE, USB_MS_SUBCLASS_CODE,
                                     USB_MS_PROTOCOL_CODE, g_UsbMS.ms_param->desc_product);

            /* initialize MASS STORAGE MODE */
            USB_Init(USB_MASS_STORAGE);
#endif
}

void usb_dev_app_init_hid()
{
#ifdef MTK_USB_HID_ENABLE
            g_UsbHID_Comm.hid_param = USB_GetHidParam();
            USB_Register_CreateFunc("HID", USB_Hid_CommIf_Create, USB_Hid_CommIf_Reset,
                                    USB_Hid_CommIf_Enable, (usb_speed_if_func_ptr)USB_Hid_CommIf_Speed_Reset, NULL);
            USB_Register_Device_Code(USB_HID_DEVICE_CODE, USB_HID_SUBCLASS_CODE,
                                     USB_HID_PROTOCOL_CODE, g_UsbHID_Comm.hid_param->desc_product);

            USB_Init(USB_HID);
#endif
}

void usb_dev_app_init_audio()
{
#if defined(MTK_USB_AUDIO_V1_ENABLE) || defined(MTK_USB_AUDIO_V2_ENABLE)
            g_UsbAudio_Comm.audio_param = USB_GetAudioParam();
            USB_Register_CreateFunc("AUDIO Control", USB_Audio_ControlIf_Create, USB_Audio_ControlIf_Reset,
                                    USB_Audio_ControlIf_Enable, (usb_speed_if_func_ptr)USB_Audio_ControlIf_Speed_Reset, NULL);
            USB_Register_CreateFunc("AUDIO Stream", USB_Audio_StreamIf_Create, USB_Audio_StreamIf_Reset,
                                    USB_Audio_StreamIf_Enable, (usb_speed_if_func_ptr)USB_Audio_StreamIf_Speed_Reset, NULL);
#ifdef MTK_USB_AUDIO_MICROPHONE
            USB_Register_CreateFunc("AUDIO Microphone", USB_Audio_StreamIf_Microphone_Create, USB_Audio_StreamIf_Microphone_Reset,
                                    USB_Audio_StreamIf_Microphone_Enable, (usb_speed_if_func_ptr)USB_Audio_StreamIf_Microphone_Speed_Reset, NULL);
#endif
            USB_Register_Device_Code(USB_AUDIO_DEVICE_CODE, USB_AUDIO_SUBCLASS_CODE,
                                    USB_AUDIO_PROTOCOL_CODE, g_UsbAudio_Comm.audio_param->desc_product);

            USB_Init(USB_AUDIO);
#endif
}    


bool ap_usb_init(USB_DEVICE_TYPE type)
{
    uint32_t mask_irq = 0;

    hal_nvic_save_and_set_interrupt_mask(&mask_irq);
    if (usb_initial == true) {
        hal_nvic_restore_interrupt_mask(mask_irq);
        return false;
    }

    usb_initial = true;
    hal_nvic_restore_interrupt_mask(mask_irq);

    /*LOG_I(common,"++++++++++ usb lcok sleep\n");*/
#ifndef HAL_SLEEP_MANAGER_SUPPROT_DEEPSLEEP_LOCK
    hal_sleep_manager_lock_sleep(usb_sleep_handler);
#else
    hal_sleep_manager_acquire_sleeplock(usb_sleep_handler, HAL_SLEEP_LOCK_ALL);
#endif
    hal_usb_init();
    ///* Get customize parameters */
    gUsbDevice.device_param = USB_GetDeviceParam();
    
    LOG_MSGID_E(common, "usb app init type [%d]\n", 1, type);

    switch (type) {
        case USB_CDC_ACM:
            usb_dev_app_init_cdc_acm();
            break;
        case USB_MASS_STORAGE:
            usb_dev_app_init_msc();
            break;
        case USB_HID:
            usb_dev_app_init_hid();
            break;
        case USB_AUDIO:
            usb_dev_app_init_audio();
            break;
        default:
            LOG_MSGID_E(common, "ASSERT\n", 0);
            break;
    }
    return true;
}

bool ap_usb_deinit(void)
{
    uint32_t mask_irq = 0;

    hal_nvic_save_and_set_interrupt_mask(&mask_irq);
    if (usb_initial == false) {
        hal_nvic_restore_interrupt_mask(mask_irq);
        return false;
    }

    hal_nvic_restore_interrupt_mask(mask_irq);

    USB_Release_Type();

#ifndef HAL_SLEEP_MANAGER_SUPPROT_DEEPSLEEP_LOCK
    hal_sleep_manager_unlock_sleep(usb_sleep_handler);
#else
    hal_sleep_manager_release_sleeplock(usb_sleep_handler, HAL_SLEEP_LOCK_ALL);
#endif
    /*LOG_I(common,"--------- usb unlcok sleep\n");*/
    hal_nvic_save_and_set_interrupt_mask(&mask_irq);
    usb_initial = false;
    hal_nvic_restore_interrupt_mask(mask_irq);

    return true;
}

void usb_cable_init_mt2822(void)
{
#if (PRODUCT_VERSION == 2822) || (PRODUCT_VERSION == 1565)
    int i = 0;
    uint8_t charger_type = 0;

    if (usb_sleep_handler == 0) 
    {
        usb_sleep_handler = hal_sleep_manager_set_sleep_handle("USBDemo");
    }

    if (is_vusb_ready()) 
    {
        if (usb_initial == true) {
            return; 
        } else {
            hal_usb_phy_preinit();
			#ifdef MTK_BATTERY_MANAGEMENT_ENABLE
            charger_type = battery_management_get_battery_property(BATTERY_PROPERTY_CHARGER_TYPE);
			#endif
        }
        LOG_MSGID_I(common, "USB in\n", 0);
#ifdef USB_DETECT_LED
        usb_detect_led(LED_GPIO, true);
#endif
        ap_usb_init(USB_CDC_ACM);

#ifdef MTK_BATTERY_MANAGEMENT_ENABLE
        if (charger_type == SDP_CHARGER) 
        {
            do {
                usb_hisr();
                if (i++ >= BOOT_TIMEOUT_CNT) {
                    break;
                }
            } while ((USB_Get_Device_State() != DEVSTATE_CONFIG));

            LOG_MSGID_I(common, "USBCOM init, hisr cnt%d\n\r", 1, i);
            i = 0;
            /*call usb_hisr to handle EP0 callback, for usb logging before enable interrupt*/
            do {
                usb_hisr();
                if (i++ >= 10000) {
                    break;
                }
            } while (1);
            LOG_MSGID_I(common, "USBCOM init 2, hisr cnt%d\n\r", 1, i);
        }else{ //else if ((SDP_CHARGER != charger_type) && (CDP_CHARGER != charger_type)) {
            LOG_MSGID_I(common, "usb not connect to PC, deinit usb.\n\r", 0, i);
            ap_usb_deinit();
            usb_task_deinit();
        }
#endif

#ifdef MTK_PORT_SERVICE_ENABLE
        if (g_serial_port_usb_callback[0] != NULL) {
            g_serial_port_usb_callback[0](SERIAL_PORT_DEV_USB_COM1, SERIAL_PORT_EVENT_USB_CONNECTION, NULL);
            LOG_MSGID_I(common, "USB1 CONNECTION callback\n", 0);
        }
        if (g_serial_port_usb_callback[1] != NULL) {
            g_serial_port_usb_callback[1](SERIAL_PORT_DEV_USB_COM2, SERIAL_PORT_EVENT_USB_CONNECTION, NULL);
            LOG_MSGID_I(common, "USB2 CONNECTION callback\n", 0);
        }
#endif
    } else {
        if (usb_initial == true) {
#ifdef MTK_PORT_SERVICE_ENABLE
            if (g_serial_port_usb_callback[0] != NULL) {
                g_serial_port_usb_callback[0](SERIAL_PORT_DEV_USB_COM1, SERIAL_PORT_EVENT_USB_DISCONNECTION, NULL);
                LOG_MSGID_I(common, "USB1 CONNECTION callback\n", 0);
            }
            if (g_serial_port_usb_callback[1] != NULL) {
                g_serial_port_usb_callback[1](SERIAL_PORT_DEV_USB_COM2, SERIAL_PORT_EVENT_USB_DISCONNECTION, NULL);
                LOG_MSGID_I(common, "USB2 CONNECTION callback\n", 0);
            }

#endif
            LOG_MSGID_I(common, "USB out\n", 0);
            ap_usb_deinit();
            usb_task_deinit();
            hal_usb_save_current();
#ifdef USB_DETECT_LED
            usb_detect_led(LED_GPIO, false);
#endif
        } else {
            LOG_MSGID_I(common, "USB not plug in !\n", 0);
        }
    }
#endif
}

void usb_cable_detect(void)
{
    printf("usb usb_cable_detect");
#if (PRODUCT_VERSION == 2822) || (PRODUCT_VERSION == 1565)
    usb_cable_init_mt2822();
#else
#if ((PRODUCT_VERSION == 1552) || defined(AM255X))
#ifdef MTK_BATTERY_MANAGEMENT_ENABLE
    int i = 0;
    uint8_t charger_type = 0;
#endif
#elif defined(AG3335)
    int i = 0;
#endif
    uint32_t mask_irq = 0;

    hal_nvic_save_and_set_interrupt_mask(&mask_irq);
    if (usb_sleep_handler == 0) {
        usb_sleep_handler = hal_sleep_manager_set_sleep_handle("USBDemo");
    }
    hal_nvic_restore_interrupt_mask(mask_irq);

#if defined(MTK_USB_AUDIO_V1_ENABLE) || defined(MTK_USB_AUDIO_V2_ENABLE)
    usb_task_init();
#endif

    if (is_vusb_ready()) {
        if (usb_initial == true) {
            return; /**/
        } else {
#if ((PRODUCT_VERSION == 1552) || defined(AM255X))
#ifdef MTK_BATTERY_MANAGEMENT_ENABLE
            /*get charger type first, otherwise can not get correct type after DP/DM switched to USB.*/
            hal_usb_phy_preinit();
            charger_type = battery_management_get_charger_type_bc12();
#endif
#endif
        }

        LOG_MSGID_I(common, "USB in\n", 0);
#ifdef USB_DETECT_LED
        usb_detect_led(LED_GPIO, true);
#endif
#if defined(MTK_USB_BOOT_MSC)
#ifdef AM255X
        usb_task_init();
#endif
        ap_usb_init(USB_MASS_STORAGE);
#elif defined(MTK_USB_HID_ENABLE)
        ap_usb_init(USB_HID);
#elif defined(MTK_USB_AUDIO_V1_ENABLE) || defined(MTK_USB_AUDIO_V2_ENABLE)
        ap_usb_init(USB_AUDIO);
#else //defined(MTK_USB_BOOT_ACM)
        ap_usb_init(USB_CDC_ACM);
#endif

#if ((PRODUCT_VERSION == 1552) || defined(AM255X))
#ifdef MTK_BATTERY_MANAGEMENT_ENABLE
        /*This section of the code only excuted when usb connect to PC not adapter, and only excuted in system init stage.*/
        /*otherwise usb hot-plug maybe cause music to stop.*/
        if (((SDP_CHARGER == charger_type) || (CDP_CHARGER == charger_type)) && (HAL_CORE_ACTIVE != hal_core_status_read(HAL_CORE_CM4))) {
            do {
                usb_hisr();
                if (i++ >= BOOT_TIMEOUT_CNT) {
                    break;
                }
            } while ((USB_Get_Device_State() != DEVSTATE_CONFIG));
            LOG_MSGID_I(common, "USBCOM init, hisr cnt%d\n\r", 1, i);

            i = 0;
            /*call usb_hisr to handle EP0 callback, for usb logging before enable interrupt*/
            do {
                usb_hisr();
                if (i++ >= 10000) {
                    break;
                }
            } while (1);
            LOG_MSGID_I(common, "USBCOM init 2, hisr cnt%d\n\r", 1, i);
        } else if ((SDP_CHARGER != charger_type) && (CDP_CHARGER != charger_type)) {
            LOG_MSGID_I(common, "usb not connect to PC, deinit usb.\n\r", 0, i);
            ap_usb_deinit();
            usb_task_deinit();
        }
#endif
#elif defined(AG3335)
        LOG_MSGID_I(common, "usb 3335 USBCOM init s, hisr cnt%d\n\r", 1, i);
        do {
            usb_hisr();
            i++;
        } while ((i < 900000));
        USB_Set_Device_Status();
        LOG_MSGID_I(common, "usb 3335 USBCOM init e, hisr cnt%d\n\r", 1, i);
#endif
#ifdef MTK_PORT_SERVICE_ENABLE
        if (g_serial_port_usb_callback[0] != NULL) {
            g_serial_port_usb_callback[0](SERIAL_PORT_DEV_USB_COM1, SERIAL_PORT_EVENT_USB_CONNECTION, NULL);
            LOG_MSGID_I(common, "USB1 CONNECTION callback\n", 0);
        }
        if (g_serial_port_usb_callback[1] != NULL) {
            g_serial_port_usb_callback[1](SERIAL_PORT_DEV_USB_COM2, SERIAL_PORT_EVENT_USB_CONNECTION, NULL);
            LOG_MSGID_I(common, "USB2 CONNECTION callback\n", 0);
        }
#endif

#ifdef  __USB_MASS_STORAGE_ENABLE__
        if (g_msc_usb_callback != NULL) {
            g_msc_usb_callback(MSC_EVENT_USB_CONNECTION);
            LOG_MSGID_I(common, "msc CONNECTION callback\n", 0);
        }
#endif


    } else {
        if (usb_initial == true) {
#ifdef MTK_PORT_SERVICE_ENABLE
            if (g_serial_port_usb_callback[0] != NULL) {
                g_serial_port_usb_callback[0](SERIAL_PORT_DEV_USB_COM1, SERIAL_PORT_EVENT_USB_DISCONNECTION, NULL);
                LOG_MSGID_I(common, "USB1 CONNECTION callback\n", 0);
            }
            if (g_serial_port_usb_callback[1] != NULL) {
                g_serial_port_usb_callback[1](SERIAL_PORT_DEV_USB_COM2, SERIAL_PORT_EVENT_USB_DISCONNECTION, NULL);
                LOG_MSGID_I(common, "USB2 CONNECTION callback\n", 0);
            }

#endif

#ifdef  __USB_MASS_STORAGE_ENABLE__
            if (g_msc_usb_callback != NULL) {
                g_msc_usb_callback(MSC_EVENT_USB_DISCONNECTION);
                LOG_MSGID_I(common, "msc DISCONNECTION callback\n", 0);
            }
#endif
            LOG_MSGID_I(common, "USB out\n", 0);
            ap_usb_deinit();
#if defined(MTK_USB_AUDIO_V1_ENABLE) || defined(MTK_USB_AUDIO_V2_ENABLE)
            // We cannot delete USB task and related queue, since we need to pass unplug message to
            // it and will call the user provided unplug callback in USB task.
#else
            usb_task_deinit();
#endif

#ifdef USB_DETECT_LED
            usb_detect_led(LED_GPIO, false);
#endif
        } else {
            LOG_MSGID_I(common, "USB not plug in !\n", 0);
#if ((PRODUCT_VERSION == 1552) || defined(AM255X))
            hal_usb_save_current();
#endif
        }
    }

#endif
}

bool usb_device_test_case(usb_device_case_type_t usb_case)
{   
    bool ret_value = false;
    
    LOG_MSGID_I(common, "usb usb_device_test_case case [%d]\r\n", 1, (int)usb_case);

    uint32_t i = 0;

    switch(usb_case)
    {
        case usb_device_slt:
            ap_usb_init(USB_CDC_ACM);
            
            do {
                usb_hisr();
                if (i++ >= BOOT_TIMEOUT_CNT) {
                    break;
                }
            } while ((USB_Get_Device_State() != DEVSTATE_CONFIG));

            if(USB_Get_Device_State() == DEVSTATE_CONFIG){
                ret_value = true;
                LOG_MSGID_I(common, "usb usb_device_test_case slt pass\r\n", 0);
            }else{
                ret_value = false;
                LOG_MSGID_I(common, "usb usb_device_test_case slt fail\r\n", 0);
            }
            break;
        default:
            break;
    }

    return ret_value;
}



void usb_boot_init(void)
{
    int i = 0;

    usb_task_init();
    hal_usb_drv_create_isr();

    if (usb_sleep_handler == 0) {
        usb_sleep_handler = hal_sleep_manager_set_sleep_handle("USBDemo");
        LOG_MSGID_I(common, "usb_sleep_handler=%d\n", 1, usb_sleep_handler);
    }

    if (is_vusb_ready()) {
        LOG_MSGID_I(common, "Boot-USB_in\n", 0);

#if defined(MTK_USB_BOOT_MSC)
        ap_usb_init(USB_MASS_STORAGE);
#else //defined(MTK_USB_BOOT_MSC)
        ap_usb_init(USB_CDC_ACM);
        LOG_MSGID_I(common, "ap_usb_init(USB_CDC_ACM)\n", 0);
#endif

        do {
            usb_hisr();
            if (i++ >= BOOT_TIMEOUT_CNT) {
                break;
            }
        } while ((USB_Get_Device_State() != DEVSTATE_CONFIG));
        LOG_MSGID_I(common, "USBCOM init, hisr cnt%d\n\r", 1, i);
    } else {
        /*call usb phy preint for current save & BC1.2 control PD/PM*/
#if ((PRODUCT_VERSION == 1552) || defined(AM255X))
        hal_usb_save_current();
#endif
        LOG_MSGID_I(common, "USB not plug in !\n", 0);

    }


}

bool usb_entry_force_enumeration()
{
    bool ret = true;
    uint32_t count = 0;

    do {
        usb_hisr();

        if (count++ >= BOOT_TIMEOUT_CNT) {
            ret = false;
            break;
        }
    } while ((USB_Get_Device_State() != DEVSTATE_CONFIG));

    LOG_MSGID_I(common, "usb usb_entry_force_enumeration step1 [%d]\r", 1, count);

    count= 0;

    do {
        usb_hisr();
        if (count++ >= 10000) {
            break;
        }
    } while (1);

    LOG_MSGID_I(common, "usb usb_entry_force_enumeration step2 [%d]\n\r", 1, count);
    
    return ret;
}


void usb_entry_case_boot()
{
#if (PRODUCT_VERSION == 2822) || (PRODUCT_VERSION == 1565)
    bool status = false;
    hal_usb_status_t hal_status = HAL_USB_STATUS_OK;
    uint8_t charger_type = 0;

    status = is_vusb_ready(); 
    if(status)
    {
        LOG_MSGID_I(common, "usb charger_type[%d]", 1, charger_type);

        status = usb_task_init();
        if(status == false)
            LOG_MSGID_I(common, "usb task init fail", 0);
        
        hal_usb_phy_preinit();
        #ifdef MTK_BATTERY_MANAGEMENT_ENABLE
            #if defined(AB1568)
                charger_type = pmu_get_bc12_charger_type_2568();
            #elif defined(AB1565)
                charger_type = pmu_get_bc12_charger_type_2565();
            #endif
        #endif

        hal_status = hal_usb_drv_create_isr();
        if(hal_status != HAL_USB_STATUS_OK)
            LOG_MSGID_I(common, "usb isr init fail", 0);

        if (usb_sleep_handler == 0) {
            usb_sleep_handler = hal_sleep_manager_set_sleep_handle("USBDemo");
            LOG_MSGID_I(common, "usb_sleep_handler=%d\n", 1, usb_sleep_handler);
        }
        
        if(charger_type == SDP_CHARGER)
        {
            ap_usb_init(USB_CDC_ACM);
            usb_entry_force_enumeration();
            usb_initial = true;
        }else{
            ap_usb_deinit();
            usb_task_deinit();
            usb_initial = false;
        }
    }else{
        ap_usb_deinit();
    }
#endif 
}



#ifdef  __USB_MASS_STORAGE_ENABLE__
void ap_usb_register_msc_callback(msc_register_callback_t callback)
{
    g_msc_usb_callback = callback;
    LOG_MSGID_I(common, "register MSC connection callback\n", 0);

    if (g_msc_usb_callback != NULL) {
        if (is_vusb_ready()) {
            g_msc_usb_callback(MSC_EVENT_USB_CONNECTION);
            LOG_MSGID_I(common, "MSC_CONNECTION callback\n", 0);
        } else {
            g_msc_usb_callback(MSC_EVENT_USB_DISCONNECTION);
            LOG_MSGID_I(common, "MSC_DISCONNECTION callback\n", 0);
        }
    }
}
#endif

/*
 * After the boot, could be through the led's gpio and vibr power domain to control led status
 * LED light: GPIO = low  and VIBR = on
 * LED dark : GPIO = high and VIBR = off
 * */
#ifdef USB_DETECT_LED
void usb_detect_led(hal_gpio_pin_t gpio_index, bool status)
{

    if (status == true) {
        LOG_MSGID_I(common, "LED Light\n", 0);
        hal_gpio_init(gpio_index);
        hal_pinmux_set_function(gpio_index, 0);/* Set pin as GPIO mode.*/
        hal_gpio_set_direction(gpio_index, HAL_GPIO_DIRECTION_OUTPUT); /* Set GPIO as output.*/
        hal_gpio_set_output(gpio_index, HAL_GPIO_DATA_LOW); /*set gpio output low*/

        PMIC_VR_VOSEL_INIT_CONFIG(PMIC_VIBR, VIBR_VOL); /*set vibr voltage */
        PMIC_VR_CONTROL(PMIC_VIBR, PMIC_VR_CTL_ENABLE); /*vibr enable*/
    } else {
        LOG_MSGID_I(common, "LED off\n", 0);
        hal_gpio_init(gpio_index);
        hal_pinmux_set_function(gpio_index, 0);
        hal_gpio_set_direction(gpio_index, HAL_GPIO_DIRECTION_OUTPUT);
        hal_gpio_set_output(gpio_index, HAL_GPIO_DATA_HIGH);

        PMIC_VR_VOSEL_INIT_CONFIG(PMIC_VIBR, VIBR_VOL);
        PMIC_VR_CONTROL(PMIC_VIBR, PMIC_VR_CTL_DISABLE);
    }
}

#endif
#endif //MTK_USB_DEMO_ENABLED


