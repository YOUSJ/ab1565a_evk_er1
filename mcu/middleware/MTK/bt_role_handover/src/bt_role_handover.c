/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#if defined(MTK_AWS_MCE_ENABLE)
#include "bt_role_handover.h"
#include "bt_callback_manager.h"
#include "bt_type.h"
#include "bt_debug.h"
#include "bt_os_layer_api.h"
#include "FreeRTOS.h"
#include <timers.h>
#include "bt_connection_manager.h"

log_create_module(BT_RHO, PRINT_LEVEL_INFO);
#define BT_ROLE_HANDOVER_RETRY_TIME   (100)
#define BT_ROLE_HANDOVER_PREPARE_TIME (5000)

//task event
#define BT_ROLE_HANDOVER_TASK_EVENT_RETRY_TIMER            (1 << 0)
#define BT_ROLE_HANDOVER_TASK_EVENT_PREPARE_TIMER          (1 << 1)
#define BT_ROLE_HANDOVER_TASK_EVENT_RX_STATE_CHANGE_IND_OK (1 << 2)

#define BT_ROLE_HANDOVER_DATA_HEADER_PER_MODULE (2)

typedef struct {
    bt_bd_addr_t remote_addr;       /*addr needs to be changed after rho.*/
    bt_aws_mce_role_t role;
    bt_role_handover_state_t state; /*RHO is ongoing or in idle state.*/
    uint32_t aws_handle;            /*handle won't be changed after rho.*/
    uint32_t task_events;
    uint32_t rho_time;              /*for rho performance debug.*/
    bt_aws_mce_agent_state_type_t aws_state;
    //for agent
    uint32_t prepare_pending_flag;  /*use each bit to represent each user*/
    TimerHandle_t prepare_timer_handle;
    TimerHandle_t retry_timer_handle;
    //for partner
    uint8_t *rho_data;
    uint16_t rho_data_len[BT_ROLE_HANDOVER_MODULE_MAX];
    bt_aws_mce_role_handover_profile_t* profile_info;
}bt_role_handover_context_t;

static bt_role_handover_callbacks_t bt_rho_srv_callbacks[BT_ROLE_HANDOVER_MODULE_MAX] = {{0}};
static bt_role_handover_context_t bt_rho_srv_context = {{0}};
static bool bt_rho_srv_init_flag = false;
static bt_status_t bt_role_handover_aws_event_callback(bt_msg_type_t msg, bt_status_t status, void *buff);
static bt_status_t bt_role_handover_prepare_rho(void);
static void bt_role_handover_free_rho_data(void);
static void bt_role_handover_free_profile_info(void);

extern void bt_os_take_stack_mutex(void);
extern void bt_os_give_stack_mutex(void);

#define RHOS_MUTEX_LOCK() bt_os_take_stack_mutex()
#define RHOS_MUTEX_UNLOCK() bt_os_give_stack_mutex()

static uint8_t *bt_role_handover_alloc(uint16_t size)
{
    uint8_t *p = (uint8_t *)pvPortMalloc(size);
    return ((p == NULL) ? NULL : p);
}

static void bt_role_handover_free(uint8_t *p)
{
    vPortFree((void *)p);
    p = NULL;
}

/*RHO service cant use BT external timer because external timer is stopped during RHO.
For busy case, RHO service needs to start a timer to retry */
static uint32_t bt_role_handover_is_timer_active(TimerHandle_t timer_id)
{
    if ((timer_id != NULL) && (xTimerIsTimerActive(timer_id) != pdFALSE)) {
        return 1;
    } else {
        return 0;
    }
}

static void bt_role_handover_stop_timer(TimerHandle_t timer_id)
{
    BaseType_t ret;

    if  ((timer_id != NULL) && (bt_role_handover_is_timer_active(timer_id) == 1)) {
        ret = xTimerStop(timer_id, 0);
        if (ret != pdPASS) {
            LOG_MSGID_E(BT_RHO, "stop_timer fail 0x%x", 1, ret);
            configASSERT(0);
        }
    }
}

static void bt_role_handover_start_timer_ext(char *name, TimerHandle_t *timer_id, bool is_repeat, uint32_t ms, TimerCallbackFunction_t pxCallbackFunction)
{
    BaseType_t ret;

    if (*timer_id == NULL) {
        *timer_id = xTimerCreate((const char *const)name, 0xffff, is_repeat, NULL, pxCallbackFunction);
        LOG_MSGID_E(BT_RHO, "create timer_id 0x%x", 1, *timer_id);
    }
    uint32_t time_length = ms / portTICK_PERIOD_MS + 1;
    if (*timer_id == NULL) {
        return;
    }
    ret = xTimerChangePeriod(*timer_id, time_length, 0);
    if (ret != pdPASS) {
        LOG_MSGID_E(BT_RHO, "start_timer_ext fail 0x%x", 1, ret);
        configASSERT(0);
    }
}

static void bt_role_handover_timer_expired_notify(TimerHandle_t id)
{
    uint32_t mask;
    bt_os_layer_disable_interrupt(&mask);
    if (id == bt_rho_srv_context.prepare_timer_handle) {
        bt_rho_srv_context.task_events |= BT_ROLE_HANDOVER_TASK_EVENT_PREPARE_TIMER;
    } else if (id == bt_rho_srv_context.retry_timer_handle) {
        bt_rho_srv_context.task_events |= BT_ROLE_HANDOVER_TASK_EVENT_RETRY_TIMER;
    }
    bt_os_layer_enable_interrupt(mask);
    bt_trigger_interrupt(0);
}

static void bt_role_handover_init(void)
{
    //RHOS_MUTEX_LOCK();
    memset(&bt_rho_srv_context, 0, sizeof(bt_role_handover_context_t));
    bt_rho_srv_init_flag = true;
    bt_callback_manager_register_callback(bt_callback_type_app_event,
                                          (uint32_t)(MODULE_MASK_AWS_MCE),
                                          (void *)bt_role_handover_aws_event_callback);
    //RHOS_MUTEX_UNLOCK();
}

static void bt_role_handover_reset_state(void)
{
    bt_rho_srv_context.state = BT_ROLE_HANDOVER_STATE_IDLE;
}

static void bt_role_handover_status_callback(bt_role_handover_event_t event, bt_status_t status)
{
    uint32_t i;
    if (event == BT_ROLE_HANDOVER_START_IND) {
        LOG_MSGID_E(BT_RHO, "[BT_RHO]status_callback, RHO start, role 0x%x\r\n", 1, bt_rho_srv_context.role);
        //todo, use os layer for bluetooth service later
        bt_rho_srv_context.rho_time = bt_os_layer_get_hal_gpt_time();
    } else if (event == BT_ROLE_HANDOVER_COMPLETE_IND){
        uint32_t current = bt_os_layer_get_hal_gpt_time();
        LOG_MSGID_E(BT_RHO, "[BT_RHO]status_callback, RHO SRV end, role 0x%x, status 0x%x, time(ms):%ld\r\n", 3, bt_rho_srv_context.role, (unsigned int)status, current - bt_rho_srv_context.rho_time);
    }
    for (i = 0; i < BT_ROLE_HANDOVER_MODULE_MAX; i++){
        if (bt_rho_srv_callbacks[i].status_cb != NULL) {
            bt_rho_srv_callbacks[i].status_cb((const bt_bd_addr_t *)&(bt_rho_srv_context.remote_addr), bt_rho_srv_context.role, event, status);
        }
    }
}

//for agent
static void bt_role_handover_stop_timers()
{
    LOG_MSGID_I(BT_RHO, "stop_timer", 0);

    bt_role_handover_stop_timer(bt_rho_srv_context.prepare_timer_handle);
    bt_role_handover_stop_timer(bt_rho_srv_context.retry_timer_handle);
}

static void bt_role_handover_notify_agent_end(bt_status_t status)
{
    LOG_MSGID_I(BT_RHO, "notify_agent_end, status 0x%x, task_events 0x%x", 2, status, bt_rho_srv_context.task_events);
    //stop timer firstly when success, then notify success when BT task run again.
    if (status != BT_STATUS_SUCCESS) {
        bt_role_handover_stop_timers();
    }
    bt_role_handover_status_callback(BT_ROLE_HANDOVER_COMPLETE_IND, status);
    bt_role_handover_reset_state();
    bt_rho_srv_context.prepare_pending_flag = 0;
    bt_rho_srv_context.task_events = 0;
}

//for partner
static void bt_role_handover_notify_partner_end(bt_status_t status)
{
    LOG_MSGID_I(BT_RHO, "notify_partner_end, status 0x%x, task_events 0x%x", 2, status, bt_rho_srv_context.task_events);
    bt_role_handover_free_rho_data();
    bt_role_handover_free_profile_info();
    bt_role_handover_status_callback(BT_ROLE_HANDOVER_COMPLETE_IND, status);  
    bt_role_handover_reset_state();
    bt_rho_srv_context.task_events = 0;
}


bt_status_t bt_role_handover_register_callbacks(bt_role_handover_module_type_t type, bt_role_handover_callbacks_t *callbacks)
{
    //RHOS_MUTEX_LOCK();//mutex doesn't exist before BT first power on. sink init->ext timer init(register)->bt power on
    bt_status_t status = BT_STATUS_SUCCESS;
    LOG_MSGID_I(BT_RHO, "register_callbacks, type %d, status callback 0x%x", 2, 
        type, 
        type < BT_ROLE_HANDOVER_MODULE_MAX ? bt_rho_srv_callbacks[type].status_cb : 0);
    if (bt_rho_srv_init_flag == false) {
        bt_role_handover_init();
    }
    if (type < BT_ROLE_HANDOVER_MODULE_MAX && \
        bt_rho_srv_callbacks[type].status_cb == NULL && \
        callbacks->status_cb != NULL) {
        memcpy((void *)(&bt_rho_srv_callbacks[type]), (void *)callbacks, sizeof(bt_role_handover_callbacks_t));
    } else {
        LOG_MSGID_E(BT_RHO, "wrong type or already registered", 0);
        status = BT_STATUS_FAIL;
    }
    //RHOS_MUTEX_UNLOCK();            
    return status;
}

bt_status_t bt_role_handover_deregister_callbacks(bt_role_handover_module_type_t type)
{
    //RHOS_MUTEX_LOCK();
    bt_status_t status = BT_STATUS_SUCCESS;

    LOG_MSGID_I(BT_RHO, "deregister_callbacks, type %d, status callback 0x%x", 2,
        type, 
        type < BT_ROLE_HANDOVER_MODULE_MAX ? bt_rho_srv_callbacks[type].status_cb : 0);
    if (type < BT_ROLE_HANDOVER_MODULE_MAX && \
        bt_rho_srv_callbacks[type].status_cb != NULL) {
        memset(&bt_rho_srv_callbacks[type], 0, sizeof(bt_role_handover_callbacks_t));
    } else {
        LOG_MSGID_E(BT_RHO, "wrong type or not registered", 0);
        status = BT_STATUS_FAIL;
    }
    //RHOS_MUTEX_UNLOCK();
    return status;

}

//switch to BT task to handle timer to avoid timer task blocking by BT mutex.
static void bt_role_handover_post_timeout_handler(TimerHandle_t id)
{
    bt_role_handover_timer_expired_notify(id);
}

static void bt_role_handover_prepare_timeout_handler(TimerHandle_t xTimer)
{
    LOG_MSGID_I(BT_RHO, "prepare_timeout_handler, flag 0x%x", 1, bt_rho_srv_context.prepare_pending_flag);
    bt_role_handover_notify_agent_end(BT_STATUS_TIMEOUT);
}

static void bt_role_handover_retry_timeout_handler(TimerHandle_t xTimer)
{
    LOG_MSGID_I(BT_RHO, "retry_timeout_handler\r\n", 0);
    //todo, add max retry times.
    bt_status_t status = bt_role_handover_prepare_rho();
    if (status != BT_STATUS_BUSY && status != BT_STATUS_SUCCESS) {
        bt_role_handover_notify_agent_end(status);
    }
}

static bt_status_t bt_role_handover_prepare_rho(void) 
{
    bt_status_t status;
    //prepare RHO
    bt_bd_addr_t *partner_address = bt_connection_manager_device_local_info_get_peer_aws_address();
    uint32_t current = bt_os_layer_get_hal_gpt_time();
    LOG_MSGID_E(BT_RHO, "[BT_RHO]prepare_rho, role 0x%x, time(ms):%ld\r\n", 2, bt_rho_srv_context.role, current - bt_rho_srv_context.rho_time);
    status = bt_aws_mce_prepare_role_handover(bt_rho_srv_context.aws_handle, (const bt_bd_addr_t *)partner_address);
    if (status == BT_STATUS_BUSY) {
        //start a retry timer.
        bt_role_handover_start_timer_ext("RHOS retry", &bt_rho_srv_context.retry_timer_handle, false, BT_ROLE_HANDOVER_RETRY_TIME, bt_role_handover_post_timeout_handler);
    } else {
        //waiting for prepare cnf or failed.
    }
    return status;
}

bt_status_t bt_role_handover_start(void)
{
    RHOS_MUTEX_LOCK();
    uint32_t i;
    bt_status_t status = BT_STATUS_SUCCESS;
    
    bt_rho_srv_context.role = bt_connection_manager_device_local_info_get_aws_role();
    LOG_MSGID_I(BT_RHO, "start, state %d, aws handle 0x%x, role 0x%x", 3, 
        bt_rho_srv_context.state,
        bt_rho_srv_context.aws_handle,
        bt_rho_srv_context.role);
    if (bt_rho_srv_context.aws_handle == 0) {
        RHOS_MUTEX_UNLOCK();
        return BT_STATUS_ROLE_HANDOVER_AWS_CONNECTION_NOT_FOUND;
    }
    
    if (bt_rho_srv_context.role != BT_AWS_MCE_ROLE_AGENT) {
        RHOS_MUTEX_UNLOCK();
        return BT_STATUS_ROLE_HANDOVER_NOT_AGENT;
    }  
    
    if (bt_rho_srv_context.state != BT_ROLE_HANDOVER_STATE_IDLE) {
        RHOS_MUTEX_UNLOCK();
        return BT_STATUS_ROLE_HANDOVER_ONGOING;
    }

    //get remote addr again.
    bt_bd_addr_t * addr = (bt_bd_addr_t *)bt_aws_mce_get_bd_addr_by_handle(bt_rho_srv_context.aws_handle);
    if (addr == NULL) {
        RHOS_MUTEX_UNLOCK();
        return BT_STATUS_ROLE_HANDOVER_AWS_CONNECTION_NOT_FOUND;
    }
    memcpy((void *)(&(bt_rho_srv_context.remote_addr)), (void *)addr, sizeof(bt_bd_addr_t));
    
    bt_role_handover_status_callback(BT_ROLE_HANDOVER_START_IND, BT_STATUS_SUCCESS);
    bt_rho_srv_context.state = BT_ROLE_HANDOVER_STATE_ONGOING;
    
    //query all user
    for (i = 0; i < BT_ROLE_HANDOVER_MODULE_MAX; i++){
        if (bt_rho_srv_callbacks[i].allowed_cb != NULL) {
            status = bt_rho_srv_callbacks[i].allowed_cb((const bt_bd_addr_t *)&(bt_rho_srv_context.remote_addr));
            if (status != BT_STATUS_PENDING && status != BT_STATUS_SUCCESS) {
                bt_role_handover_notify_agent_end(status);
                RHOS_MUTEX_UNLOCK();
                return status;
            } else if (status == BT_STATUS_PENDING) {
                bt_rho_srv_context.prepare_pending_flag |= 1<<i;
            }
        }
    }
    LOG_MSGID_I(BT_RHO, "start, flag 0x%x", 1, bt_rho_srv_context.prepare_pending_flag);

    if (bt_rho_srv_context.prepare_pending_flag != 0) {
        //start prepare pending timer 5s.
        bt_role_handover_start_timer_ext("RHOS prepare", &bt_rho_srv_context.prepare_timer_handle, false, BT_ROLE_HANDOVER_PREPARE_TIME, bt_role_handover_post_timeout_handler);
        status = BT_STATUS_SUCCESS;
    } else {
        status = bt_role_handover_prepare_rho();
    }
    
    bt_role_handover_status_callback(BT_ROLE_HANDOVER_PREPARE_REQ_IND, BT_STATUS_SUCCESS);
    
    LOG_MSGID_I(BT_RHO, "start, status 0x%x", 1, status);
    if (status != BT_STATUS_BUSY && status != BT_STATUS_SUCCESS) {
        bt_role_handover_notify_agent_end(status);
    }
    RHOS_MUTEX_UNLOCK();
    return status;
}

bt_status_t bt_role_handover_reply_prepare_request(bt_role_handover_module_type_t type)
{
    RHOS_MUTEX_LOCK();
    bt_status_t status = BT_STATUS_SUCCESS;
    LOG_MSGID_I(BT_RHO, "prepare_ready, type %d, flag 0x%x", 2, type, bt_rho_srv_context.prepare_pending_flag);
    if (type < BT_ROLE_HANDOVER_MODULE_MAX && \
        bt_rho_srv_context.prepare_pending_flag != 0 && \
        (bt_rho_srv_context.prepare_pending_flag & (1<<type)) != 0) {
        bt_rho_srv_context.prepare_pending_flag &= ~(1<<type);
        //all user are ready
        if (bt_rho_srv_context.prepare_pending_flag == 0) {
            bt_role_handover_stop_timer(bt_rho_srv_context.prepare_timer_handle);
            bt_status_t ret = bt_role_handover_prepare_rho();
            if (ret != BT_STATUS_SUCCESS && ret != BT_STATUS_BUSY) {
                bt_role_handover_notify_agent_end(ret);
            }
        }
    } else {
        status = BT_STATUS_FAIL;
        LOG_MSGID_E(BT_RHO, "prepare_ready, type does not exist", 0);
    }
    RHOS_MUTEX_UNLOCK();
    return status;
    
}

bt_role_handover_state_t bt_role_handover_get_state(void)
{
    return bt_rho_srv_context.state;
}

static void bt_role_handover_prepare_cnf_handler(bt_status_t status, void *buff)
{
    bt_status_t ret = status;
    uint8_t * rho_data = NULL;

    if (ret == BT_STATUS_SUCCESS) {
        //query all user to get len and data
        uint8_t len[BT_ROLE_HANDOVER_MODULE_MAX];
        uint16_t total_len = 0, i;
        for (i = 0; i < BT_ROLE_HANDOVER_MODULE_MAX; i++){
            if (bt_rho_srv_callbacks[i].get_len_cb != NULL) {
                len[i] = bt_rho_srv_callbacks[i].get_len_cb((const bt_bd_addr_t *)&(bt_rho_srv_context.remote_addr));
                if (len[i] > 0) {
                    total_len += len[i] + BT_ROLE_HANDOVER_DATA_HEADER_PER_MODULE;
                }
            }
        }
        if (total_len != 0) {
            if (total_len > 0xff) {
                LOG_MSGID_I(BT_RHO, "prepare_cnf_handler, len %d is too large!", 1, total_len);
                bt_role_handover_notify_agent_end(BT_STATUS_UNSUPPORTED);
                return;
            }
            rho_data = bt_role_handover_alloc(total_len);
            if (rho_data != NULL) {
                uint8_t *p = rho_data;
                for (i = 0; i < BT_ROLE_HANDOVER_MODULE_MAX; i++){
                    if (bt_rho_srv_callbacks[i].get_data_cb != NULL && len[i] > 0) {
                        p[0] = i; //type
                        p[1] = len[i]; //len
                        LOG_MSGID_I(BT_RHO, "prepare_cnf_handler, type 0x%x, len 0x%x", 2, p[0], p[1]);
                        ret = bt_rho_srv_callbacks[i].get_data_cb((const bt_bd_addr_t *)&(bt_rho_srv_context.remote_addr), p+BT_ROLE_HANDOVER_DATA_HEADER_PER_MODULE);//value
                        if (ret == BT_STATUS_SUCCESS) {
                            p += len[i] + BT_ROLE_HANDOVER_DATA_HEADER_PER_MODULE;
                        } else {
                            total_len -= len[i] + 2;
                            len[i] = 0;
                            p[0] = 0;
                            p[1] = 0;
                        }
                    }
                }
            } else {
                LOG_MSGID_I(BT_RHO, "prepare_cnf_handler, len %d, heap oom", 1, total_len);
                bt_role_handover_notify_agent_end(BT_STATUS_OUT_OF_MEMORY);
                return;
            }
            
        } else {
            LOG_MSGID_I(BT_RHO, "prepare_cnf_handler, data is empty", 0);
        }
        ret = bt_aws_mce_role_handover(bt_rho_srv_context.aws_handle, rho_data, total_len);
    } 
    //free buffer
    if (rho_data != NULL) {
        bt_role_handover_free(rho_data);
    }
    if (ret == BT_STATUS_BUSY) {
        //start a retry timer
        bt_role_handover_start_timer_ext("RHOS retry", &bt_rho_srv_context.retry_timer_handle, false, BT_ROLE_HANDOVER_RETRY_TIME, bt_role_handover_post_timeout_handler);
    } else if (ret != BT_STATUS_SUCCESS) {
        //notify fail
        bt_role_handover_notify_agent_end(ret);
    }
}

static void bt_role_handover_rho_cnf_handler(bt_status_t status, void *buff)
{
    if (status == BT_STATUS_SUCCESS) {
        //waiting for status change ind.
    } else if (status == BT_STATUS_BUSY) {
        //start a retry timer
        bt_role_handover_start_timer_ext("RHOS retry", &bt_rho_srv_context.retry_timer_handle, false, BT_ROLE_HANDOVER_RETRY_TIME, bt_role_handover_post_timeout_handler);
    } else {
        //notify fail
        bt_role_handover_notify_agent_end(status);
    }

}

//for partner
static void bt_role_handover_store_rho_data(uint16_t len, uint8_t *data)
{
    if (len != 0) {
        bt_rho_srv_context.rho_data = bt_role_handover_alloc(len);
        if (bt_rho_srv_context.rho_data != NULL) {
            memcpy((void *)(bt_rho_srv_context.rho_data), (void *)data, len);

            //parse the data len of each module.
            uint16_t total_len = 0;
            uint8_t * p = bt_rho_srv_context.rho_data;
            while (total_len < len) {
                uint8_t type = p[0];
                configASSERT(type <= BT_ROLE_HANDOVER_MODULE_MAX);
                uint8_t data_len = p[1];
                bt_rho_srv_context.rho_data_len[type] = data_len;
                total_len += data_len + BT_ROLE_HANDOVER_DATA_HEADER_PER_MODULE;
                p += data_len + BT_ROLE_HANDOVER_DATA_HEADER_PER_MODULE;
            }
        } else {
            LOG_MSGID_I(BT_RHO, "rho_ind_handler, store data, len %d, heap OOM", 1, len);
            bt_role_handover_notify_partner_end(BT_STATUS_OUT_OF_MEMORY);
        }
    }
}

//for partner
static void bt_role_handover_free_rho_data(void)
{
    if (bt_rho_srv_context.rho_data != NULL) {
        bt_role_handover_free(bt_rho_srv_context.rho_data);
        bt_rho_srv_context.rho_data = NULL;
        memset(bt_rho_srv_context.rho_data_len, 0, sizeof(uint16_t) * BT_ROLE_HANDOVER_MODULE_MAX);
    }
}

//for partner
static void bt_role_handover_store_profile_info(bt_aws_mce_role_handover_profile_t *profile_info)
{
    bt_rho_srv_context.profile_info = (bt_aws_mce_role_handover_profile_t*)bt_role_handover_alloc(sizeof(bt_aws_mce_role_handover_profile_t));
    if (bt_rho_srv_context.profile_info) {
        memcpy((void *)(bt_rho_srv_context.profile_info), (void *)profile_info, sizeof(bt_aws_mce_role_handover_profile_t));
    } else {
        LOG_MSGID_I(BT_RHO, "rho_ind_handler, store profile info, heap OOM", 0);
        bt_role_handover_notify_partner_end(BT_STATUS_OUT_OF_MEMORY);
    }

}

//for partner
static void bt_role_handover_free_profile_info(void)
{
    if (bt_rho_srv_context.profile_info != NULL) {
        bt_role_handover_free((uint8_t *)bt_rho_srv_context.profile_info);
        bt_rho_srv_context.profile_info = NULL;
    }
}

static void bt_role_handover_rho_ind_handler(bt_status_t status, void *buff)
{
    bt_aws_mce_role_handover_ind_t* rho_ind = (bt_aws_mce_role_handover_ind_t*)buff;
    
    bt_rho_srv_context.role = bt_connection_manager_device_local_info_get_aws_role();
    if (status == BT_STATUS_SUCCESS) {
        //get remote addr again.
        bt_bd_addr_t * addr = (bt_bd_addr_t *)bt_aws_mce_get_bd_addr_by_handle(bt_rho_srv_context.aws_handle);
        if (addr == NULL) {
            return;
        }
        memcpy((void *)(&(bt_rho_srv_context.remote_addr)), (void *)addr, sizeof(bt_bd_addr_t));
        bt_rho_srv_context.state = BT_ROLE_HANDOVER_STATE_ONGOING;
        //store data/profile info and wait for status change ind.
        bt_role_handover_status_callback(BT_ROLE_HANDOVER_START_IND, BT_STATUS_SUCCESS);
        bt_role_handover_store_rho_data(rho_ind->length, rho_ind->data);
        bt_role_handover_store_profile_info(&(rho_ind->profile_info));
    } 
}

static void bt_role_handover_state_change_ind_notify(void)
{
    uint32_t mask;
    bt_os_layer_disable_interrupt(&mask);
    bt_rho_srv_context.task_events |= BT_ROLE_HANDOVER_TASK_EVENT_RX_STATE_CHANGE_IND_OK;
    bt_os_layer_enable_interrupt(mask);
    bt_trigger_interrupt(0);
}

static void bt_role_handover_update_handler(bt_status_t status)
{
    if (bt_rho_srv_context.role == BT_AWS_MCE_ROLE_AGENT) {
        bt_role_handover_notify_agent_end(status);
    } else {
        bt_role_handover_notify_partner_end(status);
    }
    
    //update role, handle is same, addr will be updated when start rho again.
    if (status == BT_STATUS_SUCCESS) {
        bt_rho_srv_context.role = (bt_rho_srv_context.role == BT_AWS_MCE_ROLE_AGENT) ? BT_AWS_MCE_ROLE_PARTNER : BT_AWS_MCE_ROLE_AGENT;
    }

}

static bool bt_role_handover_is_valid_state_change_ind(bt_aws_mce_agent_state_type_t pre_state)
{
    //state change might be reported before RHO state change.
    if ((bt_rho_srv_context.role == BT_AWS_MCE_ROLE_AGENT && \
        (pre_state == BT_AWS_MCE_AGENT_STATE_ATTACHED && bt_rho_srv_context.aws_state == BT_AWS_MCE_AGENT_STATE_NONE)) || \
        (bt_rho_srv_context.role == BT_AWS_MCE_ROLE_PARTNER && \
        (pre_state == BT_AWS_MCE_AGENT_STATE_NONE && bt_rho_srv_context.aws_state == BT_AWS_MCE_AGENT_STATE_ATTACHED))) {
        return true;
    }
    LOG_MSGID_E(BT_RHO, "ignore invalid state change ind for RHO SRV, role 0x%x, pre_state 0x%x, aws_state 0x%x", 3, 
        bt_rho_srv_context.role,
        pre_state,
        bt_rho_srv_context.aws_state);
    return false;
}

static void bt_role_handover_state_change_ind_handler(bt_status_t status, void *buff)
{
    bt_aws_mce_state_change_ind_t *ind = (bt_aws_mce_state_change_ind_t *)buff;
    bt_role_handover_state_t state = bt_role_handover_get_state();
    bt_aws_mce_agent_state_type_t pre_state;
    if (ind->handle == bt_rho_srv_context.aws_handle) {
        pre_state = bt_rho_srv_context.aws_state;
        bt_rho_srv_context.aws_state = ind->state;
    } else {
        LOG_MSGID_I(BT_RHO, "state_change_ind_handler, aws_handle 0x%x != context handle 0x%x", 2, 
            ind->handle, 
            bt_rho_srv_context.aws_handle);
        return;
    }
    if (state == BT_ROLE_HANDOVER_STATE_ONGOING) {
        bool valid = bt_role_handover_is_valid_state_change_ind(pre_state);
        if (valid == false) {
            return;
        }
        if (status == BT_STATUS_SUCCESS) {
            //RHO role change event
            int8_t i;
            uint8_t *p = bt_rho_srv_context.rho_data;
            
            bt_role_handover_update_info_t info = {0};
            info.addr = (const bt_bd_addr_t *)&(bt_rho_srv_context.remote_addr);
            info.profile_info = bt_rho_srv_context.profile_info;
            info.role = bt_rho_srv_context.role;
            LOG_MSGID_I(BT_RHO, "state_change_ind_handler, addr:0x%x:%x:%x:%x:%x:%x ", 6, 
                (*(info.addr))[5], 
                (*(info.addr))[4], 
                (*(info.addr))[3],
                (*(info.addr))[2],
                (*(info.addr))[1],
                (*(info.addr))[0]);
            
            if (bt_rho_srv_context.role == BT_AWS_MCE_ROLE_AGENT) {
                for (i = BT_ROLE_HANDOVER_MODULE_MAX - 1; i >= 0; i--) {
                    if (bt_rho_srv_callbacks[i].update_cb != NULL) {
                        bt_rho_srv_callbacks[i].update_cb(&info);
                    }
                }
            } else {
                for (i = 0; i < BT_ROLE_HANDOVER_MODULE_MAX; i++) {
                    if (bt_rho_srv_callbacks[i].update_cb != NULL) {
                        if (bt_rho_srv_context.rho_data_len[i] != 0) {
                            uint16_t offset = bt_rho_srv_context.rho_data_len[i] + BT_ROLE_HANDOVER_DATA_HEADER_PER_MODULE;//type and len are two bytes.
                            info.data = p + BT_ROLE_HANDOVER_DATA_HEADER_PER_MODULE;
                            info.length = bt_rho_srv_context.rho_data_len[i];
                            p += offset;
                        } else {
                            info.data = NULL;
                            info.length = 0;
                        }
                        bt_rho_srv_callbacks[i].update_cb(&info);
                    }
                }
            }
            
            //notify success when BT task run again.
            bt_role_handover_stop_timers();
            bt_role_handover_state_change_ind_notify();
        }else {
            bt_role_handover_update_handler(status);
        }
    } else {
        LOG_MSGID_I(BT_RHO, "state_change_ind_handler, state 0x%x, status 0x%x, aws_handle 0x%x", 3, state, status, ind->handle);
    }
        
}

static void bt_role_handover_disconnect_handler(bt_status_t status, void *buff)
{
    bt_aws_mce_disconnected_t *ind = (bt_aws_mce_disconnected_t *)buff;
    bt_role_handover_state_t state = bt_role_handover_get_state();
    if (state == BT_ROLE_HANDOVER_STATE_ONGOING) {
        //Disconnect during RHO.
        if (ind->handle == bt_rho_srv_context.aws_handle) {
            if (bt_rho_srv_context.role == BT_AWS_MCE_ROLE_PARTNER) {
                bt_role_handover_notify_partner_end(ind->reason);
            } else {
                bt_role_handover_notify_agent_end(ind->reason);
            }
        }
    }
    
    //clear handle & addr
    if (ind->handle == bt_rho_srv_context.aws_handle) {
        bt_rho_srv_context.aws_handle = 0;
        bt_rho_srv_context.aws_state = BT_AWS_MCE_AGENT_STATE_NONE;
        memset(&(bt_rho_srv_context.remote_addr), 0, sizeof(bt_bd_addr_t));
    }
}

static bt_status_t bt_role_handover_aws_event_callback(bt_msg_type_t msg, bt_status_t status, void *buff)
{
    if (msg != BT_AWS_MCE_CONNECTED && \
        msg != BT_AWS_MCE_DISCONNECTED && \
        msg != BT_AWS_MCE_PREPARE_ROLE_HANDOVER_CNF && \
        msg != BT_AWS_MCE_ROLE_HANDOVER_CNF && \
        msg != BT_AWS_MCE_ROLE_HANDOVER_IND && \
        msg != BT_AWS_MCE_STATE_CHANGED_IND) {
        return BT_STATUS_SUCCESS;
    } else {
        /*if bt task is blocked by other high priority tasks when handle rx,
        need to handle rho event firstly before handle other aws events. */
        if (bt_rho_srv_context.task_events & BT_ROLE_HANDOVER_TASK_EVENT_RX_STATE_CHANGE_IND_OK) {
            LOG_MSGID_I(BT_RHO, "notify previous rho result firstly, evt 0x%x", 1, bt_rho_srv_context.task_events);
            bt_rho_srv_context.task_events &= ~BT_ROLE_HANDOVER_TASK_EVENT_RX_STATE_CHANGE_IND_OK;
            bt_role_handover_update_handler(BT_STATUS_SUCCESS);
        }
    }

    LOG_MSGID_I(BT_RHO, "event callback, msg 0x%x, status 0x%x, state 0x%x", 3, msg, status, bt_role_handover_get_state());
    switch (msg) {
        case BT_AWS_MCE_CONNECTED:
        {
            if (status == BT_STATUS_SUCCESS) {
                bt_aws_mce_connected_t *connected = (bt_aws_mce_connected_t *)buff;
                bt_bd_addr_t *local_addr = bt_connection_manager_device_local_info_get_local_address();
                //check if it's the phone/agent connection, record handle & addr.
                if (memcmp(local_addr, connected->address, sizeof(bt_bd_addr_t)) != 0) {
                    bt_rho_srv_context.aws_handle = connected->handle;
                    memcpy((void *)(&(bt_rho_srv_context.remote_addr)), (void *)(connected->address), sizeof(bt_bd_addr_t));
                }
            }
            break;
        }
        case BT_AWS_MCE_DISCONNECTED:
        {
            bt_role_handover_disconnect_handler(status, buff);
            break;
        }
        case BT_AWS_MCE_PREPARE_ROLE_HANDOVER_CNF:
        {
            bt_role_handover_prepare_cnf_handler(status, buff);
            break;
        }
        case BT_AWS_MCE_ROLE_HANDOVER_CNF:
        {
            bt_role_handover_rho_cnf_handler(status, buff);
            break;
        }
        case BT_AWS_MCE_ROLE_HANDOVER_IND:
        {
            bt_role_handover_rho_ind_handler(status, buff);
            break;
        }
        case BT_AWS_MCE_STATE_CHANGED_IND:
        {
            bt_role_handover_state_change_ind_handler(status, buff);
            break;
        }
    }
    return BT_STATUS_SUCCESS;

}

int32_t bt_role_handover_handle_interrupt(void)
{
    uint32_t current_events, mask;
    RHOS_MUTEX_LOCK();
    bt_os_layer_disable_interrupt(&mask);
    current_events = bt_rho_srv_context.task_events;
    bt_rho_srv_context.task_events = 0;
    bt_os_layer_enable_interrupt(mask);

    if (current_events & BT_ROLE_HANDOVER_TASK_EVENT_RETRY_TIMER) {
        bt_role_handover_retry_timeout_handler(NULL);
    }

    if (current_events & BT_ROLE_HANDOVER_TASK_EVENT_PREPARE_TIMER) {
        bt_role_handover_prepare_timeout_handler(NULL);
    }

    if (current_events & BT_ROLE_HANDOVER_TASK_EVENT_RX_STATE_CHANGE_IND_OK) {
        bt_role_handover_update_handler(BT_STATUS_SUCCESS);
    }
    
    RHOS_MUTEX_UNLOCK();
    return BT_STATUS_SUCCESS;
}
#endif /*#if defined(MTK_AWS_MCE_ENABLE) */

