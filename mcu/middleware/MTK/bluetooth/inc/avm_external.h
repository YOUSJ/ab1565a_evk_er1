/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef _AVM_EXTERNAL_H_
#define _AVM_EXTERNAL_H_

//#include "types.h"
//#include "bt_os_manager.h"
//#include "config.h"
//#include "bt_types.h"

//<<<=====================================================================>>>//
//<<                            INCLUDE HEADER FILES                       >>//
//<<<=====================================================================>>>//
#if 1

//<<<=====================================================================>>>//
//<<                            Constant                                  >>//
//<<<=====================================================================>>>//

/**
 * @brief    Bluetooth memory buffer types.
 * @{
 */
#if 0
typedef enum {
    BT_MEMORY_TX_BUFFER = 0,     /**< TX packet buffer, a buffer type for the Memory Management module.*/
    BT_MEMORY_RX_BUFFER,         /**< RX packet buffer, a buffer type for the Memory Management module.*/
    BT_MEMORY_CONTROLLER_BUFFER  /**< Controller buffer, a buffer type for the Memory Management module.*/
} bt_memory_packet_t;

typedef enum{
    BT_QUEUE_TYPE_RX = 0,
    BT_QUEUE_TYPE_TX_ACL,
    BT_QUEUE_TYPE_TX_CMD,
    BT_QUEUE_TYPE_TX_IF_PACKET
}bt_hb_queue_type_t;
#endif

/*structure for getting BT clock*/
typedef struct bt_stru_bttime
{
    uint32_t period;
    uint16_t phase;
} BT_TIME_STRU, *BT_TIME_STRU_PTR;

#define     BT_MEMORY_TYPE_TX_BUFFER          0      /**< TX packet buffer, a buffer type for the Memory Management module.*/
#define     BT_MEMORY_TYPE_RX_BUFFER          1      /**< RX packet buffer, a buffer type for the Memory Management module.*/
#define     BT_MEMORY_TYPE_CONTROLLER_BUFFER  2      /** < Controller buffer, a buffer type for the Memory Management module.*/
#define     BT_QUEUE_RX_TYPE                  0
#define     BT_QUEUE_TX_ACL_TYPE              1
#define     BT_QUEUE_TX_CMD_TYPE              2
#define     BT_QUEUE_TX_IF_PACKET_TYPE        3





//<<<=====================================================================>>>//
//<<                            Data Type                                 >>//
//<<<=====================================================================>>>//
typedef struct avm_pka_callbacks {
    unsigned char *(*bt_hb_mm_allocate)(uint8_t type, unsigned int size);
    void (*bt_hb_mm_free)(uint8_t type, unsigned char *ptr);
    void (*bt_hb_rx_enqueue)(unsigned char *hb_header);
    void *(*bt_hb_tx_dequeue)(uint8_t type, unsigned char *q_header);
    unsigned short (*bt_get_hb_header_size)(void);
    void (*bt_rx_notify_hb)(void);
} avm_pka_callbacks_t;

//<<<=====================================================================>>>//
//<<                           Function Declaration                        >>//
//<<<=====================================================================>>>//

void bt_avm_pka_register_callbacks(avm_pka_callbacks_t callbacks);
unsigned short bt_get_pka_header_size(uint8_t type);
void bt_tx_notify_pka(uint8_t type, unsigned char *q_header);
unsigned char bt_pka_get_bt_clock(uint16_t hci_handle, BT_TIME_STRU_PTR current_bt_clk);

char* bt_pikachu_lib_verno(void);
char* bt_pikachu_lib_lastest_commit(void);
//<<<=====================================================================>>>//
//<<                           External Reference                          >>//
//<<<=====================================================================>>>//
extern avm_pka_callbacks_t avm_callbacks;

//<<<=====================================================================>>>//
//<<                          External Function Reference                 >>//
//<<<=====================================================================>>>//

//<<<=====================================================================>>>//
//<<                            MACRO                                     >>//
//<<<=====================================================================>>>//



#endif
#endif // _AVM_EXTERNAL_H_

