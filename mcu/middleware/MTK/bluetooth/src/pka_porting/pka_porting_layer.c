/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */



#include "FreeRTOS.h"
#include "hal_gpt.h"
#include "pka_porting_layer.h"
#include "semphr.h"
#include "task.h"
#include "timers.h"
#include "hal_nvic_internal.h"
#include "hal_nvic.h"
#include "memory_attribute.h"
#include "task_def.h"
#include "uECC.h"
#include "avm_external.h"
#include "hal_dvfs.h"
#include "hal_platform.h"
#include "syslog.h"

#if !defined(MBEDTLS_CONFIG_FILE)^M
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif
#include "mbedtls/md.h"
#include "mbedtls/sha256.h"


#define FEA_SUPP_AVM_PORTING_LAYER    (TRUE)

extern unsigned int port_OsStart;
extern avm_pka_callbacks_t avm_callbacks;

 ////////////////////////////////////////////////////////////////////////////////
 // HEAP MEMORY FUNCTION PROTOTYPES /////////////////////////////////////////////
 ////////////////////////////////////////////////////////////////////////////////

 /**
  * @brief Heap Memory allocation
  *
  * @param Size Number of bytes memory to allocate
  * @return Pointer of allocated memory
  */
ATTR_TEXT_IN_TCM void* pka_heap_malloc (uint32_t  Size)
 {
        //printf("malloc %d\n", Size);

#if (!FEA_SUPP_AVM_PORTING_LAYER)
     return pvPortMallocNC(Size);
#else
    //bt_debug_port(0,1);
    void *ptr = avm_callbacks.bt_hb_mm_allocate(BT_MEMORY_TYPE_CONTROLLER_BUFFER, Size);
    //void *ptr = pvPortMallocNC(Size);
    //bt_debug_port(0,0);
     return ptr;
#endif
 }

 /**
  * @brief free specific memory space
  *
  * @param ptr memory to free
  */
ATTR_TEXT_IN_TCM void pka_heap_free (void* ptr)
 {
         //printf("free ptr");
#if (!FEA_SUPP_AVM_PORTING_LAYER)
     return vPortFreeNC(ptr);
#else

    //bt_debug_port(1,1);
    avm_callbacks.bt_hb_mm_free(BT_MEMORY_TYPE_CONTROLLER_BUFFER, ptr);
    //vPortFreeNC(ptr);
    //bt_debug_port(1,0);

#endif
     //vPortFree(ptr);
 }

//

ATTR_TEXT_IN_TCM char* pka_os_get_task_name(void)
{
    return pcTaskGetTaskName(NULL);
}

ATTR_TEXT_IN_TCM void pka_os_task_resume(void* taskHandle)
{
    vTaskResume(taskHandle);
}


ATTR_TEXT_IN_TCM uint32_t  pka_os_task_resume_from_isr(void* taskHandle)
{
    return xTaskResumeFromISR(taskHandle);
}

ATTR_TEXT_IN_TCM void pka_os_yield_from_isr(void)
{
    portYIELD_FROM_ISR(true);
}

ATTR_TEXT_IN_TCM uint32_t pka_os_is_run_in_isr(void)
{
    if (HAL_NVIC_QUERY_EXCEPTION_NUMBER > 0) {
        /* is ISR context */
        return true;
    } else {
        /* is Task context */
        return false;
    }
}


ATTR_TEXT_IN_TCM void *pka_os_get_timer_id( void * xTimer )
{
    return pvTimerGetTimerID(xTimer);
}


ATTR_TEXT_IN_TCM void pka_os_cancel_timer( void * xTimer )
{
    xTimerDelete(xTimer, 0);
}


ATTR_TEXT_IN_TCM void* pka_os_create_timer( const char * const pcTimerName, const uint32_t  xTimerPeriodInTicks, const uint32_t  uxAutoReload, void * const pvTimerID, TimerCallbackFunction_t pxCallbackFunction )
{

    return xTimerCreate(pcTimerName, xTimerPeriodInTicks, uxAutoReload, pvTimerID, pxCallbackFunction);

}

ATTR_TEXT_IN_TCM void pka_os_start_timer( void * xTimer )
{
    xTimerStart(xTimer, 0);
}



ATTR_TEXT_IN_TCM void pka_os_task_suspend(void* taskHandle)
{
    vTaskSuspend(taskHandle);
}

ATTR_TEXT_IN_TCM void pka_os_task_notify_wait(uint32_t  ulBitsToClearOnEntry, uint32_t  ulBitsToClearOnExit, uint32_t  *pulNotificationValue)
{
    xTaskNotifyWait( ulBitsToClearOnEntry, ulBitsToClearOnExit, pulNotificationValue, portMAX_DELAY);
}

ATTR_TEXT_IN_TCM void pka_os_task_notify_from_isr( void* xTaskToNotify, uint32_t  ulValue, xNotifyAction eAction, BaseType_t *pxHigherPriorityTaskWoken )
{
    xTaskNotifyFromISR(xTaskToNotify, ulValue, eAction,pxHigherPriorityTaskWoken);
}

ATTR_TEXT_IN_TCM void pka_os_task_notify( void* xTaskToNotify, uint32_t  ulValue, xNotifyAction eAction)
{
    xTaskNotify(xTaskToNotify, ulValue, eAction);
}



void* pka_os_semaphore_init()
{
    return xSemaphoreCreateRecursiveMutex();
}



ATTR_TEXT_IN_TCM void pka_os_semaphore_take(void* pSemaphore)
{
    if(xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED) {
        return;
    }

 //   if(port_OsStart)
    {
        xSemaphoreTakeRecursive( pSemaphore, portMAX_DELAY );
    }
}

ATTR_TEXT_IN_TCM void pka_os_semaphore_give(void* pSemaphore)
{
    if(xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED) {
        return;
    }


    //if(port_OsStart)
    {
        xSemaphoreGiveRecursive( pSemaphore );
    }
}


void pka_os_task_create(TaskFunction_t fTaskEntry, const char * const taskName, uint32_t  stackSize, void * const pParameters, uint8_t  taskPriority, void* taskHandle)
{
    xTaskCreate( fTaskEntry, taskName, stackSize, pParameters, taskPriority, taskHandle );

}


ATTR_TEXT_IN_TCM uint32_t  pka_os_get_interrupt_mask(void)
{
    uint32_t  nvic_mask;
    hal_nvic_save_and_set_interrupt_mask(&nvic_mask);
    return nvic_mask;
}

ATTR_TEXT_IN_TCM void pka_os_restore_interrupt_mask(uint32_t  nvic_mask)
{
    hal_nvic_restore_interrupt_mask(nvic_mask);
}

void pka_os_register_bt_interrupt(bt_isr_t intr_handler)
{
    hal_nvic_register_isr_handler(BT_IRQn, (hal_nvic_isr_t)intr_handler);
}

void pka_os_register_bt_timer_interrupt(bt_isr_t intr_handler)
{
    hal_nvic_register_isr_handler(BT_TIMER_IRQn, (hal_nvic_isr_t)intr_handler);
}

void pka_os_enable_bt_and_timer_interrupt()
{
    hal_nvic_enable_irq(BT_IRQn);
    hal_nvic_enable_irq(BT_TIMER_IRQn);
}

void pka_os_disable_bt_and_timer_interrupt()
{
    hal_nvic_disable_irq(BT_IRQn);
    hal_nvic_disable_irq(BT_TIMER_IRQn);
}

uint32_t pka_os_get_lm_task_priority(void)
{
    return TASK_PRIORITY_SOFT_REALTIME;
}

uint32_t pka_os_get_lc_task_priority(void)
{
    return TASK_PRIORITY_BT_CMD_TASK;
}
uint32_t pka_os_get_lc_process_task_priority(void)
{
    return TASK_PRIORITY_BT_ROUTINE_TASK;
}


volatile uint32_t nvic_iser0 = 0;
volatile uint32_t nvic_iser1 = 0;
#define NVIC_BT_IRQS_MASK (0x0f)
#define NVIC_32_BIT_REG_MASK (0x1f)

/*Disable all irq except BT irq.*/
ATTR_TEXT_IN_TCM void pka_disable_all_irq_except_bt(void)
{
    uint32_t mask;

    hal_nvic_save_and_set_interrupt_mask(&mask);
    nvic_iser0 = NVIC->ISER[0];
    nvic_iser1 = NVIC->ISER[1];

	NVIC->ICER[0] = nvic_iser0;
	NVIC->ICER[1] = nvic_iser1 & (~(NVIC_BT_IRQS_MASK << (BT_IRQn & NVIC_32_BIT_REG_MASK)));
    hal_nvic_restore_interrupt_mask(mask);
}

ATTR_TEXT_IN_TCM void pka_restore_all_irq_except_bt(void)
{
    uint32_t mask;

    hal_nvic_save_and_set_interrupt_mask(&mask);
	NVIC->ISER[0] = nvic_iser0;
	NVIC->ISER[1] = NVIC->ISER[1] | (nvic_iser1 & (~(NVIC_BT_IRQS_MASK << (BT_IRQn & NVIC_32_BIT_REG_MASK))));
	nvic_iser0 = 0;
	nvic_iser1 = 0;
    hal_nvic_restore_interrupt_mask(mask);


}

void pka_uECC_p192_compute_public_key(uint8_t *privatekey, uint8_t *publicKey)
{
    uECC_compute_public_key(privatekey,publicKey,uECC_secp192r1());
}

void pka_uECC_p192_shared_secret(uint8_t * remotePublicKey, uint8_t *privatekey, uint8_t *DHKey)
{
    /* big endian input */
    uECC_shared_secret(remotePublicKey,privatekey, DHKey, uECC_secp192r1());
}

int8_t pka_hal_gpt_delay_us( uint32_t us)
{
    return hal_gpt_delay_us(us);
}

void pka_uECC_p256_compute_public_key(uint8_t *privatekey, uint8_t *publicKey)
{
    uECC_compute_public_key(privatekey,publicKey,uECC_secp256r1());
}

void pka_uECC_p256_shared_secret(uint8_t * remotePublicKey, uint8_t *privatekey, uint8_t *DHKey)
{
    /* big endian input */
    uECC_shared_secret(remotePublicKey,privatekey, DHKey, uECC_secp256r1());
}

void pka_external_hmac_sha256(uint8_t *input, uint8_t ilen, uint8_t *key, uint8_t keylen, uint8_t *output)
{
#ifdef MBEDTLS_CONFIG_FILE
    /* big endian input */
    mbedtls_md_hmac(mbedtls_md_info_from_type(MBEDTLS_MD_SHA256),
            key, keylen, input, ilen, output);
#endif
#if 0
    uint8_t i;
    printf("input %d", ilen);
    for(i=0; i<ilen; i+=8)
    {
        printf("%d - %d: %x %x %x %x %x %x %x %x", i, i+7, input[i], input[i+1], input[i+2], input[i+3], input[i+4], input[i+5], input[i+6], input[i+7]);
    }
    printf("key %d", keylen);
    for(i=0; i<keylen; i+=8)
    {
        printf("%d - %d: %x %x %x %x %x %x %x %x", i, i+7, key[i], key[i+1], key[i+2], key[i+3], key[i+4], key[i+5], key[i+6], key[i+7]);
    }
    printf("output");
    for(i=0; i<16; i+=8)
    {
        printf("%d - %d: %x %x %x %x %x %x %x %x", i, i+7, output[i], output[i+1], output[i+2], output[i+3], output[i+4], output[i+5], output[i+6], output[i+7]);
    }
#endif
}

void pka_external_sha256(uint8_t *input, uint8_t ilen, uint8_t *output)
{
#ifdef MBEDTLS_CONFIG_FILE
    mbedtls_sha256_ret( input, ilen, output, 0);
#endif
#if 0
    uint8_t i;
    printf("input %d", ilen);
    for(i=0; i<ilen; i+=8)
    {
        printf("%d - %d: %x %x %x %x %x %x %x %x", i, i+7, input[i], input[i+1], input[i+2], input[i+3], input[i+4], input[i+5], input[i+6], input[i+7]);
    }
    printf("output");
    for(i=0; i<32; i+=8)
    {
        printf("%d - %d: %x %x %x %x %x %x %x %x", i, i+7, output[i], output[i+1], output[i+2], output[i+3], output[i+4], output[i+5], output[i+6], output[i+7]);
    }
#endif
}

void pka_dvfs_lock_control_SpeedUpTo208M(uint8_t lock)
{
    hal_dvfs_lock_control(HAL_DVFS_HIGH_SPEED_208M,lock);;
}

void pka_hal_nvic_save_and_set_interrupt_mask_special(uint32_t* mask)
{
    hal_nvic_save_and_set_interrupt_mask_special(mask);
}

void pka_hal_nvic_restore_interrupt_mask_special(uint32_t mask)
{
    hal_nvic_restore_interrupt_mask_special(mask);
}

void pka_llcp_print(uint8_t* lmp_log_buffer_array,uint32_t* lmp_log_buffer_length_array,uint32_t ret_len)
{
    LOG_TLVDUMP_I(common, LOG_TYPE_BT_LMP_LLCP_DATA, lmp_log_buffer_array, lmp_log_buffer_length_array, ret_len);
}


