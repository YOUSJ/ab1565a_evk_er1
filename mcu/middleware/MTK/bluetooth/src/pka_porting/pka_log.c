/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */ 

#include "string.h"
#include "syslog.h"
log_create_module(PKA_DM2L, PRINT_LEVEL_INFO);
log_create_module(PKA_LC, PRINT_LEVEL_INFO);
log_create_module(PKA_COMMON, PRINT_LEVEL_INFO);

typedef struct {
    log_control_block_t* log_ctrl_blk;
    char * module;
} bt_log_ctrl_blk_t;

const static bt_log_ctrl_blk_t pka_log_filter_blk[] = {
       {&log_control_block_PKA_DM2L, "[PKA_LOG_DM2L]"},
       {&log_control_block_PKA_LC, "[PKA_LOG_USER01]"},
       {&log_control_block_PKA_COMMON, "[PKA_COMMON]"},
};

static log_control_block_t* bt_debug_find_ctrl_blk_by_module(const char *module)
{
    uint32_t idx = 0;
    const uint32_t bt_log_ctrl_blk_len = sizeof(pka_log_filter_blk) / sizeof(bt_log_ctrl_blk_t);
    while (idx < bt_log_ctrl_blk_len) {
        if (memcmp(pka_log_filter_blk[idx].module, module, strlen(module)) == 0) {
            break;
        }
        ++idx;
    }
    if (idx == bt_log_ctrl_blk_len) {
        idx = 0;
    }
    return pka_log_filter_blk[idx].log_ctrl_blk;
}

void bt_pka_log_msgid_i(const char *module, const char *message, uint32_t arg_cnt, ...)
{
#if !defined (MTK_DEBUG_LEVEL_NONE)
    va_list ap;
    log_control_block_t *block_name = bt_debug_find_ctrl_blk_by_module(module);

    va_start(ap, arg_cnt);
    log_print_msgid(block_name, PRINT_LEVEL_INFO, message, arg_cnt, ap);
    va_end(ap);
#endif
}

void bt_pka_log_msgid_d(const char *module, const char *message, uint32_t arg_cnt, ...)
{
#if !defined (MTK_DEBUG_LEVEL_NONE)
    va_list ap;
    log_control_block_t *block_name = bt_debug_find_ctrl_blk_by_module(module);

    va_start(ap, arg_cnt);
    log_print_msgid(block_name, PRINT_LEVEL_DEBUG, message, arg_cnt, ap);
    va_end(ap);
#endif
}

void bt_pka_log_msgid_w(const char *module, const char *message, uint32_t arg_cnt, ...)
{
#if !defined (MTK_DEBUG_LEVEL_NONE)
    va_list ap;
    log_control_block_t *block_name = bt_debug_find_ctrl_blk_by_module(module);

    va_start(ap, arg_cnt);
    log_print_msgid(block_name, PRINT_LEVEL_WARNING, message, arg_cnt, ap);
    va_end(ap);
#endif
}

void bt_pka_log_msgid_e(const char *module, const char *message, uint32_t arg_cnt, ...)
{
#if !defined (MTK_DEBUG_LEVEL_NONE)
    va_list ap;
    log_control_block_t *block_name = bt_debug_find_ctrl_blk_by_module(module);

    va_start(ap, arg_cnt);
    log_print_msgid(block_name, PRINT_LEVEL_ERROR, message, arg_cnt, ap);
    va_end(ap);
#endif
}

