/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "bt_sink_srv_utils.h"
#include "bt_sink_srv_music.h"
#include "bt_sink_srv_ami.h"
#include "audio_src_srv.h"
#include "bt_sink_srv_music_rho.h"
#include "bt_sink_srv_a2dp.h"
#include "bt_sink_srv_avrcp.h"
#include "bt_connection_manager.h"
#include "bt_timer_external.h"
#include "bt_device_manager_internal.h"
#ifdef __BT_AWS_MCE_A2DP_SUPPORT__
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
#include "bt_role_handover.h"
static bt_status_t bt_sink_srv_music_role_handover_service_allowed(const bt_bd_addr_t *addr);
static uint8_t bt_sink_srv_music_role_handover_service_get_length(const bt_bd_addr_t *addr);
static bt_status_t bt_sink_srv_music_role_handover_service_get_data(const bt_bd_addr_t *addr,
    void *data);
static bt_status_t bt_sink_srv_music_role_handover_service_update(bt_role_handover_update_info_t *info);
static void bt_sink_srv_music_role_handover_service_status_callback(const bt_bd_addr_t *addr,
    bt_aws_mce_role_t role, bt_role_handover_event_t event,
    bt_status_t status);
#endif

bool bt_sink_srv_music_role_handover_is_allowed(const bt_bd_addr_t *addr)
{
    bt_sink_srv_music_context_t *ctx = bt_sink_srv_music_get_context();
    bt_sink_srv_music_device_t *sp_dev = ctx->run_dev ? ctx->run_dev:bt_sink_srv_music_get_device(BT_SINK_SRV_MUSIC_DEVICE_SP, NULL);
    bool ret = true;
    uint32_t connect_bit = 0;
    uint32_t sp_flag = 0;
    uint32_t sp_op = 0;
    if(sp_dev) {
        if (sp_dev->flag & BT_SINK_SRV_MUSIC_FLAG_WAIT_AVRCP_CONN_RESULT) {
            ret = false;
        }else if(ctx->run_dev && !(sp_dev->op&BT_SINK_SRV_MUSIC_OP_DRV_PLAY)) {
            ret = false;
        }
        sp_flag = sp_dev->flag;
        sp_op = sp_dev->op;
        connect_bit = sp_dev->conn_bit;
    }
    if(ret) {
        ctx->rho_flag = 1;
    }

    bt_sink_srv_report_id("[music_rho]ret:%d, flag: 0x%08x, op:0x%08x, conn_bit: 0x%08x, sp_dev:0x%08x", 5, ret, sp_flag, sp_op, connect_bit, sp_dev);
    return ret;
}

uint8_t bt_sink_srv_music_role_handover_get_length(const bt_bd_addr_t* addr)
{
    bt_sink_srv_music_context_t *ctx = bt_sink_srv_music_get_context();
    bt_sink_srv_music_device_t *sp_dev = ctx->run_dev ? ctx->run_dev:bt_sink_srv_music_get_device(BT_SINK_SRV_MUSIC_DEVICE_SP, NULL);
    uint8_t ctx_len = sizeof(bt_sink_srv_music_change_context_t);
    if(!sp_dev) {
        ctx_len = 0;
    }
#ifndef SUPPORT_ROLE_HANDOVER_SERVICE
    ctx->rho_flag = 1;
#endif
    bt_sink_srv_report_id("[music_rho]rho_get_length return len is %d", 1, ctx_len);
    return ctx_len;
}

void bt_sink_srv_music_role_handover_get_data(const bt_bd_addr_t* addr, uint8_t *data)
{
    bt_sink_srv_report_id("[music_rho] rho get data", 0);

    bt_sink_srv_music_context_t *ctx = bt_sink_srv_music_get_context();
    bt_sink_srv_music_device_t *sp_dev = ctx->run_dev ? ctx->run_dev:bt_sink_srv_music_get_device(BT_SINK_SRV_MUSIC_DEVICE_SP, NULL);
    bt_sink_srv_music_change_context_t *change_ctx = (bt_sink_srv_music_change_context_t *)data;
    if(sp_dev && change_ctx) {
        change_ctx->conn_bit = sp_dev->conn_bit;
        change_ctx->flag = sp_dev->flag;
        change_ctx->op = sp_dev->op;
        change_ctx->a2dp_status = sp_dev->a2dp_status;
        change_ctx->avrcp_status = sp_dev->avrcp_status;
        change_ctx->last_play_pause_action = sp_dev->last_play_pause_action;
        change_ctx->rho_temp_flag = 0;
        bt_device_manager_db_remote_pnp_info_t device_id;
        if (BT_STATUS_SUCCESS == bt_device_manager_remote_find_pnp_info(sp_dev->dev_addr, &device_id)) {
            bt_sink_srv_memcpy(&change_ctx->device_id, &device_id, sizeof(bt_device_manager_db_remote_pnp_info_t));
        }
        if(sp_dev->handle->flag & AUDIO_SRC_SRV_FLAG_WAITING) {
            BT_SINK_SRV_SET_FLAG(change_ctx->rho_temp_flag, BT_SINK_SRV_MUSIC_RHO_WAITING_LIST_FLAG);
        }
        if(sp_dev->op&BT_SINK_SRV_MUSIC_START_PLAY_ON_NEW_AGENT_FLAG) {
            bt_sink_srv_report_id("[music_rho] BT_SINK_SRV_MUSIC_START_PLAY_ON_NEW_AGENT_FLAG on.", 0);
            BT_SINK_SRV_REMOVE_FLAG(sp_dev->op, BT_SINK_SRV_MUSIC_START_PLAY_ON_NEW_AGENT_FLAG);
            BT_SINK_SRV_SET_FLAG(change_ctx->rho_temp_flag, BT_SINK_SRV_MUSIC_RHO_START_ON_NEW_AGENT);
        }
        bt_sink_srv_music_stored_data_t dev_db;

        if (bt_sink_srv_music_get_a2dp_nvdm_data(&(sp_dev->dev_addr), &dev_db, sizeof(dev_db))) {
            if ((dev_db.music_volume & 0xff00) == BT_SINK_SRV_A2DP_MAGIC_CODE) {
                /* use storge volume value */
                change_ctx->vol = (dev_db.music_volume & 0x00ff);
            } else {
                change_ctx->vol = BT_SINK_SRV_A2DP_INVALID_VOLUME;
            }
        }
        change_ctx->vol_rsp_flag = sp_dev->volume_change_status;
    }
}


//extern audio_src_srv_handle_t *g_aws_mce_a2dp_pse_hd;

void bt_sink_srv_music_role_handover(const bt_bd_addr_t* addr, bt_aws_mce_role_handover_profile_t *profile_data, uint8_t *data)
{
    //bt_aws_mce_role_t role = bt_connection_manager_device_local_info_get_aws_role();
    bt_sink_srv_music_context_t *ctx = bt_sink_srv_music_get_context();
    bt_sink_srv_music_device_t *aws_a2dp_dev = bt_sink_srv_music_get_device(BT_SINK_SRV_MUSIC_DEVICE_AWS,
        (void *)AUDIO_SRC_SRV_PSEUDO_DEVICE_AWS_A2DP);

    int8_t temp_aid = ctx->a2dp_aid;
    ctx->a2dp_aid = ctx->aws_aid;
    ctx->aws_aid = temp_aid;
#ifndef SUPPORT_ROLE_HANDOVER_SERVICE
    ctx->rho_flag = 0;
#endif


    if(profile_data) {
        bt_sink_srv_music_change_context_t *change_ctx = (bt_sink_srv_music_change_context_t *)data;
        bt_sink_srv_assert(aws_a2dp_dev);
        bt_bd_addr_t *sp_addr = (bt_bd_addr_t *)addr;
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
        sp_addr = (bt_bd_addr_t *)bt_gap_get_remote_address(profile_data->gap_handle);
#endif

        if(!change_ctx) {
            aws_a2dp_dev->aws_hd = bt_sink_srv_aws_mce_get_handle(sp_addr);
            aws_a2dp_dev->handle->dev_id = bt_sink_srv_music_convert_btaddr_to_devid(sp_addr);
            bt_sink_srv_memcpy(&(aws_a2dp_dev->dev_addr), sp_addr, sizeof(bt_bd_addr_t));
            bt_sink_srv_report_id("[music_rho] no a2dp&avrcp connection, RHO CLIENT-->AGENT success.", 0);
            return;
        }

        //to alloc sp_aws_dev
        bt_sink_srv_music_device_t *sp_aws_dev = bt_sink_srv_music_get_device(BT_SINK_SRV_MUSIC_DEVICE_UNUSED, (void *)AUDIO_SRC_SRV_PSEUDO_DEVICE_AWS_A2DP);
        bt_sink_srv_assert(sp_aws_dev);
        sp_aws_dev->aws_hd = bt_sink_srv_aws_mce_get_handle(sp_addr);
        bt_sink_srv_memcpy(&(sp_aws_dev->dev_addr), sp_addr, sizeof(bt_bd_addr_t));
        /* Init pse handle */
        sp_aws_dev->handle = bt_sink_srv_a2dp_alloc_pseudo_handle();
        sp_aws_dev->handle->type = AUDIO_SRC_SRV_PSEUDO_DEVICE_AWS_A2DP;
        bt_sink_srv_music_fill_audio_src_callback((audio_src_srv_handle_t *)sp_aws_dev->handle);
        sp_aws_dev->handle->priority = AUDIO_SRC_SRV_PRIORITY_NORMAL;
        //dev->gap_role = bt_sink_srv_cm_get_gap_role(&(dev->dev_addr));
        sp_aws_dev->handle->dev_id = bt_sink_srv_music_convert_btaddr_to_devid(sp_addr);
        BT_SINK_SRV_SET_FLAG(sp_aws_dev->conn_bit, BT_SINK_SRV_MUSIC_AWS_CONN_BIT);
        bt_sink_srv_music_state_machine_handle(sp_aws_dev, BT_SINK_SRV_MUSIC_EVT_READY, NULL);
#if 0
        //to alloc agent_aw_dev
        bt_bd_addr_t *old_agent_addr = bt_connection_manager_device_local_info_get_local_address();
        bt_sink_srv_music_device_t *agent_aws_dev = bt_sink_srv_music_get_device(BT_SINK_SRV_MUSIC_DEVICE_UNUSED, (void *)AUDIO_SRC_SRV_PSEUDO_DEVICE_AWS_A2DP);
        bt_sink_srv_assert(old_agent_addr);
        bt_sink_srv_assert(agent_aws_dev);
        agent_aws_dev->aws_hd = bt_sink_srv_aws_mce_get_handle(old_agent_addr);
        bt_sink_srv_memcpy(&(agent_aws_dev->dev_addr), old_agent_addr, sizeof(bt_bd_addr_t));
        /* Init pse handle */
        agent_aws_dev->handle = bt_sink_srv_a2dp_alloc_pseudo_handle();
        agent_aws_dev->handle->type = AUDIO_SRC_SRV_PSEUDO_DEVICE_AWS_A2DP;
        bt_sink_srv_music_fill_audio_src_callback((audio_src_srv_handle_t *)agent_aws_dev->handle);
        agent_aws_dev->handle->priority = AUDIO_SRC_SRV_PRIORITY_NORMAL;
        //dev->gap_role = bt_sink_srv_cm_get_gap_role(&(dev->dev_addr));
        agent_aws_dev->handle->dev_id = bt_sink_srv_music_convert_btaddr_to_devid(old_agent_addr);
        bt_sink_srv_report_id("[music_rho]sp_aws_dev handle : 0x%08x, agent_aws_dev handle: 0x%08x", 2,
        sp_aws_dev->aws_hd, agent_aws_dev->aws_hd);
#endif
        bt_sink_srv_assert(change_ctx);
        aws_a2dp_dev->handle->type = AUDIO_SRC_SRV_PSEUDO_DEVICE_A2DP;
        if(ctx->run_dev && !(ctx->run_dev->op&BT_SINK_SRV_MUSIC_OP_DRV_PLAY)) {
            BT_SINK_SRV_SET_FLAG(aws_a2dp_dev->flag, BT_SINK_SRV_MUSIC_FLAG_RHO_HAPPEN_DURING_STARTING_PLAY);
            bt_sink_srv_report_id("[music_rho]RHO happen during partner starting play", 0);
        } else {
            aws_a2dp_dev->flag = change_ctx->flag;
            aws_a2dp_dev->op = change_ctx->op;
        }
        aws_a2dp_dev->conn_bit = change_ctx->conn_bit;
        aws_a2dp_dev->volume_change_status = change_ctx->vol_rsp_flag;
        //bt_sink_srv_avrcp_set_vol_rsp_flag(change_ctx->vol_rsp_flag);

        bt_sink_srv_report_id("[music_rho] dev:0x%08x", 1,
            aws_a2dp_dev);
        bt_sink_srv_memcpy(&aws_a2dp_dev->dev_addr, sp_addr, sizeof(bt_bd_addr_t));
        aws_a2dp_dev->a2dp_hd = profile_data->a2dp_handle;
        aws_a2dp_dev->avrcp_hd = profile_data->avrcp_handle;
        aws_a2dp_dev->aws_hd = BT_SINK_SRV_MUSIC_INVALID_HD;
        aws_a2dp_dev->handle->dev_id = bt_sink_srv_music_convert_btaddr_to_devid(sp_addr);
        aws_a2dp_dev->a2dp_status = change_ctx->a2dp_status;
        aws_a2dp_dev->avrcp_status = change_ctx->avrcp_status;
        aws_a2dp_dev->last_play_pause_action = change_ctx->last_play_pause_action;
        bt_status_t ret = bt_device_manager_remote_update_pnp_info((void*)sp_addr, &change_ctx->device_id);
        if(change_ctx->rho_temp_flag&BT_SINK_SRV_MUSIC_RHO_WAITING_LIST_FLAG) {
            audio_src_srv_add_waiting_list(aws_a2dp_dev->handle);
        }
        if(change_ctx->vol != BT_SINK_SRV_A2DP_INVALID_VOLUME) {
            ctx->vol_lev = change_ctx->vol;
            bt_sink_srv_music_set_nvdm_data(&(aws_a2dp_dev->dev_addr),  BT_SINK_SRV_MUSIC_DATA_VOLUME, &ctx->vol_lev);
        }
        if(change_ctx->rho_temp_flag&BT_SINK_SRV_MUSIC_RHO_START_ON_NEW_AGENT) {
            bt_sink_srv_music_state_machine_handle(aws_a2dp_dev, BT_A2DP_START_STREAMING_IND, NULL);
            bt_sink_srv_report_id("[music_rho] start music on new agent", 0);
        }
        if((aws_a2dp_dev->conn_bit&BT_SINK_SRV_MUSIC_A2DP_CONN_BIT) && !(aws_a2dp_dev->conn_bit&BT_SINK_SRV_MUSIC_AVRCP_CONN_BIT)) {
            bt_timer_ext_start(BT_SINK_SRV_AVRCP_CONNECTION_TIMER_ID, (uint32_t)aws_a2dp_dev,
                BT_SINK_SRV_AVRCP_CONNECTION_TIMER_DUR, bt_sink_srv_a2dp_initial_avrcp_timer);
        }
        bt_sink_srv_report_id("[music_rho] RHO CLIENT-->AGENT success, conn_bit:0x%08x, flag:0x%08x, ret:0x%08x, rho_flag:0x%08x", 4, change_ctx->conn_bit, change_ctx->flag,
                                                                                                                                        ret, change_ctx->rho_temp_flag);
    } else {

        bt_sink_srv_music_device_t *sp_dev = bt_sink_srv_music_get_device(BT_SINK_SRV_MUSIC_DEVICE_SP,
            NULL);
        bt_bd_addr_t *sp_addr = (bt_bd_addr_t *)addr;
        bt_sink_srv_assert(aws_a2dp_dev);
        if(!sp_dev) {
            bt_sink_srv_memcpy(&(aws_a2dp_dev->dev_addr), bt_connection_manager_device_local_info_get_local_address(), sizeof(bt_bd_addr_t));
            aws_a2dp_dev->handle->dev_id = bt_sink_srv_music_convert_btaddr_to_devid(&(aws_a2dp_dev->dev_addr));
            aws_a2dp_dev->aws_hd = bt_sink_srv_aws_mce_get_handle(sp_addr);
            bt_sink_srv_report_id("[music_rho] no a2dp&avrcp connection, RHO AGENT-->CLIENT success.", 0);
            return;
        }
        while(aws_a2dp_dev) {
            bt_sink_srv_report_id("[music_rho] to free mce dev, aws_a2dp_dev:0x%08x", 1, aws_a2dp_dev);
            bt_sink_srv_a2dp_free_pseudo_handle(aws_a2dp_dev->handle);
            aws_a2dp_dev->handle = NULL;
            bt_sink_srv_music_reset_device(aws_a2dp_dev);
            aws_a2dp_dev = bt_sink_srv_music_get_device(BT_SINK_SRV_MUSIC_DEVICE_AWS_BY_LINK, (void *)AUDIO_SRC_SRV_PSEUDO_DEVICE_AWS_A2DP);
        }

        bt_sink_srv_assert(sp_dev);
        bt_timer_ext_stop(BT_SINK_SRV_AVRCP_CONNECTION_TIMER_ID);
        sp_dev->handle->type = AUDIO_SRC_SRV_PSEUDO_DEVICE_AWS_A2DP;
        sp_dev->conn_bit = BT_SINK_SRV_MUSIC_AWS_CONN_BIT;
        sp_dev->a2dp_hd = BT_SINK_SRV_MUSIC_INVALID_HD;
        sp_dev->avrcp_hd = BT_SINK_SRV_MUSIC_INVALID_HD;
        sp_dev->volume_change_status = false;
        //to du replace handle
        bt_sink_srv_memcpy(&sp_dev->dev_addr, bt_connection_manager_device_local_info_get_local_address(), sizeof(bt_bd_addr_t));
        sp_dev->handle->dev_id = bt_sink_srv_music_convert_btaddr_to_devid(&sp_dev->dev_addr);
        uint8_t *addr_point = (uint8_t *)(&sp_dev->dev_addr);
        bt_sink_srv_report_id("[music_rho]addr:0x%02x:0x%02x:0x%02x:0x%02x:0x%02x:0x%02x", 6,
            addr_point[0],addr_point[1], addr_point[2], addr_point[3], addr_point[4],
            addr_point[5]);
        sp_dev->aws_hd = bt_sink_srv_aws_mce_get_handle(sp_addr);
        if(sp_dev->handle->flag & AUDIO_SRC_SRV_FLAG_WAITING) {
            bt_sink_srv_report_id("[music_rho]RHO happen, pseduo device is in waiting list", 0);
            audio_src_srv_del_waiting_list(sp_dev->handle);
        }

        bt_sink_srv_report_id("[music_rho] aws_hd:0x%08x", 1, sp_dev->aws_hd);
        bt_sink_srv_report_id("[music_rho] RHO AGENT-->CLIENT success", 0);
    }
}


/*
bt_status_t
bt_sink_srv_music_role_handover_service_register_callbacks(bt_role_handover_module_type_t
type, bt_role_handover_callbacks_t callbacks)
{

}
*/
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
bt_status_t bt_sink_srv_music_role_handover_service_init(void)
{

    bt_role_handover_callbacks_t handover_callbacks = {
        bt_sink_srv_music_role_handover_service_allowed,
        bt_sink_srv_music_role_handover_service_get_length,
        bt_sink_srv_music_role_handover_service_get_data,
        bt_sink_srv_music_role_handover_service_update,
        bt_sink_srv_music_role_handover_service_status_callback,
    };

    bt_status_t ret = bt_role_handover_register_callbacks(BT_ROLE_HANDOVER_MODULE_SINK_MUSIC, &handover_callbacks);
    bt_sink_srv_report_id("[music_rho]init ret:0x%08x", 1, ret);
    return ret;
}

static bt_status_t bt_sink_srv_music_role_handover_service_allowed(const bt_bd_addr_t *addr)
{
    bool result = bt_sink_srv_music_role_handover_is_allowed(addr);

    bt_timer_ext_t* avrcp_timer = bt_timer_ext_find(BT_SINK_SRV_AVRCP_CONNECTION_TIMER_ID);
    if(avrcp_timer) {
        return BT_STATUS_PENDING;
    }
    return result ? BT_STATUS_SUCCESS : BT_STATUS_FAIL;
}

static uint8_t bt_sink_srv_music_role_handover_service_get_length(const bt_bd_addr_t *addr)
{
    return bt_sink_srv_music_role_handover_get_length(addr);
}

static bt_status_t bt_sink_srv_music_role_handover_service_get_data(const bt_bd_addr_t
*addr, void *data)
{
    bt_sink_srv_music_role_handover_get_data(addr, data);
    return BT_STATUS_SUCCESS;
}

static bt_status_t bt_sink_srv_music_role_handover_service_update(bt_role_handover_update_info_t *info)
{
    bt_sink_srv_music_role_handover((const bt_bd_addr_t *)(info->addr),
        (bt_aws_mce_role_handover_profile_t *)(info->profile_info), (uint8_t *)(info->data));
    return BT_STATUS_SUCCESS;
}

static void bt_sink_srv_music_role_handover_service_status_callback(const bt_bd_addr_t
    *addr, bt_aws_mce_role_t role, bt_role_handover_event_t event, bt_status_t status)
{
    bt_sink_srv_report_id("[music_rho]role:0x%02x, event:0x%02x, status:0x%08x", 3, role, event, status);
    bt_sink_srv_music_context_t *ctx = bt_sink_srv_music_get_context();
    switch (event) {
        case BT_ROLE_HANDOVER_START_IND:
            ctx->rho_flag = 1;
            break;

        case BT_ROLE_HANDOVER_PREPARE_REQ_IND:{
            //stop timer
            bt_timer_ext_t *avrcp_timer = bt_timer_ext_find(BT_SINK_SRV_AVRCP_CONNECTION_TIMER_ID);
            if(avrcp_timer) {
                bt_timer_ext_stop(BT_SINK_SRV_AVRCP_CONNECTION_TIMER_ID);
            }
            //notify
            bt_role_handover_reply_prepare_request(BT_ROLE_HANDOVER_MODULE_SINK_MUSIC);
            break;
        }
        case BT_ROLE_HANDOVER_COMPLETE_IND:
            ctx->rho_flag = 0;
            break;
        default:
            break;
    }
}
#endif
#endif
