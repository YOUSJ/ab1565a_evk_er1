/* Copyright Statement:
 *
 * (C) 2005-2016  MediaTek Inc. All rights reserved.
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. ("MediaTek") and/or its licensors.
 * Without the prior written permission of MediaTek and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) MediaTek Software
 * if you have agreed to and been bound by the applicable license agreement with
 * MediaTek ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of MediaTek Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT MEDIATEK SOFTWARE RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 */

#include "bt_avm.h"
#include "avm_direct.h"
#include "bt_callback_manager.h"
#include "bt_sink_srv_ami.h"
#include "bt_sink_srv_utils.h"

#include "bt_connection_manager_internal.h"
#include "bt_sink_srv_call.h"
#include "bt_sink_srv_a2dp.h"
#include "bt_sink_srv_avrcp.h"
#include "bt_sink_srv_pbapc.h"
#include "bt_sink_srv_hf.h"
#ifdef MTK_AWS_MCE_ENABLE
#include "bt_aws_mce_report.h"
#endif /*MTK_AWS_MCE_ENABLE*/

#ifdef MTK_AWS_MCE_ENABLE
extern bt_status_t bt_sink_srv_aws_mce_common_callback(bt_msg_type_t msg, bt_status_t status, void *buffer);
extern void bt_sink_srv_aws_mce_a2dp_app_report_callback(bt_aws_mce_report_info_t *para);
extern void bt_sink_srv_aws_mce_call_app_report_callback(bt_aws_mce_report_info_t *para);
#endif /*MTK_AWS_MCE_ENABLE*/

bt_sink_feature_config_t bt_sink_srv_features_config;

void bt_sink_srv_config_features(bt_sink_feature_config_t *features)
{
    bt_sink_srv_memcpy(&bt_sink_srv_features_config, features, sizeof(bt_sink_feature_config_t));
}

const bt_sink_feature_config_t *bt_sink_srv_get_features_config(void)
{
    return &bt_sink_srv_features_config;
}

bt_status_t bt_sink_srv_common_callback(bt_msg_type_t msg, bt_status_t status, void *buffer)
{
    bt_status_t result = BT_STATUS_SUCCESS;
    uint32_t moduel = msg & 0xFF000000;
    switch (moduel) {
    #ifndef MTK_BT_CM_SUPPORT
        case BT_MODULE_SYSTEM:
            result = bt_sink_srv_cm_system_callback(msg, status, buffer);
            break;
    #endif
        case BT_MODULE_GAP:
        case BT_MODULE_SDP:
    #ifndef MTK_BT_CM_SUPPORT
            result = bt_sink_srv_cm_gap_callback(msg, status, buffer);
    #endif
            result = bt_sink_srv_hf_gap_callback(msg, status, buffer);
            result = bt_sink_srv_a2dp_common_callback(msg, status, buffer);
            break;

        case BT_MODULE_HFP:
        case BT_MODULE_HSP:
            result = bt_sink_srv_call_common_callback(msg, status, buffer);
            break;

        case BT_MODULE_A2DP:
            result = bt_sink_srv_a2dp_common_callback(msg, status, buffer);
         break;
         
        case BT_MODULE_AVM:
            result = bt_sink_srv_a2dp_common_callback(msg, status, buffer);
    #ifdef MTK_AWS_MCE_ENABLE
            result = bt_sink_srv_aws_mce_common_callback(msg, status, buffer);
    #endif
            break;

        case BT_MODULE_AVRCP:
            result = bt_sink_srv_avrcp_common_callback(msg, status, buffer);
            break;

        case BT_MODULE_PBAPC:
            result = bt_sink_srv_pbapc_common_callback(msg, status, buffer);
            break;
        
    #ifdef MTK_AWS_MCE_ENABLE
        case BT_MODULE_AWS_MCE:
            result = bt_sink_srv_aws_mce_common_callback(msg, status, buffer);
            break;
    #endif

        case BT_MODULE_MM:
            result = bt_sink_srv_a2dp_common_callback(msg, status, buffer);
            break;
        default:
            bt_sink_srv_report_id("[SINK][COMMON]Unknown Bluetooth MSG:0x%x, status:0x%x", 2, msg, status);
            break;
    }
    return result;
}


bt_status_t bt_sink_srv_set_clock_offset_ptr_to_dsp(const bt_bd_addr_t *address)
{
    bt_sink_srv_assert(address && "Err: address NULL");
    uint32_t handle = bt_sink_srv_cm_get_gap_handle((bt_bd_addr_t *)address);
    if (handle == 0) {
        bt_sink_srv_report_id("[SINK][COMMON]GAP handle is NULL.", 0);
        //bt_sink_srv_assert(0 && "Err: handle NULL");
        return BT_SINK_SRV_STATUS_INVALID_PARAM;
    }

    const void *clk_offset_buf = bt_avm_get_clock_offset_address(handle);
    if (clk_offset_buf == NULL) {
        bt_sink_srv_report_id("[SINK][COMMON]Get clock offset ptr buff is NULL.", 0);
        //bt_sink_srv_assert(0 && "Err: clock_buff ptr NULL");
        return BT_SINK_SRV_STATUS_FAIL;
    }

    bt_sink_srv_ami_set_bt_inf_address((bt_sink_srv_am_bt_audio_param_t)clk_offset_buf);
    bt_sink_srv_report_id("[SINK][COMMON]Set clock offset ptr, handle:0x%08x, buf ptr:0x%08x", 2, 
                        handle, clk_offset_buf);
    return BT_STATUS_SUCCESS;
}


#ifdef MTK_AWS_MCE_ENABLE
bt_status_t bt_sink_srv_bt_clock_addition(bt_clock_t *target_clk, bt_clock_t *base_clk, uint32_t duration)
{
    
    bt_bd_addr_t *device_p = bt_sink_srv_cm_get_aws_connected_device();
    bt_status_t ret =BT_STATUS_FAIL;
    if(device_p && target_clk) {
        bt_clock_t dur_clock;
        dur_clock.nclk = (duration/1250)<<2;
        dur_clock.nclk_intra = duration%1250;
        if(!base_clk) {
            uint32_t connection_handle = bt_sink_srv_cm_get_gap_handle(device_p);
            bt_clock_t cur_clock = {0};
            bt_get_bt_clock(connection_handle, &(cur_clock));
#ifdef MTK_INITIAL_SYNC_BY_SAMPLE_INDEX_SUPPORT
            avm_direct_bt_clock_add_duration(target_clk, &cur_clock, &dur_clock, BT_ROLE_MASTER);
#else
            avm_direct_bt_clock_add_duration(target_clk, &cur_clock, &dur_clock, BT_ROLE_SLAVE);
#endif
            bt_sink_srv_report_id("[Sink] cur_nclk:0x%08x, cur_intra:0x%08x, duration:%d", 3, 
                cur_clock.nclk, cur_clock.nclk_intra, duration);
            bt_sink_srv_report_id("[Sink] target_nclk:0x%08x, target_intra:0x%08x", 2, 
                target_clk->nclk, target_clk->nclk_intra);
        } else {
#ifdef MTK_INITIAL_SYNC_BY_SAMPLE_INDEX_SUPPORT
            avm_direct_bt_clock_add_duration(target_clk, base_clk, &dur_clock, BT_ROLE_MASTER);
#else
            avm_direct_bt_clock_add_duration(target_clk, base_clk, &dur_clock, BT_ROLE_SLAVE);
#endif
        }
        ret = BT_STATUS_SUCCESS;
    }

    return ret;
}

bt_status_t bt_sink_srv_bt_clock_get_duration(bt_clock_t *target_clk, bt_clock_t 
    *base_clk, int32_t *duration)
{
    bt_bd_addr_t *device_p = bt_sink_srv_cm_get_aws_connected_device();
    bt_status_t ret = BT_STATUS_FAIL;
    if(device_p && target_clk && duration) {
        if(!base_clk) {
            uint32_t connection_handle = bt_sink_srv_cm_get_gap_handle(device_p);
            bt_clock_t cur_clock = {0};
            bt_get_bt_clock(connection_handle, &(cur_clock));
            *duration = ((int)(target_clk->nclk - cur_clock.nclk)*625>>1) + 
                (int16_t)(target_clk->nclk_intra - cur_clock.nclk_intra);
            bt_sink_srv_report_id("[Sink] cur_nclk:0x%08x, cur_intra:0x%08x, duration:%d", 3, 
                cur_clock.nclk, cur_clock.nclk_intra, *duration);
            bt_sink_srv_report_id("[Sink] target_nclk:0x%08x, target_intra:0x%08x", 2, 
                target_clk->nclk, target_clk->nclk_intra);
        } else {
            *duration = ((int)(target_clk->nclk - base_clk->nclk)*625>>1) + 
                (int16_t)(target_clk->nclk_intra - base_clk->nclk_intra);
        }
        ret = BT_STATUS_SUCCESS;
    }

    return ret;
}
#endif /*MTK_AWS_MCE_ENABLE*/

void bt_sink_srv_register_callback_init(void)
{
    bt_callback_manager_register_callback(bt_callback_type_app_event,
                                      (uint32_t)(MODULE_MASK_GAP | MODULE_MASK_SYSTEM | MODULE_MASK_HFP |
                                              MODULE_MASK_HSP | MODULE_MASK_AVRCP | MODULE_MASK_A2DP |
                                              MODULE_MASK_PBAPC | MODULE_MASK_SPP | MODULE_MASK_AWS_MCE | 
                                              MODULE_MASK_MM | MODULE_MASK_AVM | MODULE_MASK_SDP),
                                      (void *)bt_sink_srv_common_callback);
    bt_callback_manager_register_callback(bt_callback_type_hfp_get_init_params,
                                      0,
                                      (void *)bt_sink_srv_hf_get_init_params);
    bt_callback_manager_register_callback(bt_callback_type_a2dp_get_init_params,
                                      0,
                                      (void *)bt_sink_srv_a2dp_get_init_params);
}

#ifdef MTK_AWS_MCE_ENABLE
void bt_sink_srv_register_aws_mce_report_callback(void)
{
    bt_aws_mce_report_register_callback(BT_AWS_MCE_REPORT_MODULE_SINK_MUSIC, bt_sink_srv_aws_mce_a2dp_app_report_callback);
    bt_aws_mce_report_register_callback(BT_AWS_MCE_REPORT_MODULE_SINK_CALL, bt_sink_srv_aws_mce_call_app_report_callback);
}
#endif /*MTK_AWS_MCE_ENABLE*/

uint32_t bt_sink_srv_get_volume(bt_bd_addr_t *bd_addr, bt_sink_srv_volume_type_t type)
{
    uint32_t volume = 0xffffffff;
    if(type == BT_SINK_SRV_VOLUME_HFP) {
        bt_sink_srv_hf_get_speaker_volume(bd_addr, &volume);
    } else if(type == BT_SINK_SRV_VOLUME_A2DP) {
        bt_sink_srv_a2dp_get_volume(bd_addr, &volume);
    }
    
    return volume;
}

