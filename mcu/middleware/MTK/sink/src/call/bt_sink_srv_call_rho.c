/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "bt_gap.h"
#include "bt_sink_srv_utils.h"
#include "bt_sink_srv_call.h"
#include "bt_sink_srv_hf.h"
#include "bt_sink_srv_aws_mce_call.h"
#include "bt_connection_manager_internal.h"
#include "bt_sink_srv_call_pseudo_dev.h"

#ifdef MTK_BT_HSP_ENABLE
#include "bt_sink_srv_hsp.h"
#endif /*MTK_BT_HSP_ENABLE*/

#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
#include "bt_role_handover.h"
#endif /*SUPPORT_ROLE_HANDOVER_SERVICE*/

typedef struct {
    /*HFP used*/
    bt_hfp_ag_feature_t ag_featues;
    bt_hfp_ag_hold_feature_t ag_chld_feature;
    bt_sink_srv_hf_call_state_t call_state;
    bt_sink_srv_hf_flag_t flag;
    /*HSP used*/
    bt_sink_srv_sco_connection_state_t sco_state;
} bt_sink_srv_call_rho_content_t;

extern void bt_sink_srv_hf_set_hsp_flag(bool enable);
extern void bt_sink_srv_aws_mce_call_pseudo_dev_callback(bt_sink_srv_call_pseudo_dev_event_t event_id, void *device, void *params);
extern void bt_sink_srv_hf_set_highlight_device(bt_sink_srv_hf_context_t *device);
extern void bt_sink_srv_hf_reset_highlight_device(void);
extern void bt_sink_srv_hf_pseudo_dev_callback(bt_sink_srv_call_pseudo_dev_event_t event_id, void *device, void *params);

#ifdef MTK_BT_HSP_ENABLE
extern void bt_sink_srv_hsp_pseudo_dev_callback(bt_sink_srv_call_pseudo_dev_event_t event_id, void *device, void *params);
extern void bt_sink_srv_hsp_set_highlight_device(bt_sink_srv_hsp_context_t *device);
#endif /*MTK_BT_HSP_ENABLE*/
bt_sink_srv_call_rho_content_t g_bt_sink_srv_call_rho_content = {0};

#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
static bt_status_t bt_sink_srv_call_role_handover_is_allowed(const bt_bd_addr_t* addr);
static uint8_t bt_sink_srv_call_role_handover_get_length(const bt_bd_addr_t* addr);
static bt_status_t bt_sink_srv_call_role_handover_get_data(const bt_bd_addr_t* addr, void *data);
static bt_status_t bt_sink_srv_call_role_handover_update(bt_role_handover_update_info_t *info);
static void bt_sink_srv_call_role_handover_status_callback(const bt_bd_addr_t *addr, bt_aws_mce_role_t role, bt_role_handover_event_t event, bt_status_t status);

bt_role_handover_callbacks_t bt_sink_srv_call_call_role_cb = {
    .allowed_cb = bt_sink_srv_call_role_handover_is_allowed,
    .get_len_cb = bt_sink_srv_call_role_handover_get_length,
    .get_data_cb = bt_sink_srv_call_role_handover_get_data,
    .update_cb = bt_sink_srv_call_role_handover_update,
    .status_cb = bt_sink_srv_call_role_handover_status_callback
};

void bt_sink_srv_call_role_handover_init(void)
{
    //bt_role_handover_init();
    bt_role_handover_register_callbacks(
        BT_ROLE_HANDOVER_MODULE_SINK_CALL, &bt_sink_srv_call_call_role_cb);
}

bt_status_t bt_sink_srv_call_role_handover_is_allowed(const bt_bd_addr_t* addr)
{
    bt_status_t result = BT_STATUS_SUCCESS;
    if (bt_sink_srv_call_psd_is_all_in_steady_state()) {
        result = BT_STATUS_SUCCESS;
    } else {
        result = BT_STATUS_FAIL;
    }
    bt_sink_srv_report_id("[CALL][AWS_MCE][RHO]is allowed result:0x%x", 1,result);
    return result;
}

uint8_t bt_sink_srv_call_role_handover_get_length(const bt_bd_addr_t* addr)
{

    uint8_t len = 0xFF;
    bt_sink_srv_hf_context_t *hfp_context = NULL;
    hfp_context = (bt_sink_srv_hf_context_t *)bt_sink_srv_hf_get_context_by_address((bt_bd_addr_t*)addr);
#ifdef MTK_BT_HSP_ENABLE
    bt_sink_srv_hsp_context_t *hsp_context = NULL;
    hsp_context = (bt_sink_srv_hsp_context_t *)bt_sink_srv_hsp_get_context_by_address((bt_bd_addr_t*)addr);
#endif /*MTK_BT_HSP_ENABLE*/

    if ((hfp_context && bt_sink_srv_call_psd_is_ready(hfp_context->device))
        #ifdef MTK_BT_HSP_ENABLE
        || (hsp_context && bt_sink_srv_call_psd_is_ready(hsp_context->device))
        #endif /*MTK_BT_HSP_ENABLE*/
        ) {
        len = (uint8_t)(sizeof(bt_sink_srv_call_rho_content_t)&0x000000FF);
    } else {
        len = 0;
    }

    bt_sink_srv_assert(len < 255);
    bt_sink_srv_report_id("[CALL][AWS_MCE][RHO]len:%d", 1,len);
    return len;
}

bt_status_t bt_sink_srv_call_role_handover_get_data(const bt_bd_addr_t* addr, void *data)
{
    bt_sink_srv_hf_context_t *hf_context = bt_sink_srv_hf_get_context_by_address((bt_bd_addr_t* )addr);
#ifdef MTK_BT_HSP_ENABLE
    bt_sink_srv_hsp_context_t *hs_context = bt_sink_srv_hsp_get_context_by_address((bt_bd_addr_t* )addr);
#endif /*MTK_BT_HSP_ENABLE*/

    if (hf_context) {
        g_bt_sink_srv_call_rho_content.ag_featues = hf_context->link.ag_featues;
        g_bt_sink_srv_call_rho_content.ag_chld_feature = hf_context->link.ag_chld_feature;
        g_bt_sink_srv_call_rho_content.call_state = hf_context->link.call_state;
        g_bt_sink_srv_call_rho_content.flag = hf_context->link.flag;
    }
#ifdef MTK_BT_HSP_ENABLE
    if (hs_context) {
        g_bt_sink_srv_call_rho_content.sco_state = hs_context->sco_state;
    }
#endif /*MTK_BT_HSP_ENABLE*/
    bt_sink_srv_memcpy(data, (void *)&g_bt_sink_srv_call_rho_content, sizeof(bt_sink_srv_call_rho_content_t));
    return BT_STATUS_SUCCESS;
}

bt_status_t bt_sink_srv_call_role_handover_update(bt_role_handover_update_info_t *info)
{
    bt_sink_srv_assert(info);
    bt_sink_srv_report_id("[CALL][AWS_MCE][RHO]update, role:0x%x, profile info:0x%x, call data:0x%x, len:%d", 4,
                        info->role, info->profile_info, info->data, info->length);
    if (info->role == BT_AWS_MCE_ROLE_PARTNER) {
        //Previous Partner, create hfp context and destory aws call context.
        bt_sink_srv_report_id("[CALL][AWS_MCE][RHO]SUCCESS, Partner->Agent.", 0);

        bt_sink_srv_assert(info->profile_info);
        bt_sink_srv_report_id("[CALL][AWS_MCE][RHO]gap handle:0x%x,hfp handle:0x%x,hsp handle:0x%x", 3,
                info->profile_info->gap_handle, info->profile_info->hfp_handle, info->profile_info->hsp_handle);
        bt_bd_addr_t* sp_addr = (bt_bd_addr_t*)bt_gap_get_remote_address(info->profile_info->gap_handle);
        bt_sink_srv_assert(sp_addr && "SP address is null!");
        //bt_sink_srv_aws_mce_call_context_t *aws_call_cntx = (bt_sink_srv_aws_mce_call_context_t*)
                    //bt_sink_srv_aws_mce_get_module_info(sp_addr, BT_SINK_SRV_AWS_MCE_MODULE_CALL);
        bt_sink_srv_aws_mce_call_context_t *aws_call_cntx = bt_sink_srv_aws_mce_call_get_context_by_address((const bt_bd_addr_t *)sp_addr);
        bt_sink_srv_report_id("[CALL][AWS_MCE][RHO]Prev call info:%d,%d,%d,%d", 4,
                aws_call_cntx->call_info.call_state, aws_call_cntx->call_info.sco_state,
                aws_call_cntx->call_info.volume, aws_call_cntx->call_info.is_ring);
        bt_sink_srv_assert(aws_call_cntx);

        //1. Update hfp context
        if (info->profile_info->hfp_handle) {
                    bt_sink_srv_call_rho_content_t* call_rho_content = (bt_sink_srv_call_rho_content_t*)info->data;
            bt_sink_srv_hf_context_t *hf_context = bt_sink_srv_hf_alloc_free_context(sp_addr);
            bt_sink_srv_assert(hf_context);
            //1.1. Update device.
            hf_context->device = aws_call_cntx->device;
            bt_sink_srv_call_psd_set_device_id(hf_context->device, sp_addr);
            bt_sink_srv_call_psd_reset_user_callback(hf_context->device, bt_sink_srv_hf_pseudo_dev_callback);

            //1.2. update link context.
            hf_context->is_used = true;
            hf_context->link.handle = info->profile_info->hfp_handle; //hfp handle, will get from profile data
            hf_context->link.ag_featues = call_rho_content->ag_featues;
            hf_context->link.ag_chld_feature = call_rho_content->ag_chld_feature;
            hf_context->link.call_state = call_rho_content->call_state;
            hf_context->link.flag = call_rho_content->flag;
            bt_sink_srv_memcpy(&hf_context->link.address, sp_addr, sizeof(bt_bd_addr_t));
            bt_sink_srv_hf_set_highlight_device(hf_context);

            //1.3. Store volume to the nvdm.
            bt_sink_srv_hf_stored_data_t stored_data;
            stored_data.speaker_volume = (uint8_t)bt_sink_srv_call_psd_get_speaker_volume(hf_context->device) | BT_SINK_SRV_HF_VOLUME_MASK;
            bt_sink_srv_hf_set_nvdm_data(sp_addr, &stored_data, sizeof(stored_data));
    #ifdef MTK_BT_HSP_ENABLE
        } else if (info->profile_info->hsp_handle) {
            bt_sink_srv_call_rho_content_t* call_rho_content = (bt_sink_srv_call_rho_content_t*)info->data;
        //2. Update hsp context
            bt_sink_srv_hsp_context_t *hs_context = bt_sink_srv_hsp_alloc_free_context(sp_addr);
            bt_sink_srv_assert(hs_context);
            //1.1. Update device.
            hs_context->device = aws_call_cntx->device;
            bt_sink_srv_call_psd_set_device_id(hs_context->device, sp_addr);
            bt_sink_srv_call_psd_reset_user_callback(hs_context->device, bt_sink_srv_hsp_pseudo_dev_callback);

            //1.2. update link context.
            hs_context->is_used = true;
            hs_context->handle = info->profile_info->hsp_handle; //hsp handle, will get from profile data
            hs_context->sco_state = call_rho_content->sco_state;
            bt_sink_srv_memcpy(&hs_context->address, sp_addr, sizeof(bt_bd_addr_t));
            bt_sink_srv_hsp_set_highlight_device(hs_context);

            //1.3. Store volume to the nvdm.
            bt_sink_srv_hf_stored_data_t stored_data;
            stored_data.speaker_volume = (uint8_t)bt_sink_srv_call_psd_get_speaker_volume(hs_context->device) | BT_SINK_SRV_HF_VOLUME_MASK;
            bt_sink_srv_hf_set_nvdm_data(sp_addr, &stored_data, sizeof(stored_data));
    #endif /*MTK_BT_HSP_ENABLE*/

        } else {
            // no hfp&hsp connection, need delete aws psd device.
            bt_sink_srv_report_id("[CALL][AWS_MCE][RHO]Partner, no HSP&HFP connection.", 0);
            bt_sink_srv_assert(info->data == NULL);
            uint32_t aws_handle = aws_call_cntx->aws_handle;
            bt_sink_srv_call_psd_state_event_notify(aws_call_cntx->device, BT_SINK_SRV_CALL_EVENT_LINK_DISCONNECTED, NULL);
            aws_call_cntx->aws_handle = aws_handle;
        }
        //5. Delele aws call pseudo context.
        aws_call_cntx->device = NULL;
    } else if (info->role == BT_AWS_MCE_ROLE_AGENT) {
        bt_sink_srv_report_id("[CALL][AWS_MCE][RHO]SUCCESS, Agent->Partner.", 0);
        //Previous agent, create aws call context and destory hfp context.
        //bt_sink_srv_aws_mce_call_context_t *aws_call_cntx = (bt_sink_srv_aws_mce_call_context_t*)
                    //bt_sink_srv_aws_mce_get_module_info((bt_bd_addr_t *)info->addr, BT_SINK_SRV_AWS_MCE_MODULE_CALL);
        bt_sink_srv_aws_mce_call_context_t *aws_call_cntx = bt_sink_srv_aws_mce_call_get_context_by_address(info->addr);
        bt_sink_srv_assert(aws_call_cntx);
        bt_sink_srv_report_id("[CALL][AWS_MCE][RHO]Prev call info:%d,%d,%d,%d", 4,
                aws_call_cntx->call_info.call_state, aws_call_cntx->call_info.sco_state,
                aws_call_cntx->call_info.volume, aws_call_cntx->call_info.is_ring);
        bt_sink_srv_hf_context_t *hf_context = bt_sink_srv_hf_get_context_by_address((bt_bd_addr_t* )info->addr);
        //bt_sink_srv_assert(hf_context);
    #ifdef MTK_BT_HSP_ENABLE
        bt_sink_srv_hsp_context_t *hs_context = bt_sink_srv_hsp_get_context_by_address((bt_bd_addr_t* )info->addr);
        //bt_sink_srv_assert(hs_context);
        #ifdef MTK_BT_TIMER_EXTERNAL_ENABLE
        bt_timer_ext_t* timer_ext = bt_timer_ext_find(BT_SINK_SRV_TIMER_ID_HS_WAIT_RING_IND);
        if (timer_ext) {
            bt_sink_srv_report_id("[CALL][AWS_MCE][RHO]Stop hsp timer when become patner.", 0);
            bt_timer_ext_stop(BT_SINK_SRV_TIMER_ID_HS_WAIT_RING_IND);
        }
        #endif/*MTK_BT_TIMER_EXTERNAL_ENABLE*/
    #endif/*MTK_BT_HSP_ENABLE*/

        //1. Update aws call context.
        //only hfp has call state, so we can get call state from hfp always even if hfp is not connected.
        if (hf_context) {
            aws_call_cntx->call_info.call_state = bt_sink_srv_aws_mce_call_transfer_hf_call_state(hf_context->link.call_state);
        } else {
            aws_call_cntx->call_info.call_state = BT_SINK_SRV_AWS_MCE_CALL_STATE_IDLE;
        }

        void* current_device = NULL;
        if(hf_context && hf_context->device) {
            current_device = hf_context->device;
    #ifdef MTK_BT_HSP_ENABLE
        } else if (hs_context && hs_context->device) {
            current_device = hs_context->device;
    #endif /*MTK_BT_HSP_ENABLE*/
        } else {
            bt_sink_srv_report_id("[CALL][AWS_MCE][RHO]Agent, no HSP&HFP connection.", 0);
        }

        if (current_device) {
            bt_bd_addr_t *old_agent_addr = bt_connection_manager_device_local_info_get_local_address();
            aws_call_cntx->call_info.volume = (uint8_t)bt_sink_srv_call_psd_get_speaker_volume(current_device);
            aws_call_cntx->device = current_device;
            bt_sink_srv_call_psd_set_device_id(aws_call_cntx->device, old_agent_addr);
            bt_sink_srv_call_psd_reset_user_callback(aws_call_cntx->device, bt_sink_srv_aws_mce_call_pseudo_dev_callback);

            bt_sink_srv_report_id("[CALL][AWS_MCE][RHO]New call info:%d,%d,%d,%d", 4,

                aws_call_cntx->call_info.call_state, aws_call_cntx->call_info.sco_state,
                aws_call_cntx->call_info.volume, aws_call_cntx->call_info.is_ring);
            } else {
                //RHO case, if hfp or hsp is not connected, aws call context need alloc a audio_src.
                bt_bd_addr_t* agent_addr = bt_connection_manager_device_local_info_get_local_address();
                bt_sink_srv_assert(agent_addr);
                aws_call_cntx->device = bt_sink_srv_call_psd_alloc_device(agent_addr, bt_sink_srv_aws_mce_call_pseudo_dev_callback);
                bt_sink_srv_assert(aws_call_cntx->device);
                bt_sink_srv_call_psd_state_event_notify(aws_call_cntx->device, BT_SINK_SRV_CALL_EVENT_CONNECT_LINK_REQ_IND, NULL);
                bt_sink_srv_call_psd_state_event_notify(aws_call_cntx->device, BT_SINK_SRV_CALL_EVENT_LINK_CONNECTED, NULL);
                bt_sink_srv_call_psd_set_speaker_volume(aws_call_cntx->device, (bt_sink_srv_call_audio_volume_t)aws_call_cntx->call_info.volume);
            }

         //2. Delele hfp context.
         if (hf_context) {
            bt_sink_srv_memset((void*)hf_context, 0x00, sizeof(bt_sink_srv_hf_context_t));
         }
         bt_sink_srv_hf_reset_highlight_device();

     #ifdef MTK_BT_HSP_ENABLE
        //3. Delele hsp context.
        if (hs_context) {
            bt_sink_srv_memset((void*)hs_context, 0x00, sizeof(bt_sink_srv_hsp_context_t));
        }
        bt_sink_srv_hsp_set_highlight_device(NULL);
     #endif
    }else {
        bt_sink_srv_assert(0 && "Wrong AWS role!");
    }

    return BT_STATUS_SUCCESS;
}

void bt_sink_srv_call_role_handover_status_callback(
    const bt_bd_addr_t *addr, bt_aws_mce_role_t role,
    bt_role_handover_event_t event, bt_status_t status)
{
    bt_sink_srv_report_id("[CALL][AWS_MCE][RHO]status callback, role:0x%x, event:0x%x, status:0x%x", 3,
                         role, event, status);
    switch (event) {
        case BT_ROLE_HANDOVER_START_IND:
        {

        }
        break;

        case BT_ROLE_HANDOVER_PREPARE_REQ_IND:
        {

        }
        break;

        case BT_ROLE_HANDOVER_COMPLETE_IND:
        {

        }
        break;

        default:
        break;
    }
    return;
}
#endif /*SUPPORT_ROLE_HANDOVER_SERVICE*/
