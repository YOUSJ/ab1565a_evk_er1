ifeq ($(MTK_BT_DUO_ENABLE), y)
ifeq ($(MTK_BT_A2DP_VENDOR_ENABLE), y)
# Check if vendor codec exist in dsp side
VENDOR_LIB = $(strip $(SOURCE_DIR))/../dsp/prebuilt/middleware/third_party/dspalg/vendor_decoder/vend.flag
ifneq ($(VENDOR_LIB), $(wildcard $(VENDOR_LIB)))
$(error If BT vendor codec doesn't exist in dsp side, user can't enable MTK_BT_A2DP_VENDOR_ENABLE)
endif

CFLAGS   += -DMTK_BT_A2DP_VENDOR_CODEC_SUPPORT
endif

ifeq ($(MTK_INITIAL_SYNC_BY_SAMPLE_INDEX_SUPPORT), y)
CFLAGS += -DMTK_INITIAL_SYNC_BY_SAMPLE_INDEX_SUPPORT
endif

# BT sink source files
BT_SINK_SRV_SRC = middleware/MTK/sink/src
BT_SINK_SRV_FILES = $(BT_SINK_SRV_SRC)/bt_sink_srv.c \
                    $(BT_SINK_SRV_SRC)/bt_sink_srv_atci_cmd.c \
                    $(BT_SINK_SRV_SRC)/bt_sink_srv_common.c \
                    $(BT_SINK_SRV_SRC)/bt_sink_srv_state_notify.c \
                    $(BT_SINK_SRV_SRC)/bt_sink_srv_utils.c \

# Sink call related
BT_SINK_SRV_CALL_SRC = middleware/MTK/sink/src/call
BT_SINK_SRV_FILES += $(BT_SINK_SRV_CALL_SRC)/bt_sink_srv_call_audio.c \
                     $(BT_SINK_SRV_CALL_SRC)/bt_sink_srv_call.c \
                     $(BT_SINK_SRV_CALL_SRC)/bt_sink_srv_call_pseudo_dev.c \
                     $(BT_SINK_SRV_CALL_SRC)/bt_sink_srv_call_pseudo_dev_mgr.c \
                     $(BT_SINK_SRV_CALL_SRC)/bt_sink_srv_hf.c \
                     $(BT_SINK_SRV_CALL_SRC)/bt_sink_srv_hf_call_manager.c \
                     $(BT_SINK_SRV_CALL_SRC)/bt_sink_srv_hf_multipoint.c \
                     $(BT_SINK_SRV_CALL_SRC)/bt_sink_srv_pbapc.c
ifeq ($(MTK_AWS_MCE_ENABLE), y)
BT_SINK_SRV_FILES += $(BT_SINK_SRV_CALL_SRC)/bt_sink_srv_aws_mce_call.c \
		             $(BT_SINK_SRV_CALL_SRC)/bt_sink_srv_call_rho.c
endif                 
ifeq ($(MTK_BT_HSP_ENABLE), y)
BT_SINK_SRV_FILES += $(BT_SINK_SRV_CALL_SRC)/bt_sink_srv_hsp.c
endif

# Sink bt_music related
BT_SINK_SRV_BT_MUSIC_SRC = middleware/MTK/sink/src/bt_music
BT_SINK_SRV_FILES += $(BT_SINK_SRV_BT_MUSIC_SRC)/bt_sink_srv_a2dp.c \
                     $(BT_SINK_SRV_BT_MUSIC_SRC)/bt_sink_srv_a2dp_callback.c \
                     $(BT_SINK_SRV_BT_MUSIC_SRC)/bt_sink_srv_a2dp_state_machine.c \
                     $(BT_SINK_SRV_BT_MUSIC_SRC)/bt_sink_srv_avrcp.c \
                     $(BT_SINK_SRV_BT_MUSIC_SRC)/bt_sink_srv_music.c \
                     $(BT_SINK_SRV_BT_MUSIC_SRC)/bt_sink_srv_music_iot_device.c 

ifeq ($(MTK_BT_SPEAKER_ENABLE), y)
BT_SINK_SRV_FILES += $(BT_SINK_SRV_BT_MUSIC_SRC)/speaker_fec.c
endif

# Sink avm_direct related
BT_SINK_SRV_AVM_DIRECT = middleware/MTK/sink/src/avm_direct
BT_SINK_SRV_FILES += $(BT_SINK_SRV_AVM_DIRECT)/avm_direct_util.c \

ifeq ($(MTK_AWS_MCE_ENABLE), y)
BT_SINK_SRV_FILES += $(BT_SINK_SRV_BT_MUSIC_SRC)/bt_sink_srv_aws_mce_a2dp.c \
                     $(BT_SINK_SRV_BT_MUSIC_SRC)/bt_sink_srv_music_rho.c
endif

# AWS MCE related
ifeq ($(MTK_AWS_MCE_ENABLE), y)
BT_SINK_SRV_FILES += $(BT_SINK_SRV_SRC)/bt_sink_srv_aws_mce.c
ifeq ($(MTK_PROMPT_SOUND_SYNC_ENABLE), y)
BT_SINK_SRV_FILES += $(BT_SINK_SRV_SRC)/bt_sink_srv_aws_mce_vp_sync.c
CFLAGS += -DMTK_PROMPT_SOUND_SYNC_ENABLE
endif
endif

C_FILES += $(BT_SINK_SRV_FILES)

# BT callback manager module
include $(SOURCE_DIR)/middleware/MTK/bt_callback_manager/module.mk

# BT device manager module
include $(SOURCE_DIR)/middleware/MTK/bluetooth_service/bt_device_manager_module.mk

# Audio manager
include $(SOURCE_DIR)/middleware/MTK/audio_manager/module.mk

# Include bt sink path
CFLAGS += -I$(SOURCE_DIR)/middleware/MTK/bluetooth_service/inc
CFLAGS += -I$(SOURCE_DIR)/middleware/MTK/sink/inc
CFLAGS += -I$(SOURCE_DIR)/middleware/MTK/sink/inc/call
CFLAGS += -I$(SOURCE_DIR)/middleware/MTK/sink/inc/bt_music
#CFLAGS += -I$(SOURCE_DIR)/middleware/MTK/bt_sink/inc/audio_command_receiver
CFLAGS += -I$(SOURCE_DIR)/middleware/MTK/bluetooth/inc
CFLAGS += -I$(SOURCE_DIR)/driver/chip/inc

ifeq ($(MTK_AVM_DIRECT), y)
    CFLAGS += -I$(SOURCE_DIR)/middleware/MTK/audio/bt_codec/inc
else
    CFLAGS += -I$(SOURCE_DIR)/driver/board/mt25x3_hdk/bt_codec/inc
endif

CFLAGS += -mno-unaligned-access

endif

ifeq ($(MTK_WAV_DECODER_ENABLE), y)
CFLAGS += -DMTK_WAV_DECODER_ENABLE
endif

ifeq ($(MTK_MP3_DECODER_ENABLED), y)
CFLAGS += -DMTK_MP3_DECODER_ENABLED
endif

ifeq ($(MTK_AUDIO_GAIN_TABLE_ENABLE), y)
CFLAGS += -DMTK_AUDIO_GAIN_TABLE_ENABLE
endif

ifeq ($(MTK_AM_NOT_SUPPORT_STREAM_IN), y)
CFLAGS += -DMTK_AM_NOT_SUPPORT_STREAM_IN
endif

ifneq ($(wildcard $(strip $(SOURCE_DIR))/middleware/MTK/avm_direct_protected/),)
include $(SOURCE_DIR)/middleware/MTK/avm_direct_protected/GCC/module.mk
else
LIBS += $(SOURCE_DIR)/prebuilt/middleware/MTK/bluetooth/lib/libavm_direct.a
endif

