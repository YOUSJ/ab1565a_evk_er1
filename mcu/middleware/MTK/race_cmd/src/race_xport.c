/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */


#include "race_cmd_feature.h"
#include "FreeRTOS.h"
#include "task.h"
#include "hal_uart.h"
#include "syslog.h"
#include "serial_port.h"
#include "bt_callback_manager.h"
#include "atci_adapter.h"
#include "atci_main.h"
#include "race_cmd.h"
#include "race_xport.h"
#include "race_bt.h"
#include "task_def.h"
#include "race_lpcomm_packet.h"
#include "race_timer.h"
#include "race_lpcomm_util.h"
#include "race_storage_util.h"
#include "race_noti.h"
#include "race_lpcomm_recv.h"
#include "race_lpcomm_ps_list.h"
#include "race_lpcomm_retry.h"
#include "race_util.h"
#include "race_event_internal.h"
#ifdef RACE_FOTA_CMD_ENABLE
#include "fota_multi_info.h"
#include "race_fota_util.h"
#include "race_fota.h"
#endif
#ifdef RACE_ROLE_HANDOVER_SERVICE_ENABLE
#include "bt_role_handover.h"
#endif
#ifdef RACE_FIND_ME_ENABLE
#include "race_cmd_find_me.h"
#endif
#include "race_cmd_bluetooth.h"
#include "serial_port_assignment.h"

#if defined(MTK_MUX_ENABLE)
#include "mux.h"
#endif

#if defined(MTK_MUX_ENABLE)
static mux_port_t race_muxid_from_portid(serial_port_dev_t port_id)
{
    switch(port_id){
        case SERIAL_PORT_DEV_UART_0:
            return MUX_UART_0;
        case SERIAL_PORT_DEV_UART_1:
            return MUX_UART_1;
        case SERIAL_PORT_DEV_UART_2:
            return MUX_UART_2;
        case SERIAL_PORT_DEV_UART_3:
            return MUX_UART_3;
        case SERIAL_PORT_DEV_BT_SPP:
            return MUX_BT_SPP;
        case SERIAL_PORT_DEV_BT_LE:
            return MUX_BT_BLE;
#ifdef MTK_IAP2_PROFILE_ENABLE
        case SERIAL_PORT_DEV_IAP2_SESSION2:
            return MUX_IAP2_SESSION2;
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
        case SERIAL_PORT_DEV_BT_GATT_OVER_BREDR:
            return MUX_BT_GATT_OVER_BREDR;
#endif
        default:
            return 0xFF;
    }

    return 0xFF;
}

static serial_port_dev_t race_portid_from_muxid(mux_port_t mux_id)
{
    switch (mux_id) {
        case MUX_UART_0:
            return SERIAL_PORT_DEV_UART_0;

        case MUX_BT_SPP:
            return SERIAL_PORT_DEV_BT_SPP;

        case MUX_BT_BLE:
            return SERIAL_PORT_DEV_BT_LE;

#ifdef MTK_AIRUPDATE_ENABLE
        case MUX_BT_AIRUPATE:
            return SERIAL_PORT_DEV_BT_AIRUPDATE;
#endif

#ifdef MTK_IAP2_PROFILE_ENABLE
        case MUX_IAP2_SESSION2:
            return SERIAL_PORT_DEV_IAP2_SESSION2;
#endif

#ifdef MTK_GATT_OVER_BREDR_ENABLE
        case MUX_BT_GATT_OVER_BREDR:
            return SERIAL_PORT_DEV_BT_GATT_OVER_BREDR;
#endif
        default:
            return 0xFF;
    }
}


static RACE_ERRCODE race_convert_mux_event(serial_port_dev_t *device,
                                                   serial_port_callback_event_t *event,
                                                   void **parameter,
                                                   mux_handle_t handle,
                                                   mux_event_t mux_event,
                                                   uint32_t data_len,
                                                   void* user_data)
{
    UNUSED(parameter);
    UNUSED(data_len);
    UNUSED(user_data);

    RACE_ERRCODE ret = RACE_ERRCODE_SUCCESS;

    if (!device || !event) {
        return RACE_ERRCODE_PARAMETER_ERROR;
    }

    *device = race_portid_from_muxid(handle & 0xFF);

    if (0xFF == *device) {
        return RACE_ERRCODE_NOT_SUPPORT;
    }

    switch (mux_event) {
        case MUX_EVENT_READY_TO_READ: {
            *event = SERIAL_PORT_EVENT_READY_TO_READ;
            break;
        }

        case MUX_EVENT_READY_TO_WRITE: {
            *event = SERIAL_PORT_EVENT_READY_TO_WRITE;
            break;
        }

        case MUX_EVENT_CONNECTION: {
            if (SERIAL_PORT_DEV_BT_SPP == *device ||
#ifdef MTK_AIRUPDATE_ENABLE
                SERIAL_PORT_DEV_BT_AIRUPDATE == *device ||
#endif
                SERIAL_PORT_DEV_BT_LE == *device) {
                *event = SERIAL_PORT_EVENT_BT_CONNECTION;
            }
#ifdef MTK_IAP2_PROFILE_ENABLE
            else if (SERIAL_PORT_DEV_IAP2_SESSION2 == *device) {
                *event = SERIAL_PORT_EVENT_IAP2_SESSION_OPEN;
            }
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
            else if (SERIAL_PORT_DEV_BT_GATT_OVER_BREDR == *device) {
                *event = SERIAL_PORT_EVENT_BT_CONNECTION;
            }
#endif
            else {
                ret = RACE_ERRCODE_NOT_SUPPORT;
            }
            break;
        }

        case MUX_EVENT_DISCONNECTION: {
            if (SERIAL_PORT_DEV_BT_SPP == *device ||
#ifdef MTK_AIRUPDATE_ENABLE
                SERIAL_PORT_DEV_BT_AIRUPDATE == *device ||
#endif
                SERIAL_PORT_DEV_BT_LE == *device) {
                *event = SERIAL_PORT_EVENT_BT_DISCONNECTION;
            }
#ifdef MTK_IAP2_PROFILE_ENABLE
            else if (SERIAL_PORT_DEV_IAP2_SESSION2 == *device) {
                *event = SERIAL_PORT_EVENT_IAP2_SESSION_CLOSE;
            }
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
            else if (SERIAL_PORT_DEV_BT_GATT_OVER_BREDR == *device) {
                *event = SERIAL_PORT_EVENT_BT_DISCONNECTION;
            }
#endif
            else {
                ret = RACE_ERRCODE_NOT_SUPPORT;
            }
            break;
        }

        case MUX_EVENT_WAKEUP_FROM_SLEEP: {
            *event = SERIAL_PORT_EVENT_WAKEUP_SLEEP;
            break;
        }

        default: {
            ret = RACE_ERRCODE_NOT_SUPPORT;
            break;
        }
    }

    return ret;
}
#endif

#if (RACE_DEBUG_PRINT_ENABLE)
extern void race_debug_print(uint8_t *data, uint32_t size, const char *print_tag);
#endif

#ifdef MTK_ONLINE_LOG_TO_APK_ENABLE
#include "hal_nvic.h"
#endif

log_create_module(race, PRINT_LEVEL_INFO);


typedef struct
{
    serial_port_dev_t serial_port;
    uint32_t serial_port_handle;
}race_serial_port_info;

#define RACE_SERIAL_PORT_INFO_INIT(a) \
do{\
    (a).serial_port = SERIAL_PORT_DEV_UNDEFINED;\
    (a).serial_port_handle = 0;\
}while(0)

typedef struct
{
    race_serial_port_info ble_port;
    race_serial_port_info spp_port;
    race_serial_port_info airupdate_port;
    race_serial_port_info iap2_port;
    race_serial_port_info uart_port;
    race_serial_port_info gatt_over_bredr_port;
}race_serial_port_list;

uint8_t *auReceiveBuff = NULL;
race_serial_port_list *g_race_serial_port_list = NULL;
uint32_t g_race_serial_port_queue = 0;
uint32_t g_race_send_data_cache_left_len = 0;
uint32_t g_race_send_data_cache_len = 0;
uint8_t* g_race_send_data_cache = NULL;
serial_port_dev_t g_race_uart_port = SERIAL_PORT_DEV_UART_1;
serial_port_dev_t g_race_uart_port_default = SERIAL_PORT_DEV_UART_1;
#ifdef RACE_CREATE_TASK_DYNAMICALLY
TaskHandle_t g_race_task_hdl;
#endif
race_init_ctrl_t g_race_init_ctrl;

#ifdef MTK_RACE_EVENT_ID_ENABLE
int32_t g_race_event_register_id = RACE_EVENT_INVALID_REGISTER_ID;
#endif

race_input_cmd_msg_t uart_msg_item = { SERIAL_PORT_DEV_UNDEFINED, 0, 0, true, NULL };
race_input_cmd_msg_t ble_msg_item = { SERIAL_PORT_DEV_UNDEFINED, 0, 0, true, NULL };
race_input_cmd_msg_t spp_msg_item = { SERIAL_PORT_DEV_UNDEFINED, 0, 0, true, NULL };
race_input_cmd_msg_t airupdate_msg_item = { SERIAL_PORT_DEV_UNDEFINED, 0, 0, true, NULL };
race_input_cmd_msg_t iap2_msg_item = { SERIAL_PORT_DEV_UNDEFINED, 0, 0, true, NULL };
race_input_cmd_msg_t gatt_over_bredr_msg_item = { SERIAL_PORT_DEV_UNDEFINED, 0, 0, true, NULL };

#define RACE_COMMAND_ERROR_CHECK \
   "It is RACE command mode, please check the command format!\r\n"

#define G_SET_UART_PORT(u) (g_race_serial_port_list->uart_port.serial_port = ((serial_port_dev_t)u))
#define G_GET_UART_PORT() (g_race_serial_port_list->uart_port.serial_port)
#define G_SET_UART_PORT_HANDLE(u) (g_race_serial_port_list->uart_port.serial_port_handle = ((uint32_t)u))
#define G_GET_UART_PORT_HANDLE() (g_race_serial_port_list->uart_port.serial_port_handle)

#define G_SET_BLE_PORT(u) (g_race_serial_port_list->ble_port.serial_port = ((serial_port_dev_t)u))
#define G_GET_BLE_PORT() (g_race_serial_port_list->ble_port.serial_port)
#define G_SET_BLE_PORT_HANDLE(u) (g_race_serial_port_list->ble_port.serial_port_handle = ((uint32_t)u))
#define G_GET_BLE_PORT_HANDLE() (g_race_serial_port_list->ble_port.serial_port_handle)

#define G_SET_SPP_PORT(u) (g_race_serial_port_list->spp_port.serial_port = ((serial_port_dev_t)u))
#define G_GET_SPP_PORT() (g_race_serial_port_list->spp_port.serial_port)
#define G_SET_SPP_PORT_HANDLE(u) (g_race_serial_port_list->spp_port.serial_port_handle = ((uint32_t)u))
#define G_GET_SPP_PORT_HANDLE() (g_race_serial_port_list->spp_port.serial_port_handle)

#define G_SET_AIRUPDATE_PORT(u) (g_race_serial_port_list->airupdate_port.serial_port = ((serial_port_dev_t)u))
#define G_GET_AIRUPDATE_PORT() (g_race_serial_port_list->airupdate_port.serial_port)
#define G_SET_AIRUPDATE_PORT_HANDLE(u) (g_race_serial_port_list->airupdate_port.serial_port_handle = ((uint32_t)u))
#define G_GET_AIRUPDATE_PORT_HANDLE() (g_race_serial_port_list->airupdate_port.serial_port_handle)

#define G_SET_IAP2_PORT(u) (g_race_serial_port_list->iap2_port.serial_port = ((serial_port_dev_t)u))
#define G_GET_IAP2_PORT() (g_race_serial_port_list->iap2_port.serial_port)
#define G_SET_IAP2_PORT_HANDLE(u) (g_race_serial_port_list->iap2_port.serial_port_handle = ((uint32_t)u))
#define G_GET_IAP2_PORT_HANDLE() (g_race_serial_port_list->iap2_port.serial_port_handle)

#define G_SET_GATT_OVER_BREDR_PORT(u) (g_race_serial_port_list->gatt_over_bredr_port.serial_port = ((serial_port_dev_t)u))
#define G_GET_GATT_OVER_BREDR_PORT() (g_race_serial_port_list->gatt_over_bredr_port.serial_port)
#define G_SET_GATT_OVER_BREDR_PORT_HANDLE(u) (g_race_serial_port_list->gatt_over_bredr_port.serial_port_handle = ((uint32_t)u))
#define G_GET_GATT_OVER_BREDR_PORT_HANDLE() (g_race_serial_port_list->gatt_over_bredr_port.serial_port_handle)

#define STRNCPY(dest, source) strncpy(dest, source, strlen(source)+1);

race_status_t race_send_data(uint32_t port_handle, uint8_t* data, uint32_t data_len);


#if defined(MTK_RACE_CMD_ENABLE)

void* race_mem_alloc(uint32_t size)
{
    void *pvReturn = NULL;
    uint32_t  free_size;
    free_size = xPortGetFreeHeapSize();
    if (free_size > size) {
        pvReturn = pvPortMalloc(size);
    }

    return pvReturn;
}

void race_mem_free(void *buf)
{
    if (buf != NULL) {
        vPortFree(buf);
        buf = NULL;
    }
}

race_status_t race_vaild_port_check(uint32_t port)
{
    race_status_t ret = RACE_STATUS_ERROR;

#if (defined(MTK_ATCI_VIA_PORT_SERVICE) || defined(MTK_ATCI_VIA_MUX)) && defined(MTK_PORT_SERVICE_ENABLE)
    if( port != 0)
        ret = RACE_STATUS_OK;
#else
    if( port < HAL_UART_MAX)
        ret = RACE_STATUS_OK;
#endif

    return ret;
}

uint32_t race_serial_port_send_data(uint32_t port, uint8_t* buf, uint32_t buf_len)
{
    uint32_t data_len = 0;
    serial_port_status_t status = SERIAL_PORT_STATUS_OK;

    if(port == 0)
        return data_len;
#if defined(MTK_MUX_ENABLE)
    if(((race_get_channel_id_by_port_handle(port) == RACE_SERIAL_PORT_TYPE_UART) && ((port & 0xFF)== MUX_UART_0))
#if defined(MTK_MUX_BT_ENABLE)
        || race_get_channel_id_by_port_handle(port) == RACE_SERIAL_PORT_TYPE_SPP
        || race_get_channel_id_by_port_handle(port) == RACE_SERIAL_PORT_TYPE_BLE
#endif
#if defined(MTK_IAP2_VIA_MUX_ENABLE)
        || race_get_channel_id_by_port_handle(port) == RACE_SERIAL_PORT_TYPE_IAP2
#endif
#if defined(MTK_GATT_OVER_BREDR_ENABLE)
        || race_get_channel_id_by_port_handle(port) == RACE_SERIAL_PORT_TYPE_GATT_OVER_BREDR
#endif
    )
    {
        mux_status_t mux_status;
        mux_buffer_t mux_buffer={buf, buf_len};
        mux_status = mux_tx(port, &mux_buffer, 1, &data_len);
        if(mux_status != MUX_STATUS_OK){
            RACE_LOG_MSGID_E("race_serial_port_send_data() mux_tx fail, mux_status[%d]", 1, mux_status);
            status = SERIAL_PORT_STATUS_FAIL;
        }
    }
#else
    serial_port_write_data_t send_data;
    if((race_get_channel_id_by_port_handle(port) == RACE_SERIAL_PORT_TYPE_UART)
            || race_get_channel_id_by_port_handle(port) == RACE_SERIAL_PORT_TYPE_SPP
            || race_get_channel_id_by_port_handle(port) == RACE_SERIAL_PORT_TYPE_BLE
            || race_get_channel_id_by_port_handle(port) == RACE_SERIAL_PORT_TYPE_IAP2
            || race_get_channel_id_by_port_handle(port) == RACE_SERIAL_PORT_TYPE_GATT_OVER_BREDR)
    {
        send_data.data = buf;
        send_data.size = buf_len;
        status = serial_port_control(port, SERIAL_PORT_CMD_WRITE_DATA, (serial_port_ctrl_para_t*)&send_data);
        data_len = send_data.ret_size;
    }
#endif
    if (status != SERIAL_PORT_STATUS_OK)
    {
        data_len = 0;
        RACE_LOG_MSGID_E("race_serial_port_send_data() send data fail\r\n", 0);
    }

    return data_len;

}

uint32_t race_port_send_data(uint32_t port, uint8_t* buf, uint32_t buf_len)
{
    uint32_t data_len = 0;

#if (defined(MTK_ATCI_VIA_PORT_SERVICE) || defined(MTK_ATCI_VIA_MUX)) && defined(MTK_PORT_SERVICE_ENABLE)
    data_len = race_serial_port_send_data(port, buf, buf_len);
#else
    data_len = hal_uart_send_dma((hal_uart_port_t)port, buf, buf_len);
#endif

    RACE_LOG_MSGID_I("race_port_send_data, port:%d, data_len:%d",2, port, data_len);

    return data_len;
}


uint32_t race_serial_port_read_data(serial_port_handle_t handle, uint8_t *buf, uint32_t buf_len)
{
    //serial_port_get_read_avail_t read_data2;
    uint32_t data_len = 0;

    //status = serial_port_control(handle, SERIAL_PORT_CMD_GET_READ_AVAIL , (serial_port_ctrl_para_t *)&read_data2);
    //RACE_LOG_E("race_serial_port_read_data() receive data Length : %ld ", read_data2.ret_size);
#if defined(MTK_MUX_ENABLE)
    mux_status_t mux_status = MUX_STATUS_ERROR;

    if(((race_get_channel_id_by_port_handle(handle) == RACE_SERIAL_PORT_TYPE_UART) && ((handle & 0xFF)== MUX_UART_0))
#if defined(MTK_MUX_BT_ENABLE)
        || race_get_channel_id_by_port_handle(handle) == RACE_SERIAL_PORT_TYPE_SPP
        || race_get_channel_id_by_port_handle(handle) == RACE_SERIAL_PORT_TYPE_BLE
#endif
#if defined(MTK_IAP2_VIA_MUX_ENABLE)
        || race_get_channel_id_by_port_handle(handle) == RACE_SERIAL_PORT_TYPE_IAP2
#endif
#if defined(MTK_GATT_OVER_BREDR_ENABLE)
            || race_get_channel_id_by_port_handle(handle) == RACE_SERIAL_PORT_TYPE_GATT_OVER_BREDR
#endif
    )
    {
        mux_buffer_t mux_buffer={buf, buf_len};
        mux_status = mux_rx(handle, &mux_buffer, &data_len);
        RACE_LOG_MSGID_E("race_serial_port_read_data() mux_rx, status[%d], data_len[%d]", 2, mux_status, data_len);
    }
#else
    serial_port_read_data_t read_data;
    serial_port_status_t status = SERIAL_PORT_STATUS_FAIL;
    if((race_get_channel_id_by_port_handle(handle) == RACE_SERIAL_PORT_TYPE_UART)
        || race_get_channel_id_by_port_handle(handle) == RACE_SERIAL_PORT_TYPE_SPP
        || race_get_channel_id_by_port_handle(handle) == RACE_SERIAL_PORT_TYPE_BLE
        || race_get_channel_id_by_port_handle(handle) == RACE_SERIAL_PORT_TYPE_IAP2
        || race_get_channel_id_by_port_handle(handle) == RACE_SERIAL_PORT_TYPE_GATT_OVER_BREDR)
    {
        read_data.buffer = (uint8_t*)buf;
        read_data.size = buf_len;//read_data2.ret_size;
        status = serial_port_control(handle, SERIAL_PORT_CMD_READ_DATA, (serial_port_ctrl_para_t*)&read_data);

        if (status != SERIAL_PORT_STATUS_OK)
        {
            RACE_LOG_MSGID_E("race_serial_port_read_data() serial_port_control read fail", 0);
            return data_len;
        }
        data_len = read_data.ret_size;
    }
#endif
    return data_len;
}

uint32_t race_port_read_data(uint32_t port, uint8_t *buf, uint32_t buf_len)
{
    uint32_t data_len = 0;

#if defined(MTK_PORT_SERVICE_ENABLE)
    data_len = race_serial_port_read_data(port, buf, buf_len);
#else
    data_len = hal_uart_receive_dma((hal_uart_port_t)port, buf, buf_len);
#endif

    return data_len;
}

race_status_t race_serial_uart_port_check(serial_port_dev_t uart)
{
    if (uart >= SERIAL_PORT_DEV_UART_0 && uart <= SERIAL_PORT_DEV_UART_3)
    {
        return RACE_STATUS_OK;
    }
    if (uart >= SERIAL_PORT_DEV_USB_COM1 && uart <= SERIAL_PORT_DEV_USB_COM2)
    {
        return RACE_STATUS_OK;
    }
    return RACE_STATUS_ERROR;
}
race_status_t race_serial_spp_port_check(serial_port_dev_t dev_t)
{
    if (dev_t == SERIAL_PORT_DEV_BT_SPP)
    {
        return RACE_STATUS_OK;
    }
    return RACE_STATUS_ERROR;
}

race_status_t race_serial_ble_port_check(serial_port_dev_t dev_t)
{
    if (dev_t == SERIAL_PORT_DEV_BT_LE)
    {
        return RACE_STATUS_OK;
    }
    return RACE_STATUS_ERROR;
}

race_status_t race_serial_airupdate_port_check(serial_port_dev_t dev_t)
{
    if (dev_t == SERIAL_PORT_DEV_BT_AIRUPDATE)
    {
        return RACE_STATUS_OK;
    }
    return RACE_STATUS_ERROR;
}

#ifdef MTK_IAP2_PROFILE_ENABLE
race_status_t race_serial_iap2_port_check(serial_port_dev_t dev_t)
{
    if (dev_t == SERIAL_PORT_DEV_IAP2_SESSION2)
    {
        return RACE_STATUS_OK;
    }
    return RACE_STATUS_ERROR;
}
#endif

#ifdef MTK_GATT_OVER_BREDR_ENABLE
race_status_t race_serial_gatt_over_bredr_port_check(serial_port_dev_t dev_t)
{
    if (dev_t == SERIAL_PORT_DEV_BT_GATT_OVER_BREDR)
    {
        return RACE_STATUS_OK;
    }
    return RACE_STATUS_ERROR;
}
#endif

void GET_INPUT_CMD_MSG_PTR(serial_port_dev_t dev_t, race_input_cmd_msg_t **ptr)
{
    if (race_serial_uart_port_check((dev_t)) == RACE_STATUS_OK)
    {
        (*ptr) = &uart_msg_item;
    }
    else if (race_serial_ble_port_check((dev_t)) == RACE_STATUS_OK)
    {
        (*ptr) = &ble_msg_item;
    }
    else if (race_serial_spp_port_check((dev_t)) == RACE_STATUS_OK)
    {
        (*ptr) = &spp_msg_item;
    }
    else if (race_serial_airupdate_port_check((dev_t)) == RACE_STATUS_OK)
    {
        (*ptr) = &airupdate_msg_item;
    }
#ifdef MTK_IAP2_PROFILE_ENABLE
    else if (race_serial_iap2_port_check((dev_t)) == RACE_STATUS_OK)
    {
        (*ptr) = &iap2_msg_item;
    }
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
        else if (race_serial_gatt_over_bredr_port_check((dev_t)) == RACE_STATUS_OK)
        {
            (*ptr) = &gatt_over_bredr_msg_item;
        }
#endif

    return;
}

uint32_t race_get_serial_port_handle(serial_port_dev_t port)
{
    if (race_serial_uart_port_check(port) == RACE_STATUS_OK)
    {
        return G_GET_UART_PORT_HANDLE();
    }
    if (race_serial_ble_port_check(port) == RACE_STATUS_OK)
    {
        return G_GET_BLE_PORT_HANDLE();
    }
    if (race_serial_spp_port_check(port) == RACE_STATUS_OK)
    {
        return G_GET_SPP_PORT_HANDLE();
    }
    if (race_serial_airupdate_port_check(port) == RACE_STATUS_OK)
    {
        return G_GET_AIRUPDATE_PORT_HANDLE();
    }
#ifdef MTK_IAP2_PROFILE_ENABLE
    if (race_serial_iap2_port_check(port) == RACE_STATUS_OK)
    {
        return G_GET_IAP2_PORT_HANDLE();
    }
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
        if (race_serial_gatt_over_bredr_port_check(port) == RACE_STATUS_OK)
        {
            return G_GET_GATT_OVER_BREDR_PORT_HANDLE();
        }
#endif

    return 0;
}
void race_alloc_serial_port_list(race_serial_port_list **ptr)
{
    if (ptr)
    {
        *ptr = (race_serial_port_list *)race_mem_alloc(sizeof(race_serial_port_list));
        if (*ptr)
        {
            RACE_SERIAL_PORT_INFO_INIT((*ptr)->ble_port);
            RACE_SERIAL_PORT_INFO_INIT((*ptr)->spp_port);
            RACE_SERIAL_PORT_INFO_INIT((*ptr)->airupdate_port);
            RACE_SERIAL_PORT_INFO_INIT((*ptr)->iap2_port);
            RACE_SERIAL_PORT_INFO_INIT((*ptr)->uart_port);
            RACE_SERIAL_PORT_INFO_INIT((*ptr)->gatt_over_bredr_port);
        }
    }
}

/*void abc(race_xport_event_t event)
{
    RACE_LOG_MSGID_E("Callback fnction test success!!!!!!!!!!!!! event = %ld\r\n",1, (uint8_t)event);

}*/


void race_serial_port_data_callback(serial_port_dev_t device, serial_port_callback_event_t event, void *parameter)
{
    race_general_msg_t msg_queue_item;
    //uint16_t         queue_msg_num = 0;

    RACE_LOG_MSGID_I("race_serial_port_data_callback, device[%d], event[%d]", 2, device, event);

    switch (event)
    {
    case SERIAL_PORT_EVENT_READY_TO_READ:
    {
        RACE_LOG_MSGID_I("race_serial_port_data_callback, SERIAL_PORT_EVENT_READY_TO_READ", 0);
        msg_queue_item.msg_id = MSG_ID_RACE_LOCAL_SEND_CMD_IND;
        msg_queue_item.dev_t = device;
        msg_queue_item.msg_data = NULL;
        race_send_msg(&msg_queue_item);
        break;
    }

    case SERIAL_PORT_EVENT_READY_TO_WRITE:
    {
        RACE_LOG_MSGID_I("race_serial_port_data_callback, SERIAL_PORT_EVENT_READY_TO_WRITE", 0);
        msg_queue_item.msg_id = MSG_ID_RACE_LOCAL_WRITE_CMD_IND;
        msg_queue_item.dev_t = device;
        msg_queue_item.msg_data = NULL;
        race_send_msg(&msg_queue_item);
        break;
    }

    case SERIAL_PORT_EVENT_BT_DISCONNECTION:
    {
        if (SERIAL_PORT_DEV_BT_LE == device)
        {
            /* The current API is running in the port_service task. */
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_BLE_DISCONNECT, NULL);
        }

        if (SERIAL_PORT_DEV_BT_SPP == device)
        {
            /* The current API is running in the port_service task. */
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_SPP_DISCONNECT, NULL);
        }

#ifdef MTK_AIRUPDATE_ENABLE
        if (SERIAL_PORT_DEV_BT_AIRUPDATE == device)
        {
            /* The current API is running in the port_service task. */
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_AIRUPDATE_DISCONNECT, NULL);
        }
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
        if (SERIAL_PORT_DEV_BT_GATT_OVER_BREDR == device)
        {
            /* The current API is running in the port_service task. */
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_GATT_OVER_BREDR_DISCONNECT, NULL);
        }
#endif
        break;
    }

    case SERIAL_PORT_EVENT_BT_CONNECTION:
    {
        if (SERIAL_PORT_DEV_BT_LE == device)
        {
            /* The current API is running in the port_service task. */
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_BLE_CONNECT, NULL);
        }

        if (SERIAL_PORT_DEV_BT_SPP == device)
        {
            /* The current API is running in the port_service task. */
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_SPP_CONNECT, NULL);
        }

#ifdef MTK_AIRUPDATE_ENABLE
        if (SERIAL_PORT_DEV_BT_AIRUPDATE == device)
        {
            /* The current API is running in the port_service task. */
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_AIRUPDATE_CONNECT, NULL);
        }
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
        if (SERIAL_PORT_DEV_BT_GATT_OVER_BREDR == device)
        {
            /* The current API is running in the port_service task. */
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_GATT_OVER_BREDR_CONNECT, NULL);
        }
#endif
        break;
    }

#ifdef MTK_IAP2_PROFILE_ENABLE
    case SERIAL_PORT_EVENT_IAP2_SESSION_OPEN:
    {
        if (SERIAL_PORT_DEV_IAP2_SESSION2 == device)
        {
            /* The current API is running in the port_service task. */
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_IAP2_CONNECT, NULL);
        }

        break;
    }

    case SERIAL_PORT_EVENT_IAP2_SESSION_CLOSE:
    {
        if (SERIAL_PORT_DEV_IAP2_SESSION2 == device)
        {
            /* The current API is running in the port_service task. */
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_IAP2_DISCONNECT, NULL);
        }

        break;
    }
#endif

    case SERIAL_PORT_EVENT_USB_CONNECTION:
    {
        break;
    }
    case SERIAL_PORT_EVENT_USB_DISCONNECTION:
    {
        break;
    }

    default:
        break;
    }

}

#if defined(MTK_MUX_BT_ENABLE)

typedef enum {
    mux_buffer_idx_spp = 0,
    mux_buffer_idx_ble,
    mux_buffer_idx_iap2,
    mux_buffer_idx_gatt_over_bredr,
    mux_buffer_idx_max,
} race_mux_buffer_neum;

typedef struct
{
    mux_buffer_t *mux_buffer;
    uint32_t offset;
    uint32_t idx;
    uint32_t counter;
    uint32_t left_data_len;
    uint32_t total_length;
}race_mux_buffer_stru;

race_mux_buffer_stru race_mux_buffer[mux_buffer_idx_max];

static void race_multi_buffer_fetch_init( mux_buffer_t buffers[],uint32_t buffers_counter, uint8_t index)
{
    uint32_t i;
    race_mux_buffer[index].mux_buffer = buffers;
    race_mux_buffer[index].offset = 0;
    race_mux_buffer[index].idx = 0;
    race_mux_buffer[index].counter = buffers_counter;
    race_mux_buffer[index].left_data_len = 0;
    for(i=0; i<buffers_counter; i++) {
        race_mux_buffer[index].left_data_len += buffers[i].buf_size;
    }
    race_mux_buffer[index].total_length = race_mux_buffer[index].left_data_len;
}

static bool race_protocol_header_fetch(uint8_t *out_buf, uint32_t out_len, uint8_t index)
{
    uint32_t i;

    if (race_mux_buffer[index].idx >= race_mux_buffer[index].counter) {
        return false;
    }

    if(race_mux_buffer[index].left_data_len < out_len) {
        return false;
    }

    for(i=0; i<out_len; i++, race_mux_buffer[index].left_data_len--, race_mux_buffer[index].offset++)
    {
        if(race_mux_buffer[index].offset >= race_mux_buffer[index].mux_buffer[race_mux_buffer[index].idx].buf_size)
        {
            race_mux_buffer[index].idx++;
            race_mux_buffer[index].offset = 0;
            if(race_mux_buffer[index].idx>=race_mux_buffer[index].counter) {
                assert(0);
            }
        }
        *(out_buf+i) = *(race_mux_buffer[index].mux_buffer[race_mux_buffer[index].idx].p_buf + race_mux_buffer[index].offset);
    }
    return true;
}

/*
Complete: only used for mux spp during mux_init()
*/
static void race_mux_rx_protocol_callback_spp(mux_handle_t *handle, mux_buffer_t buffers[],uint32_t buffers_counter,
    uint32_t *consume_len, uint32_t *package_len,void *user_data)
{
    // *handle=G_GET_SPP_PORT_HANDLE();

    uint8_t race_channel, race_type, idx;
    uint16_t race_length, race_id;

    race_length = 0;
    *package_len = 0;
    *consume_len = 0;

    idx = mux_buffer_idx_spp;

    race_multi_buffer_fetch_init(buffers,buffers_counter, idx);

    if (race_protocol_header_fetch(&race_channel, 1, idx) == false) {
        *package_len = 0;
        *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
        RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_spp, race_channel fetch fail", 0);
        return;
    }

    if(race_channel & 0x05) { //RACE PACKAGE
        if (race_protocol_header_fetch(&race_type, 1, idx) == false) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_spp, race_type fetch fail", 0);
            return;
        }
        if ((race_type < RACE_TYPE_COMMAND) || (race_type > RACE_TYPE_NOTIFICATION)) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_spp fail, race_type[%d]", 1, race_type);
            return;
        }

        if (race_protocol_header_fetch((uint8_t *)&race_length, 2, idx) == false) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_spp, race_length fetch fail", 0);
            return;
        }

        if (race_protocol_header_fetch((uint8_t *)&race_id, 2, idx)== false) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_spp, race_id fetch fail", 0);
            return;
        }

        *handle = G_GET_SPP_PORT_HANDLE();
        if(*handle != NULL) {
            *package_len = race_length + 6 - 2;
            *consume_len = 0;
        } else {
            *package_len = 0;
            *consume_len = race_length + 6 - 2;
        }

        // RACE_LOG_MSGID_I("race_mux_rx_protocol_callback_spp ok, race_channel[0x%x], race_type[0x%x], race_length[%d], race_id[0x%x] handle[0x%x]", 5,
        //     race_channel, race_type, race_length, race_id, G_GET_SPP_PORT_HANDLE());
    }
    else
    {
        *package_len = 0;
        *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
        RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_spp fail, not RACE format", 0);
    }
}

static void race_mux_rx_protocol_callback_ble(mux_handle_t *handle, mux_buffer_t buffers[],uint32_t buffers_counter,
    uint32_t *consume_len, uint32_t *package_len,void *user_data)
{
    uint8_t race_channel, race_type, idx;
    uint16_t race_length, race_id;

    race_length = 0;
    *package_len = 0;
    *consume_len = 0;

    idx = mux_buffer_idx_ble;

    race_multi_buffer_fetch_init(buffers,buffers_counter, idx);

    if (race_protocol_header_fetch(&race_channel, 1, idx) == false) {
        *package_len = 0;
        *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
        RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_ble, race_channel fetch fail", 0);
        return;
    }

    if(race_channel & 0x05) { //RACE PACKAGE
        if (race_protocol_header_fetch(&race_type, 1, idx) == false) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_ble, race_type fetch fail", 0);
            return;
        }
        if ((race_type < RACE_TYPE_COMMAND) || (race_type > RACE_TYPE_NOTIFICATION)) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_ble fail, race_type[%d]", 1, race_type);
            return;
        }

        if (race_protocol_header_fetch((uint8_t *)&race_length, 2, idx) == false) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_ble, race_length fetch fail", 0);
            return;
        }

        if (race_protocol_header_fetch((uint8_t *)&race_id, 2, idx)== false) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_ble, race_id fetch fail", 0);
            return;
        }

        *handle = G_GET_BLE_PORT_HANDLE();
        if(*handle != NULL) {
            *package_len = race_length + 6 - 2;
            *consume_len = 0;
        } else {
            *package_len = 0;
            *consume_len = race_length + 6 - 2;
        }

        //RACE_LOG_MSGID_I("race_mux_rx_protocol_callback_ble ok, race_channel[0x%x], race_type[0x%x], race_length[%d], race_id[0x%x] handle[0x%x]", 5,
        //    race_channel, race_type, race_length, race_id, G_GET_BLE_PORT_HANDLE());
    }
    else
    {
        *package_len = 0;
        *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
        RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_ble fail, not RACE format channel:%x", 1, race_channel);
    }
}
#endif

#if defined(MTK_IAP2_PROFILE_ENABLE) && defined(MTK_IAP2_VIA_MUX_ENABLE)
static void race_mux_rx_protocol_callback_iap2(mux_handle_t *handle, mux_buffer_t buffers[],uint32_t buffers_counter,
    uint32_t *consume_len, uint32_t *package_len,void *user_data)
{
    uint8_t race_channel, race_type, idx;
    uint16_t race_length, race_id;

    race_length = 0;
    *package_len = 0;
    *consume_len = 0;

    idx = mux_buffer_idx_iap2;

    race_multi_buffer_fetch_init(buffers,buffers_counter, idx);

    if (race_protocol_header_fetch(&race_channel, 1, idx) == false) {
        *package_len = 0;
        *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
        RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_iap2, race_channel fetch fail", 0);
        return;
    }

    if(race_channel & 0x05) { //RACE PACKAGE
        if (race_protocol_header_fetch(&race_type, 1, idx) == false) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_iap2, race_type fetch fail", 0);
            return;
        }
        if ((race_type < RACE_TYPE_COMMAND) || (race_type > RACE_TYPE_NOTIFICATION)) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_iap2 fail, race_type[%d]", 1, race_type);
            return;
        }

        if (race_protocol_header_fetch((uint8_t *)&race_length, 2, idx) == false) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_iap2, race_length fetch fail", 0);
            return;
        }

        if (race_protocol_header_fetch((uint8_t *)&race_id, 2, idx)== false) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_iap2, race_id fetch fail", 0);
            return;
        }

        *handle = G_GET_IAP2_PORT_HANDLE();
        if(*handle != NULL) {
            *package_len = race_length + 6 - 2;
            *consume_len = 0;
        } else {
            *package_len = 0;
            *consume_len = race_length + 6 - 2;
        }

        // RACE_LOG_MSGID_I("race_mux_rx_protocol_callback_iap2 ok, race_channel[0x%x], race_type[0x%x], race_length[%d], race_id[0x%x] handle[0x%x]", 5,
        //     race_channel, race_type, race_length, race_id, G_GET_IAP2_PORT_HANDLE());
    }
    else
    {
        *package_len = 0;
        *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
        RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_iap2 fail, not RACE format", 0);
    }
}
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
static void race_mux_rx_protocol_callback_gatt_over_bredr(mux_handle_t *handle, mux_buffer_t buffers[],uint32_t buffers_counter,
    uint32_t *consume_len, uint32_t *package_len,void *user_data)
{
    uint8_t race_channel, race_type, idx;
    uint16_t race_length, race_id;

    race_length = 0;
    *package_len = 0;
    *consume_len = 0;

    idx = mux_buffer_idx_gatt_over_bredr;

    race_multi_buffer_fetch_init(buffers,buffers_counter, idx);

    if (race_protocol_header_fetch(&race_channel, 1, idx) == false) {
        *package_len = 0;
        *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
        RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_gatt_over_bredr, race_channel fetch fail", 0);
        return;
    }

    if(race_channel & 0x05) { //RACE PACKAGE
        if (race_protocol_header_fetch(&race_type, 1, idx) == false) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_gatt_over_bredr, race_type fetch fail", 0);
            return;
        }
        if ((race_type < RACE_TYPE_COMMAND) || (race_type > RACE_TYPE_NOTIFICATION)) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_gatt_over_bredr fail, race_type[%d]", 1, race_type);
            return;
        }

        if (race_protocol_header_fetch((uint8_t *)&race_length, 2, idx) == false) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_gatt_over_bredr, race_length fetch fail", 0);
            return;
        }

        if (race_protocol_header_fetch((uint8_t *)&race_id, 2, idx)== false) {
            *package_len = 0;
            *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
            RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_gatt_over_bredr, race_id fetch fail", 0);
            return;
        }

        *handle = G_GET_GATT_OVER_BREDR_PORT_HANDLE();
        if(*handle != NULL) {
            *package_len = race_length + 6 - 2;
            *consume_len = 0;
        } else {
            *package_len = 0;
            *consume_len = race_length + 6 - 2;
        }

        // RACE_LOG_MSGID_I("race_mux_rx_protocol_callback_gatt_over_bredr ok, race_channel[0x%x], race_type[0x%x], race_length[%d], race_id[0x%x] handle[0x%x]", 5,
        //     race_channel, race_type, race_length, race_id, G_GET_GATT_OVER_BREDR_PORT_HANDLE());
    }
    else
    {
        *package_len = 0;
        *consume_len = race_mux_buffer[idx].total_length - race_mux_buffer[idx].left_data_len;
        RACE_LOG_MSGID_E("race_mux_rx_protocol_callback_gatt_over_bredr fail, not RACE format", 0);
    }
}
#endif

/*
Complete: should be used for all mux ports during mux_open()
*/
#if defined(MTK_MUX_ENABLE)
static void race_mux_callback(mux_handle_t handle, mux_event_t event, uint32_t data_len,void *user_data)
{
    RACE_ERRCODE ret;
    serial_port_dev_t device;
    serial_port_callback_event_t port_event;
    void *parameter = NULL;

    RACE_LOG_MSGID_E("mux_callback,handle[%x], event[%d], data_len[%d]", 3, handle, event, data_len);

    ret = race_convert_mux_event(&device, &port_event, &parameter, handle, event, data_len, user_data);

    if (ret == RACE_ERRCODE_SUCCESS)
    {
        race_serial_port_data_callback(device, port_event, &parameter);
        RACE_LOG_MSGID_I("mux_callback ok\r\n", 0);
    }
    else
    {
        RACE_LOG_MSGID_E("mux_callback fail, status[%d]", 1, ret);
    }
}
#endif

race_status_t race_serial_port_init(serial_port_dev_t port, uint32_t *serial_port_handle)
{
    race_status_t ret = RACE_STATUS_ERROR;
    serial_port_status_t status = SERIAL_PORT_STATUS_FAIL;
    serial_port_open_para_t serial_port_open_para = {0};

    RACE_LOG_MSGID_I("race_serial_port_init, port[%d], serial_port_handle[0x%x], *serial_port_handle[0x%x]", 3,
        port, serial_port_handle, *serial_port_handle);

    if ((!serial_port_handle) || (*serial_port_handle))
    {
        RACE_LOG_MSGID_E("race_serial_port_init fail, port handle error\r\n", 0);
        return RACE_STATUS_ERROR;
    }

#if defined(MTK_MUX_ENABLE)
    if((port == SERIAL_PORT_DEV_UART_0) || (port == SERIAL_PORT_DEV_UART_1) ||
       (port == SERIAL_PORT_DEV_UART_2) || (port == SERIAL_PORT_DEV_UART_3)) // For UART port, we use mux service instead of port service.
    {
        mux_status_t mux_status;

        /*mux_port_setting_t setting;
        mux_protocol_t protocol_callback = {NULL, race_mux_rx_protocol_callback_spp, NULL};
        setting.tx_buffer_size = 1024;
        setting.rx_buffer_size = 1024;
        Don't need to call mux_init(), since it's already called in syslog.
        mux_init(race_muxid_from_portid(port), &setting, &protocol_callback);*/

        mux_status = mux_open(race_muxid_from_portid(port), "RACE_CMD", serial_port_handle, race_mux_callback, NULL);
        if(mux_status != MUX_STATUS_OK)
        {
            // When open failed, serial_port_open() will set handle value to an invalid value,
            // we reset it to zero for race_xport logic.
            *serial_port_handle = 0;
            status = SERIAL_PORT_STATUS_FAIL;
        }
        else
        {
            status = SERIAL_PORT_STATUS_OK;
        }
    }
    #if defined(MTK_MUX_BT_ENABLE)
    else if(port == SERIAL_PORT_DEV_BT_SPP) // For SPP, we use mux service instead of port service.
    {
        mux_port_setting_t setting;
        mux_protocol_t protocol_callback = {NULL, race_mux_rx_protocol_callback_spp, NULL};
        mux_status_t mux_status;

        setting.tx_buffer_size = RACE_MUX_SPP_TX_BUFFER_SIZE;
        setting.rx_buffer_size = RACE_MUX_SPP_RX_BUFFER_SIZE;
        mux_init(race_muxid_from_portid(port), &setting, &protocol_callback);
        mux_status = mux_open(race_muxid_from_portid(port), "BT_SPP", serial_port_handle, race_mux_callback, NULL);
        if(mux_status != MUX_STATUS_OK)
        {
            *serial_port_handle = 0; //set 0 if fail
            status = SERIAL_PORT_STATUS_FAIL;
        }
        else
        {
            status = SERIAL_PORT_STATUS_OK;
        }
    }
    else if(port == SERIAL_PORT_DEV_BT_LE){ // For BLE, we use mux service instead of port service.
        mux_port_setting_t setting;
        mux_protocol_t protocol_callback = {NULL, race_mux_rx_protocol_callback_ble, NULL};
        mux_status_t mux_status;

        setting.tx_buffer_size = RACE_MUX_BLE_TX_BUFFER_SIZE;
        setting.rx_buffer_size = RACE_MUX_BLE_RX_BUFFER_SIZE;
        mux_init(race_muxid_from_portid(port), &setting, &protocol_callback);
        mux_status = mux_open(race_muxid_from_portid(port), "BT_BLE", serial_port_handle, race_mux_callback, NULL);
        if(mux_status != MUX_STATUS_OK)
        {
            *serial_port_handle = 0; //set 0 if fail
            status = SERIAL_PORT_STATUS_FAIL;
        }
        else
        {
            status = SERIAL_PORT_STATUS_OK;
        }
    }
    #ifdef MTK_GATT_OVER_BREDR_ENABLE
    else if(port == SERIAL_PORT_DEV_BT_GATT_OVER_BREDR){ // For GATT, we use mux service instead of port service.
        mux_port_setting_t setting;
        mux_protocol_t protocol_callback = {NULL, race_mux_rx_protocol_callback_gatt_over_bredr, NULL};
        mux_status_t mux_status;

        setting.tx_buffer_size = RACE_MUX_GATT_OVER_BREDR_TX_BUFFER_SIZE;
        setting.rx_buffer_size = RACE_MUX_GATT_OVER_BREDR_RX_BUFFER_SIZE;
        mux_init(race_muxid_from_portid(port), &setting, &protocol_callback);
        mux_status = mux_open(race_muxid_from_portid(port), "GATT", serial_port_handle, race_mux_callback, NULL);
        if(mux_status != MUX_STATUS_OK)
        {
            *serial_port_handle = 0; //set 0 if fail
            status = SERIAL_PORT_STATUS_FAIL;
        }
        else
        {
            status = SERIAL_PORT_STATUS_OK;
        }
    }
    #endif
    #endif
    #if defined(MTK_IAP2_VIA_MUX_ENABLE) && defined(MTK_IAP2_PROFILE_ENABLE)
    else if(port == SERIAL_PORT_DEV_IAP2_SESSION2){ // For SPP, we use mux service instead of port service.
        mux_port_setting_t setting;
        mux_protocol_t protocol_callback = {NULL, race_mux_rx_protocol_callback_iap2, NULL};
        mux_status_t mux_status;

        setting.tx_buffer_size = RACE_MUX_IAP2_TX_BUFFER_SIZE;
        setting.rx_buffer_size = RACE_MUX_IAP2_RX_BUFFER_SIZE;
        mux_init(race_muxid_from_portid(port), &setting, &protocol_callback);
        mux_status = mux_open(race_muxid_from_portid(port), "iAP2", serial_port_handle, race_mux_callback, NULL);
        if(mux_status != MUX_STATUS_OK)
        {
            *serial_port_handle = 0; //set 0 if fail
            status = SERIAL_PORT_STATUS_FAIL;
        }
        else
        {
            status = SERIAL_PORT_STATUS_OK;
        }
    }
    #endif
    else
    {
        RACE_LOG_MSGID_E("race_serial_port_init fail, port is not support MUX, port[%d]", 1, port);
        // register the serial user event callback
        serial_port_open_para.callback = race_serial_port_data_callback;
        serial_port_open_para.tx_buffer_size = ATCI_UART_RX_FIFO_BUFFER_SIZE;
        serial_port_open_para.rx_buffer_size = ATCI_TX_BUFFER_SIZE;

        // for serial port configuration
        status = serial_port_open(port, &serial_port_open_para, serial_port_handle);
        if ((status == SERIAL_PORT_STATUS_OK) || (status == SERIAL_PORT_STATUS_DEV_NOT_READY))
        {
            RACE_LOG_MSGID_I("race_serial_port_init ok, port_service, port[%d], status[%d]", 2, port, status);
            ret = RACE_STATUS_OK;
        }
        else
        {
            RACE_LOG_MSGID_I("race_serial_port_init fail, port_service, port[%d], status[%d]", 2, port, status);
            *serial_port_handle = 0; //set 0 if fail
            ret = RACE_STATUS_ERROR;
        }
        return ret;
    }

    if (status == SERIAL_PORT_STATUS_OK) //todo check SERIAL_PORT_STATUS_DEV_NOT_READY
    {
        RACE_LOG_MSGID_I("race_serial_port_init ok, mux, port[%d], status[%d]", 2, port, status);
        ret = RACE_STATUS_OK;
    }
    else
    {
        RACE_LOG_MSGID_E("race_serial_port_init fail, mux, port[%d], status[%d]", 2, port, status);
        ret = RACE_STATUS_ERROR;
    }
#else
    // register the serial user event callback
    serial_port_open_para.callback = race_serial_port_data_callback;
    serial_port_open_para.tx_buffer_size = ATCI_UART_RX_FIFO_BUFFER_SIZE;
    serial_port_open_para.rx_buffer_size = ATCI_TX_BUFFER_SIZE;

    // for serial port configuration
    status = serial_port_open(port, &serial_port_open_para, serial_port_handle);
    if ((status == SERIAL_PORT_STATUS_OK) || (status == SERIAL_PORT_STATUS_DEV_NOT_READY))
    {
        RACE_LOG_MSGID_I("race_serial_port_init ok, port_service, port[%d], status[%d]", 2, port, status);
        ret = RACE_STATUS_OK;
    }
    else
    {
        RACE_LOG_MSGID_I("race_serial_port_init fail, port_service, port[%d], status[%d]", 2, port, status);
        *serial_port_handle = 0; //set 0 if fail
        ret = RACE_STATUS_ERROR;
    }
#endif

    return ret;
}


race_serial_port_type_enum race_get_channel_id_by_port_handle(uint32_t port_handle)
{
    // TODO: return real port type by chn id

    if (port_handle == G_GET_UART_PORT_HANDLE())
        return RACE_SERIAL_PORT_TYPE_UART;
    else if (port_handle == G_GET_BLE_PORT_HANDLE())
        return RACE_SERIAL_PORT_TYPE_BLE;
    else if (port_handle == G_GET_SPP_PORT_HANDLE())
        return RACE_SERIAL_PORT_TYPE_SPP;
    else if (port_handle == G_GET_AIRUPDATE_PORT_HANDLE())
        return RACE_SERIAL_PORT_TYPE_AIRUPDATE;
    else if (port_handle == G_GET_IAP2_PORT_HANDLE())
        return RACE_SERIAL_PORT_TYPE_IAP2;
    else if (port_handle == G_GET_GATT_OVER_BREDR_PORT_HANDLE())
        return RACE_SERIAL_PORT_TYPE_GATT_OVER_BREDR;
    else
        return RACE_SERIAL_PORT_TYPE_NONE;
}

uint32_t race_get_port_handle_by_channel_id(uint8_t channel_id)
{
    // TODO: return real port type by chn id

    race_serial_port_type_enum port_type = (race_serial_port_type_enum)channel_id;

    if (port_type == RACE_SERIAL_PORT_TYPE_UART)
        return G_GET_UART_PORT_HANDLE();
    else if (port_type == RACE_SERIAL_PORT_TYPE_BLE)
        return G_GET_BLE_PORT_HANDLE();
    else if (port_type == RACE_SERIAL_PORT_TYPE_SPP)
        return G_GET_SPP_PORT_HANDLE();
    else if (port_type == RACE_SERIAL_PORT_TYPE_AIRUPDATE)
        return G_GET_AIRUPDATE_PORT_HANDLE();
    else if (port_type == RACE_SERIAL_PORT_TYPE_IAP2)
        return G_GET_IAP2_PORT_HANDLE();
    else if (port_type == RACE_SERIAL_PORT_TYPE_GATT_OVER_BREDR)
        return G_GET_GATT_OVER_BREDR_PORT_HANDLE();
    else
        return 0;
}


uint8_t race_get_channel_id_by_port_type(race_serial_port_type_enum port_type)
{
    return (uint8_t)port_type;
}


race_serial_port_type_enum race_get_port_type_by_channel_id(uint8_t channel_id)
{
    /* The relationship between the port_type and the channel_id may change. So use port_handle to check. */
    if (G_GET_BLE_PORT_HANDLE() &&
        G_GET_BLE_PORT_HANDLE() == race_get_port_handle_by_channel_id(channel_id))
    {
        return RACE_SERIAL_PORT_TYPE_BLE;
    }

    if (G_GET_SPP_PORT_HANDLE() &&
        G_GET_SPP_PORT_HANDLE() == race_get_port_handle_by_channel_id(channel_id))
    {
        return RACE_SERIAL_PORT_TYPE_SPP;
    }

    if (G_GET_AIRUPDATE_PORT_HANDLE() &&
        G_GET_AIRUPDATE_PORT_HANDLE() == race_get_port_handle_by_channel_id(channel_id))
    {
        return RACE_SERIAL_PORT_TYPE_AIRUPDATE;
    }

    if (G_GET_IAP2_PORT_HANDLE() &&
        G_GET_IAP2_PORT_HANDLE() == race_get_port_handle_by_channel_id(channel_id))
    {
        return RACE_SERIAL_PORT_TYPE_IAP2;
    }

    if (G_GET_UART_PORT_HANDLE() &&
        G_GET_UART_PORT_HANDLE() == race_get_port_handle_by_channel_id(channel_id))
    {
        return RACE_SERIAL_PORT_TYPE_UART;
    }

    if (G_GET_GATT_OVER_BREDR_PORT_HANDLE() &&
        G_GET_GATT_OVER_BREDR_PORT_HANDLE() == race_get_port_handle_by_channel_id(channel_id))
    {
        return RACE_SERIAL_PORT_TYPE_GATT_OVER_BREDR;
    }

    return RACE_SERIAL_PORT_TYPE_NONE;
}

race_status_t race_serial_port_open(race_serial_port_type_enum port_type, serial_port_dev_t port_num)
{
    race_status_t ret = RACE_STATUS_ERROR;

    RACE_LOG_MSGID_I("race_serial_port_open, port_type[%d], port_num[%d], g_race_serial_port_list[0x%x]", 3,
        port_type, port_num, g_race_serial_port_list);

    if (!g_race_serial_port_list)
    {
        RACE_LOG_MSGID_E("race_serial_port_open fail, g_race_serial_port_list is NULL\r\n", 0);
        return ret;
    }

    switch (port_type)
    {
        case RACE_SERIAL_PORT_TYPE_UART:
            if (race_serial_uart_port_check(port_num) == RACE_STATUS_OK && (!G_GET_UART_PORT_HANDLE()))
            {
                G_SET_UART_PORT(port_num);
                ret = race_serial_port_init(port_num, &G_GET_UART_PORT_HANDLE());
                if (ret == RACE_STATUS_OK)
                {
                    g_race_init_ctrl.race_serial_port_uart = RACE_INIT_DONE;
                }
            }
            else
            {
                RACE_LOG_MSGID_E("race_serial_port_open, UART check fail, check[%d], handle[%d]", 2,
                    race_serial_uart_port_check(port_num), G_GET_UART_PORT_HANDLE());
            }
        break;
        case RACE_SERIAL_PORT_TYPE_SPP:
            if (race_serial_spp_port_check(port_num) == RACE_STATUS_OK && !G_GET_SPP_PORT_HANDLE())
            {
                G_SET_SPP_PORT(port_num);
                ret = race_serial_port_init(port_num, &G_GET_SPP_PORT_HANDLE());
                if (ret == RACE_STATUS_OK)
                {
                    g_race_init_ctrl.race_serial_port_spp = RACE_INIT_DONE;
                }
            }
            else
            {
                RACE_LOG_MSGID_E("race_serial_port_open, SPP check fail, check[%d], handle[%d]", 2,
                    race_serial_spp_port_check(port_num), G_GET_SPP_PORT_HANDLE());
            }
        break;
        case RACE_SERIAL_PORT_TYPE_BLE:
            if (race_serial_ble_port_check(port_num) == RACE_STATUS_OK && !G_GET_BLE_PORT_HANDLE())
            {
                G_SET_BLE_PORT(port_num);
                ret = race_serial_port_init(port_num, &G_GET_BLE_PORT_HANDLE());
                if (ret == RACE_STATUS_OK)
                {
                    g_race_init_ctrl.race_serial_port_ble = RACE_INIT_DONE;
                }
            }
            else
            {
                RACE_LOG_MSGID_E("race_serial_port_open, BLE check fail, check[%d], handle[%d]", 2,
                    race_serial_ble_port_check(port_num), G_GET_BLE_PORT_HANDLE());
            }
        break;
        case RACE_SERIAL_PORT_TYPE_AIRUPDATE:
            if (race_serial_airupdate_port_check(port_num) == RACE_STATUS_OK && !G_GET_AIRUPDATE_PORT_HANDLE())
            {
                G_SET_AIRUPDATE_PORT(port_num);
                ret = race_serial_port_init(port_num, &G_GET_AIRUPDATE_PORT_HANDLE());
                if (ret == RACE_STATUS_OK)
                {
                    g_race_init_ctrl.race_serial_port_airupdate = RACE_INIT_DONE;
                }
            }
            else
            {
                RACE_LOG_MSGID_E("race_serial_port_open, AIRUPDATE check fail, check[%d], handle[%d]", 2,
                    race_serial_airupdate_port_check(port_num), G_GET_AIRUPDATE_PORT_HANDLE());
            }
        break;
#ifdef MTK_IAP2_PROFILE_ENABLE
        case RACE_SERIAL_PORT_TYPE_IAP2:
            if (race_serial_iap2_port_check(port_num) == RACE_STATUS_OK && !G_GET_IAP2_PORT_HANDLE())
            {
                G_SET_IAP2_PORT(port_num);
                ret = race_serial_port_init(port_num, &G_GET_IAP2_PORT_HANDLE());
                if (ret == RACE_STATUS_OK)
                {
                    g_race_init_ctrl.race_serial_port_iap2 = RACE_INIT_DONE;
                }
            }
            else
            {
                RACE_LOG_MSGID_E("race_serial_port_open, IAP2 check fail, check[%d], handle[%d]", 2,
                    race_serial_iap2_port_check(port_num), G_GET_IAP2_PORT_HANDLE());
            }
        break;
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
        case RACE_SERIAL_PORT_TYPE_GATT_OVER_BREDR:
            if (race_serial_gatt_over_bredr_port_check(port_num) == RACE_STATUS_OK && !G_GET_GATT_OVER_BREDR_PORT_HANDLE())
            {
                G_SET_GATT_OVER_BREDR_PORT(port_num);
                ret = race_serial_port_init(port_num, &G_GET_GATT_OVER_BREDR_PORT_HANDLE());
                if (ret == RACE_STATUS_OK)
                {
                    g_race_init_ctrl.race_serial_port_gatt_over_bredr = RACE_INIT_DONE;
                }
            }
            else
            {
                RACE_LOG_MSGID_E("race_serial_port_open, GATT check fail, check[%d], handle[%d]", 2,
                    race_serial_gatt_over_bredr_port_check(port_num), G_GET_GATT_OVER_BREDR_PORT_HANDLE());
            }
        break;
#endif
        default:
        break;
    }

    if (ret == RACE_STATUS_OK)
    {
        RACE_LOG_MSGID_I("race_serial_port_open ok, port_type[%d], port_num[%d]", 2, port_type, port_num);
    }
    else
    {
        RACE_LOG_MSGID_E("race_serial_port_open fail, port_type[%d], port_num[%d]", 2, port_type, port_num);
    }

    return ret;
}

/*init spp,ble,airupdate*/
race_status_t race_serial_port_list_init(serial_port_dev_t port)
{
#if defined(MTK_MUX_BT_ENABLE)
    serial_port_dev_t ble_port = SERIAL_PORT_DEV_BT_LE;
    serial_port_dev_t spp_port = SERIAL_PORT_DEV_BT_SPP;
    serial_port_dev_t airupdate_port = SERIAL_PORT_DEV_BT_AIRUPDATE;
#endif
#ifdef MTK_IAP2_PROFILE_ENABLE
    serial_port_dev_t iap2_port = SERIAL_PORT_DEV_IAP2_SESSION2;
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
    serial_port_dev_t gatt_over_bredr_port = SERIAL_PORT_DEV_BT_GATT_OVER_BREDR;
#endif

    if (g_race_serial_port_list == NULL)
    {
        race_alloc_serial_port_list(&g_race_serial_port_list);
        if (g_race_serial_port_list == NULL)
        {
            return RACE_STATUS_ERROR;
        }
    }
    else
    {
        return RACE_STATUS_ERROR;
    }
    g_race_uart_port = port;

    race_serial_port_open(RACE_SERIAL_PORT_TYPE_UART, port);
#if defined(MTK_MUX_BT_ENABLE)
    race_serial_port_open(RACE_SERIAL_PORT_TYPE_SPP, spp_port);
    race_serial_port_open(RACE_SERIAL_PORT_TYPE_BLE, ble_port);
    race_serial_port_open(RACE_SERIAL_PORT_TYPE_AIRUPDATE, airupdate_port);
#endif
#ifdef MTK_IAP2_PROFILE_ENABLE
    race_serial_port_open(RACE_SERIAL_PORT_TYPE_IAP2, iap2_port);
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
    race_serial_port_open(RACE_SERIAL_PORT_TYPE_GATT_OVER_BREDR, iap2_port);
#endif

    return RACE_STATUS_OK;
}

race_status_t race_local_init()
{
    /* Init Queue */
    if (NULL == (void *)g_race_serial_port_queue)
    {
        g_race_serial_port_queue = atci_queue_create(RACE_LOCAL_QUEUE_LENGTH, sizeof(race_general_msg_t));
        if ((void *)g_race_serial_port_queue == NULL)
        {
            RACE_LOG_MSGID_E("race_local_init fail, g_race_serial_port_queue is NULL\r\n", 0);
            return RACE_STATUS_ERROR;
        }
    }
    if(auReceiveBuff == NULL)
    {
        auReceiveBuff = (uint8_t *)race_mem_alloc(SERIAL_PORT_RECEIVE_BUFFER_SIZE);
        if(auReceiveBuff == NULL)
        {
            RACE_LOG_MSGID_E("race_local_init fail, auReceiveBuff is NULL\r\n", 0);
            return RACE_STATUS_ERROR;
        }
    }

    return RACE_STATUS_OK;
}

race_status_t race_serial_port_uart_init(serial_port_dev_t port)
{
    race_status_t ret = RACE_STATUS_ERROR;

    RACE_LOG_MSGID_I("race_serial_port_uart_init init stste check state[%d], uart[%d], g_race_uart_port[%d]", 3,
        g_race_init_ctrl.race_init_status, g_race_init_ctrl.race_serial_port_uart, g_race_uart_port);

    if (g_race_init_ctrl.race_serial_port_uart == RACE_INIT_DONE)
    {
        RACE_LOG_MSGID_I("race_serial_port_uart_init exist.", 0);
        return RACE_STATUS_OK;
    }

#if defined(MTK_PORT_SERVICE_ENABLE)
    ret = race_serial_port_open(RACE_SERIAL_PORT_TYPE_UART, port);
#endif

    if (ret == RACE_STATUS_OK)
    {
        g_race_uart_port = port;
        g_race_init_ctrl.race_serial_port_uart = RACE_INIT_DONE;
        RACE_LOG_MSGID_I("race_serial_port_uart_init ok\r\n", 0);
    }
    else
    {
        g_race_uart_port = SERIAL_PORT_DEV_UART_1;
        g_race_init_ctrl.race_serial_port_uart = RACE_INIT_NONE;
        RACE_LOG_MSGID_I("race_serial_port_uart_init fail, status[%d]", 1, ret);
    }

    return ret;
}

race_status_t race_init_port_service(serial_port_dev_t port)
{
    race_status_t ret = RACE_STATUS_ERROR;

    RACE_LOG_MSGID_I("race_init_port_service, port[%d]", 1, port);

    /*RACE_LOG_I("race_init_port_service() init stste check state = %d, uart = %d, spp = %d, ble = %d\r\n",
        g_race_init_ctrl.race_init_status, g_race_init_ctrl.race_serial_port_uart,
        g_race_init_ctrl.race_serial_port_spp, g_race_init_ctrl.race_serial_port_ble);*/

    ret = race_local_init();

    if (ret != RACE_STATUS_OK) {
        //ret = race_port_deinit(port);//to do
        RACE_LOG_MSGID_I("race_local_init fail\r\n", 0);
        return ret;
    }

    ret = race_serial_port_list_init(port);

    if (ret == RACE_STATUS_OK)
        RACE_LOG_MSGID_I("race_init_port_service ok\r\n", 0);
    else
        RACE_LOG_MSGID_I("race_init_port_service fail, status[%d]", 1, ret);

#ifdef RACE_RELAY_CMD_ENABLE
    race_relay_cmd_init();
#endif

    return ret;
}

static race_status_t race_serial_port_deinit(serial_port_dev_t port, uint32_t serial_port_handle)
{
    race_status_t ret = RACE_STATUS_ERROR;
    serial_port_status_t status = SERIAL_PORT_STATUS_FAIL;

    RACE_LOG_MSGID_I("race_serial_port_deinit, port[%d], serial_port_handle[0x%x]", 2, port, serial_port_handle);

    if (!serial_port_handle)
    {
        RACE_LOG_MSGID_E("race_serial_port_deinit fail, port handle error\r\n", 0);
        return RACE_STATUS_ERROR;
    }

#if defined(MTK_MUX_ENABLE)
    mux_status_t mux_status;

    if((port == SERIAL_PORT_DEV_UART_0) || (port == SERIAL_PORT_DEV_UART_1) ||
       (port == SERIAL_PORT_DEV_UART_2) || (port == SERIAL_PORT_DEV_UART_3))
    {
        mux_status = mux_close(serial_port_handle);
        if(mux_status != MUX_STATUS_OK)
        {
            RACE_LOG_MSGID_E("race_serial_port_deinit, UART mux_close fail, mux_status[%d]", 1, mux_status);
            status = SERIAL_PORT_STATUS_FAIL;
        }
        else
        {
            mux_status = mux_deinit(race_muxid_from_portid(port));
            if(mux_status != MUX_STATUS_OK)
            {
                RACE_LOG_MSGID_E("race_serial_port_deinit, UART mux_deinit fail, mux_status[%d]", 1, mux_status);
                status = SERIAL_PORT_STATUS_FAIL;
            }
            else
            {
                status = SERIAL_PORT_STATUS_OK;
            }
        }
        G_SET_UART_PORT(SERIAL_PORT_DEV_UNDEFINED);
        G_SET_UART_PORT_HANDLE(NULL);
        g_race_init_ctrl.race_serial_port_uart = RACE_INIT_NONE;
    }
#if defined(MTK_MUX_BT_ENABLE)
    else if(port == SERIAL_PORT_DEV_BT_SPP)
    {
        mux_status = mux_close(serial_port_handle);
        if(mux_status != MUX_STATUS_OK)
        {
            RACE_LOG_MSGID_E("race_serial_port_deinit, SPP mux_close fail, mux_status[%d]", 1, mux_status);
            status = SERIAL_PORT_STATUS_FAIL;
        }
        else
        {
            mux_status = mux_deinit(race_muxid_from_portid(port));
            if(mux_status != MUX_STATUS_OK)
            {
                RACE_LOG_MSGID_E("race_serial_port_deinit, SPP mux_deinit fail, mux_status[%d]", 1, mux_status);
                status = SERIAL_PORT_STATUS_FAIL;
            }
            else
            {
                status = SERIAL_PORT_STATUS_OK;
            }
        }
        G_SET_SPP_PORT(SERIAL_PORT_DEV_UNDEFINED);
        G_SET_SPP_PORT_HANDLE(NULL);
        g_race_init_ctrl.race_serial_port_spp = RACE_INIT_NONE;
    }
    else if(port == SERIAL_PORT_DEV_BT_LE)
    {
        mux_status = mux_close(serial_port_handle);
        if(mux_status != MUX_STATUS_OK)
        {
            RACE_LOG_MSGID_E("race_serial_port_deinit, BLE mux_close fail, mux_status[%d]", 1, mux_status);
            status = SERIAL_PORT_STATUS_FAIL;
        }
        else
        {
            mux_status = mux_deinit(race_muxid_from_portid(port));
            if(mux_status != MUX_STATUS_OK)
            {
                RACE_LOG_MSGID_E("race_serial_port_deinit, BLE mux_deinit fail, mux_status[%d]", 1, mux_status);
                status = SERIAL_PORT_STATUS_FAIL;
            }
            else
            {
                status = SERIAL_PORT_STATUS_OK;
            }
        }
        G_SET_BLE_PORT(SERIAL_PORT_DEV_UNDEFINED);
        G_SET_BLE_PORT_HANDLE(NULL);
        g_race_init_ctrl.race_serial_port_ble = RACE_INIT_NONE;
    }
#ifdef MTK_GATT_OVER_BREDR_ENABLE
    else if(port == SERIAL_PORT_DEV_BT_GATT_OVER_BREDR)
    {
        mux_status = mux_close(serial_port_handle);
        if(mux_status != MUX_STATUS_OK)
        {
            RACE_LOG_MSGID_E("race_serial_port_deinit, GATT mux_close fail, mux_status[%d]", 1, mux_status);
            status = SERIAL_PORT_STATUS_FAIL;
        }
        else
        {
            mux_status = mux_deinit(race_muxid_from_portid(port));
            if(mux_status != MUX_STATUS_OK)
            {
                RACE_LOG_MSGID_E("race_serial_port_deinit, GATT mux_deinit fail, mux_status[%d]", 1, mux_status);
                status = SERIAL_PORT_STATUS_FAIL;
            }
            else
            {
                status = SERIAL_PORT_STATUS_OK;
            }
        }
        G_SET_GATT_OVER_BREDR_PORT(SERIAL_PORT_DEV_UNDEFINED);
        G_SET_GATT_OVER_BREDR_PORT_HANDLE(NULL);
        g_race_init_ctrl.race_serial_port_gatt_over_bredr = RACE_INIT_NONE;
    }
#endif
#endif
#if defined(MTK_IAP2_VIA_MUX_ENABLE) && defined(MTK_IAP2_PROFILE_ENABLE)
    else if(port == SERIAL_PORT_DEV_IAP2_SESSION2)
    {
        mux_status = mux_close(serial_port_handle);
        if(mux_status != MUX_STATUS_OK)
        {
            RACE_LOG_MSGID_E("race_serial_port_deinit, IAP2 mux_close fail, mux_status[%d]", 1, mux_status);
            status = SERIAL_PORT_STATUS_FAIL;
        }
        else
        {
            mux_status = mux_deinit(race_muxid_from_portid(port));
            if(mux_status != MUX_STATUS_OK)
            {
                RACE_LOG_MSGID_E("race_serial_port_deinit, IAP2 mux_deinit fail, mux_status[%d]", 1, mux_status);
                status = SERIAL_PORT_STATUS_FAIL;
            }
            else
            {
                status = SERIAL_PORT_STATUS_OK;
            }
        }
        G_SET_IAP2_PORT(SERIAL_PORT_DEV_UNDEFINED);
        G_SET_IAP2_PORT_HANDLE(NULL);
        g_race_init_ctrl.race_serial_port_iap2 = RACE_INIT_NONE;
    }
#endif
    else
    {
        RACE_LOG_MSGID_E("race_serial_port_deinit fail, port is not support MUX, port[%d]", 1, port);
    }

    if (status == SERIAL_PORT_STATUS_OK) //todo check : SERIAL_PORT_STATUS_DEV_NOT_READY
    {
        RACE_LOG_MSGID_I("race_serial_port_deinit ok, mux, port[%d], status[%d]", 2, port, status);
        ret = RACE_STATUS_OK;
    }
    else
    {
        RACE_LOG_MSGID_E("race_serial_port_deinit fail, mux, port[%d], status[%d]", 2, port, status);
        ret = RACE_STATUS_ERROR;
    }
#endif

    return ret;
}


#ifndef MTK_PORT_SERVICE_ENABLE
static race_status_t race_hal_uart_deinit(hal_uart_port_t port)
{
    hal_uart_status_t uart_ret = HAL_UART_STATUS_ERROR;
    race_status_t ret = RACE_STATUS_ERROR;

    uart_ret = hal_uart_deinit(port);

    RACE_LOG_MSGID_I("race_hal_uart_deinit() uart_ret = %d ",1, uart_ret);

    if (uart_ret == HAL_UART_STATUS_OK) {

        ret = RACE_STATUS_OK;
    }

    return ret;
}
#endif


race_status_t race_port_deinit(void)
{
    race_status_t ret = RACE_STATUS_ERROR;

    return ret;
}

/*race_status_t race_local_deinit(void)
{
    uint16_t i;
    uint16_t serial_port_queue_msg_num = 0;
    race_general_msg_t msg_queue_data;
    uint32_t data_len = 0;

    race_status_t ret =  RACE_STATUS_ERROR;

    RACE_LOG_MSGID_I("race_local_deinit enter\r\n", 0);

    if (NULL != (void *)g_race_serial_port_queue)
    {
        serial_port_queue_msg_num = atci_queue_get_item_num(g_race_serial_port_queue);
        RACE_LOG_MSGID_I("race_local_deinit serial_port_queue_msg_num = %d\r\n",1, serial_port_queue_msg_num);

        while (serial_port_queue_msg_num > 0) {

            atci_queue_receive_no_wait(g_race_serial_port_queue, &msg_queue_data);
            if (msg_queue_data.msg_id == MSG_ID_RACE_LOCAL_WRITE_CMD_IND) {
                // send the serial port output data
                if (g_race_send_data_cache_left_len > 0) {
                    race_send_data(G_GET_UART_PORT_HANDLE(), NULL, 0);
                }
            }
            serial_port_queue_msg_num --;
        }
        ret = RACE_STATUS_OK;
    }

    return ret;
}*/

race_status_t race_uart_deinit(void)
{
    race_status_t ret = RACE_STATUS_ERROR;

    RACE_LOG_MSGID_I("race_uart_deinit, race_serial_port_uart[%d]", 1, g_race_init_ctrl.race_serial_port_uart);

    if (g_race_init_ctrl.race_serial_port_uart == RACE_INIT_NONE) {
        RACE_LOG_MSGID_E("race_uart_deinit fail, race_serial_port_uart is not ready\r\n", 0);
        return RACE_STATUS_ERROR;
    }

#if defined(MTK_PORT_SERVICE_ENABLE)
    serial_port_status_t status = serial_port_close(G_GET_UART_PORT_HANDLE());
    if (status == SERIAL_PORT_STATUS_OK)
    {
        G_SET_UART_PORT(SERIAL_PORT_DEV_UNDEFINED);
        G_SET_UART_PORT_HANDLE(NULL);
        ret = RACE_STATUS_OK;
    }
#else
    ret = race_hal_uart_deinit(g_race_uart_port);
#endif

    if (ret == RACE_STATUS_OK)
    {
        g_race_init_ctrl.race_serial_port_uart = RACE_INIT_NONE;
        RACE_LOG_MSGID_I("race_uart_deinit ok\r\n", 0);
    }
    else
    {
        RACE_LOG_MSGID_E("race_uart_deinit fail\r\n", 0);
    }

    return ret;
}

race_status_t race_serial_port_close(race_serial_port_type_enum port_type)
{
    race_status_t ret = RACE_STATUS_ERROR;
    //#ifndef MTK_MUX_ENABLE
    serial_port_status_t status = SERIAL_PORT_STATUS_FAIL;
    //#endif

    RACE_LOG_MSGID_I("race_serial_port_close, port_type[%d], g_race_serial_port_list[0x%x]", 2,
        port_type, g_race_serial_port_list);

    if (!g_race_serial_port_list)
    {
        RACE_LOG_MSGID_I("race_serial_port_close fail, g_race_serial_port_list is NULL\r\n", 0);
        return ret;
    }

    switch (port_type)
    {
        case RACE_SERIAL_PORT_TYPE_UART:
            #if defined(MTK_MUX_ENABLE)
            if (race_serial_uart_port_check(G_GET_UART_PORT()) == RACE_STATUS_OK && G_GET_UART_PORT_HANDLE())
            {
                ret = race_serial_port_deinit(G_GET_UART_PORT(), G_GET_UART_PORT_HANDLE());
                if (ret == RACE_STATUS_OK)
                {
                    G_SET_UART_PORT(SERIAL_PORT_DEV_UNDEFINED);
                    G_SET_UART_PORT_HANDLE(NULL);
                    g_race_init_ctrl.race_serial_port_uart = RACE_INIT_NONE;
                }
            }
            else
            {
                RACE_LOG_MSGID_E("race_serial_port_close, UART check fail, check[%d], handle[%d]", 2,
                    race_serial_uart_port_check(G_GET_UART_PORT()), G_GET_UART_PORT_HANDLE());
            }
            #else
            status = serial_port_close(G_GET_UART_PORT_HANDLE());
            G_SET_UART_PORT(SERIAL_PORT_DEV_UNDEFINED);
            G_SET_UART_PORT_HANDLE(NULL);
            #endif
        break;

        case RACE_SERIAL_PORT_TYPE_SPP: //SPP is not disconnected
            #if defined(MTK_MUX_ENABLE) && defined(MTK_MUX_BT_ENABLE)
            if (race_serial_spp_port_check(G_GET_SPP_PORT()) == RACE_STATUS_OK && G_GET_SPP_PORT_HANDLE())
            {
                ret = race_serial_port_deinit(G_GET_SPP_PORT(), G_GET_SPP_PORT_HANDLE());
                if (ret == RACE_STATUS_OK)
                {
                    G_SET_SPP_PORT(SERIAL_PORT_DEV_UNDEFINED);
                    G_SET_SPP_PORT_HANDLE(NULL);
                    g_race_init_ctrl.race_serial_port_spp = RACE_INIT_NONE;
                }
            }
            else
            {
                RACE_LOG_MSGID_E("race_serial_port_close, SPP check fail, check[%d], handle[%d]", 2,
                    race_serial_spp_port_check(G_GET_SPP_PORT()), G_GET_SPP_PORT_HANDLE());
            }
            #else
            status = serial_port_close(G_GET_SPP_PORT_HANDLE());
            G_SET_SPP_PORT(SERIAL_PORT_DEV_UNDEFINED);
            G_SET_SPP_PORT_HANDLE(NULL);
            #endif
        break;

        case RACE_SERIAL_PORT_TYPE_BLE: //BLE is not disconnected
            #if defined(MTK_MUX_ENABLE) && defined(MTK_MUX_BT_ENABLE)
            if (race_serial_ble_port_check(G_GET_BLE_PORT()) == RACE_STATUS_OK && G_GET_BLE_PORT_HANDLE())
            {
                ret = race_serial_port_deinit(G_GET_BLE_PORT(), G_GET_BLE_PORT_HANDLE());
                if (ret == RACE_STATUS_OK)
                {
                    G_SET_BLE_PORT(SERIAL_PORT_DEV_UNDEFINED);
                    G_SET_BLE_PORT_HANDLE(NULL);
                    g_race_init_ctrl.race_serial_port_ble = RACE_INIT_NONE;
                }
            }
            else
            {
                RACE_LOG_MSGID_E("race_serial_port_close, BLE check fail, check[%d], handle[%d]", 2,
                    race_serial_ble_port_check(G_GET_BLE_PORT()), G_GET_BLE_PORT_HANDLE());
            }
            #else
            status = serial_port_close(G_GET_BLE_PORT_HANDLE());
            G_SET_BLE_PORT(SERIAL_PORT_DEV_UNDEFINED);
            G_SET_BLE_PORT_HANDLE(NULL);
            #endif
        break;

        case RACE_SERIAL_PORT_TYPE_AIRUPDATE: //TODO: check if AIRUPDATE is disconnected by calling this API
            #if 0//defined(MTK_MUX_ENABLE) && defined(MTK_MUX_BT_ENABLE)
            if (race_serial_airupdate_port_check(G_GET_AIRUPDATE_PORT()) == RACE_STATUS_OK && G_GET_AIRUPDATE_PORT_HANDLE())
            {
                ret = race_serial_port_deinit(G_GET_AIRUPDATE_PORT(), G_GET_AIRUPDATE_PORT_HANDLE());
                if (ret == RACE_STATUS_OK)
                {
                    G_SET_AIRUPDATE_PORT(SERIAL_PORT_DEV_UNDEFINED);
                    G_SET_AIRUPDATE_PORT_HANDLE(NULL);
                    g_race_init_ctrl.race_serial_port_airupdate = RACE_INIT_NONE;
                }
            }
            else
            {
                RACE_LOG_MSGID_E("race_serial_port_close, AIRUPDATE check fail, check[%d], handle[%d]", 2,
                    race_serial_airupdate_port_check(G_GET_AIRUPDATE_PORT()), G_GET_AIRUPDATE_PORT_HANDLE());
            }
            #else
            status = serial_port_close(G_GET_AIRUPDATE_PORT_HANDLE());
            G_SET_AIRUPDATE_PORT(SERIAL_PORT_DEV_UNDEFINED);
            G_SET_AIRUPDATE_PORT_HANDLE(NULL);
            #endif
        break;

        case RACE_SERIAL_PORT_TYPE_GATT_OVER_BREDR: //GATT is not disconnected
            #if defined(MTK_MUX_ENABLE) && defined(MTK_MUX_BT_ENABLE) && defined(MTK_GATT_OVER_BREDR_ENABLE)
                    if (race_serial_gatt_over_bredr_port_check(G_GET_GATT_OVER_BREDR_PORT()) == RACE_STATUS_OK && G_GET_GATT_OVER_BREDR_PORT_HANDLE())
                    {
                        ret = race_serial_port_deinit(G_GET_GATT_OVER_BREDR_PORT(), G_GET_GATT_OVER_BREDR_PORT_HANDLE());
                        if (ret == RACE_STATUS_OK)
                        {
                            G_SET_GATT_OVER_BREDR_PORT(SERIAL_PORT_DEV_UNDEFINED);
                            G_SET_GATT_OVER_BREDR_PORT_HANDLE(NULL);
                            g_race_init_ctrl.race_serial_port_gatt_over_bredr = RACE_INIT_NONE;
                        }
                    }
                    else
                    {
                        RACE_LOG_MSGID_E("race_serial_port_close, GATT check fail, check[%d], handle[%d]", 2,
                            race_serial_gatt_over_bredr_port_check(G_GET_GATT_OVER_BREDR_PORT()), G_GET_GATT_OVER_BREDR_PORT_HANDLE());
                    }
            #else
                    status = serial_port_close(G_GET_GATT_OVER_BREDR_PORT_HANDLE());
                    G_SET_GATT_OVER_BREDR_PORT(SERIAL_PORT_DEV_UNDEFINED);
                    G_SET_GATT_OVER_BREDR_PORT_HANDLE(NULL);
            #endif
        break;

        case RACE_SERIAL_PORT_TYPE_IAP2:
            #if defined(MTK_MUX_ENABLE) && defined(MTK_IAP2_VIA_MUX_ENABLE) && defined(MTK_IAP2_PROFILE_ENABLE)
            if (race_serial_iap2_port_check(G_GET_IAP2_PORT()) == RACE_STATUS_OK && G_GET_IAP2_PORT_HANDLE())
            {
                ret = race_serial_port_deinit(G_GET_IAP2_PORT(), G_GET_IAP2_PORT_HANDLE());
                if (ret == RACE_STATUS_OK)
                {
                    G_SET_IAP2_PORT(SERIAL_PORT_DEV_UNDEFINED);
                    G_SET_IAP2_PORT_HANDLE(NULL);
                    g_race_init_ctrl.race_serial_port_iap2 = RACE_INIT_NONE;
                }
            }
            else
            {
                RACE_LOG_MSGID_E("race_serial_port_close, IAP2 check fail, check[%d], handle[%d]", 2,
                    race_serial_iap2_port_check(G_GET_IAP2_PORT()), G_GET_IAP2_PORT_HANDLE());
            }
            #else
            status = serial_port_close(G_GET_IAP2_PORT_HANDLE());
            G_SET_IAP2_PORT(SERIAL_PORT_DEV_UNDEFINED);
            G_SET_IAP2_PORT_HANDLE(NULL);
            #endif
        break;

        default:
        break;
    }
    #ifndef MTK_MUX_ENABLE
    if (status == SERIAL_PORT_STATUS_OK)
    {
        RACE_LOG_MSGID_I("race_serial_port_close ok\r\n", 0);
        ret = RACE_STATUS_OK;
    }
    else
    {
        RACE_LOG_MSGID_I("race_serial_port_close fail, status[%d]", 1, status);
    }
    #endif

    return ret;
}

race_status_t race_send_data(uint32_t port_handle, uint8_t* data, uint32_t data_len)
{
    uint8_t* send_data = NULL;
    uint32_t send_len = 0;
    uint32_t sent_len = 0;
    race_status_t ret = RACE_STATUS_OK;

    RACE_LOG_MSGID_I("race_send_data() enter function, cache_left_len:%ld ",1, g_race_send_data_cache_left_len);
    if (race_vaild_port_check(port_handle) == RACE_STATUS_ERROR)
    {
        RACE_LOG_MSGID_E("race_send_data() send data fail,because port_handle is invalid:%ld ",1, port_handle);
        return RACE_STATUS_ERROR;
    }

    if (g_race_send_data_cache_left_len == 0) {

        send_data = data;
        send_len = data_len;
    }
    else if ((data == NULL) && (g_race_send_data_cache_left_len > 0)) {

        if (g_race_send_data_cache != NULL)
        {
            send_data = &g_race_send_data_cache[g_race_send_data_cache_len - g_race_send_data_cache_left_len];
            send_len = g_race_send_data_cache_left_len;
        }
        else
            return  RACE_STATUS_ERROR;
    }
    else
        return  RACE_STATUS_ERROR;

    // send data by port
    sent_len = race_port_send_data(port_handle, send_data, send_len);
    if (sent_len == 0)
    {
        ret = RACE_STATUS_ERROR;
        RACE_LOG_MSGID_E("race_send_data() send data fail\r\n", 0);
    }

    RACE_LOG_MSGID_I("race_send_data() sending data(len:%ld), sent data(len:%ld)",2, send_len, sent_len);

    if (sent_len >= send_len) {
        // send all the input data
        if (g_race_send_data_cache != NULL)
        {
            race_mem_free(g_race_send_data_cache);
            g_race_send_data_cache = NULL;
            RACE_LOG_MSGID_I("race_send_data() race_mem_free\r\n", 0);
        }

        g_race_send_data_cache_len = 0;
        g_race_send_data_cache_left_len = 0;


        return RACE_STATUS_OK;
    }
    else {
        // cache the left sending data
        if (g_race_send_data_cache_len == 0) {

            g_race_send_data_cache_len = send_len - sent_len;

            if (g_race_send_data_cache == NULL)
            {
                g_race_send_data_cache = (uint8_t *)race_mem_alloc(g_race_send_data_cache_len);
                if (g_race_send_data_cache == NULL)
                {
                    RACE_LOG_MSGID_W("race_send_data() alloc memory fail, drop the cached data(data_len:%ld)",1, g_race_send_data_cache_left_len);
                    g_race_send_data_cache_len = 0;
                    g_race_send_data_cache_left_len = 0;
                    return RACE_STATUS_ERROR;
                }
            }

            memset(g_race_send_data_cache, 0, g_race_send_data_cache_len);
            memcpy(g_race_send_data_cache, &send_data[sent_len], g_race_send_data_cache_len);
        }

        g_race_send_data_cache_left_len = send_len - sent_len;

        RACE_LOG_MSGID_I("race_send_data() cached data(data_len:%ld)",1, g_race_send_data_cache_left_len);
    }

    return ret;
}
//check 4 byte header
race_status_t race_command_valid_check(uint8_t *header, uint32_t *length)
{
    if ((0x05 == (header[0] & 0x0F)) &&
        ((0x5A == header[1]) ||
         (0x5B == header[1]) ||
         (0x5C == header[1]) ||
         (0x5D == header[1])))
    {
        *length = (uint32_t)(header[3] << 8) + (uint32_t)(header[2]);
        if (*length > RACE_MAX_LENGTH)
        {
            return RACE_STATUS_ERROR;
        }
        return RACE_STATUS_OK;
    }
    return RACE_STATUS_ERROR;
}


race_status_t race_read_data(serial_port_dev_t dev_t)
{
    race_status_t ret = RACE_STATUS_OK;
    RACE_ERRCODE retn = RACE_ERRCODE_SUCCESS;
    uint32_t port_handle = 0;
    uint32_t  length = 0;
    uint8_t *race_cmd_header;
    uint32_t read_len = 0, offset = 0;
    uint8_t *ptrPos = NULL;
    race_input_cmd_msg_t *msg_item = NULL;
    race_general_msg_t msg_queue_item;

    port_handle = race_get_serial_port_handle(dev_t);
    RACE_LOG_MSGID_I("race_read_data, enter,port_handle:%ld\r\n",1, port_handle);
    if (race_vaild_port_check(port_handle) == RACE_STATUS_ERROR)
    {
        return RACE_STATUS_ERROR;
    }

    GET_INPUT_CMD_MSG_PTR(dev_t,&msg_item);
    if (msg_item == NULL)
    {
        return RACE_STATUS_ERROR;
    }

    read_len = race_port_read_data(port_handle, auReceiveBuff, SERIAL_PORT_RECEIVE_BUFFER_SIZE);
    RACE_LOG_MSGID_I("race_read_data, data length:%ld\r\n",1, read_len);

    ptrPos = auReceiveBuff;
    offset = 0;
    while (read_len > 0)
    {
        if (msg_item->isNewRaceCmd)
        {
            //check header 4 bytes
            race_cmd_header = ptrPos;
            if (read_len < 4)
            {
                RACE_LOG_MSGID_I("race_read_data, length < 4 when read a new race command\r\n", 0);
                ret = RACE_STATUS_ERROR;
                break;
            }
            //check header and alloc memery
            if (race_command_valid_check(race_cmd_header, &length) == RACE_STATUS_OK)
            {
                if (msg_item->msg_data != NULL)
                {
                    /* Should not happen actually */
                    RACE_LOG_MSGID_W("Free last uncompleted race_cmd. msg_data:%x",1, msg_item->msg_data);
                    race_mem_free(msg_item->msg_data);
                }
                msg_item->msg_data = race_mem_alloc(length + 4);
                msg_item->uLength = 0;
                if (!msg_item->msg_data)
                {
                    ret = RACE_STATUS_ERROR;
                    break;
                }
            }
            else
            {
                RACE_LOG_MSGID_I("race_read_data, check race command fail.", 0);
                ret = RACE_STATUS_ERROR;
                break;
            }
            memcpy(msg_item->msg_data, race_cmd_header, 4);
            msg_item->uLength += 4;
            msg_item->uLeftLength = length;
            msg_item->isNewRaceCmd = false;
            msg_item->dev_t = dev_t;

            offset = 4;
        }
        else
        {
            //read left data
            /*
            read_len = race_port_read_data(port_handle, (uint8_t*)(msg_item->msg_data + msg_item->uLength), msg_item->uLeftLength);
            if (read_len == 0)
            {
                break;
            }
            */
            if (read_len < msg_item->uLeftLength)
            {
                memcpy((uint8_t*)(msg_item->msg_data + msg_item->uLength), ptrPos, read_len);
                msg_item->isNewRaceCmd = false;
                msg_item->uLength += read_len;
                msg_item->uLeftLength -= read_len;

                offset = read_len;
            }
            else
            {
                memcpy((uint8_t*)(msg_item->msg_data + msg_item->uLength), ptrPos, msg_item->uLeftLength);
                msg_queue_item.msg_id = MSG_ID_RACE_LOCAL_RSP_NOTIFY_IND;
                msg_queue_item.dev_t = dev_t;
                msg_queue_item.msg_data = msg_item->msg_data;
                RACE_LOG_MSGID_I("race_read_data, read a race command done. length = %d\r\n",1, (msg_item->uLength + msg_item->uLeftLength));
                retn = race_send_msg(&msg_queue_item);
                if (RACE_ERRCODE_SUCCESS != retn)
                {
                    RACE_LOG_MSGID_I("race_read_data, race_send_msg MSG_ID_RACE_LOCAL_RSP_NOTIFY_IND fail\r\n", 0);
                    //race_mem_free(msg_item->msg_data);
                    /* Do not break here to process the rest data received. */
                }

                offset = msg_item->uLeftLength;

                msg_item->isNewRaceCmd = true;
                msg_item->uLeftLength = 0;
                msg_item->msg_data = NULL;
                msg_item->uLength = 0;
                msg_item->dev_t = SERIAL_PORT_DEV_UNDEFINED;
            }
        }

        read_len -= offset;
        ptrPos += offset;

    }

    //race_mem_free(auReciveBuff);

    return ret;
}


race_status_t race_flush_packet_relay(race_pkt_t *race_pkt, uint16_t length, uint8_t channel_id, uint8_t type, uint8_t send_idx)
{
#ifdef RACE_RELAY_CMD_ENABLE
    bt_send_aws_mce_race_cmd_data(race_pkt, length, channel_id, type, send_idx);
#endif

    return RACE_STATUS_OK;
}


race_status_t race_flush_packet(uint8_t *ptr, uint8_t channel_id)
{
    race_status_t ret = RACE_STATUS_ERROR;
    race_send_pkt_t* pCmd = NULL;

    uint32_t port_handle= 0;

    RACE_LOG_MSGID_I("race_flush_packet, ptr:%x channel_id:%d ", 2, ptr, channel_id);

    if (ptr != NULL)
    {
        pCmd = race_pointer_cnv_pkt_to_send_pkt((void *)ptr);
        port_handle = race_get_port_handle_by_channel_id(channel_id);
        race_port_send_data(port_handle, (uint8_t*)&pCmd->race_data, pCmd->length);
#if (RACE_DEBUG_PRINT_ENABLE)
        race_pkt_t      *pret;
        race_send_pkt_t *psend;
        psend = (race_send_pkt_t *)pCmd;
        pret = &psend->race_data;
        race_debug_print((uint8_t *)pret, (uint32_t)(pret->hdr.length + 4), "Race flush data:");
#endif
        ret = RACE_STATUS_OK;
        race_mem_free(pCmd);
    }
    else
    {
    }

    return ret;
}

race_status_t race_input_command_handler(uint32_t port_handle, uint8_t* race_input_cmd)
{
    race_status_t ret = RACE_STATUS_ERROR;
    //race_pkt_t* pCmd = NULL;
    race_send_pkt_t* pEvt = NULL;
    uint32_t length = 0;
    uint8_t channel_id = 0;

    RACE_LOG_MSGID_I("race_input_command_handler() enter ", 0);

    //g_race_input_command
    race_pkt_t* pCmd = (race_pkt_t*)race_input_cmd;
    if (pCmd)
    {
        channel_id = (uint8_t)race_get_channel_id_by_port_handle(port_handle);
        if ((pEvt = RACE_CmdHandler(pCmd, channel_id)))
        {//handled complete
            if (pEvt != NULL)
            {
                length = race_port_send_data(port_handle, (uint8_t*)&pEvt->race_data, pEvt->length);
                //length = race_port_send_data(g_race_port_handle, (uint8_t*)&g_race_input_command, g_race_input_command.input_len);
                RACE_LOG_MSGID_I("race_port_send_data() length = %d ",1, (int)length);
                ret = RACE_STATUS_OK;
                race_mem_free(pEvt);
            }
        }
    }
    return ret;
}

void race_flush_msg(uint32_t port, void* msg)
{
    uint8_t *pMsg;
    uint32_t length = (uint32_t)strlen((char *)msg)+1;


    RACE_LOG_MSGID_I("race_flush_msg length = %d",1, length);
    pMsg = (uint8_t *)race_mem_alloc(length);

    if (pMsg != NULL)
    {
        STRNCPY((char *)pMsg, msg);
        race_port_send_data(port, (uint8_t*)pMsg, length);
        race_mem_free(pMsg);
    }
}

void race_input_data_processing(race_general_msg_t *msg)
{
    race_status_t ret = RACE_STATUS_ERROR;
    ret = race_read_data(msg->dev_t);
    if (ret == RACE_STATUS_OK)
    {
        RACE_LOG_MSGID_I("race_read_data() done!", 0);
    }
    else
    {
        RACE_LOG_MSGID_I("race_read_data() fail ret:%d!",1, ret);
        race_flush_msg(G_GET_UART_PORT_HANDLE(), RACE_COMMAND_ERROR_CHECK);
    }
}
void race_send_cache(race_general_msg_t *msg)
{
    uint32_t port_handle = 0;
    // send the cache output data
    RACE_LOG_MSGID_I("g_send_data_cache_left_len(%ld)",1, g_race_send_data_cache_left_len);
    port_handle = race_get_serial_port_handle(msg->dev_t);
    if (g_race_send_data_cache_left_len > 0)
    {
        race_send_data(port_handle, NULL, 0);
    }
}
void race_handle_command(race_general_msg_t *msg)
{
    uint32_t port_handle = 0;
    /* handling someone RACE command. Need to wait the response and then could handle the next RACE command */

    if (!msg || !msg->msg_data)
    {
        RACE_LOG_MSGID_I("race_handle_command, msg:%x, invalid parameters.", 1, msg ? msg : NULL);
        return;
    }

    RACE_LOG_MSGID_I("race_handle_command, msg:%x, msg_data:%x", 2, msg, msg->msg_data);

    port_handle = race_get_serial_port_handle(msg->dev_t);
    if (race_vaild_port_check(port_handle) == RACE_STATUS_ERROR)
    {
        //race_mem_free(msg->msg_data);
        RACE_LOG_MSGID_E("port_handle is invalid:%ld ",1, port_handle);
        return;
    }
    race_input_command_handler(port_handle, msg->msg_data);
    race_mem_free(msg->msg_data);
}

#ifdef MTK_ONLINE_LOG_TO_APK_ENABLE
#define RACE_HEARDER_SIZE        (12)
#define RACE_DATA_SIZE           (500)
#define SEND_THREAD_HOLD_SIZE    (2)
#define SPP_BT_MTU_MAX_SIZE  (RACE_DATA_SIZE - RACE_HEARDER_SIZE)
extern void online_log_race_sent_data(uint32_t *curr_log_addr, uint32_t *curr_log_size);
extern void online_log_race_update_readp(uint32_t read_length);
volatile uint32_t retry_send;
volatile uint32_t retry_read_addr;
volatile uint32_t retry_log_size;

ATTR_ZIDATA_IN_NONCACHED_SYSRAM_4BYTE_ALIGN static uint8_t g_syslog_buffer[SPP_BT_MTU_MAX_SIZE+RACE_HEARDER_SIZE];
void race_send_online_Log_data(race_general_msg_t *msg)
{
    uint32_t read_address;
    uint32_t curr_log_size;
    uint32_t length, log_length, send_len;
    uint32_t port_handle;
    uint32_t mask;
    uint32_t send_times;

    (void ) msg;

    if (retry_send == 0) {
        //HW write point is changed, if log is wrapped which will cause issue
        online_log_race_sent_data(&read_address, &curr_log_size);
    } else {
        read_address = retry_read_addr;
        curr_log_size = retry_log_size;
        if (retry_send > 100) {
            assert(0);
        }
    }

    send_times = 0;
    g_syslog_buffer[0] = 6;               //OS_OFFSET_OF(RACE_IPC_STRU, payload);
    g_syslog_buffer[1] = 0;
    //g_syslog_buffer[2] = (length+6);    //sizeof(RACE_COMMON_HDR_STRU) + dat_len;
   // g_syslog_buffer[3] = (length+6)>>8;
    g_syslog_buffer[4] = 5;               //channel_id: SERIAL_PORT_DEV_BT_SPP
    g_syslog_buffer[5] = 0xCC;
    g_syslog_buffer[6] = 0x05;
    g_syslog_buffer[7] = 0x5D;
    //g_syslog_buffer[8] = (length+2);
    //g_syslog_buffer[9] = (length+2)>>8;
    g_syslog_buffer[10] = 0xFF;
    g_syslog_buffer[11] = 0xFF;

    length = SPP_BT_MTU_MAX_SIZE;
    log_length = curr_log_size;
    port_handle = race_get_port_handle_by_channel_id(RACE_SERIAL_PORT_TYPE_SPP);

    while (log_length > 0) {
        if (log_length > SPP_BT_MTU_MAX_SIZE) {
            length = SPP_BT_MTU_MAX_SIZE;
        } else {
            length = log_length;    //packet size
        }

        //g_syslog_buffer[0] = 6;               //OS_OFFSET_OF(RACE_IPC_STRU, payload);
        //g_syslog_buffer[1] = 0;
        //g_syslog_buffer[2] = (length+6);    //sizeof(RACE_COMMON_HDR_STRU) + dat_len;
        //g_syslog_buffer[3] = (length+6)>>8;
        //g_syslog_buffer[4] = 5;               //channel_id: SERIAL_PORT_DEV_BT_SPP
        //g_syslog_buffer[5] = 0xCC;
        //g_syslog_buffer[6] = 0x05;
        //g_syslog_buffer[7] = 0x5D;
        //g_syslog_buffer[8] = (length+2);
        //g_syslog_buffer[9] = (length+2)>>8;
        //g_syslog_buffer[10] = 0xFF;
        //g_syslog_buffer[11] = 0xFF;
        g_syslog_buffer[2] = (length+6);    //sizeof(RACE_COMMON_HDR_STRU) + dat_len;
        g_syslog_buffer[3] = (length+6)>>8;
        //data is 05 5D LENGTH FF FF Datas;
        //LENGTH includes FF FF datas
        g_syslog_buffer[8] = (length+2);
        g_syslog_buffer[9] = (length+2)>>8;

        memcpy(&g_syslog_buffer[12], (uint8_t *)(read_address + send_times * SPP_BT_MTU_MAX_SIZE), length);
        send_len = race_port_send_data(port_handle, &g_syslog_buffer[6], (length+6));
        if ((send_len == 0) || (send_len != (length+6))) {
            hal_nvic_save_and_set_interrupt_mask(&mask);
            retry_send++;
            retry_read_addr = read_address + send_times * SPP_BT_MTU_MAX_SIZE;
            retry_log_size = curr_log_size - send_times * SPP_BT_MTU_MAX_SIZE;
            hal_nvic_restore_interrupt_mask(mask);
            return;
        }

        send_times++;
        retry_send = 0;
        retry_read_addr = 0;
        retry_log_size = 0;
        online_log_race_update_readp(length);
        log_length -= length;
        if (log_length > SEND_THREAD_HOLD_SIZE) {
            /* Delay 60ms to send next packege. */
            vTaskDelay(60 / portTICK_PERIOD_MS);
        }
    }
}
#endif

/* msg will be copied into message queue and should be freed after this API returns if it's allocated dynamically. */
RACE_ERRCODE race_send_msg(race_general_msg_t *msg)
{
    int32_t status = ATCI_STATUS_ERROR;

    if (!msg)
    {
        return RACE_ERRCODE_PARAMETER_ERROR;
    }

    status = atci_queue_send(g_race_serial_port_queue, (void*)msg);

    status = ATCI_STATUS_OK != status ? RACE_ERRCODE_FAIL : RACE_ERRCODE_SUCCESS;

    RACE_LOG_MSGID_I("race_send_msg %x msg_id:%d", 2, status, msg->msg_id);
    return status;
}


void race_processing(void)
{
    int common_queue_msg_num = 0;
    race_general_msg_t msg_queue_data;

    RACE_LOG_MSGID_I("race_processing() enter", 0);

    while (1) {
        RACE_LOG_MSGID_I("race_processing() wait message queue", 0);
        common_queue_msg_num = atci_queue_get_item_num(g_race_serial_port_queue);
        if(common_queue_msg_num > 0)
        {
            RACE_LOG_MSGID_I("race_processing() input_queue_msg_num(%d)",1, common_queue_msg_num);
        }
        atci_queue_receive_wait(g_race_serial_port_queue, &msg_queue_data, RACE_MAX_DELAY);
        RACE_LOG_MSGID_I("race_processing() handle message, msg_id: %d",1,msg_queue_data.msg_id);
        switch (msg_queue_data.msg_id)
        {
        case MSG_ID_RACE_LOCAL_SEND_CMD_IND:
            race_input_data_processing(&msg_queue_data);
            break;
        case MSG_ID_RACE_LOCAL_WRITE_CMD_IND:
            race_send_cache(&msg_queue_data);
            break;
        case MSG_ID_RACE_LOCAL_URC_NOTIFY_IND:
            break;
        case MSG_ID_RACE_LOCAL_RSP_NOTIFY_IND:
            race_handle_command(&msg_queue_data);
            break;

        case MSG_ID_RACE_LOCAL_DELAY_NOTI_IND:
        {
            race_noti_delay_msg_process(&msg_queue_data);
            break;
        }

        case MSG_ID_RACE_LOCAL_EVENT_NOTIFY_REQ:
        {
            race_event_notify_msg_process(&msg_queue_data);
            break;
        }

#ifdef RACE_STORAGE_CMD_ENABLE
        case MSG_ID_RACE_LOCAL_ERASE_PARTITION_CONTINUE_IND:
        {
            race_storage_erase_partition_continue_msg_process(&msg_queue_data);
            break;
        }

        case MSG_ID_RACE_LOCAL_STORAGE_SHA256_GENERATE_CONTINUE_IND:
        {
            race_storage_nb_sha256_generate_continue_msg_process((void *)msg_queue_data.msg_data);
            break;
        }
#endif

#ifdef RACE_BT_EVENT_MSG_HDL
        case MSG_ID_RACE_LOCAL_BT_EVENT_IND:
        {
            race_bt_event_ind_msg_process((race_bt_event_msg_info_struct *)msg_queue_data.msg_data);
            race_mem_free(msg_queue_data.msg_data);
            break;
        }
#endif

#ifdef RACE_LPCOMM_ENABLE
        case MSG_ID_RACE_LOCAL_LPCOMM_DATA_RECV_IND:
        {
            race_lpcomm_data_recv_msg_process(&msg_queue_data);
            race_mem_free(msg_queue_data.msg_data);
            break;
        }

        case MSG_ID_RACE_LOCAL_TIMER_EXPIRATION_IND:
        {
            race_timer_expiration_msg_process();
            break;
        }
#endif

#ifdef RACE_FOTA_CMD_ENABLE
        case MSG_ID_RACE_LOCAL_FOTA_STOP_IND:
        {
            race_fota_stop_msg_process(&msg_queue_data);
            if (msg_queue_data.msg_data)
            {
                race_mem_free(msg_queue_data.msg_data);
            }
            break;
        }
#endif

#ifdef RACE_RELAY_CMD_ENABLE
        case MSG_ID_RACE_LOCAL_RELAY_RACE_CMD:
        {
            race_cmd_relay_aws_mce_msg_process(&msg_queue_data);
            break;
        }
#endif
        case MSG_ID_RACE_LOCAL_GET_RSSI_CMD:{
#ifdef RACE_BT_CMD_ENABLE
            race_bt_notify_rssi();
#endif
            break;
        }
#ifdef MTK_ONLINE_LOG_TO_APK_ENABLE
        case MSG_ID_RACE_LOCAL_SEND_LOG_IND:
            race_send_online_Log_data(&msg_queue_data);
            break;
#endif
        default:
            break;
        }

    }
}

void race_task(void *arg)
{
    while(1)
    {
        race_processing();
    }
}


#ifdef RACE_CREATE_TASK_DYNAMICALLY
bool race_is_task_existed(void)
{
    return g_race_task_hdl != NULL;
}


void race_create_task()
{
#ifdef MTK_AUDIO_TUNING_ENABLED
#define RACE_TASK_STACKSIZE         (1506) /*unit byte!*/
#else
#define RACE_TASK_STACKSIZE         (512) /*unit byte!*/
#endif

#define RACE_TASK_PRIO              (TASK_PRIORITY_NORMAL)
#define RACE_TASK_NAME              ("race command")

    RACE_LOG_MSGID_I("xCreate task : race command, pri %d\r\n", 1, RACE_TASK_PRIO);

    BaseType_t ret = xTaskCreate(race_task,
                    RACE_TASK_NAME,
                    RACE_TASK_STACKSIZE,
                    NULL,
                    RACE_TASK_PRIO,
                    &g_race_task_hdl);

    if (ret != pdPASS || !g_race_task_hdl) {
        RACE_LOG_MSGID_E("xCreate race task failed, x:%x\r\n",1, ret);
    } else {
        RACE_LOG_MSGID_I("xCreate race task succeeded\r\n", 0);
    }
}
#endif


#ifdef RACE_ROLE_HANDOVER_SERVICE_ENABLE
static bt_status_t race_rho_srv_allowed_callback(const bt_bd_addr_t *addr)
{
    /* Always PENDING for SPP/BLE/AIRUPDATE needs be disconnected. */
    return BT_STATUS_PENDING;
}


static void race_rho_srv_status_callback(const bt_bd_addr_t *addr,
                                                 bt_aws_mce_role_t role,
                                                 bt_role_handover_event_t event,
                                                 bt_status_t status)
{
    if (BT_ROLE_HANDOVER_START_IND == event)
    {
        race_send_event_notify_msg(RACE_EVENT_TYPE_BT_RHO_START, NULL);
    }
    else if (BT_ROLE_HANDOVER_PREPARE_REQ_IND == event)
    {
        /* All allow RHO */
        race_send_event_notify_msg(RACE_EVENT_TYPE_BT_RHO_PREPARE, NULL);
    }
    else if (BT_ROLE_HANDOVER_COMPLETE_IND == event)
    {
        RACE_LOG_MSGID_E("RHO result:%d",1, BT_STATUS_SUCCESS == status);
        race_event_send_bt_rho_result_event(BT_STATUS_SUCCESS == status);
    }
}
#endif


#ifdef MTK_RACE_EVENT_ID_ENABLE
RACE_ERRCODE race_event_cb(int32_t register_id, race_event_type_enum event_type, void *param, void *user_data)
#else
RACE_ERRCODE race_event_cb(race_event_type_enum event_type, void *param, void *user_data)
#endif
{
#ifdef MTK_RACE_EVENT_ID_ENABLE
    RACE_LOG_MSGID_I("register_id:%d event_type:%d param:%x user_data:%x",4, register_id, event_type, param, user_data);

    if (g_race_event_register_id != register_id)
    {
        RACE_LOG_MSGID_E("register_id does not match! register_id:%d, g_register_id:%d",2, register_id, g_race_event_register_id);
        return RACE_ERRCODE_PARAMETER_ERROR;
    }
#else
    RACE_LOG_MSGID_I("event_type:%d param:%x user_data:%x",3, event_type, param, user_data);
#endif

    switch (event_type)
    {
        case RACE_EVENT_TYPE_CONN_BLE_DISCONNECT:
        case RACE_EVENT_TYPE_CONN_SPP_DISCONNECT:
#ifdef MTK_AIRUPDATE_ENABLE
        case RACE_EVENT_TYPE_CONN_AIRUPDATE_DISCONNECT:
#endif
#ifdef MTK_IAP2_PROFILE_ENABLE
        case RACE_EVENT_TYPE_CONN_IAP2_DISCONNECT:
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
        case RACE_EVENT_TYPE_CONN_GATT_OVER_BREDR_DISCONNECT:
#endif
        {
            if (RACE_EVENT_TYPE_CONN_BLE_DISCONNECT == event_type)
            {
                ble_msg_item.isNewRaceCmd = TRUE;
                if (ble_msg_item.msg_data)
                {
                    race_mem_free(ble_msg_item.msg_data);
                    ble_msg_item.msg_data = NULL;
                }
            }

            /* Normally, only Agent will receive BLE_DISC, SPP_DISC events. */
            if (RACE_EVENT_TYPE_CONN_SPP_DISCONNECT == event_type)
            {
                spp_msg_item.isNewRaceCmd = TRUE;
                if (spp_msg_item.msg_data)
                {
                    race_mem_free(spp_msg_item.msg_data);
                    spp_msg_item.msg_data = NULL;
                }
            }
#ifdef MTK_AIRUPDATE_ENABLE
            if (RACE_EVENT_TYPE_CONN_AIRUPDATE_DISCONNECT == event_type)
            {
                airupdate_msg_item.isNewRaceCmd = TRUE;
                if (airupdate_msg_item.msg_data)
                {
                    race_mem_free(airupdate_msg_item.msg_data);
                    airupdate_msg_item.msg_data = NULL;
                }
            }
#endif
#ifdef MTK_IAP2_PROFILE_ENABLE
            if (RACE_EVENT_TYPE_CONN_IAP2_DISCONNECT == event_type)
            {
                iap2_msg_item.isNewRaceCmd = TRUE;
                if (iap2_msg_item.msg_data)
                {
                    race_mem_free(iap2_msg_item.msg_data);
                    iap2_msg_item.msg_data = NULL;
                }
            }
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
            if (RACE_EVENT_TYPE_CONN_GATT_OVER_BREDR_DISCONNECT == event_type)
            {
                gatt_over_bredr_msg_item.isNewRaceCmd = TRUE;
                if (gatt_over_bredr_msg_item.msg_data)
                {
                    race_mem_free(gatt_over_bredr_msg_item.msg_data);
                    gatt_over_bredr_msg_item.msg_data = NULL;
                }
            }
#endif
            break;
        }

        case RACE_EVENT_TYPE_CONN_BLE_CONNECT:
        case RACE_EVENT_TYPE_CONN_SPP_CONNECT:
#ifdef MTK_AIRUPDATE_ENABLE
        case RACE_EVENT_TYPE_CONN_AIRUPDATE_CONNECT:
#endif
#ifdef MTK_IAP2_PROFILE_ENABLE
        case RACE_EVENT_TYPE_CONN_IAP2_CONNECT:
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
        case RACE_EVENT_TYPE_CONN_GATT_OVER_BREDR_CONNECT:
#endif
        {
#ifdef RACE_FIND_ME_ENABLE
#ifdef RACE_ROLE_HANDOVER_SERVICE_ENABLE
            race_cmd_set_find_me_trans_method(event_type);
#endif
#endif
            break;
        }

#ifdef RACE_AWS_ENABLE
#ifdef RACE_ROLE_HANDOVER_SERVICE_ENABLE
        case RACE_EVENT_TYPE_BT_RHO_START:
        {
            /* Warning: When this event is received by Partner before RHO, RHO may have already been done.
                        * This is because BT task's priority is much higher than race task's. Need add mutex protect for
                        * race timer and stop the timer in race_rho_srv_status_callback(). However, there's no problem
                        * currently because the timer's timeout value is long enough.
                        */
            /* Start RHO timer when processing Role_Switch cmd. Stop RHO timer on receiving RHO_SRV's START event,
                        * because if RHO START event is received, RHO END will be received for sure.
                        */
            race_timer_smart_stop(race_lpcomm_get_rho_timer_id());
            race_lpcomm_set_rho_timer_id(RACE_TIMER_INVALID_TIMER_ID);
            break;
        }

        /* Only Agent will receive RHO_PREPARE Event. */
        case RACE_EVENT_TYPE_BT_RHO_PREPARE:
        {
#ifndef RACE_RHO_WITHOUT_SMARTPHONE_DISCONNECT_ENABLE
            /* When the serial port is closed, DISCONNECT event triggered by BT_AIR module will not
                        * be received. Therefore, send DISCONNECT event here.
                        */
            race_serial_port_close(RACE_SERIAL_PORT_TYPE_SPP);
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_SPP_DISCONNECT, NULL);

            race_serial_port_close(RACE_SERIAL_PORT_TYPE_BLE);
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_BLE_DISCONNECT, NULL);

#ifdef MTK_AIRUPDATE_ENABLE
            race_serial_port_close(RACE_SERIAL_PORT_TYPE_AIRUPDATE);
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_AIRUPDATE_DISCONNECT, NULL);
#endif
#ifdef MTK_IAP2_PROFILE_ENABLE
            race_serial_port_close(RACE_SERIAL_PORT_TYPE_IAP2);
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_IAP2_DISCONNECT, NULL);
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
            race_serial_port_close(RACE_SERIAL_PORT_TYPE_GATT_OVER_BREDR);
            race_send_event_notify_msg(RACE_EVENT_TYPE_CONN_GATT_OVER_BREDR_DISCONNECT, NULL);
#endif
#endif
            bt_role_handover_reply_prepare_request(BT_ROLE_HANDOVER_MODULE_RACE_CMD);
            break;
        }
#endif

        case RACE_EVENT_TYPE_BT_RHO_RESULT:
        {
            race_lpcomm_set_role_switch_enable(FALSE);
#ifndef RACE_RHO_WITHOUT_SMARTPHONE_DISCONNECT_ENABLE
            /* Only when RACE_AWS_ENABLE is defined, will this event be received. */
#ifdef RACE_ROLE_HANDOVER_SERVICE_ENABLE
            RACE_LOG_MSGID_I("open ports", 0);
            race_serial_port_open(RACE_SERIAL_PORT_TYPE_SPP,
                                  SERIAL_PORT_DEV_BT_SPP);
            race_serial_port_open(RACE_SERIAL_PORT_TYPE_BLE,
                                  SERIAL_PORT_DEV_BT_LE);
#ifdef MTK_AIRUPDATE_ENABLE
            race_serial_port_open(RACE_SERIAL_PORT_TYPE_AIRUPDATE,
                                  SERIAL_PORT_DEV_BT_AIRUPDATE);
#endif
#ifdef MTK_IAP2_PROFILE_ENABLE
            race_serial_port_open(RACE_SERIAL_PORT_TYPE_IAP2,
                                  SERIAL_PORT_DEV_IAP2_SESSION2);
#endif
#ifdef MTK_GATT_OVER_BREDR_ENABLE
            race_serial_port_open(RACE_SERIAL_PORT_TYPE_GATT_OVER_BREDR,
                                  SERIAL_PORT_DEV_BT_GATT_OVER_BREDR);
#endif

#endif
#endif
            break;
        }
#endif

        default:
            break;
    }

    return RACE_ERRCODE_SUCCESS;
}


void race_init (void)
{
    int32_t ret = 0;
#ifdef RACE_ROLE_HANDOVER_SERVICE_ENABLE
    bt_role_handover_callbacks_t rho_srv_callbacks = {race_rho_srv_allowed_callback,
                                                              NULL,
                                                              NULL,
                                                              NULL,
                                                              race_rho_srv_status_callback};
#endif

    RACE_LOG_MSGID_I("race_init", 0);

#ifdef RACE_BT_CMD_ENABLE
    /* Listen to BT Events */
    ret = bt_callback_manager_register_callback(bt_callback_type_app_event,
                                                MODULE_MASK_SYSTEM | MODULE_MASK_GAP | MODULE_MASK_HFP | MODULE_MASK_A2DP,
#ifdef RACE_BT_EVENT_MSG_HDL
                                                (void*)race_bt_app_event_handler);
#else
                                                (void*)race_bt_event_process);
#endif

    if (BT_STATUS_SUCCESS != ret)
    {
        RACE_LOG_MSGID_E("BT events register failed. ret:%d",1, ret);
        return;
    }

#ifdef RACE_ROLE_HANDOVER_SERVICE_ENABLE
    bt_role_handover_register_callbacks(BT_ROLE_HANDOVER_MODULE_RACE_CMD, &rho_srv_callbacks);
#endif

    /* BT Init */
    race_bt_init();
#endif /* RACE_BT_CMD_ENABLE */

#ifdef RACE_LPCOMM_ENABLE
    race_lpcomm_ps_list_init();

#ifdef RACE_LPCOMM_RETRY_ENABLE
    race_lpcomm_retry_init();
#endif
#endif

    race_event_init();
#ifdef MTK_RACE_EVENT_ID_ENABLE
    race_event_register(&g_race_event_register_id, race_event_cb, NULL);
#else
    race_event_register(race_event_cb, NULL);
#endif

#ifdef RACE_FOTA_CMD_ENABLE
    race_fota_init();
#endif

#if defined(MTK_RACE_CMD_ENABLE) && defined(MTK_PORT_SERVICE_ENABLE)
    serial_port_dev_t race_port;
#if (PRODUCT_VERSION != 2822) && !defined(AB156X)
    serial_port_setting_uart_t race_uart_setting;
    if(serial_port_config_read_dev_number("race", &race_port) != SERIAL_PORT_STATUS_OK)
    {
        race_port = CONFIG_RACE_PORT;
        serial_port_config_write_dev_number("race", race_port);
        LOG_MSGID_W(common, "serial_port_config_write_dev_number setting uart", 0);
        race_uart_setting.baudrate = CONFIG_RACE_BAUDRATE;
        serial_port_config_write_dev_setting(race_port, (serial_port_dev_setting_t *)&race_uart_setting);
    }
#else
    race_port = CONFIG_SYSLOG_RUNNING_STAGE_PORT;
#endif
    g_race_uart_port_default = race_port;
    race_init_port_service(race_port);
#else
    race_init_port_service(CONFIG_RACE_PORT);
#endif
}

#endif

