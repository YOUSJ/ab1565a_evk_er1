/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */


#include "race_cmd_feature.h"
#ifdef RACE_NVDM_CMD_ENABLE
#include "FreeRTOS.h"
#include "task.h"
#include "serial_port.h"
#include "hal.h"
#include "nvdm.h"
#include "nvdm_internal.h"
#include "atci.h"
#include "atci_main.h"
#include "race_xport.h"
#include "race_cmd_nvdm.h"
#include "race_event_internal.h"
#include "race_util.h"
#include "serial_port_assignment.h"
#include "bt_system.h"
#include "nvkey.h"
#include "nvkey_id_list.h"
#include "bt_connection_manager.h"
#include "bt_connection_manager_internal.h"
#include "bt_callback_manager.h"
#include "hal_sleep_manager.h"
#include "at_command_bt.h"
#include "bt_sink_srv_ami.h"

////////////////////////////////////////////////////////////////////////////////
// Constant Definitions ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#define AUDIO_NVDM_GROUP_NAME "AB15"
#define AUDIO_NVDM_DATA_ITEM_NAME_SIZE 2


////////////////////////////////////////////////////////////////////////////////
// Global Variables ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
enum
{
    RACE_SWITCH_TO_AT,
    RACE_SWITCH_TO_RELAY,
    RACE_SWITCH_TO_LOG,
    RACE_SWITCH_TO_HCI,
};


////////////////////////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
nvdm_status_t Write_NVDM_AB15_NUM(uint16_t id, uint8_t *data, uint32_t size)
{
    char hex[5];
    snprintf(hex, sizeof(hex), "%X", id);
    return nvdm_write_data_item("AB15", hex, NVDM_DATA_ITEM_TYPE_RAW_DATA, data, size);
}


nvdm_status_t Read_NVDM_AB15_NUM(uint16_t id, const uint8_t *data, uint32_t *size)
{
    char hex[5];
    snprintf(hex, sizeof(hex), "%X", id);
    return nvdm_read_data_item("AB15", hex, (uint8_t*) data, size);
}

nvdm_status_t Read_NVDM_AB15_NUM_Length(uint16_t id, uint32_t *size)
{
    char hex[5];
    snprintf(hex, sizeof(hex), "%X", id);
    return nvdm_query_data_item_length("AB15", hex, size);
}

void* RACE_NVKEY_READFULLKEY_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    RACE_LOG_MSGID_I("RACE_NVKEY_READFULLKEY_HDR() enter, channel_id = %x \r\n",1, channel_id);

    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint16_t NVKEY_ID;
        uint16_t length_of_read_bytes;
    }PACKED RACE_NVKEY_READFULLKEY_CMD_STRU;

    typedef struct
    {
        uint16_t length_of_read_bytes;
        uint8_t ReadData[0];
    }PACKED RACE_NVKEY_READFULLKEY_EVT_STRU;

    uint32_t        size;
    nvdm_status_t status;
    RACE_NVKEY_READFULLKEY_EVT_STRU* pEvt = NULL;

    RACE_NVKEY_READFULLKEY_CMD_STRU* pCmd = (RACE_NVKEY_READFULLKEY_CMD_STRU*)pCmdMsg;
    status = Read_NVDM_AB15_NUM_Length(pCmd->NVKEY_ID, &size);
    if (status == NVDM_STATUS_OK)
    {
        pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_NVKEY_READFULLKEY,
                                                                 (uint16_t)(sizeof(RACE_NVKEY_READFULLKEY_EVT_STRU) + size), channel_id);
        if (pEvt != NULL)
        {
            status = Read_NVDM_AB15_NUM(pCmd->NVKEY_ID, (const uint8_t*)pEvt->ReadData, &size);
            pEvt->length_of_read_bytes = size;
            if (status != NVDM_STATUS_OK)
            {
                RACE_LOG_MSGID_E("Read_NVDM_AB15_NUM() fail, status = %d \r\n",1, status);
            }
            else if(size > 128)
            {
                race_send_pkt_t* pSndPkt;
                uint32_t port_handle, ret_size;
                uint8_t *ptr;

                pSndPkt = race_pointer_cnv_pkt_to_send_pkt((void *)pEvt);
                port_handle = race_get_port_handle_by_channel_id(channel_id);
                ret_size = race_port_send_data(port_handle, (uint8_t*)&pSndPkt->race_data, pSndPkt->length);
                size = pSndPkt->length;
                size -= ret_size;
                ptr = (uint8_t*)&pSndPkt->race_data;
                ptr += ret_size;
                while(size > 0)
                {
                    ret_size = race_port_send_data(port_handle, ptr, size);
                    size -= ret_size;
                    ptr += ret_size;
                }
                race_mem_free(pSndPkt);
                return NULL;
            }
        }
    }
    else if (status == NVDM_STATUS_ITEM_NOT_FOUND)
    {
        pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_NVKEY_READFULLKEY,
                                                                 (uint16_t)(sizeof(RACE_NVKEY_READFULLKEY_EVT_STRU) + 1), channel_id);
        if (pEvt != NULL)
        {
            pEvt->length_of_read_bytes = 0;
            *((uint8_t *)pEvt->ReadData) = 0;
        }
        RACE_LOG_MSGID_E("Read_NVDM_AB15_NUM_Length() NVDM_STATUS_ITEM_NOT_FOUND add 1 byte zero, status = %d \r\n",1, status);
    }
    else
    {
        RACE_LOG_MSGID_E("Read_NVDM_AB15_NUM_Length() error, status = %d \r\n",1, status);
    }

    return pEvt;
}

void* RACE_NVKEY_READFULLKEY_RESP_NVID_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    RACE_LOG_MSGID_I("RACE_NVKEY_READFULLKEY_RESP_NVID_HDR() enter, channel_id = %x \r\n",1, channel_id);

    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint16_t NVKEY_ID;
        uint16_t length_of_read_bytes;
    }PACKED RACE_NVKEY_READFULLKEY_RESP_NVID_CMD_STRU;

    typedef struct
    {
        uint8_t ReturnCode;
        uint16_t NVKEY_ID;
        uint16_t length_of_read_bytes;
        uint8_t ReadData[0];
    }PACKED RACE_NVKEY_READFULLKEY_RESP_NVID_EVT_STRU;

    uint32_t        size;
    nvdm_status_t status;
    RACE_NVKEY_READFULLKEY_RESP_NVID_EVT_STRU* pEvt = NULL;

    RACE_NVKEY_READFULLKEY_RESP_NVID_CMD_STRU* pCmd = (RACE_NVKEY_READFULLKEY_RESP_NVID_CMD_STRU*)pCmdMsg;
    status = Read_NVDM_AB15_NUM_Length(pCmd->NVKEY_ID, &size);
    if (status == NVDM_STATUS_OK)
    {
        pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_NVKEY_READFULLKEY_RESP_NVID,
                                                                 (uint16_t)(sizeof(RACE_NVKEY_READFULLKEY_RESP_NVID_EVT_STRU) + size), channel_id);
        if (pEvt != NULL)
        {
            status = Read_NVDM_AB15_NUM(pCmd->NVKEY_ID, (const uint8_t*)pEvt->ReadData, &size);
            pEvt->length_of_read_bytes = size;
            pEvt->ReturnCode = 0;
            pEvt->NVKEY_ID = pCmd->NVKEY_ID;
            if (status != NVDM_STATUS_OK)
            {
                RACE_LOG_MSGID_E("Read_NVDM_AB15_NUM() fail, status = %d \r\n",1, status);
            }
            else if(size > 128)
            {
                race_send_pkt_t* pSndPkt;
                uint32_t port_handle, ret_size;
                uint8_t *ptr;

                pSndPkt = race_pointer_cnv_pkt_to_send_pkt((void *)pEvt);
                port_handle = race_get_port_handle_by_channel_id(channel_id);
                ret_size = race_port_send_data(port_handle, (uint8_t*)&pSndPkt->race_data, pSndPkt->length);
                size = pSndPkt->length;
                size -= ret_size;
                ptr = (uint8_t*)&pSndPkt->race_data;
                ptr += ret_size;
                while(size > 0)
                {
                    ret_size = race_port_send_data(port_handle, ptr, size);
                    size -= ret_size;
                    ptr += ret_size;
                }
                race_mem_free(pSndPkt);
                return NULL;
            }
        }
    }
    else if (status == NVDM_STATUS_ITEM_NOT_FOUND)
    {
        pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_NVKEY_READFULLKEY_RESP_NVID,
                                                                 (uint16_t)(sizeof(RACE_NVKEY_READFULLKEY_RESP_NVID_EVT_STRU) + 1), channel_id);
        if (pEvt != NULL)
        {
            pEvt->length_of_read_bytes = 0;
            pEvt->ReturnCode = 0;
            pEvt->NVKEY_ID = pCmd->NVKEY_ID;
            *((uint8_t *)pEvt->ReadData) = 0;
        }
        RACE_LOG_MSGID_E("Read_NVDM_AB15_NUM_Length() NVDM_STATUS_ITEM_NOT_FOUND add 1 byte zero, status = %d \r\n",1, status);
    }
    else
    {
        RACE_LOG_MSGID_E("Read_NVDM_AB15_NUM_Length() error, status = %d \r\n",1, status);
    }

    return pEvt;
}

void* RACE_NVKEY_WRITEFULLKEY_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    RACE_LOG_MSGID_I("RACE_NVKEY_WRITEFULLKEY_HDR() enter, channel_id = %x \r\n",1, channel_id);

    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint16_t NVKEY_ID;
        uint8_t WriteData[0];

    }PACKED RACE_NVKEY_WRITEFULLKEY_CMD_STRU;

    typedef struct
    {
        uint8_t return_code;
    }PACKED RACE_NVKEY_WRITEFULLKEY_EVT_STRU;

    uint32_t nvdm_length;
    nvdm_status_t status;
    uint32_t size;
    uint8_t* pData;
    uint32_t i;

    RACE_NVKEY_WRITEFULLKEY_CMD_STRU* pCmd = (RACE_NVKEY_WRITEFULLKEY_CMD_STRU*)pCmdMsg;
    RACE_NVKEY_WRITEFULLKEY_EVT_STRU* pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_NVKEY_WRITEFULLKEY, (uint16_t)(sizeof(RACE_NVKEY_WRITEFULLKEY_EVT_STRU)), channel_id);

    if (pEvt != NULL)
    {
        pEvt->return_code = RACE_ERRCODE_FAIL;
        nvdm_length = pCmd->Hdr.length - sizeof(pCmd->Hdr.id) - sizeof(pCmd->NVKEY_ID);

        if (nvdm_length > NVDM_DATA_LENGTH)
            nvdm_length = NVDM_DATA_LENGTH;

        RACE_LOG_MSGID_I("nvdm_length = %ld \r\n",1, nvdm_length);

        status = Write_NVDM_AB15_NUM(pCmd->NVKEY_ID, pCmd->WriteData, nvdm_length);
        if (status == NVDM_STATUS_OK)
        {
            RACE_LOG_MSGID_I("NVDM Write Done, Read NVDM and compare Data. \r\n", 0);

            Read_NVDM_AB15_NUM_Length(pCmd->NVKEY_ID, &size);
            pData = (uint8_t*)race_mem_alloc(size);
            status = Read_NVDM_AB15_NUM(pCmd->NVKEY_ID, (const uint8_t*)pData, &size);

            if (status == NVDM_STATUS_OK)
            {
                //compare_buffer(pData, pCmd->WriteData, size);
                for (i = 0; i < size; i++)
                {
                    //RACE_LOG_E("nvdm data are :  %x   %x  \r\n", pData[i], pCmd->WriteData[i]);
                    if (pData[i] != pCmd->WriteData[i])
                    {
                        RACE_LOG_MSGID_E("nvdm data different : %x != %x  \r\n",2, pData[i], pCmd->WriteData[i]);
                        status = NVDM_STATUS_ERROR;
                        break;
                    }
                }
                if (status == NVDM_STATUS_ERROR)
                    pEvt->return_code = RACE_ERRCODE_FAIL;
                else
                    pEvt->return_code = RACE_ERRCODE_SUCCESS;
            }
            race_mem_free(pData);

            #ifdef SUPPORT_PEQ_NVKEY_UPDATE
            aud_peq_chk_nvkey(pCmd->NVKEY_ID);
            #endif
        }
    }

    return pEvt;
}

void* RACE_NVKEY_WRITEFULLKEY_RESP_NVID_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    RACE_LOG_MSGID_I("RACE_NVKEY_WRITEFULLKEY_RESP_NVID_HDR() enter, channel_id = %x \r\n",1, channel_id);

    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint16_t NVKEY_ID;
        uint8_t WriteData[0];

    }PACKED RACE_NVKEY_WRITEFULLKEY_RESP_NVID_CMD_STRU;

    typedef struct
    {
        uint16_t NVKEY_ID;
        uint8_t return_code;
    }PACKED RACE_NVKEY_WRITEFULLKEY_RESP_NVID_EVT_STRU;

    uint32_t nvdm_length;
    nvdm_status_t status;
    uint32_t size;
    uint8_t* pData;
    uint32_t i;

    RACE_NVKEY_WRITEFULLKEY_RESP_NVID_CMD_STRU* pCmd = (RACE_NVKEY_WRITEFULLKEY_RESP_NVID_CMD_STRU*)pCmdMsg;
    RACE_NVKEY_WRITEFULLKEY_RESP_NVID_EVT_STRU* pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_NVKEY_WRITEFULLKEY_RESP_NVID, (uint16_t)(sizeof(RACE_NVKEY_WRITEFULLKEY_RESP_NVID_EVT_STRU)), channel_id);

    if (pEvt != NULL)
    {
        pEvt->NVKEY_ID = pCmd->NVKEY_ID;
        pEvt->return_code = RACE_ERRCODE_FAIL;
        nvdm_length = pCmd->Hdr.length - sizeof(pCmd->Hdr.id) - sizeof(pCmd->NVKEY_ID);

        if (nvdm_length > NVDM_DATA_LENGTH)
            nvdm_length = NVDM_DATA_LENGTH;

        RACE_LOG_MSGID_I("nvdm_length = %ld \r\n",1, nvdm_length);

        status = Write_NVDM_AB15_NUM(pCmd->NVKEY_ID, pCmd->WriteData, nvdm_length);
        if (status == NVDM_STATUS_OK)
        {
            RACE_LOG_MSGID_I("NVDM Write Done, Read NVDM and compare Data. \r\n", 0);

            Read_NVDM_AB15_NUM_Length(pCmd->NVKEY_ID, &size);
            pData = (uint8_t*)race_mem_alloc(size);
            status = Read_NVDM_AB15_NUM(pCmd->NVKEY_ID, (const uint8_t*)pData, &size);

            if (status == NVDM_STATUS_OK)
            {
                //compare_buffer(pData, pCmd->WriteData, size);
                for (i = 0; i < size; i++)
                {
                    //RACE_LOG_E("nvdm data are :  %x   %x  \r\n", pData[i], pCmd->WriteData[i]);
                    if (pData[i] != pCmd->WriteData[i])
                    {
                        RACE_LOG_MSGID_E("nvdm data different : %x != %x  \r\n",2, pData[i], pCmd->WriteData[i]);
                        status = NVDM_STATUS_ERROR;
                        break;
                    }
                }
                if (status == NVDM_STATUS_ERROR)
                    pEvt->return_code = RACE_ERRCODE_FAIL;
                else
                    pEvt->return_code = RACE_ERRCODE_SUCCESS;
            }
            race_mem_free(pData);

            #ifdef SUPPORT_PEQ_NVKEY_UPDATE
            aud_peq_chk_nvkey(pCmd->NVKEY_ID);
            #endif
        }
    }

    return pEvt;
}

#if 0
void* RACE_NVKEY_NEXT_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    RACE_LOG_MSGID_I("RACE_NVKEY_NEXT_HDR() enter, channel_id = %x \r\n",1, channel_id);

    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint16_t NVKEY_ID;
        uint16_t length_of_read_bytes;
    }PACKED RACE_NVKEY_READFULLKEY_CMD_STRU;

    typedef struct
    {
        uint16_t length_of_read_bytes;
        uint8_t ReadData[0];
    }PACKED RACE_NVKEY_READFULLKEY_EVT_STRU;

    uint32_t        size;


    RACE_NVKEY_READFULLKEY_CMD_STRU* pCmd = (RACE_NVKEY_READFULLKEY_CMD_STRU*)pCmdMsg;


    Read_NVDM_AB15_NUM_Length(pCmd->NVKEY_ID, &size);

    RACE_NVKEY_READFULLKEY_EVT_STRU* pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_NVKEY_READFULLKEY,
                                                             (uint16_t)(sizeof(RACE_NVKEY_READFULLKEY_EVT_STRU) + size), channel_id);


    if (pEvt != NULL)
    {
        nvdm_status_t status;
        uint32_t nvdm_length = pCmd->length_of_read_bytes;

        #if defined (MTK_ATCI_VIA_PORT_SERVICE) && defined(MTK_PORT_SERVICE_ENABLE)
            if (nvdm_length > SERIAL_PORT_RECEIVE_BUFFER_SIZE)
                nvdm_length = SERIAL_PORT_RECEIVE_BUFFER_SIZE;
        #else
            if (nvdm_length > 1024)
                nvdm_length = 1024;
        #endif

        RACE_LOG_MSGID_I("pCmd->NVKEY_ID = %ld, nvdm_length = %ld \r\n",2, pCmd->NVKEY_ID, nvdm_length);

        pEvt->length_of_read_bytes = nvdm_length;

        size = nvdm_length;
        status = Read_NVDM_AB15_NUM(pCmd->NVKEY_ID, (const uint8_t*)pEvt->ReadData, &size);
        pEvt->length_of_read_bytes = size;
        if (status != NVDM_STATUS_OK)
        {
            RACE_LOG_MSGID_E("Read_NVDM_AB15_NUM() fail, status = %d \r\n",1, status);
        }
    }

    return pEvt;
}
#endif

/**
 * RACE_NVKEY_RECLAIM_HDR
 *
 * RACE NVKEY RECLAIM Handler
 *
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 *
 */
void* RACE_NVKEY_RECLAIM_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    RACE_LOG_MSGID_I("RACE_NVKEY_RECLAIM_HDR() enter, channel_id = %x \r\n",1, channel_id);

    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint16_t minimum_space;
    }PACKED RACE_NVDM_RECLAIM_CMD_STRU;

    typedef struct
    {
        uint8_t Status;
    }PACKED RACE_NVDM_RECLAIM_EVT_STRU;

    RACE_NVDM_RECLAIM_CMD_STRU* pCmd = (RACE_NVDM_RECLAIM_CMD_STRU*)pCmdMsg;
    RACE_NVDM_RECLAIM_EVT_STRU* pEvt = NULL;

    pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_NVKEY_RECLAIM,
                            (uint16_t)(sizeof(RACE_NVDM_RECLAIM_EVT_STRU)), channel_id);
    if (pEvt != NULL)
    {
        uint32_t total_avail_space, curr_used_space;
        if( (nvdm_query_space_info(&total_avail_space, &curr_used_space) == NVDM_STATUS_OK)
            && (total_avail_space - curr_used_space > (uint32_t)pCmd->minimum_space) )
        {
            pEvt->Status = 1; //follow old proj, return 1 when success.
        }
        else
        {
            pEvt->Status = 0;
        }
    }

    return pEvt;
}

void* RACE_NVDM_GETALL_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    RACE_LOG_MSGID_I("RACE_NVDM_GETALL_HDR() enter, channel_id = %x \r\n",1, channel_id);

    char group_name[4], data_item_name[4];
    bool found_audio_group = false;
    nvdm_status_t ret;
    uint32_t data_item_count, tmp_data;
    uint8_t idx = 0;
    uint16_t length_NVDM_data;

    typedef struct
    {
        uint8_t Status;
        uint16_t NumNvdm;
        uint16_t NvdmData[0];
    }PACKED RACE_NVDM_GETALL_EVT_STRU;

    RACE_NVDM_GETALL_EVT_STRU* pEvt = NULL;

    /* Below flow will be done when CM4 receives race command Frist time. */
    ret = nvdm_query_begin();
    if (ret != NVDM_STATUS_OK)
    {
        RACE_LOG_MSGID_I("nvdm_query_begin fail, ret = 0x%x\r\n", 1, ret);
        return NULL;
    }

    ret = nvdm_query_next_group_name(group_name);
    while(ret == NVDM_STATUS_OK)
    {
        if (!strcmp(group_name, AUDIO_NVDM_GROUP_NAME))
        {
            RACE_LOG_MSGID_I("Compare Group Correct\r\n", 0);
            found_audio_group = true;
            break;
        }
        else
        {
            ret = nvdm_query_next_group_name(group_name);
            if (ret != NVDM_STATUS_OK)
            {
                RACE_LOG_MSGID_I("nvdm_query_next_group_name fail, ret = 0x%x\r\n", 1, ret);
                return NULL;
            }
        }
    }

    if (found_audio_group == false)
    {
        RACE_LOG_MSGID_I("Compare Group Error\r\n", 0);
        pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_NVDM_GETALL,
                                (uint16_t)(sizeof(RACE_NVDM_GETALL_EVT_STRU)), channel_id);
        if (pEvt != NULL)
        {
            pEvt->Status = RACE_ERRCODE_FAIL;
            pEvt->NumNvdm = (uint16_t)0;
        }
    }
    else
    {
        /* query count of data item belong to specific group */
        nvdm_query_data_item_count(group_name, (uint32_t *)&data_item_count);
        length_NVDM_data = data_item_count * AUDIO_NVDM_DATA_ITEM_NAME_SIZE;

        pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_NVDM_GETALL,
                                (uint16_t)(sizeof(RACE_NVDM_GETALL_EVT_STRU) + length_NVDM_data), channel_id);
        if (pEvt != NULL)
        {
            pEvt->Status = RACE_ERRCODE_SUCCESS;
            pEvt->NumNvdm = (uint16_t)data_item_count;

            RACE_LOG_MSGID_I("data_item_count = %d, length_NVDM_data = %d \r\n",2, data_item_count, length_NVDM_data);
            /* read name one by one and save to buffer */
            while(nvdm_query_next_data_item_name(data_item_name) == NVDM_STATUS_OK)
            {
                char *tmp = data_item_name;
                sscanf(tmp, "%x", (unsigned int *)&tmp_data);
                //RACE_LOG_I("data_item_name value = %x \r\n", tmp_data);

                pEvt->NvdmData[idx] = (uint16_t)tmp_data;
                idx++;
            }
            RACE_LOG_MSGID_I("NVDM data = %x, %x, %x \r\n",3, pEvt->NvdmData[0], pEvt->NvdmData[1], pEvt->NvdmData[2]);
        }
    }
    ret = nvdm_query_end();
    if (ret != NVDM_STATUS_OK)
    {
        RACE_LOG_MSGID_I("nvdm_query_end fail, ret = 0x%x\r\n", 1, ret);
    }

    return pEvt;
}
#if ((PRODUCT_VERSION == 1552) || defined(AM255X))
extern bool bt_driver_enter_relay_mode(uint8_t port);
extern void bt_driver_relay_register_callbacks(void *callback);
extern atci_bt_relay_callbacks atci_bt_relay_cb;
#if _MSC_VER >= 1500
    #pragma comment(linker, "/alternatename:_bt_driver_enter_relay_mode=_bt_driver_enter_relay_mode_ext")
    #pragma comment(linker, "/alternatename:_bt_driver_relay_register_callbacks=_bt_driver_relay_register_callbacks_ext")

#elif defined(__GNUC__) || defined(__ICCARM__) || defined(__ARMCC_VERSION)
    #pragma weak bt_driver_enter_relay_mode = bt_driver_enter_relay_mode_ext
    #pragma weak bt_driver_relay_register_callbacks = bt_driver_relay_register_callbacks_ext
#else
    #error "Unsupported Platform"
#endif

bool bt_driver_enter_relay_mode_ext(uint8_t port)
{
    return false;
}

void bt_driver_relay_register_callbacks_ext(void *callback)
{
    return;
}

static void RACE_NVDM_ENTER_RELAY_WITH_TX_CONFIG()
{
    uint32_t tx_config_size = sizeof(bt_config_tx_power_ext1_t);
    bt_config_tx_power_ext1_t tx_cfg = {
        .bdr_init_tx_power_level = 7,
        .reserved = 0,
        .fixed_tx_power_enable = 0,
        .fixed_tx_power_level = 0,
        .le_init_tx_power_level = 7,
        .bt_max_tx_power_level = 7,
        .bdr_tx_power_level_offset = 1,
        .bdr_fine_tx_power_level_offset = 0,
        .edr_tx_power_level_offset = 1,
        .edr_fine_tx_power_level_offset = 0,
        .ble_tx_power_level_offset = 1,
        .ble_fine_tx_power_level_offset = 0,
        .edr_init_tx_power_level = 7
    };
    nvkey_status_t nvkey_ret = nvkey_read_data(NVKEYID_BT_DEFAULT_TX_POWER_EXTEND, (uint8_t *)(&tx_cfg), &tx_config_size);
    RACE_LOG_MSGID_I("read nvkey of BT default tx power extend, ret:%d\r\n", 1, nvkey_ret);
    bt_status_t status = bt_config_tx_power_level_ext1(&tx_cfg);
    RACE_LOG_MSGID_I("status:0x%08x, bdr_init_tx:%d, bt_max_tx:%d, le_init_tx:%d, fixed_tx_enable:%d, fixed_tx:%d, bdr_offset:%d, bdr_fine_offset:%d, edr_offset:%d, edr_fine_offset:%d, ble_offset:%d, ble_fine_offset:%d, edr_init_tx:%d",
    13, status, tx_cfg.bdr_init_tx_power_level, tx_cfg.bt_max_tx_power_level,
    tx_cfg.le_init_tx_power_level,tx_cfg.fixed_tx_power_enable, tx_cfg.fixed_tx_power_level,
    tx_cfg.bdr_tx_power_level_offset, tx_cfg.bdr_fine_tx_power_level_offset,
    tx_cfg.edr_tx_power_level_offset, tx_cfg.edr_fine_tx_power_level_offset,
    tx_cfg.ble_tx_power_level_offset, tx_cfg.ble_fine_tx_power_level_offset,
    tx_cfg.edr_init_tx_power_level);
    uint8_t bt_sleep_manager_handle = hal_sleep_manager_set_sleep_handle("bt");
    hal_sleep_manager_lock_sleep(bt_sleep_manager_handle);
    bt_driver_relay_register_callbacks((void *)&atci_bt_relay_cb);
    bool ret = bt_driver_enter_relay_mode(g_race_uart_port);
    RACE_LOG_MSGID_I("enter relay mode result: 0x%x, port:%d\r\n",2, ret, g_race_uart_port);
}

static bt_status_t RACE_NVDM_BT_GAP_EVENT_CALLBACK(bt_msg_type_t msg, bt_status_t status, void *buff)
{
    switch (msg) {
        case BT_POWER_ON_CNF: {
            RACE_LOG_MSGID_I("BT_POWER_ON_CNF\r\n", 0);
            bt_sink_srv_send_action(BT_SINK_SRV_ACTION_BT_STANDBY, NULL);
        }
        break;

        case BT_POWER_OFF_CNF: {
            RACE_LOG_MSGID_I("BT_POWER_OFF_CNF\r\n", 0);
            RACE_NVDM_ENTER_RELAY_WITH_TX_CONFIG();
        }
        break;
    }
    return BT_STATUS_SUCCESS;
}
#endif

void* RACE_SWITCH_FUNC_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    RACE_LOG_MSGID_I("RACE_SWITCH_FUNC_HDR() enter, channel_id = %x",1, channel_id);

    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint8_t SwitchFunc;

    }PACKED RACE_SWITCH_FUNC_CMD_STRU;

    typedef struct
    {
        uint8_t return_code;
    }PACKED RACE_SWITCH_FUNC_EVT_STRU;

    RACE_SWITCH_FUNC_CMD_STRU* pCmd = (RACE_SWITCH_FUNC_CMD_STRU*)pCmdMsg;

    if (pCmd != NULL)
    {
        switch (pCmd->SwitchFunc)
        {
            case RACE_SWITCH_TO_AT :
            {
#if defined(MTK_MUX_ENABLE)
                RACE_LOG_MSGID_I("RACE_SWITCH_FUNC_HDR, no support MUX",0);
                return NULL;
#else
                RACE_SWITCH_FUNC_EVT_STRU* pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_SWITCH_FUNC, (uint16_t)(sizeof(RACE_SWITCH_FUNC_EVT_STRU)), channel_id);

                if (pEvt != NULL)
                {
                    pEvt->return_code = RACE_STATUS_OK;
                    race_flush_packet((uint8_t *)pEvt, channel_id);
                }
                race_status_t sta = race_uart_deinit();
                if (sta != RACE_STATUS_OK)
                {}
                RACE_LOG_MSGID_I("RACE switch to AT. result = %d, port = %d, default port = %d",3, sta, g_race_uart_port, g_race_uart_port_default);

                if (g_race_uart_port == g_race_uart_port_default)
                {
                    atci_status_t sta = atci_deinit_keep_table(g_atci_uart_port_default);
                    if (sta != ATCI_STATUS_OK)
                    {}

                #if defined(MTK_ATCI_VIA_PORT_SERVICE) && defined(MTK_PORT_SERVICE_ENABLE)
                    serial_port_dev_t port;
                    port = g_race_uart_port_default;
                    atci_init(port);
                #else
                    atci_init(g_race_uart_port_default);
                #endif

                }
                else
                {
                #if defined(MTK_ATCI_VIA_PORT_SERVICE) && defined(MTK_PORT_SERVICE_ENABLE)
                    serial_port_dev_t port;
                    serial_port_setting_uart_t uart_setting;

                    if(serial_port_config_read_dev_number("atci", &port) != SERIAL_PORT_STATUS_OK)
                    {
                        port = CONFIG_ATCI_PORT;
                        serial_port_config_write_dev_number("atci", port);
                        RACE_LOG_MSGID_I("serial_port_config_write_dev_number setting uart1", 0);
                        uart_setting.baudrate = HAL_UART_BAUDRATE_921600;
                        serial_port_config_write_dev_setting(port, (serial_port_dev_setting_t *)&uart_setting);
                    }
                    atci_init(port);
                #else
                    atci_init(CONFIG_ATCI_PORT);
                #endif
                #if defined(MTK_RACE_CMD_ENABLE) && defined(MTK_PORT_SERVICE_ENABLE)
                    serial_port_dev_t race_port;
                    serial_port_setting_uart_t race_uart_setting;

                    if(serial_port_config_read_dev_number("race", &race_port) != SERIAL_PORT_STATUS_OK)
                    {
                        race_port = CONFIG_RACE_PORT;
                        serial_port_config_write_dev_number("race", race_port);
                        RACE_LOG_MSGID_I("serial_port_config_write_dev_number setting uart1", 0);
                        race_uart_setting.baudrate = HAL_UART_BAUDRATE_921600;
                        serial_port_config_write_dev_setting(race_port, (serial_port_dev_setting_t *)&race_uart_setting);
                    }
                    race_serial_port_uart_init(race_port);
                #else
                    race_serial_port_uart_init(CONFIG_RACE_PORT);
                #endif
                }
#endif
            }
            break;

            case RACE_SWITCH_TO_LOG :
            {

            }
            break;

            case RACE_SWITCH_TO_HCI :
            {

            }
            case RACE_SWITCH_TO_RELAY :
            {
#if ((PRODUCT_VERSION == 1552) || defined(AM255X))
                RACE_LOG_MSGID_I("RACE_SWITCH_TO_RELAY", 0);
                RACE_SWITCH_FUNC_EVT_STRU* pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_SWITCH_FUNC, (uint16_t)(sizeof(RACE_SWITCH_FUNC_EVT_STRU)), channel_id);

                if (pEvt != NULL)
                {
                    pEvt->return_code = RACE_STATUS_OK;
                    race_flush_packet((uint8_t *)pEvt, channel_id);
                }

                race_status_t sta = race_uart_deinit();
                bt_cm_power_state_t power_state = bt_connection_manager_power_get_state();
                if(sta != RACE_STATUS_OK) {
                    RACE_LOG_MSGID_I("Deinit race fail", 0);
                }
                if(BT_CM_POWER_STATE_ON == power_state || BT_CM_POWER_STATE_ON_PENDING == power_state
                    || BT_CM_POWER_STATE_OFF_PENDING == power_state) {
                    bt_gap_ata_callback_register((void *)RACE_NVDM_BT_GAP_EVENT_CALLBACK);
                    if(BT_CM_POWER_STATE_ON == power_state) {
                        bt_sink_srv_send_action(BT_SINK_SRV_ACTION_BT_STANDBY, NULL);
                    }
                } else {
                    RACE_NVDM_ENTER_RELAY_WITH_TX_CONFIG();
                }
#endif
            }
            break;

            default:
            {
                while(1);
            }
            break;
        }
    }
    return NULL;
}

/**
 * race_reload_nvdm_to_ram
 *
 * Notify module to reload data from NDVM to RAM
 *
 * @p_cmd_msg : pointer of ptr_race_pkt_t
 *
 */
static void* race_reload_nvdm_to_ram(ptr_race_pkt_t p_cmd_msg, uint8_t channel_id)
{
    typedef struct {
        uint8_t status;
    } PACKED race_response_t;

    typedef struct {
        RACE_COMMON_HDR_STRU Hdr;
        uint16_t nvkey_id;
    } PACKED race_cmd_t;

    race_response_t *p_response = RACE_ClaimPacket(RACE_TYPE_RESPONSE, RACE_RELOAD_NVKEY_TO_RAM, sizeof(race_response_t), channel_id);
    if (p_response == NULL) {
        RACE_LOG_MSGID_E("response alloc fail", 0);
        return NULL;
    }

    race_cmd_t* cmd = (race_cmd_t*)p_cmd_msg;

    uint16_t *p_nvkey_id = (uint16_t *)race_mem_alloc(sizeof(uint16_t));
    if (p_nvkey_id) {
        *p_nvkey_id = cmd->nvkey_id;
        p_response->status = race_send_event_notify_msg(RACE_EVENT_RELOAD_NVKEY_TO_RAM, (void *)p_nvkey_id);
    } else {
        RACE_LOG_MSGID_E("Cannot malloc p_nvkey_id\r\n", 0);
        p_response->status = RACE_ERRCODE_NOT_ENOUGH_MEMORY;
    }

    return p_response;
}

void* RACE_CmdHandler_NVDM(ptr_race_pkt_t pRaceHeaderCmd, uint16_t length, uint8_t channel_id)
{
    void* ptr = NULL;

    RACE_LOG_MSGID_I("RACE_CmdHandler_NVDM() enter, pRaceHeaderCmd->hdr.id = %d \r\n",1, (int)pRaceHeaderCmd->hdr.id);

    switch (pRaceHeaderCmd->hdr.id)
    {
        case RACE_NVKEY_READFULLKEY :
        {
            ptr = RACE_NVKEY_READFULLKEY_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_NVKEY_WRITEFULLKEY :
        {
            ptr = RACE_NVKEY_WRITEFULLKEY_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_NVKEY_NEXT :
        {
            //ptr = RACE_NVKEY_NEXT_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_NVKEY_RECLAIM :
        {
            ptr = RACE_NVKEY_RECLAIM_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_NVKEY_READFULLKEY_RESP_NVID :
        {
            ptr = RACE_NVKEY_READFULLKEY_RESP_NVID_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_NVKEY_WRITEFULLKEY_RESP_NVID :
        {
            ptr = RACE_NVKEY_WRITEFULLKEY_RESP_NVID_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_NVDM_GETALL :
        {
            ptr = RACE_NVDM_GETALL_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_SWITCH_FUNC :
        {
            RACE_LOG_MSGID_I("RACE_CmdHandler_NVDM() enter RACE_SWITCH_FUNC\r\n", 0);
            ptr = RACE_SWITCH_FUNC_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_RELOAD_NVKEY_TO_RAM :
        {
            RACE_LOG_MSGID_I("RACE_RELOAD_NVKEY_TO_RAM\r\n", 0);
            ptr = race_reload_nvdm_to_ram(pRaceHeaderCmd, channel_id);
        }
        break;

        default:
        {
            while(1);
        }
        break;
    }

    return ptr;
}


#endif /* RACE_NVDM_CMD_ENABLE */

