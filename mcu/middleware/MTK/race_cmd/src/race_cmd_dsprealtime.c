/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */


#include "race_cmd_feature.h"
#ifdef RACE_DSP_REALTIME_CMD_ENABLE
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "hal.h"
#include "race_cmd.h"
#include "race_cmd_dsprealtime.h"
#include "race_xport.h"
#include "race_noti.h"
#if defined(HAL_AUDIO_MODULE_ENABLED)
#include "hal_ccni.h"
#include "memory_attribute.h"
#include "hal_audio_cm4_dsp_message.h"
#include "hal_audio_message_struct.h"
#include "hal_audio_internal.h"
#include "bt_sink_srv_ami.h"
#include "bt_sink_srv_common.h"
#include "bt_connection_manager_internal.h"
#include "hal_resource_assignment.h"
#endif
#ifndef MTK_ANC_V2
#include "at_command_audio_ata_test.h"
#endif
#include <math.h>
#ifdef MTK_AIRDUMP_EN_MIC_RECORD
#include "record_control.h"
#include "hal_dvfs.h"
#endif
#ifdef MTK_LEAKAGE_DETECTION_ENABLE
#include "race_event.h"
#endif
#ifdef MTK_USER_TRIGGER_FF_ENABLE
#include "race_cmd_relay_cmd.h"
#endif
#include "race_event.h"
#include "race_event_internal.h"

////////////////////////////////////////////////////////////////////////////////
// Constant Definitions ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#define dsp_realtime_min(x,y) (x > y) ? y : x
#define PEQ_SPECIAL_PHASE   (0xFF)


//////////////////////////////////////////////////////////////////////////////
// Global Variables ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
#if defined(MTK_PEQ_ENABLE) || defined(MTK_LINEIN_PEQ_ENABLE)
#define PEQ_NVKEY_BUF_SIZE (sizeof(AMI_AWS_MCE_PEQ_PACKT_t) + sizeof(DSP_PEQ_NVKEY_t))
uint8_t g_peq_nvkey[PEQ_NVKEY_BUF_SIZE];
uint8_t g_peq_nvkey_available = 1;
#endif
#ifdef MTK_ANC_ENABLE
static uint8_t g_anc_race_ch_id;
static uint32_t g_anc_notify_on; //bit[0]:agent_done, bit[1]:partner_done, bit[16:23]:agent_filter, bit[24:31]:parnter_filter
static uint32_t g_anc_notify_off; //bit[0]:agent_done, bit[1]:partner_done, bit[31]: request_from_race
static uint32_t g_anc_set_agent_vol; //anc_sw_gain_t
static uint32_t g_anc_set_partner_vol; //anc_sw_gain_t
static SemaphoreHandle_t g_race_anc_mutex = NULL;
#endif
#ifdef MTK_AIRDUMP_EN
uint8_t g_airdump_race_ch_id;
uint8_t g_airdump_cnt_past = 0;
static TimerHandle_t g_airdump_timer = NULL;
#endif
#ifdef MTK_AIRDUMP_EN_MIC_RECORD
uint8_t g_airdump_common_race_ch_id;
uint8_t g_airdump_common_race_request;
uint8_t g_airdump_common_cnt = 0;
TimerHandle_t g_airdump_common_timer = NULL;
int16_t g_record_airdump_data[256];  /*TODO: Change to dynamic alloc.*/
extern uint16_t g_dump;
extern bool g_record_airdump;
#if 0
const int16_t g_128_1ktone[128] =
{
0, 6284, 11612, 15173, 16423, 15171, 11612, 6284, -1, -6284, -11613, -15172, -16422, -15173, -11613, -6284,
0, 6284, 11612, 15173, 16423, 15171, 11612, 6284, -1, -6284, -11613, -15172, -16422, -15173, -11613, -6284,
0, 6284, 11612, 15173, 16423, 15171, 11612, 6284, -1, -6284, -11613, -15172, -16422, -15173, -11613, -6284,
0, 6284, 11612, 15173, 16423, 15171, 11612, 6284, -1, -6284, -11613, -15172, -16422, -15173, -11613, -6284,
0, 6284, 11612, 15173, 16423, 15171, 11612, 6284, -1, -6284, -11613, -15172, -16422, -15173, -11613, -6284,
0, 6284, 11612, 15173, 16423, 15171, 11612, 6284, -1, -6284, -11613, -15172, -16422, -15173, -11613, -6284,
0, 6284, 11612, 15173, 16423, 15171, 11612, 6284, -1, -6284, -11613, -15172, -16422, -15173, -11613, -6284,
0, 6284, 11612, 15173, 16423, 15171, 11612, 6284, -1, -6284, -11613, -15172, -16422, -15173, -11613, -6284,
};
const int16_t g_128_2ktone[128] =
{
0, 11612, 16422, 11612, 0, -11612, -16422, -11612, 1, 11612, 16422, 11613, -1, -11612, -16422, -11613,
0, 11612, 16422, 11612, 0, -11612, -16422, -11612, 1, 11612, 16422, 11613, -1, -11612, -16422, -11613,
0, 11612, 16422, 11612, 0, -11612, -16422, -11612, 1, 11612, 16422, 11613, -1, -11612, -16422, -11613,
0, 11612, 16422, 11612, 0, -11612, -16422, -11612, 1, 11612, 16422, 11613, -1, -11612, -16422, -11613,
0, 11612, 16422, 11612, 0, -11612, -16422, -11612, 1, 11612, 16422, 11613, -1, -11612, -16422, -11613,
0, 11612, 16422, 11612, 0, -11612, -16422, -11612, 1, 11612, 16422, 11613, -1, -11612, -16422, -11613,
0, 11612, 16422, 11612, 0, -11612, -16422, -11612, 1, 11612, 16422, 11613, -1, -11612, -16422, -11613,
0, 11612, 16422, 11612, 0, -11612, -16422, -11612, 1, 11612, 16422, 11613, -1, -11612, -16422, -11613,
};
#endif
#endif
#ifdef MTK_LEAKAGE_DETECTION_ENABLE
static uint16_t g_LD_result_agent = 0;  //bit[0:7]:result, bit[15]:done_or_not
static uint16_t g_LD_result_partner = 0;//bit[0:7]:result, bit[15]:done_or_not
#endif
#ifdef MTK_USER_TRIGGER_FF_ENABLE
#define USER_TRIGGER_FF_STATUS_RUNNING 2
#define USER_TRIGGER_FF_STATUS_DONE 3
#define USER_TRIGGER_FF_STATUS_ABORT 4
#define USER_TRIGGER_FF_STATUS_SMALL_VOLUME 5
#define USER_TRIGGER_FF_STATUS_Compare 6


#if (RACE_DEBUG_PRINT_ENABLE)
extern void race_debug_print(uint8_t *data, uint32_t size, const char *print_tag);
#endif
#endif

//////////////////////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS /////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
#if defined(MTK_PEQ_ENABLE) || defined(MTK_LINEIN_PEQ_ENABLE)
/**
 * race_dsprt_peq_report
 *
 * For agent and partner, g_peq_nvkey buffer is used to store new PEQ coefficients and parameters.
 * Release g_peq_nvkey with set g_peq_nvkey_available as 1.
 *
 * @pu2PeqParam : reserved.
 */
void race_dsprt_peq_report(uint16_t *pu2PeqParam)
{
    RACE_LOG_MSGID_I("race_dsprt_peq_report\n", 0);
    g_peq_nvkey_available = 1;
}

/**
 * race_dsprt_peq_realtime_data
 *
 * For agent, prepare AMI parameters for ami event, AMI_EVENT_PEQ_REALTIME, and send IF packet which contain PEQ data to partner.
 * For partner, prepare AMI paramters for ami event, AMI_EVENT_PEQ_REALTIME.
 *
 * @setting_mode  : [Agent only] 0:PEQ_DIRECT  1:PEQ_SYNC.
 * @target_bt_clk   : [Agent only] a bt clock for agent/partner to apply PEQ simultaneously.
 * @p_coef            : [Agent only] new PEQ coefficients.
 * @coef_size        : [Agent only] new PEQ coefficient size (bytes).
 */
uint32_t race_dsprt_peq_realtime_data(uint8_t phase_id, uint8_t setting_mode, uint32_t target_bt_clk, uint8_t *p_coef, uint32_t coef_size, am_feature_type_t audio_path_id)
{
    uint32_t ret = 0;
    uint8_t *peq_coef_buf;
    bt_sink_srv_am_feature_t am_feature;

    if(bt_connection_manager_device_local_info_get_aws_role() == BT_AWS_MCE_ROLE_AGENT) //Agent (Transmitter)
    {
        AMI_AWS_MCE_PEQ_PACKT_t *peq_packet = (AMI_AWS_MCE_PEQ_PACKT_t *)race_mem_alloc(sizeof(AMI_AWS_MCE_PEQ_PACKT_t) + coef_size);
        if (peq_packet) {
            uint32_t size = ((uint32_t)&peq_packet->peq_data.peq_data_rt.peq_coef - (uint32_t)peq_packet) + coef_size;
            peq_packet->phase_id = phase_id;
            peq_packet->setting_mode = setting_mode;
            peq_packet->target_bt_clk = target_bt_clk;
            peq_packet->peq_data.peq_data_rt.peq_coef_size = coef_size;
            memcpy((void *)&peq_packet->peq_data.peq_data_rt.peq_coef, (void *)p_coef, coef_size);
            bt_sink_srv_aws_mce_ami_data(AMI_EVENT_PEQ_REALTIME, (uint8_t *)peq_packet, size, false, 0);
            race_mem_free(peq_packet);
        } else {
            RACE_LOG_MSGID_E("peq realtime : 1st malloc failed, size:%d\n",1, sizeof(AMI_AWS_MCE_PEQ_PACKT_t) + coef_size);
            configASSERT(0);
        }
    }
    peq_coef_buf = race_mem_alloc(coef_size);
    if (peq_coef_buf) {
        memcpy((void *)peq_coef_buf, (void *)p_coef, coef_size);
        memset(&am_feature, 0, sizeof(bt_sink_srv_am_feature_t));
        am_feature.type_mask                             = audio_path_id;
        am_feature.feature_param.peq_param.enable        = 1;
        am_feature.feature_param.peq_param.sound_mode    = PEQ_SOUND_MODE_REAlTIME;
        am_feature.feature_param.peq_param.u2ParamSize   = (uint16_t)coef_size;
        am_feature.feature_param.peq_param.pu2Param      = (uint16_t *)peq_coef_buf;
        am_feature.feature_param.peq_param.target_bt_clk = target_bt_clk;
        am_feature.feature_param.peq_param.setting_mode  = setting_mode;
        am_feature.feature_param.peq_param.phase_id      = phase_id;
        am_feature.feature_param.peq_param.peq_notify_cb = race_dsprt_peq_report;
        ret = am_audio_set_feature(FEATURE_NO_NEED_ID, &am_feature);
    } else {
        ret = -1;
        RACE_LOG_MSGID_E("peq realtime : 2nd malloc failed, size:%d\n",1, coef_size);
        configASSERT(0);
    }
    return ret;
}

/**
 * race_dsprt_peq_change_mode_data
 *
 * For agent, prepare AMI parameters for ami event, AMI_EVENT_PEQ_CHANGE_MODE, and send IF packet which contain PEQ data to partner.
 * For partner, prepare AMI paramters for ami event, AMI_EVENT_PEQ_CHANGE_MODE.
 *
 * @setting_mode  : [Agent only] 0:PEQ_DIRECT  1:PEQ_SYNC.
 * @target_bt_clk   : [Agent only] a bt clock for agent/partner to apply PEQ simultaneously.
 * @enable            : [Agent only] going to enable PEQ or disable PEQ.
 * @sound_mode    : [Agent only] going to change to sound_mode mode.
 */
uint32_t race_dsprt_peq_change_mode_data(uint8_t phase_id, uint8_t setting_mode, uint32_t target_bt_clk, uint8_t enable, uint8_t sound_mode, am_feature_type_t audio_path_id)
{
    uint32_t ret = 0;
    bt_sink_srv_am_feature_t am_feature;

    if(bt_connection_manager_device_local_info_get_aws_role() == BT_AWS_MCE_ROLE_AGENT) //Agent (Transmitter)
    {
        AMI_AWS_MCE_PEQ_PACKT_t *peq_packet = (AMI_AWS_MCE_PEQ_PACKT_t *)race_mem_alloc(sizeof(AMI_AWS_MCE_PEQ_PACKT_t));
        uint32_t size = (uint32_t)&peq_packet->peq_data.peq_data_cm.reserved - (uint32_t)peq_packet;
        if (peq_packet) {
            peq_packet->phase_id = phase_id;
            peq_packet->setting_mode = setting_mode;
            peq_packet->target_bt_clk = target_bt_clk;
            peq_packet->peq_data.peq_data_cm.enable = enable;
            peq_packet->peq_data.peq_data_cm.sound_mode = sound_mode;
            bt_sink_srv_aws_mce_ami_data(AMI_EVENT_PEQ_CHANGE_MODE, (uint8_t *)peq_packet, size, false, 0);
            race_mem_free(peq_packet);
        } else {
            RACE_LOG_MSGID_E("peq change : 1st malloc failed, size:%d\n",1, sizeof(AMI_AWS_MCE_PEQ_PACKT_t));
            configASSERT(0);
        }
    }
    memset(&am_feature, 0, sizeof(bt_sink_srv_am_feature_t));
    am_feature.type_mask                             = audio_path_id;
    am_feature.feature_param.peq_param.enable        = enable;
    am_feature.feature_param.peq_param.sound_mode    = sound_mode;
    am_feature.feature_param.peq_param.u2ParamSize   = 0;
    am_feature.feature_param.peq_param.pu2Param      = 0;
    am_feature.feature_param.peq_param.target_bt_clk = target_bt_clk;
    am_feature.feature_param.peq_param.setting_mode  = setting_mode;
    am_feature.feature_param.peq_param.phase_id      = phase_id;
    am_feature.feature_param.peq_param.peq_notify_cb = race_dsprt_peq_report;
    ret = am_audio_set_feature(FEATURE_NO_NEED_ID, &am_feature);
    return ret;
}
#ifdef MTK_AWS_MCE_ENABLE
/**
 * race_dsprt_peq_collect_data
 *
 * For partner, to collect IF packets which contain PEQ data and call function to prepare AMI paramters according to ami event ...
 *
 * @info : report info from aws_mce_report service
 */
int32_t race_dsprt_peq_collect_data(bt_aws_mce_report_info_t *info)
{
    AMI_AWS_MCE_PACKET_HDR_t *ami_pkt_header = (AMI_AWS_MCE_PACKET_HDR_t *)info->param;
    AMI_AWS_MCE_PEQ_PACKT_t *peq_param = (AMI_AWS_MCE_PEQ_PACKT_t *)((uint8_t *)info->param + sizeof(AMI_AWS_MCE_PACKET_HDR_t));

    static ami_event_t _ami_event = 0;
    static uint32_t _save_len = 0;
    static uint32_t _total_pkt = 0;
    static int32_t _pre_pkt = 0;

    uint32_t temp_size;
    int32_t ret = 1;

    if((ami_pkt_header->ami_event != AMI_EVENT_PEQ_REALTIME) && (ami_pkt_header->ami_event != AMI_EVENT_PEQ_CHANGE_MODE)) {
        RACE_LOG_MSGID_W("race_dsprt_peq_collect_data wrong ami event:%d\n", 1, ami_pkt_header->ami_event);
        return -2;
    }

    if(ami_pkt_header->SubPktId == 0)
    {
        if(g_peq_nvkey_available != 1) {
            RACE_LOG_MSGID_W("g_peq_nvkey is unavailable, (%d) %d %d, %d %d\n", 5, ami_pkt_header->ami_event, ami_pkt_header->numSubPkt, ami_pkt_header->SubPktId, _total_pkt, _pre_pkt);
        }
        _ami_event = ami_pkt_header->ami_event;
        _save_len = 0;
        _total_pkt = (uint32_t)ami_pkt_header->numSubPkt;
        _pre_pkt = -1;
        g_peq_nvkey_available = 0;
    }

    if((ami_pkt_header->SubPktId == (_pre_pkt + 1)) && (ami_pkt_header->numSubPkt == _total_pkt) && (ami_pkt_header->ami_event == _ami_event) && (_total_pkt >= 1))
    {
        temp_size = (uint32_t)info->param_len - sizeof(AMI_AWS_MCE_PACKET_HDR_t);
        memcpy(g_peq_nvkey + _save_len, peq_param, temp_size);
        _save_len += temp_size;
        _pre_pkt = (uint32_t)ami_pkt_header->SubPktId;
        if(_save_len > PEQ_NVKEY_BUF_SIZE) {
            RACE_LOG_MSGID_E("PEQ collect data from IF\n", 0);
            configASSERT(0);
        }
    }
    else
    {
        RACE_LOG_MSGID_E("PEQ packet header is wrong, (%d) %d %d, %d %d\n", 5, ami_pkt_header->ami_event, ami_pkt_header->numSubPkt, ami_pkt_header->SubPktId, _total_pkt, _pre_pkt);
        return -2;
    }

    if((_pre_pkt + 1) == _total_pkt)
    {
        bt_sink_srv_am_feature_t am_feature;
        memset(&am_feature, 0, sizeof(bt_sink_srv_am_feature_t));
        switch(ami_pkt_header->ami_event)
        {
            case AMI_EVENT_PEQ_REALTIME:
            {
                AMI_AWS_MCE_PEQ_PACKT_t *peq_packet = (AMI_AWS_MCE_PEQ_PACKT_t *)g_peq_nvkey;
                uint32_t size = ((uint32_t)&peq_packet->peq_data.peq_data_rt.peq_coef - (uint32_t)peq_packet) + peq_packet->peq_data.peq_data_rt.peq_coef_size;
                if (_save_len == size) {
                    uint16_t peq_coef_buf_size = peq_packet->peq_data.peq_data_rt.peq_coef_size;
                    uint8_t *peq_coef_buf = (uint8_t *)race_mem_alloc(peq_coef_buf_size);
                    if (peq_coef_buf) {
                        memcpy(peq_coef_buf, &peq_packet->peq_data.peq_data_rt.peq_coef, peq_coef_buf_size);
                        am_feature.type_mask                             = AM_A2DP_PEQ;
                        am_feature.feature_param.peq_param.enable        = 1;
                        am_feature.feature_param.peq_param.sound_mode    = PEQ_SOUND_MODE_REAlTIME;
                        am_feature.feature_param.peq_param.pu2Param      = (uint16_t *)peq_coef_buf;
                        am_feature.feature_param.peq_param.u2ParamSize   = peq_coef_buf_size;
                        am_feature.feature_param.peq_param.target_bt_clk = peq_packet->target_bt_clk;
                        am_feature.feature_param.peq_param.setting_mode  = peq_packet->setting_mode;
                        am_feature.feature_param.peq_param.phase_id      = peq_packet->phase_id;
                        am_feature.feature_param.peq_param.peq_notify_cb = race_dsprt_peq_report;
                        ret = am_audio_set_feature(FEATURE_NO_NEED_ID, &am_feature);
                        RACE_LOG_MSGID_I("peq collect data - realtime peq_param_length=%u target_bt_clk=0x%x ret=%d\n",3,am_feature.feature_param.peq_param.u2ParamSize,am_feature.feature_param.peq_param.target_bt_clk,ret);
                    } else {
                        RACE_LOG_MSGID_E("peq collect data - realtime malloc fail, size:%d\n",1, peq_coef_buf_size);
                        configASSERT(0);
                        ret = -1;
                    }
                    g_peq_nvkey_available = 1;
                } else {
                    ret = -1;
                    RACE_LOG_MSGID_E("PEQ acc packet size is wrong, %d %d\n",2, am_feature.feature_param.peq_param.u2ParamSize, size);
                    configASSERT(0);
                }
                break;
            }
            case AMI_EVENT_PEQ_CHANGE_MODE:
            {
                AMI_AWS_MCE_PEQ_PACKT_t *peq_packet = (AMI_AWS_MCE_PEQ_PACKT_t *)g_peq_nvkey;
                uint32_t size = (uint32_t)&peq_packet->peq_data.peq_data_cm.reserved - (uint32_t)peq_packet;
                if(_save_len == size) {
                    am_feature.type_mask                             = AM_A2DP_PEQ;
                    am_feature.feature_param.peq_param.enable        = peq_packet->peq_data.peq_data_cm.enable;
                    am_feature.feature_param.peq_param.sound_mode    = peq_packet->peq_data.peq_data_cm.sound_mode;
                    am_feature.feature_param.peq_param.pu2Param      = 0;
                    am_feature.feature_param.peq_param.u2ParamSize   = 0;
                    am_feature.feature_param.peq_param.target_bt_clk = peq_packet->target_bt_clk;
                    am_feature.feature_param.peq_param.setting_mode  = peq_packet->setting_mode;
                    am_feature.feature_param.peq_param.phase_id      = peq_packet->phase_id;
                    am_feature.feature_param.peq_param.peq_notify_cb = race_dsprt_peq_report;
                    ret = am_audio_set_feature(FEATURE_NO_NEED_ID, &am_feature);
                    g_peq_nvkey_available = 1;
                    RACE_LOG_MSGID_I("enable=%d sound_mode=%d target_bt_clk=%d ret=%d\n",4,am_feature.feature_param.peq_param.enable,am_feature.feature_param.peq_param.sound_mode,am_feature.feature_param.peq_param.target_bt_clk,ret);
                } else {
                    ret = -1;
                    RACE_LOG_MSGID_E("PEQ packet size is wrong, %d %d\n",2, am_feature.feature_param.peq_param.u2ParamSize, size);
                    configASSERT(0);
                }
                break;
            }
            default:
            {
                ret = -1;
                RACE_LOG_MSGID_E("un-expected ami event : %d\n",1, ami_pkt_header->ami_event);
                break;
            }
        }
        _ami_event = 0;
        _save_len = 0;
        _total_pkt = 0;
        _pre_pkt = 0;
    }

    if(ret == 0) {
        RACE_LOG_MSGID_I("race_dsprt_peq_collect_data SUCCESS\n", 0);
    } else if (ret == 1) {
        RACE_LOG_MSGID_I("race_dsprt_peq_collect_data CONTINUE\n", 0);
    } else {
        g_peq_nvkey_available = 1;
        RACE_LOG_MSGID_E("race_dsprt_peq_collect_data FAIL\n", 0);
    }

    return ret;
}

int32_t race_dsprt_peq_get_target_bt_clk(bt_aws_mce_role_t role, uint8_t *setting_mode, bt_clock_t *target_bt_clk)
{
    bt_clock_t current_bt_clk = {0};
    int32_t diff = 0;
    uint32_t ret;
    if(role == BT_AWS_MCE_ROLE_AGENT)
    {
        *setting_mode = PEQ_SYNC;
        ret = bt_sink_srv_bt_clock_addition(&current_bt_clk, NULL, 0);
        if(ret == BT_STATUS_FAIL) RACE_LOG_MSGID_W("get current bt clock FAIL\n",0);
        ret = bt_sink_srv_bt_clock_addition(target_bt_clk, NULL, PEQ_FW_LATENCY*1000);
        if(ret == BT_STATUS_FAIL) RACE_LOG_MSGID_W("get target bt clock FAIL with duration %d us\n", 1,PEQ_FW_LATENCY*1000);
        diff = (((int32_t)target_bt_clk->nclk - (int32_t)current_bt_clk.nclk)*625/2 + ((int32_t)target_bt_clk->nclk_intra - (int32_t)current_bt_clk.nclk_intra));
        if((diff > PEQ_FW_LATENCY*1000+10000) || (diff < PEQ_FW_LATENCY*1000-10000)) {
            RACE_LOG_MSGID_W("get cur: 0x%x.0x%x tar: 0x%x.0x%x  diff: %d xxxxxxxxxxxx\n",5,current_bt_clk.nclk,current_bt_clk.nclk_intra, target_bt_clk->nclk,target_bt_clk->nclk_intra,diff);
        } else {
            RACE_LOG_MSGID_I("get cur: %x.%x tar: %x.%x \n",4,current_bt_clk.nclk,current_bt_clk.nclk_intra, target_bt_clk->nclk,target_bt_clk->nclk_intra);
        }
        if(ret == BT_STATUS_FAIL) { // for agent only case
            *setting_mode = PEQ_DIRECT;
        }
    } else {
        *setting_mode = PEQ_DIRECT;
    }
    return 0;
}
#endif
#endif

#ifdef MTK_ANC_ENABLE
#ifndef MTK_ANC_V2
#ifdef MTK_AWS_MCE_ENABLE
uint32_t race_dsprt_anc_feedback_data(aws_mce_report_anc_param_t *anc_param)
{
    race_general_msg_t msg_queue_item;
    PTR_RACE_COMMON_HDR_STRU race_pkt_hdr;
    race_dsprealtime_anc_struct *anc_r_nv_cmd;
    uint8_t channel_id = (uint8_t)(anc_param->ch_info & 0xFF);
    RACE_ERRCODE error;

    switch (channel_id)
    {
        case RACE_SERIAL_PORT_TYPE_UART:
            msg_queue_item.dev_t = SERIAL_PORT_DEV_UART_0;
            break;
        case RACE_SERIAL_PORT_TYPE_SPP:
            msg_queue_item.dev_t = SERIAL_PORT_DEV_BT_SPP;
            break;
        case RACE_SERIAL_PORT_TYPE_BLE:
            msg_queue_item.dev_t = SERIAL_PORT_DEV_BT_LE;
            break;
        case RACE_SERIAL_PORT_TYPE_AIRUPDATE:
            msg_queue_item.dev_t = SERIAL_PORT_DEV_BT_AIRUPDATE;
            break;
        default:
            msg_queue_item.dev_t = SERIAL_PORT_DEV_UNDEFINED;
            break;
    }
    //msg_queue_item.dev_t = SERIAL_PORT_DEV_UART_0;
    msg_queue_item.msg_id = MSG_ID_RACE_LOCAL_RSP_NOTIFY_IND;
    msg_queue_item.msg_data = race_mem_alloc(sizeof(RACE_COMMON_HDR_STRU) + sizeof(race_dsprealtime_anc_struct));
    if(msg_queue_item.msg_data) {
        race_pkt_hdr = (PTR_RACE_COMMON_HDR_STRU)msg_queue_item.msg_data;
        race_pkt_hdr->pktId.value = (uint8_t)(anc_param->ch_info >> 8);
        race_pkt_hdr->type = RACE_TYPE_COMMAND;
        race_pkt_hdr->length = sizeof(RACE_COMMON_HDR_STRU) + sizeof(race_dsprealtime_anc_struct) - 4;
        race_pkt_hdr->id = RACE_DSPREALTIME_ANC;
        anc_r_nv_cmd = (race_dsprealtime_anc_struct *)(msg_queue_item.msg_data + sizeof(RACE_COMMON_HDR_STRU));
        anc_r_nv_cmd->status = 0x80 | (anc_param->ch_info & 0xFF); //channel_id
        anc_r_nv_cmd->anc_id = RACE_ANC_READ_PARTNER_NVDM;
        anc_r_nv_cmd->param.gain.val = anc_param->arg;
        RACE_LOG_MSGID_I("race_dsprt_anc_feedback_data channel:%d, gain=%d %d \n",3, anc_r_nv_cmd->status, anc_r_nv_cmd->param.gain.gain_index_l,anc_r_nv_cmd->param.gain.gain_index_r);
        error = race_send_msg(&msg_queue_item);
        if(error != RACE_ERRCODE_SUCCESS) {
            RACE_LOG_MSGID_E("race_dsprt_anc_feedback_data error:%x",1,error);
        }
    } else {
        RACE_LOG_MSGID_E("race_dsprt_anc_feedback_data malloc fail",0);
        configASSERT(0);
        error = RACE_ERRCODE_FAIL;
    }
    return error;
}
#endif
static void race_anc_mutex_take(void)
{
    if(g_race_anc_mutex == NULL) {
        g_race_anc_mutex = xSemaphoreCreateMutex();
        if (g_race_anc_mutex == NULL) {
            RACE_LOG_MSGID_E("g_race_anc_mutex create error\r\n", 0);
        }
    }
    if(g_race_anc_mutex != NULL)
    {
        if (xSemaphoreTake(g_race_anc_mutex, portMAX_DELAY) == pdFALSE) {
            RACE_LOG_MSGID_E("g_race_anc_mutex error\r\n", 0);
        }
    }
}
static void race_anc_mutex_give(void)
{
    if(g_race_anc_mutex != NULL)
    {
        if (xSemaphoreGive(g_race_anc_mutex) == pdFALSE) {
            RACE_LOG_MSGID_E("g_race_anc_mutex error\r\n", 0);
        }
    }
}
void race_dsprt_anc_notify(anc_control_event_t event_id, uint8_t fromPartner, uint32_t arg)
{
    if (anc_get_sync_time() == 0) {
        bt_aws_mce_role_t role = bt_connection_manager_device_local_info_get_aws_role();
        race_anc_mutex_take();
        if ((role == BT_AWS_MCE_ROLE_AGENT) || (role == BT_AWS_MCE_ROLE_NONE)) {
            if (event_id == ANC_CONTROL_EVENT_ON) {
                uint32_t filter = (g_anc_notify_on >> 8) & 0xFF;
                if ((filter == 0) || (arg == 0) || (filter != arg)) {
                    race_anc_mutex_give();
                    RACE_LOG_MSGID_W("race_dsprt_anc_notify, event:on, wrong filter:0x%x 0x%x",2, filter, arg);
                    return;
                }
                if (fromPartner == 0) {
                    g_anc_notify_on |= RACE_ANC_AGENT;
                    g_anc_notify_on |= (arg << 16);
                    if ((g_anc_notify_on & RACE_ANC_PARTNER) && (filter != ((g_anc_notify_on >> 24) & 0xFF))) {
                         g_anc_notify_on &= (~RACE_ANC_PARTNER);
                         g_anc_notify_on &= (~(0xFF << 24));
                    }
                } else {
                    g_anc_notify_on |= RACE_ANC_PARTNER;
                    g_anc_notify_on |= (arg << 24);
                    if ((g_anc_notify_on & RACE_ANC_AGENT) && (filter != ((g_anc_notify_on >> 16) & 0xFF))) {
                         g_anc_notify_on &= (~RACE_ANC_AGENT);
                         g_anc_notify_on &= (~(0xFF << 16));
                    }
                }
#ifdef MTK_AWS_MCE_ENABLE
                if ((g_anc_notify_on & RACE_ANC_BOTH) == RACE_ANC_BOTH)
#else
                if (g_anc_notify_on & RACE_ANC_AGENT)
#endif
                {
                    race_dsprealtime_anc_struct *pEvt = RACE_ClaimPacket(RACE_TYPE_NOTIFICATION, RACE_DSPREALTIME_ANC, sizeof(race_dsprealtime_anc_struct), g_anc_race_ch_id);
                    if (pEvt != NULL) {
                        memset(pEvt, 0, sizeof(race_dsprealtime_anc_struct));
                        pEvt->anc_id = RACE_ANC_ON;
                        pEvt->param.anc_on_param.anc_filter_type = (uint8_t)(arg & ANC_FILTER_TYPE_MASK);
                        pEvt->param.anc_on_param.anc_mode = (uint8_t)((arg & ANC_FF_ONLY_BIT_MASK) ? 1 : (arg & ANC_FB_ONLY_BIT_MASK) ? 2 : 0);
                        RACE_LOG_MSGID_I("MP_Step__4__ set anc on filter:0x%x, 0x%x",2,arg,g_anc_notify_on);
                        race_flush_packet((void *)pEvt, g_anc_race_ch_id);
                        g_anc_notify_on = 0;
                    }
                }
            } else if (event_id == ANC_CONTROL_EVENT_SET_VOLUME) {
                uint32_t *p_set_vol = (fromPartner == 0) ? (&g_anc_set_agent_vol) : (&g_anc_set_partner_vol);
                if ((*p_set_vol != arg)) {
                    race_anc_mutex_give();
                    RACE_LOG_MSGID_W("race_dsprt_anc_notify, event:set_%c_vol, wrong vol:0x%x 0x%x",3,(fromPartner==0)?'A':'P', *p_set_vol, arg);
                    return;
                } else {
                    race_dsprealtime_anc_struct *pEvt = RACE_ClaimPacket(RACE_TYPE_RESPONSE, RACE_DSPREALTIME_ANC, sizeof(race_dsprealtime_anc_struct), g_anc_race_ch_id);
                    if (pEvt != NULL) {
                        memset(pEvt, 0, sizeof(race_dsprealtime_anc_struct));
                        pEvt->param.gain.val = arg;
                        if (fromPartner == 0) {
                            pEvt->anc_id = RACE_ANC_SET_AGENT_VOL;
                            RACE_LOG_MSGID_I("MP_Step__3__ set A gain: 0x%x race_dsprt_anc_notify",1,arg);
                        } else {
                            pEvt->anc_id = RACE_ANC_SET_PARTNER_VOL;
                            RACE_LOG_MSGID_I("MP_Step__6__ set P gain: 0x%x race_dsprt_anc_notify",1,arg);
                        }
                        race_flush_packet((void *)pEvt, g_anc_race_ch_id);
                        *p_set_vol = 0;
                    }
                }
            } else if (event_id == ANC_CONTROL_EVENT_OFF) {
                if (g_anc_notify_off == 0) {
                    race_anc_mutex_give();
                    RACE_LOG_MSGID_I("race_dsprt_anc_notify, event:off, request is not from race cmd",0);
                    return;
                }
                if (fromPartner == 0) {
                    g_anc_notify_off |= RACE_ANC_AGENT;
                } else {
                    g_anc_notify_off |= RACE_ANC_PARTNER;
                }
#ifdef MTK_AWS_MCE_ENABLE
                if ((g_anc_notify_off & RACE_ANC_BOTH) == RACE_ANC_BOTH)
#else
                if (g_anc_notify_off & RACE_ANC_AGENT)
#endif
                {
                    race_dsprealtime_anc_struct *pEvt = RACE_ClaimPacket(RACE_TYPE_RESPONSE, RACE_DSPREALTIME_ANC, sizeof(race_dsprealtime_anc_struct), g_anc_race_ch_id);
                    if (pEvt != NULL) {
                        memset(pEvt, 0, sizeof(race_dsprealtime_anc_struct));
                        pEvt->anc_id = RACE_ANC_OFF;
                        RACE_LOG_MSGID_I("MP_Step__4__ set anc off",0);
                        race_flush_packet((void *)pEvt, g_anc_race_ch_id);
                        g_anc_notify_off = 0;
                    }
                }
            }
        }
#ifdef MTK_AWS_MCE_ENABLE
        else { //partner
            if (event_id == ANC_CONTROL_EVENT_ON) {
                if (arg == 0) {
                    race_anc_mutex_give();
                    RACE_LOG_MSGID_E("race_dsprt_anc_notify, event:on, partner wrong filter:0",0);
                    return;
                }
                anc_send_aws_mce_param(ANC_DIRECT, ANC_CONTROL_EVENT_ON, (uint32_t)arg, 0);
#ifndef MTK_ANC_V2
            } else if (event_id == ANC_CONTROL_EVENT_SET_VOLUME) {
                anc_send_aws_mce_param(ANC_DIRECT, ANC_CONTROL_EVENT_SET_VOLUME, (uint32_t)arg, 0);
#endif
            } else if (event_id == ANC_CONTROL_EVENT_OFF) {
                anc_send_aws_mce_param(ANC_DIRECT, ANC_CONTROL_EVENT_OFF, (uint32_t)arg, 0);
            }
        }
#endif
        race_anc_mutex_give();
    }
}

#ifdef MTK_LEAKAGE_DETECTION_ENABLE
void anc_leakage_detection_racecmd_callback(uint16_t leakage_status)
{
    bt_aws_mce_role_t role = bt_connection_manager_device_local_info_get_aws_role();

    RACE_LOG_MSGID_I("[RECORD_LC] anc_leakage_detection_racecmd_callback result:%d for role:0x%x",2, leakage_status, role);

    audio_anc_leakage_compensation_stop();
    audio_anc_leakage_compensation_set_status(0);
    if (leakage_status == LD_STATUS_PASS) {
        leakage_status = 0; //re-map
    }

    if ((role == BT_AWS_MCE_ROLE_AGENT) || (role == BT_AWS_MCE_ROLE_NONE)) {
        anc_leakage_detection_racecmd_response(leakage_status, 0);

    } else {
        anc_send_aws_mce_param(ANC_DIRECT, ANC_CONTROL_EVENT_LD_START, (uint32_t)leakage_status, 0);
    }

    //no need to resume a2dp because a2dp will be resumed by AM when VP close.
    //am_audio_dl_resume();

}

void anc_leakage_detection_racecmd_response(uint16_t leakage_status, uint8_t fromPartner)
{
    typedef struct
    {
        adaptive_check_notify_t header;
        uint8_t agent_status;
        uint8_t partner_status;
    } PACKED leakage_detection_notify_t;

    bt_aws_mce_role_t role = bt_connection_manager_device_local_info_get_aws_role();

    race_anc_mutex_take();
    if (fromPartner == 0) {
        g_LD_result_agent = leakage_status;
        g_LD_result_agent |= 0x8000;
        RACE_LOG_MSGID_I("[RECORD_LC] Get agent LD result : %d",1,leakage_status);
    } else {
        g_LD_result_partner = leakage_status;
        g_LD_result_partner |= 0x8000;
        RACE_LOG_MSGID_I("[RECORD_LC] Get partner LD result : %d",1,leakage_status);
    }

    if ((role == BT_AWS_MCE_ROLE_NONE)
        || ((bt_sink_srv_cm_get_special_aws_device() != NULL) && (bt_sink_srv_cm_get_aws_link_state() != BT_AWS_MCE_AGENT_STATE_ATTACHED))
        || ((g_LD_result_agent & 0x8000) && (g_LD_result_partner & 0x8000))) {
        leakage_detection_notify_t *pEvt = RACE_ClaimPacket(RACE_TYPE_NOTIFICATION,
                                                         RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK,
                                                         sizeof(leakage_detection_notify_t),
                                                         g_anc_race_ch_id);
        if ((g_LD_result_partner & 0x8000) == 0) {
            g_LD_result_partner = LD_STATUS_ABSENT;
        }
        if (pEvt) {
            pEvt->header.start_or_stop     = 1;
            pEvt->header.mode              = LEAKAGE_DETECTION_MODE;
            pEvt->header.bit_per_sample    = 2;
            pEvt->header.channel_num       = 0;
            pEvt->header.frame_size        = 0;
            pEvt->header.seq_num           = 0;
            pEvt->header.total_data_length = 2;
            pEvt->header.data_length       = 2;
            pEvt->agent_status             = (uint8_t)(g_LD_result_agent & 0xFF);
            pEvt->partner_status           = (uint8_t)(g_LD_result_partner & 0xFF);
            race_flush_packet((void *)pEvt, g_anc_race_ch_id);
            RACE_LOG_MSGID_I("[RECORD_LC] Send LD result to APK : %d %d",2,pEvt->agent_status,pEvt->partner_status);
        }
        g_LD_result_agent = 0;
        g_LD_result_partner = 0;
        #ifdef MTK_LEAKAGE_DETECTION_ENABLE
        race_cmd_vp_struct* param = (race_cmd_vp_struct*)race_mem_alloc(sizeof(race_cmd_vp_struct));
        if(param){
            param->vp_type = RACE_CMD_VP_LEAKAGE_DETECTION;
            param->play_flag = false;
            RACE_ERRCODE ret = race_send_event_notify_msg(RACE_EVENT_TYPE_CMD_VP, param);
            if(RACE_ERRCODE_SUCCESS != ret){
                race_mem_free(param);
            }
        }
        audio_anc_leakage_detection_resume_dl();
        #endif
    }
    race_anc_mutex_give();
}
#endif
#ifdef MTK_USER_TRIGGER_FF_ENABLE
static void anc_user_trigger_adaptive_ff_callback(int32_t Cal_status)
{
    extern anc_user_trigger_ff_param_t g_user_trigger_ff_info;
    bt_aws_mce_role_t role = bt_connection_manager_device_local_info_get_aws_role();
    bt_status_t ret = BT_SINK_SRV_STATUS_FAIL;

    RACE_LOG_MSGID_I("[user_trigger_ff]anc_user_trigger_adaptive_ff_callback, recieve status:%d ", Cal_status);
    if (Cal_status != USER_TRIGGER_FF_STATUS_DONE) {
        typedef struct
        {
            uint8_t status;
            uint8_t mode;
        }PACKED RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_EVT_STRU;

        RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_EVT_STRU *pEvt = RACE_ClaimPacket(RACE_TYPE_RESPONSE,
                                                         RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK,
                                                         sizeof(RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_EVT_STRU),
                                                         g_anc_race_ch_id);

        audio_anc_user_trigger_ff_stop();
        audio_anc_user_trigger_ff_recover_anc(Cal_status);

        if (pEvt) {
            pEvt->status = Cal_status;
            pEvt->mode = ADAPTIVE_FF_ANC_MODE;

            if ((role == BT_AWS_MCE_ROLE_AGENT) || (role == BT_AWS_MCE_ROLE_NONE)) {
                race_flush_packet((void *)pEvt, g_anc_race_ch_id);

            } else {/*partner send agent result*/
                race_send_pkt_t* pSndPkt;
                pSndPkt = (void *)race_pointer_cnv_pkt_to_send_pkt((void *)pEvt);
                race_pkt_t      *pret;
                race_send_pkt_t *psend;
                psend = (race_send_pkt_t *)pSndPkt;
                pret = &psend->race_data;
                #if (RACE_DEBUG_PRINT_ENABLE)
                race_debug_print((uint8_t *)pret, (uint32_t)(pret->hdr.length + 4), "Race evt:");
                #endif

                if(pSndPkt) {
                    ret = bt_send_aws_mce_race_cmd_data(&pSndPkt->race_data, pSndPkt->length, g_anc_race_ch_id, RACE_CMD_RSP_FROM_PARTNER,0);
                    if (ret != BT_STATUS_SUCCESS) {
                        RACE_LOG_MSGID_I("[relay_cmd][user_trigger_ff]partner send relay req FAIL \n", 0);
                    } else {
                        RACE_LOG_MSGID_I("[relay_cmd][user_trigger_ff]anc_user_trigger_adaptive_ff_callback, send status:%d success", pEvt->status);
//                        peq_relay_dbg.send_idx++;
                    }
                    race_mem_free(pSndPkt);
                }
            }
        }

    } else {
        typedef struct
        {
            uint8_t status;
            uint8_t mode;
            int32_t array[300];
        }PACKED RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_EVT_STRU;

        RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_EVT_STRU *pEvt = RACE_ClaimPacket(RACE_TYPE_RESPONSE,
                                                         RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK,
                                                         sizeof(RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_EVT_STRU),
                                                         g_anc_race_ch_id);

        if (pEvt) {
            pEvt->status = Cal_status;
            pEvt->mode = ADAPTIVE_FF_ANC_MODE;

            memcpy(pEvt->array, (g_user_trigger_ff_info.report_array+1), sizeof(int32_t) * 300);

            race_send_pkt_t* pSndPkt;
            uint32_t port_handle, ret_size, size;
            uint8_t *ptr;

            if ((role == BT_AWS_MCE_ROLE_AGENT) || (role == BT_AWS_MCE_ROLE_NONE)) {
                pSndPkt = race_pointer_cnv_pkt_to_send_pkt((void *)pEvt);
                port_handle = race_get_port_handle_by_channel_id(g_anc_race_ch_id);
                ret_size = race_port_send_data(port_handle, (uint8_t*)&pSndPkt->race_data, pSndPkt->length);

                size = pSndPkt->length;
                ptr = (uint8_t*)&pSndPkt->race_data;
                size -= ret_size;
                ptr += ret_size;
                while(size > 0)
                {
                    ret_size = race_port_send_data(port_handle, ptr, size);
                    size -= ret_size;
                    ptr += ret_size;
                }
                race_mem_free(pSndPkt);

            } else {
                pSndPkt = race_pointer_cnv_pkt_to_send_pkt((void *)pEvt);
                race_pkt_t      *pret;
                race_send_pkt_t *psend;
                psend = (race_send_pkt_t *)pSndPkt;
                pret = &psend->race_data;
                race_debug_print((uint8_t *)pret, (uint32_t)(pret->hdr.length + 4), "Race evt:");
                RACE_LOG_MSGID_I("[relay_cmd][user_trigger_ff]anc_user_trigger_adaptive_ff_callback, status1:%d success", pEvt->status);
                if(pSndPkt) {
                    ret = bt_send_aws_mce_race_cmd_data(&pSndPkt->race_data, pSndPkt->length, g_anc_race_ch_id, RACE_CMD_RSP_FROM_PARTNER,0);
                    if (ret != BT_STATUS_SUCCESS) {
                        RACE_LOG_MSGID_I("[relay_cmd][user_trigger_ff]partner send relay req FAIL \n", 0);
                    } else {
                        RACE_LOG_MSGID_I("[relay_cmd][user_trigger_ff]anc_user_trigger_adaptive_ff_callback, send status:%d success", pEvt->status);

                    }
                    race_mem_free(pSndPkt);
                }
            }
        }
    }

    RACE_LOG_MSGID_I("[user_trigger_ff]anc user trigger adaptive ff send status:%d, and waiting for response",1, Cal_status);

}

static void anc_user_trigger_adaptive_ff_receive_filter_callback(int32_t Cal_status)
{
    extern anc_user_trigger_ff_param_t g_user_trigger_ff_info;
    bt_aws_mce_role_t role = bt_connection_manager_device_local_info_get_aws_role();
    bt_status_t ret = BT_SINK_SRV_STATUS_FAIL;

    typedef struct
    {
        uint8_t status;
        uint8_t mode;
    }PACKED RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_EVT_STRU;

    RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_EVT_STRU *pEvt = RACE_ClaimPacket(RACE_TYPE_RESPONSE,
                                                         RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT,
                                                         sizeof(RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_EVT_STRU),
                                                         g_anc_race_ch_id);
    printf("[user_trigger_ff]anc_user_trigger_adaptive_ff_receive_filter_callback, send Cal_status:%d", (int)Cal_status);

    if (pEvt) {
        pEvt->status = (uint8_t)Cal_status;
        pEvt->mode = ADAPTIVE_FF_ANC_MODE;
        if ((role == BT_AWS_MCE_ROLE_AGENT) || (role == BT_AWS_MCE_ROLE_NONE)) {
            race_flush_packet((void *)pEvt, g_anc_race_ch_id);

        } else {
            race_send_pkt_t* pSndPkt;
            pSndPkt = (void *)race_pointer_cnv_pkt_to_send_pkt((void *)pEvt);
            race_pkt_t      *pret;
            race_send_pkt_t *psend;
            psend = (race_send_pkt_t *)pSndPkt;
            pret = &psend->race_data;
            #if (RACE_DEBUG_PRINT_ENABLE)
            race_debug_print((uint8_t *)pret, (uint32_t)(pret->hdr.length + 4), "Race evt:");
            #endif

            if(pSndPkt) {
                ret = bt_send_aws_mce_race_cmd_data(&pSndPkt->race_data, pSndPkt->length, g_anc_race_ch_id, RACE_CMD_RSP_FROM_PARTNER,0);
                if (ret != BT_STATUS_SUCCESS) {
                    RACE_LOG_MSGID_I("[relay_cmd][user_trigger_ff]partner send relay req FAIL \n", 0);
                } else {
                    RACE_LOG_MSGID_I("[relay_cmd][user_trigger_ff]anc_user_trigger_adaptive_ff_receive_filter_callback, send status:%d success", pEvt->status);

                }
                race_mem_free(pSndPkt);
            }
        }
    }

    audio_anc_user_trigger_ff_stop();
    /*turn on ANC if needed*/
    audio_anc_user_trigger_ff_recover_anc(Cal_status);
    race_send_event_notify_msg(RACE_EVENT_TYPE_ANC_ADAPTIVE_FF_END, NULL);
}
#endif
#endif
#endif

/**
 * RACE_DSPREALTIME_SUSPEND_HDR
 *
 * RACE_DSPREALTIME_SUSPEND_HDR Handler
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 *
 */
void* RACE_DSPREALTIME_SUSPEND_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
	typedef struct
	{
		uint8_t Status;
	}PACKED RACE_DSPREALTIME_SUSPEND_EVT_STRU;

	RACE_LOG_MSGID_I("RACE_DSPREALTIME_SUSPEND_HDR() enter, channel_id = %x \r\n",1, channel_id);

	RACE_DSPREALTIME_SUSPEND_EVT_STRU* pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_DSPREALTIME_SUSPEND, (uint16_t)sizeof(RACE_DSPREALTIME_SUSPEND_EVT_STRU), channel_id);
    bt_sink_srv_am_result_t am_status;

    if (pEvt != NULL)
    {
        am_status = am_audio_set_pause();
        pEvt->Status = am_status != AUD_EXECUTION_SUCCESS ? RACE_ERRCODE_FAIL : RACE_ERRCODE_SUCCESS;
    }

	return pEvt;
}


/**
 * RACE_DSPREALTIME_RESUME_HDR
 *
 * RACE_DSPREALTIME_RESUME_HDR Handler
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 *
 */
void* RACE_DSPREALTIME_RESUME_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
	typedef struct
	{
		uint8_t Status;
	}PACKED RACE_DSPREALTIME_RESUME_EVT_STRU;

	RACE_LOG_MSGID_I("channel_id = %x \r\n",1, channel_id);

	RACE_DSPREALTIME_RESUME_EVT_STRU* pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_DSPREALTIME_RESUME, (uint16_t)sizeof(RACE_DSPREALTIME_RESUME_EVT_STRU), channel_id);
    bt_sink_srv_am_result_t am_status;


    if (pEvt != NULL)
    {
        //API
        am_status = am_audio_set_resume();
        if (am_status != AUD_EXECUTION_SUCCESS) pEvt->Status = (uint8_t)RACE_ERRCODE_FAIL;
        else pEvt->Status = (uint8_t)RACE_ERRCODE_SUCCESS;
    }

    return pEvt;
}

#if defined(MTK_PEQ_ENABLE) || defined(MTK_LINEIN_PEQ_ENABLE)
/**
 * RACE_DSPREALTIME_PEQ_HDR
 *
 * RACE_DSPREALTIME_PEQ_HDR Handler
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 *
 */
void* RACE_DSPREALTIME_PEQ_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint8_t peq_phase;
        uint8_t Peq_param[1];
    }PACKED RACE_DSPREALTIME_PEQ_CMD_STRU;

    typedef struct
    {
        uint8_t Status;
    }PACKED RACE_DSPREALTIME_PEQ_EVT_STRU;

    RACE_DSPREALTIME_PEQ_CMD_STRU *pCmd = (RACE_DSPREALTIME_PEQ_CMD_STRU *)pCmdMsg;
    uint16_t Peq_param_length = (pCmd->Hdr.length >= 3) ? (pCmd->Hdr.length - 3) : 0; //Hdr.id: 2bytes, peq_phase: 1byte
    uint32_t ret;

    RACE_LOG_MSGID_I("RACE_DSPREALTIME_PEQ_HDR Peq_param_length=%d\n",1, Peq_param_length);

    RACE_DSPREALTIME_PEQ_EVT_STRU* pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE, (uint16_t)RACE_DSPREALTIME_PEQ, (uint16_t)sizeof(RACE_DSPREALTIME_PEQ_EVT_STRU), channel_id);

    if(((pCmd->peq_phase != 0) && (pCmd->peq_phase != 1) && (pCmd->peq_phase != PEQ_SPECIAL_PHASE))
        || ((pCmd->peq_phase != PEQ_SPECIAL_PHASE) && (Peq_param_length < (5*1*2+2+1)))
        || ((pCmd->peq_phase == PEQ_SPECIAL_PHASE) && (Peq_param_length < 1))) {
        RACE_LOG_MSGID_E("Un-supported phase id: %d or abnormal param length: %d\n",2,pCmd->peq_phase,Peq_param_length);
        if (pEvt != NULL) {
            pEvt->Status = (uint8_t)RACE_ERRCODE_FAIL;
        }
        return pEvt;
    }

    if (pEvt != NULL)
    {
        bt_clock_t target_bt_clk = {0};
        uint8_t setting_mode = PEQ_DIRECT;
        bt_aws_mce_role_t role = bt_connection_manager_device_local_info_get_aws_role();
        bt_sink_srv_am_type_t cur_type = NONE;
        am_feature_type_t audio_path_id = 0;
        cur_type = bt_sink_srv_ami_get_current_scenario();
        if (cur_type == A2DP) {
            audio_path_id = AM_A2DP_PEQ;
        } else if (cur_type == LINE_IN) {
            audio_path_id = AM_LINEIN_PEQ;
        } else {
            audio_path_id = AM_A2DP_PEQ;//to fix return fail while nothing playing
            RACE_LOG_MSGID_E("Un-supported scenario: %d\n",1,cur_type);
        }
#ifdef MTK_AWS_MCE_ENABLE
        race_dsprt_peq_get_target_bt_clk(role, &setting_mode, &target_bt_clk);
#endif
        if((role == BT_AWS_MCE_ROLE_AGENT) || (role == BT_AWS_MCE_ROLE_NONE)) {
            if(pCmd->peq_phase == PEQ_SPECIAL_PHASE) {
                ret = race_dsprt_peq_change_mode_data(0, setting_mode, target_bt_clk.nclk, pCmd->Peq_param[0], PEQ_SOUND_MODE_FORCE_DRC, audio_path_id);

                #ifndef MTK_ANC_ENABLE
                ret = race_dsprt_peq_change_mode_data(1, setting_mode, target_bt_clk.nclk, pCmd->Peq_param[0], PEQ_SOUND_MODE_FORCE_DRC, audio_path_id);
                #else
                if (pCmd->Peq_param[0] == 1) {
                    uint8_t anc_enable;
                    uint8_t hybrid_enable;
                    anc_get_status(&anc_enable, NULL, &hybrid_enable);
                    if ((anc_enable > 0) && (hybrid_enable > 0)) {
                        ret = race_dsprt_peq_change_mode_data(1, setting_mode, target_bt_clk.nclk, pCmd->Peq_param[0], PEQ_SOUND_MODE_FORCE_DRC, audio_path_id);
                    } else {
                        ret = race_dsprt_peq_change_mode_data(1, setting_mode, target_bt_clk.nclk, 0, PEQ_SOUND_MODE_UNASSIGNED, audio_path_id);
                    }
                } else {
                    ret = race_dsprt_peq_change_mode_data(1, setting_mode, target_bt_clk.nclk, pCmd->Peq_param[0], PEQ_SOUND_MODE_FORCE_DRC, audio_path_id);
                }
                #endif
            } else {
                ret = race_dsprt_peq_realtime_data(pCmd->peq_phase, setting_mode, target_bt_clk.nclk,(uint8_t *)&pCmd->Peq_param,(uint32_t)Peq_param_length, audio_path_id);
            }
        } else {
            ret = 1;
            RACE_LOG_MSGID_W("RACE_DSPREALTIME_PEQ_HDR role: 0x%x error\n",1,role);
        }

        if(ret == 0) {
            pEvt->Status = (uint8_t)RACE_ERRCODE_SUCCESS;
        } else {
            pEvt->Status = (uint8_t)RACE_ERRCODE_FAIL;
        }
    }

    return pEvt;
}
#endif

/**
 * RACE_DSPREALTIME_GET_REFRENCE_GAIN_HDR
 *
 * RACE_DSPREALTIME_GET_REFRENCE_GAIN_HDR Handler
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 *
 */
void* RACE_DSPREALTIME_GET_REFRENCE_GAIN_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    typedef struct
    {
        uint8_t status;
    }PACKED RSP;

    int32_t ret = RACE_ERRCODE_FAIL;
    RSP *pEvt = RACE_ClaimPacket(RACE_TYPE_RESPONSE,
                                 RACE_DSPREALTIME_GET_REFRENCE_GAIN,
                                 sizeof(RSP),
                                 channel_id);
    if (pEvt)
    {
        race_dsprealtime_get_refrence_gain_noti_struct *noti = NULL;

        /* A1. Execute the cmd. */
        uint8_t *ref_gain = pvPortMallocNC(sizeof(race_dsprealtime_get_refrence_gain_noti_struct));

        if (ref_gain)
        {
            hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_REQ_GET_REALTIME_REF_GAIN,
                                                  0,
                                                  hal_memview_cm4_to_infrasys((uint32_t)ref_gain),
                                                  true);
            /* A2. Create the noti. */
            noti = (void *)RACE_ClaimPacket(RACE_TYPE_NOTIFICATION,
                                            RACE_DSPREALTIME_GET_REFRENCE_GAIN,
                                            sizeof(race_dsprealtime_get_refrence_gain_noti_struct),
                                            channel_id);
            if (noti)
            {
                /* A3. Set the noti parameters with the cmd results.  */
                memcpy(noti->Data, ref_gain, sizeof(noti->Data));

                /* A4. Send the noti. */
                ret = race_noti_send(noti, channel_id, TRUE);
                if (RACE_ERRCODE_SUCCESS != ret)
                {
                    /* A5. Free the noti if needed. */
                    RACE_FreePacket(noti);
                    noti = NULL;
                }
            }
            else
            {
                ret = RACE_ERRCODE_NOT_ENOUGH_MEMORY;
            }

            vPortFreeNC(ref_gain);
        }

        pEvt->status = ret;
    }

    return pEvt;
}

#ifdef MTK_AIRDUMP_EN_MIC_RECORD
void airdump_common_timer_callback(void);
airdump_timer_result_t airdump_common_timer_create(void)
{
    airdump_timer_result_t ret = AIRDUMP_EXECUTION_SUCCESS;
    int32_t duration_ms = 8;
    if(g_airdump_common_timer == NULL) {
        g_airdump_common_timer = xTimerCreate("AirCMDumpTimer",
                                       (duration_ms / portTICK_PERIOD_MS),
                                       pdTRUE,
                                       0,
                                       (TimerCallbackFunction_t)airdump_common_timer_callback);
        if(g_airdump_common_timer == NULL) {
            RACE_LOG_MSGID_I("[AirDump][COMMON][CM4] create timer failed\n", 0);
            ret = AIRDUMP_EXECUTION_FAIL;
        }else {
            RACE_LOG_MSGID_I("[AirDump][COMMON][CM4] create timer success\n", 0);
            ret = AIRDUMP_EXECUTION_SUCCESS;
        }
    }
    return ret;
}

void airdump_common_timer_delete(void)
{
    if(g_airdump_common_timer != NULL)
    {
        if (pdPASS == xTimerDelete(g_airdump_common_timer, 0))
        {
            RACE_LOG_MSGID_I("[AirDump][COMMON]airdump_timer_delete\n", 0);
            g_airdump_common_timer = NULL;
        }
    }
}
void airdump_common_timer_callback(void)
{
#if 0
    uint32_t curr_cnt = 0;
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &curr_cnt);
    RACE_LOG_MSGID_I("[AirDump][COMMON] hisr(%d)(%d): timer start);\r\n", 2,(curr_cnt / 1000), curr_cnt);
#endif
	race_dsprealtime_airdump_common_notify_struct *pRacePkt;
#if 1
    //send race cmd back to APP
    uint32_t frameSize = 128 * sizeof(uint16_t);
    uint32_t dspBufsize = audio_record_control_get_share_buf_data_byte_count();
    record_control_result_t result = RECORD_CONTROL_EXECUTION_FAIL;
    if(dspBufsize >= frameSize){
        result = audio_record_control_read_data(&g_record_airdump_data, frameSize);
        if(result != RECORD_CONTROL_EXECUTION_SUCCESS){
            RACE_LOG_MSGID_E("[AirDump][COMMON] audio_record_control_read_data fail\n",0);
        }
        pRacePkt = RACE_ClaimPacket(RACE_TYPE_NOTIFICATION, (uint16_t)RACE_DSPREALTIME_AIRDUMP_ENTRY, sizeof(race_dsprealtime_airdump_common_notify_struct), g_airdump_common_race_ch_id);
        pRacePkt->airdump_id = Airdump_mic_record;
        pRacePkt->param.air_dump_mic_record_notify.Data_sequence_number = g_airdump_common_cnt;
        pRacePkt->param.air_dump_mic_record_notify.Data_size_per_sample = 2;
        pRacePkt->param.air_dump_mic_record_notify.Dump_Request = g_airdump_common_race_request;
        pRacePkt->param.air_dump_mic_record_notify.Data_length = 128;
        memcpy((int16_t *)pRacePkt->airdump_data, (int16_t *)&g_record_airdump_data, pRacePkt->param.air_dump_mic_record_notify.Data_length * sizeof(uint16_t) * 2);
        race_flush_packet((void *)pRacePkt, g_airdump_common_race_ch_id);
    } else {
        RACE_LOG_MSGID_I("[AirDump][COMMON] Airdump bufer size low %d", 1, dspBufsize);
        return;
    }
#else
    pRacePkt = RACE_ClaimPacket(RACE_TYPE_NOTIFICATION, (uint16_t)RACE_DSPREALTIME_AIRDUMP_ENTRY, sizeof(race_dsprealtime_airdump_common_notify_struct), g_airdump_common_race_ch_id);
    if(pRacePkt != NULL)
    {
        pRacePkt->airdump_id = Airdump_mic_record;
        pRacePkt->param.air_dump_mic_record_notify.Data_sequence_number = g_airdump_common_cnt;
        pRacePkt->param.air_dump_mic_record_notify.Data_size_per_sample = 2;
        pRacePkt->param.air_dump_mic_record_notify.Dump_Request = g_airdump_common_race_request;
        pRacePkt->param.air_dump_mic_record_notify.Data_length = 128;
        memcpy((int16_t *)pRacePkt->airdump_data, (int16_t *)&g_128_1ktone, pRacePkt->param.air_dump_mic_record_notify.Data_length * sizeof(uint16_t));
        memcpy((int16_t *)pRacePkt->airdump_data + (pRacePkt->param.air_dump_mic_record_notify.Data_length), (uint8_t *)&g_128_2ktone, pRacePkt->param.air_dump_mic_record_notify.Data_length * sizeof(uint16_t));
        race_flush_packet((void *)pRacePkt, g_airdump_common_race_ch_id);
    }
#endif
    g_airdump_common_cnt++;
#if 0
    /*For debug dump.*/
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &curr_cnt);
    RACE_LOG_MSGID_I("[AirDump][COMMON] hisr(%d)(%d): timer end);\r\n", 2, (curr_cnt / 1000), curr_cnt);
#endif
}

/**
 * RACE_DSPREALTIME_AIRDUMP_COMMON_HDR
 *
 * RACE_DSPREALTIME_AIRDUMP_COMMON_HDR Handler
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 *
 */
void* RACE_DSPREALTIME_AIRDUMP_COMMON_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    race_dsprealtime_airdump_common_response_struct *pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE,
                                                                   (uint16_t)RACE_DSPREALTIME_AIRDUMP_ENTRY,
                                                                   (uint16_t)sizeof(race_dsprealtime_airdump_common_response_struct),
                                                                   channel_id);
    g_airdump_common_race_ch_id = channel_id;
    record_control_result_t Airdump_common_ret = RECORD_CONTROL_EXECUTION_FAIL;
    if (pEvt)
    {
        race_dsprealtime_airdump_common_request_struct *pAirdump_common_Cmd;
        //uint16_t Airdump_common_param_length;
        pAirdump_common_Cmd = (race_dsprealtime_airdump_common_request_struct *)pCmdMsg->payload;
        memset(pEvt, 0, sizeof(race_dsprealtime_airdump_common_response_struct));
        RACE_LOG_MSGID_I("RACE_DSPREALTIME_AIRDUMP_COMMON_HDR payload: %d %d %d\n", 3, pAirdump_common_Cmd->airdump_id, pAirdump_common_Cmd->param.air_dump_mic_record_request.Dump_Enable, pAirdump_common_Cmd->param.air_dump_mic_record_request.Dump_Request);
        switch (pAirdump_common_Cmd->airdump_id) {
            case Airdump_mic_record:
                if(pAirdump_common_Cmd->param.air_dump_mic_record_request.Dump_Enable == 1){
                    g_airdump_common_race_request = pAirdump_common_Cmd->param.air_dump_mic_record_request.Dump_Request;
                    hal_audio_set_dvfs_control(HAL_AUDIO_DVFS_MAX_SPEED, HAL_AUDIO_DVFS_LOCK);
                    /*For debug dump.*/
                    //g_dump = true;
                    g_record_airdump = true;
                    Airdump_common_ret = audio_record_control_airdump(true , pAirdump_common_Cmd->param.air_dump_mic_record_request.Dump_Request);
                    airdump_common_timer_create();
                    xTimerStart(g_airdump_common_timer, 0);
                } else if(pAirdump_common_Cmd->param.air_dump_mic_record_request.Dump_Enable == 0){
                    Airdump_common_ret = audio_record_control_airdump(false, pAirdump_common_Cmd->param.air_dump_mic_record_request.Dump_Request);
                    airdump_common_timer_delete();
                    /*For debug dump.*/
                    //g_dump = false;
                    g_record_airdump = false;
                    hal_audio_set_dvfs_control(HAL_AUDIO_DVFS_MAX_SPEED, HAL_AUDIO_DVFS_UNLOCK);
                    g_airdump_common_cnt = 0;
                } else {
                    pEvt->status = RACE_ERRCODE_PARAMETER_ERROR;
                    return pEvt;
                }
                break;
            default:
                pEvt->status = RACE_ERRCODE_PARAMETER_ERROR;
                return pEvt;
        }
        pEvt->status = (Airdump_common_ret == RECORD_CONTROL_EXECUTION_SUCCESS) ? RACE_ERRCODE_SUCCESS : RACE_ERRCODE_FAIL;
    }
    RACE_LOG_MSGID_E("RACE_DSPREALTIME_AIRDUMP_COMMON_HDR pEvt->status: %d  Airdump_common_ret: %d\n",2,pEvt->status,Airdump_common_ret);
    return pEvt;
}
#endif

#ifdef MTK_AIRDUMP_EN
airdump_timer_result_t airdump_timer_create(void)
{
    airdump_timer_result_t ret = AIRDUMP_EXECUTION_SUCCESS;
    int32_t duration = 20000;
    if(g_airdump_timer == NULL) {
        g_airdump_timer = xTimerCreate("AirDumpTimer",
                                       (duration / 1000 / portTICK_PERIOD_MS),
                                       pdTRUE,
                                       0,
                                       (TimerCallbackFunction_t)airdump_timer_callback);
        if(g_airdump_timer == NULL) {
            RACE_LOG_MSGID_I("[AirDump][CM4] create timer failed\n", 0);
            ret = AIRDUMP_EXECUTION_FAIL;
        }else {
            RACE_LOG_MSGID_I("[AirDump][CM4] create timer success\n", 0);
            ret = AIRDUMP_EXECUTION_SUCCESS;
        }
    }
    return ret;
}

void airdump_timer_delete(void)
{
    if(g_airdump_timer != NULL)
    {
        if (pdPASS == xTimerDelete(g_airdump_timer, 0))
        {
            RACE_LOG_MSGID_I("[AirDump] airdump_timer_delete\n", 0);
            g_airdump_timer = NULL;
            hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_AIRDUMP_EN, 0, 0, false);
        }
    }
}

void airdump_timer_callback(void)
{
    //RACE_LOG_MSGID_I("[AirDump][CM4] airdump_timer_callback start\n", 0);
    AIRDUMP_BUFFER_INFO *buffer;
    buffer = (AIRDUMP_BUFFER_INFO *)hal_audio_query_hfp_air_dump();
    uint8_t airdump_cnt_now = buffer->notify_count;
    if(g_airdump_cnt_past != airdump_cnt_now)
    {
        //send race cmd back to APP
        RACE_AirDump_Send((uint8_t)g_airdump_race_ch_id,
                          (uint8_t)RACE_TYPE_NOTIFICATION,
                          (uint8_t *)(&buffer->data[0] + buffer->read_offset),
                          (uint16_t)sizeof(RACE_DSPREALTIME_AIRDUMP_EVT_NOTI_STRU));

        uint32_t ro = (buffer->read_offset + AEC_AIRDUMP_FRAME_SIZE*AEC_AIRDUMP_FRAME_NUM_HALF)%buffer->length; // move frame size
        buffer->read_offset = ro;
        //RACE_LOG_MSGID_I("[AirDump][CM4] update ro: %d, wo:%d, cnt_past:%d, cnt_now:%d",2,buffer->read_offset, buffer->write_offset, g_airdump_cnt_past, airdump_cnt_now);
        g_airdump_cnt_past = airdump_cnt_now;
    }
}

void RACE_AirDump_Send(uint8_t channelId,uint8_t RaceType, uint8_t *ptr, uint16_t len)
{
	RACE_DSPREALTIME_AIRDUMP_EVT_NOTI_STRU *pRacePkt = RACE_ClaimPacket(RaceType, (uint16_t)RACE_DSPREALTIME_AIRDUMP_ON_OFF, len, channelId);
	if(pRacePkt != NULL)
	{
		memcpy((uint8_t *)pRacePkt->data, ptr, len);
		race_flush_packet((void *)pRacePkt, g_airdump_race_ch_id);
	}
}

/**
 * RACE_DSPREALTIME_AIRDUMP_HDR
 *
 * RACE_DSPREALTIME_AIRDUMP_HDR Handler
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 *
 */
void* RACE_DSPREALTIME_AIRDUMP_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    g_airdump_race_ch_id = channel_id;
    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint8_t AirDumpEn;
    }PACKED RACE_DSPREALTIME_AIRDUMP_CMD_STRU;

    typedef struct
    {
        uint8_t status;
    }PACKED RACE_DSPREALTIME_AIRDUMP_EVT_RES_STRU;

    RACE_DSPREALTIME_AIRDUMP_EVT_RES_STRU *pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE,
                                                                   (uint16_t)RACE_DSPREALTIME_AIRDUMP_ON_OFF,
                                                                   (uint16_t)sizeof(RACE_DSPREALTIME_AIRDUMP_EVT_RES_STRU),
                                                                   channel_id);
    if (pEvt)
    {
        RACE_DSPREALTIME_AIRDUMP_CMD_STRU *pCmd = (RACE_DSPREALTIME_AIRDUMP_CMD_STRU *)pCmdMsg;

        if(pCmd)
        {
            if(pCmd->AirDumpEn == 1)
            {
                RACE_LOG_MSGID_I("[RACE_DSPREALTIME_AIRDUMP_HDR] Start, AirDumpEn:%d\n",1, pCmd->AirDumpEn);
                bt_sink_srv_am_type_t type = bt_sink_srv_ami_get_current_scenario();
                if(type == HFP)
                {
                    airdump_timer_create();
                    xTimerStart(g_airdump_timer, 0);
                    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_AIRDUMP_EN, 0, pCmd->AirDumpEn, false);
                } else {
                    RACE_LOG_MSGID_I("[RACE_DSPREALTIME_AIRDUMP_HDR] Please Start eSCO first",0);
                }
            }
            else if(pCmd->AirDumpEn == 0)
            {
                RACE_LOG_MSGID_I("[RACE_DSPREALTIME_AIRDUMP_HDR] Stop, AirDumpEn:%d\n",1, pCmd->AirDumpEn);
                airdump_timer_delete();
                //hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_AIRDUMP_EN, 0, pCmd->AirDumpEn, false);
            }
        }

        pEvt->status = (uint8_t)RACE_ERRCODE_SUCCESS;
    }

    return pEvt;
}
#endif


/**
 * RACE_DSPREALTIME_2MIC_SWAP_HDR
 *
 * RACE_DSPREALTIME_2MIC_SWAP_HDR Handler
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 *
 */
void* RACE_DSPREALTIME_2MIC_SWAP_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint8_t MicSwapSel;
    }PACKED RACE_DSPREALTIME_MIC_SWAP_CMD_STRU;

    typedef struct
    {
        uint8_t status;
    }PACKED RACE_DSPREALTIME_MIC_SWAP_EVT_RES_STRU;

    RACE_DSPREALTIME_MIC_SWAP_EVT_RES_STRU *pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE,
                                                                   (uint16_t)RACE_DSPREALTIME_2MIC_SWAP,
                                                                   (uint16_t)sizeof(RACE_DSPREALTIME_MIC_SWAP_EVT_RES_STRU),
                                                                   channel_id);
    if (pEvt)
    {
        RACE_DSPREALTIME_MIC_SWAP_CMD_STRU *pCmd = (RACE_DSPREALTIME_MIC_SWAP_CMD_STRU *)pCmdMsg;

        if(pCmd)
        {
            if(pCmd->MicSwapSel == 1)
            {
                RACE_LOG_MSGID_I("[RACE_DSPREALTIME_MIC_SWAP] SWAP to R MIC, MicSwapSel:%d\n",1, pCmd->MicSwapSel);
                hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_CHANGE_DSP_CHANEL, 1, MIC_R_ONLY, false);
                }
            else if(pCmd->MicSwapSel == 0)
            {
                RACE_LOG_MSGID_I("[RACE_DSPREALTIME_MIC_SWAP] SWAP to L MIC, MicSwapSel:%d\n",1, pCmd->MicSwapSel);
                hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_CHANGE_DSP_CHANEL, 1, MIC_L_ONLY, false);
            }
#if defined(HAL_AUDIO_SUPPORT_MULTIPLE_MICROPHONE)
            else if(pCmd->MicSwapSel == 2)
            {
                RACE_LOG_MSGID_I("[RACE_DSPREALTIME_MIC_SWAP] SWAP to L MIC, MicSwapSel:%d\n",1, pCmd->MicSwapSel);
                hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_CHANGE_DSP_CHANEL, 1, MIC_3_ONLY, false);
            }
#endif
        }

        pEvt->status = (uint8_t)RACE_ERRCODE_SUCCESS;
    }

    return pEvt;
}

/**
 * RACE_DSPREALTIME_OPEN_ALL_MIC_HDR
 *
 * RACE_DSPREALTIME_OPEN_ALL_MIC_HDR Handler
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 * @channel_id : race cmd channel id
 *
 */
void* RACE_DSPREALTIME_OPEN_ALL_MIC_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint8_t OpenAllMic;
    }PACKED RACE_DSPREALTIME_OPEN_ALL_MIC_CMD_STRU;

    typedef struct
    {
        uint8_t status;
    }PACKED RACE_DSPREALTIME_OPEN_ALL_MIC_EVT_RES_STRU;

    RACE_DSPREALTIME_OPEN_ALL_MIC_EVT_RES_STRU *pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE,
                                                                   (uint16_t)RACE_DSPREALTIME_OPEN_ALL_MIC,
                                                                   (uint16_t)sizeof(RACE_DSPREALTIME_OPEN_ALL_MIC_EVT_RES_STRU),
                                                                   channel_id);
    if (pEvt)
    {
        RACE_DSPREALTIME_OPEN_ALL_MIC_CMD_STRU *pCmd = (RACE_DSPREALTIME_OPEN_ALL_MIC_CMD_STRU *)pCmdMsg;

        if(pCmd)
        {
            if(pCmd->OpenAllMic == 1)
            {
                if(AUD_EXECUTION_SUCCESS != ami_set_audio_device(STREAM_IN, AU_DSP_VOICE, HAL_AUDIO_DEVICE_MAIN_MIC_DUAL, HAL_AUDIO_INTERFACE_1, NOT_REWRITE)){
                    RACE_LOG_MSGID_E("[RACE_DSPREALTIME_OPEN_ALL_MIC] Open All Mic Fail!!!!!!",0);
                } else {
                    RACE_LOG_MSGID_I("[RACE_DSPREALTIME_OPEN_ALL_MIC] Open All Mic, OpenAllMic:%d\n",1, pCmd->OpenAllMic);
            }
            }
            else if(pCmd->OpenAllMic == 0)
            {
                /*if(AUD_EXECUTION_FAIL == audio_nvdm_configure_init()){
                    RACE_LOG_MSGID_E("RACE_DSPREALTIME_OPEN_ALL_MIC]] Default Mic Setting, OpenAllMic Faill!!!!!!",0);
                }*/
            }
        }

        pEvt->status = (uint8_t)RACE_ERRCODE_SUCCESS;
    }

    return pEvt;
}

/**
 * RACE_DSPREALTIME_AECNR_ON_OFF_HDR
 *
 * RACE_DSPREALTIME_AECNR_ON_OFF_HDR Handler
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 *
 */
void* RACE_DSPREALTIME_AECNR_ON_OFF_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint8_t AecNrEn;
    }PACKED RACE_DSPREALTIME_AECNR_ON_OFF_CMD_STRU;

    typedef struct
    {
        uint8_t status;
    }PACKED RACE_DSPREALTIME_AECNR_ON_OFF_EVT_RES_STRU;

    RACE_DSPREALTIME_AECNR_ON_OFF_EVT_RES_STRU *pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE,
                                                                   (uint16_t)RACE_DSPREALTIME_AECNR_ON_OFF,
                                                                   (uint16_t)sizeof(RACE_DSPREALTIME_AECNR_ON_OFF_EVT_RES_STRU),
                                                                   channel_id);
    if (pEvt)
    {
        RACE_DSPREALTIME_AECNR_ON_OFF_CMD_STRU *pCmd = (RACE_DSPREALTIME_AECNR_ON_OFF_CMD_STRU *)pCmdMsg;

        if(pCmd)
        {
            if(pCmd->AecNrEn == 1)
            {
                RACE_LOG_MSGID_I("[RACE_DSPREALTIME_AECNR_ON_OFF_HDR] AECNR on, AecNrEn:%d\n",1, pCmd->AecNrEn);
                hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_AEC_NR_EN, 0, AEC_NR_ENABLE, false);
            }
            else if(pCmd->AecNrEn == 0)
            {
                RACE_LOG_MSGID_I("[RACE_DSPREALTIME_AECNR_ON_OFF_HDR] AECNR off, AecNrEn:%d\n",1, pCmd->AecNrEn);
                hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_AEC_NR_EN, 0, AEC_NR_DISABLE, false);
            }
        }

        pEvt->status = (uint8_t)RACE_ERRCODE_SUCCESS;
    }

    return pEvt;
}

/**
 * RACE_DSPREALTIME_SWGAIN_BYPASS_HDR
 *
 * RACE_DSPREALTIME_SWGAIN_BYPASS_HDR Handler
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 *
 */
void* RACE_DSPREALTIME_SWGAIN_EN_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint8_t SW_GAIN_EN;
    }PACKED RACE_DSPREALTIME_SWGAIN_EN_CMD_STRU;

    typedef struct
    {
        uint8_t status;
    }PACKED RACE_DSPREALTIME_SWGAIN_EN_EVT_RES_STRU;

    RACE_DSPREALTIME_SWGAIN_EN_EVT_RES_STRU *pEvt = RACE_ClaimPacket((uint8_t)RACE_TYPE_RESPONSE,
                                                                   (uint16_t)RACE_DSPREALTIME_SWGAIN_BYPASS,
                                                                   (uint16_t)sizeof(RACE_DSPREALTIME_SWGAIN_EN_EVT_RES_STRU),
                                                                   channel_id);
    if (pEvt)
    {
        RACE_DSPREALTIME_SWGAIN_EN_CMD_STRU *pCmd = (RACE_DSPREALTIME_SWGAIN_EN_CMD_STRU *)pCmdMsg;

        if(pCmd)
        {
            if(pCmd->SW_GAIN_EN == 0)
            {
                RACE_LOG_MSGID_I("[RACE_DSPREALTIME_SWGAIN_BYPASS] Bypass SW Gain, ByPassStatus:%d\n",1, pCmd->SW_GAIN_EN);
                hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_SW_GAIN_EN, 0, SW_GAIN_DISABLE, false);
            } else if(pCmd->SW_GAIN_EN == 1)
            {
                RACE_LOG_MSGID_I("[RACE_DSPREALTIME_SWGAIN_BYPASS] Don't Bypass SW Gain, ByPassStatus:%d\n",1, pCmd->SW_GAIN_EN);
                hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_SW_GAIN_EN, 0, SW_GAIN_ENABLE, false);
            }
        }

        pEvt->status = (uint8_t)RACE_ERRCODE_SUCCESS;
    }

    return pEvt;
}

#ifdef MTK_ANC_ENABLE
#ifndef MTK_ANC_V2
/**
 * RACE_DSPREALTIME_ANC_HDR
 *
 * RACE_DSPREALTIME_ANC_HDR Handler
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 *
 */
void* RACE_DSPREALTIME_ANC_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    race_dsprealtime_anc_struct *pEvt = RACE_ClaimPacket(RACE_TYPE_RESPONSE,
                                                         RACE_DSPREALTIME_ANC,
                                                         sizeof(race_dsprealtime_anc_struct),
                                                         channel_id);
    g_anc_race_ch_id = channel_id;
    if (pEvt)
    {
        race_dsprealtime_anc_struct *pAnc_cmd;
        anc_control_result_t anc_ret = ANC_CONTROL_EXECUTION_NONE;
        uint16_t anc_param_length;

        pAnc_cmd = (race_dsprealtime_anc_struct *)pCmdMsg->payload;
        memset(pEvt, 0, sizeof(race_dsprealtime_anc_struct));
        pEvt->anc_id = pAnc_cmd->anc_id;

        RACE_LOG_MSGID_I("RACE_DSPREALTIME_ANC_HDR msg_length:%d payload: %d %d %d\n",4,pCmdMsg->hdr.length,pAnc_cmd->status,pAnc_cmd->anc_id,pAnc_cmd->param.anc_on_param.anc_filter_type);

        if (pCmdMsg->hdr.length >= 4) { //hdr.length = hdr.id(2bytes) + pAnc_cmd->status(1byte) + pAnc_cmd->anc_id(1byte) + pAnc_cmd->param(0/1/2/4 bytes)
            anc_param_length = pCmdMsg->hdr.length - 4;
            switch (pAnc_cmd->anc_id) {
                case RACE_ANC_ON:
                    if (anc_param_length > 1) {
                        anc_filter_type_t filter_type = (anc_filter_type_t)pAnc_cmd->param.anc_on_param.anc_filter_type;
                        pEvt->param.anc_on_param = pAnc_cmd->param.anc_on_param;
                        if (pAnc_cmd->param.anc_on_param.anc_mode == 1) {
                            filter_type |= ANC_FF_ONLY_BIT_MASK;
                        } else if (pAnc_cmd->param.anc_on_param.anc_mode == 2) {
                            filter_type |= ANC_FB_ONLY_BIT_MASK;
                        }
                        race_anc_mutex_take();
                        g_anc_notify_on = ((uint32_t)filter_type << 8);
                        g_anc_notify_off = 0;
                        race_anc_mutex_give();
                        RACE_LOG_MSGID_I("MP_Step__1__ set anc on: 0x%x",1,filter_type);
                        anc_ret = audio_anc_enable(filter_type, ANC_UNASSIGNED_GAIN, NULL);
                    } else if (anc_param_length > 0) {
                        anc_filter_type_t filter_type = (anc_filter_type_t)pAnc_cmd->param.anc_on_param.anc_filter_type;
                        pEvt->param.anc_on_param.anc_filter_type = pAnc_cmd->param.anc_on_param.anc_filter_type;
                        race_anc_mutex_take();
                        g_anc_notify_on = ((uint32_t)filter_type << 8);
                        g_anc_notify_off = 0;
                        race_anc_mutex_give();
                        anc_ret = audio_anc_enable(filter_type, ANC_UNASSIGNED_GAIN, NULL);
                    }
                    break;
                case RACE_ANC_OFF:
                    race_anc_mutex_take();
                    g_anc_notify_off |= (1 << 31);
                    g_anc_notify_on = 0;
                    race_anc_mutex_give();
                    RACE_LOG_MSGID_I("MP_Step__1__ set anc off",0);
                    anc_ret = audio_anc_disable(NULL);
                    if (anc_get_sync_time() == 0) {
                        RACE_FreePacket(pEvt);
                        return NULL;
                    }
                    break;
                case RACE_ANC_SET_VOL:
                    if (anc_param_length >= 4) {
                        pEvt->param.gain.val = pAnc_cmd->param.gain.val;
                        anc_ret = audio_anc_set_volume(pAnc_cmd->param.gain, TO_BOTH);
                    }
                    break;
                case RACE_ANC_SET_RUNTIME_VOL:
                    if (anc_param_length >= 2) {
                        pEvt->param.runtime_gain = pAnc_cmd->param.runtime_gain;
                        anc_ret = audio_anc_set_runtime_volume(pAnc_cmd->param.runtime_gain);
                    }
                    break;
                case RACE_ANC_READ_NVDM:
                    anc_ret = audio_anc_read_volume_nvdm(&pEvt->param.gain, TO_AGENT);
                    break;
                case RACE_ANC_WRITE_NVDM:
                    if (anc_param_length >= 4) {
                        pEvt->param.gain.val = pAnc_cmd->param.gain.val;
                        anc_ret = audio_anc_write_volume_nvdm(&pAnc_cmd->param.gain, TO_AGENT);
                    }
                    break;
#ifdef MTK_AWS_MCE_ENABLE
                case RACE_ANC_SET_AGENT_VOL:
                    if (anc_param_length >= 4) {
                        pEvt->param.gain.val = pAnc_cmd->param.gain.val;
                        race_anc_mutex_take();
                        g_anc_set_agent_vol = pAnc_cmd->param.gain.val;
                        race_anc_mutex_give();
                        RACE_LOG_MSGID_I("MP_Step__1__ set A gain: 0x%x",1,pAnc_cmd->param.gain.val);
                        anc_ret = audio_anc_set_volume(pAnc_cmd->param.gain, TO_AGENT);
                        if (anc_get_sync_time() == 0) {
                            RACE_FreePacket(pEvt);
                            return NULL;
                        }
                    }
                    break;
                case RACE_ANC_SET_PARTNER_VOL:
                    if (anc_param_length >= 4) {
                        pEvt->param.gain.val = pAnc_cmd->param.gain.val;
                        race_anc_mutex_take();
                        g_anc_set_partner_vol = pAnc_cmd->param.gain.val;
                        race_anc_mutex_give();
                        RACE_LOG_MSGID_I("MP_Step__4__ set P gain: 0x%x",1,pAnc_cmd->param.gain.val);
                        anc_ret = audio_anc_set_volume(pAnc_cmd->param.gain, TO_PARTNER);
                        if (anc_get_sync_time() == 0) {
                            RACE_FreePacket(pEvt);
                            return NULL;
                        }
                    }
                    break;
                case RACE_ANC_READ_PARTNER_NVDM:
                    if (pAnc_cmd->status == 0) {
                        uint32_t pktId = (uint32_t)pCmdMsg->hdr.pktId.value;
                        if ((bt_sink_srv_cm_get_special_aws_device() != NULL) && (bt_sink_srv_cm_get_aws_link_state() == BT_AWS_MCE_AGENT_STATE_ATTACHED)) {
                            anc_ret = anc_send_aws_mce_param(ANC_DIRECT, ANC_CONTROL_EVENT_READ_VOLUME_NVDM, 0, ((pktId << 8) | channel_id));
                            RACE_FreePacket(pEvt);
                            return NULL;
                        } else {
                            RACE_LOG_MSGID_E("ANC read partner nvdm error : bt power off or aws link state isn't attached", 0);
                            pEvt->status = RACE_ERRCODE_FAIL;
                            return pEvt;
                        }
                    } else {
                        uint8_t true_channel_id = pAnc_cmd->status & (~0x80);
                        RACE_LOG_MSGID_I("anc get partner's vol nvdm: %d %d\n", 2, pAnc_cmd->param.gain.gain_index_l, pAnc_cmd->param.gain.gain_index_r);
                        pEvt->status = 0;
                        pEvt->param.gain.val = pAnc_cmd->param.gain.val;
                        race_flush_packet((void *)pEvt, true_channel_id);
                        return NULL;
                    }
                case RACE_ANC_WRITE_PARTNER_NVDM:
                    if (anc_param_length >= 4) {
                        pEvt->param.gain.val = pAnc_cmd->param.gain.val;
                        anc_ret = audio_anc_write_volume_nvdm(&pAnc_cmd->param.gain, TO_PARTNER);
                    }
                    break;
                case RACE_ANC_SET_SYNC_TIME:
                    if (anc_param_length >= 2) {
                        pEvt->param.u2param = pAnc_cmd->param.u2param;
                        anc_set_sync_time(pAnc_cmd->param.u2param);
                        anc_ret = ANC_CONTROL_EXECUTION_SUCCESS;
                        if ((bt_sink_srv_cm_get_special_aws_device() != NULL) && (bt_sink_srv_cm_get_aws_link_state() == BT_AWS_MCE_AGENT_STATE_ATTACHED)) {
                            anc_ret = anc_send_aws_mce_param(ANC_DIRECT, ANC_CONTROL_EVENT_SET_SYNC_TIME, (uint32_t)pAnc_cmd->param.u2param, 0);
                        } else {
                            RACE_LOG_MSGID_E("ANC set sync time error : bt power off or aws link state isn't attached", 0);
                            pEvt->status = RACE_ERRCODE_FAIL;
                            return pEvt;
                        }
                    }
                    break;
#endif
                case RACE_ANC_GET_HYBRID_CAP:
                    anc_ret = anc_get_hybrid_capability(&pEvt->param.hybrid_cap);
                    break;
                default:
                    pEvt->status = RACE_ERRCODE_NOT_SUPPORT;
                    return pEvt;
            }
            pEvt->status = (anc_ret == ANC_CONTROL_EXECUTION_SUCCESS) ? RACE_ERRCODE_SUCCESS : RACE_ERRCODE_FAIL;
        }
        else
        {
            pEvt->status = RACE_ERRCODE_PARAMETER_ERROR;
        }
        if(pEvt->status != RACE_ERRCODE_SUCCESS) {
            RACE_LOG_MSGID_E("RACE_DSPREALTIME_ANC_HDR pEvt->status: %d  anc_ret: %d\n",2,pEvt->status,anc_ret);
        }
    }

    return pEvt;
}

/**
 * RACE_DSPREALTIME_PASSTHRU_HDR
 *
 * RACE_DSPREALTIME_PASSTHRU_HDR Handler
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 *
 */
void* RACE_DSPREALTIME_PASSTHRU_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint8_t enable;
    }PACKED RACE_DSPREALTIME_PASSTHRU_CMD_STRU;

    typedef struct
    {
        uint8_t status;
    }PACKED RACE_DSPREALTIME_PASSTHRU_EVT_STRU;

    RACE_DSPREALTIME_PASSTHRU_CMD_STRU *pCmd = (RACE_DSPREALTIME_PASSTHRU_CMD_STRU *)pCmdMsg;
    RACE_DSPREALTIME_PASSTHRU_EVT_STRU *pEvt = RACE_ClaimPacket(RACE_TYPE_RESPONSE,
                                                         pCmd->Hdr.id,
                                                         sizeof(RACE_DSPREALTIME_PASSTHRU_EVT_STRU),
                                                         channel_id);
    if (pEvt)
    {
        anc_control_result_t anc_ret = ANC_CONTROL_EXECUTION_NONE;
        if (pCmd->Hdr.id == RACE_DSPREALTIME_PASS_THROUGH_ON_OFF) {
            if (pCmd->enable == 0) {
                uint8_t enable;
                uint32_t runtime_info;
                anc_get_status(&enable, &runtime_info, NULL);
                if ((enable == 1) && ((runtime_info & ANC_FILTER_TYPE_MASK) != FILTER_5)) {
                    anc_ret = ANC_CONTROL_EXECUTION_SUCCESS;
                } else {
                    anc_ret = audio_anc_disable(NULL);
                }
            } else if (pCmd->enable == 1) {
                anc_ret = audio_anc_enable(FILTER_5, ANC_UNASSIGNED_GAIN, NULL);
            }
        }
        pEvt->status = anc_ret;
    }

    return pEvt;
}

void* RACE_DSPREALTIME_PASSTHRU_TEST_MUTE_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint8_t LR_mic;
    }PACKED RACE_DSPREALTIME_PASSTHRU_TEST_CMD_STRU;

    typedef struct
    {
        uint8_t status;
    }PACKED RACE_DSPREALTIME_PASSTHRU_TEST_EVT_RES_STRU;

    typedef struct
    {
        uint32_t unknown;
        int db_value;
        uint32_t freq;
    }PACKED RACE_DSPREALTIME_PASSTHRU_TEST_NOTIFICATION_STRU;


    int32_t ret = RACE_ERRCODE_FAIL;
    RACE_DSPREALTIME_PASSTHRU_TEST_EVT_RES_STRU *pEvt = RACE_ClaimPacket(RACE_TYPE_RESPONSE,
                                 RACE_DSPREALTIME_PASS_THROUGH_TEST,
                                 sizeof(RACE_DSPREALTIME_PASSTHRU_TEST_EVT_RES_STRU),
                                 channel_id);
    RACE_DSPREALTIME_PASSTHRU_TEST_CMD_STRU *pCmd = (RACE_DSPREALTIME_PASSTHRU_TEST_CMD_STRU *)pCmdMsg;

    if (pEvt)
    {
        PTT_u4Freq_Mag_data result = {0, 0};

        if (pCmd)
        {
            RACE_DSPREALTIME_PASSTHRU_TEST_NOTIFICATION_STRU *noti = NULL;

            /* A1. Execute the cmd. */
            pass_through_test(PTT_AMIC_DCC, pCmd->LR_mic, PTT_MUTE, &result);
            RACE_LOG_MSGID_I("PASS_THROUGH_TEST, Freq=%d, Mag=%d, db=%f\r\n", 3, result.freq_data, result.mag_data, result.db_data);

            /* A2. Create the noti. */
            noti = (void *)RACE_ClaimPacket(RACE_TYPE_NOTIFICATION,
                                            RACE_DSPREALTIME_PASS_THROUGH_TEST,
                                            sizeof(RACE_DSPREALTIME_PASSTHRU_TEST_NOTIFICATION_STRU),
                                            channel_id);
            if (noti)
            {
                /* A3. Set the noti parameters with the cmd results.  */
                noti->db_value = (int)round(result.db_data);
                noti->freq = result.freq_data;

                /* A4. Send the noti. */
                ret = race_noti_send(noti, channel_id, TRUE);
                if (RACE_ERRCODE_SUCCESS != ret)
                {
                    /* A5. Free the noti if needed. */
                    RACE_FreePacket(noti);
                    noti = NULL;
                    pEvt->status = ret;
                    return pEvt;
                }
            }
            else
            {
                ret = RACE_ERRCODE_NOT_ENOUGH_MEMORY;
                pEvt->status = ret;
                return pEvt;
            }


        }

        pEvt->status = ret;
    }

    return pEvt;

}

void* RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint8_t mode;
        uint8_t enable;
    }PACKED RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_CMD_STRU;

    typedef struct
    {
        uint8_t status;
        uint8_t mode;
        uint8_t enable;
    }PACKED RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_EVT_STRU;

    RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_CMD_STRU *pCmd = (RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_CMD_STRU *)pCmdMsg;
    RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_EVT_STRU *pEvt = NULL;

    g_anc_race_ch_id = channel_id;
    switch (pCmd->mode) {
#ifdef MTK_LEAKAGE_DETECTION_ENABLE
        case LEAKAGE_DETECTION_MODE:
        {
            race_anc_mutex_take();
            g_LD_result_agent = 0;
            g_LD_result_partner = 0;
            race_anc_mutex_give();
            pEvt = RACE_ClaimPacket(RACE_TYPE_RESPONSE, pCmd->Hdr.id, sizeof(RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_EVT_STRU) , channel_id);
            if (pEvt) {
                pEvt->mode = LEAKAGE_DETECTION_MODE;
                pEvt->enable = pCmd->enable;
                pEvt->status = 0;
                if (pCmd->enable == 1) {
                    if (bt_sink_srv_ami_get_current_scenario() == HFP) {
                        RACE_LOG_MSGID_I("[RECORD_LC] anc leakage detection is terminated by HFP",0);
                        race_flush_packet((void *)pEvt, g_anc_race_ch_id);
                        anc_leakage_detection_racecmd_response(LD_STATUS_TERMINATE, 0);
                        anc_leakage_detection_racecmd_response(LD_STATUS_TERMINATE, 1);
                        pEvt = NULL;
                    } else {
                        anc_control_result_t anc_ret = audio_anc_leakage_detection_start(anc_leakage_detection_racecmd_callback);
                        if (anc_ret == ANC_CONTROL_EXECUTION_SUCCESS) {
                            race_cmd_vp_struct* param = (race_cmd_vp_struct*)race_mem_alloc(sizeof(race_cmd_vp_struct));
                            if(param){
                                param->vp_type = RACE_CMD_VP_LEAKAGE_DETECTION;
                                param->play_flag = true;
                                RACE_ERRCODE ret = race_send_event_notify_msg(RACE_EVENT_TYPE_CMD_VP, param);
                                if(RACE_ERRCODE_SUCCESS != ret){
                                    race_mem_free(param);
                                }
                            }
                        } else {
                            anc_leakage_detection_racecmd_response(LD_STATUS_TERMINATE, 0);
                            RACE_LOG_MSGID_I("[RECORD_LC] anc leakage detection is not allowed:%d",1, anc_ret);
                        }
                        pEvt->status = (anc_ret == ANC_CONTROL_EXECUTION_SUCCESS) ? RACE_ERRCODE_SUCCESS : RACE_ERRCODE_FAIL;
                    }
                } else {
                    anc_control_result_t anc_ret = audio_anc_leakage_detection_stop();
                    pEvt->status = (anc_ret == ANC_CONTROL_EXECUTION_SUCCESS) ? RACE_ERRCODE_SUCCESS : RACE_ERRCODE_FAIL;
                    RACE_LOG_MSGID_I("[RECORD_LC] anc leakage detection is terminated by end-user",0);
                }
            }
            break;
        }
#endif
#ifdef MTK_USER_TRIGGER_FF_ENABLE
        case ADAPTIVE_FF_ANC_MODE:
        {
            if (pCmd->enable) {
                /*
                hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &user_trigger_ff_start_count);
                RACE_LOG_MSGID_I("[user_trigger_ff]race cmd user trigger adaptive FF start, start count:%d", 1, user_trigger_ff_start_count);
                audio_anc_user_trigger_ff_start(anc_user_trigger_adaptive_ff_callback);
                */
                race_send_event_notify_msg(RACE_EVENT_TYPE_ANC_ADAPTIVE_FF_START, NULL); //Add new api to app do start
                RACE_FreePacket(pEvt);
                pEvt = NULL;
            } else {/*cancelled by user*/
                if (audio_anc_user_trigger_ff_get_status()) {
                    RACE_LOG_MSGID_I("[user_trigger_ff]user trigger adaptive FF is cancelled by end-user", 0);
                    audio_anc_user_trigger_ff_stop();
                    audio_anc_user_trigger_ff_recover_anc(ANC_K_STATUS_KEEP_DEFAULT);
                    pEvt->status = 0;
                    race_send_event_notify_msg(RACE_EVENT_TYPE_ANC_ADAPTIVE_FF_CANCEL, NULL);
                }
            }
            break;
        }
#endif
        default:
            break;
    }

    return pEvt;
}

#ifdef MTK_USER_TRIGGER_FF_ENABLE
void race_dsprealtime_anc_adaptive_trigger_ff()
{
    RACE_LOG_MSGID_I("[user_trigger_ff]race cmd user trigger adaptive FF start", 0);
    audio_anc_user_trigger_ff_start(anc_user_trigger_adaptive_ff_callback);
}

void race_dsprealtime_anc_adaptive_notify_end()
{
    race_send_event_notify_msg(RACE_EVENT_TYPE_ANC_ADAPTIVE_FF_END, NULL);
}

void race_dsprealtime_anc_adaptive_abort()
{
    bt_aws_mce_role_t role = bt_connection_manager_device_local_info_get_aws_role();

    typedef struct
    {
        uint8_t status;
        uint8_t mode;
    }PACKED RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_EVT_STRU;

    RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_EVT_STRU *pEvt = RACE_ClaimPacket(RACE_TYPE_RESPONSE,
                                                         RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK,
                                                         sizeof(RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_EVT_STRU),
                                                         g_anc_race_ch_id);

    if (pEvt) {
        pEvt->status = USER_TRIGGER_FF_STATUS_ABORT;
        pEvt->mode = ADAPTIVE_FF_ANC_MODE;
        if ((role == BT_AWS_MCE_ROLE_AGENT) || (role == BT_AWS_MCE_ROLE_NONE)) {
            race_flush_packet((void *)pEvt, g_anc_race_ch_id);
        } else {/*partner send agent result*/
            race_send_pkt_t* pSndPkt;
            pSndPkt = (void *)race_pointer_cnv_pkt_to_send_pkt((void *)pEvt);
            race_pkt_t      *pret;
            race_send_pkt_t *psend;
            psend = (race_send_pkt_t *)pSndPkt;
            pret = &psend->race_data;
            #if (RACE_DEBUG_PRINT_ENABLE)
            race_debug_print((uint8_t *)pret, (uint32_t)(pret->hdr.length + 4), "Race evt:");
            #endif

            if(pSndPkt) {
                bt_status_t ret = bt_send_aws_mce_race_cmd_data(&pSndPkt->race_data, pSndPkt->length, g_anc_race_ch_id, RACE_CMD_RSP_FROM_PARTNER,0);
                if (ret != BT_STATUS_SUCCESS) {
                    RACE_LOG_MSGID_I("[relay_cmd][user_trigger_ff]partner send relay req FAIL \n", 0);
                } else {
                    RACE_LOG_MSGID_I("[relay_cmd][user_trigger_ff]race_dsprealtime_anc_adaptive_abort, send status:%d success", pEvt->status);
                }
                race_mem_free(pSndPkt);
            }
        }
    } else {
        RACE_LOG_MSGID_I("[user_trigger_ff]race_dsprealtime_anc_adaptive_abort, pkt claim fail", 0);
    }

}
#endif

void* RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    typedef struct
    {
        RACE_COMMON_HDR_STRU Hdr;
        uint8_t status;
        uint8_t mode;
        uint8_t filter[1];
    }PACKED RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_CMD_STRU;

    typedef struct
    {
        uint8_t status;
        uint8_t mode;
    }PACKED RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_EVT_STRU;

    RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_CMD_STRU *pCmd = (RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_CMD_STRU *)pCmdMsg;
    RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_EVT_STRU *pEvt = RACE_ClaimPacket(RACE_TYPE_RESPONSE,
                                                         pCmd->Hdr.id,
                                                         sizeof(RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_EVT_STRU),
                                                         channel_id);
    g_anc_race_ch_id = channel_id;

    if (pEvt) {
        pEvt->status = 0xFF;
        switch (pCmd->mode) {
#ifdef MTK_USER_TRIGGER_FF_ENABLE
            case ADAPTIVE_FF_ANC_MODE:
            {

                if (audio_anc_user_trigger_ff_get_status()) {
                    if (pCmd->status == ANC_K_STATUS_Compare) {
                        uint16_t filter_length = (pCmd->Hdr.length >= 4) ? (pCmd->Hdr.length - 4) : 0; //Hdr.id: 2bytes, status: 1byte, mode: 1byte
                        RACE_LOG_MSGID_I("[user_trigger_ff]RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_HDR, status:%d, filter_length=%d\n",2, pCmd->status, filter_length);
                        #ifdef MTK_USER_TRIGGER_FF_ENABLE
                            audio_anc_user_trigger_ff_receive_filter(anc_user_trigger_adaptive_ff_receive_filter_callback, (uint8_t *)&pCmd->filter, (uint32_t)filter_length);
                            RACE_FreePacket(pEvt);
                            pEvt = NULL;
                        #endif
                    } else {
                        RACE_LOG_MSGID_I("[user_trigger_ff]RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_HDR, status:%d, keep default and stop record\n",1, pCmd->status);
                        pEvt->status = ANC_K_STATUS_KEEP_DEFAULT;
                        pEvt->mode = ADAPTIVE_FF_ANC_MODE;

                        //test, force to compare
        //                    uint32_t buf_size = sizeof(anc_fwd_iir_t);
        //                    anc_fwd_iir_t *tmp_buffer = (anc_fwd_iir_t *)pvPortMalloc(buf_size);
        //                    flash_memory_read_nvdm_data(NVKEY_DSP_PARA_ANC_L_FILTER2, (uint8_t *)tmp_buffer, &buf_size);
        //                    audio_anc_user_trigger_ff_receive_filter(NULL, (uint8_t *)tmp_buffer, buf_size);

                        audio_anc_user_trigger_ff_stop();
                        audio_anc_user_trigger_ff_recover_anc(ANC_K_STATUS_KEEP_DEFAULT);
                        race_send_event_notify_msg(RACE_EVENT_TYPE_ANC_ADAPTIVE_FF_END, NULL);
                    }

                /*user trigger FF was terminated*/
                } else {
                    pEvt->status = ANC_K_STATUS_ABORT;
                    pEvt->mode = ADAPTIVE_FF_ANC_MODE;
                }
            }
            break;
#endif
            default:
                break;
        }
    }

    return pEvt;
}
#else
/**
 * RACE_DSPREALTIME_ANC_HDR
 *
 * RACE_DSPREALTIME_ANC_HDR Handler
 *
 * @pCmdMsg : pointer of ptr_race_pkt_t
 *
 */
extern uint16_t g_anc_race_cmd_length;
void* RACE_DSPREALTIME_ANC_PASSTHRU_HDR(ptr_race_pkt_t pCmdMsg, uint8_t channel_id)
{
    race_dsprealtime_anc_struct *pEvt = RACE_ClaimPacket(RACE_TYPE_RESPONSE,
                                                         RACE_DSPREALTIME_ANC,
                                                         sizeof(race_dsprealtime_anc_struct),
                                                         channel_id);
    g_anc_race_ch_id = channel_id;
    if (pEvt)
    {
        race_dsprealtime_anc_struct *pAnc_cmd;
        anc_control_result_t anc_ret = ANC_CONTROL_EXECUTION_NONE;

        pAnc_cmd = (race_dsprealtime_anc_struct *)pCmdMsg->payload;
        g_anc_race_cmd_length = pCmdMsg->hdr.length - 4;
        memset(pEvt, 0, sizeof(race_dsprealtime_anc_struct));
        //RACE_LOG_MSGID_I("RACE_DSPREALTIME_ANC_HDR msg_length:%d payload: %d %d %d\n",4,pCmdMsg->hdr.length,pAnc_cmd->status,pAnc_cmd->param.header.ancId,pAnc_cmd->param.anc_on_param.anc_filter_type);
        RACE_LOG_MSGID_E("[RACE][ANC_PTHR]channel_id: %d, ANC_id: %d\n", 2, channel_id, pAnc_cmd->param.header.ancId);

        if (pCmdMsg->hdr.length >= 4) { //hdr.length = hdr.id(2bytes) + pAnc_cmd->status(1byte) + pAnc_cmd->anc_id(1byte) + pAnc_cmd->param(0/1/2/4 bytes)
            switch (pAnc_cmd->param.header.ancId) {
                case RACE_ANC_ON:{
                    anc_ret = audio_anc_command_handler(FROM_RACE, 0, (void *)pAnc_cmd);
                    pEvt->param.onRsp.header.ancId  = pAnc_cmd->param.onCmd.header.ancId;
                    pEvt->param.onRsp.header.status = anc_ret;
                    pEvt->param.onRsp.flash_no      = pAnc_cmd->param.onCmd.flash_no;
                    pEvt->param.onRsp.ancType       = pAnc_cmd->param.onCmd.ancType;
                    pEvt->param.onRsp.syncMode      = pAnc_cmd->param.onCmd.syncMode;
                    break;
                }
                case RACE_ANC_OFF:{
                    anc_ret = audio_anc_command_handler(FROM_RACE, 0, (void *)pAnc_cmd);
                    pEvt->param.offRsp.header.ancId  = pAnc_cmd->param.offCmd.header.ancId;
                    pEvt->param.offRsp.header.status = anc_ret;
                    pEvt->param.offRsp.syncMode      = pAnc_cmd->param.offCmd.syncMode;
                    if (audio_anc_get_sync_time() == 0) {
                        RACE_FreePacket(pEvt);
                        return NULL;
                    }
                    break;
                }
                case RACE_ANC_SET_VOL:{
                    anc_ret = audio_anc_command_handler(FROM_RACE, 0, (void *)pAnc_cmd);
                    pEvt->param.gainRsp.header.ancId  = pAnc_cmd->param.gainCmd.header.ancId;
                    pEvt->param.gainRsp.header.status = anc_ret;
                    pEvt->param.gainRsp.gainFFl       = pAnc_cmd->param.gainCmd.gainFFl;
                    pEvt->param.gainRsp.gainFBl       = pAnc_cmd->param.gainCmd.gainFBl;
                    pEvt->param.gainRsp.gainFFr       = pAnc_cmd->param.gainCmd.gainFFr;
                    pEvt->param.gainRsp.gainFBr       = pAnc_cmd->param.gainCmd.gainFBr;
                    break;
                }
                case RACE_ANC_READ_NVDM:{
                    race_dsprealtime_anc_struct pAnc_rsp;
                    pAnc_rsp.param.readNVDMRsp.header.ancId = pAnc_cmd->param.readNVDMCmd.header.ancId;
                    anc_ret = audio_anc_command_handler(FROM_RACE, 0, (void *)&pAnc_rsp);
                    pEvt->param.readNVDMRsp.header.ancId  = pAnc_rsp.param.readNVDMRsp.header.ancId;
                    pEvt->param.readNVDMRsp.header.status = anc_ret;
                    pEvt->param.readNVDMRsp.gainFFl       = pAnc_rsp.param.readNVDMRsp.gainFFl;
                    pEvt->param.readNVDMRsp.gainFBl       = pAnc_rsp.param.readNVDMRsp.gainFBl;
                    pEvt->param.readNVDMRsp.gainFFr       = pAnc_rsp.param.readNVDMRsp.gainFFr;
                    pEvt->param.readNVDMRsp.gainFBr       = pAnc_rsp.param.readNVDMRsp.gainFBr;
                    break;
                }
                case RACE_ANC_WRITE_NVDM:{
                    anc_ret = audio_anc_command_handler(FROM_RACE, 0, (void *)pAnc_cmd);
                    pEvt->param.writeNVDMRsp.header.ancId  = pAnc_cmd->param.writeNVDMCmd.header.ancId;
                    pEvt->param.writeNVDMRsp.header.status = anc_ret;
                    pEvt->param.writeNVDMRsp.gainFFl       = pAnc_cmd->param.writeNVDMCmd.gainFFl;
                    pEvt->param.writeNVDMRsp.gainFBl       = pAnc_cmd->param.writeNVDMCmd.gainFBl;
                    pEvt->param.writeNVDMRsp.gainFFr       = pAnc_cmd->param.writeNVDMCmd.gainFFr;
                    pEvt->param.writeNVDMRsp.gainFBr       = pAnc_cmd->param.writeNVDMCmd.gainFBr;
                    break;
                }
                case RACE_ANC_SET_RUNTIME_VOL:{
                    anc_ret = audio_anc_command_handler(FROM_RACE, 0, (void *)pAnc_cmd);
                    pEvt->param.runtimeGainRsp.header.ancId  = pAnc_cmd->param.runtimeGainCmd.header.ancId;
                    pEvt->param.runtimeGainRsp.header.status = anc_ret;
                    pEvt->param.runtimeGainRsp.gain          = pAnc_cmd->param.runtimeGainCmd.gain;
                    pEvt->param.runtimeGainRsp.syncMode      = pAnc_cmd->param.runtimeGainCmd.syncMode;
                    break;
                }
#ifdef MTK_AWS_MCE_ENABLE
                case RACE_ANC_SET_AGENT_VOL:
                case RACE_ANC_SET_PARTNER_VOL:
                case RACE_ANC_READ_PARTNER_NVDM:
                case RACE_ANC_WRITE_PARTNER_NVDM:
                case RACE_ANC_SET_SYNC_TIME:
                    pEvt->param.header.status = RACE_ERRCODE_NOT_SUPPORT;
                    return pEvt;
                    break;
#endif
                case RACE_ANC_GET_HYBRID_CAP_V2:{
                    anc_ret = audio_anc_command_handler(FROM_RACE, 0, (void *)pAnc_cmd);
                    break;
                }
                case RACE_ANC_REATIME_UPDATE_COEF:{
                    anc_ret = audio_anc_command_handler(FROM_RACE, 0, (void *)pAnc_cmd);
                    pEvt->param.setCoefRsp.header.ancId  = pAnc_cmd->param.setCoefCmd.header.ancId;
                    pEvt->param.setCoefRsp.header.status = anc_ret;
                    break;
                }
                default:
                    pEvt->param.header.status = RACE_ERRCODE_NOT_SUPPORT;
                    return pEvt;
            }
            pEvt->param.header.status = (anc_ret == ANC_CONTROL_EXECUTION_SUCCESS) ? RACE_ERRCODE_SUCCESS : RACE_ERRCODE_FAIL;
        }
        else
        {
            pEvt->param.header.status = RACE_ERRCODE_PARAMETER_ERROR;
        }
        if(pEvt->param.header.status != RACE_ERRCODE_SUCCESS) {
            RACE_LOG_MSGID_E("RACE_DSPREALTIME_ANC_HDR pEvt->status: %d  anc_ret: %d\n",2,pEvt->param.header.status,anc_ret);
        }
    }

    return pEvt;
}
#endif
#endif

void* RACE_CmdHandler_DSPREALTIME(ptr_race_pkt_t pRaceHeaderCmd, uint16_t length, uint8_t channel_id)
{
    void* ptr = NULL;

    RACE_LOG_MSGID_I("pRaceHeaderCmd->hdr.id = 0x%x \r\n",1, (int)pRaceHeaderCmd->hdr.id);

    switch (pRaceHeaderCmd->hdr.id)
    {
        case RACE_DSPREALTIME_SUSPEND:
        {
            ptr = RACE_DSPREALTIME_SUSPEND_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_DSPREALTIME_RESUME:
        {
            ptr = RACE_DSPREALTIME_RESUME_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

#if defined(MTK_PEQ_ENABLE) || defined(MTK_LINEIN_PEQ_ENABLE)
        case RACE_DSPREALTIME_PEQ:
        {
            ptr = RACE_DSPREALTIME_PEQ_HDR(pRaceHeaderCmd, channel_id);
        }
        break;
#endif

        case RACE_DSPSOUND_EFFECT:
        {
            // reserved.
        }
        break;

        case RACE_DSPREALTIME_GET_REFRENCE_GAIN:
        {
            ptr = RACE_DSPREALTIME_GET_REFRENCE_GAIN_HDR(pRaceHeaderCmd, channel_id);
        }
        break;
#ifdef MTK_AIRDUMP_EN_MIC_RECORD
        case RACE_DSPREALTIME_AIRDUMP_ENTRY:
        {
            ptr = RACE_DSPREALTIME_AIRDUMP_COMMON_HDR(pRaceHeaderCmd, channel_id);
        }
        break;
#endif
#ifdef MTK_AIRDUMP_EN
        case RACE_DSPREALTIME_AIRDUMP_ON_OFF:
        {
            ptr = RACE_DSPREALTIME_AIRDUMP_HDR(pRaceHeaderCmd, channel_id);
        }
        break;
#endif
        case RACE_DSPREALTIME_2MIC_SWAP:
        {
            ptr = RACE_DSPREALTIME_2MIC_SWAP_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_DSPREALTIME_AECNR_ON_OFF:
        {
            ptr = RACE_DSPREALTIME_AECNR_ON_OFF_HDR(pRaceHeaderCmd, channel_id);
        }
        break;


        case RACE_DSPREALTIME_SWGAIN_BYPASS:
        {
            ptr = RACE_DSPREALTIME_SWGAIN_EN_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_DSPREALTIME_OPEN_ALL_MIC:
        {
            ptr = RACE_DSPREALTIME_OPEN_ALL_MIC_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

#ifdef MTK_ANC_ENABLE
#ifdef MTK_ANC_V2
        case RACE_DSPREALTIME_ANC:
        {
            ptr = RACE_DSPREALTIME_ANC_PASSTHRU_HDR(pRaceHeaderCmd, channel_id);
        }
        break;
#else
        case RACE_DSPREALTIME_ANC:
        {
            ptr = RACE_DSPREALTIME_ANC_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_DSPREALTIME_PASS_THROUGH_ON_OFF:
        case RACE_DSPREALTIME_PASS_THROUGH_TEST:
        {
            ptr = RACE_DSPREALTIME_PASSTHRU_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_DSPREALTIME_PASS_THROUGH_TEST_MUTE:
        {
            ptr = RACE_DSPREALTIME_PASSTHRU_TEST_MUTE_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK:
        {
            ptr = RACE_DSPREALTIME_ANC_ADAPTIVE_CHECK_HDR(pRaceHeaderCmd, channel_id);
        }
        break;

        case RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT:
        {
            //printf("[user_trigger_ff]RACE_CmdHandler_DSPREALTIME, RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT");
            ptr = RACE_DSPREALTIME_ANC_ADAPTIVE_RESULT_HDR(pRaceHeaderCmd, channel_id);
        }
        break;
#endif
#endif

        default:
            break;
    }

    return ptr;
}

#endif /* RACE_DSP_REALTIME_CMD_ENABLE */

