/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */


#include "race_cmd_feature.h"
#include "stdint.h"
#include "FreeRTOS.h"
#include "task.h"
#include "hal_uart.h"
#include "syslog.h"
#include "atci_adapter.h"
#include "atci_main.h"
#include "race_cmd.h"
#include "race_xport.h"
#include "race_cmd_nvdm.h"
#include "race_cmd_bluetooth.h"
#include "race_cmd_ctrl_baseband.h"
#include "race_cmd_dsprealtime.h"
#include "race_cmd_fota.h"
#include "race_cmd_storage.h"
#include "race_cmd_ctrl_baseband.h"
#include "race_cmd_hostaudio.h"
#include "race_cmd_informational.h"
#include "race_cmd_bootreason.h"
#include "race_cmd_offline_log.h"
#include "race_cmd_online_log.h"
#include "race_util.h"
#include "race_cmd_captouch.h"
#include "race_cmd_register.h"
#include "race_cmd_rofs.h"
#include "race_cmd_system_power.h"
#include "race_cmd_system.h"

//#include "at_command.h"
#include "memory_attribute.h"
#include <string.h>
#include <stdio.h>
#include "race_fota_util.h"
#ifdef RACE_FIND_ME_ENABLE
#include "race_cmd_find_me.h"
#endif
#if defined(RACE_RELAY_CMD_ENABLE) || defined(RACE_DUMMY_RELAY_CMD_ENABLE)
#include "race_cmd_relay_cmd.h"
#endif

#ifdef RACE_BT_CMD_ENABLE
#include "race_cmd_fcd_cmd.h"
#endif
#include "race_cmd_key_cmd.h"

#define RACE_ID_FOTA_BEGIN 0x1C00
#define RACE_ID_FOTA_END 0x1C1F

#define RACE_ID_FLASH_BEGIN 0x702
#define RACE_ID_FLASH_END 0x70F

#define RACE_ID_NVKEY_BEGIN 0x0A00
#define RACE_ID_NVKEY_END 0x0AFF

#define RACE_ID_BLUETOOTH_BEGIN 0x0CC0
#define RACE_ID_BLUETOOTH_END 0x0CDF

#define RACE_ID_INFORMATIONAL_BEGIN 0x0300
#define RACE_ID_INFORMATIONAL_END   0x03FF

#define RACE_ID_STORAGE_BEGIN  0x0400
#define RACE_ID_STORAGE_END  0x0433

#define RACE_ID_DSPREALTIME_BEGIN   (RACE_DSPREALTIME_BEGIN_ID)
#define RACE_ID_DSPREALTIME_END     (RACE_DSPREALTIME_END_ID)

#define RACE_ID_CTRL_BASEBAND_BEGIN 0x20C
#define RACE_ID_CTRL_BASEBAND_END 0x20D

#define RACE_ID_HOSTAUDIO_BEGIN 0x0900
#define RACE_ID_HOSTAUDIO_END 0x09FF

#define RACE_ID_CAPTOUCH_BEGIN 0x1600
#define RACE_ID_CAPTOUCH_END 0x167f

#define RACE_ID_RG_RW_BEGIN 0x1680
#define RACE_ID_RG_RW_END 0x16ff

#define RACE_ID_2WIRE_RG_RW_BEGIN 0x0210
#define RACE_ID_2WIRE_RG_RW_END 0x0211

#define RACE_ID_FIND_ME_BEGIN 0X2C00
#define RACE_ID_FIND_ME_END 0x2C01

#define RACE_ID_BOOTREASON_BEGIN 0x1E00
#define RACE_ID_BOOTREASON_END 0x1E05

#define RACE_ID_OFFLINE_LOG_BEGIN 0x1E06

#define RACE_ID_OFFLINE_LOG_END 0x1E07

//offline and online share 0x1E07 for assert
#define RACE_ID_ONLINE_LOG_BEGIN 0x1E07
#define RACE_ID_ONLINE_LOG_END 0x1E0B

#define RACE_ID_RELAY_BEGIN 0X0D00
#define RACE_ID_RELAY_END 0x0D01
#define RACE_ID_ROFS_BEGIN 0x1D00
#define RACE_ID_ROFS_END 0x1D0F

#define RACE_ID_RSSI_START 0x1700
#define RACE_ID_RSSI_END 0x1700

#define RACE_ID_FCD_START 0x1F00
#define RACE_ID_FCD_END 0x1F3F

#define RACE_ID_KEY_START 0x1100
#define RACE_ID_KEY_END 0x1101

#define RACE_ID_SYS_PWR_START 0x1110
#define RACE_ID_SYS_PWR_END 0x111F

#define RACE_ID_SYSTEM_BEGIN 0x0200
#define RACE_ID_SYSTEM_END 0x0201

#define RACE_ID_SLEEP_CONTROL_BEGIN 0x0220
#define RACE_ID_SLEEP_CONTROL_END 0x0221

const RACE_HANDLER race_handlers[] = {
#ifdef RACE_NVDM_CMD_ENABLE
    {RACE_ID_NVKEY_BEGIN, RACE_ID_NVKEY_END, RACE_CmdHandler_NVDM},
#endif
    {RACE_ID_INFORMATIONAL_BEGIN, RACE_ID_INFORMATIONAL_END, RACE_CmdHandler_INFORMATION},

#ifdef RACE_FOTA_CMD_ENABLE
    {RACE_ID_FOTA_BEGIN, RACE_ID_FOTA_END, RACE_CmdHandler_FOTA},
#endif

#ifdef RACE_BT_CMD_ENABLE
    {RACE_ID_BLUETOOTH_BEGIN, RACE_ID_BLUETOOTH_END, RACE_CmdHandler_BLUETOOTH},
#endif

#ifdef RACE_STORAGE_CMD_ENABLE
    {RACE_ID_STORAGE_BEGIN, RACE_ID_STORAGE_END, race_cmdhdl_storage},
#endif

#ifdef RACE_DSP_REALTIME_CMD_ENABLE
    {RACE_ID_DSPREALTIME_BEGIN, RACE_ID_DSPREALTIME_END, RACE_CmdHandler_DSPREALTIME},
#endif

#ifdef RACE_CTRL_BASEBAND_CMD_ENABLE
    {RACE_ID_CTRL_BASEBAND_BEGIN, RACE_ID_CTRL_BASEBAND_END, RACE_CmdHandler_CTRL_BASEBAND},
#endif

#ifdef RACE_HOSTAUDIO_CMD_ENABLE
    {RACE_ID_HOSTAUDIO_BEGIN, RACE_ID_HOSTAUDIO_END, RACE_CmdHandler_HOSTAUDIO},
#endif

#ifdef RACE_CAPTOUCH_CMD_ENABLE
    {RACE_ID_CAPTOUCH_BEGIN, RACE_ID_CAPTOUCH_END, RACE_CmdHandler_captouch},
#endif

#ifdef RACE_RG_READ_WRITE_ENABLE
    {RACE_ID_RG_RW_BEGIN, RACE_ID_RG_RW_END, RACE_CmdHandler_RG_read_write},
    {RACE_ID_2WIRE_RG_RW_BEGIN, RACE_ID_2WIRE_RG_RW_END, RACE_CmdHandler_2wire_RG_read_write},
#endif

#ifdef RACE_FIND_ME_ENABLE
    {RACE_ID_FIND_ME_BEGIN, RACE_ID_FIND_ME_END, RACE_CmdHandler_FIND_ME},
#endif

#ifdef RACE_BOOTREASON_CMD_ENABLE
    {RACE_ID_BOOTREASON_BEGIN, RACE_ID_BOOTREASON_END, RACE_CmdHandler_bootreason},
#endif

#ifdef RACE_OFFLINE_LOG_CMD_ENABLE
    {RACE_ID_OFFLINE_LOG_BEGIN, RACE_ID_OFFLINE_LOG_END, RACE_CmdHandler_offline_log},
#endif

#ifdef RACE_ONLINE_LOG_CMD_ENABLE
    {RACE_ID_ONLINE_LOG_BEGIN, RACE_ID_ONLINE_LOG_END, RACE_CmdHandler_online_log},
#else
#ifdef MTK_MUX_ENABLE
    {RACE_ID_ONLINE_LOG_BEGIN, RACE_ID_ONLINE_LOG_END, RACE_CmdHandler_online_log_2},
#endif
#endif

#if defined(RACE_RELAY_CMD_ENABLE) || defined(RACE_DUMMY_RELAY_CMD_ENABLE)
    {RACE_ID_RELAY_BEGIN, RACE_ID_RELAY_END, RACE_CmdHandler_RELAY_RACE_CMD},
#endif
#ifdef RACE_ROFS_CMD_ENABLE
    {RACE_ID_ROFS_BEGIN, RACE_ID_ROFS_END, RACE_CmdHandlerROFS},
#endif
#ifdef RACE_BT_CMD_ENABLE
    {RACE_ID_RSSI_START, RACE_ID_RSSI_END, RACE_CmdHandler_GET_RSSI},
    {RACE_ID_FCD_START, RACE_ID_FCD_END, RACE_CmdHandler_FCD},
#endif
    {RACE_ID_KEY_START, RACE_ID_KEY_END, RACE_CmdHandler_KEY},
    {RACE_ID_SYS_PWR_START, RACE_ID_SYS_PWR_END, RACE_CmdHandler_SYS_PWR},
    {RACE_ID_SYSTEM_BEGIN, RACE_ID_SYSTEM_END, RACE_CmdHandler_System},
    {RACE_ID_SLEEP_CONTROL_BEGIN, RACE_ID_SLEEP_CONTROL_END, RACE_CmdHandler_System},
};


/*******************************************************************************/
/*                      Global Variables                                      */
/*******************************************************************************/
//static Handler app_race_handler = NULL;
static uint32_t g_race_registered_table_number;
static RACE_HANDLER g_race_cm4_general_hdlr_tables[RACE_MAX_GNENERAL_TABLE_NUM];

void *RACE_ClaimPacket(uint8_t race_type, uint16_t race_id, uint16_t dat_len, uint8_t channel_id)
{
    race_send_pkt_t *pPacket = NULL;

    RACE_LOG_MSGID_I("race_type:%x cmd_id:%x dat_len:%d channel_id:%d",4,
               race_type, race_id, dat_len, channel_id);

    pPacket = (race_send_pkt_t *)race_mem_alloc(sizeof(race_send_pkt_t) + dat_len);
    if (pPacket != NULL) {
        pPacket->channel_id = channel_id;
        pPacket->length = sizeof(RACE_COMMON_HDR_STRU) + dat_len;
        pPacket->offset = 6;//OS_OFFSET_OF(RACE_IPC_STRU, payload);
        pPacket->reserve = 0xCC;

        pPacket->race_data.hdr.pktId.value = 0x05;
        pPacket->race_data.hdr.type = race_type;
        pPacket->race_data.hdr.length = sizeof(uint16_t) + dat_len;
        pPacket->race_data.hdr.id = race_id;

        return pPacket->race_data.payload;
    } else {
        return NULL;
    }
}

void *RACE_ClaimPacketAppID(uint8_t app_id, uint8_t race_type, uint16_t race_id, uint16_t dat_len, uint8_t channel_id)
{
    race_send_pkt_t *pPacket = NULL;
    //uint8_t message[16];

    RACE_LOG_MSGID_I("race_type:%x cmd_id:%x dat_len:%d channel_id:%d",4,
               race_type, race_id, dat_len, channel_id);

    pPacket = (race_send_pkt_t *)race_mem_alloc(sizeof(race_send_pkt_t) + dat_len);
    if (pPacket != NULL) {
        pPacket->channel_id = channel_id;
        pPacket->length = sizeof(RACE_COMMON_HDR_STRU) + dat_len;
        pPacket->offset = 6;//OS_OFFSET_OF(RACE_IPC_STRU, payload);
        pPacket->reserve = 0xCC;

        pPacket->race_data.hdr.pktId.value = ((app_id << 4) | 0x05);
        pPacket->race_data.hdr.type = race_type;
        pPacket->race_data.hdr.length = sizeof(uint16_t) + dat_len;
        pPacket->race_data.hdr.id = race_id;

        /*memcpy(message, pPacket, 16);
        RACE_LOG_MSGID_I("pPacket : %X %X %X %X %X %X %X %X %X %X %X %X %X %X %X %X \r\n",16, message[0], message[1], message[2], message[3]
                                                     , message[4], message[5], message[6], message[7]
                                                     , message[8], message[9], message[10], message[11]
                                                     , message[12], message[13], message[14], message[15]);*/
        return pPacket->race_data.payload;
    } else {
        return NULL;
    }
}


/* Input the pointer returned by RACE_ClaimPacket() or RACE_ClaimPacketAppID() */
void RACE_FreePacket(void *data)
{
    race_send_pkt_t *send_pkt = NULL;

    /* Convert payload pointer to the pointer points to the begining of the whole package. */
    send_pkt = race_pointer_cnv_pkt_to_send_pkt(data);
    race_mem_free(send_pkt);
}

race_status_t RACE_Register_Handler(RACE_HANDLER *pHandler)
{
    if(g_race_registered_table_number == RACE_MAX_GNENERAL_TABLE_NUM)
        return RACE_STATUS_REGISTRATION_FAILURE;
    else if(!pHandler)
        return RACE_STATUS_ERROR;

    memcpy(&g_race_cm4_general_hdlr_tables[g_race_registered_table_number], pHandler, sizeof(RACE_HANDLER));
    g_race_registered_table_number++;

    return RACE_STATUS_OK;
}

#if (RACE_DEBUG_PRINT_ENABLE)
void race_debug_print(uint8_t *data, uint32_t size, const char *print_tag)
{
    uint8_t *pdata;
    uint8_t *t_data1;
    uint8_t *t_data2;
    uint32_t ret_len, i;
    uint32_t print_len;
    uint32_t data_len;

    data_len = size;

    t_data1 = (uint8_t *)race_mem_alloc(data_len + 20);
    t_data2 = (uint8_t *)race_mem_alloc((data_len * 3) + strlen(print_tag) + 20);

    if ((t_data1 == NULL) || (t_data2 == NULL)) {
        race_mem_free(t_data1);
        race_mem_free(t_data2);
        RACE_LOG_MSGID_I("log missing,not enough heap memmory size\r\n", 0);
        return;
    } else {
        memcpy(t_data1, data, size);

        ret_len   = 0;
        print_len = 0;

        pdata = (uint8_t *)t_data1;

        ret_len    = sprintf((char *)(t_data2 + print_len), "%s len[%d]: ", (char *)print_tag, (int)data_len);
        print_len += ret_len;

        for (i = 0; i < data_len; i++) {
            ret_len    = sprintf((char *)(t_data2 + print_len), "%x ", *(pdata++));
            print_len += ret_len;
        }

        RACE_LOG_I("%s", (char *)t_data2);
    }

    race_mem_free(t_data1);
    race_mem_free(t_data2);
}
#endif


void *RACE_CmdHandler(race_pkt_t *pMsg, uint8_t channel_id)
{
    uint32_t i;

    void *ptr = NULL;

    if (!pMsg) {
        return NULL;
    }

#if (RACE_DEBUG_PRINT_ENABLE)
    race_debug_print((uint8_t *)pMsg, (uint32_t)(pMsg->hdr.length + 4), "Race cmd:");
#endif

    switch (pMsg->hdr.type) {
        case RACE_TYPE_COMMAND:
        case RACE_TYPE_COMMAND_WITHOUT_RSP:
        case RACE_TYPE_RESPONSE:
        case RACE_TYPE_NOTIFICATION: {
            for (i = 0; i < sizeof(race_handlers) / sizeof(RACE_HANDLER); i++) {
                if (pMsg->hdr.id >= race_handlers[i].id_start && pMsg->hdr.id <= race_handlers[i].id_end) {
                    RACE_LOG_MSGID_I("recv cmd: type:%x cmd_id:%x app_id:%d",3, pMsg->hdr.type, pMsg->hdr.id, pMsg->hdr.pktId.field.app_id);
#ifdef RACE_FOTA_CMD_ENABLE
                    if (RACE_APP_ID_FOTA == pMsg->hdr.pktId.field.app_id) {
                        RACE_ERRCODE ret = race_fota_cmd_preprocess(pMsg->hdr.id,
                                           pMsg->hdr.type,
                                           channel_id);
                        if (RACE_ERRCODE_SUCCESS != ret) {
                            break;
                        }
                    }
#endif /* RACE_FOTA_CMD_ENABLE */

                    ptr = race_handlers[i].handler(pMsg, pMsg->hdr.length, channel_id);

                    if (ptr) {
                        ptr = (void *)race_pointer_cnv_pkt_to_send_pkt(ptr);
#if (RACE_DEBUG_PRINT_ENABLE)
                        race_pkt_t      *pret;
                        race_send_pkt_t *psend;
                        psend = (race_send_pkt_t *)ptr;
                        pret = &psend->race_data;
                        race_debug_print((uint8_t *)pret, (uint32_t)(pret->hdr.length + 4), "Race evt:");
#endif
                    }
                    break;
                }
            }
            if (i == sizeof(race_handlers) / sizeof(RACE_HANDLER)) { //not found
                for(i = 0; i < g_race_registered_table_number; i++) {
                    if (pMsg->hdr.id >= g_race_cm4_general_hdlr_tables[i].id_start && pMsg->hdr.id <= g_race_cm4_general_hdlr_tables[i].id_end) {
                        RACE_LOG_MSGID_I("recv cmd: type:%x cmd_id:%x app_id:%d handled by 0x%x",4, pMsg->hdr.type, pMsg->hdr.id, pMsg->hdr.pktId.field.app_id, g_race_cm4_general_hdlr_tables[i].handler);

                        ptr = g_race_cm4_general_hdlr_tables[i].handler(pMsg, pMsg->hdr.length, channel_id);

                        if (ptr) {
                            ptr = (void *)race_pointer_cnv_pkt_to_send_pkt(ptr);
#if (RACE_DEBUG_PRINT_ENABLE)
                            race_pkt_t      *pret;
                            race_send_pkt_t *psend;
                            psend = (race_send_pkt_t *)ptr;
                            pret = &psend->race_data;
                            race_debug_print((uint8_t *)pret, (uint32_t)(pret->hdr.length + 4), "Race evt:");
#endif
                        }
                        break;
                    }
                }
            }
#if 0
            if (i == sizeof(race_handlers) / sizeof(RACE_HANDLER)) { //not found
                if (app_race_handler) {
                    RACE_Send2Handler(pMsg, app_race_handler);
                } else {
                    PTR_RACE_PAYLOAD_STRU payload;
                    uint8_t *cptr = RACE_ClaimPacket(RACE_TYPE_RESPONSE, pRaceHeaderCmd->id, sizeof(uint8_t), pMsg->channel_id);
                    cptr[0] = RACE_ERRCODE_NOT_SUPPORT;
                    //PTR_RACE_IPC_STRU packet;
                    payload = OS_CONTAINER_OF(cptr, RACE_PAYLOAD_STRU, param);
                    ptr = (void *)OS_CONTAINER_OF(payload, RACE_IPC_STRU, payload);
                }
            }
#endif
            break;
        }

        default: {
            break;
        }
    }

    // We should free the memory.
    //OSMEM_Put(pMsg);
    return ptr;
}

