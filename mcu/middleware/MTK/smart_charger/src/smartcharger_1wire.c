/* Copyright Statement:
 *
 * (C) 2018  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "FreeRTOS.h"
#include "timers.h"

#include "smartcharger.h"

#include "hal_uart_internal.h"
#include "hal_eint.h"
#include "hal_gpio.h"
#include "hal_log.h"
#include "chargersmartcase.h"
#include "battery_management.h"
#include "hal.h"
#include "hal_dvfs_internal.h"
#include "hal_sleep_manager_platform.h"
#include "battery_management_core.h"
#include "battery_management.h"
#include "memory_attribute.h"
#include "bt_sink_srv_ami.h"

#define BIT_FIELD_CLEAR16(r,p,l)    (((U16)(r)) & ~((((U16)1 << (l)) - 1) << (p)))
#define BIT_FIELD_CLEAR32(r,p,l)    (((U32)(r)) & ~((((U32)1 << (l)) - 1) << (p)))
#define BIT_FIELD_EXTRACT8(r,p,l)   (((U8)(r)  >> (p)) & (((U8)1  << (l)) - 1))
#define BIT_FIELD_EXTRACT16(r,p,l)  (((U16)(r) >> (p)) & (((U16)1 << (l)) - 1))
#define BIT_FIELD_EXTRACT32(r,p,l)  (((U32)(r) >> (p)) & (((U32)1 << (l)) - 1))
#define BIT_FIELD_INSERT8(r,p,l,v)  (BIT_FIELD_CLEAR8((r),(p),(l))  | ((U8)(v)  << (p)))
#define BIT_FIELD_INSERT16(r,p,l,v) (BIT_FIELD_CLEAR16((r),(p),(l)) | ((U16)(v) << (p)))
#define BIT_FIELD_INSERT32(r,p,l,v) (BIT_FIELD_CLEAR32((r),(p),(l)) | ((U32)(v) << (p)))


#define SMART_CHARGER_CASE_1WIRE_TEST FALSE
////////////////////////////////////////////////////////////////////////////////
// Global Variables ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
ATTR_ZIDATA_IN_NONCACHED_SYSRAM_4BYTE_ALIGN static uint8_t oneWire_tx[32];
ATTR_ZIDATA_IN_NONCACHED_SYSRAM_4BYTE_ALIGN static uint8_t oneWire_rx[32];

uint8_t  lastEvent;
uint8_t  lastCmd;
uint8_t  isCommMode;
uint16_t lastData_len;
uint32_t lastData;

#if (SMART_CHARGER_CASE_1WIRE_TEST)
TimerHandle_t pSmartChargerTimerTest;
#endif


#if (PRODUCT_VERSION == 2822 || defined(AB1568))
#define CHARGER_CASE_1WIRE_1568
#elif defined(AB1565)
#define CHARGER_CASE_1WIRE_1565
#else
#define CHARGER_CASE_1WIRE_155x
#endif

#ifdef CHARGER_CASE_1WIRE_1568
#ifdef MTK_SMART_CHARGER_1WIRE_SAMPLE
#define CHARGER_COMM_MODE_GPIO HAL_GPIO_10
#elif defined(MTK_SMART_CHARGER_1WIRE_EVK)
#define CHARGER_COMM_MODE_GPIO HAL_GPIO_6
#else
#define CHARGER_COMM_MODE_GPIO HAL_GPIO_3
#endif

#define CHARGER_1WIRE_TX_GPIO HAL_GPIO_4
#define CHARGER_1WIRE_RX_GPIO HAL_GPIO_5

#define CHARGER_1WIRE_TX_GPIO_GPIO   HAL_GPIO_4_GPIO4
#define CHARGER_1WIRE_TX_GPIO_TX     HAL_GPIO_4_UART1_TXD

#define CHARGER_1WIRE_RX_GPIO_GPIO   HAL_GPIO_5_GPIO5
#define CHARGER_1WIRE_RX_GPIO_RX     HAL_GPIO_5_UART1_RXD

#define CHARGER_1WIRE_OUT_ADC        500

#elif defined(CHARGER_CASE_1WIRE_1565)

#ifdef MTK_SMART_CHARGER_1WIRE_SAMPLE
#define CHARGER_COMM_MODE_GPIO HAL_GPIO_10
#else
#define CHARGER_COMM_MODE_GPIO HAL_GPIO_3
#endif

#define CHARGER_1WIRE_TX_GPIO HAL_GPIO_4
#define CHARGER_1WIRE_RX_GPIO HAL_GPIO_5

#define CHARGER_1WIRE_TX_GPIO_GPIO   HAL_GPIO_4_GPIO4
#define CHARGER_1WIRE_TX_GPIO_TX     HAL_GPIO_4_UART1_TXD

#define CHARGER_1WIRE_RX_GPIO_GPIO   HAL_GPIO_5_GPIO5
#define CHARGER_1WIRE_RX_GPIO_RX     HAL_GPIO_5_UART1_RXD

#define CHARGER_1WIRE_OUT_ADC        150
#else

#define CHARGER_COMM_MODE_GPIO HAL_GPIO_1

#define CHARGER_1WIRE_TX_GPIO HAL_GPIO_10
#define CHARGER_1WIRE_RX_GPIO HAL_GPIO_9

#define CHARGER_1WIRE_TX_GPIO_GPIO   HAL_GPIO_10_GPIO10
#define CHARGER_1WIRE_TX_GPIO_TX     HAL_GPIO_10_UART1_TXD

#define CHARGER_1WIRE_RX_GPIO_GPIO   HAL_GPIO_9_GPIO9
#define CHARGER_1WIRE_RX_GPIO_RX     HAL_GPIO_9_UART1_RXD
#define CHARGER_1WIRE_OUT_ADC        500
#endif

extern TimerHandle_t pSmartChargerTimer;
extern TimerHandle_t pSmartChargerOutTimer;
extern uint8_t LOCK_DVFS_SLEEP;

static uint8_t raceEvt[]= {
    0x05,      //race header
    0x5D,
    0x05,      //lengthL
    0x00,      //lengthH
    0x00,      //cmd type
    0x20,      //cmd tpye
    0x00,      //event
    0x00,      //R or L
    0x00,      //parameter
};

#define RACE_TYPE       1
#define LENGTH_L_INDEX  3
#define LENGTH_H_INDEX  2
#define EVENT_INDEX     6
#define EARBUD_INDEX    7
#define PARAMETER_INDEX 8
#define KEY_INDEX       8

#define CASE_LID_OPEN      2
#define CASE_LID_CLOSE     3
#define CASE_CHARGER_OFF   4
#define CASE_CHARGER_KEY   5
#define CASE_BATTER_LEVEL  6
#define CASE_CHARGER_STATE 7
#define CASE_USER_1        8

uint8_t LOCK_DVFS = false;
////////////////////////////////////////////////////////////////////////////////
//  Declarations ///////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
static void smart_charger_lock_dvfs_1wire(void)
{
    if(LOCK_DVFS == FALSE)
    {
        log_hal_msgid_info("[SmartCharger]DVFS LOCK\r\n", 0);
        //dvfs_lock_control("SmartCharger",DVFS_156M_SPEED, DVFS_LOCK);
        LOCK_DVFS = TRUE;
    }
}

static void smart_charger_unlock_dvfs_1wire(void)
{
    if(LOCK_DVFS == TRUE)
    {
        log_hal_msgid_info("[SmartCharger]DVFS UNLOCK\r\n", 0);
        //dvfs_lock_control("SmartCharger",DVFS_156M_SPEED, DVFS_UNLOCK);
        LOCK_DVFS = FALSE;
    }
}


static void DRV_SmartChargerCase_Chg_Mode(void)
{
    log_hal_msgid_info("[SmartCharger][1Wire]charger mode\r\n",0);

    if(xTimerStop(pSmartChargerOutTimer, 0) == pdFAIL)
    {
        log_hal_msgid_info("[SmartCharger][1Wire]cancel charger timeout fail\r\n",0);
    }

    hal_pinmux_set_function(CHARGER_1WIRE_RX_GPIO , CHARGER_1WIRE_RX_GPIO_GPIO);

#ifndef CHARGER_CASE_1WIRE_1565
    hal_gpio_set_output(CHARGER_COMM_MODE_GPIO, HAL_GPIO_DATA_HIGH);
#endif

    if(LOCK_DVFS_SLEEP == true)
    {
        hal_sleep_manager_unlock_sleep(SLEEP_LOCK_CHARGER_CASE);
        LOCK_DVFS_SLEEP = false;
    }
}

static void DRV_SmartChargerCase_Comm_Mode(void)
{
    log_hal_msgid_info("[SmartCharger][1Wire]comm mode\r\n",0);

    hal_pinmux_set_function(CHARGER_1WIRE_RX_GPIO , CHARGER_1WIRE_RX_GPIO_RX);

#ifndef CHARGER_CASE_1WIRE_1565
    hal_gpio_set_output(CHARGER_COMM_MODE_GPIO, HAL_GPIO_DATA_LOW);
#endif

    if(LOCK_DVFS_SLEEP != true)
    {
        hal_sleep_manager_lock_sleep(SLEEP_LOCK_CHARGER_CASE);
        LOCK_DVFS_SLEEP = true;
    }
}

static void DRV_SmartCharger_1wire_Send_to_App(uint8_t event, uint32_t data, uint16_t data_len)
{
    smartcharger_callback_t callback_handler;
    /* send cmd to app*/
    callback_handler = ChargerSmartCase_GetSmartCaseHandle();

    if(lastCmd == event)
    {
        if(event == DRV_CHARGER_EVENT_LID_OPEN_PATTERN || event == DRV_CHARGER_EVENT_LID_CLOSE_PATTERN || event == DRV_CHARGER_EVENT_USER1_PATTERN)
        {
            log_hal_msgid_info("[SmartCharger][1Wire]duplicate event %d\r\n", 1, event);
            return;
        }
    }

    lastCmd = event;

    if(event != 0)
    {
        if(callback_handler)
        {
            callback_handler(event, data, data_len);
        }
        else
        {
            lastEvent     = event;
            lastData_len  = data_len;
            lastData      = data;
        }
    }
}

void DRV_SmartCharger_1wire_preHandler(void)
{
    if(lastEvent)
    {
        DRV_SmartCharger_1wire_Send_to_App(lastEvent , lastData, lastData_len);
        lastEvent     = 0;
        lastData_len  = 0;
        lastData      = 0;
    }
}

static void DRV_SmartChargerCase_1wire_Uart_Cb(hal_uart_callback_event_t status, void *user_data)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    if (status == HAL_UART_EVENT_READY_TO_READ)
    {
        if( xTimerStartFromISR(pSmartChargerTimer, &xHigherPriorityTaskWoken ) != pdPASS )
        {
            log_hal_msgid_info("[SmartCharger][1Wire]timer start from isr fail", 0);
        }
    }

    if (status == HAL_UART_EVENT_WAKEUP_SLEEP)
    {
    }

    if (xHigherPriorityTaskWoken != pdFALSE) {
        // Actual macro used here is port specific.
        portYIELD_FROM_ISR(pdTRUE);
    }
}

const uint8_t patternTable[] = {0,
                                0,
                                DRV_CHARGER_EVENT_LID_OPEN_PATTERN,
                                DRV_CHARGER_EVENT_LID_CLOSE_PATTERN,
                                DRV_CHARGER_EVENT_CHARGER_OFF_PATTERN,
                                DRV_CHARGER_EVENT_CHARGER_KEY_PATTERN,
                                0,
                                0,
                                DRV_CHARGER_EVENT_USER1_PATTERN
                                };

static void DRV_SmartCharger_1wire_CallBack( TimerHandle_t pxExpiredTimer )
{
    UNUSED(pxExpiredTimer);

    uint32_t rxSize;
    uint8_t  raceCmd[32];
    uint8_t  batterLevel;

    rxSize = hal_uart_get_available_receive_bytes(HAL_UART_1);

    log_hal_msgid_info("[SmartCharger][1Wire] Rx Size[%d]", 1,rxSize);

    if(xTimerReset(pSmartChargerOutTimer, 0) == pdFAIL)
    {
        log_hal_msgid_error("[SmartCharger][1Wire]cancel charger timeout fail\r\n",0);
    }

    if(rxSize > 7)
    {
        hal_uart_receive_dma(HAL_UART_1, raceCmd, rxSize);

        log_hal_msgid_info("[SmartCharger][1Wire] race tyte[0x%x] earbud[%d] event[%d] parameter[%d]", 4, raceCmd[RACE_TYPE]
                                                                                                        , raceCmd[EARBUD_INDEX]
                                                                                                        , raceCmd[EVENT_INDEX]
                                                                                                        , raceCmd[PARAMETER_INDEX]);

        audio_channel_t channel = ami_get_audio_channel();

        raceEvt[EARBUD_INDEX] = channel;

        log_hal_msgid_info("[SmartCharger][1Wire] earbud direction[%d]", 1, channel);

        if(raceCmd[0] == 0x5)
        {
            /* check cmd or event adn which earbud */
            if(raceCmd[RACE_TYPE] == 0x5A)
            {
                if(raceCmd[EVENT_INDEX])
                {
                    if(raceCmd[PARAMETER_INDEX] == 100)
                    {
                        batterLevel = 31;
                    }
                    else if(raceCmd[PARAMETER_INDEX] == 0)
                    {
                        batterLevel = 0;
                    }
                    else
                    {
                        batterLevel = 31 - 31*(100 - raceCmd[PARAMETER_INDEX])/100;
                    }

                    /* send cmd to app*/
                    DRV_SmartCharger_1wire_Send_to_App(patternTable[raceCmd[EVENT_INDEX]], batterLevel, 3);

                    if(raceCmd[EVENT_INDEX] == CASE_LID_CLOSE)
                    {
                        /* down frequncy */
                        smart_charger_unlock_dvfs_1wire();
                    }

                    if(raceCmd[EARBUD_INDEX] == channel)
                    {
                        /* return event */
                        raceEvt[EVENT_INDEX]     = raceCmd[EVENT_INDEX];

                        if(raceEvt[EVENT_INDEX] == CASE_CHARGER_STATE)
                        {
                            if(battery_management_get_battery_property(BATTERY_PROPERTY_CAPACITY) == 100)
                            {
                                raceEvt[PARAMETER_INDEX] = 1;
                            }
                            else
                            {
                                raceEvt[PARAMETER_INDEX] = 0;

                            }
                        }
                        else
                        {
                            raceEvt[PARAMETER_INDEX] = battery_management_get_battery_property(BATTERY_PROPERTY_CAPACITY);
                        }
                        hal_pinmux_set_function(CHARGER_1WIRE_TX_GPIO, CHARGER_1WIRE_TX_GPIO_TX);
                        uart_send_polling(HAL_UART_1 , (const uint8_t *)raceEvt, sizeof(raceEvt));
                        hal_gpt_delay_us(500);

                        if(raceCmd[EVENT_INDEX] == CASE_CHARGER_OFF)
                        {
                            hal_pinmux_set_function(CHARGER_1WIRE_RX_GPIO , CHARGER_1WIRE_RX_GPIO_GPIO);
                        }

                        hal_pinmux_set_function(CHARGER_1WIRE_TX_GPIO, CHARGER_1WIRE_TX_GPIO_GPIO);
                    }
                }
            }
        }
        else
        {
            /* just drop if not race */
        }
    }
    else
    {
        /* just drop */
        hal_uart_receive_dma(HAL_UART_1, raceCmd, rxSize);
    }
}



static void DRV_SmartCharger_1wire_CallBack_Out( TimerHandle_t pxExpiredTimer )
{
    uint32_t data;
    uint32_t rxLength;
#if defined(CHARGER_CASE_1WIRE_1565)
    uint32_t pmuData;
#endif

#if defined(CHARGER_CASE_1WIRE_1568)
    data = pmu_auxadc_get_channel_value(PMU_AUX_VBUS);
#elif defined(CHARGER_CASE_1WIRE_1565)
    data = pmu_auxadc_get_channel_value(PMU_AUX_VCHG);
#else
    hal_adc_get_data_polling(HAL_ADC_CHANNEL_6 , &data);
#endif
    rxLength = hal_uart_get_available_receive_bytes(HAL_UART_0);

    log_hal_msgid_info("[SmartCharger][1Wire]polling adc[%d] rxLength[%d]\r\n",2, data, rxLength);

    /* if adc less than 500 means real charger out */
    if(data < CHARGER_1WIRE_OUT_ADC && !rxLength)
    {
        /* real charger out */
        DRV_SmartChargerCase_Chg_Mode();
        hal_gpio_pull_down(CHARGER_1WIRE_RX_GPIO);

        /* send charger out to app */
        DRV_SmartCharger_1wire_Send_to_App(DRV_CHARGER_EVENT_CHARGER_OUT_PATTERN, 0, 0);

        /* down frequncy */
        smart_charger_unlock_dvfs_1wire();

#if defined(CHARGER_CASE_1WIRE_1565)
        /* Dsiable VIO18 pullup */
        pmuData  = pmu_get_register_value_2565(0x2, 0xFFFF, 0);
        pmuData  = BIT_FIELD_INSERT32(pmuData, 3, 1, 1);
        pmu_force_set_register_value_2565(0x2, pmuData);

        /* VBUS discharge on */
        pmuData  = pmu_get_register_value_2565(0x32A, 0xFFFF, 0);
        pmuData = BIT_FIELD_INSERT16(pmuData, 11, 1, 1);
        pmu_force_set_register_value_2565(0x32A, pmuData);

        /* EOC */
        pmuData  = pmu_get_register_value_2565(0x1E, 0xFFFF, 0);
        pmuData = BIT_FIELD_INSERT16(pmuData, 5, 3, 1);
        pmu_force_set_register_value_2565(0x1E, pmuData);

        pmu_eoc_ctrl(1);

        /* PSW_MAIN_CL = ON PSW_MAIN = OFF */
        pmuData  = pmu_get_register_value_2565(0x2, 0xFFFF, 0);
        pmuData  = BIT_FIELD_INSERT32(pmuData, 2, 1, 1);
        pmuData  = BIT_FIELD_INSERT32(pmuData, 0, 1, 1);
        pmu_force_set_register_value_2565(0x2, pmuData);

        /* VBUS discharge off */
        pmuData  = pmu_get_register_value_2565(0x32A, 0xFFFF, 0);
        pmuData = BIT_FIELD_INSERT16(pmuData, 11, 1, 0);
        pmu_force_set_register_value_2565(0x32A, pmuData);

        isCommMode  = FALSE;
#endif
    }
}

#if (SMART_CHARGER_CASE_1WIRE_TEST)
static void DRV_SmartCharger_1wire_CallBack_Out_test( TimerHandle_t pxExpiredTimer )
{
    uint8_t test[]= {
        0x00,
        0x01,      //race header
        0x02,
        0x03,      //lengthH
        0x04,      //lengthL
        0x05,      //cmd type
        0x06,      //cmd tpye
        0x07,      //event
        0x08,      //R or L
        0x09      //parameter
    };

    log_hal_msgid_info("[SmartCharger][1Wire]testtt\r\n",0);

    hal_pinmux_set_function(CHARGER_1WIRE_RX_GPIO , CHARGER_1WIRE_RX_GPIO_GPIO);
    hal_gpio_set_output(CHARGER_COMM_MODE_GPIO, HAL_GPIO_DATA_HIGH);

    hal_pinmux_set_function(CHARGER_1WIRE_TX_GPIO, CHARGER_1WIRE_TX_GPIO_TX);
    uart_send_polling(HAL_UART_1 , (const uint8_t *)test, sizeof(test));
}
#endif


static void battery_management_callback_for_smart_1wire(battery_management_event_t event, const void *data)
{
    log_hal_msgid_info("[SmartCharger][1Wire]Received battery event: %d\r\n",1, event);

    if (event == BATTERY_MANAGEMENT_EVENT_CHARGER_EXIST_UPDATE)
    {
        int32_t charging_exist = battery_management_get_battery_property(BATTERY_PROPERTY_CHARGER_EXIST);
        log_hal_msgid_info("[SmartCharger][1Wire]Received battery exist: %d\r\n",1, charging_exist);

        uint32_t data;

#ifdef CHARGER_CASE_1WIRE_1565
        uint32_t pmuData;
        if(charging_exist == 0)
        {
            /* Dsiable VIO18 pullup */
            pmuData  = pmu_get_register_value_2565(0x2, 0xFFFF, 0);
            pmuData  = BIT_FIELD_INSERT32(pmuData, 3, 1, 1);
            pmu_force_set_register_value_2565(0x2, pmuData);

            /* VBUS discharge on */
            pmuData  = pmu_get_register_value_2565(0x32A, 0xFFFF, 0);
            pmuData = BIT_FIELD_INSERT16(pmuData, 11, 1, 1);
            pmu_force_set_register_value_2565(0x32A, pmuData);

            /* EOC */
            pmuData  = pmu_get_register_value_2565(0x1E, 0xFFFF, 0);
            pmuData = BIT_FIELD_INSERT16(pmuData, 5, 3, 1);
            pmu_force_set_register_value_2565(0x1E, pmuData);

            pmu_eoc_ctrl(1);

            data = pmu_auxadc_get_channel_value(PMU_AUX_VCHG);
            log_hal_msgid_info("[SmartCharger][1Wire]vchg adc = %d\r\n",1, data);

            /* always comm mode */
            /* PSW_MAIN = OFF , ENABLE PULL UP */
            pmuData  = pmu_get_register_value_2565(0x2, 0xFFFF, 0);
            pmuData  = BIT_FIELD_INSERT32(pmuData, 0, 1, 1);
            pmuData  = BIT_FIELD_INSERT32(pmuData, 3, 1, 0);
            pmu_force_set_register_value_2565(0x2, pmuData);

            /* VBUS discharge off */
            pmuData  = pmu_get_register_value_2565(0x32A, 0xFFFF, 0);
            pmuData = BIT_FIELD_INSERT16(pmuData, 11, 1, 0);
            pmu_force_set_register_value_2565(0x32A, pmuData);

            DRV_SmartChargerCase_Comm_Mode();

            xTimerStart(pSmartChargerOutTimer, 0);
        }
        else if (charging_exist == 1)
        {
            pmuData  = pmu_get_register_value_2565(0x2, 0xFFFF, 0);
            if (isCommMode == FALSE)
            {
                /* Enable VIO18 pullup */
                pmuData  = BIT_FIELD_INSERT32(pmuData, 3, 1, 0);
            }
            else
            {
                /* PSW_MAON_CL ON */
                pmuData  = BIT_FIELD_INSERT32(pmuData, 2, 1, 1);
            }

            /* PSW_MAIN ON */
            pmuData  = BIT_FIELD_INSERT32(pmuData, 0, 1, 0);
            pmu_force_set_register_value_2565(0x2, pmuData);
            hal_gpt_delay_us(100);

            /* PSW_MAIN_CL OFF */
            pmuData  = BIT_FIELD_INSERT32(pmuData, 2, 1, 0);
            pmu_force_set_register_value_2565(0x2, pmuData);

            /* EOC = CHG*/
            pmuData  = pmu_get_register_value_2565(0x1E, 0xFFFF, 0);
            pmuData = BIT_FIELD_INSERT16(pmuData, 5, 3, 0);
            pmu_force_set_register_value_2565(0x1E, pmuData);

            pmu_eoc_ctrl(0);

            /* do not set if plug in from comm mode */
            /* VBUS discharge on */
            U8 tmpdata;
            if (isCommMode == FALSE)
            {
                tmpdata = 1;
            }
            else
            {
                tmpdata = 0;
            }

            pmuData  = pmu_get_register_value_2565(0x32A, 0xFFFF, 0);
            pmuData = BIT_FIELD_INSERT16(pmuData, 11, 1, tmpdata);
            pmu_force_set_register_value_2565(0x32A, pmuData);

            /*charger mode first when charger in */
            DRV_SmartChargerCase_Chg_Mode();
            DRV_SmartCharger_1wire_Send_to_App(DRV_CHARGER_EVENT_CHARGER_IN_PATTERN, 0, 0);

            isCommMode = FALSE;
        }
#else
#ifdef CHARGER_CASE_1WIRE_1568
        data = pmu_auxadc_get_channel_value(PMU_AUX_VBUS);
#else
        hal_adc_get_data_polling(HAL_ADC_CHANNEL_6 , &data);
#endif
        log_hal_msgid_info("[SmartCharger][1Wire]adc = %d\r\n",1, data);

        if(charging_exist == 0)
        {
            /* comm mode */
            if(data > 700 && data < 3000)
            {
                DRV_SmartChargerCase_Comm_Mode();
                xTimerStart(pSmartChargerOutTimer, 0);
            }
            else
            {
                /* real charger out */
                /* send charger out to app */
                DRV_SmartCharger_1wire_Send_to_App(DRV_CHARGER_EVENT_CHARGER_OUT_PATTERN, 0, 0);
                DRV_SmartChargerCase_Chg_Mode();
                hal_gpio_pull_down(CHARGER_1WIRE_RX_GPIO);

                /* down frequncy */
                smart_charger_unlock_dvfs_1wire();
            }
        }
        else if (charging_exist == 1)
        {
            if(lastCmd == DRV_CHARGER_EVENT_CHARGER_OUT_PATTERN)
            {
                /* up frequncy */
                smart_charger_lock_dvfs_1wire();
            }

            /*charger mode first when charger in */
            DRV_SmartChargerCase_Chg_Mode();
            hal_gpio_pull_up(CHARGER_1WIRE_RX_GPIO);
            DRV_SmartCharger_1wire_Send_to_App(DRV_CHARGER_EVENT_CHARGER_IN_PATTERN, 0, 0);
        }
#endif
    }
}

void DRV_SmartChargerCase_1wire_Init(smartcharger_callback_t handler)
{
    lastCmd = DRV_CHARGER_EVENT_CHARGER_OUT_PATTERN;

    pSmartChargerTimer    = xTimerCreate( "Smart Charger", pdMS_TO_TICKS(2), pdFALSE, NULL, DRV_SmartCharger_1wire_CallBack );

    if(!pSmartChargerTimer)
    {
        log_hal_msgid_error("[1Wire]Smart Charger Timer Create fail", 0);
    }

    pSmartChargerOutTimer = xTimerCreate( "Smart Charger", pdMS_TO_TICKS(200), pdTRUE, NULL, DRV_SmartCharger_1wire_CallBack_Out );

    if(!pSmartChargerOutTimer)
    {
        log_hal_msgid_error("[1Wire]Smart Charger Timer Out Create fail", 0);
    }

#if (SMART_CHARGER_CASE_1WIRE_TEST)
    pSmartChargerTimerTest = xTimerCreate( "Smart Charger", pdMS_TO_TICKS(500), pdTRUE, NULL, DRV_SmartCharger_1wire_CallBack_Out_test );

    if(!pSmartChargerTimerTest)
    {
        log_hal_msgid_error("[1Wire]Smart Charger Timer Test Create fail", 0);
    }
#endif

#ifdef CHARGER_CASE_1WIRE_1565

    uint32_t pmuData;

    /* PSW_MAIN_CL = ON , PSW_MAIN = OFF , DISABLE VIO PULL UP */
    pmuData  = pmu_get_register_value_2565(0x2, 0xFFFF, 0);
    pmuData  = BIT_FIELD_INSERT32(pmuData, 2, 1, 1);
    pmuData  = BIT_FIELD_INSERT32(pmuData, 0, 1, 1);
    pmuData  = BIT_FIELD_INSERT32(pmuData, 3, 1, 1);
    pmu_force_set_register_value_2565(0x2, pmuData);

    /* CHG_STATUS = EOC */
    pmuData  = pmu_get_register_value_2565(0x1E, 0xFFFF, 0);
    pmuData = BIT_FIELD_INSERT16(pmuData, 5, 3, 1);
    pmu_force_set_register_value_2565(0x1E, pmuData);

    pmu_eoc_ctrl(1);

    /* set uart tx to disable pull */
    hal_pinmux_set_function(CHARGER_1WIRE_TX_GPIO , CHARGER_1WIRE_TX_GPIO_GPIO);
    hal_gpio_set_direction(CHARGER_1WIRE_TX_GPIO , HAL_GPIO_DIRECTION_INPUT);
    hal_gpio_disable_pull(CHARGER_1WIRE_TX_GPIO);

    /* set uart rx to input pull down */
    hal_pinmux_set_function(CHARGER_1WIRE_RX_GPIO , CHARGER_1WIRE_RX_GPIO_GPIO);
    hal_gpio_set_direction(CHARGER_1WIRE_RX_GPIO , HAL_GPIO_DIRECTION_INPUT);
    hal_gpio_pull_down(CHARGER_1WIRE_RX_GPIO);

    /* VBUS discharge off */
    pmuData  = pmu_get_register_value_2565(0x32A, 0xFFFF, 0);
    pmuData = BIT_FIELD_INSERT16(pmuData, 11, 1, 0);
    pmu_force_set_register_value_2565(0x32A, pmuData);


#else

#ifdef CHARGER_CASE_1WIRE_155x
    /* set gpio0 to adc mode */
    hal_adc_init();
    hal_pinmux_set_function(HAL_GPIO_0 , HAL_GPIO_0_AUXADC6);
#endif

    /* set uart tx to disable pull */
    hal_pinmux_set_function(CHARGER_1WIRE_TX_GPIO , CHARGER_1WIRE_TX_GPIO_GPIO);
    hal_gpio_set_direction(CHARGER_1WIRE_TX_GPIO , HAL_GPIO_DIRECTION_INPUT);
    hal_gpio_disable_pull(CHARGER_1WIRE_TX_GPIO);

    /* set uart rx to input pull down */
    hal_pinmux_set_function(CHARGER_1WIRE_RX_GPIO , CHARGER_1WIRE_RX_GPIO_GPIO);
    hal_gpio_set_direction(CHARGER_1WIRE_RX_GPIO , HAL_GPIO_DIRECTION_INPUT);
    hal_gpio_pull_down(CHARGER_1WIRE_RX_GPIO);

    /* chg/comm gpio init */
    hal_pinmux_set_function(CHARGER_COMM_MODE_GPIO , HAL_GPIO_3_GPIO3);
    hal_gpio_set_direction(CHARGER_COMM_MODE_GPIO , HAL_GPIO_DIRECTION_OUTPUT);
    hal_gpio_set_output(CHARGER_COMM_MODE_GPIO, HAL_GPIO_DATA_HIGH);

#ifdef CHARGER_CASE_1WIRE_1568
    /* enable read vbus adc */
    pmu_set_register_value_ab2568(0x128, PMU_RG_BUS_SNS_LOGIC_SEL_MASK, PMU_RG_BUS_SNS_LOGIC_SEL_SHIFT, 0);
#endif

#endif

    /* init 1 wire uart */
    hal_uart_config_t uart_config;
    hal_uart_status_t status;
    hal_uart_dma_config_t dma_config;

    uart_config.baudrate = HAL_UART_BAUDRATE_115200;
    uart_config.parity = HAL_UART_PARITY_NONE;
    uart_config.stop_bit = HAL_UART_STOP_BIT_1;
    uart_config.word_length = HAL_UART_WORD_LENGTH_8;
    hal_uart_deinit(HAL_UART_1);
    status = hal_uart_init(HAL_UART_1, &uart_config);

    if(status != HAL_UART_STATUS_OK)
    {
        log_hal_msgid_info("[SmartCharger][1Wire]uart init fail status[%d]", 1,status);
    }

    dma_config.receive_vfifo_alert_size = 8;
    dma_config.receive_vfifo_buffer = oneWire_rx;
    dma_config.receive_vfifo_buffer_size = 32;
    dma_config.receive_vfifo_threshold_size = 5;
    dma_config.send_vfifo_buffer = oneWire_tx;
    dma_config.send_vfifo_buffer_size = 32;
    dma_config.send_vfifo_threshold_size = 5;
    status = hal_uart_set_dma(HAL_UART_1, (const hal_uart_dma_config_t *)&dma_config);

    if(status != HAL_UART_STATUS_OK)
    {
        log_hal_msgid_info("[SmartCharger][1Wire]uart set dma fail status[%d]", 1,status);
    }

    hal_uart_register_callback(HAL_UART_1, DRV_SmartChargerCase_1wire_Uart_Cb, NULL);

    if (battery_management_register_callback(battery_management_callback_for_smart_1wire) != BATTERY_MANAGEMENT_STATUS_OK) {
        log_hal_msgid_info("[SmartCharger] Error Cannot register battery callback", 0);
        return;
    }

    log_hal_msgid_info("[SmartCharger][1Wire]uart init done", 0);
}