/* Copyright Statement:
 *
 * (C) 2005-2016  MediaTek Inc. All rights reserved.
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. ("MediaTek") and/or its licensors.
 * Without the prior written permission of MediaTek and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) MediaTek Software
 * if you have agreed to and been bound by the applicable license agreement with
 * MediaTek ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of MediaTek Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT MEDIATEK SOFTWARE RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include "bt_sink_srv_am_task.h"
#include "bt_sink_srv_ami.h"
#include "hal_audio_internal.h"
#include "bt_sink_srv_audio_setting.h"
#include "nvdm.h"
#include "FreeRTOS.h"
#ifdef __AM_DEBUG_INFO__
#include "bt_sink_srv_utils.h"
#endif

#ifdef MTK_EXTERNAL_DSP_ENABLE
#include "external_dsp_application.h"
#endif

#ifdef MTK_AWS_MCE_ENABLE
#include "bt_sink_srv.h"
#ifdef MTK_RACE_CMD_ENABLE
#include "race_xport.h"
#endif
#include "bt_sink_srv_aws_mce.h"
#include "bt_aws_mce_report.h"
#include "bt_aws_mce_report_internal.h"
#endif

#ifdef MTK_ANC_ENABLE
#ifdef MTK_ANC_V2
  #include "anc_control_api.h"
#else
  #include "anc_control.h"
#endif
#endif
#ifdef MTK_RACE_CMD_ENABLE
#include "race_cmd_dsprealtime.h"
#endif
#include "audio_log.h"
#include "nvkey_dspfw.h"
#include "nvkey.h"
#include "hal_nvic.h"
#include "hal_audio_cm4_dsp_message.h"
#include "audio_nvdm_common.h"
//#define ANALOG_VOL_MAX     10 /*depend on HAL*/
//#define DIGITAL_VOL_MAX    10 /*depend on HAL*/

#define AUDIO_SRC_SRV_AMI_SET_FLAG(MASK, FLAG) ((MASK) |= (FLAG))
#define AUDIO_SRC_SRV_AMI_RESET_FLAG(MASK, FLAG) ((MASK) &= ~(FLAG))

bt_sink_srv_am_background_t *g_prCurrent_player = NULL;
#if defined(MTK_EXTERNAL_DSP_NEED_SUPPORT)
am_audio_app_callback_t  g_am_audio_app_callback = NULL;
static I2S_param_for_external_DSP_t g_param_for_external_dsp;
#endif
bt_sink_srv_am_id_t g_aud_id_num = 0;
bt_bd_addr_t g_int_dev_addr = {0};
#ifdef MTK_AVM_DIRECT
SemaphoreHandle_t g_ami_hal_semaphore_handle = NULL;
#endif

//==== Static variables ====
static bt_sink_srv_am_a2dp_sink_latency_t g_a2dp_sink_latency = 140000;
static bt_sink_srv_am_bt_audio_param_t    g_bt_inf_address    = 0x0;
static hal_audio_device_t                 g_afe_device        = HAL_AUDIO_DEVICE_DAC_DUAL;
#if defined(MTK_AVM_DIRECT)
extern HAL_AUDIO_CHANNEL_SELECT_t audio_Channel_Select;
extern HAL_DSP_PARA_AU_AFE_CTRL_t audio_nvdm_HW_config;
extern HAL_AUDIO_DVFS_CLK_SELECT_t audio_nvdm_dvfs_config;
extern bt_sink_srv_am_amm_struct *ptr_callback_amm;
extern bt_sink_srv_am_amm_struct *ptr_isr_callback_amm;
extern void *pxCurrentTCB;
void*    AUDIO_SRC_SRV_AM_TASK = NULL;
#endif

#ifdef SUPPORT_PEQ_NVKEY_UPDATE
static ami_attach_nvdm_ctrl_t ami_attach_nvdm_ctrl = {
    .buffer = NULL,
    .buffer_offset = 0,
    .total_pkt= 0,
    .pre_pkt = 0,
};
#endif

#ifndef WIN32_UT
xSemaphoreHandle g_xSemaphore_ami = NULL;
void ami_mutex_lock(xSemaphoreHandle handle)
{
    if (handle != NULL) {
        xSemaphoreTake(handle, portMAX_DELAY);
    }
}
void ami_mutex_unlock(xSemaphoreHandle handle)
{
    if (handle != NULL) {
        xSemaphoreGive(handle);
    }
}
#else
bt_sink_srv_am_amm_struct *g_prAmm_current = NULL;
#define ami_mutex_lock()   { }
#define ami_mutex_unlock() { }
#endif

/*****************************************************************************
 * FUNCTION
 *  ami_register_get_id
 * DESCRIPTION
 *  Get the redistered ID
 * PARAMETERS
 *  void
 * RETURNS
 *  bt_sink_srv_am_id_t
 *****************************************************************************/
static bt_sink_srv_am_id_t ami_register_get_id(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    bt_sink_srv_am_id_t bAud_id = 0;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    while (g_rAm_aud_id[bAud_id].use != ID_CLOSE_STATE) {
        bAud_id++;
    }
    g_rAm_aud_id[bAud_id].use = ID_IDLE_STATE;
    //g_rAm_aud_id[bAud_id].contain_ptr = NULL;
    g_aud_id_num++;
    return bAud_id;
}

/*****************************************************************************
 * FUNCTION
 *  ami_register_check_id_exist
 * DESCRIPTION
 *  Check if the specified ID is valid
 * PARAMETERS
 *  aud_id          [IN]
 * RETURNS
 *  bt_sink_srv_am_id_t
 *****************************************************************************/
 #if 0
static bt_sink_srv_am_id_t ami_register_check_id_exist(bt_sink_srv_am_id_t aud_id)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if ((aud_id < AM_REGISTER_ID_TOTAL) && (g_rAm_aud_id[aud_id].use != ID_CLOSE_STATE)) {
        return TRUE;
    }
    return FALSE;
}
#endif


/*****************************************************************************
 * FUNCTION
 *  ami_register_delete_id
 * DESCRIPTION
 *  Delete redistered ID
 * PARAMETERS
 *  aud_id           [IN]
 * RETURNS
 *  void
 *****************************************************************************/
static void ami_register_delete_id(bt_sink_srv_am_id_t aud_id)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if (g_rAm_aud_id[aud_id].use != ID_CLOSE_STATE) {
        g_rAm_aud_id[aud_id].use = ID_CLOSE_STATE;
        g_aud_id_num--;
    }
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_get_current_scenario
 * DESCRIPTION
 *  Get current audio scenario
 * PARAMETERS
 *  void
 * RETURNS
 *  bt_sink_srv_am_type_t
 *****************************************************************************/
bt_sink_srv_am_type_t bt_sink_srv_ami_get_current_scenario()
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if(g_prCurrent_player) {
        return g_prCurrent_player->type;
    } else {
        return NONE;
    }
}

#ifdef __GAIN_TABLE_NVDM_DIRECT__
extern uint8_t AUD_A2DP_VOL_OUT_MAX;
extern uint8_t AUD_A2DP_VOL_OUT_DEFAULT;
extern uint8_t AUD_VPRT_VOL_OUT_MAX;
extern uint8_t AUD_VPRT_VOL_OUT_DEFAULT;
extern uint8_t AUD_LINEIN_VOL_OUT_MAX;
extern uint8_t AUD_LINEIN_VOL_OUT_DEFAULT;
#endif
/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_get_a2dp_max_volume_level
 * DESCRIPTION
 *  Get A2DP max volume level which can be changed by config tool
 * PARAMETERS
 *  void
 * RETURNS
 *  bt_sink_srv_am_volume_level_t
 *****************************************************************************/
bt_sink_srv_am_volume_level_t bt_sink_srv_ami_get_a2dp_max_volume_level()
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
#ifdef __BT_SINK_SRV_AUDIO_SETTING_SUPPORT__
    if((AUD_A2DP_VOL_OUT_MAX == 0) || (AUD_A2DP_VOL_OUT_MAX > 100)){ //Gain table max level was 100
        audio_src_srv_report("[AudM]ami_get_a2dp_max_volume_level error. Max level:%d\n",1, AUD_A2DP_VOL_OUT_MAX);
        audio_src_srv_report("[AudM]A2DP max volume level error.", 0);
        configASSERT(0);
    }
    return AUD_A2DP_VOL_OUT_MAX;
#else
    return AUD_VOL_OUT_LEVEL15;
#endif
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_get_a2dp_default_volume_level
 * DESCRIPTION
 *  Get A2DP max volume level which can be changed by config tool
 * PARAMETERS
 *  void
 * RETURNS
 *  bt_sink_srv_am_volume_level_t
 *****************************************************************************/
bt_sink_srv_am_volume_level_t bt_sink_srv_ami_get_a2dp_default_volume_level()
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
#ifdef __BT_SINK_SRV_AUDIO_SETTING_SUPPORT__
    if(AUD_A2DP_VOL_OUT_DEFAULT > AUD_A2DP_VOL_OUT_MAX){ //Gain table max level was AUD_A2DP_VOL_OUT_MAX
        audio_src_srv_report("[AudM]ami_get_a2dp_default_volume_level error. Default level:%d\n",1, AUD_A2DP_VOL_OUT_DEFAULT);
        audio_src_srv_report("[AudM]A2DP default volume level error.", 0);
        configASSERT(0);
    }
    return AUD_A2DP_VOL_OUT_DEFAULT;
#else
    return AUD_VOL_OUT_LEVEL6;
#endif
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_get_vp_max_volume_level
 * DESCRIPTION
 *  Get VP max volume level which can be changed by config tool
 * PARAMETERS
 *  void
 * RETURNS
 *  bt_sink_srv_am_volume_level_t
 *****************************************************************************/
bt_sink_srv_am_volume_level_t bt_sink_srv_ami_get_vp_max_volume_level()
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
#ifdef __BT_SINK_SRV_AUDIO_SETTING_SUPPORT__
    if((AUD_VPRT_VOL_OUT_MAX == 0) || (AUD_VPRT_VOL_OUT_MAX > 15)){ //Gain table max level was 15
        audio_src_srv_report("[AudM]ami_get_vp_max_volume_level error. Max level:%d\n",1, AUD_VPRT_VOL_OUT_MAX);
        //audio_src_srv_report("[AudM]VP max volume level error.", 0);
        //configASSERT(0);
    }
    return AUD_VPRT_VOL_OUT_MAX;
#else
    return AUD_VOL_OUT_LEVEL15;
#endif
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_get_vp_default_volume_level
 * DESCRIPTION
 *  Get VP max volume level which can be changed by config tool
 * PARAMETERS
 *  void
 * RETURNS
 *  bt_sink_srv_am_volume_level_t
 *****************************************************************************/
bt_sink_srv_am_volume_level_t bt_sink_srv_ami_get_vp_default_volume_level()
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
#ifdef __BT_SINK_SRV_AUDIO_SETTING_SUPPORT__
    if(AUD_VPRT_VOL_OUT_DEFAULT > AUD_VPRT_VOL_OUT_MAX){ //Gain table max level was AUD_VPRT_VOL_OUT_MAX
        audio_src_srv_report("[AudM]ami_get_vp_default_volume_level error. Default level:%d\n",1, AUD_VPRT_VOL_OUT_DEFAULT);
        audio_src_srv_report("[AudM]VP default volume level error.", 0);
        configASSERT(0);
    }
    return AUD_VPRT_VOL_OUT_DEFAULT;
#else
    return AUD_VOL_OUT_LEVEL11;
#endif
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_get_lineIN_max_volume_level
 * DESCRIPTION
 *  Get LineIN max volume level which can be changed by config tool
 * PARAMETERS
 *  void
 * RETURNS
 *  bt_sink_srv_am_volume_level_t
 *****************************************************************************/
bt_sink_srv_am_volume_level_t bt_sink_srv_ami_get_lineIN_max_volume_level()
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
#ifdef __BT_SINK_SRV_AUDIO_SETTING_SUPPORT__
    if((AUD_LINEIN_VOL_OUT_MAX == 0) || (AUD_LINEIN_VOL_OUT_MAX > 100)){ //Gain table max level was 100
        audio_src_srv_report("[AudM]ami_get_lineIN_max_volume_level error. Max level:%d\n",1, AUD_LINEIN_VOL_OUT_MAX);
        audio_src_srv_report("[AudM]LineIN max volume level error.", 0);
        configASSERT(0);
    }
    return AUD_LINEIN_VOL_OUT_MAX;
#else
    return AUD_VOL_OUT_LEVEL15;
#endif
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_get_lineIN_default_volume_level
 * DESCRIPTION
 *  Get LineIN max volume level which can be changed by config tool
 * PARAMETERS
 *  void
 * RETURNS
 *  bt_sink_srv_am_volume_level_t
 *****************************************************************************/
bt_sink_srv_am_volume_level_t bt_sink_srv_ami_get_lineIN_default_volume_level()
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
#ifdef __BT_SINK_SRV_AUDIO_SETTING_SUPPORT__
    if(AUD_LINEIN_VOL_OUT_DEFAULT > AUD_LINEIN_VOL_OUT_MAX){ //Gain table max level was AUD_LINEIN_VOL_OUT_MAX
        audio_src_srv_report("[AudM]ami_get_lineIN_default_volume_level error. Default level:%d\n",1, AUD_LINEIN_VOL_OUT_DEFAULT);
        audio_src_srv_report("[AudM]LineIN default volume level error.", 0);
        configASSERT(0);
    }
    return AUD_LINEIN_VOL_OUT_DEFAULT;
#else
    return AUD_VOL_OUT_LEVEL15;
#endif
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_audio_open
 * DESCRIPTION
 *  Use this function to open a new audio handler and get the audio ID.
 * PARAMETERS
 *  priority         [IN]
 *  handler          [IN]
 * RETURNS
 *  bt_sink_srv_am_id_t
 *****************************************************************************/
bt_sink_srv_am_id_t bt_sink_srv_ami_audio_open(bt_sink_srv_am_priority_t priority,
        bt_sink_srv_am_notify_callback handler)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    bt_sink_srv_am_background_t temp_background_t = {0};
    bt_sink_srv_am_id_t bAud_id;
    int32_t pri = 0;
    //bt_sink_srv_am_hal_result_t result = HAL_AUDIO_STATUS_ERROR;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    audio_src_srv_report("[AudM]open-pri: %d, num: %d, cb: 0x%x",3, priority, g_aud_id_num, (unsigned int)handler);
    //printf("[AudM]open-pri: %d, num: %d, cb: 0x%x\n", priority, g_aud_id_num, (unsigned int)handler);

    pri = (int32_t)priority;
    if ((g_aud_id_num < AM_REGISTER_ID_TOTAL) && (pri >= AUD_LOW) && (pri <= AUD_HIGH)) {
        ami_mutex_lock(g_xSemaphore_ami);
        bAud_id = ami_register_get_id();
        ami_mutex_unlock(g_xSemaphore_ami);
#ifdef __AM_DEBUG_INFO__
        audio_src_srv_report("[Sink][AMI] Open func, ID: %d, pri: %d, num: %d",3,
            bAud_id, priority, g_aud_id_num);
#endif
        temp_background_t.aud_id = bAud_id;
        temp_background_t.notify_cb = handler;
        temp_background_t.priority = priority;
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_STREAM_OPEN_REQ, &temp_background_t,
                                 FALSE, NULL);
        return (bt_sink_srv_am_id_t)bAud_id;
    }
    return AUD_ID_INVALID;
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_audio_play
 * DESCRIPTION
 *  Start to play the specified audio handler.
 * PARAMETERS
 *  aud_id           [IN]
 *  capability_t     [IN]
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t bt_sink_srv_ami_audio_play(bt_sink_srv_am_id_t aud_id,
        bt_sink_srv_am_audio_capability_t *capability_t)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    bt_sink_srv_am_background_t temp_background_t = {0};

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    //printf("[AudM]play-id: %d, num: %d, use: %d, ptr: 0x%x, type: %d\n",
        //aud_id, g_aud_id_num, g_rAm_aud_id[aud_id].use,
        //(unsigned int)g_rAm_aud_id[aud_id].contain_ptr, capability_t->type);
    audio_src_srv_report("[AudM]play-id: %d, num: %d, use: %d, ptr: 0x%x, type: %d",5,
        aud_id, g_aud_id_num, g_rAm_aud_id[aud_id].use,
        (unsigned int)g_rAm_aud_id[aud_id].contain_ptr, capability_t->type);
    if ((aud_id < AM_REGISTER_ID_TOTAL) &&
            ((g_rAm_aud_id[aud_id].use == ID_IDLE_STATE) || (g_rAm_aud_id[aud_id].use == ID_RESUME_STATE) || (g_rAm_aud_id[aud_id].use == ID_STOPING_STATE))) {
#ifdef __AM_DEBUG_INFO__
        audio_src_srv_report("[Sink][AMI] Play func, ID: %d",1, aud_id);
#endif

#ifdef MTK_AUTOMOTIVE_SUPPORT
        external_dsp_echo_ref_sw_config(true);
#endif


        temp_background_t.aud_id = aud_id;
        temp_background_t.type = capability_t->type;
        temp_background_t.audio_path_type = capability_t->audio_path_type;
        memcpy(&(temp_background_t.local_context), &(capability_t->codec), sizeof(bt_sink_srv_am_codec_t));
        memcpy(&(temp_background_t.audio_stream_in), &(capability_t->audio_stream_in), sizeof(bt_sink_srv_am_audio_stream_in_t));
        memcpy(&(temp_background_t.audio_stream_out), &(capability_t->audio_stream_out), sizeof(bt_sink_srv_am_audio_stream_out_t));
        memcpy(&g_int_dev_addr, &(capability_t->dev_addr), sizeof(bt_bd_addr_t));
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_STREAM_PLAY_REQ, &temp_background_t,
                                 FALSE, NULL);
        return AUD_EXECUTION_SUCCESS;
    }
    return AUD_EXECUTION_FAIL;
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_audio_stop
 * DESCRIPTION
 *  Stop playing the specified audio handler.
 * PARAMETERS
 *  aud_id           [IN]
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t bt_sink_srv_ami_audio_stop(bt_sink_srv_am_id_t aud_id)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    bt_sink_srv_am_background_t temp_background_t = {0};

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    //printf("[AudM]stop-id: %d, num: %d, use: %d, ptr: 0x%x\n",
        //aud_id, g_aud_id_num, g_rAm_aud_id[aud_id].use, (unsigned int)g_rAm_aud_id[aud_id].contain_ptr);
    audio_src_srv_report("[AudM]stop-id: %d, num: %d, use: %d, ptr: 0x%x",4,
        aud_id, g_aud_id_num, g_rAm_aud_id[aud_id].use, (unsigned int)g_rAm_aud_id[aud_id].contain_ptr);
    if ((aud_id < AM_REGISTER_ID_TOTAL) && (g_rAm_aud_id[aud_id].use == ID_PLAY_STATE)) {
#ifdef __AM_DEBUG_INFO__
        audio_src_srv_report("[Sink][AMI] Stop func, ID: %d",1, aud_id);
#endif

#ifdef MTK_AUTOMOTIVE_SUPPORT
        external_dsp_echo_ref_sw_config(false);
#endif

        temp_background_t.aud_id = aud_id;
        g_rAm_aud_id[aud_id].use = ID_STOPING_STATE;
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_STREAM_STOP_REQ, &temp_background_t,
                                 FALSE, NULL);
        return AUD_EXECUTION_SUCCESS;
    }
    return AUD_EXECUTION_FAIL;
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_audio_close
 * DESCRIPTION
 *  Close the audio handler opened.
 * PARAMETERS
 *  aud_id           [IN]
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t bt_sink_srv_ami_audio_close(bt_sink_srv_am_id_t aud_id)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    bt_sink_srv_am_background_t temp_background_t = {0};

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    //audio_src_srv_report("[Sink][AMI] Pre-close func, ID: %d, use:%d", 2, aud_id, g_rAm_aud_id[aud_id].use);
    //audio_src_srv_report("[AudM]close-id: %d, num: %d, use: %d, ptr: 0x%x\n", 4,
        //aud_id, g_aud_id_num, g_rAm_aud_id[aud_id].use, (unsigned int)g_rAm_aud_id[aud_id].contain_ptr);
    audio_src_srv_report("[AudM]close-id: %d, num: %d, use: %d, ptr: 0x%x",4,
        aud_id, g_aud_id_num, g_rAm_aud_id[aud_id].use, (unsigned int)g_rAm_aud_id[aud_id].contain_ptr);
    if ((aud_id < AM_REGISTER_ID_TOTAL) &&
            ((g_rAm_aud_id[aud_id].use == ID_IDLE_STATE) ||
             (g_rAm_aud_id[aud_id].use == ID_SUSPEND_STATE) ||
             (g_rAm_aud_id[aud_id].use == ID_RESUME_STATE)  ||
             (g_rAm_aud_id[aud_id].use == ID_STOPING_STATE))) {
#ifdef __AM_DEBUG_INFO__
        audio_src_srv_report("[Sink][AMI] Close func, ID: %d",1, aud_id);
#endif
        temp_background_t.aud_id = aud_id;
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_STREAM_CLOSE_REQ, &temp_background_t,
                                 FALSE, NULL);
        ami_mutex_lock(g_xSemaphore_ami);
        ami_register_delete_id(aud_id);
        ami_mutex_unlock(g_xSemaphore_ami);
        return AUD_EXECUTION_SUCCESS;
    }
    return AUD_EXECUTION_FAIL;
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_audio_set_volume
 * DESCRIPTION
 *  Set audio input/output volume.
 * PARAMETERS
 *  aud_id           [IN]
 *  volume_level     [IN]
 *  in_out           [IN]
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t bt_sink_srv_ami_audio_set_volume(bt_sink_srv_am_id_t aud_id,
        bt_sink_srv_am_volume_level_t volume_level,
        bt_sink_srv_am_stream_type_t in_out)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    bt_sink_srv_am_background_t temp_background_t = {0};

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    audio_src_srv_report("[Sink][AMI] set volume: aud_id:%d, volume_level:%d, in_out:%d",3, aud_id, volume_level, in_out);
    if ((aud_id < AM_REGISTER_ID_TOTAL)
        //&& (g_rAm_aud_id[aud_id].use == ID_PLAY_STATE)
        ) {

        temp_background_t.aud_id = aud_id;
        temp_background_t.in_out = in_out;
        if (in_out == STREAM_OUT) {
#ifndef __BT_SINK_SRV_AUDIO_SETTING_SUPPORT__
            if (volume_level < AUD_VOL_OUT_MAX) {
#endif
                temp_background_t.audio_stream_out.audio_volume = (bt_sink_srv_am_volume_level_out_t)volume_level;
#ifndef __BT_SINK_SRV_AUDIO_SETTING_SUPPORT__
            } else {
#ifdef __AM_DEBUG_INFO__
                audio_src_srv_report("[Sink][AMI] Vol-level error", 0);
#endif
                return AUD_EXECUTION_FAIL;
            }
#endif
        } else if (in_out == STREAM_IN) {
#ifndef __BT_SINK_SRV_AUDIO_SETTING_SUPPORT__
            if (volume_level < AUD_VOL_IN_MAX) {
#endif
                temp_background_t.audio_stream_in.audio_volume = (bt_sink_srv_am_volume_level_in_t)volume_level;
#ifndef __BT_SINK_SRV_AUDIO_SETTING_SUPPORT__
            } else {
#ifdef __AM_DEBUG_INFO__
                audio_src_srv_report("[Sink][AMI] Vol-level error", 0);
#endif
                return AUD_EXECUTION_FAIL;
            }
#endif
        }
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_STREAM_SET_VOLUME_REQ, &temp_background_t,
                                 FALSE, NULL);
        return AUD_EXECUTION_SUCCESS;
    }
    return AUD_EXECUTION_FAIL;
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_audio_set_mute
 * DESCRIPTION
 *  Mute audio input/output device.
 * PARAMETERS
 *  aud_id           [IN]
 *  mute             [IN]
 * in_out            [IN]
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t bt_sink_srv_ami_audio_set_mute(bt_sink_srv_am_id_t aud_id,
        bool mute,
        bt_sink_srv_am_stream_type_t in_out)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    bt_sink_srv_am_background_t temp_background_t = {0};

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if(aud_id == FEATURE_NO_NEED_ID){
    /*Special Entry. For not main audio stream mute. And scenario whitch cannot get current ID.*/
        if((STREAM_OUT_2 == in_out)||(STREAM_OUT == in_out)||(STREAM_IN == in_out)){
            temp_background_t.aud_id = FEATURE_NO_NEED_ID;
            temp_background_t.in_out = in_out;
            switch(in_out){
                case STREAM_IN:{
                    temp_background_t.audio_stream_in.audio_mute = mute;
                    break;
                }
                case STREAM_OUT:
                case STREAM_OUT_2:
                    temp_background_t.audio_stream_out.audio_mute = mute;
                    break;
                default:
                    audio_src_srv_report("[Sink][AMI]Set mute no_need_ID in_out error",0);
                    return AUD_EXECUTION_FAIL;
            }
            bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                     MSG_ID_STREAM_MUTE_DEVICE_REQ, &temp_background_t,
                                     FALSE, NULL);
            return AUD_EXECUTION_SUCCESS;
        }else{
            audio_src_srv_report("[Sink][AMI]Set mute no_need_ID error",0);
            return AUD_EXECUTION_FAIL;
        }
    }
    if ((aud_id < AM_REGISTER_ID_TOTAL) &&
            (g_rAm_aud_id[aud_id].use == ID_PLAY_STATE)) {
        temp_background_t.aud_id = aud_id;
        temp_background_t.in_out = in_out;
        if (in_out == STREAM_OUT) {
            temp_background_t.audio_stream_out.audio_mute = mute;
        } else if (in_out == STREAM_IN) {
            temp_background_t.audio_stream_in.audio_mute = mute;
        }
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_STREAM_MUTE_DEVICE_REQ, &temp_background_t,
                                 FALSE, NULL);
        return AUD_EXECUTION_SUCCESS;
    } else {
        temp_background_t.aud_id = FEATURE_NO_NEED_ID;
        temp_background_t.in_out = in_out;
        if (in_out == STREAM_OUT) {
            temp_background_t.audio_stream_out.audio_mute = mute;
        } else if (in_out == STREAM_IN) {
            temp_background_t.audio_stream_in.audio_mute = mute;
        }
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_STREAM_MUTE_DEVICE_REQ, &temp_background_t,
                                 FALSE, NULL);
        return AUD_EXECUTION_SUCCESS;
    }
    return AUD_EXECUTION_FAIL;
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_audio_set_device
 * DESCRIPTION
 *  Set audio input/output device.
 * PARAMETERS
 *  aud_id           [IN]
 *  device           [IN]
 *  in_out           [IN]
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t bt_sink_srv_ami_audio_set_device(bt_sink_srv_am_id_t aud_id,
        bt_sink_srv_am_device_set_t device)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    bt_sink_srv_am_background_t temp_background_t = {0};
    bt_sink_srv_am_id_t bCount = 0;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if ((aud_id < AM_REGISTER_ID_TOTAL) &&
            (g_rAm_aud_id[aud_id].use == ID_PLAY_STATE)) {
        while (device > 0) {
            if ((device & 1) == 1) {
                bCount++;
            }
            device >>= 1;
        }
        if (bCount > 1) {
            return AUD_EXECUTION_FAIL;
        }
        temp_background_t.aud_id = aud_id;
        if (device & DEVICE_OUT_LIST) {
            temp_background_t.in_out = STREAM_OUT;
            temp_background_t.audio_stream_out.audio_device = device;
        } else if (device & DEVICE_IN_LIST) {
            temp_background_t.in_out = STREAM_IN;
            temp_background_t.audio_stream_in.audio_device = device;
        }
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_STREAM_CONFIG_DEVICE_REQ, &temp_background_t,
                                 FALSE, NULL);
        return AUD_EXECUTION_SUCCESS;
    }
    return AUD_EXECUTION_FAIL;
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_audio_continue_stream
 * DESCRIPTION
 *  Continuously write data to audio output for palyback / read data from audio input for record.
 * PARAMETERS
 *  aud_id           [IN]
 *  buffer           [IN/OUT]
 *  data_count       [IN]
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t bt_sink_srv_ami_audio_continue_stream(bt_sink_srv_am_id_t aud_id,
        void *buffer,
        uint32_t data_count)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    bt_sink_srv_am_background_t temp_background_t = {0};

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if ((aud_id < AM_REGISTER_ID_TOTAL) &&
            (g_rAm_aud_id[aud_id].contain_ptr->type == PCM) &&
            ((g_rAm_aud_id[aud_id].use == ID_PLAY_STATE) ||
             (g_rAm_aud_id[aud_id].use == ID_RESUME_STATE))) {
        temp_background_t.aud_id = aud_id;
        temp_background_t.local_context.pcm_format.stream.size = data_count;
        temp_background_t.local_context.pcm_format.stream.buffer = buffer;
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_STREAM_READ_WRITE_DATA_REQ, &temp_background_t,
                                 FALSE, NULL);
        return AUD_EXECUTION_SUCCESS;
    }
    return AUD_EXECUTION_FAIL;
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_audio_get_stream_length
 * DESCRIPTION
 *  Query available input/output data length.
 * PARAMETERS
 *  aud_id           [IN]
 *  data_length      [OUT]
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t bt_sink_srv_ami_audio_get_stream_length(bt_sink_srv_am_id_t aud_id,
        uint32_t *data_length,
        bt_sink_srv_am_stream_type_t in_out)
{
    #if 0
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    bt_sink_srv_am_background_t temp_background_t = {0};

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if (ami_register_check_id_exist(aud_id)) {
        temp_background_t.aud_id = aud_id;
        temp_background_t.in_out = in_out;
        temp_background_t.data_length_ptr = data_length;
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_STREAM_GET_LENGTH_REQ, &temp_background_t,
                                 FALSE, NULL);
        return AUD_EXECUTION_SUCCESS;
    }
    return AUD_EXECUTION_FAIL;
    #endif
    bt_sink_srv_assert(data_length);
    if (STREAM_OUT == in_out) {
        hal_audio_get_stream_out_sample_count(data_length);
    } else if (STREAM_IN == in_out) {
        hal_audio_get_stream_in_sample_count(data_length);
    }

    return AUD_EXECUTION_SUCCESS;
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_set_a2dp_sink_latency
 * DESCRIPTION
 *  SET current a2dp sink latency
 * PARAMETERS
 *  local_a2dp_sink_latency    [IN]
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t bt_sink_srv_ami_set_a2dp_sink_latency(bt_sink_srv_am_a2dp_sink_latency_t a2dp_sink_latency)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if(a2dp_sink_latency) {
        g_a2dp_sink_latency = a2dp_sink_latency;
        return AUD_EXECUTION_SUCCESS;
    } else {
        return AUD_EXECUTION_FAIL;
    }
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_get_a2dp_sink_latency
 * DESCRIPTION
 *  GET current a2dp sink latency
 * PARAMETERS
 *  void
 * RETURNS
 *  bt_sink_srv_am_a2dp_sink_latency_t
 *****************************************************************************/
bt_sink_srv_am_a2dp_sink_latency_t bt_sink_srv_ami_get_a2dp_sink_latency(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
        return g_a2dp_sink_latency;
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_set_bt_inf_address
 * DESCRIPTION
 *  SET current bt information address
 * PARAMETERS
 *  local_bt_inf_address    [IN]
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t bt_sink_srv_ami_set_bt_inf_address(bt_sink_srv_am_bt_audio_param_t bt_inf_address)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if(bt_inf_address) {
        g_bt_inf_address = bt_inf_address;
        return AUD_EXECUTION_SUCCESS;
    } else {
        return AUD_EXECUTION_FAIL;
    }
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_get_bt_inf_address
 * DESCRIPTION
 *  GET current bt information address
 * PARAMETERS
 *  void
 * RETURNS
 *  bt_sink_srv_am_bt_audio_param_t
 *****************************************************************************/
bt_sink_srv_am_bt_audio_param_t bt_sink_srv_ami_get_bt_inf_address(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
        return g_bt_inf_address;
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_set_afe_device
 * DESCRIPTION
 *  SET current afe device
 * PARAMETERS
 *  afe_device    [IN]
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t bt_sink_srv_ami_set_afe_device(hal_audio_device_t afe_device)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if(afe_device) {
        g_afe_device = afe_device;
        return AUD_EXECUTION_SUCCESS;
    } else {
        return AUD_EXECUTION_FAIL;
    }
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_get_afe_device
 * DESCRIPTION
 *  GET current afe device
 * PARAMETERS
 *  void
 * RETURNS
 *  hal_audio_device_t
 *****************************************************************************/
hal_audio_device_t bt_sink_srv_ami_get_afe_device(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
        return g_afe_device;
}

/*****************************************************************************
 * FUNCTION
 *  am_audio_set_pause
 * DESCRIPTION
 *  Pause current audio
 * PARAMETERS
 *  void
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t am_audio_set_pause(void)
{
    bt_sink_srv_am_background_t temp_background_t = {0};

    if(g_prCurrent_player) {
        temp_background_t.aud_id = g_prCurrent_player->aud_id;
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_AUDIO_SET_PAUSE, &temp_background_t,
                                 FALSE, NULL);
    }

    audio_src_srv_report("[AMI] am_audio_set_pause", 0);
    return AUD_EXECUTION_SUCCESS;
}

/*****************************************************************************
 * FUNCTION
 *  am_audio_set_resume
 * DESCRIPTION
 *  Resume audio
 * PARAMETERS
 *
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t am_audio_set_resume(void)
{
    bt_sink_srv_am_background_t temp_background_t = {0};

    if(g_prCurrent_player) {
        temp_background_t.aud_id = g_prCurrent_player->aud_id;
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_AUDIO_SET_RESUME, &temp_background_t,
                                 FALSE, NULL);
    }

    audio_src_srv_report("[AMI] am_audio_set_resume", 0);
    return AUD_EXECUTION_SUCCESS;
}

extern void audio_side_tone_enable_hdlr(bt_sink_srv_am_amm_struct *amm_ptr);
/*****************************************************************************
 * FUNCTION
 *  am_audio_side_tone_enable
 * DESCRIPTION
 *  Enable side tone
 * PARAMETERS
 *
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t am_audio_side_tone_enable(void)
{
    bool is_am_task = false;
    uint32_t savedmask;
    hal_nvic_save_and_set_interrupt_mask(&savedmask);
    if(AUDIO_SRC_SRV_AM_TASK == pxCurrentTCB){
        is_am_task = true;
    } else {
        is_am_task = false;
    }
    hal_nvic_restore_interrupt_mask(savedmask);
    if(is_am_task){
        audio_side_tone_enable_hdlr(NULL);
    } else {
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                             MSG_ID_AUDIO_SIDE_TONE_ENABLE, NULL,
                             FALSE, NULL);
    }

    audio_src_srv_report("[AMI] am_audio_side_tone_enable", 0);
    return AUD_EXECUTION_SUCCESS;
}

/*****************************************************************************
 * FUNCTION
 *  am_audio_side_tone_disable
 * DESCRIPTION
 *  Disable side tone
 * PARAMETERS
 *
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t am_audio_side_tone_disable(void)
{
    bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                             MSG_ID_AUDIO_SIDE_TONE_DISABLE, NULL,
                             FALSE, NULL);

    audio_src_srv_report("[AMI] am_audio_side_tone_disable", 0);
    return AUD_EXECUTION_SUCCESS;
}

/*****************************************************************************
 * FUNCTION
 *  am_audio_dl_suspend
 * DESCRIPTION
 *  Suspend DL 1
 * PARAMETERS
 *
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
volatile uint8_t g_audio_dl_suspend_by_user = false;
bt_sink_srv_am_result_t am_audio_dl_suspend(void)
{
    if(g_prCurrent_player && (g_prCurrent_player->type != NONE))
    {
        g_audio_dl_suspend_by_user = true;
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_AUDIO_DL_SUSPEND, NULL,
                                 FALSE, NULL);

        audio_src_srv_report("[AMI] am_audio_dl_suspend: Scenario:%d",1, g_prCurrent_player->type);
        return AUD_EXECUTION_SUCCESS;
    }
    else
    {
        audio_src_srv_report("[AMI] am_audio_dl_suspend Fail", 0);
        return AUD_EXECUTION_FAIL;
    }
}

/*****************************************************************************
 * FUNCTION
 *  am_audio_dl_resume
 * DESCRIPTION
 *  Resume DL 1
 * PARAMETERS
 *
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t am_audio_dl_resume(void)
{
    g_audio_dl_suspend_by_user = false;
    bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                             MSG_ID_AUDIO_DL_RESUME, NULL,
                             FALSE, NULL);

    audio_src_srv_report("[AMI] am_audio_dl_resume", 0);
    return AUD_EXECUTION_SUCCESS;
}

/*****************************************************************************
 * FUNCTION
 *  am_audio_ul_suspend
 * DESCRIPTION
 *  Suspend UL 1
 * PARAMETERS
 *
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t am_audio_ul_suspend(void)
{
    if(g_prCurrent_player && (g_prCurrent_player->type != NONE))
    {
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_AUDIO_UL_SUSPEND, NULL,
                                 FALSE, NULL);

        audio_src_srv_report("[AMI] am_audio_ul_suspend: Scenario:%d",1, g_prCurrent_player->type);
        return AUD_EXECUTION_SUCCESS;
    }
    else
    {
        audio_src_srv_report("[AMI] am_audio_ul_suspend Fail", 0);
        return AUD_EXECUTION_FAIL;
    }
}

/*****************************************************************************
 * FUNCTION
 *  am_audio_ul_resume
 * DESCRIPTION
 *  Resume UL 1
 * PARAMETERS
 *
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t am_audio_ul_resume(void)
{
    bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                             MSG_ID_AUDIO_UL_RESUME, NULL,
                             FALSE, NULL);

    audio_src_srv_report("[AMI] am_audio_ul_resume", 0);
    return AUD_EXECUTION_SUCCESS;
}

/*****************************************************************************
 * FUNCTION
 *  am_audio_set_feature
 * DESCRIPTION
 *  Set parameters to DSP or set feature control through AM task
 * PARAMETERS
 *
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t am_audio_set_feature(bt_sink_srv_am_id_t aud_id,
    bt_sink_srv_am_feature_t *feature_param)
{
    bt_sink_srv_am_background_t temp_background_t = {0};
    bt_sink_srv_am_feature_t *local_feature = &temp_background_t.local_feature;
    am_feature_type_t feature_type, feature_type_mask;
    uint8_t shift = 0;

    if(feature_param == NULL) {
        audio_src_srv_report("[AMI] am_audio_set_param error, invalid param.", 0);
        return AUD_EXECUTION_FAIL;
    } else if(feature_param->type_mask == 0) {
        audio_src_srv_report("[AMI] am_audio_set_param error, invalid param.", 0);
        return AUD_EXECUTION_FAIL;
    } else if( (g_prCurrent_player == NULL) && (aud_id != FEATURE_NO_NEED_ID) ) {
        audio_src_srv_report("[AMI] am_audio_set_param error, no current player.", 0);
        return AUD_EXECUTION_FAIL;
    }

    feature_type_mask = feature_param->type_mask;

    temp_background_t.aud_id = aud_id;
    if(g_prCurrent_player != NULL) {
        temp_background_t.type = g_prCurrent_player->type;
    }

    do {
        feature_type = feature_type_mask & (1 << shift++);
        feature_type_mask &= (~feature_type);
        if(feature_type == 0)
            continue;

        local_feature->type_mask |= feature_type;

        switch (feature_type)
        {
#if defined(MTK_AMP_DC_COMPENSATION_ENABLE)
            case DC_COMPENSATION:
                break;
#endif

#ifdef MTK_PEQ_ENABLE
            case AM_A2DP_PEQ:
            {
                memcpy(&local_feature->feature_param.peq_param, &feature_param->feature_param.peq_param, sizeof(bt_sink_srv_am_peq_param_t));
                break;
            }
#endif
#ifdef MTK_LINEIN_PEQ_ENABLE
            case AM_LINEIN_PEQ:
            {
                memcpy(&local_feature->feature_param.peq_param, &feature_param->feature_param.peq_param, sizeof(bt_sink_srv_am_peq_param_t));
                break;
            }
#endif
#ifdef MTK_ANC_ENABLE
            case AM_ANC:
            {
#ifdef MTK_ANC_V2
                local_feature->feature_param.anc_param.event = feature_param->feature_param.anc_param.event;
                memcpy(&local_feature->feature_param.anc_param.cap, &feature_param->feature_param.anc_param.cap, sizeof(anc_control_cap_t));
#if 0 //for debug
                audio_src_srv_report("am_audio_set_feature before send filter(0x%x) value(0x%x).\r\n", 2, local_feature->feature_param.anc_param.cap.filter_cap.filter_mask, local_feature->feature_param.anc_param.cap.filter_cap.param.Dgain);
                audio_src_srv_report("am_audio_set_feature with event(%d) flash_id(%d) type(%d) filter_mask(0x%x) sram_bank(%d) sram_bank_mask(0x%x)\n", 6,
                       local_feature->feature_param.anc_param.event, local_feature->feature_param.anc_param.cap.filter_cap.flash_id,
                       local_feature->feature_param.anc_param.cap.filter_cap.type, local_feature->feature_param.anc_param.cap.filter_cap.filter_mask, local_feature->feature_param.anc_param.cap.filter_cap.sram_bank, local_feature->feature_param.anc_param.cap.filter_cap.sram_bank_mask);
#endif
                break;
#else
                local_feature->feature_param.anc_param.event = feature_param->feature_param.anc_param.event;
                local_feature->feature_param.anc_param.filter = feature_param->feature_param.anc_param.filter;
                local_feature->feature_param.anc_param.sw_gain = feature_param->feature_param.anc_param.sw_gain;
                local_feature->feature_param.anc_param.user_runtime_gain = feature_param->feature_param.anc_param.user_runtime_gain;
                local_feature->feature_param.anc_param.param = feature_param->feature_param.anc_param.param;
                local_feature->feature_param.anc_param.anc_control_callback = feature_param->feature_param.anc_param.anc_control_callback;
                break;
#endif
            }
#endif
#ifdef MTK_PROMPT_SOUND_ENABLE
            case AM_VP:
            {
                memcpy(&local_feature->feature_param.vp_param, &feature_param->feature_param.vp_param, sizeof(bt_sink_srv_am_vp_param_t));
                break;
            }
#endif
            case AM_DYNAMIC_CHANGE_DSP_SETTING:
            {
                local_feature->feature_param.channel = feature_param->feature_param.channel;
                break;
            }
#ifdef MTK_USER_TRIGGER_FF_ENABLE
            case AM_ADAPTIVE_FF:
            {
                memcpy(&local_feature->feature_param.adaptive_ff_param, &feature_param->feature_param.adaptive_ff_param, sizeof(bt_sink_srv_am_adaptive_ff_param_t));
                break;
            }
#endif
            default:
                break;
        }
    } while(feature_type_mask != 0);

    bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                             MSG_ID_AUDIO_SET_FEATURE, &temp_background_t,
                             FALSE, NULL);

    //audio_src_srv_report("[AMI] am_audio_set_param (type: 0x%x)", 1, feature_type_mask);
    return AUD_EXECUTION_SUCCESS;
}

/*****************************************************************************
 * FUNCTION
 *  am_audio_set_feature_ISR
 * DESCRIPTION
 *  Set parameters to DSP or set feature control through AM task
 * PARAMETERS
 *
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t am_audio_set_feature_ISR(bt_sink_srv_am_id_t aud_id,
    bt_sink_srv_am_feature_t *feature_param)
{
    bt_sink_srv_am_background_t temp_background_t = {0};
    bt_sink_srv_am_feature_t *local_feature = &temp_background_t.local_feature;
    am_feature_type_t feature_type, feature_type_mask;
    uint8_t shift = 0;

    if(feature_param == NULL) {
        audio_src_srv_report("[AMI] am_audio_set_param error, invalid param.", 0);
        return AUD_EXECUTION_FAIL;
    } else if(feature_param->type_mask == 0) {
        audio_src_srv_report("[AMI] am_audio_set_param error, invalid param.", 0);
        return AUD_EXECUTION_FAIL;
    } else if( (g_prCurrent_player == NULL) && (aud_id != FEATURE_NO_NEED_ID) ) {
        audio_src_srv_report("[AMI] am_audio_set_param error, no current player.", 0);
        return AUD_EXECUTION_FAIL;
    }

    feature_type_mask = feature_param->type_mask;

    temp_background_t.aud_id = aud_id;
    if(g_prCurrent_player != NULL) {
        temp_background_t.type = g_prCurrent_player->type;
    }

    do {
        feature_type = feature_type_mask & (1 << shift++);
        feature_type_mask &= (~feature_type);
        if(feature_type == 0)
            continue;

        local_feature->type_mask |= feature_type;

        switch (feature_type)
        {

#ifdef MTK_AWS_MCE_ENABLE
            case AM_HFP_AVC:
            {
                local_feature->feature_param.avc_vol = feature_param->feature_param.avc_vol;
                break;
            }
#endif

            default:
                break;
        }
    } while(feature_type_mask != 0);

    bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                             MSG_ID_AUDIO_SET_FEATURE, &temp_background_t,
                             TRUE, ptr_callback_amm);

    //audio_src_srv_report("[AMI] am_audio_set_param (type: 0x%x)", 1, feature_type_mask);
    return AUD_EXECUTION_SUCCESS;
}

/*****************************************************************************
 * FUNCTION
 *
 *
 *
 *
 *****************************************************************************/
bt_sink_srv_am_priority_table_t g_pseudo_device_pri_table[AM_TYPE_TOTAL] = { {AUD_HIGH   , AUD_LOW  , AUD_LOW    , AUD_LOW},    //PCM
                                                                             {AUD_MIDDLE , AUD_LOW  , AUD_LOW    , AUD_LOW},    //A2DP
                                                                             {AUD_HIGH   , AUD_LOW  , AUD_HIGH   , AUD_LOW},    //HFP
                                                                             {AUD_LOW    , AUD_LOW  , AUD_LOW    , AUD_LOW},    //NONE
                                                                             {AUD_LOW    , AUD_LOW  , AUD_LOW    , AUD_LOW},    //FILES
                                                                             {AUD_MIDDLE , AUD_LOW  , AUD_LOW    , AUD_LOW},    //AWS
                                                                             {AUD_LOW    , AUD_LOW  , AUD_MIDDLE , AUD_LOW},    //RECORD
                                                                             {AUD_LOW    , AUD_HIGH , AUD_LOW    , AUD_LOW},    //VOICE_PROMPT
                                                                             {AUD_MIDDLE , AUD_LOW  , AUD_MIDDLE , AUD_LOW},    //LINE_IN
                                                                             };
static bt_sink_srv_am_priority_table_t ami_register_get_pri_table(bt_sink_srv_am_type_t type)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/

    return g_pseudo_device_pri_table[type];
}

bt_sink_srv_am_id_t ami_audio_open(bt_sink_srv_am_type_t pseudo_device_type,
                                        bt_sink_srv_am_notify_callback handler)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    bt_sink_srv_am_background_t temp_background_t = {0};
    bt_sink_srv_am_id_t bAud_id;
    bt_sink_srv_am_priority_table_t pri_table;
    //bt_sink_srv_am_hal_result_t result = HAL_AUDIO_STATUS_ERROR;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    audio_src_srv_report("[AudM]open-type: %d, num: %d, cb: 0x%x",3, pseudo_device_type, g_aud_id_num, (unsigned int)handler);
    //printf("[AudM]open-pri: %d, num: %d, cb: 0x%x\n", priority, g_aud_id_num, (unsigned int)handler);

    pri_table = ami_register_get_pri_table(pseudo_device_type);
    if ((g_aud_id_num < AM_REGISTER_ID_TOTAL)) {
        ami_mutex_lock(g_xSemaphore_ami);
        bAud_id = ami_register_get_id();
        ami_mutex_unlock(g_xSemaphore_ami);
#ifdef __AM_DEBUG_INFO__
        audio_src_srv_report("[Sink][AMI] Open func(edit), ID: %d, type: %d, num: %d",3,
            bAud_id, pseudo_device_type, g_aud_id_num);
#endif
        temp_background_t.aud_id    = bAud_id;
        temp_background_t.type      = pseudo_device_type;
        temp_background_t.notify_cb = handler;
        temp_background_t.priority  = AUD_LOW;
        temp_background_t.priority_table = pri_table;
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_STREAM_OPEN_REQ, &temp_background_t,
                                 FALSE, NULL);
        return (bt_sink_srv_am_id_t)bAud_id;
    }
    return AUD_ID_INVALID;
}

bt_sink_srv_am_result_t ami_audio_play(bt_sink_srv_am_id_t aud_id,
                                           bt_sink_srv_am_audio_capability_t *capability_t)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    bt_sink_srv_am_background_t temp_background_t = {0};

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    //printf("[AudM]play-id: %d, num: %d, use: %d, ptr: 0x%x, type: %d\n",
        //aud_id, g_aud_id_num, g_rAm_aud_id[aud_id].use,
        //(unsigned int)g_rAm_aud_id[aud_id].contain_ptr, capability_t->type);
    audio_src_srv_report("[AudM]Test_play-id: %d, num: %d, use: %d, ptr: 0x%x, type: %d",5,
        aud_id, g_aud_id_num, g_rAm_aud_id[aud_id].use,
        (unsigned int)g_rAm_aud_id[aud_id].contain_ptr, capability_t->type);
    if ((aud_id < AM_REGISTER_ID_TOTAL) &&
            ((g_rAm_aud_id[aud_id].use == ID_IDLE_STATE) || (g_rAm_aud_id[aud_id].use == ID_RESUME_STATE) || (g_rAm_aud_id[aud_id].use == ID_STOPING_STATE))) {
#ifdef __AM_DEBUG_INFO__
        audio_src_srv_report("[Sink][AMI] Play func, ID: %d",1, aud_id);
#endif

#ifdef MTK_AUTOMOTIVE_SUPPORT
        external_dsp_echo_ref_sw_config(true);
#endif


        temp_background_t.aud_id = aud_id;
        temp_background_t.type = capability_t->type;
        temp_background_t.audio_path_type = capability_t->audio_path_type;
        memcpy(&(temp_background_t.local_context), &(capability_t->codec), sizeof(bt_sink_srv_am_codec_t));
        memcpy(&(temp_background_t.audio_stream_in), &(capability_t->audio_stream_in), sizeof(bt_sink_srv_am_audio_stream_in_t));
        memcpy(&(temp_background_t.audio_stream_out), &(capability_t->audio_stream_out), sizeof(bt_sink_srv_am_audio_stream_out_t));
        memcpy(&g_int_dev_addr, &(capability_t->dev_addr), sizeof(bt_bd_addr_t));
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_STREAM_PLAY_REQ_TEST, &temp_background_t,
                                 FALSE, NULL);
        return AUD_EXECUTION_SUCCESS;
    }
    return AUD_EXECUTION_FAIL;
}

bt_sink_srv_am_result_t ami_audio_stop(bt_sink_srv_am_id_t aud_id)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    bt_sink_srv_am_background_t temp_background_t = {0};

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    //printf("[AudM]stop-id: %d, num: %d, use: %d, ptr: 0x%x\n",
        //aud_id, g_aud_id_num, g_rAm_aud_id[aud_id].use, (unsigned int)g_rAm_aud_id[aud_id].contain_ptr);
    audio_src_srv_report("[AudM]stop-id: %d, num: %d, use: %d, ptr: 0x%x",4,
        aud_id, g_aud_id_num, g_rAm_aud_id[aud_id].use, (unsigned int)g_rAm_aud_id[aud_id].contain_ptr);
    if ((aud_id < AM_REGISTER_ID_TOTAL) && (g_rAm_aud_id[aud_id].use == ID_PLAY_STATE)) {
#ifdef __AM_DEBUG_INFO__
        audio_src_srv_report("[Sink][AMI] Stop func, ID: %d",1, aud_id);
#endif

#ifdef MTK_AUTOMOTIVE_SUPPORT
        external_dsp_echo_ref_sw_config(false);
#endif

        temp_background_t.aud_id = aud_id;
        g_rAm_aud_id[aud_id].use = ID_STOPING_STATE;
        bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_STREAM_STOP_REQ_TEST, &temp_background_t,
                                 FALSE, NULL);
        return AUD_EXECUTION_SUCCESS;
    }
    return AUD_EXECUTION_FAIL;
}

audio_channel_t ami_get_audio_channel(void)
{
    /*Get Stream out channel.*/
    if(audio_Channel_Select.modeForAudioChannel){
        //HW mode
        //ToDO
        return AUDIO_CHANNEL_NONE;
    } else {
        if((audio_Channel_Select.audioChannel & 0x0F) == AU_DSP_CH_L){
            return AUDIO_CHANNEL_L;
        } else if ((audio_Channel_Select.audioChannel & 0x0F) == AU_DSP_CH_R){
            return AUDIO_CHANNEL_R;
        }
    }
    return AUDIO_CHANNEL_NONE;
}

bt_sink_srv_am_result_t ami_set_audio_channel(audio_channel_t set_in_channel, audio_channel_t set_out_channel, bool rewrite_nv)
{
    nvdm_status_t result;
    uint8_t channel_temp = 0x00;

    if(audio_Channel_Select.modeForAudioChannel){
        //HW mode
        //ToDO
        return AUD_EXECUTION_FAIL;
    } else {
        //Set stream IN channel
        if(set_in_channel == AUDIO_CHANNEL_SWAP){
            channel_temp &= 0x0F;
            channel_temp |= AU_DSP_CH_SWAP << 4;
        } else if(set_in_channel == AUDIO_CHANNEL_L){
            channel_temp &= 0x0F;
            channel_temp |= AU_DSP_CH_L << 4;
        } else if(set_in_channel == AUDIO_CHANNEL_R){
            channel_temp &= 0x0F;
            channel_temp |= AU_DSP_CH_R << 4;
        } else {
            channel_temp &= 0x0F;
            channel_temp |= AU_DSP_CH_LR << 4;
        }

        //Set stream OUT channel
        if (set_out_channel == AUDIO_CHANNEL_SWAP){
            channel_temp &= 0xF0;
            channel_temp |= AU_DSP_CH_SWAP;
        } else if(set_out_channel == AUDIO_CHANNEL_L){
            channel_temp &= 0xF0;
            channel_temp |= AU_DSP_CH_L;
        } else if(set_out_channel == AUDIO_CHANNEL_R){
            channel_temp &= 0xF0;
            channel_temp |= AU_DSP_CH_R;
        } else {
            channel_temp &= 0xF0;
            channel_temp |= AU_DSP_CH_LR;
        }

        //Update Audio In/Out Channel Table
        audio_Channel_Select.audioChannel = channel_temp;

        //Update Audio Channel NVDM.
        if(rewrite_nv){
            result = nvkey_write_data(NVKEYID_APP_AUDIO_CHANNEL, (const uint8_t *)&audio_Channel_Select, sizeof(audio_Channel_Select));
            if(result){
                audio_src_srv_report("Audio Channel NVDM write error", 0);
                return AUD_EXECUTION_FAIL;
            }
        }
    }
    return AUD_EXECUTION_SUCCESS;
}

bt_sink_srv_am_result_t ami_set_audio_device(bt_sink_srv_am_stream_type_t in_out, audio_scenario_sel_t Audio_or_Voice, hal_audio_device_t Device, hal_audio_interface_t i2s_interface, bool rewrite_nv)
{
    nvdm_status_t result;
    uint8_t device_temp = 0;
    if(in_out == STREAM_OUT){// in: 1, out:2
        if( (Device & HAL_AUDIO_DEVICE_DAC_DUAL) || (Device & HAL_AUDIO_DEVICE_EXT_CODEC)){
            //Update Audio Out Device Table
            switch(Device){
                case HAL_AUDIO_DEVICE_I2S_MASTER:{
                    device_temp = device_temp | 0x30;
                    break;
                }
                case HAL_AUDIO_DEVICE_DAC_DUAL:{
                    device_temp = device_temp | 0x20;
                    break;
                }
                case HAL_AUDIO_DEVICE_DAC_R:{
                    device_temp = device_temp | 0x10;
                    break;
                }
                case HAL_AUDIO_DEVICE_DAC_L:{
                    //device_temp = device_temp | 0x00;
                    break;
                }
                default:
                    audio_src_srv_report("Audio set out_device error", 0);
                    break;
            }
            switch(i2s_interface){
                case HAL_AUDIO_INTERFACE_1:{
                    //device_temp = device_temp | 0x00;
                    break;
                }
                case HAL_AUDIO_INTERFACE_2:{
                    device_temp = device_temp | 0x01;
                    break;
                }
                case HAL_AUDIO_INTERFACE_3:{
                    device_temp = device_temp | 0x02;
                    break;
                }
                case HAL_AUDIO_INTERFACE_4:{
                    device_temp = device_temp | 0x03;
                    break;
                }
                default:
                    audio_src_srv_report("Audio set out_device error", 0);
                    break;
            }
            if(Audio_or_Voice == AU_DSP_VOICE){
                audio_nvdm_HW_config.Voice_OutputDev = device_temp;
            } else if (Audio_or_Voice == AU_DSP_AUDIO){
                audio_nvdm_HW_config.Audio_OutputDev = device_temp;
            } else if (Audio_or_Voice == AU_DSP_ANC){
                audio_nvdm_HW_config.ANC_OutputDev = device_temp;
            }
            //Update Audio Out Device NVDM.
            if(rewrite_nv){
                result = nvkey_write_data(NVKEYID_APP_AUDIO_HW_IO_CONFIGURE, (const uint8_t *)&audio_nvdm_HW_config, sizeof(audio_nvdm_HW_config));
                if(result){
                    audio_src_srv_report("Audio out_device NVDM write error", 0);
                }
            }
        } else {
            audio_src_srv_report("Audio set out_device device error", 0);
            return AUD_EXECUTION_FAIL;
        }
    } else {
        if( (Device & 0x003f) || (Device & HAL_AUDIO_DEVICE_EXT_CODEC)){
            //Update Audio In Device Table
            switch(Device){
                case HAL_AUDIO_DEVICE_I2S_MASTER:{
                    device_temp = device_temp | 0x80;
                    break;
                }
                case HAL_AUDIO_DEVICE_DIGITAL_MIC_DUAL:{
                    device_temp = device_temp | 0x42;
                    break;
                }
                case HAL_AUDIO_DEVICE_DIGITAL_MIC_R:{
                    device_temp = device_temp | 0x41;
                    break;
                }
                case HAL_AUDIO_DEVICE_DIGITAL_MIC_L:{
                    device_temp = device_temp | 0x40;
                    break;
                }
                case HAL_AUDIO_DEVICE_MAIN_MIC_DUAL:{
                    device_temp = device_temp | 0x08;
                    break;
                }
                case HAL_AUDIO_DEVICE_MAIN_MIC_R:{
                    device_temp = device_temp | 0x04;
                    break;
                }
                case HAL_AUDIO_DEVICE_MAIN_MIC_L:{
                    //device_temp = device_temp | 0x00;
                    break;
                }
                default:
                    audio_src_srv_report("Audio set in_device enum(%d) error", 1, Device);
                    break;
            }
            switch(i2s_interface){
                case HAL_AUDIO_INTERFACE_1:{
                    //device_temp = device_temp | 0x00;
                    break;
                }
                case HAL_AUDIO_INTERFACE_2:{
                    device_temp = device_temp | 0x10;
                    break;
                }
                case HAL_AUDIO_INTERFACE_3:{
                    device_temp = device_temp | 0x20;
                    break;
                }
                case HAL_AUDIO_INTERFACE_4:{
                    device_temp = device_temp | 0x30;
                    break;
                }
                default:
                    audio_src_srv_report("Audio set In_device error", 0);
                    break;
            }
            if(Audio_or_Voice == AU_DSP_VOICE){
                audio_nvdm_HW_config.Voice_InputDev = device_temp;
            } else if(Audio_or_Voice == AU_DSP_AUDIO){
                audio_src_srv_report("Audio set Audio In_device error", 0);
            } else if(Audio_or_Voice == AU_DSP_ANC){
                audio_nvdm_HW_config.MIC_Select_ANC_FF = (Device == HAL_AUDIO_DEVICE_MAIN_MIC_R) ? 0x01 : 0x00; //temp: only support to choose main mic.
            } else if(Audio_or_Voice == AU_DSP_RECORD){
                switch(Device){
                    case HAL_AUDIO_DEVICE_DIGITAL_MIC_DUAL:{
                        audio_nvdm_HW_config.MIC_Select_Record_Main = 0x02;
                        audio_nvdm_HW_config.MIC_Select_Record_Ref  = 0x03;
                        break;
                    }
                    case HAL_AUDIO_DEVICE_DIGITAL_MIC_R:{
                        audio_nvdm_HW_config.MIC_Select_Record_Main = 0x03;
                        break;
                    }
                    case HAL_AUDIO_DEVICE_DIGITAL_MIC_L:{
                        audio_nvdm_HW_config.MIC_Select_Record_Main = 0x02;
                        break;
                    }
                    case HAL_AUDIO_DEVICE_MAIN_MIC_DUAL:{
                        audio_nvdm_HW_config.MIC_Select_Record_Main = 0x00;
                        audio_nvdm_HW_config.MIC_Select_Record_Ref  = 0x01;
                        break;
                    }
                    case HAL_AUDIO_DEVICE_MAIN_MIC_R:{
                        audio_nvdm_HW_config.MIC_Select_Record_Main = 0x01;
                        break;
                    }
                    case HAL_AUDIO_DEVICE_MAIN_MIC_L:{
                        audio_nvdm_HW_config.MIC_Select_Record_Main = 0x00;
                        break;
                    }
                    case HAL_AUDIO_DEVICE_I2S_MASTER:
                    default:
                        audio_nvdm_HW_config.MIC_Select_Record_Main = 0x00;
                        audio_src_srv_report("Record set in_device error", 0);
                        break;
                }
            }

            //Update Audio In Device NVDM.
            if(rewrite_nv){
                result = nvkey_write_data(NVKEYID_APP_AUDIO_HW_IO_CONFIGURE, (const uint8_t *)&audio_nvdm_HW_config, sizeof(audio_nvdm_HW_config));
                if(result){
                    audio_src_srv_report("Audio in_device NVDM write error", 0);
                }
            }
        } else {
            audio_src_srv_report("Audio set in_device device error", 0);
            return AUD_EXECUTION_FAIL;
        }
    }
    return AUD_EXECUTION_SUCCESS;
}

bt_sink_srv_am_result_t ami_get_audio_device(void)
{
    audio_src_srv_report("Get Audio_in device(0x%x) Audio_out device(0x%x) Voice_in device(0x%x) Voice_out device(0x%x)", 4, audio_nvdm_HW_config.Audio_InputDev, audio_nvdm_HW_config.Audio_OutputDev, audio_nvdm_HW_config.Voice_InputDev, audio_nvdm_HW_config.Voice_OutputDev);
    return AUD_EXECUTION_SUCCESS;
}

extern volatile uint32_t g_am_task_mask;
void ami_set_audio_mask (uint32_t flag, bool isSet)
{
    uint32_t savedmask;
    hal_nvic_save_and_set_interrupt_mask(&savedmask);
    if(isSet){
        AUDIO_SRC_SRV_AMI_SET_FLAG(g_am_task_mask, flag);
    } else {
        AUDIO_SRC_SRV_AMI_RESET_FLAG(g_am_task_mask, flag);
    }
    hal_nvic_restore_interrupt_mask(savedmask);
}

#if defined(MTK_EXTERNAL_DSP_NEED_SUPPORT)
bt_sink_srv_am_result_t ami_set_app_notify_callback(am_audio_app_callback_t ami_app_callback)
{
    audio_src_srv_report("Set ami app callback(0x%x)", 1, ami_app_callback);
    g_am_audio_app_callback = ami_app_callback;
    return AUD_EXECUTION_SUCCESS;
}

bt_sink_srv_am_result_t ami_set_afe_param (bt_sink_srv_am_stream_type_t stream_type, hal_audio_sampling_rate_t sampling_rate, bool enable)
{
    audio_src_srv_report("[Rdebug]ami_set_afe_param type(%d) enable(%d) spl_hz(%d)", 3, stream_type, enable, sampling_rate);
    uint32_t savedmask;
    audio_app_param_t app_param;
    app_param.I2S_param.Enable = enable;
    app_param.I2S_param.sampling_rate = sampling_rate;
    bool is_main_stream = false;
    bool main_stream_exist = false;
    bool DL2_exist = false;
    hal_nvic_save_and_set_interrupt_mask(&savedmask);
    if(enable){
        if(stream_type != STREAM_OUT_2){
            is_main_stream = true;
            if(g_am_task_mask & AM_TASK_MASK_VP_HAPPENING){
               DL2_exist = true;
            }
        } else {
            is_main_stream = false;
            if((g_am_task_mask & AM_TASK_MASK_DL1_HAPPENING) ||
                (g_am_task_mask & AM_TASK_MASK_SIDE_TONE_ENABLE) ||
                (g_am_task_mask & AM_TASK_MASK_UL1_HAPPENING)){
               main_stream_exist = true;
            }
        }
        if(is_main_stream){
            if(DL2_exist){
                hal_nvic_restore_interrupt_mask(savedmask);
                //**SUSPEND. No need to Notify app sampling rate.
            } else {
                hal_nvic_restore_interrupt_mask(savedmask);
                //**Notify app sampling rate.
                if(g_am_audio_app_callback != NULL){
                    if(g_param_for_external_dsp.Enable == false){
                        g_am_audio_app_callback(AUDIO_APPS_EVENTS_SAMPLE_RATE_CHANGE, app_param);
                    }
                    g_param_for_external_dsp.Enable        = app_param.I2S_param.Enable;
                    g_param_for_external_dsp.sampling_rate = app_param.I2S_param.sampling_rate;
                }
            }
        } else {
            if(main_stream_exist){
                hal_nvic_restore_interrupt_mask(savedmask);
                //**No need to Notify app sampling rate.
            } else {
                hal_nvic_restore_interrupt_mask(savedmask);
                //**Notify app sampling rate.
                if(g_am_audio_app_callback != NULL){
                    g_am_audio_app_callback(AUDIO_APPS_EVENTS_SAMPLE_RATE_CHANGE, app_param);
                    g_param_for_external_dsp.Enable        = app_param.I2S_param.Enable;
                    g_param_for_external_dsp.sampling_rate = app_param.I2S_param.sampling_rate;
                }
            }
        }
    } else {
        hal_nvic_restore_interrupt_mask(savedmask);
        if(false == ((g_am_task_mask & AM_TASK_MASK_VP_HAPPENING) ||
                      (g_am_task_mask & AM_TASK_MASK_DL1_HAPPENING) ||
                      (g_am_task_mask & AM_TASK_MASK_SIDE_TONE_ENABLE) ||
                      (g_am_task_mask & AM_TASK_MASK_UL1_HAPPENING)))
        {
            //**Notify app disable.
            if(g_am_audio_app_callback != NULL){
                g_am_audio_app_callback(AUDIO_APPS_EVENTS_SAMPLE_RATE_CHANGE, app_param);
                g_param_for_external_dsp.Enable 	   = app_param.I2S_param.Enable;
                g_param_for_external_dsp.sampling_rate = app_param.I2S_param.sampling_rate;
            }
        }
    }
    return AUD_EXECUTION_SUCCESS;
}
#endif

#ifdef MTK_PROMPT_SOUND_ENABLE
/*****************************************************************************
 * FUNCTION
 *  am_prompt_control_play_tone
 * DESCRIPTION
 *  This function is prompt_sound_control ami interface which is used to lead VP request calling API
 *  or calling task event. (APP VP request >> AM Task event >> MP3 Task event)
 * PARAMETERS
 *  tone_type       [IN]
 *  *tone_buf       [IN]
 *  tone_size        [IN]
 *  sync_time       [IN]
 *  callback          [OUT]
 *SAMPLE CODE:
 *     @code
 *         static const uint8_t voice_prompt_data[];    //have to be static data.
 *         tone_buf = (uint8_t *)voice_prompt_mix_mp3_tone_long;
 *         tone_size = sizeof(voice_prompt_mix_mp3_tone_long);
 *         am_prompt_control_play_tone(VPC_MP3, tone_buf, tone_size, 0, voice_prompt_callback);
 *     @endcode
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t am_prompt_control_play_sync_tone(prompt_control_tone_type_t tone_type,
                                                            uint8_t *tone_buf,
                                                            uint32_t tone_size,
                                                            uint32_t sync_time,
                                                            prompt_control_callback_t callback)
{
#ifdef MTK_MP3_TASK_DEDICATE
    audio_src_srv_report("[AMI] am_audio_prompt_control_play_tone", 0);
    bt_sink_srv_am_feature_t feature_param;
    feature_param.type_mask = AM_VP;
    feature_param.feature_param.vp_param.event = PROMPT_CONTROL_MEDIA_PLAY;

    feature_param.feature_param.vp_param.tone_type       = tone_type;
    feature_param.feature_param.vp_param.tone_buf        = tone_buf;
    feature_param.feature_param.vp_param.tone_size       = tone_size;
    feature_param.feature_param.vp_param.sync_time       = sync_time;
    feature_param.feature_param.vp_param.vp_end_callback = callback;

    return am_audio_set_feature(FEATURE_NO_NEED_ID,&feature_param);
#else
    audio_src_srv_report("[AMI] prompt_control_play_tone [MP3 Task dynamic]", 0);
    bool ret = false;
    ret = prompt_control_play_tone_internal(tone_type, tone_buf, tone_size, sync_time, callback);
    if (ret == true){
        return AUD_EXECUTION_SUCCESS;
    } else {
        return AUD_EXECUTION_FAIL;
    }
#endif
}

/*****************************************************************************
 * FUNCTION
 *  am_prompt_control_stop_tone
 * DESCRIPTION
 *  This function is prompt_sound_control ami interface which is used to lead VP request calling API
 *  or calling task event. (APP VP request >> AM Task event >> MP3 Task event)
 * PARAMETERS
 *  void
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t am_prompt_control_stop_tone(void)
{
#ifdef MTK_MP3_TASK_DEDICATE
    #if 1
    audio_src_srv_report("[AMI] am_audio_prompt_control_stop_tone", 0);
    bt_sink_srv_am_feature_t feature_param;
    feature_param.type_mask = AM_VP;
    feature_param.feature_param.vp_param.event = PROMPT_CONTROL_MEDIA_STOP;

    am_audio_set_feature(FEATURE_NO_NEED_ID,&feature_param);
    #else
    audio_src_srv_report("[AMI] am_audio_prompt_control_stop_tone direct.", 0);
    prompt_control_stop_tone_internal();
    #endif
#else
    audio_src_srv_report("[AMI] prompt_control_stop_tone [MP3 Task dynamic]", 0);
    prompt_control_stop_tone_internal();
#endif
    return AUD_EXECUTION_SUCCESS;
}
#endif

bt_sink_srv_am_result_t am_dynamic_change_channel(audio_channel_selection_t change_channel)
{
    if(change_channel < AUDIO_CHANNEL_SELECTION_NUM){
        audio_src_srv_report("[AMI] am_dynamic_change (%d)channel", 1, change_channel);
    }else {
        audio_src_srv_report("[AMI] am_dynamic_change_channel_error", 0);
        return AUD_EXECUTION_FAIL;
    }
    bt_sink_srv_am_feature_t feature_param;
    feature_param.type_mask = AM_DYNAMIC_CHANGE_DSP_SETTING;
    feature_param.feature_param.channel = change_channel;

    am_audio_set_feature(FEATURE_NO_NEED_ID,&feature_param);
    return AUD_EXECUTION_SUCCESS;
}

void am_hfp_ndvc_sent_avc_vol(bt_sink_avc_sync_statys_t sync_status)
{
    uint32_t avc_vol;
    if(sync_status == AVC_INIT_SYNC) {
        avc_vol = 8192;
    } else if (sync_status == AVC_UPDATE_SYNC) {
        avc_vol = hal_audio_dsp2mcu_data_get();
    } else {
		avc_vol = 8192;
	}
#ifdef MTK_AWS_MCE_ENABLE
    bt_aws_mce_report_info_t info;
    info.module_id = BT_AWS_MCE_REPORT_MODULE_HFP_AVC;
    info.param_len = sizeof(uint32_t);
    info.param = &avc_vol;
    bt_aws_mce_report_send_event(&info);
    audio_src_srv_report("[NDVC] Agent sent IF pkt, avc_vol: %d", 1, avc_vol);
#endif

    // Sent avc_vol back to agent to update agent's volumne
    bt_sink_srv_am_feature_t feature_param;
    feature_param.type_mask = AM_HFP_AVC;
    feature_param.feature_param.avc_vol = avc_vol;
    am_audio_set_feature_ISR(FEATURE_NO_NEED_ID, &feature_param);
    //audio_src_srv_report("[NDVC] Agent sent IF pkt done", 0);
}

#ifdef MTK_AWS_MCE_ENABLE
void am_hfp_ndvc_callback(bt_aws_mce_report_info_t *para)
{
    if(para != NULL) {
        uint32_t* avc_vol = para->param;
        bt_sink_srv_am_feature_t feature_param;
        feature_param.type_mask = AM_HFP_AVC;
        feature_param.feature_param.avc_vol = *avc_vol;
        audio_src_srv_report("[NDVC] Partner receive IF pkt, avc_vol: %d", 1, avc_vol);
        am_audio_set_feature_ISR(FEATURE_NO_NEED_ID, &feature_param);
    } else {
        audio_src_srv_report("[am_hfp_ndvc_receive_callback para] if NULL", 0);
    }
}
#endif

uint32_t * get_audio_dump_info_from_nvdm(void)
{
    DSP_PARA_DATADUMP_STRU *prDumpInfo = NULL;
    uint8_t status;
    uint32_t tableSize = 0;
    static uint32_t mask[2];

    status = nvkey_data_item_length(NVKEYID_DSP_FW_PARA_DATADUMP, &tableSize);

    if(status || !tableSize){
        audio_src_srv_report("[Sink][Setting]get_audio_dump_info_from_nvdm query length Fail. Status:%d Len:%d\n",2, 2, status, tableSize);
        return 0;
    }

    prDumpInfo = (DSP_PARA_DATADUMP_STRU *)pvPortMalloc(tableSize);

    status = nvkey_read_data(NVKEYID_DSP_FW_PARA_DATADUMP, (uint8_t *)prDumpInfo, &tableSize);


    if(status || !prDumpInfo){
        audio_src_srv_report("[Sink][Setting]get_audio_dump_info_from_nvdm read nvdm Fail. Status:%d prDumpInfo:0x%x\n",2, 2, status, prDumpInfo);
        if(prDumpInfo){
            vPortFree(prDumpInfo);
        }
        return 0;
    }

    mask[0] = prDumpInfo->Dump_Mask_0;
    mask[1] = prDumpInfo->Dump_Mask_1;

    vPortFree(prDumpInfo);

    return mask;
}

void am_set_audio_output_volume_parameters_to_nvdm(ami_audio_volume_parameters_nvdm_t *volume_param_ptr)
{
    sysram_status_t ret;
    ret = flash_memory_write_nvdm_data(NVKEY_DSP_PARA_VOLUME_PARAMETERS, (uint8_t *)volume_param_ptr, sizeof(ami_audio_volume_parameters_nvdm_t));
    if(ret){
        audio_src_srv_report("[Sink][Setting]set_audio_output_volume_parameters_to_nvdm nvdm Fail. Ret:%d \n", 1, ret);
    }
}


void am_load_audio_output_volume_parameters_from_nvdm(void)
{
    sysram_status_t ret;
    uint32_t mask[2];
    uint32_t length = sizeof(ami_audio_volume_parameters_nvdm_t);

    /*
    Ratio |    db    | Compensation
    10%   |  -20db   | 0xF830
    20%   | -13.98db | 0xFA8B
    30%   | -10.46db | 0xFBEB
    40%   |  -7.96db | 0xFCE5
    50%   |  -6.02db | 0xFDA6
    60%   |  -4.44db | 0xFE45
    70%   |  -3.1db  | 0xFECB
    80%   |  -1.94db | 0xFF3F
    90%   |  -0.92db | 0xFFA5
    100%  |     0db  | 0
    */

    ret = flash_memory_read_nvdm_data(NVKEY_DSP_PARA_VOLUME_PARAMETERS, (uint8_t *)mask, &length);
    if(ret){
        audio_src_srv_report("[Sink][Setting]load_audio_output_volume_parameters Fail. Ret:%d \n", 1, ret);
    }
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_SET_OUTPUT_VOLUME_PARAMETERS, (uint16_t)mask[1], mask[0], true);
}


bt_sink_srv_am_result_t ami_hal_set_mutex_lock(void)
{
    bt_sink_srv_am_result_t status = AUD_EXECUTION_SUCCESS;

    if ( g_ami_hal_semaphore_handle != NULL ) {
        audio_src_srv_report("[AMI] ami_hal_mutex_lock() +\r\n", 0);
        xSemaphoreTake(g_ami_hal_semaphore_handle, portMAX_DELAY);
    }
    else {
        status = AUD_EXECUTION_FAIL;
    }

    return status;
}
bt_sink_srv_am_result_t ami_hal_set_mutex_unlock(void)
{
    bt_sink_srv_am_result_t status = AUD_EXECUTION_SUCCESS;

    if ( g_ami_hal_semaphore_handle != NULL ) {
        xSemaphoreGive(g_ami_hal_semaphore_handle);
        audio_src_srv_report("[AMI] ami_hal_mutex_lock() -\r\n", 0);
    }
    else {
        status = AUD_EXECUTION_FAIL;
    }

    return status;
}

/*****************************************************************************
 * FUNCTION
 *  ami_hal_audio_status_set_running_flag
 * DESCRIPTION
 *  This function is used to protect hal_audio set running flag.
 * PARAMETERS
 *  type               [IN]
 *  is_running       [IN]
 * RETURNS
 *  void
 *****************************************************************************/
void ami_hal_audio_status_set_running_flag(audio_message_type_t type, bool is_running)
{
    if ( g_ami_hal_semaphore_handle == NULL ) {
       g_ami_hal_semaphore_handle = xSemaphoreCreateMutex();
    }
    ami_hal_set_mutex_lock();
    hal_audio_status_set_running_flag(type, is_running);
    ami_hal_set_mutex_unlock();
}

bool ami_hal_audio_status_query_running_flag(audio_message_type_t type)
{
    bool query_status;
    if ( g_ami_hal_semaphore_handle == NULL ) {
       g_ami_hal_semaphore_handle = xSemaphoreCreateMutex();
    }
    ami_hal_set_mutex_lock();
    query_status = hal_audio_status_query_running_flag(type);
    ami_hal_set_mutex_unlock();
    return query_status;
}

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_ami_send_amm
 * DESCRIPTION
 *  This function is used to send audio manager message.
 * PARAMETERS
 *  dest_id          [IN]
 *  src_id           [IN]
 *  cb_msg_id        [IN]
 *  msg_id           [IN]
 *  background_info  [IN]
 * RETURNS
 *  void
 *****************************************************************************/
void bt_sink_srv_ami_send_amm(bt_sink_srv_am_module_t dest_id,
                              bt_sink_srv_am_module_t src_id,
                              bt_sink_srv_am_cb_msg_class_t cb_msg_id,
                              bt_sink_srv_am_msg_id_t msg_id,
                              bt_sink_srv_am_background_t *background_info,
                              uint8_t fromISR,
                              bt_sink_srv_am_amm_struct *pr_Amm)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
#ifndef WIN32_UT
    bt_sink_srv_am_amm_struct *g_prAmm_current = NULL;
#endif
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if (HAL_NVIC_QUERY_EXCEPTION_NUMBER == HAL_NVIC_NOT_EXCEPTION) {
#ifndef WIN32_UT
        g_prAmm_current = (bt_sink_srv_am_amm_struct *)pvPortMalloc(sizeof(bt_sink_srv_am_amm_struct));
#else
        g_prAmm_current = (bt_sink_srv_am_amm_struct *)malloc(sizeof(bt_sink_srv_am_amm_struct));
#endif

        if (g_prAmm_current == NULL) {
            audio_src_srv_report("[AudM]error. Allocate fail", 0);
            return;
        }

        g_prAmm_current->cb_msg_id = cb_msg_id;
        g_prAmm_current->src_mod_id = src_id;
        g_prAmm_current->dest_mod_id = dest_id;
        g_prAmm_current->msg_id = msg_id;
        if (background_info) {
            memcpy(&(g_prAmm_current->background_info), background_info, sizeof(bt_sink_srv_am_background_t));
        }

#ifndef WIN32_UT
        if (g_xQueue_am != 0) {
            /*Do NOT AM Task send AM queue in this path.*/
            bool is_am_task = false;
            uint32_t savedmask;
            hal_nvic_save_and_set_interrupt_mask(&savedmask);
            if(AUDIO_SRC_SRV_AM_TASK == pxCurrentTCB){
                is_am_task = true;
            } else {
                is_am_task = false;
            }
            hal_nvic_restore_interrupt_mask(savedmask);
            if(is_am_task){
                audio_src_srv_report("[AudM]warning. ============== AM send own queue has risk ==============", 0);
                //audio_src_srv_report("[AudM]error. AM send own queue.", 0);
                //configASSERT(0);
                xQueueSend(g_xQueue_am, (void *) &g_prAmm_current, portMAX_DELAY);
            } else {
                xQueueSend(g_xQueue_am, (void *) &g_prAmm_current, portMAX_DELAY);
            }
        } else {
            audio_src_srv_report("AudM]error. AM send queue fail, AM queue not create yet. Dst_id:%d,Src_id:%d,CB_msg_id:%d,Msg_ID:%d.",
                4, dest_id, src_id, cb_msg_id, msg_id);
        }
#endif
    } else {
        pr_Amm->cb_msg_id = cb_msg_id;
        pr_Amm->src_mod_id = src_id;
        pr_Amm->dest_mod_id = dest_id;
        pr_Amm->msg_id = msg_id;
        if (background_info) {
            memcpy(&(pr_Amm->background_info), background_info, sizeof(bt_sink_srv_am_background_t));
        }

#ifndef WIN32_UT
        if (g_xQueue_am != 0) {
            if(pdFALSE == xQueueIsQueueFullFromISR(g_xQueue_am)){
                xQueueSendFromISR(g_xQueue_am, (void *) &pr_Amm, &xHigherPriorityTaskWoken);
            }else{
                audio_src_srv_report("[AudM]Send queue error. Queue full (Drop:msg_id (%d))", 1, pr_Amm->msg_id);
            }
        } else {
            audio_src_srv_report("AudM]error. AM send queue from ISR fail, AM queue not create yet. Dst_id:%d,Src_id:%d,CB_msg_id:%d,Msg_ID:%d.",
                4, dest_id, src_id, cb_msg_id, msg_id);
        }
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
#endif
    }
}

#if STANDALONE_TEST
/* Hal API func */
hal_audio_status_t hal_audio_get_memory_size(uint32_t *memory_size)
{
    //*memory_size = 16<<10;
    return HAL_AUDIO_STATUS_OK;
}

hal_audio_status_t hal_audio_set_memory(uint16_t *memory)
{
    return HAL_AUDIO_STATUS_OK;
}
#endif

#ifdef MTK_AWS_MCE_ENABLE
static bt_aws_mce_report_module_id_t ami_event_to_report_module_id(ami_event_t ami_event)
{
    bt_aws_mce_report_module_id_t module_id;
    switch (ami_event) {
        case AMI_EVENT_PEQ_REALTIME:
        case AMI_EVENT_PEQ_CHANGE_MODE:
        case AMI_EVENT_ATTACH_INFO:
        case AMI_EVENT_ATTACH_NVDM_INFO:
            module_id = BT_AWS_MCE_REPORT_MODULE_PEQ;
            break;
        default:
            module_id = -1;
            break;
    }
    return module_id;
}

static void aws_mce_ami_send_attch_packet(void)  //Ryan: ANC
{
#if defined(MTK_ANC_ENABLE) || defined(MTK_PEQ_ENABLE)
    AMI_AWS_MCE_ATTACH_PACKT_t attach_packet;
    memset(&attach_packet, 0, sizeof(AMI_AWS_MCE_ATTACH_PACKT_t));
#ifdef MTK_ANC_ENABLE
    uint8_t anc_enable, attach_enable;
    uint32_t runtime_info;
#ifdef MTK_ANC_V2
    attach_enable = audio_anc_get_attach_enable();
#else
    anc_get_attach_enable(&attach_enable);
#endif
    if (attach_enable == 1) {
#ifdef MTK_ANC_V2
        anc_enable = 0; //TODO
#else
        anc_get_status(NULL, &runtime_info, NULL);
        anc_get_backup_status(&anc_enable);
#endif
        attach_packet.anc_enable = anc_enable;
        attach_packet.anc_runtime_gain = (int16_t)(runtime_info >> 16);
        attach_packet.anc_filter_type = (uint8_t)(runtime_info & 0xF);
    } else {
        attach_packet.anc_enable = 0xFF;
    }
    audio_src_srv_report("[mce_ami] send attach info: anc:%d %d %d\n", 3, attach_packet.anc_enable, attach_packet.anc_filter_type, attach_packet.anc_runtime_gain);
#endif
#ifdef MTK_PEQ_ENABLE
    if(aud_peq_get_sound_mode(A2DP, &attach_packet.a2dp_pre_peq_enable) != 0) {
        audio_src_srv_err("[mce_ami] send attach info: get peq sound mode error !!!\n", 0);
    }
    audio_src_srv_report("[mce_ami] send attach info: peq: %d %d\n", 4, attach_packet.a2dp_pre_peq_enable, attach_packet.a2dp_pre_peq_sound_mode, attach_packet.a2dp_post_peq_enable, attach_packet.a2dp_post_peq_sound_mode);
#endif
    bt_sink_srv_aws_mce_ami_data(AMI_EVENT_ATTACH_INFO, (uint8_t *)&attach_packet, sizeof(AMI_AWS_MCE_ATTACH_PACKT_t), false, 0);
#endif
}

#ifdef SUPPORT_PEQ_NVKEY_UPDATE
static void aws_mce_ami_send_attch_nvdm_packet(void)
{
    uint32_t total_size;
    uint8_t *packet = NULL;
    aud_peq_get_changed_nvkey(&packet, &total_size);
    if ((packet != NULL) && (total_size > 0)) {
        bt_sink_srv_aws_mce_ami_data(AMI_EVENT_ATTACH_NVDM_INFO, (uint8_t *)packet, total_size, false, 0);
    }
    if (packet != NULL) {
        vPortFree(packet);
    }
}
#endif


/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_aws_mce_ami_state_change_handler
 * DESCRIPTION
 *  For BT_SINK_SRV_MODULE_INTERNAL_AMI, to be notified when receives AWS MCE state change.
 * PARAMETERS
 *  state_ind     [IN]
 * RETURNS
 *****************************************************************************/
static void bt_sink_srv_aws_mce_ami_state_change_handler(bt_aws_mce_state_change_ind_t *state_ind)  //Ryan: Partner Later join.
{
    bt_aws_mce_role_t role = bt_connection_manager_device_local_info_get_aws_role();
    bt_bd_addr_t *local_addr = bt_connection_manager_device_local_info_get_local_address();
    uint32_t aws_handle = bt_sink_srv_aws_mce_get_handle(local_addr);

    audio_src_srv_report("[mce_ami]state_change(s)-aws_hd: 0x%x, state 0x%0x", 2, state_ind->handle, state_ind->state);

    if(state_ind->handle == aws_handle) {
        audio_src_srv_report("[mce_ami]It is same with special link handle", 0);
        return;
    }

    if (role == BT_AWS_MCE_ROLE_AGENT) {
        if (state_ind->state == BT_AWS_MCE_AGENT_STATE_ATTACHED) {
#ifdef SUPPORT_PEQ_NVKEY_UPDATE
            aws_mce_ami_send_attch_nvdm_packet();
#endif
            aws_mce_ami_send_attch_packet();
            am_hfp_ndvc_sent_avc_vol(AVC_INIT_SYNC);
            audio_src_srv_report("[mce_ami] Partner is attached !", 0);
        } else if(state_ind->state == BT_AWS_MCE_AGENT_STATE_INACTIVE) { // disconnect
            audio_src_srv_report("[mce_ami] Agent is inactive", 0);
        } else {
            audio_src_srv_report("[mce_ami] Please handle deattach precedure", 0);
        }
    } else {
        audio_src_srv_report("It is not on agent", 0);
    }
}

static void aws_mce_ami_receive_attch_packet(AMI_AWS_MCE_ATTACH_PACKT_t *attach_packet)
{
#ifdef MTK_ANC_ENABLE
#ifndef MTK_ANC_V2
{
    uint32_t anc_filter_type;
    uint32_t anc_runtime_gain;
    audio_src_srv_report("[mce_ami] receive attach info: anc: %d %d %d\n", 3, attach_packet->anc_enable, attach_packet->anc_filter_type, attach_packet->anc_runtime_gain);

    if (audio_anc_backup_while_suspend(attach_packet->anc_enable, attach_packet->anc_filter_type, attach_packet->anc_runtime_gain) == ANC_CONTROL_EXECUTION_SUCCESS) {
        if (attach_packet->anc_enable == 1) {
            anc_filter_type = attach_packet->anc_filter_type;
            anc_runtime_gain = (uint32_t)attach_packet->anc_runtime_gain;
        //audio_anc_control(ANC_CONTROL_EVENT_ON, (anc_filter_type<<16)|(anc_runtime_gain&0xFFFF), NULL);
    } else {
            anc_runtime_gain = (uint32_t)attach_packet->anc_runtime_gain;
        //audio_anc_control(ANC_CONTROL_EVENT_OFF, 0, NULL);
        //audio_anc_control(ANC_CONTROL_EVENT_SET_RUNTIME_VOLUME, anc_runtime_gain, NULL);
        }
    }
}
#endif
#endif
#ifdef MTK_PEQ_ENABLE
{
    bt_sink_srv_am_feature_t am_feature;
    audio_src_srv_report("[mce_ami] receive attach info: peq: %d %d %d %d \n", 4, attach_packet->a2dp_pre_peq_enable, attach_packet->a2dp_pre_peq_sound_mode, attach_packet->a2dp_post_peq_enable, attach_packet->a2dp_post_peq_sound_mode);
    memset(&am_feature, 0, sizeof(bt_sink_srv_am_feature_t));
    am_feature.type_mask                             = AM_A2DP_PEQ;
    am_feature.feature_param.peq_param.enable        = attach_packet->a2dp_pre_peq_enable;
    am_feature.feature_param.peq_param.sound_mode    = attach_packet->a2dp_pre_peq_sound_mode;
    am_feature.feature_param.peq_param.setting_mode  = PEQ_DIRECT;
    am_feature.feature_param.peq_param.not_clear_sysram = 1;
    am_feature.feature_param.peq_param.phase_id      = 0;
    am_audio_set_feature(FEATURE_NO_NEED_ID, &am_feature);
    am_feature.feature_param.peq_param.enable        = attach_packet->a2dp_post_peq_enable;
    am_feature.feature_param.peq_param.sound_mode    = attach_packet->a2dp_post_peq_sound_mode;
    am_feature.feature_param.peq_param.phase_id      = 1;
    am_audio_set_feature(FEATURE_NO_NEED_ID, &am_feature);
}
#endif
}

#ifdef SUPPORT_PEQ_NVKEY_UPDATE
static void aws_mce_ami_receive_attch_nvdm_packet(bt_aws_mce_report_info_t *info)
{
    AMI_AWS_MCE_PACKET_HDR_t *ami_packet_hdr = (AMI_AWS_MCE_PACKET_HDR_t *)info->param;
    AMI_AWS_MCE_ATTACH_NVDM_PACKT_t *attach_nvdm_packet = (AMI_AWS_MCE_ATTACH_NVDM_PACKT_t *)((uint8_t *)info->param + sizeof(AMI_AWS_MCE_PACKET_HDR_t));
    if (ami_packet_hdr->SubPktId == 0) {
        if (ami_attach_nvdm_ctrl.buffer != NULL) {
            vPortFree(ami_attach_nvdm_ctrl.buffer);
        }
        ami_attach_nvdm_ctrl.buffer = pvPortMalloc(ami_packet_hdr->numSubPkt * BT_AWS_MCE_MAX_DATA_LENGTH);
        ami_attach_nvdm_ctrl.buffer_offset = 0;
        ami_attach_nvdm_ctrl.total_pkt = ami_packet_hdr->numSubPkt;
        ami_attach_nvdm_ctrl.pre_pkt = -1;
    }

    if((ami_packet_hdr->SubPktId == (ami_attach_nvdm_ctrl.pre_pkt + 1)) && (ami_packet_hdr->numSubPkt == ami_attach_nvdm_ctrl.total_pkt) && (ami_attach_nvdm_ctrl.buffer != NULL))
    {
        uint16_t temp_size = info->param_len - sizeof(AMI_AWS_MCE_PACKET_HDR_t);
        memcpy(ami_attach_nvdm_ctrl.buffer + ami_attach_nvdm_ctrl.buffer_offset, (uint8_t *)attach_nvdm_packet, temp_size);
        ami_attach_nvdm_ctrl.buffer_offset += temp_size;
        ami_attach_nvdm_ctrl.pre_pkt = ami_packet_hdr->SubPktId;
        audio_src_srv_report("[mce_ami] receive attach nvdm, sub_pkt_id:%d/%d size:%d",3,ami_packet_hdr->SubPktId,ami_packet_hdr->numSubPkt,temp_size);
    }
    else
    {
        audio_src_srv_err("[mce_ami] packet header is wrong, (%d/%d), (%d/%d), buffer:0x%08x offset:%d\n",6, ami_packet_hdr->SubPktId, ami_packet_hdr->numSubPkt, ami_attach_nvdm_ctrl.pre_pkt, ami_attach_nvdm_ctrl.total_pkt, ami_attach_nvdm_ctrl.buffer, ami_attach_nvdm_ctrl.buffer_offset);
        return;
    }

    if(((ami_attach_nvdm_ctrl.pre_pkt + 1) == ami_attach_nvdm_ctrl.total_pkt) && (ami_attach_nvdm_ctrl.buffer != NULL))/*means last pkt*/
    {
        uint32_t total_size;
        uint8_t *packet;
        uint32_t have_changed;
        bt_aws_mce_role_t role = bt_connection_manager_device_local_info_get_aws_role();
        attach_nvdm_packet = (AMI_AWS_MCE_ATTACH_NVDM_PACKT_t *)ami_attach_nvdm_ctrl.buffer;
        have_changed = aud_peq_save_changed_nvkey(attach_nvdm_packet, (uint32_t)ami_attach_nvdm_ctrl.buffer_offset);
        vPortFree(ami_attach_nvdm_ctrl.buffer);
        ami_attach_nvdm_ctrl.buffer = NULL;
        ami_attach_nvdm_ctrl.buffer_offset = 0;
        ami_attach_nvdm_ctrl.total_pkt = 0;
        ami_attach_nvdm_ctrl.pre_pkt = 0;

        if (role == BT_AWS_MCE_ROLE_PARTNER) {
            aud_peq_get_changed_nvkey(&packet, &total_size);
            if ((packet != NULL) && (total_size > 0)) {
                bt_sink_srv_aws_mce_ami_data(AMI_EVENT_ATTACH_NVDM_INFO, (uint8_t *)packet, total_size, false, 0);
            }
            if (packet != NULL) {
                vPortFree(packet);
            }
        } else if ((role == BT_AWS_MCE_ROLE_AGENT) && (have_changed == 1)) {
            bt_sink_srv_am_feature_t am_feature;
            uint8_t a2dp_peq_status[4];
            aud_peq_get_sound_mode(A2DP, &a2dp_peq_status[0]);
            memset(&am_feature, 0, sizeof(bt_sink_srv_am_feature_t));
            am_feature.type_mask                             = AM_A2DP_PEQ;
            am_feature.feature_param.peq_param.enable        = a2dp_peq_status[0];
            am_feature.feature_param.peq_param.sound_mode    = a2dp_peq_status[1];
            am_feature.feature_param.peq_param.setting_mode  = PEQ_DIRECT;
            am_feature.feature_param.peq_param.not_clear_sysram = 1;
            am_feature.feature_param.peq_param.phase_id      = 0;
            am_audio_set_feature(FEATURE_NO_NEED_ID, &am_feature);
        }
    }
}
#endif

void bt_aws_mce_report_peq_callback(bt_aws_mce_report_info_t *info)  //Ryan: Partner peq
{
    bt_aws_mce_role_t role = bt_connection_manager_device_local_info_get_aws_role();
    //RACE_LOG_MSGID_I("PEQ AWS MCE Report: 0x%x 0x%x 0x%x 0x%x 0x%x\n",5,info->module_id,info->is_sync,info->sync_time,info->param_len,info->param);
    if ((info->module_id == BT_AWS_MCE_REPORT_MODULE_PEQ) && (info->param != NULL)) {
        AMI_AWS_MCE_PACKET_HDR_t *ami_pkt_header = (AMI_AWS_MCE_PACKET_HDR_t*)info->param;
        if ((ami_pkt_header->ami_event == AMI_EVENT_PEQ_REALTIME) || (ami_pkt_header->ami_event == AMI_EVENT_PEQ_CHANGE_MODE)) {
            #ifdef MTK_PEQ_ENABLE
            if (role != BT_AWS_MCE_ROLE_AGENT) {
                race_dsprt_peq_collect_data(info);
            }
            #endif
        } else if (ami_pkt_header->ami_event == AMI_EVENT_ATTACH_INFO) {
            if (role != BT_AWS_MCE_ROLE_AGENT) {
                if(info->param_len == sizeof(AMI_AWS_MCE_PACKET_HDR_t) + sizeof(AMI_AWS_MCE_ATTACH_PACKT_t)) {
                    aws_mce_ami_receive_attch_packet((AMI_AWS_MCE_ATTACH_PACKT_t *)((uint8_t *)info->param + sizeof(AMI_AWS_MCE_PACKET_HDR_t)));
                }
            }
        } else if (ami_pkt_header->ami_event == AMI_EVENT_ATTACH_NVDM_INFO) {
            #ifdef SUPPORT_PEQ_NVKEY_UPDATE
            aws_mce_ami_receive_attch_nvdm_packet(info);
            #endif
        }
    }
}
#endif

/*****************************************************************************
 * FUNCTION
 *  bt_sink_srv_aws_mce_ami_data
 * DESCRIPTION
 *  For BT_SINK_SRV_MODULE_INTERNAL_AMI, to fragment data to multiple IF packet if data size is larger than 200 bytes, and send with bt_sink_srv_send_aws_mce_packet(.).
 * PARAMETERS
 *  ami_event     [IN]
 *  buffer           [IN]
 *  length          [IN]
 * RETURNS
 *  bt_sink_srv_am_result_t
 *****************************************************************************/
bt_sink_srv_am_result_t bt_sink_srv_aws_mce_ami_data(ami_event_t ami_event, uint8_t *buffer, uint32_t length, bool is_sync, uint32_t sync_time) //Ryan: IF Patcket.
{
#ifdef MTK_AWS_MCE_ENABLE
    bt_aws_mce_report_info_t info;
    AMI_AWS_MCE_PACKET_HDR_t *ami_packet;
    bt_status_t aws_ret;
    uint32_t header_size = sizeof(bt_aws_mce_report_sync_payload_header_t) + sizeof(AMI_AWS_MCE_PACKET_HDR_t);
    uint32_t num_pkt = (length/(BT_AWS_MCE_MAX_DATA_LENGTH-header_size)) + 1;
    uint32_t cur_pkt;
    uint32_t size;

    if ((buffer == NULL) || (length == 0) || (bt_connection_manager_device_local_info_get_aws_role() == BT_AWS_MCE_ROLE_NONE))
    {
        audio_src_srv_report("bt_sink_srv_aws_mce_ami_data invalid param\n", 0);
        return AUD_EXECUTION_FAIL;
    }

    for(cur_pkt = 0; cur_pkt < num_pkt; cur_pkt++)
    {
        size = ((header_size+length) < BT_AWS_MCE_MAX_DATA_LENGTH) ? (header_size+length) : BT_AWS_MCE_MAX_DATA_LENGTH;
        size -= sizeof(bt_aws_mce_report_sync_payload_header_t);

        ami_packet = (AMI_AWS_MCE_PACKET_HDR_t *)pvPortMalloc(size);
        if(!ami_packet) {
            audio_src_srv_report("mce_ami malloc failed\n", 0);
            return AUD_EXECUTION_FAIL;
        }

        ami_packet->ami_event = ami_event;
        ami_packet->numSubPkt = num_pkt;
        ami_packet->SubPktId  = cur_pkt;

        size -= sizeof(AMI_AWS_MCE_PACKET_HDR_t);
        memcpy((uint8_t *)ami_packet + sizeof(AMI_AWS_MCE_PACKET_HDR_t), buffer, size);
        buffer += size;
        length -= size;

        info.module_id = ami_event_to_report_module_id(ami_event);
        info.is_sync = is_sync;
        info.sync_time = sync_time;
        info.param_len = size + sizeof(AMI_AWS_MCE_PACKET_HDR_t);
        info.param = (void *)ami_packet;

        if (is_sync) {
            aws_ret = bt_aws_mce_report_send_sync_event(&info);
        } else {
            aws_ret = bt_aws_mce_report_send_event(&info);
        }

        if(aws_ret != BT_STATUS_SUCCESS) {
            audio_src_srv_report("Send AMI aws mce data FAIL \n", 0);
        } else {
            audio_src_srv_report("Send AMI aws mce data SUCCESS \n", 0);
        }
        race_mem_free(ami_packet);
    }
    return aws_ret;
#else
    return AUD_EXECUTION_SUCCESS;
#endif
}

/*****************************************************************************
 * FUNCTION
 *  bt_event_ami_callback
 * DESCRIPTION
 *  To receive MODULE_MASK_AWS_MCE event.
 * PARAMETERS
 *  msg             [IN]
 *  status          [IN]
 *  buffer          [IN]
 * RETURNS
 *  bt_status_t
 *****************************************************************************/
bt_status_t bt_event_ami_callback(bt_msg_type_t msg, bt_status_t status, void *buffer)
{
#ifdef MTK_AWS_MCE_ENABLE
    int32_t ret = BT_STATUS_SUCCESS;

    switch (msg) {
        case  BT_AWS_MCE_STATE_CHANGED_IND: {
            bt_aws_mce_state_change_ind_t *state_change = (bt_aws_mce_state_change_ind_t *)buffer;
            bt_sink_srv_aws_mce_ami_state_change_handler(state_change);
            break;
        }
        default:
            break;
    }

    return ret;
#else
    return 0;
#endif
}

bt_sink_srv_am_result_t ami_audio_power_off_flow(void)
{
    /* save status into nvdm */
    #ifdef MTK_ANC_ENABLE
    //anc_save_misc_param();
    #endif
    #if defined(MTK_PEQ_ENABLE) || defined(MTK_LINEIN_PEQ_ENABLE)
    aud_peq_save_misc_param();
    #endif

    return AUD_EXECUTION_SUCCESS;
}

#ifdef MTK_VENDOR_VOLUME_TABLE_ENABLE

#include "audio_src_srv.h"

#define MAX_MODE_NUM  16

typedef struct {
    uint32_t (*p_table)[2];
    uint32_t table_num;
    bool is_mode_set;
} vendor_volume_table_t;

vendor_volume_table_t s_vendor_volume_table[VOL_TOTAL][MAX_MODE_NUM];
uint32_t s_vendor_volume_mode[VOL_TOTAL];

bt_sink_srv_am_result_t ami_register_volume_table(vol_type_t type, int mode, const uint32_t table[][2], size_t table_num)
{
    if((type >= VOL_TOTAL) || (mode >= MAX_MODE_NUM)) {
        audio_src_srv_report("[VENDOR_VOLUME]error type or mode, type = %d, mode = %d  \n", 2, type, mode);
        return AUD_EXECUTION_FAIL;
    }
    audio_src_srv_report("[VENDOR_VOLUME]ami_register_volume_table: type = %d, mode = %d, table = %x, table_num = %d  \n", 4, type, mode, *table, table_num);
    s_vendor_volume_table[type][mode].p_table = table;
    s_vendor_volume_table[type][mode].table_num = table_num;
    s_vendor_volume_table[type][mode].is_mode_set = true;

    return AUD_EXECUTION_SUCCESS;
}

bt_sink_srv_am_result_t ami_switch_volume_table(vol_type_t type, int mode)
{
    if((type >= VOL_TOTAL) || (mode >= MAX_MODE_NUM)) {
        audio_src_srv_report("[VENDOR_VOLUME]error type or mode, type = %d, mode = %d  \n", 2, type, mode);
        return AUD_EXECUTION_FAIL;
    }
    if(false == s_vendor_volume_table[type][mode].is_mode_set)
    {
        audio_src_srv_report("[VENDOR_VOLUME]mode not registerd, type = %d, mode = %d  \n", 2, type, mode);
        return AUD_EXECUTION_FAIL;
    }
    audio_src_srv_report("[VENDOR_VOLUME]ami_switch_volume_table: type = %d, mode = %d  \n", 2, type, mode);
    s_vendor_volume_mode[type]  = mode;

    const audio_src_srv_handle_t *running_handle = audio_src_srv_get_runing_pseudo_device();
    if(running_handle != NULL) {
        if((running_handle->type == AUDIO_SRC_SRV_PSEUDO_DEVICE_A2DP) || (running_handle->type == AUDIO_SRC_SRV_PSEUDO_DEVICE_HFP))
        {
            audio_src_srv_report("[VENDOR_VOLUME]current running device is: %d", 1, running_handle->type);
            //set volume directly if current running device is A2DP or HFP

            bt_sink_srv_am_id_t aud_id = g_prCurrent_player->aud_id;

            bt_sink_srv_am_volume_level_t volume_level_stream_out = g_prCurrent_player->audio_stream_out.audio_volume;
            bt_sink_srv_am_volume_level_t volume_level_stream_in = g_prCurrent_player->audio_stream_in.audio_volume;

            bt_sink_srv_ami_audio_set_volume(aud_id, volume_level_stream_out, STREAM_OUT);
            if (running_handle->type == AUDIO_SRC_SRV_PSEUDO_DEVICE_HFP) {
                bt_sink_srv_ami_audio_set_volume(aud_id, volume_level_stream_in, STREAM_IN);
            }
        }
    }

    return AUD_EXECUTION_SUCCESS;
}

bt_sink_srv_am_result_t ami_get_stream_out_volume(vol_type_t type, uint32_t level, uint32_t *digital_gain, uint32_t *analog_gain)
{
    if(type >= VOL_TOTAL) {
        audio_src_srv_report("[VENDOR_VOLUME]error type, type = %d  \n", 1, type);
        return AUD_EXECUTION_FAIL;
    }
    uint32_t (*p_table)[2] = s_vendor_volume_table[type][s_vendor_volume_mode[type]].p_table;

    if(p_table == NULL) {
        audio_src_srv_report("[VENDOR_VOLUME]no volume table is switched before use, type = %d  \n", 1, type);
        return AUD_EXECUTION_FAIL;
    }

    uint32_t total_level =s_vendor_volume_table[type][s_vendor_volume_mode[type]].table_num;

    if(level >= total_level) {
        audio_src_srv_report("[VENDOR_VOLUME]volume level exceed the total level, use the max level,  level = %d, max_level = %d  \n", 2, level, total_level - 1);
        level = total_level -1;
        //return AUD_EXECUTION_FAIL;
    }

    *digital_gain = ((uint32_t *)(p_table + level))[0];
    *analog_gain = ((uint32_t *)(p_table + level))[1];

    audio_src_srv_report("[VENDOR_VOLUME]ami_get_stream_out_volume: type = %d, level = %d, digital_gain = 0x%08x, analog_gain = 0x%08x  \n", 4, type, level, *digital_gain, *analog_gain);

    return AUD_EXECUTION_SUCCESS;
}

#endif

#ifdef MTK_VENDOR_SOUND_EFFECT_ENABLE
#define VENDOR_SE_USER_MAX   2
vendor_se_context_t s_vendor_se_context[VENDOR_SE_USER_MAX] = {
    {false, false, NULL, NULL},
    {false, false, NULL, NULL}
};

am_vendor_se_id_t ami_get_vendor_se_id(void)
{
    uint32_t int_mask;
    hal_nvic_save_and_set_interrupt_mask(&int_mask);

    for(uint32_t i = 0; i < VENDOR_SE_USER_MAX; i++) {
        if(s_vendor_se_context[i].is_used == false) {
            s_vendor_se_context[i].is_used = true;
            s_vendor_se_context[i].is_vendor_se_set = false;
            hal_nvic_restore_interrupt_mask(int_mask);
            return (am_vendor_se_id_t)i;
        }
    }
    hal_nvic_restore_interrupt_mask(int_mask);
    audio_src_srv_report("[VENDOR_SE]ami_get_vendor_se_id: no extra id for use, maxmum user is %d  \n", 1, (int32_t)VENDOR_SE_USER_MAX);
    return (am_vendor_se_id_t)-1;
}

bt_sink_srv_am_result_t ami_register_vendor_se(am_vendor_se_id_t id, am_vendor_se_callback_t callback)
{
    if((id == -1) || (s_vendor_se_context[id].is_used == false)) {
        audio_src_srv_report("[VENDOR_SE]ami_register_vendor_se[%d]: wrong id, please call ami_get_vendor_se_id to get a valid id first   \n", 1, (int32_t)id);
    }
    audio_src_srv_report("[VENDOR_SE]ami_register_vendor_se[%d]: callback = 0x%08x  \n", 2, (int32_t)id, (int32_t)callback);
    s_vendor_se_context[id].am_vendor_se_callback = callback;
    return AUD_EXECUTION_SUCCESS;
}

bt_sink_srv_am_result_t ami_set_vendor_se(am_vendor_se_id_t id, void *arg)
{
    if((id == -1) || (s_vendor_se_context[id].is_used == false)) {
        audio_src_srv_report("[VENDOR_SE]ami_set_vendor_se[%d]: wrong id, please call ami_get_vendor_se_id to get a valid id first   \n", 1, (int32_t)id);
    }
    if(arg == NULL) {
        audio_src_srv_report("[VENDOR_SE]ami_set_vendor_se[%d]: arg is NULL!  \n", 1, (int32_t)id);
        return AUD_EXECUTION_FAIL;
    }
    s_vendor_se_context[id].vendor_se_arg = arg;
    s_vendor_se_context[id].is_vendor_se_set = true;

    audio_src_srv_report("[VENDOR_SE]ami_set_vendor_se[%d]: arg address = 0x%08x,  \n", 2, (int32_t)id, (int32_t)arg);
    if(arg != NULL) {
        audio_src_srv_report("[VENDOR_SE]ami_set_vendor_se[%d]: arg content = 0x%08x,  \n", 2, (int32_t)id, *(uint32_t *)arg);
    }

    bt_sink_srv_ami_send_amm(MOD_AM, MOD_AMI, AUD_SELF_CMD_REQ,
                                 MSG_ID_AUDIO_SET_VENDOR_SE, NULL,
                                 FALSE, NULL);
    return AUD_EXECUTION_SUCCESS;
}

bt_sink_srv_am_result_t ami_execute_vendor_se(vendor_se_event_t event)
{
    audio_src_srv_report("[VENDOR_SE]ami_execute_vendor_se: event = %d  \n", 1, (int32_t)event);

    if(event == EVENT_SET_VENDOREFFECT) {
        for(uint32_t i = 0; i < VENDOR_SE_USER_MAX; i++) {
            if((s_vendor_se_context[i].is_used == true) && (s_vendor_se_context[i].am_vendor_se_callback != NULL) && (s_vendor_se_context[i].is_vendor_se_set == true)) {
                audio_src_srv_report("[VENDOR_SE]ami_execute_vendor_se: id = %d  \n", 1, (int32_t)i);
                s_vendor_se_context[i].am_vendor_se_callback(EVENT_SET_VENDOREFFECT, s_vendor_se_context[i].vendor_se_arg);
                s_vendor_se_context[i].is_vendor_se_set = false;
            }
        }
    } else {
        for(uint32_t i = 0; i < VENDOR_SE_USER_MAX; i++) {
            if((s_vendor_se_context[i].is_used == true) && (s_vendor_se_context[i].am_vendor_se_callback != NULL)) {
                audio_src_srv_report("[VENDOR_SE]ami_execute_vendor_se: id = %d  \n", 1, (int32_t)i);
                s_vendor_se_context[i].am_vendor_se_callback(event, NULL);
            }
        }
    }
    return AUD_EXECUTION_SUCCESS;
}

#endif

