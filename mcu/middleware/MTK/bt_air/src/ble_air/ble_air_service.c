/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
/*****************************************************************************
 *
 *
 * Description:
 * ------------
 * This file implements BLE Air service main function
 *
 ****************************************************************************/
 
#ifdef MTK_PORT_SERVICE_BT_ENABLE
#include <stdint.h>
#include "bt_gatts.h"
#include "bt_gattc.h"
#include "ble_air_interface.h"
#include "bt_gap_le.h"
#include "bt_uuid.h"
#include "bt_type.h"
#include "bt_callback_manager.h"
#include "ble_air_internal.h"

#ifndef WIN32
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#endif
#include "syslog.h"

#if 0
#ifdef BLE_AIR_LOW_POWER_CONTROL
#include "timers.h"
TimerHandle_t g_xTimer_low_power = NULL;
extern TimerHandle_t ble_air_create_timer(void);
#endif
#endif
log_create_module(AIR, PRINT_LEVEL_INFO);

#define ble_air_hex_dump(_message,...)   LOG_HEXDUMP_I(AIR, (_message), ##__VA_ARGS__)

#define AIR_SERVICE_UUID                \
    {{0x45, 0x4C, 0x42, 0x61, 0x68, 0x6F, 0x72, 0x69,               \
     0x41, 0x03, 0xAB, 0x2D, 0x4D, 0x49, 0x52, 0x50}}


#define AIR_RX_CHAR_UUID                \
    {{0x45, 0x4C, 0x42, 0x61, 0x68, 0x6F, 0x72, 0x69,               \
     0x41, 0x32, 0xAB, 0x2D, 0x52, 0x41, 0x48, 0x43}}



#define AIR_TX_CHAR_UUID                \
    {{0x45, 0x4C, 0x42, 0x61, 0x68, 0x6F, 0x72, 0x69,               \
     0x41, 0x31, 0xAB, 0x2D, 0x52, 0x41, 0x48, 0x43}}


/************************************************
*   Global
*************************************************/ 

const bt_uuid_t AIR_RX_CHAR_UUID128 = {
    {0x45, 0x4C, 0x42, 0x61, 0x68, 0x6F, 0x72, 0x69,               \
     0x41, 0x32, 0xAB, 0x2D, 0x52, 0x41, 0x48, 0x43}
};


const bt_uuid_t AIR_TX_CHAR_UUID128 = { 
    {0x45, 0x4C, 0x42, 0x61, 0x68, 0x6F, 0x72, 0x69,               \
     0x41, 0x31, 0xAB, 0x2D, 0x52, 0x41, 0x48, 0x43}
};


typedef struct {
    bool in_use;
    ble_air_common_callback_t callback;
} ble_air_callback_node_t;

ble_air_cntx_t g_air_cntx[BT_CONNECTION_MAX] = {{false, false, 0x00, BT_HANDLE_INVALID, {0}, {0}, 0x0, 0x0}};
static ble_air_callback_node_t ble_air_cb_list[BLE_AIR_SUPPORT_CB_MAX_NUM] = {{0}};

/************************************************
*   static utilities
*************************************************/
static uint32_t ble_air_tx_char_cccd_callback(const uint8_t rw, uint16_t handle, void *data, uint16_t size, uint16_t offset);
static uint32_t ble_air_rx_write_char_callback(const uint8_t rw, uint16_t handle, void *data, uint16_t size, uint16_t offset);
static int32_t ble_air_event_callback(ble_air_event_t event_id, void *param);
static bt_status_t ble_air_check_user(void);
static void ble_air_connection_status_notify(ble_air_common_callback_t callback);
static ble_air_status_t ble_air_cb_register(ble_air_common_callback_t callback);
static ble_air_status_t ble_air_cb_deregister(ble_air_common_callback_t callback);



BT_GATTS_NEW_PRIMARY_SERVICE_128(ble_air_primary_service, AIR_SERVICE_UUID);

BT_GATTS_NEW_CHARC_128(ble_air_rx_char,
    BT_GATT_CHARC_PROP_WRITE_WITHOUT_RSP | BT_GATT_CHARC_PROP_WRITE, AIR_RX_CHAR_VALUE_HANDLE, AIR_RX_CHAR_UUID);

BT_GATTS_NEW_CHARC_VALUE_CALLBACK(ble_air_rx_char_value, AIR_RX_CHAR_UUID128,
                BT_GATTS_REC_PERM_WRITABLE,
                ble_air_rx_write_char_callback);

BT_GATTS_NEW_CHARC_128(ble_air_tx_char,
    BT_GATT_CHARC_PROP_NOTIFY, AIR_TX_CHAR_VALUE_HANDLE, AIR_TX_CHAR_UUID);

BT_GATTS_NEW_CHARC_VALUE_UINT8(ble_air_tx_char_value, AIR_TX_CHAR_UUID128,
                               BT_GATTS_REC_PERM_READABLE, 0);

BT_GATTS_NEW_CLIENT_CHARC_CONFIG(ble_air_tx_client_config,
        BT_GATTS_REC_PERM_READABLE|BT_GATTS_REC_PERM_WRITABLE,
        ble_air_tx_char_cccd_callback);

static const bt_gatts_service_rec_t *ble_air_service_rec[] = {
    (const bt_gatts_service_rec_t*) &ble_air_primary_service,
    (const bt_gatts_service_rec_t*) &ble_air_rx_char,
    (const bt_gatts_service_rec_t*) &ble_air_rx_char_value,
    (const bt_gatts_service_rec_t*) &ble_air_tx_char,
    (const bt_gatts_service_rec_t*) &ble_air_tx_char_value,
    (const bt_gatts_service_rec_t*) &ble_air_tx_client_config,
    };

const bt_gatts_service_t ble_air_service = {
    .starting_handle = 0x0051,
    .ending_handle = 0x0056,
    .required_encryption_key_size = 0,
    .records = ble_air_service_rec
    };


static uint32_t ble_air_mutex;

//MUTEX LOCK
static uint32_t ble_air_create_mutex(void)
{
    return (uint32_t)xSemaphoreCreateRecursiveMutex();
}

static void ble_air_take_mutex(uint32_t mutex_id)
{
    if(xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED) {
        return;
    }
    if (mutex_id == 0) {
        configASSERT(0 && "BLE Air is not initialized.");
    }
        
    xSemaphoreTakeRecursive((SemaphoreHandle_t)mutex_id, portMAX_DELAY);
}

static void ble_air_give_mutex(uint32_t mutex_id)
{
    if(xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED) {
        return;
    }
    xSemaphoreGiveRecursive((SemaphoreHandle_t)mutex_id);
}

#if defined(MTK_AWS_MCE_ENABLE) && defined (BT_ROLE_HANDOVER_WITH_SPP_BLE)
#include "bt_role_handover.h"

typedef struct {
    bool                            is_real_connected; //After ACL link connected and first write request had come
    bool                            need_ready2write;
    uint16_t                        notify_enabled;
    uint16_t                        conn_handle;
    bt_bd_addr_t                    peer_addr; /**< Address information of the remote device. */
} ble_air_rho_cntx_t;

typedef struct {
    uint8_t connected_dev_num;
} ble_air_rho_header_t;

ble_air_rho_header_t air_rho_header = {0};

static bt_status_t ble_air_rho_allowed_cb(const bt_bd_addr_t *addr)
{
    //App need call ble_air_get_rx_available() to check if any data in RX queue, if have, need get all data, then to allow RHO
    return BT_STATUS_SUCCESS;
}

static uint8_t ble_air_rho_get_data_length_cb(const bt_bd_addr_t *addr)
{
    uint8_t i, counter = 0;

    for (i = 0; i < BT_CONNECTION_MAX; i++) {
        if (BT_HANDLE_INVALID != g_air_cntx[i].conn_handle) {
            counter++;
        }
    }        
    air_rho_header.connected_dev_num = counter;
    if (air_rho_header.connected_dev_num) {
        return (sizeof(ble_air_rho_header_t) + (sizeof(ble_air_callback_node_t) * BLE_AIR_SUPPORT_CB_MAX_NUM)
            + (sizeof(ble_air_rho_cntx_t) * air_rho_header.connected_dev_num));
    }
    return 0;
}

static bt_status_t ble_air_rho_get_data_cb(const bt_bd_addr_t *addr, void * data)
{
    configASSERT((NULL != data));

    uint8_t i,j;

    if (air_rho_header.connected_dev_num) {
        uint8_t index = 0;
        ble_air_rho_header_t *rho_head= (ble_air_rho_header_t *)data;        
        rho_head->connected_dev_num = air_rho_header.connected_dev_num;
        ble_air_callback_node_t *rho_cb = (ble_air_callback_node_t *)(rho_head + 1);
        for (i = 0; i < BLE_AIR_SUPPORT_CB_MAX_NUM; i++) {
            ((ble_air_callback_node_t *)(rho_cb + i))->in_use = ble_air_cb_list[i].in_use;
            ((ble_air_callback_node_t *)(rho_cb + i))->callback = ble_air_cb_list[i].callback;
        }
 
        ble_air_rho_cntx_t *rho_cntx = (ble_air_rho_cntx_t *)(rho_cb + BLE_AIR_SUPPORT_CB_MAX_NUM);
        for (j = 0; ((j < BT_CONNECTION_MAX) && (index < air_rho_header.connected_dev_num)); j++) {
            if (BT_HANDLE_INVALID != g_air_cntx[j].conn_handle) {
                ((ble_air_rho_cntx_t *)(rho_cntx + index))->conn_handle = g_air_cntx[j].conn_handle;
                ((ble_air_rho_cntx_t *)(rho_cntx + index))->is_real_connected = g_air_cntx[j].is_real_connected;
                ((ble_air_rho_cntx_t *)(rho_cntx + index))->need_ready2write = g_air_cntx[j].need_ready2write;
                ((ble_air_rho_cntx_t *)(rho_cntx + index))->notify_enabled = g_air_cntx[j].notify_enabled;   
                memcpy(((ble_air_rho_cntx_t *)(rho_cntx + index))->peer_addr, g_air_cntx[j].peer_addr, sizeof(bt_bd_addr_t));
                index++;
            }
        } 
    }
    return BT_STATUS_SUCCESS;
}

static void ble_air_rho_status_cb(const bt_bd_addr_t *addr, bt_aws_mce_role_t role, bt_role_handover_event_t event, bt_status_t status)
{
    uint8_t i, j;
    switch (event) {
        case BT_ROLE_HANDOVER_COMPLETE_IND: {
            if ((BT_AWS_MCE_ROLE_AGENT == role) && (BT_STATUS_SUCCESS == status)) {
                for (i = 0; i < BLE_AIR_SUPPORT_CB_MAX_NUM; i++) {
                    ble_air_cb_list[i].callback = NULL;
                    ble_air_cb_list[i].in_use = false;
                }
                for (j = 0; j < BT_CONNECTION_MAX ; j++) {
                    memset(&(g_air_cntx[j]), 0, sizeof(ble_air_cntx_t));
                    g_air_cntx[j].conn_handle = BT_HANDLE_INVALID;
                }
                air_rho_header.connected_dev_num = 0;
            }
        }
            break;

        default:
            break;
    }
}

static bt_status_t ble_air_rho_update_cb(bt_role_handover_update_info_t *info)
{
    uint8_t i, j;
    if (info && (BT_AWS_MCE_ROLE_PARTNER == info->role)) {
        if ((info->length > 0) && (info->data)) {//copy data to context
            ble_air_rho_header_t *rho_head= (ble_air_rho_header_t *)info->data;        
            ble_air_callback_node_t *rho_cb = (ble_air_callback_node_t *)(rho_head + 1);
            for (i = 0; i < BLE_AIR_SUPPORT_CB_MAX_NUM; i++) {
                ble_air_cb_list[i].in_use = ((ble_air_callback_node_t *)(rho_cb + i))->in_use;
                ble_air_cb_list[i].callback = ((ble_air_callback_node_t *)(rho_cb + i))->callback;
            }
            ble_air_rho_cntx_t *rho_cntx = (ble_air_rho_cntx_t *)(rho_cb + BLE_AIR_SUPPORT_CB_MAX_NUM);
            for (j = 0; j < rho_head->connected_dev_num; j++) {
                g_air_cntx[j].conn_handle = ((ble_air_rho_cntx_t *)(rho_cntx + j))->conn_handle;
                g_air_cntx[j].is_real_connected = ((ble_air_rho_cntx_t *)(rho_cntx + j))->is_real_connected;
                g_air_cntx[j].need_ready2write = ((ble_air_rho_cntx_t *)(rho_cntx + j))->need_ready2write;
                g_air_cntx[j].notify_enabled = ((ble_air_rho_cntx_t *)(rho_cntx + j))->notify_enabled;   
                memcpy(g_air_cntx[j].peer_addr, ((ble_air_rho_cntx_t *)(rho_cntx + j))->peer_addr, sizeof(bt_bd_addr_t));
            } 

        } else {
            //error log
            return BT_STATUS_FAIL;
        }
    }
    return BT_STATUS_SUCCESS;
}

bt_role_handover_callbacks_t ble_air_rho_callbacks = {
    .allowed_cb = ble_air_rho_allowed_cb,/*optional if always allowed*/
    .get_len_cb = ble_air_rho_get_data_length_cb,  /*optional if no RHO data to partner*/
    .get_data_cb = ble_air_rho_get_data_cb,   /*optional if no RHO data to partner*/
    .update_cb = ble_air_rho_update_cb,       /*optional if no RHO data to partner*/
    .status_cb = ble_air_rho_status_cb, /*Mandatory for all users.*/
};

#endif /*__MTK_AWS_MCE_ENABLE__ */



/********************Connection Info**************************/

static bool ble_air_has_real_connected(void)
{
    uint8_t i = 0;
    for (i = 0; i< BT_CONNECTION_MAX; i++) {
        if (g_air_cntx[i].is_real_connected) {
            return true;
        }
    }
    if (i == BT_CONNECTION_MAX) { 
        LOG_MSGID_I(AIR, "ble_air_has_real_connected,no real connected link!\r\n", 0);
    }
    return false;
}

uint16_t ble_air_get_real_connected_handle(void)
{
    uint8_t i = 0;
    for (i = 0; i< BT_CONNECTION_MAX; i++) {
        if ((BT_HANDLE_INVALID != g_air_cntx[i].conn_handle) && 
            (g_air_cntx[i].is_real_connected)) {
            return g_air_cntx[i].conn_handle;
        }
    }
    if (i == BT_CONNECTION_MAX) { 
        LOG_MSGID_I(AIR, "ble_air_get_cntx_by_handle,not connected!\r\n", 0);
    }
    return BT_HANDLE_INVALID;
}

ble_air_cntx_t *ble_air_get_cntx_by_handle(uint16_t conn_handle)
{
    uint8_t i = 0;
    for (i = 0; i< BT_CONNECTION_MAX; i++) {
        if ((conn_handle != BT_HANDLE_INVALID) && (conn_handle == g_air_cntx[i].conn_handle)) {
            return &(g_air_cntx[i]);
        }
    }
    if (i == BT_CONNECTION_MAX) { 
        LOG_MSGID_I(AIR, "ble_air_get_cntx_by_handle,not connected!\r\n", 0);
    }
    return NULL;
}

static bt_status_t ble_air_save_connection_info(void *buff)
{
    uint8_t i;
    bt_status_t status = BT_STATUS_SUCCESS;
    bt_gap_le_connection_ind_t *conn_ind = (bt_gap_le_connection_ind_t *)buff;

    for (i = 0; i< BT_CONNECTION_MAX; i++) {
        /**< first connect, to save connection info. */
        if (BT_HANDLE_INVALID == g_air_cntx[i].conn_handle) {
            g_air_cntx[i].conn_handle = conn_ind->connection_handle;  
            memcpy(g_air_cntx[i].peer_addr, conn_ind->peer_addr.addr, sizeof(conn_ind->peer_addr.addr));
            LOG_MSGID_I(AIR, "connection handle=0x%04x", 1, g_air_cntx[i].conn_handle);
        #ifdef BLE_AIR_LOW_POWER_CONTROL
            g_air_cntx[i].low_power_t.conn_interval= conn_ind->conn_interval;
        #endif 
            break;
        /**< Reconnect. */
        } else if (conn_ind->connection_handle == g_air_cntx[i].conn_handle) {
            LOG_MSGID_I(AIR, "reconnect, connection handle=0x%04x", 1, g_air_cntx[i].conn_handle);
            break;
        }
    }

    if (i == BT_CONNECTION_MAX) {    
        LOG_MSGID_I(AIR, "Reach maximum connection, no empty buffer!\r\n", 0);
        status = BT_STATUS_OUT_OF_MEMORY;
    }
    return status;
}

static bt_status_t ble_air_delete_connection_info(void *buff)
{
    uint8_t i; 
    bt_status_t status = BT_STATUS_SUCCESS;
    bt_hci_evt_disconnect_complete_t *disconn_ind;

    LOG_MSGID_I(AIR, "ble_air_delete_connection_info", 0);

    disconn_ind = (bt_hci_evt_disconnect_complete_t*) buff;
    for (i = 0; i< BT_CONNECTION_MAX ; i++) {
        if (disconn_ind->connection_handle == g_air_cntx[i].conn_handle) {
            memset(&(g_air_cntx[i]), 0, sizeof(ble_air_cntx_t));
            memset(g_air_cntx[i].peer_addr, 0, sizeof(g_air_cntx[i].peer_addr));
            g_air_cntx[i].conn_handle = BT_HANDLE_INVALID;
            break;
        }
    }
    if (i == BT_CONNECTION_MAX) {
        LOG_MSGID_I(AIR, "Can not find the connection info to delete!\r\n", 0);
        status = BT_STATUS_FAIL;
    } 
    return status;
}

static void ble_air_clear_all_connection_info(void)
{
    uint8_t i;
    LOG_MSGID_I(AIR, "ble_air_clear_all_connection_info", 0);

    for (i = 0; i< BT_CONNECTION_MAX ; i++) {   
        memset(&(g_air_cntx[i]), 0, sizeof(ble_air_cntx_t));
        memset(g_air_cntx[i].peer_addr, 0, sizeof(g_air_cntx[i].peer_addr));
        g_air_cntx[i].conn_handle = BT_HANDLE_INVALID;
    }
}

/**********************************************/

/**
 * @brief Function for handling Client Configuration Characteristisc Descriptor's read and write event.
 *
 * @param[in]   rw                    Flag of Read or Write event.
 * @param[in]   handle                Connection handle.
 * @param[in]   size                  Length of the data.
 * @param[in]   *data                 Data buffer.
 * @param[in]   *data                 Data buffer.
 * @param[in]  offset                 Write or Read offset.
 *
 * @return      Real wrote or read length of the data.
 */
static uint32_t ble_air_tx_char_cccd_callback(const uint8_t rw, uint16_t handle, void *data, uint16_t size, uint16_t offset)
{
    LOG_MSGID_I(AIR, "ble_air_tx_char_cccd_callback, rw is %d, size is %d, offset is %d \r\n", 3, rw, size, offset);
    
    ble_air_cntx_t *temp_cntx = ble_air_get_cntx_by_handle(handle);
    //if ((handle != BT_HANDLE_INVALID) && (handle == g_air_cntx.conn_handle)) {    
    if ((handle != BT_HANDLE_INVALID) && (temp_cntx)) {
        /** record for each connection. */                
        if (rw == BT_GATTS_CALLBACK_WRITE) {
            if (size != sizeof(uint16_t)) { //Size check
                return 0;
            }
            
            ble_air_take_mutex(ble_air_mutex);
            //g_air_cntx.notify_enabled = *(uint16_t *)data;
            if ((false == ble_air_has_real_connected()) || ((0 == *(uint16_t *)data) && (true == temp_cntx->is_real_connected))) {
                temp_cntx->notify_enabled = *(uint16_t *)data;
            } else {
                ble_air_give_mutex(ble_air_mutex);
                return 0;
            }
            ble_air_give_mutex(ble_air_mutex);
            if ((0 == ble_air_check_user()) && (BLE_AIR_CCCD_NOTIFICATION == temp_cntx->notify_enabled)) {
                ble_air_ready_to_write_t ready_to_write;
                if (false == temp_cntx->is_real_connected) {
                    ble_air_connect_t connect_param;
                    temp_cntx->is_real_connected = true;
                    memset(&connect_param, 0x0, sizeof(ble_air_connect_t));
                    connect_param.conn_handle = temp_cntx->conn_handle;
                    memcpy(&(connect_param.bdaddr), &(temp_cntx->peer_addr), BT_BD_ADDR_LEN);            
                    LOG_MSGID_I(AIR, "BLE_AIR_EVENT_CONNECT_IND\r\n", 0);
                    ble_air_event_callback(BLE_AIR_EVENT_CONNECT_IND, (void *)&connect_param);
                }
                memset(&ready_to_write, 0x0, sizeof(ble_air_ready_to_write_t));
                ready_to_write.conn_handle= handle;
                memcpy(&(ready_to_write.bdaddr), &(temp_cntx->peer_addr), BT_BD_ADDR_LEN);
                ble_air_event_callback(BLE_AIR_EVENT_READY_TO_WRITE_IND, (void *)&ready_to_write);
            } else if ((0 == ble_air_check_user()) && (0 == temp_cntx->notify_enabled))  {
                if (true == temp_cntx->is_real_connected) {
                    ble_air_disconnect_t disconnect_param;
                    temp_cntx->is_real_connected = false;
                    memset(&disconnect_param, 0x0, sizeof(ble_air_disconnect_t));
                    disconnect_param.conn_handle = temp_cntx->conn_handle;
                    memcpy(&(disconnect_param.bdaddr), &(temp_cntx->peer_addr), BT_BD_ADDR_LEN);
                    LOG_MSGID_I(AIR, "disconnec handle=0x%04x, because CCCD Disable\r\n", 1, handle);
                    ble_air_event_callback(BLE_AIR_EVENT_DISCONNECT_IND, (void *)&disconnect_param);
                }
            }
            LOG_MSGID_I(AIR, "ble_air_tx_char_cccd_callback, data:%d \r\n", 1,  temp_cntx->notify_enabled);
        } else if (rw == BT_GATTS_CALLBACK_READ) {
            if (size != 0) {
                uint16_t *buf = (uint16_t*) data;
                *buf = (uint16_t) temp_cntx->notify_enabled;
                LOG_MSGID_I(AIR, "read cccd value = %d\r\n", 1, *buf);
            }
        }
        
        return sizeof(uint16_t);
    }
    
    return 0;
}


/**
 * @brief Function for handling Air Rx Characteristisc's read and write event.
 *
 * @param[in]   rw                    Flag of Read or Write event.
 * @param[in]   handle                Connection handle.
 * @param[in]   size                  Length of the data.
 * @param[in]   *data                 Data buffer.
 * @param[in]   offset                Write or Read offset.
 * 
 * @return      Real wrote or read length of the data.
 */
static uint32_t ble_air_rx_write_char_callback(const uint8_t rw, uint16_t handle, void *data, uint16_t size, uint16_t offset)
{
    LOG_MSGID_I(AIR, "ble_air_rx_write_char_callback:rw is %d, size is %d, offset is %d \r\n", 3, rw, size, offset);
    
    ble_air_cntx_t *buffer_t = ble_air_get_cntx_by_handle(handle);

    //TODO:Should record for each connection handle.
    if ((handle != BT_HANDLE_INVALID) && (buffer_t) && (buffer_t->is_real_connected)) {
        if (rw == BT_GATTS_CALLBACK_WRITE) {
            /**remote write & notify app ready to read*/
            LOG_MSGID_I(AIR, "write length= %d\r\n", 1, size);
            if (size > (BLE_AIR_RECEIVE_BUFFER_SIZE - buffer_t->receive_buffer_length)) {
                LOG_MSGID_I(AIR, "write characteristic: buffer full error!\r\n", 0);
                
                return 0; /**means fail, buffer full*/
            }
            
        #ifdef BLE_AIR_LOW_POWER_CONTROL
            ble_air_set_remote_device_type(handle, BLE_AIR_REMOTE_DEVICE_IOS);
            ble_air_update_connection_interval(handle);
        #endif    
            ble_air_take_mutex(ble_air_mutex);
            memcpy(&(buffer_t->receive_buffer[buffer_t->receive_buffer_length]), data, size);
            buffer_t->receive_buffer_length += size;
            ble_air_give_mutex(ble_air_mutex);

            if ((0 == ble_air_check_user())) {
                ble_air_ready_to_read_t ready_to_read;
                memset(&ready_to_read, 0, sizeof(ble_air_ready_to_read_t));

                ready_to_read.conn_handle= handle;
                memcpy(&(ready_to_read.bdaddr), &(buffer_t->peer_addr), BT_BD_ADDR_LEN);

                LOG_MSGID_I(AIR, "write characteristic: write size = %d \r\n", 1, size);
                ble_air_event_callback(BLE_AIR_EVENT_READY_TO_READ_IND, (void *)&ready_to_read);

            }

            return size;
        }
    } 
    
    return 0;
}

/**
 *  @brief Function for handling the Application's BLE Stack events.
 *
 *  @param[in] msg    Stack event type.
 *  @param[in] *buff  Stack event parameters.
 */
static bt_status_t ble_air_common_event_handler(bt_msg_type_t msg, bt_status_t status, void *buff)
{   
    //LOG_MSGID_I(AIR, "ble_air_on_ble_evt msg = 0x%04x \r\n", 1, msg);
    switch (msg) {                     
        case BT_GAP_LE_CONNECT_IND: {           
            ble_air_save_connection_info(buff);
        }
        break;

        case BT_GAP_LE_DISCONNECT_IND: {
            bt_hci_evt_disconnect_complete_t *connection_ind = (bt_hci_evt_disconnect_complete_t *)buff;
            uint16_t conn_handle = connection_ind->connection_handle;
            ble_air_cntx_t *buffer_t = ble_air_get_cntx_by_handle(conn_handle);
            if ((conn_handle != BT_HANDLE_INVALID) && (buffer_t)) {
                if (0 == ble_air_check_user() && (buffer_t->is_real_connected)) {
                    ble_air_disconnect_t disconnect_param;
                    memset(&disconnect_param, 0, sizeof(ble_air_disconnect_t));
                    disconnect_param.conn_handle = conn_handle;
                    memcpy(&disconnect_param.bdaddr, &(buffer_t->peer_addr), BT_BD_ADDR_LEN);     
                    LOG_MSGID_I(AIR, "disconnec handle=0x%04x\r\n", 1, conn_handle);
                    ble_air_event_callback(BLE_AIR_EVENT_DISCONNECT_IND, (void *)&disconnect_param);
                }
                ble_air_delete_connection_info(buff);                
            }
        }
        break;
        
#ifdef BLE_AIR_LOW_POWER_CONTROL        
        //case BT_GAP_LE_CONNECTION_UPDATE_CNF:
        case BT_GAP_LE_CONNECTION_UPDATE_IND: {
            bt_gap_le_connection_update_ind_t *ind = (bt_gap_le_connection_update_ind_t *)buff;
            uint16_t conn_handle = ind->conn_handle;
            ble_air_cntx_t *buffer_t = ble_air_get_cntx_by_handle(conn_handle);
            if ((conn_handle != BT_HANDLE_INVALID) && (buffer_t)) {
                LOG_MSGID_I(AIR, "CONNECTION UPDATE: event_id = %x, interval = %d\n", 2, msg, ind->conn_interval);
                buffer_t->low_power_t.conn_interval = ind->conn_interval;
                buffer_t->low_power_t.conn_priority = ble_air_get_current_connection_interval(conn_handle, ind->conn_interval);        
            }
        }
        break;
#endif

        case BT_POWER_OFF_CNF: {
            uint16_t conn_handle = ble_air_get_real_connected_handle();
            ble_air_cntx_t *buffer_t = ble_air_get_cntx_by_handle(conn_handle);
            if ((BT_HANDLE_INVALID != conn_handle) && (buffer_t)) {
                ble_air_disconnect_t disconnect_param;
                memset(&disconnect_param, 0, sizeof(ble_air_disconnect_t));
                disconnect_param.conn_handle = conn_handle;
                memcpy(&(disconnect_param.bdaddr), &(buffer_t->peer_addr), BT_BD_ADDR_LEN);
                LOG_MSGID_I(AIR, "BT power off, then disconn handle=0x%04x\r\n", 1, conn_handle);
                ble_air_event_callback(BLE_AIR_EVENT_DISCONNECT_IND, (void *)&disconnect_param);
            }
            ble_air_clear_all_connection_info();
        }
        break;

        default:
            break;
    }
    
    return BT_STATUS_SUCCESS;
}


static bt_status_t ble_air_event_callback_init(void)
{
    bt_status_t result = bt_callback_manager_register_callback(bt_callback_type_app_event, MODULE_MASK_SYSTEM | MODULE_MASK_GAP, (void*)ble_air_common_event_handler);
    if (result != BT_STATUS_SUCCESS) {
        LOG_MSGID_I(AIR, "ble_air_event_callback_init fail! \r\n", 0);
        return BT_STATUS_FAIL;
    }
    ble_air_clear_all_connection_info();
    return result;
}


/************************************************
*   Functions
*************************************************/
/**
 * @brief Function for sending the Air service tx characteristic value.
 *
 * @param[in]   conn_handle                           connection handle.
 *
 * @return      ble_status_t                              0 means success.
 */
static bt_status_t ble_air_service_tx_send(bt_handle_t conn_handle, uint8_t *data, uint32_t length)
{
    bt_status_t status = BT_STATUS_FAIL;

    uint8_t *buf = NULL;
    bt_gattc_charc_value_notification_indication_t *air_noti_rsp;
    ble_air_cntx_t *buffer_t = ble_air_get_cntx_by_handle(conn_handle);

    buf = (uint8_t *)pvPortMalloc(5 + length);
    if (buf == NULL) {
        LOG_MSGID_I(AIR, "ble_air_service_tx_send fail, OOM!\r\n", 0);  
        return status;
    }
    air_noti_rsp = (bt_gattc_charc_value_notification_indication_t*) buf;
    LOG_I(AIR, "%s: new RX Char Value is %d\r\n", __FUNCTION__, data[0]);  

    if ((conn_handle != BT_HANDLE_INVALID) && (buffer_t) && (buffer_t->is_real_connected) &&
         (BLE_AIR_CCCD_NOTIFICATION ==  buffer_t->notify_enabled)) {
        air_noti_rsp->att_req.opcode = BT_ATT_OPCODE_HANDLE_VALUE_NOTIFICATION;
        air_noti_rsp->att_req.handle = AIR_TX_CHAR_VALUE_HANDLE;
        memcpy((void*)(air_noti_rsp->att_req.attribute_value), data, length);
        air_noti_rsp->attribute_value_length = 3 + length;
        
        LOG_MSGID_I(AIR, "ble_air_service_notify conn_handle is %x, send data is %d\r\n", 2, conn_handle, data[0]);
        
        status = bt_gatts_send_charc_value_notification_indication(conn_handle, air_noti_rsp); 
    }
    if (buf != NULL) {
        vPortFree(buf);
    }
    return status;
}

/**
 * @brief Function for application to write data to the send buffer.
 */
uint32_t ble_air_write_data(uint16_t conn_handle, uint8_t *buffer, uint32_t size)
{
    uint32_t send_size = 0;
    uint32_t mtu_size;
    ble_air_cntx_t *buffer_t = ble_air_get_cntx_by_handle(conn_handle);

    if ((conn_handle != BT_HANDLE_INVALID) && (buffer_t) && 
        (buffer_t->is_real_connected)) {

        mtu_size = bt_gattc_get_mtu((bt_handle_t)conn_handle);

        LOG_MSGID_I(AIR, "mtu = %d\r\n", 1, mtu_size);
        if ((mtu_size - 3) < size) {
            send_size = mtu_size - 3;
        } else {
            send_size = size;
        }
        if (0 == send_size) {
            LOG_MSGID_I(AIR, "[BLE_AIR] ble_air_send_data send_size is 0!\r\n", 0);
            
            return 0;
        }

#ifdef BLE_AIR_LOW_POWER_CONTROL
            ble_air_set_remote_device_type(conn_handle, BLE_AIR_REMOTE_DEVICE_IOS);
            ble_air_update_connection_interval(conn_handle);
#endif   

        if (BT_STATUS_SUCCESS == ble_air_service_tx_send(conn_handle, buffer, send_size)) {
            LOG_MSGID_I(AIR, "[BLE_AIR] ble_air_send_data: send_size[%d]\r\n", 1, send_size);
            
            return send_size;
        }
    }
        
    return 0;
}

uint32_t ble_air_get_rx_available(uint16_t conn_handle)
{
    ble_air_cntx_t *buffer_t = ble_air_get_cntx_by_handle(conn_handle);

    if ((conn_handle != BT_HANDLE_INVALID) && (buffer_t) && (buffer_t->is_real_connected)) {
        return buffer_t->receive_buffer_length;
    }
    
    return 0;
}

/**
 * @brief Function for application to read data from the receive buffer.
 */
uint32_t ble_air_read_data(uint16_t conn_handle, uint8_t *buffer, uint32_t size)
{
    uint32_t read_size = 0;
    ble_air_cntx_t *buffer_t = ble_air_get_cntx_by_handle(conn_handle);

    if ((conn_handle != BT_HANDLE_INVALID) && (buffer_t) && (buffer_t->is_real_connected)) {
        if (buffer_t->receive_buffer_length > size) {
            read_size = size;
        } else {
            read_size = buffer_t->receive_buffer_length;
        }

        if (0 == read_size) {
            LOG_MSGID_I(AIR, "[BLE_AIR] ble_air_read_data: read buffer is null\r\n", 0);
            
            return 0;
        }        
        memcpy(buffer, buffer_t->receive_buffer, read_size);
                
        ble_air_take_mutex(ble_air_mutex);
        if (buffer_t->receive_buffer_length > read_size) {
            memmove(buffer_t->receive_buffer, &(buffer_t->receive_buffer[read_size]), (buffer_t->receive_buffer_length - read_size));
            buffer_t->receive_buffer_length -= read_size;
        } else {
            buffer_t->receive_buffer_length = 0;
            memset(buffer_t->receive_buffer, 0, sizeof(buffer_t->receive_buffer));
        }
        ble_air_give_mutex(ble_air_mutex);

        LOG_MSGID_I(AIR, "[BLE_AIR] ble_air_read_data: read_size is [%d]\r\n", 1, read_size);
        
        return read_size;
    }
    
    LOG_MSGID_I(AIR, "[BLE_AIR] ble_air_read_data: conn id error [%d]\r\n", 1, conn_handle);
    
    return 0;
}

void ble_air_main(void)
{
    /* mutex init */
    if (ble_air_mutex == 0){
        ble_air_mutex = ble_air_create_mutex();
    }
#if 0
#ifdef BLE_AIR_LOW_POWER_CONTROL
    g_xTimer_low_power = ble_air_create_timer();
#endif
#endif
#if defined(MTK_AWS_MCE_ENABLE) && defined (BT_ROLE_HANDOVER_WITH_SPP_BLE)
    bt_role_handover_register_callbacks(BT_ROLE_HANDOVER_MODULE_BLE_AIR, &ble_air_rho_callbacks);
#endif
    ble_air_event_callback_init();
}

/**
 * @brief Function for application main entry.
 */
ble_air_status_t ble_air_init(ble_air_common_callback_t app_callback)
{
    if (NULL == app_callback) {
        return BLE_AIR_STATUS_INVALID_PARAMETER;
    } else {
        /**Initialize.*/    
        LOG_MSGID_I(AIR, "init app_callback=0x%04x", 1, app_callback);
        return ble_air_cb_register(app_callback);
    }
    return BLE_AIR_STATUS_FAIL;
}

ble_air_status_t ble_air_deinit(ble_air_common_callback_t app_callback)
{
    if (NULL == app_callback) {
        return BLE_AIR_STATUS_INVALID_PARAMETER;
    } else {
        /**Initialize.*/
        LOG_MSGID_I(AIR, "deinit app_callback=0x%04x", 1, app_callback);
        return ble_air_cb_deregister(app_callback);
    }
    return BLE_AIR_STATUS_FAIL;
}

static int32_t ble_air_event_callback(ble_air_event_t event_id, void *param)
{
    uint8_t i = 0;
    int32_t ret = 0;

    for (i = 0; i < BLE_AIR_SUPPORT_CB_MAX_NUM; i++) {
        if (ble_air_cb_list[i].in_use && ble_air_cb_list[i].callback != NULL) {
            ble_air_cb_list[i].callback(event_id, param);
            ret = 0;
        }
    }
        
    return ret;
}
    
static bt_status_t ble_air_check_user(void)
{
    uint8_t i = 0;
    bt_status_t status = 0;
    LOG_MSGID_I(AIR, "ble_air_check_user \r\n", 0);

    for (i = 0; i < BLE_AIR_SUPPORT_CB_MAX_NUM; i++) {
        if (ble_air_cb_list[i].in_use) {
            return status;
        }
    }
    if (i == BLE_AIR_SUPPORT_CB_MAX_NUM) {
        
        LOG_MSGID_I(AIR, "not find any users existed!\r\n", 0);
        status = BLE_AIR_STATUS_FAIL;
    }             
    return status;
}

static void ble_air_connection_status_notify(ble_air_common_callback_t callback)
{
    uint16_t conn_handle = ble_air_get_real_connected_handle();
    ble_air_cntx_t *buffer_t = ble_air_get_cntx_by_handle(conn_handle);
    if ((BT_HANDLE_INVALID != conn_handle) && (buffer_t)) {
        ble_air_connect_t connect_param;
        memset(&connect_param, 0x0, sizeof(ble_air_connect_t));
        connect_param.conn_handle = conn_handle;
        memcpy(&(connect_param.bdaddr), &(buffer_t->peer_addr), BT_BD_ADDR_LEN);         
        callback(BLE_AIR_EVENT_CONNECT_IND, (void *)&connect_param);
    }
}

static ble_air_status_t ble_air_cb_register(ble_air_common_callback_t callback)
{
    uint8_t i = 0;
    ble_air_status_t status = 0;
    LOG_MSGID_I(AIR, "ble_air_cb_register callback %x\r\n", 1, callback);

    for (i = 0; i < BLE_AIR_SUPPORT_CB_MAX_NUM; i++) {
        if (!ble_air_cb_list[i].in_use) {
            ble_air_cb_list[i].callback = callback;
            ble_air_cb_list[i].in_use = true;
            ble_air_connection_status_notify(callback);
            break;
        }
    }
    if (i == BLE_AIR_SUPPORT_CB_MAX_NUM) {
        
        LOG_MSGID_I(AIR, "all are in use, please extend the value of BLE_AIR_SUPPORT_CB_MAX_NUM\r\n", 0);
        status = BLE_AIR_STATUS_FAIL;
    }             
    return status;
}

static ble_air_status_t ble_air_cb_deregister(ble_air_common_callback_t callback)
{
    uint8_t i = 0;
    ble_air_status_t status = 0;

    LOG_MSGID_I(SPPAIR, "ble_air_cb_deregister callback %x\r\n", 1, callback);

    for (i = 0; i < BLE_AIR_SUPPORT_CB_MAX_NUM; i++) {
        if (ble_air_cb_list[i].in_use && ble_air_cb_list[i].callback == callback) {
            ble_air_cb_list[i].callback = NULL;
            ble_air_cb_list[i].in_use = false;
            break;
        }
    }
    if (i == BLE_AIR_SUPPORT_CB_MAX_NUM) {    
        LOG_MSGID_I(AIR, "ble_air_cb_deregister delete fail, because of not find the callback\r\n", 0);
        status = BLE_AIR_STATUS_FAIL;
    }   
    return status;
}

#endif /*#ifdef MTK_PORT_SERVICE_BT_ENABLE*/




