ifeq ($(MTK_BT_ENABLE), y)
###################################################
# Libs
###################################################

ifeq ($(MTK_BT_FAST_PAIR_ENABLE), y)
C_FILES += middleware/MTK/bt_fast_pair/src/bt_fast_pair_utility.c
C_FILES += middleware/MTK/bt_fast_pair/src/bt_fast_pair_spp.c
C_FILES += middleware/MTK/bt_fast_pair/src/bt_fast_pair_log.c
CFLAGS += -I$(SOURCE_DIR)/middleware/MTK/bt_fast_pair/inc
CFLAGS += -D__BT_FAST_PAIR_ENABLE__
LIBS += $(SOURCE_DIR)/prebuilt/middleware/MTK/bluetooth/lib/libbt_fast_pair.a
endif

endif
