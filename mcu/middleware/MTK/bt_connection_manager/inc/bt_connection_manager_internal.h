/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __BT_SINK_SRV_CONMGR_H__
#define __BT_SINK_SRV_CONMGR_H__

#include "bt_gap.h"
#include "bt_sink_srv.h"
#include "bt_connection_manager.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************** start restucture **********************/
#define BT_CM_END_AIR_PAIRING_TIMER_DUR         (3000)
#define BT_CM_PROFILE_NOTIFY_DUR                (3000)
#define BT_CM_POWER_ON_RECONNECT_DUR            (5000)
#define BT_CM_REQUEST_DELAY_TIME_DUR            (100)//(3000)
#define BT_CM_LINK_LOST_RECONNECT_DELAY_DUR     (5000)
#define BT_CM_REQUEST_DELAY_TIME_INCREASE_DUR   (3000)  //(15000)
#define BT_CM_POWER_ON_WAITING_REMOTE_AWS_CONNECTION_DUR    (100)//15000
#define BT_CM_PROFILE_ALREADY_EXIST_TIMEOUT_DUR (500)

#define BT_CM_MAX_PROFILE_NUMBER    (6)
#define BT_CM_MAX_TRUSTED_DEV       (BT_DEVICE_MANAGER_MAX_PAIR_NUM)

#define BT_CM_MAX_CALLBACK_NUMBER (5)

/**< All the profiles were connected with AWS agent */
#define BT_CM_REMOTE_FLAG_ROLE_SWITCHING        (0x01)
#define BT_CM_REMOTE_FLAG_NEED_AWS_DISCOVERABLE (0x02)
#define BT_CM_REMOTE_FLAG_RHO_PENDING           (0x04)
#define BT_CM_REMOTE_FLAG_DISCON_ESCO           (0x08)
#define BT_CM_REMOTE_FLAG_ESCO_CONNECTED        (0x10)
#define BT_CM_REMOTE_FLAG_LOCK_DISCONNECT       (0x20)
typedef uint8_t bt_cm_remote_flag_t;

#define BT_CM_FLAG_ROLE_SWITCHING               (0x01)
#define BT_CM_FLAG_PENDING_SET_SCAN_MODE        (0x02)
#define BT_CM_FLAG_SYSTEM_POWER_OFF             (0x04)
#define BT_CM_FLAG_AWS_POWERING_ON              (0x08)
#define BT_CM_FLAG_SYSTEM_POWER_RESET           (0x10)
#define BT_CM_FLAG_POWER_OFF                    (0x20)
#define BT_CM_FLAG_POWER_RESET                  (0x40)
typedef uint8_t bt_cm_flags_t;

#define BT_CM_POWER_TEST_SYS_OFF                (0x01)
#define BT_CM_POWER_TEST_SYS_RESET              (0x02)
typedef uint8_t bt_cm_power_test_sys_t;

#define BT_CM_FIND_BY_HANDLE                    (0x00)
#define BT_CM_FIND_BY_ADDR                      (0x01)
#define BT_CM_FIND_BY_AWS_HANDLE                (0x02)
#define BT_CM_FIND_BY_AWS_STATE                 (0x03)
#define BT_CM_FIND_BY_SET_AWS_STATE_TYPE        (0x04)
#define BT_CM_FIND_BY_REMOTE_FLAG               (0x05)
typedef uint8_t bt_cm_find_t;

#define BT_CM_COMMON_TYPE_DISABLE               (0x00)
#define BT_CM_COMMON_TYPE_ENABLE                (0x01)
#define BT_CM_COMMON_TYPE_UNKNOW                (0xFF)
typedef uint8_t bt_cm_common_type_t;


typedef bt_status_t (*bt_cm_event_handle_callback_t)(bt_cm_event_t event_id, void *params, uint32_t params_len);


typedef enum {
    BT_CM_LIST_CONNECTING = 0x00,
    BT_CM_LIST_CONNECTED =  0x01,

    BT_CM_LIST_TYPE_MAX
} bt_cm_list_t;

#ifdef MTK_AWS_MCE_ENABLE
typedef struct {
    uint32_t                        aws_handle;
    bt_cm_profile_service_state_t   aws_state;
    bt_cm_profile_service_state_t   req_aws_state;
    uint8_t                         rho_sync_data_length;
    void*                           rho_sync_data;
} bt_cm_aws_dev_t;
#endif

typedef struct _bt_cm_remote_device_t{
    struct _bt_cm_remote_device_t    *next[BT_CM_LIST_TYPE_MAX];
    bt_gap_connection_handle_t      handle;
    bt_bd_addr_t                    addr;
    bt_cm_remote_flag_t             flags;
    bt_cm_acl_link_state_t          link_state;
    bt_cm_role_t                    local_role;
    bt_gap_link_sniff_status_t      sniff_state;
    uint8_t                         retry_times;
    bt_cm_profile_service_mask_t    request_connect_mask;
    bt_cm_profile_service_mask_t    request_disconnect_mask;
    bt_cm_profile_service_mask_t    connecting_mask;
    bt_cm_profile_service_mask_t    disconnecting_mask;
    bt_cm_profile_service_mask_t    connected_mask;
#ifdef MTK_AWS_MCE_ENABLE
    bt_cm_aws_dev_t                 aws_dev;
#endif
} bt_cm_remote_device_t;

typedef struct {
    uint8_t                         max_connection_num;
    uint8_t                         devices_buffer_num;
    uint8_t                         connected_dev_num;
    bt_gap_scan_mode_t              scan_mode;
    bt_cm_flags_t                   flags;
    bt_cm_remote_device_t           *handle_list[BT_CM_LIST_TYPE_MAX];
    bt_cm_profile_service_handle_callback_t profile_service_cb[BT_CM_PROFILE_SERVICE_MAX + 1];
    bt_cm_event_handle_callback_t callback_list[BT_CM_MAX_CALLBACK_NUMBER];
    bt_cm_remote_device_t           devices_list[1];
} bt_cm_cnt_t;

void        bt_cm_atci_init();
void        bt_cm_switch_role(bt_bd_addr_t address, bt_role_t role);
void        bt_cm_power_init(void);
void        bt_cm_power_deinit(void);
bt_cm_remote_device_t *
            bt_cm_find_device(bt_cm_find_t find_type, void *param);
bt_status_t bt_cm_write_scan_mode(bt_cm_common_type_t inquiry_scan, bt_cm_common_type_t page_scan);
void        bt_cm_power_update(void *params);
void        bt_cm_power_on_cnf(void);
void        bt_cm_power_off_cnf(void);
bt_status_t bt_cm_prepare_power_deinit();
void        bt_cm_power_test_sys(bt_cm_power_test_sys_t type);
bt_bd_addr_t *
            bt_cm_get_last_connected_device(void);

#ifdef MTK_AWS_MCE_ENABLE
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
void        bt_cm_rho_init();
void        bt_cm_rho_deinit();
void        bt_cm_rho_gap_event_handle(bt_msg_type_t msg, bt_status_t status, void *buffer);
#endif
void        bt_aws_mce_srv_init(void);
bt_status_t bt_aws_mce_srv_air_pairing_power_on_continue();
bool        bt_aws_mce_srv_air_pairing_is_ongoing();
uint32_t    bt_aws_mce_srv_get_aws_handle(bt_bd_addr_t *addr);
#endif


bt_status_t bt_cm_register_event_callback(bt_cm_event_handle_callback_t cb);
void        bt_cm_register_callback_notify(bt_cm_event_t event_id, void *params, uint32_t params_len);

/******************** end restucture **********************/

#ifdef __cplusplus
}
#endif

#endif
