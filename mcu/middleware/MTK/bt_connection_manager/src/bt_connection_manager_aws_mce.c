/* Copyright Statement:
 *
 * (C) 2019  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal.h"
#include "hal_wdt.h"
#include "bt_gap_le.h"
#include "bt_callback_manager.h"
#include "bt_device_manager.h"
#include "bt_device_manager_internal.h"
#include "bt_device_manager_db.h"
#include "bt_connection_manager.h"
#include "bt_connection_manager_utils.h"
#include "bt_connection_manager_internal.h"
#ifdef MTK_AWS_MCE_ENABLE
#include "bt_aws_mce_report.h"
#include "bt_aws_mce_srv.h"
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
#include "bt_role_handover.h"
#endif
#endif

#define BT_AWS_MCE_SRV_SYNC_PARTNER_ADDR_TO_AGENT   (0x00)
#define BT_AWS_MCE_SRV_SYNC_CONNECTION_CONFIRM      (0x01)
typedef uint8_t bt_aws_mce_srv_sync_t;

#define BT_AWS_MCE_SRV_FLAGS_INITINIG               (0x01)
#define BT_AWS_MCE_SRV_FLAGS_SET_ROLE_PENDING       (0x02)
#define BT_AWS_MCE_SRV_FLAGS_CONNECT_DISABLED       (0x04)
typedef uint8_t bt_aws_mce_srv_flags_t;

#define BT_AWS_MCE_SRV_SUPPORT_NUM                  (0x03)

typedef struct {
    bt_bd_addr_t                    address;
    uint32_t                        aws_handle;
    bt_cm_profile_service_state_t   aws_state;
    bt_cm_profile_service_state_t   req_aws_state;
} bt_aws_srv_dev_t;

typedef struct {
    bt_aws_mce_srv_flags_t  flags;
    bt_aws_srv_dev_t        aws_dev[BT_AWS_MCE_SRV_SUPPORT_NUM];
} bt_aws_mce_srv_cnt_t;

static bt_aws_mce_srv_cnt_t g_bt_aws_mce_srv_cnt_t;

static void         bt_aws_mce_srv_state_update(bt_cm_remote_device_t *rem_dev);

static void         bt_aws_mce_srv_send_packet(bt_aws_mce_srv_sync_t type, uint32_t data_length, void *data)
{
    bt_status_t status;
    uint32_t report_length = sizeof(bt_aws_mce_report_info_t) + data_length + 1;
    bt_aws_mce_report_info_t *cm_report = bt_cm_memory_alloc(report_length);
    if (NULL == cm_report) {
        bt_cmgr_report_id("[BT_CM][AWS_MCE][E] Send AWS packet failed can't allocat buffer.", 0);
        return;
    }
    bt_cm_memset(cm_report, 0 , sizeof(report_length));
    uint8_t *data_payload = ((uint8_t *)cm_report) + sizeof(bt_aws_mce_report_info_t);
    cm_report->module_id = BT_AWS_MCE_REPORT_MODULE_CM;
    cm_report->param_len = report_length - sizeof(bt_aws_mce_report_info_t);
    cm_report->param = data_payload;
    data_payload[0] = type;
    if (0 != data_length) {
        bt_cm_memcpy(data_payload + 1, (void*)data, data_length);
    }
    if (BT_STATUS_SUCCESS != (status = bt_aws_mce_report_send_event(cm_report))) {
        bt_cmgr_report_id("[BT_CM][AWS_MCE][E] Send AWS packet failed status 0x%x.", 1, status);
    }
    bt_cm_memory_free(cm_report);
}

static void         bt_aws_mce_srv_packet_callback(bt_aws_mce_report_info_t *para)
{
    if (NULL == para) {
        bt_cmgr_report_id("[BT_CM][AWS_MCE][E] AWS packet para is null.", 0);
        return;
    }
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] AWS packet module_id:0x%x, is_sync:%d, sync_time:%d, param_len:%d.", 4,
        para->module_id, para->is_sync, para->sync_time, para->param_len);
    if (BT_AWS_MCE_REPORT_MODULE_CM != para->module_id) {
        bt_cmgr_report_id("[BT_CM][AWS_MCE][I] AWS packet module is not CM.", 0);
        return;
    }
    bt_aws_mce_srv_sync_t event = ((uint8_t *)para->param)[0];
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] AWS packet event: %d.", 1, event);
    switch(event) {
        case BT_AWS_MCE_SRV_SYNC_PARTNER_ADDR_TO_AGENT: {
            bt_bd_addr_t *partner_addr = (bt_bd_addr_t *)(((uint8_t *)para->param) + 1);
            bt_device_manager_aws_local_info_store_peer_address(partner_addr);
        }
            break;
        case BT_AWS_MCE_SRV_SYNC_CONNECTION_CONFIRM: {

        }
            break;
        default:
            bt_cmgr_report_id("[BT_CM][AWS_MCE][E] AWS packet event error.", 0);
        break;
    }
}

static bt_cm_remote_device_t*
                    bt_aws_mce_srv_find_req_connected_dev_except(bt_cm_remote_device_t *rem_dev)
{
    extern bt_cm_cnt_t *g_bt_cm_cnt;
    extern bt_cm_config_t *g_bt_cm_cfg;
    if (NULL == g_bt_cm_cnt || NULL == g_bt_cm_cfg) {
        return NULL;
    }
    for (uint32_t i = 0; i < g_bt_cm_cfg->max_connection_num; ++i) {
        if (g_bt_cm_cnt->devices_list[i].aws_dev.req_aws_state == BT_CM_PROFILE_SERVICE_STATE_CONNECTED &&
            (NULL == rem_dev || rem_dev != &g_bt_cm_cnt->devices_list[i])) {
            return &g_bt_cm_cnt->devices_list[i];
        }
    }
    return NULL;
}

static bt_status_t  bt_aws_mce_srv_disconnect_req_connect_dev_except(bt_cm_remote_device_t *rem_dev)
{
    bt_cm_remote_device_t *device_p = bt_aws_mce_srv_find_req_connected_dev_except(rem_dev);
    if (NULL == device_p) {
        return BT_STATUS_FAIL;
    } 
    device_p->aws_dev.req_aws_state = BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED;
    if (BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED == device_p->aws_dev.aws_state) {
        bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_AWS, device_p->addr,
            BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED, BT_STATUS_SUCCESS);
        return BT_STATUS_FAIL;
    }
    bt_aws_mce_srv_state_update(device_p);
    return BT_STATUS_SUCCESS;
}

static bt_status_t  bt_aws_mce_srv_agent_set_state(bt_cm_remote_device_t *rem_dev, bool enable)
{
    bt_status_t ret = BT_STATUS_FAIL;
    bt_cm_profile_service_state_t aws_state = rem_dev->aws_dev.aws_state;
    uint32_t aws_handle = rem_dev->aws_dev.aws_handle;
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] Agent set AWS state:%d, cur_state:0x%x, aws_handle:0x%x, device:0x%x.",
        4, enable, aws_state, aws_handle, rem_dev);
    if (true == enable && BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED == aws_state) {
        if (BT_GAP_LINK_SNIFF_TYPE_ACTIVE != rem_dev->sniff_state) {
            /* The sp connection link sniff state should be active before enable LS on special link or sp link. */
            bt_status_t status;
            status = bt_gap_exit_sniff_mode(rem_dev->handle);
            bt_cmgr_report_id("[BT_CM][AWS_MCE][E] Exit sniff mode fail status 0x%x.", 1, status);
            return ret;
        } else if (BT_CM_ACL_LINK_ENCRYPTED > rem_dev->link_state) {
            /* Wait the encryption complete.  */
            return ret;
        } else if (BT_STATUS_SUCCESS == bt_aws_mce_srv_disconnect_req_connect_dev_except(rem_dev)) {
            /* Wait other aws profile disconnected. */
            return ret;
        } else if (BT_CM_ROLE_SLAVE != rem_dev->local_role) {
            /* Wait to set role to slave. */
            bt_cm_switch_role(rem_dev->addr, BT_ROLE_SLAVE);
            return ret;
        }
        ret = bt_aws_mce_set_state(aws_handle, BT_AWS_MCE_AGENT_STATE_CONNECTABLE);
    } else if (false == enable &&
        (BT_CM_PROFILE_SERVICE_STATE_CONNECTING == aws_state || BT_CM_PROFILE_SERVICE_STATE_CONNECTED == aws_state)) {
        ret = bt_aws_mce_set_state(aws_handle, BT_AWS_MCE_AGENT_STATE_INACTIVE);
    } else {
        bt_cmgr_report_id("[BT_CM][AWS_MCE][W] AWS not in correct state.", 0);
        return ret;
    }
    if (BT_STATUS_SUCCESS != ret) {
        g_bt_aws_mce_srv_cnt_t.flags |= BT_AWS_MCE_SRV_FLAGS_SET_ROLE_PENDING;
        bt_cmgr_report_id("[BT_CM][AWS_MCE][W] AWS Set state fail status:0x%x.", 1, ret);
    }
    return ret;
}

static void         bt_aws_mce_srv_agent_state_machine(bt_cm_remote_device_t *rem_dev)
{
    bt_status_t status = BT_STATUS_FAIL;
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] AWS state update req:0x%x, now:0x%x, link state:0x%x", 3,
        rem_dev->aws_dev.req_aws_state, rem_dev->aws_dev.aws_state, rem_dev->link_state);
    if (BT_CM_PROFILE_SERVICE_STATE_CONNECTED == rem_dev->aws_dev.req_aws_state) {
        if (BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED == rem_dev->aws_dev.aws_state) {
            if (BT_CM_ACL_LINK_DISCONNECTED == rem_dev->link_state ||
                BT_CM_ACL_LINK_PENDING_CONNECT == rem_dev->link_state ||
                BT_CM_ACL_LINK_CONNECTING == rem_dev->link_state) {
                status = bt_aws_mce_connect(&(rem_dev->aws_dev.aws_handle), (const bt_bd_addr_t *)&(rem_dev->addr));
            } else if (BT_CM_ACL_LINK_CONNECTED <= rem_dev->link_state) {
                status = bt_aws_mce_srv_agent_set_state(rem_dev, true);
            }
            if (BT_STATUS_SUCCESS != status) {
                bt_cmgr_report_id("[BT_CM][AWS_MCE][E] AWS connect fail status:0x%x, link state:%d.", 2, status, rem_dev->link_state);
                return;
            }
            rem_dev->aws_dev.aws_state = BT_CM_PROFILE_SERVICE_STATE_CONNECTING;
            bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_AWS, rem_dev->addr,
                BT_CM_PROFILE_SERVICE_STATE_CONNECTING, BT_STATUS_SUCCESS);
        }
    } else if (BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED == rem_dev->aws_dev.req_aws_state) {
        if ((BT_CM_PROFILE_SERVICE_STATE_CONNECTED == rem_dev->aws_dev.aws_state ||
            BT_CM_PROFILE_SERVICE_STATE_CONNECTING == rem_dev->aws_dev.aws_state) &&
            BT_CM_ACL_LINK_CONNECTED <= rem_dev->link_state) {
            if (BT_STATUS_SUCCESS != (status = bt_aws_mce_srv_agent_set_state(rem_dev, false))) {
                bt_cmgr_report_id("[BT_CM][AWS_MCE][E] AWS disconnect fail status:0x%x, link state:%d.", 2, status, rem_dev->link_state);
                return;
            }
            rem_dev->aws_dev.aws_state = BT_CM_PROFILE_SERVICE_STATE_DISCONNECTING;
            bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_AWS, rem_dev->addr,
                BT_CM_PROFILE_SERVICE_STATE_DISCONNECTING, BT_STATUS_SUCCESS);
        } else if (BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED == rem_dev->aws_dev.aws_state) {
            bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_AWS, rem_dev->addr, BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED, BT_STATUS_SUCCESS);
        }
    }
}

extern void         bt_avm_set_wide_band_scan_flag(bool enable);
static void         bt_aws_mce_srv_partner_state_machine(bt_cm_remote_device_t *rem_dev)
{
    bt_status_t status = BT_STATUS_FAIL;
    if (BT_CM_PROFILE_SERVICE_STATE_CONNECTED == rem_dev->aws_dev.req_aws_state) {
        if (BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED == rem_dev->aws_dev.aws_state) {
            bt_avm_set_wide_band_scan_flag(true);
            if (BT_STATUS_SUCCESS != (status = bt_aws_mce_connect(&(rem_dev->aws_dev.aws_handle), (const bt_bd_addr_t *)&(rem_dev->addr)))) {
                bt_avm_set_wide_band_scan_flag(false);
                bt_cmgr_report_id("[BT_CM][AWS_MCE][E] AWS connect fail status:0x%x.", 1, status);
                return;
            }
            rem_dev->aws_dev.aws_state = BT_CM_PROFILE_SERVICE_STATE_CONNECTING;
            bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_AWS, rem_dev->addr,
                BT_CM_PROFILE_SERVICE_STATE_CONNECTING, BT_STATUS_SUCCESS);
        }
    } else if (BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED == rem_dev->aws_dev.req_aws_state) {
        if (BT_CM_PROFILE_SERVICE_STATE_CONNECTED == rem_dev->aws_dev.aws_state) {
            if (BT_STATUS_SUCCESS != (status = bt_aws_mce_disconnect(rem_dev->aws_dev.aws_handle))) {
                bt_cmgr_report_id("[BT_CM][AWS_MCE][E] AWS disconnect fail status:0x%x.", 1, status);
                return;
            }
            rem_dev->aws_dev.aws_state = BT_CM_PROFILE_SERVICE_STATE_DISCONNECTING;
            bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_AWS, rem_dev->addr,
                BT_CM_PROFILE_SERVICE_STATE_DISCONNECTING, BT_STATUS_SUCCESS);
        } else if (BT_CM_PROFILE_SERVICE_STATE_CONNECTING == rem_dev->aws_dev.aws_state) {
            bt_avm_set_wide_band_scan_flag(false);
            rem_dev->aws_dev.aws_state = BT_CM_PROFILE_SERVICE_STATE_DISCONNECTING;
            bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_AWS, rem_dev->addr,
                BT_CM_PROFILE_SERVICE_STATE_DISCONNECTING, BT_STATUS_SUCCESS);
        }
    }
}

static void         bt_aws_mce_srv_client_state_machine(bt_cm_remote_device_t *rem_dev)
{
    bt_aws_mce_srv_partner_state_machine(rem_dev);
}

static void         bt_aws_mce_srv_state_update(bt_cm_remote_device_t *rem_dev)
{
    bt_aws_mce_role_t aws_role = bt_device_manager_aws_local_info_get_role();
    if (NULL == rem_dev || (g_bt_aws_mce_srv_cnt_t.flags & BT_AWS_MCE_SRV_FLAGS_INITINIG)) {
        return;
    }
    if (BT_AWS_MCE_ROLE_AGENT == aws_role) {
        bt_aws_mce_srv_agent_state_machine(rem_dev);
    } else if (BT_AWS_MCE_ROLE_PARTNER == aws_role) {
        bt_aws_mce_srv_partner_state_machine(rem_dev);
    } else if (BT_AWS_MCE_ROLE_CLINET == aws_role) {
        bt_aws_mce_srv_client_state_machine(rem_dev);
    }
}

static void         bt_aws_mce_srv_state_change_handle(bt_aws_mce_state_change_ind_t *state, bt_status_t status)
{
    bt_cm_remote_device_t *rem_dev = bt_cm_find_device(BT_CM_FIND_BY_AWS_HANDLE, &(state->handle));
    if (NULL == rem_dev) {
        bt_cmgr_report_id("[BT_CM][AWS_MCE][E] AWS state notify device not find by addr.", 0);
        return;
    }
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] State change state:0x%x, status:0x%x.", 2, state->state, status);
    if (BT_AWS_MCE_AGENT_STATE_INACTIVE == state->state) {
        rem_dev->aws_dev.aws_state = BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED;
        if (rem_dev->aws_dev.aws_state == rem_dev->aws_dev.req_aws_state) {
            bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_AWS, rem_dev->addr,
                BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED, BT_STATUS_SUCCESS);
        }
        bt_cm_remote_device_t *conn_dev = bt_aws_mce_srv_find_req_connected_dev_except(NULL);
        bt_aws_mce_srv_state_update(conn_dev);
    } else if (BT_AWS_MCE_AGENT_STATE_ATTACHED == state->state &&
        // For RHO case, there will received attached state change, but can't send confirm packet to partner.
        BT_CM_PROFILE_SERVICE_STATE_CONNECTED != rem_dev->aws_dev.aws_state) {
        rem_dev->aws_dev.aws_state = BT_CM_PROFILE_SERVICE_STATE_CONNECTED;
        bt_device_manager_aws_local_info_update();
        if (rem_dev->aws_dev.aws_state == rem_dev->aws_dev.req_aws_state) {
            // Agent need send confirm packet to partner after it received partner connected event. 
            bt_cmgr_report_id("[BT_CM][AWS_MCE][I] Send AWS confirm packet and paired list to parnter.", 0);
            bt_device_manager_remote_aws_sync_to_partner();
            bt_aws_mce_srv_send_packet(BT_AWS_MCE_SRV_SYNC_CONNECTION_CONFIRM, 0, NULL);
            bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_AWS, rem_dev->addr,
                BT_CM_PROFILE_SERVICE_STATE_CONNECTED, BT_STATUS_SUCCESS);
        }
        bt_aws_mce_srv_state_update(rem_dev);
    } else if (BT_AWS_MCE_AGENT_STATE_CONNECTABLE == state->state) {
        rem_dev->aws_dev.aws_state = BT_CM_PROFILE_SERVICE_STATE_CONNECTING;
        bt_aws_mce_srv_state_update(rem_dev);
    }
}

static void         bt_aws_mce_srv_connected_handle(bt_aws_mce_connected_t *conn, bt_status_t status)
{
    bt_aws_mce_role_t aws_role = bt_device_manager_aws_local_info_get_role();
    bt_cm_remote_device_t *rem_dev =
        bt_cm_find_device(BT_CM_FIND_BY_ADDR, conn->address);
    if (status != BT_STATUS_SUCCESS || NULL == rem_dev) {
        bt_cmgr_report_id("[BT_CM][AWS_MCE][E] Fail find the dev:0x%x.", 1, rem_dev);
        return;
    }
    rem_dev->aws_dev.aws_handle = conn->handle;
    if (BT_AWS_MCE_ROLE_AGENT == aws_role) {
        rem_dev->aws_dev.aws_state = BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED;
        bt_aws_mce_srv_state_update(rem_dev);
    } else if ((BT_AWS_MCE_ROLE_CLINET | BT_AWS_MCE_ROLE_PARTNER) & aws_role) {
        bt_bd_addr_t *local_addr = bt_device_manager_get_local_address();
        if (bt_cm_memcmp(&(rem_dev->addr), local_addr, sizeof(bt_bd_addr_t))) {
            rem_dev->aws_dev.req_aws_state = BT_CM_PROFILE_SERVICE_STATE_CONNECTED;
            rem_dev->aws_dev.aws_state = BT_CM_PROFILE_SERVICE_STATE_CONNECTED;
            bt_device_manager_aws_local_info_update();
            // Since agent may not know the partner's address, we need sync it to agent.
            bt_cmgr_report_id("[BT_CM][AWS_MCE][I] Send partner/client address to agent.", 0);
            bt_aws_mce_srv_send_packet(BT_AWS_MCE_SRV_SYNC_PARTNER_ADDR_TO_AGENT, sizeof(bt_bd_addr_t), local_addr);
            bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_AWS, rem_dev->addr,
                BT_CM_PROFILE_SERVICE_STATE_CONNECTED, BT_STATUS_SUCCESS);
        } else {
            rem_dev->aws_dev.req_aws_state = BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED;
            rem_dev->aws_dev.aws_state = BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED;
        }
    }
}

static void         bt_aws_mce_srv_disconnected_handle(bt_aws_mce_disconnected_t *disconn, bt_status_t status)
{
    bt_cm_remote_device_t *rem_dev = bt_cm_find_device(BT_CM_FIND_BY_AWS_HANDLE, &(disconn->handle));
    if (NULL == rem_dev) {
        bt_cmgr_report_id("[BT_CM][AWS_MCE][E] Fail can't find the dev by handle 0x%x.", 1, disconn->handle);
        return;
    }
    bt_cm_profile_service_state_t pre_req_state = rem_dev->aws_dev.req_aws_state; 
    rem_dev->aws_dev.aws_handle = 0;
    rem_dev->aws_dev.aws_state = BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED;
    rem_dev->aws_dev.req_aws_state = BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED;
    if (BT_CM_PROFILE_SERVICE_STATE_CONNECTED == pre_req_state || BT_CM_PROFILE_SERVICE_STATE_CONNECTING == pre_req_state) {
        bt_cm_profile_service_status_notify(BT_CM_PROFILE_SERVICE_AWS, rem_dev->addr,
            BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED, status);
    }
}

static bt_status_t  bt_aws_mce_srv_basic_event_callback(bt_msg_type_t msg, bt_status_t status, void *buffer)
{
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] AWS event msg:0x%x, status:0x%x, buff:0x%x.", 3, msg, status, buffer);
    switch (msg) {
        case BT_AWS_MCE_CONNECTED:
            bt_aws_mce_srv_connected_handle(buffer, status);
            break;
        case  BT_AWS_MCE_DISCONNECTED:
            bt_aws_mce_srv_disconnected_handle(buffer, status);
            break;
        case  BT_AWS_MCE_STATE_CHANGED_IND:
            bt_aws_mce_srv_state_change_handle(buffer, status);
            break;
        case BT_GAP_WRITE_STORED_LINK_KEY_CNF:
            if (g_bt_aws_mce_srv_cnt_t.flags & BT_AWS_MCE_SRV_FLAGS_INITINIG) {
                g_bt_aws_mce_srv_cnt_t.flags &= (~BT_AWS_MCE_SRV_FLAGS_INITINIG);
                bt_cm_remote_device_t *rem_dev = bt_aws_mce_srv_find_req_connected_dev_except(NULL);            
                bt_aws_mce_srv_state_update(rem_dev);
            }
            break;
        case BT_GAP_SET_ROLE_CNF:
            if (g_bt_aws_mce_srv_cnt_t.flags & BT_AWS_MCE_SRV_FLAGS_SET_ROLE_PENDING) {
                g_bt_aws_mce_srv_cnt_t.flags &= (~BT_AWS_MCE_SRV_FLAGS_SET_ROLE_PENDING);
                bt_cm_remote_device_t *conn_dev = bt_aws_mce_srv_find_req_connected_dev_except(NULL);
                bt_aws_mce_srv_state_update(conn_dev);
            }
            break;
        case BT_GAP_ROLE_CHANGED_IND: {
            bt_cm_remote_device_t *rem_dev = NULL;
            bt_gap_role_changed_ind_t *role_change = (bt_gap_role_changed_ind_t *)buffer;
            if (NULL != role_change && (role_change->local_role & BT_ROLE_SLAVE) &&
                NULL != (rem_dev = bt_cm_find_device(BT_CM_FIND_BY_HANDLE, &(role_change->handle)))) {
                bt_aws_mce_srv_state_update(rem_dev);
            }
        }
            break;
        case BT_GAP_SNIFF_MODE_CHANGE_IND: {
            bt_cm_remote_device_t *rem_dev = NULL;
            bt_gap_sniff_mode_changed_ind_t* ind = (bt_gap_sniff_mode_changed_ind_t *)buffer;
            if (NULL != ind && (BT_GAP_LINK_SNIFF_TYPE_ACTIVE == ind->sniff_status) &&
                NULL != (rem_dev = bt_cm_find_device(BT_CM_FIND_BY_HANDLE, &(ind->handle)))) {
                bt_aws_mce_srv_state_update(rem_dev);
            }
        }
            break;
        default:
            break;
    }
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
    bt_cm_rho_gap_event_handle(msg, status, buffer);
#endif
    return BT_STATUS_SUCCESS;
}

static bt_status_t  bt_aws_mce_srv_handle_connect_cb(void *data)
{
    bt_status_t status = BT_STATUS_SUCCESS;
    bt_aws_mce_role_t aws_role = bt_device_manager_aws_local_info_get_role();
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] Connect AWS profile.", 0);
    bt_cm_remote_device_t *rem_dev = NULL;
    if (NULL == (rem_dev = bt_cm_find_device(BT_CM_FIND_BY_ADDR, data))) {
        return BT_STATUS_FAIL;
    }
    if (((BT_AWS_MCE_ROLE_CLINET | BT_AWS_MCE_ROLE_PARTNER) & aws_role) &&
        BT_CM_PROFILE_SERVICE_STATE_DISCONNECTING == rem_dev->aws_dev.aws_state) {
        return BT_STATUS_FAIL;
    }
    rem_dev->aws_dev.req_aws_state = BT_CM_PROFILE_SERVICE_STATE_CONNECTED;
    bt_aws_mce_srv_state_update(rem_dev);
    return status;
}

static bt_status_t  bt_aws_mce_srv_handle_disconnect_cb(void *data)
{
    bt_status_t status = BT_STATUS_SUCCESS;
    bt_aws_mce_role_t aws_role = bt_device_manager_aws_local_info_get_role();
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] Disconnect AWS prfoile.", 0);
    bt_cm_remote_device_t *rem_dev = NULL;
    if (NULL == (rem_dev = bt_cm_find_device(BT_CM_FIND_BY_ADDR, data))) {
        return BT_STATUS_FAIL;
    }
    if (((BT_AWS_MCE_ROLE_CLINET | BT_AWS_MCE_ROLE_PARTNER) & aws_role) &&
        BT_CM_PROFILE_SERVICE_STATE_CONNECTING == rem_dev->aws_dev.aws_state) {
        return BT_STATUS_FAIL;
    }
    rem_dev->aws_dev.req_aws_state = BT_CM_PROFILE_SERVICE_STATE_DISCONNECTED;
    bt_aws_mce_srv_state_update(rem_dev);
    return status;
}

static bt_status_t  bt_aws_mce_srv_handle_power_on_cnf_cb()
{
    bt_aws_mce_role_t aws_role = bt_device_manager_aws_local_info_get_role();
    bt_key_t* aws_key = bt_device_manager_aws_local_info_get_key();
    bt_bd_addr_t* aws_addr = NULL;
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] AWS profile service power on init, role:0x%02x.", 1, aws_role);
    if (BT_AWS_MCE_ROLE_NONE == aws_role) {
        return BT_STATUS_FAIL;
    }
    bt_callback_manager_register_callback(bt_callback_type_app_event,
        (uint32_t)(MODULE_MASK_AWS_MCE | MODULE_MASK_GAP), (void *)bt_aws_mce_srv_basic_event_callback);
    bt_aws_mce_report_register_callback(BT_AWS_MCE_REPORT_MODULE_CM, bt_aws_mce_srv_packet_callback);
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
    bt_cm_rho_init();
#endif
    if ((BT_AWS_MCE_ROLE_CLINET | BT_AWS_MCE_ROLE_PARTNER) & aws_role) {
        aws_addr = bt_device_manager_aws_local_info_get_peer_address();
    } else {
        aws_addr = bt_device_manager_get_local_address();
    }
    bt_gap_stored_link_key_t key_list;
    bt_cm_memcpy(&(key_list.address), aws_addr, sizeof(bt_bd_addr_t));
    bt_cm_memcpy(&(key_list.key), aws_key, sizeof(bt_key_t));
    bt_gap_write_stored_link_key_param_t key_param = {
        .key_number = 1,
        .key_list = &key_list
    };
    if (BT_AWS_MCE_ROLE_PARTNER == aws_role) {
        aws_role |= BT_AWS_MCE_ROLE_CLINET;
    }
    bt_aws_mce_init_role(aws_role);
    bt_status_t ret = bt_gap_write_stored_link_key(&key_param);
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] Write AWS key result:0x%x.", 1, ret);
    bt_cm_assert(BT_STATUS_SUCCESS == ret);
    g_bt_aws_mce_srv_cnt_t.flags = BT_AWS_MCE_SRV_FLAGS_INITINIG;
    return BT_STATUS_SUCCESS;
}

static bt_status_t  bt_aws_mce_srv_handle_power_off_cnf_cb()
{
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] AWS profile service power off deinit.", 0);
    g_bt_aws_mce_srv_cnt_t.flags = 0;
    bt_callback_manager_deregister_callback(bt_callback_type_app_event, (void *)bt_aws_mce_srv_basic_event_callback);
    bt_aws_mce_report_deregister_callback(BT_AWS_MCE_REPORT_MODULE_CM);
#ifdef SUPPORT_ROLE_HANDOVER_SERVICE
    bt_cm_rho_deinit();
#endif
    return BT_STATUS_SUCCESS;
}

static bt_status_t  bt_aws_mce_srv_profile_service_handle_cb(bt_cm_profile_service_handle_t type, void *data)
{
    bt_status_t status = BT_STATUS_FAIL;
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] Connection manager handle type:0x%02x.", 1, type);
    switch (type) {
        case BT_CM_PROFILE_SERVICE_HANDLE_POWER_ON:
            status = bt_aws_mce_srv_handle_power_on_cnf_cb();
            break;
        case BT_CM_PROFILE_SERVICE_HANDLE_POWER_OFF:
            status = bt_aws_mce_srv_handle_power_off_cnf_cb();
            break;
        case BT_CM_PROFILE_SERVICE_HANDLE_CONNECT:
            status = bt_aws_mce_srv_handle_connect_cb(data);
            break;
        case BT_CM_PROFILE_SERVICE_HANDLE_DISCONNECT:
            status = bt_aws_mce_srv_handle_disconnect_cb(data);
            break;
        default:
            break;
    }
    return status;
}

bt_aws_mce_srv_link_type_t
                    bt_aws_mce_srv_get_link_type()
{
    bt_bd_addr_t aws_device;
    if (bt_cm_get_connected_devices(BT_CM_PROFILE_SERVICE_MASK(BT_CM_PROFILE_SERVICE_AWS), &aws_device, 1)) {
        bt_bd_addr_t *local_addr = bt_device_manager_get_local_address();
        bt_aws_mce_role_t aws_role = bt_device_manager_aws_local_info_get_role();
        if (BT_AWS_MCE_ROLE_AGENT == aws_role) {
            if (bt_cm_memcmp(&aws_device, local_addr, sizeof(bt_bd_addr_t))) {
                return BT_AWS_MCE_SRV_LINK_NORMAL;
            } else {
                return BT_AWS_MCE_SRV_LINK_SPECIAL;
            }
        } else if ((BT_AWS_MCE_ROLE_CLINET | BT_AWS_MCE_ROLE_PARTNER) & aws_role) {
            if (bt_cm_get_gap_handle(*local_addr)) {
                return BT_AWS_MCE_SRV_LINK_NORMAL;
            } else {
                return BT_AWS_MCE_SRV_LINK_SPECIAL;
            }
        }
    }
    return BT_AWS_MCE_SRV_LINK_NONE;
}

uint32_t            bt_aws_mce_srv_get_aws_handle(bt_bd_addr_t *addr)
{
    bt_cm_remote_device_t *aws_device = bt_cm_find_device(BT_CM_FIND_BY_ADDR, (void *)addr);
    return aws_device->aws_dev.aws_handle;
}

bt_status_t         bt_aws_mce_srv_switch_role(bt_aws_mce_role_t dest_role, bt_aws_mce_switch_role_mode_t mode)
{
    bt_aws_mce_role_t aws_role = bt_device_manager_aws_local_info_get_role();
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] switch role dest:0x%x, mode:%d", 2, dest_role, mode);
    if (aws_role == dest_role) {
        return BT_STATUS_SUCCESS;
    }
    bt_device_manager_aws_local_info_store_role(dest_role);
    if (BT_AWS_MCE_SWITCH_ROLE_MODE_ADDR_AUTO_CHANGE) {
        bt_bd_addr_t    local_addr = {0};
        bt_bd_addr_t    *peer_addr = bt_device_manager_aws_local_info_get_peer_address();
        bt_cm_memcpy((void*)&local_addr, (void*)bt_device_manager_get_local_address(), sizeof(bt_bd_addr_t));

        bt_device_manager_aws_local_info_store_local_address(peer_addr);
        bt_device_manager_aws_local_info_store_peer_address(&local_addr);
    }
    return BT_STATUS_SUCCESS;
}

bt_status_t         bt_aws_mce_srv_set_disable(bool enable)
{
    g_bt_aws_mce_srv_cnt_t.flags |= BT_AWS_MCE_SRV_FLAGS_CONNECT_DISABLED;
    return BT_STATUS_SUCCESS;
}

void                bt_aws_mce_srv_init()
{
    bt_cmgr_report_id("[BT_CM][AWS_MCE][I] AWS profile service init.", 0);
    bt_cm_memset(&g_bt_aws_mce_srv_cnt_t, 0, sizeof(g_bt_aws_mce_srv_cnt_t));
    bt_cm_profile_service_register(BT_CM_PROFILE_SERVICE_AWS, &bt_aws_mce_srv_profile_service_handle_cb);
}


