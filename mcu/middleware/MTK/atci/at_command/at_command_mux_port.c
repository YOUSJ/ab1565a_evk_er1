/* Copyright Statement:
 *
 * (C) 2020  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifdef MTK_MUX_ENABLE

/* Includes ------------------------------------------------------------------*/
#include "at_command.h"
#include "serial_port.h"
#include "mux.h"
#include "toi.h"
#include "syslog.h"
#include "string.h"
#include <ctype.h>
#include "serial_port_assignment.h"

#if  defined ( __GNUC__ )
  #ifndef __weak
    #define __weak   __attribute__((weak))
  #endif /* __weak */
#endif /* __GNUC__ */

/* Private macro -------------------------------------------------------------*/
#define AT_COMMAND_MUX_PORT_HELP \
"AT+MPORT - show or modify serial port assignment and switch.\r\n\
usage:\r\n\
   MUX port show:   AT+MPORT=0\r\n\
   MUX port assign: AT+MPORT=1,<owner_name>,<device_id>\r\n"

/* reserve
   MUX port switch: AT+MPORT=2,<owner_name>,<device_id>\r\n\
   Parameters modify:  AT+MPORT=3,<device_id>,<parameters>\r\n\
   Parameters show:    AT+MPORT=4\r\n"
*/
log_create_module(atci_MUX_port, PRINT_LEVEL_INFO);

#define LOGE(fmt,arg...)   LOG_E(atci_MUX_port, "[MUX_atci_port]"fmt,##arg)
#define LOGW(fmt,arg...)   LOG_W(atci_MUX_port, "[MUX_atci_port]"fmt,##arg)
#define LOGI(fmt,arg...)   LOG_I(atci_MUX_port ,"[MUX_atci_port]"fmt,##arg)

#define LOGMSGIDE(fmt,cnt,arg...)   LOG_MSGID_E(atci_MUX_port ,"[MUX_atci_port]"fmt,cnt,##arg)
#define LOGMSGIDW(fmt,cnt,arg...)   LOG_MSGID_W(atci_MUX_port ,"[MUX_atci_port]"fmt,cnt,##arg)
#define LOGMSGIDI(fmt,cnt,arg...)   LOG_MSGID_I(atci_MUX_port ,"[MUX_atci_port]"fmt,cnt,##arg)


#define is_0_to_f(_c)   ((_c >= '0' && _c <= '9') || (_c >= 'a' && _c <= 'f'))
#define ascii_to_number(_c)   ((_c >= '0' && _c <= '9') ? (_c - '0') : (_c - 'a' + 10))
#define number_to_ascii(_c)   ((_c <= 9) ? (_c + '0') : (_c - 10 + 'a'))

/* Private typedef -----------------------------------------------------------*/
typedef struct {
    char name[SERIAL_PORT_USER_NAME_SIZE];
    void (*serial_port_switch)(serial_port_dev_t device);
} serial_port_switch_t;

typedef struct {
    bool (*parse_para)(serial_port_dev_t device, char *parameter, serial_port_dev_setting_t *dev_setting);
    void (*show_para)(serial_port_dev_t device, serial_port_dev_setting_t *dev_setting, char *buffer);
} serial_port_atcmd_operation_t;

/* Private functions ---------------------------------------------------------*/
static bool serial_port_uart_parse_para(serial_port_dev_t device, char *parameter, serial_port_dev_setting_t *dev_setting);
static bool serial_port_usb_parse_para(serial_port_dev_t device, char *parameter, serial_port_dev_setting_t *dev_setting);
static void serial_port_uart_show_para(serial_port_dev_t device, serial_port_dev_setting_t *dev_setting, char *buffer);
static void serial_port_usb_show_para(serial_port_dev_t device, serial_port_dev_setting_t *dev_setting, char *buffer);



/* Extern functions ---------------------------------------------------------*/
#if defined(USE_ULS)
extern void uls_serial_port_switch(serial_port_dev_t device);
#endif
#if defined(EMMI_UART_DMA_MODE)
extern void emmi_serial_port_switch(serial_port_dev_t device);
#endif
#if defined(MTK_CONNL_VIA_PORT_SERVICE)
extern void connl_serial_port_switch(serial_port_dev_t device);
#if defined(MTK_CONNL_2ND_AT_PORT)
extern void connl_serial_port_switch2(serial_port_dev_t device);
#endif
#endif

extern void syslog_serial_port_switch(serial_port_dev_t device);
extern void atci_serial_port_switch(serial_port_dev_t device);

/* Private variables ---------------------------------------------------------*/
static serial_port_switch_t g_serial_port_switch[] = {
#if !defined (MTK_DEBUG_LEVEL_NONE) && !defined (MTK_SYSLOG_VERSION_2)
    {
        "syslog",
        syslog_serial_port_switch,
    },
#endif
#ifndef ATCI_APB_PROXY_ADAPTER_ENABLE
/*AT command owner needs to fix the build error for port switch.*/
#if defined(MTK_ATCI_VIA_PORT_SERVICE)
    {
        "atci",
        atci_serial_port_switch,
    },
#endif
#endif
#if defined(USE_ULS)
    {
        "uls",
        uls_serial_port_switch,
    },
#endif
#if defined(EMMI_UART_DMA_MODE)
    {
        "emmi",
        emmi_serial_port_switch,
    },
#endif
#if defined(MTK_CONNL_VIA_PORT_SERVICE)
    {
        "connl",
        connl_serial_port_switch,
    },
#if defined(MTK_CONNL_2ND_AT_PORT)
    {
        "connl2",
        connl_serial_port_switch2,
    },
#endif
#endif
};

static serial_port_atcmd_operation_t g_serial_port_atcmd_operation[SERIAL_PORT_TYPE_MAX] = {
    {
        serial_port_uart_parse_para,
        serial_port_uart_show_para,
    },
    {
        serial_port_usb_parse_para,
        serial_port_usb_show_para,
    },
};

/**
* @brief      Reassign the specific user of port service to use new device port.
* @param[in]  owner_name: The specific user of port service.
* @param[in]  dev_id: The new device port to use.
* @return     whether Reassign of device port is successfully.
*/
bool at_serial_port_switch(char *owner_name, serial_port_dev_t dev_id)
{
    uint32_t i;

    for (i = 0; i < sizeof(g_serial_port_switch) / sizeof(serial_port_switch_t); i++) {
        if (strcmp(g_serial_port_switch[i].name, owner_name) == 0) {
            g_serial_port_switch[i].serial_port_switch(dev_id);
            return true;
        }
    }

    return false;
}

/**
* @brief      Translate integer to string.
* @param[in]  number: The integer need to tranlate.
* @param[out] buffer: The buffer used to store the string translated from integer.
* @return     bit count of the string.
*/
static uint32_t digital_to_string(uint32_t number, char *buffer)
{
    uint32_t i, bits, temp[10], divider;

    bits = 0;
    divider = number;
    do {
        temp[bits] = divider % 10;
        divider /= 10;
        bits++;
    } while (divider);

    for (i = 0; i < bits; i++) {
        buffer[i] = temp[bits - i - 1] + 0x30;
    }

    return bits;
}

/**
* @brief      Parse UART specifc parameters string and store result to structure of port service.
*             Pattern of Parameters string for UART shows below:
*             <baudrate>
* @param[in]  device: the UART specific device index.
* @param[in]  parameter: parameters string for the specific device.
* @param[out] dev_setting: parameter setting structure of the specific device to store result of parse.
* @return     whether parse of parameter string is successfully.
*/
static bool serial_port_uart_parse_para(serial_port_dev_t device, char *parameter, serial_port_dev_setting_t *dev_setting)
{
    char *p_curr_pos;
    uint8_t type;
    uint32_t temp;

    p_curr_pos = parameter;
    while (*p_curr_pos && (*p_curr_pos != '\n') && (*p_curr_pos != '\r')) {
        p_curr_pos++;
    }
    *p_curr_pos = '\0';

    temp = toi(parameter, &type);
    if (!(type == TOI_DEC && temp < HAL_UART_BAUDRATE_MAX)) {
        return false;
    }

    dev_setting->port_setting_uart.baudrate = (hal_uart_baudrate_t)temp;

    return true;
}

/**
* @brief      Parse USB specifc parameters string and store result to structure of port service.
*             Now it's just reserved.
* @param[in]  device: the USB specific device index.
* @param[in]  parameter: parameters string for the specific device.
* @param[out] dev_setting: parameter setting structure of the specific device to store result of parse.
* @return     whether parse of parameter string is successfully.
*/
static bool serial_port_usb_parse_para(serial_port_dev_t device, char *parameter, serial_port_dev_setting_t *dev_setting)
{
    return false;
}

/**
* @brief      Format parameter setting of the specific UART device and store in specific buffer.
* @param[in]  device: the specific device index.
* @param[in]  dev_setting: parameter setting structure of the specific device.
* @param[out] buffer: the buffer used to store formatted data.
* @return     None.
*/
static void serial_port_uart_show_para(serial_port_dev_t device, serial_port_dev_setting_t *dev_setting, char *buffer)
{
    uint32_t bits;
    char temp[10];

    strcat(buffer, "baudrate = ");
    bits = digital_to_string(dev_setting->port_setting_uart.baudrate, temp);
    temp[bits] = '\0';
    strcat(buffer, temp);
}

/**
* @brief      Format parameter setting of the specific USB device and store in specific buffer.
* @param[in]  device: the specific device index.
* @param[in]  dev_setting: parameter setting structure of the specific device.
* @param[out] buffer: the buffer used to store formatted data.
* @return     None.
*/
static void serial_port_usb_show_para(serial_port_dev_t device, serial_port_dev_setting_t *dev_setting, char *buffer)
{
    strcat(buffer, "none");
}

/**
* @brief      Parse device common parameters string imported by ATCI input.
* @param[in]  para: parameters string imported by ATCI input.
* @param[out] owner_name: ower name of port service user after parsing.
* @param[out] dev_id: device index assigned to port service user after parsing.
* @return     whether parse of parameter string is successfully.
*/
static bool parse_command(char *para, char *owner_name, serial_port_dev_t *dev_id)
{
    uint8_t type;
    uint32_t temp;
    char *p_owner_name, *p_device_id, *p_bt_address, *p_curr_pos;

    /* skip AT+MPORT=2 */
    p_owner_name = p_curr_pos = para;
    p_curr_pos = strchr(p_owner_name, ',');
    if (p_curr_pos == NULL) {
        return false;
    }

    /* skip <owner_name> */
    p_owner_name = ++p_curr_pos;
    p_curr_pos = strchr(p_owner_name, ',');
    if (p_curr_pos == NULL) {
        return false;
    }
    *p_curr_pos = '\0';

    /* skip <device_id> */
    p_device_id = ++p_curr_pos;
    p_curr_pos = strchr(p_device_id, ',');
    if (p_curr_pos != NULL) {
        *p_curr_pos = '\0';
        p_bt_address = ++p_curr_pos;
        p_curr_pos = p_bt_address;
    } else {
        p_bt_address = NULL;
        p_curr_pos = p_device_id;
    }

    /* Replace end of parameter string to '\0' */
    while (*p_curr_pos && (*p_curr_pos != '\n') && (*p_curr_pos != '\r')) {
        p_curr_pos++;
    }
    *p_curr_pos = '\0';

    /* extract owner name from parameters string */
    if (strlen(p_owner_name) >= SERIAL_PORT_USER_NAME_SIZE) {
        return false;
    }
    strcpy(owner_name, p_owner_name);

    /* extract device index from parameters string */
    temp = toi(p_device_id, &type);
    if (!(type == TOI_DEC && temp <= SERIAL_PORT_DEV_USB_TYPE_END)) {
        return false;
    }
    *dev_id = (serial_port_dev_t)temp;

    return true;
}

/**
* @brief      Parse and store parameters string imported by ATCI input.
* @param[in]  para: parameters string imported by ATCI input.
* @return     whether parse of parameter string is successfully.
*/
static bool parse_parameter_modify_command(char *para)
{
    uint8_t type;
    uint32_t temp;
    char *p_owner_name, *p_device_id, *p_parameter, *p_curr_pos;
    serial_port_dev_t device;
    serial_port_type_t device_type;
    serial_port_dev_setting_t dev_setting;

    /* skip AT+MPORT=3. */
    p_owner_name = p_curr_pos = para;
    p_curr_pos = strchr(p_owner_name, ',');
    if (p_curr_pos == NULL) {
        return false;
    }

    /* skip <device_id>. */
    p_device_id = ++p_curr_pos;
    p_curr_pos = strchr(p_device_id, ',');
    if (p_curr_pos == NULL) {
        return false;
    }
    *p_curr_pos = '\0';

    /* skip <parameters>. */
    p_parameter = ++p_curr_pos;

    /* Replace end of parameter string to '\0' */
    while (*p_curr_pos && (*p_curr_pos != '\n') && (*p_curr_pos != '\r')) {
        p_curr_pos++;
    }
    *p_curr_pos = '\0';

    temp = toi(p_device_id, &type);
    if (!(type == TOI_DEC && temp <= SERIAL_PORT_DEV_USB_TYPE_END)) {
        return false;
    }
    device = (serial_port_dev_t)temp;

    /* Call device specific parse function to parse those parameter string. */
    serial_port_config_read_dev_setting(device, &dev_setting);
    device_type = serial_port_get_device_type(device);
    if (g_serial_port_atcmd_operation[device_type].parse_para(device, p_parameter, &dev_setting) == false) {
        return false;
    }
    serial_port_config_write_dev_setting(device, &dev_setting);

    return true;
}

/**
* @brief      Read and format default port setting of all MUX port devices.
* @param[out] response_buf: Buffer to store all formatted data.
* @return     None.
*/
static void at_mux_port_show_setting(char *response_buf)
{
    uint32_t i, j, bits;
    char temp[10];
    uint32_t user_count;
    mux_status_t status, f_status;
    mux_port_assign_t mux_port_assign[10];

    f_status = MUX_STATUS_ERROR;
    for(i=MUX_UART_BEGIN; i<MUX_PORT_END; i++) {

        user_count = 0;
        memset((uint8_t*)mux_port_assign, 0 ,sizeof(mux_port_assign_t) * 10);

        status = mux_query_port_user_number_form_nvdm(i, &user_count);
        LOGMSGIDI("status = %d mux port %d user number %d ", 3, status, i, user_count);
        if(status != MUX_STATUS_OK) {
            continue;
        }

        status = mux_query_port_user_name_from_nvdm(i, (mux_port_assign_t*)&mux_port_assign);
        LOGMSGIDI("mux query user name status = %d", 1, status);

        if((user_count != 0) && (status == MUX_STATUS_OK)) {
            for(j=0; j<user_count; j++) {
                strcat(response_buf, "+MPORT: ");
                strcat(response_buf, mux_port_assign[j].name);
                strcat(response_buf, " = ");
                bits = digital_to_string(i, temp);
                temp[bits] = '\0';
                strcat(response_buf, temp);
                strcat(response_buf, "\r\n");
            }
            f_status = MUX_STATUS_OK;
        }
    }

    if (f_status != MUX_STATUS_OK) {
        strcat(response_buf, " mux none\r\n ");
        return;
    }
}

serial_port_status_t mux_user_port_write_config(const char *user_name, mux_port_t device)
{
#ifdef MTK_NVDM_ENABLE
    mux_status_t status;
    mux_port_buffer_t query_port_buffer;
    mux_port_setting_t setting;

    if ((strlen(user_name) >= SERIAL_PORT_USER_NAME_SIZE) || (user_name == NULL )|| (device >= MUX_PORT_END)) {
        LOGMSGIDI("[ERROR_PARAMETER]: please check user_name or device", 0);
        return MUX_STATUS_ERROR_PARAMETER;
    }

    if(strcmp(user_name, "atci") ==0) {
        query_port_buffer.count = 0;
        if (MUX_STATUS_OK != mux_query_port_numbers_from_nvdm("ATCI", (mux_port_buffer_t*)&query_port_buffer.count) ){
            LOGMSGIDI("[ERROR_NVDM_NOT_FOUND]: cannot query atci user in NVDM", 0);
            return MUX_STATUS_ERROR_NVDM_NOT_FOUND;
        }

        LOGMSGIDI("atci port count = %d, atci_1 port = %d, atci_2 port = %d  set = %d",
            4, query_port_buffer.count, query_port_buffer.buf[0],  query_port_buffer.buf[1], device);

        status = mux_query_port_setting_from_nvdm(query_port_buffer.buf[1], &setting);
        if(status != MUX_STATUS_OK) {
            setting.tx_buffer_size = 1024;
            setting.rx_buffer_size = 1024;
            setting.dev_setting.uart.uart_config.baudrate            = CONFIG_ATCI_BAUDRATE;
            setting.dev_setting.uart.uart_config.word_length         = HAL_UART_WORD_LENGTH_8;
            setting.dev_setting.uart.uart_config.stop_bit            = HAL_UART_STOP_BIT_1;
            setting.dev_setting.uart.uart_config.parity              = HAL_UART_PARITY_NONE;
            setting.dev_setting.uart.flowcontrol_type                = MUX_UART_NONE_FLOWCONTROL;
        }

        if(query_port_buffer.count == 2) {
            status = mux_close_delete_from_nvdm(query_port_buffer.buf[1], "ATCI");
            if(status != MUX_STATUS_OK) {
                LOGMSGIDI("mux port delete ATCI from NVDM: error", 0);
                return MUX_STATUS_ERROR;
            }
            LOGMSGIDI("mux port delete ATCI from NVDM: OK", 0);

            status = mux_open_save_to_nvdm(device, "ATCI");
            if(status != MUX_STATUS_OK) {
                LOGMSGIDI("mux port open ATCI save to NVDM: error ", 0);
                return MUX_STATUS_ERROR;
            }
            LOGMSGIDI("mux port open ATCI save to NVDM: OK ", 0);

            status = mux_save_port_setting_to_nvdm(device, &setting);
            if(status != MUX_STATUS_OK) {
                LOGMSGIDI("mux port save ATCI setting to NVDM: error ", 0);
                return MUX_STATUS_ERROR ;
            }
            LOGMSGIDI("mux port save ATCI setting to NVDM: OK ", 0);
        }
    }

    return MUX_STATUS_OK;
#else
    return MUX_STATUS_ERROR_NVDM_NOT_FOUND;
#endif
}

/**
* @brief      Read and format all parameters setting of port service devices.
* @param[out] response_buf: Buffer to store all formatted data.
* @return     None.
*/
void at_mux_port_show_para_setting(char *response_buf)
{
    serial_port_dev_t device;
    serial_port_type_t device_type;
    uint32_t bits;
    char temp[128];
    serial_port_dev_setting_t dev_setting;

    for (device = SERIAL_PORT_DEV_UART_TYPE_BEGIN; device <= SERIAL_PORT_DEV_USB_TYPE_END; device++) {
        /* Read all parameters setting of port service devices. */
        serial_port_config_read_dev_setting(device, &dev_setting);
        /* Format those setting and store to buffer imported. */
        strcat(response_buf, "+MPORT: ");
        bits = digital_to_string(device, temp);
        temp[bits] = '\0';
        strcat(response_buf, temp);
        strcat(response_buf, "\r\n");
        memset(temp, 0, sizeof(temp));
        device_type = serial_port_get_device_type(device);
        g_serial_port_atcmd_operation[device_type].show_para(device, &dev_setting, temp);
        strcat(response_buf, temp);
        strcat(response_buf, "\r\n");
    }
}

/**
* @brief      AT command handler function for port service.
* @param[in]  parse_cmd: command parameters imported by ATCI module.
* @return     Execution result of port service AT command.
*/
atci_status_t atci_cmd_hdlr_mux_port(atci_parse_cmd_param_t *parse_cmd)
{
    atci_response_t response = {{0}};
    char owner_name[SERIAL_PORT_USER_NAME_SIZE];
    serial_port_dev_t dev_id;
    bool result;
    char cmd[256] = {0};
    uint8_t  i = 0;

    char   *ptr = NULL;
    uint32_t para[2];
    char   *cmd_str;
    cmd_str = (char *)parse_cmd->string_ptr;

    response.response_flag = 0; // Command Execute Finish.
    #ifdef ATCI_APB_PROXY_ADAPTER_ENABLE
    response.cmd_id = parse_cmd->cmd_id;
    #endif

    strncpy(cmd, (char *)parse_cmd->string_ptr, sizeof(cmd)-1);
    for (i = 0; i < strlen((char *)parse_cmd->string_ptr); i++) {
        cmd[i] = (char)toupper((unsigned char)cmd[i]);
    }

    switch (parse_cmd->mode) {
        case ATCI_CMD_MODE_TESTING:
            LOGMSGIDI("atci_cmd_hdlr_mux_port query command usage\r\n", 0);
            strcpy((char *)(response.response_buf), AT_COMMAND_MUX_PORT_HELP);
            response.response_flag = ATCI_RESPONSE_FLAG_APPEND_OK;
            response.response_len = strlen((char *)(response.response_buf));
            break;
        case ATCI_CMD_MODE_EXECUTION:
            /* Show default port for specific port service user. */
            if (strncmp((char *)cmd, "AT+MPORT=0", 10) == 0) {
                LOGMSGIDI("atci_cmd_hdlr_mux_port read port config\r\n", 0);
                at_mux_port_show_setting((char *)(response.response_buf));
                response.response_len = strlen((char *)response.response_buf);
                response.response_flag = ATCI_RESPONSE_FLAG_APPEND_OK;
            }
            /* Modify default port for specific port service user. */
            else if (strstr((char *)cmd, "AT+MPORT=1,") != NULL) {
                LOGMSGIDI("atci_cmd_hdlr_mux_port write port config\r\n", 0);
                result = parse_command(parse_cmd->string_ptr, owner_name, &dev_id);
                if (result == false) {
                    response.response_flag = ATCI_RESPONSE_FLAG_APPEND_ERROR;
                    break;
                }
                mux_user_port_write_config(owner_name, dev_id);
                //serial_port_config_write_dev_number(owner_name, dev_id);
                response.response_flag = ATCI_RESPONSE_FLAG_APPEND_OK;
            }
            /* Switch port for specific port service user. */
            /* AT+MPORT=2,<port_number>,<on/off>
                <port_number>, MUX_PORT_T.
                <on/off> 1:OFF, 0 ON.
            */
            else if (strncmp(cmd_str, "AT+MPORT=2,", strlen("AT+MPORT=2,")) == 0){
                LOGMSGIDI("atci_cmd_hdlr_mux_port ignore data of a port\r\n", 0);
                ptr = strtok(parse_cmd->string_ptr,",");
                for(i=0; i<2; i++) {
                    ptr = strtok(NULL, ",");
                    if(ptr != NULL){
                        para[i] = atoi(ptr);
                    }
                }
                if(para[1] == 1){
                    mux_control(para[0],MUX_CMD_DISCONNECT,NULL);
                    mux_control(para[0],MUX_CMD_CLEAN,NULL);
                }else if(para[1] == 0){
                    //hal_gpt_delay_ms(500);
                    mux_control(para[0],MUX_CMD_CLEAN,NULL);
                    hal_gpt_delay_ms(100);
                    mux_control(para[0],MUX_CMD_CONNECT,NULL);
                }

                response.response_flag = ATCI_RESPONSE_FLAG_APPEND_OK;
                atci_send_response(&response);
                //at_serial_port_switch(owner_name, dev_id);
                return ATCI_STATUS_OK;
            }
            /* Modify parameters setting of specific port service device. */
            else if (strstr((char *)cmd, "AT+MPORT=3,") != NULL) {
                LOGMSGIDI("atci_cmd_hdlr_mux_port modify parameters\r\n", 0);
                result = parse_parameter_modify_command(parse_cmd->string_ptr);
                if (result == false) {
                    response.response_flag = ATCI_RESPONSE_FLAG_APPEND_ERROR;
                    break;
                }
                response.response_flag = ATCI_RESPONSE_FLAG_APPEND_OK;
            }
            /* Dump parameters setting of all port service devices. */
            else if (strstr((char *)cmd, "AT+MPORT=4") != NULL) {
                LOGMSGIDI("atci_cmd_hdlr_mux_port show parameters\r\n", 0);
                //at_serial_port_show_para_setting((char *)(response.response_buf));
                response.response_len = strlen((char *)response.response_buf);
                response.response_flag = ATCI_RESPONSE_FLAG_APPEND_OK;
            } else {
                response.response_flag = ATCI_RESPONSE_FLAG_APPEND_ERROR;
            }
            break;
        default:
            strcpy((char *)(response.response_buf), "ERROR\r\n");
            response.response_flag = ATCI_RESPONSE_FLAG_APPEND_ERROR;
            response.response_len = strlen((char *)(response.response_buf));
            break;
    }

    atci_send_response(&response);

    return ATCI_STATUS_OK;
}

#endif  /*MTK_MUX_ENABLE*/

