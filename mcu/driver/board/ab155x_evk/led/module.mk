
###################################################
BOARD_SRC = driver/board/ab155x_evk
BOARD_LED_SRC = $(BOARD_SRC)/led/src
###################################################
# include path
CFLAGS  += -I$(SOURCE_DIR)/$(BOARD_SRC)/led/inc

#source file
C_FILES += $(BOARD_LED_SRC)/bsp_led_isink.c
C_FILES += $(BOARD_LED_SRC)/bsp_led.c
#################################################################################
CFLAGS         += -DMTK_LED_ENABLE
