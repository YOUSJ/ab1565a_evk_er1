/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "hal.h"
#include "bsp_led_isink.h"

#ifdef HAL_ISINK_MODULE_ENABLED

/////////////////////////////////////////Macro Defined//////////////////////////////////////////////
#define BSP_T1_T2_REPEAT_CALI_TM   10000//unit:ms

#define BSP_MS_TO_TICK(tick)      (tick * 32.768f)
#define BSP_LED_T0_TO_MS(channel) (g_led_priv_info[channel].led_cfg.t0 * g_led_priv_info[channel].led_cfg.time_unit)
#define BSP_LED_T1_TO_MS(channel) (g_led_priv_info[channel].led_cfg.t1 * g_led_priv_info[channel].led_cfg.time_unit)
#define BSP_LED_T2_TO_MS(channel) (g_led_priv_info[channel].led_cfg.t2 * g_led_priv_info[channel].led_cfg.time_unit)
#define BSP_LED_T3_TO_MS(channel) (g_led_priv_info[channel].led_cfg.t3 * g_led_priv_info[channel].led_cfg.time_unit)
#define BSP_LED_T1_T2_WITH_REPEAT_TO_MS(channel) (g_led_priv_info[channel].t1t2_cycle * g_led_priv_info[channel].led_cfg.repeat_t1t2)
#define BSP_LED_EXT_LOOP_MS(channel) (BSP_LED_T1_T2_WITH_REPEAT_TO_MS(channel) +BSP_LED_T3_TO_MS(channel) )

/////////////////////////////////////////Function Declare//////////////////////////////////////////////
extern hal_isink_status_t   hal_isink_start(hal_isink_channel_t channel);
extern hal_isink_status_t   hal_isink_stop(hal_isink_channel_t channel);
extern void                 bsp_led_process(bsp_led_channel_t channel, bsp_led_action_t action);
extern hal_isink_status_t   hal_isink_enable_pwm_mode(hal_isink_channel_t channel, uint32_t cycle_ms, uint32_t duty_persent);


////////////////////////////////////Private Variable Define//////////////////////////////////////////////
static  bsp_isink_private_info_t            g_led_priv_info[BSP_LED_CHANNEL_MAX] = {{0},{0}};
static  const uint16_t                      g_led_isink_tm_tb[] = {123,338,523,707,926,1107,1291,1507,1691,1876,2091,2276,2460,2676,2860,3075};
/*
    Comments:
     Why led_table[BSP_LED_STATE_IDLE][BSP_LED_ACTION_OF_STOP] == BSP_LED_STATE_IDLE?
     Why led_table[BSP_LED_STATE_IDLE][BSP_LED_ACTION_OF_TIMEOUT] == BSP_LED_STATE_IDLE?
             Just for void race condition, because bsp_led_enable() and bsp_led_disable() can be called by any thread, and maybe interrupt led_process() which executed by the GPT IRQ thread.
*/
static  uint32_t                            g_led_state_table[BSP_LED_STATE_MAX][BSP_LED_ACTION_MAX] =
{                           /*BSP_LED_ACTION_OF_START, BSP_LED_ACTION_OF_TIMEOUT, BSP_LED_ACTION_OF_STOP,  BSP_LED_ACTION_OF_CALI_TIMEOUT*/
/*BSP_LED_STATE_IDLE */     {BSP_LED_STATE_T0,         BSP_LED_STATE_IDLE,    BSP_LED_STATE_IDLE,          0xFFFFFFF8},
/*BSP_LED_STATE_T0 */       {0xFFFFFFF5,               BSP_LED_STATE_T1T2_RPT,BSP_LED_STATE_IDLE,          0xFFFFFFFa},
/*BSP_LED_STATE_T1T2_RPT */ {0xFFFFFFF6,               BSP_LED_STATE_T3,      BSP_LED_STATE_IDLE,          BSP_LED_STATE_T1T2_RPT},
/*BSP_LED_STATE_T3 */       {0xFFFFFFF7,               BSP_LED_STATE_T1T2_RPT,BSP_LED_STATE_IDLE,          0xFFFFFFFc},
};



/////////////////////////////////////////Private function//////////////////////////////////////////////
static  uint32_t    led_isink_get_time_sel(uint32_t time,int mul)
{
    uint32_t i   = 0;
    uint32_t tmp = 0;
    uint32_t res = 0;
    uint32_t map_sz = 0;

    map_sz = sizeof(g_led_isink_tm_tb)/sizeof(uint16_t);
    tmp = abs(time - g_led_isink_tm_tb[0]*mul);
    for(i=1;i< map_sz;i++) {
        if( tmp < (res = abs(time - g_led_isink_tm_tb[i]*mul)) ) {
            return i-1;
        }
        tmp = res;
    }
    return i-1;
}

static  void        isink_hw_config(bsp_led_channel_t ledx, bsp_led_config_t* config, uint32_t coef)
{
    bsp_isink_private_info_t    *pinfo = &g_led_priv_info[ledx];

    pinfo->led_cfg  = *config;
    pinfo->req_cali = false;
    if(pinfo->mode == BSP_LED_BLINK_MODE){
        uint32_t freq = 0;
        uint32_t duty = 0;

        /*if led is off mode, not config isink*/
        if(BSP_LED_T1_TO_MS(ledx) == 0){
            log_bsp_led_warn("[bsp][led][isink] led%d blink: alway off\r\n", 1, ledx);
            return;
        } else if(BSP_LED_T2_TO_MS(ledx) == 0){
            freq = 1;
            duty = (pinfo->led_cfg.brightness * 100)/255;
            log_bsp_led_warn("[bsp][led][isink] led%d blink: always on\r\n", 1, ledx);
        } else {
            freq = (BSP_LED_T1_TO_MS(ledx) + BSP_LED_T2_TO_MS(ledx));
            duty = (BSP_LED_T1_TO_MS(ledx) * 100)/(BSP_LED_T1_TO_MS(ledx) + BSP_LED_T2_TO_MS(ledx));
            pinfo->req_cali   = true;
            pinfo->t1t2_cycle = freq;
        }
        hal_isink_set_mode(ledx, HAL_ISINK_MODE_PWM);
        hal_isink_enable_pwm_mode(ledx, freq, duty);
        log_bsp_led_info("[bsp][led][isink] led%d blink: t1t2 cycle %dms, duty %d\r\n", 3, ledx, freq, duty);
    } else {
        hal_isink_breath_mode_t breath_mode;
        uint32_t                ton,toff;

        if(BSP_LED_T1_TO_MS(ledx) == 0){
            log_bsp_led_warn("[bsp][led][isink] led%d breath: alway off\r\n", 1, ledx);
            return;
        } else if(BSP_LED_T2_TO_MS(ledx) == 0){
            uint32_t freq = 0;
            uint32_t duty = 0;
            freq = 1;
            duty = (pinfo->led_cfg.brightness * 100)/255;
            log_bsp_led_warn("[bsp][led][isink] led%d breath: always on(duty %d)\r\n", 2, ledx, duty);
            hal_isink_set_mode(ledx, HAL_ISINK_MODE_PWM);
            hal_isink_enable_pwm_mode(ledx, freq, duty);
        }else {
            ton  = (BSP_LED_T1_TO_MS(ledx) > 246)?(BSP_LED_T1_TO_MS(ledx) - 246):0;
            toff = (BSP_LED_T2_TO_MS(ledx) > 246)?(BSP_LED_T2_TO_MS(ledx) - 246):0;
            ton  = (ton  * pinfo->sw_cali.led_sw_cali_coefficient)/100;
            toff = (toff * pinfo->sw_cali.led_sw_cali_coefficient)/100;
            breath_mode.darker_to_lighter_time1 = 0;
            breath_mode.darker_to_lighter_time2 = 0;
            breath_mode.lighter_to_darker_time1 = 0;
            breath_mode.lighter_to_darker_time2 = 0;
            breath_mode.lightest_time           = led_isink_get_time_sel(ton,  1);
            breath_mode.darkest_time            = led_isink_get_time_sel(toff, 2);

            hal_isink_set_mode(ledx, HAL_ISINK_MODE_BREATH);
            hal_isink_enable_breath_mode(ledx, breath_mode);
            pinfo->t1t2_cycle =     g_led_isink_tm_tb[breath_mode.darker_to_lighter_time1] + \
                                    g_led_isink_tm_tb[breath_mode.darker_to_lighter_time2] + \
                                    g_led_isink_tm_tb[breath_mode.lightest_time]*1 +         \
                                    g_led_isink_tm_tb[breath_mode.lighter_to_darker_time1] + \
                                    g_led_isink_tm_tb[breath_mode.lighter_to_darker_time2] + \
                                    g_led_isink_tm_tb[breath_mode.darkest_time]*2;
            log_bsp_led_warn("[bsp][led][isink] led%d breath: t1t2 cycle %dms\r\n", 2, ledx, pinfo->t1t2_cycle);
        }
    }
}

static  void        bsp_led_gpt_callback(void *args)
{
    bsp_isink_private_info_t  *pinfo = (bsp_isink_private_info_t*)args;
    bsp_led_process(pinfo->led_id, pinfo->next_action);
}

#ifdef BSP_LED_SW_CALI_FEATURE
/* led_sw_cali_coefficient = 100, means no need SW Cali.
    when 0< Cali<99, means need cali,
    there are two point to do led_sw_cali_coefficient calculate:
        1. The first time to start, just to to cali init and cali the timer of syn_tm_ms.
        2. When start T0, means a new loop, to cali next total loop*/
static  void        bsp_led_sw_cali_init(bsp_led_channel_t channel, uint32_t dly_sta_tm)
{
    bsp_isink_private_info_t    *pinfo = &g_led_priv_info[channel];
    uint32_t temp_32K_tick;
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_32K, &temp_32K_tick);
    pinfo->sw_cali.expected_expire_tick    = temp_32K_tick + BSP_MS_TO_TICK(dly_sta_tm);// next expected tick when start T0
    pinfo->sw_cali.led_sw_cali_coefficient = 100;

}
static  void        bsp_led_sw_cali(bsp_led_channel_t channel, uint32_t tw_cyc)
{
    bsp_isink_private_info_t    *pinfo = &g_led_priv_info[channel];
    uint32_t temp_32K_tick;
    uint32_t ext_loop_tick;
    int      temp_diff_32K_tick;

    ext_loop_tick = (uint32_t)BSP_MS_TO_TICK(tw_cyc);
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_32K,  &temp_32K_tick);
    temp_diff_32K_tick = temp_32K_tick - pinfo->sw_cali.expected_expire_tick;

    if(temp_diff_32K_tick > 0 && temp_diff_32K_tick >=  ext_loop_tick)
    {
        log_bsp_led_info("[bsp][led][isink] sw cali: gpt timer delay too long (%d = %d - %d) > %d", 4, \
                            temp_diff_32K_tick, \
                            temp_32K_tick,      \
                            pinfo->sw_cali.expected_expire_tick,  \
                            ext_loop_tick
                        );
    }

    pinfo->sw_cali.led_sw_cali_coefficient = ((ext_loop_tick - temp_diff_32K_tick) * 100) /ext_loop_tick;
    if(pinfo->sw_cali.led_sw_cali_coefficient<90){
        pinfo->sw_cali.led_sw_cali_coefficient = 90;
    }
    log_bsp_led_info("[bsp][led][isink] sw cali: coeff %d, gpt delay %d =%d-%d tick, ext_loop_tick:%d", 5,
                        (int)pinfo->sw_cali.led_sw_cali_coefficient,
                        (int)temp_diff_32K_tick,
                        temp_32K_tick,
                        pinfo->sw_cali.expected_expire_tick,
                        ext_loop_tick
                    );
    pinfo->sw_cali.expected_expire_tick += ext_loop_tick;
}
#endif

void                bsp_led_process(bsp_led_channel_t channel, bsp_led_action_t action)
{
    uint32_t    start_timer_continue = 0xFFFF;
    uint32_t    temp_timer_ms = 0;
    bsp_isink_private_info_t  *pinfo = &g_led_priv_info[channel];

    pinfo->cur_state = g_led_state_table[pinfo->cur_state][action];
    //log_bsp_led_info("[bsp][led][isink] led process:led %x, cur_sta:%d, next_action:%d\r\n", 3, channel, pinfo->cur_state, action);
    switch ( pinfo->cur_state ) {
        case BSP_LED_STATE_IDLE:{
            log_bsp_led_info("[bsp][led][isink] led%d process: will in idle\r\n" , 1, channel);
            hal_isink_stop(channel);
            start_timer_continue = 0xFFFFFFFF;
            pinfo->next_action = BSP_LED_ACTION_MAX;
        }break;

        case BSP_LED_STATE_T0:{
           log_bsp_led_info("[bsp][led][isink] led%d process: start t0\r\n" , 1, channel);
           start_timer_continue = BSP_LED_T0_TO_MS(channel) + pinfo->led_cfg.sync_time;
           pinfo->next_action = BSP_LED_ACTION_OF_TIMEOUT;
#ifdef BSP_LED_SW_CALI_FEATURE
           bsp_led_sw_cali_init(channel,BSP_LED_T0_TO_MS(channel) + pinfo->led_cfg.sync_time);
#endif
        }break;

        case BSP_LED_STATE_T1T2_RPT: {
            log_bsp_led_info("[bsp][led][isink] led%d process: start t1t2 repeat\r\n", 1, channel);
            start_timer_continue = 0xFFFFFFFF;
            if(BSP_LED_T2_TO_MS(channel) == 0){  //T2 =0
               log_bsp_led_info("[bsp][led][isink] led process: always on\r\n" , 0);
            } else if(pinfo->led_cfg.repeat_t1t2 == 0xFFFF) { //T1T2 repeat == 0xFFFF
#ifdef BSP_LED_SW_CALI_FEATURE
                bsp_led_sw_cali(channel, BSP_T1_T2_REPEAT_CALI_TM);
                start_timer_continue = BSP_T1_T2_REPEAT_CALI_TM;//((BSP_T1_T2_REPEAT_CALI_TM/pinfo->t1t2_cycle) + 1) * pinfo->t1t2_cycle;
                pinfo->next_action        = BSP_LED_ACTION_OF_CALI_TIMEOUT;
                hal_isink_stop(channel);
#endif
                log_bsp_led_info("[bsp][led][isink] led process: t1t2 always repeat\r\n" , 0);
                hal_isink_start(channel);
            } else {
#ifdef BSP_LED_SW_CALI_FEATURE
                //isink_hw_config(pinfo->led_id, &pinfo->led_cfg, pinfo->sw_cali.led_sw_cali_coefficient);
                bsp_led_sw_cali(channel, BSP_LED_EXT_LOOP_MS(channel));
#endif
                start_timer_continue = BSP_LED_T1_T2_WITH_REPEAT_TO_MS(channel);
                pinfo->next_action = BSP_LED_ACTION_OF_TIMEOUT;
                //process ext loop
                if(pinfo->led_cfg.repeat_ext != 0xFFFF){
                    pinfo->led_cfg.repeat_ext--;
                    if( pinfo->led_cfg.repeat_ext ==0){
                        bsp_led_process(channel, BSP_LED_ACTION_OF_STOP);
                        log_bsp_led_info("[bsp][led][isink] led process: end ext loop\r\n" , 0);
                        if(pinfo->led_cfg.call_back != NULL){
                            pinfo->led_cfg.call_back(pinfo->led_id, pinfo->led_cfg.user_data);
                        }
                        return;
                    }
                }
            }
            hal_isink_start(channel);
        }break;

        case BSP_LED_STATE_T3: {
            log_bsp_led_info("[bsp][led][isink] led%d process: start t3\r\n" , 1, channel);
            hal_isink_stop(channel);
            start_timer_continue = BSP_LED_T3_TO_MS(channel);
            pinfo->next_action = BSP_LED_ACTION_OF_TIMEOUT;
        }break;

        default:{
            hal_isink_stop(channel);
            start_timer_continue = 0xFFFFFFFF;
            log_bsp_led_info("[bsp][led][isink] led%d process: error state %d", 2, channel, pinfo->cur_state);
        }break;
    }
    // whether start or stop timer
    if(start_timer_continue != 0xFFFFFFFF){
        hal_gpt_status_t gpt_status;
#ifdef BSP_LED_SW_CALI_FEATURE
        if(pinfo->sw_cali.led_sw_cali_coefficient != 0){
           temp_timer_ms = (start_timer_continue * pinfo->sw_cali.led_sw_cali_coefficient) / 100;
        }
#else
        temp_timer_ms = start_timer_continue;
#endif
        log_bsp_led_info("[bsp][led][isink] led%d process: start time %d ms, after cali %d ms, coef %d", 4, channel, start_timer_continue, temp_timer_ms, pinfo->sw_cali.led_sw_cali_coefficient);
        gpt_status = hal_gpt_sw_start_timer_ms(pinfo->gpt_handle,temp_timer_ms, bsp_led_gpt_callback, (void*)pinfo);
        if(gpt_status != HAL_GPT_STATUS_OK){
           log_bsp_led_info("[bsp][led][isink] led process: start sw time handle(0x%x) fail %d", 2, pinfo->gpt_handle, gpt_status);
        }
    }
    else{
    	hal_gpt_status_t gpt_status;
        gpt_status = hal_gpt_sw_stop_timer_ms(pinfo->gpt_handle);
        if(gpt_status != HAL_GPT_STATUS_OK){
           log_bsp_led_info("[bsp][led][isink] led process: stop sw time handle(0x%x) fail %d", 2, pinfo->gpt_handle, gpt_status);
        }
    }
}


/////////////////////////////////////////public function//////////////////////////////////////////////
bsp_led_status_t    bsp_led_isink_init(uint8_t ledx, bsp_led_mode_t mode)
{
    bsp_isink_private_info_t    *pinfo = &g_led_priv_info[ledx];
    hal_gpt_status_t             gpt_status;

    log_bsp_led_info("[bsp][led][isink] led%d init", 1, ledx);
    if(ledx >= BSP_LED_CHANNEL_MAX){
        log_bsp_led_error("[bsp][led][isink] led %d init fail, chnl err\r\n ",1, ledx);
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    memset(pinfo, 0, sizeof(bsp_isink_private_info_t));
    pinfo->led_id = ledx;
    pinfo->sw_cali.led_sw_cali_coefficient = 100;
    pinfo->mode = mode;
    gpt_status  = hal_gpt_sw_get_timer(&pinfo->gpt_handle);
    if ( HAL_GPT_STATUS_OK !=  gpt_status) {
        log_bsp_led_error("[bsp][led][isink] led%d init fail, get sw timer failed(%d).\r\n", 2, ledx, gpt_status);
        return BSP_LED_STATUS_ERROR;
    }
    log_bsp_led_info("[bsp][led][isink] led%d sw gpt get handle(0x%x)", 2, ledx, pinfo->gpt_handle);
    hal_isink_init(ledx);
    return BSP_LED_STATUS_OK;
}


bsp_led_status_t    bsp_led_isink_deinit(uint8_t ledx)
{
    bsp_isink_private_info_t    *pinfo = NULL;

    log_bsp_led_info("[bsp][led][isink] led%d deinit", 1, ledx);
    if(ledx >= BSP_LED_CHANNEL_MAX){
        log_bsp_led_error("[bsp][led][isink] led%d deinit fail, chnl err\r\n ",1, ledx);
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    pinfo = &g_led_priv_info[ledx];
    pinfo->led_id = ledx;
    log_bsp_led_info("[bsp][led][isink] led%d sw gpt free handle(0x%x)", 2, ledx, pinfo->gpt_handle);
    hal_gpt_sw_free_timer(pinfo->gpt_handle);
    pinfo->gpt_handle = 0;
    return BSP_LED_STATUS_OK;
}


bsp_led_status_t    bsp_led_isink_config(uint8_t ledx, bsp_led_config_t *cfg)
{
    bsp_isink_private_info_t    *pinfo =  &g_led_priv_info[ledx];
    log_bsp_led_info("[bsp][led][isink] led%d config\r\n", 1, ledx);
    if(ledx >= BSP_LED_CHANNEL_MAX){
        log_bsp_led_error("[bsp][led][isink] led%d config fail, chnl error\r\n ",1, ledx);
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    if(cfg == NULL) {
        log_bsp_led_error("[bsp][led][isink] led%d config fail, para is null\r\n ",1, ledx);
        return BSP_LED_STATUS_ERROR_INVALID_PARAMETER;
    }
    pinfo->led_cfg = *cfg;
    if(BSP_LED_T1_TO_MS(ledx) == 0){
        log_bsp_led_error("[bsp][led][isink] led %d config fail, t1 == 0\r\n ",1, ledx);
        return BSP_LED_STATUS_OK;
    }
    if(cfg->repeat_t1t2 == 0){
        cfg->repeat_t1t2 = 0xFFFF;
    }
    if(cfg->repeat_ext == 0) {
        cfg->repeat_ext  = 0xFFFF;
    }
    isink_hw_config(ledx, cfg, 100);
    return BSP_LED_STATUS_OK;
}

bsp_led_status_t    bsp_led_isink_start( uint8_t ledx)
{
    bsp_isink_private_info_t    *pinfo =  &g_led_priv_info[ledx];

    log_bsp_led_info("[bsp][led][isink] led%d isink start\r\n", 1, ledx);
    if(ledx >= BSP_LED_CHANNEL_MAX){
        log_bsp_led_error("[bsp][led][isink] led%d start fail, chnl err\r\n",1, ledx);
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    if(BSP_LED_T1_TO_MS(ledx) == 0){
        log_bsp_led_error("[bsp][led][isink] led%d no start, t1 == 0\r\n ",1, ledx);
        return BSP_LED_STATUS_OK;
    }
    pinfo->cur_state = BSP_LED_STATE_IDLE;
    bsp_led_process(ledx, BSP_LED_ACTION_OF_START);
    return BSP_LED_STATUS_OK;
}

bsp_led_status_t    bsp_led_isink_stop( uint8_t ledx)
{
    log_bsp_led_info("[bsp][led][isink] led%d stop\r\n", 1, ledx);
    if(BSP_LED_T1_TO_MS(ledx) == 0){
        log_bsp_led_error("[bsp][led][isink] led%d no stop, t1 == 0\r\n ",1, ledx);
        return BSP_LED_STATUS_OK;
    }
    bsp_led_process(ledx, BSP_LED_ACTION_OF_STOP);
    return BSP_LED_STATUS_OK;
}

int                 bsp_led_isink_ioctl(uint32_t ledx, uint32_t cmd, uint32_t option)
{
    return 0;
}
#endif
