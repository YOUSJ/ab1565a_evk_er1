/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __BSP_LED_H__
#define __BSP_LED_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hal.h"
#include "hal_isink.h"

#define     LED_ISINK_PWM_MODE_ENABLE /*ENABLE ISINK PWM MODE, Default is BREATH MODE*/

#define log_bsp_led_info(fmt, cnt, ...)  LOG_MSGID_I(bsp_led_test, fmt, cnt, ##__VA_ARGS__)
#define log_bsp_led_warn(fmt, cnt, ...)  LOG_MSGID_W(bsp_led_test, fmt, cnt, ##__VA_ARGS__)
#define log_bsp_led_error(fmt, cnt, ...) LOG_MSGID_E(bsp_led_test, fmt, cnt, ##__VA_ARGS__)

typedef enum {
    BSP_LED_CHANNEL_0 = 0,
    BSP_LED_CHANNEL_1,
    BSP_LED_CHANNEL_MAX,
} bsp_led_channel_t;

typedef void (*bsp_led_callback_t)(bsp_led_channel_t chnl, void *userdata);

typedef enum {
    BSP_LED_BLINK_MODE = 0,
    BSP_LED_BREATH_MODE
}bsp_led_mode_t;


#pragma  pack(1)
typedef struct {
    uint8_t     time_unit; //t0~t4 time unit
    uint8_t     t0;
    uint8_t     t1;
    uint8_t     t2;
    uint8_t     t3;
    uint16_t    repeat_t1t2;  /*t0t1  repeat times*/
    uint16_t    repeat_ext;   /*t0~t3 repeat times*/
    uint8_t     brightness;   /*led brightness*/
    uint8_t     onoff;        /*led on off(1:on,0:off)*/
    uint32_t    sync_time;
    void        *user_data;
    bsp_led_callback_t      call_back;
}bsp_led_config_t;
#pragma  pack()


/** @brief ISINK status. */
typedef enum {
    BSP_LED_STATUS_ERROR_NO_DEVICE     = -4,
    BSP_LED_STATUS_ERROR_CHANNEL       = -3,       /**< The ISINK error channel. */
    BSP_LED_STATUS_ERROR_INVALID_PARAMETER   = -2, /**< An error occurred, invalid parameter was given. */
    BSP_LED_STATUS_ERROR               = -1,       /**< The ISINK function error occurred. */
    BSP_LED_STATUS_OK   = 0                       /**< The ISINK operation completed successfully.*/
} bsp_led_status_t;


bsp_led_status_t    bsp_led_init(uint8_t ledx, bsp_led_mode_t mode);
bsp_led_status_t    bsp_led_deinit(uint8_t ledx);
bsp_led_status_t    bsp_led_config(uint8_t ledx, bsp_led_config_t* cfg);
bsp_led_status_t    bsp_led_start( uint8_t ledx);
bsp_led_status_t    bsp_led_stop( uint8_t ledx);
int                 bsp_led_ioctl(uint32_t ledx, uint32_t cmd, uint32_t option);

#ifdef __cplusplus
}
#endif

#endif



