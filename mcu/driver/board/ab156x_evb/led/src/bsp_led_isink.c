/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
#include "hal_platform.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hal.h"
#include "bsp_led.h"
#include "hal_isink.h"
#ifdef HAL_ISINK_MODULE_ENABLED

/* ******************************************************************************
 *                          Breath mode for isink led
 *
 * *****************************************************************************
*/

#define     ISINK_BREATH_MODE_HW_GAP    104
enum {
    BSP_LED_STATE_IDLE,
    BSP_LED_STATE_REPEAT_TWINKLE,
    BSP_LED_STATE_IDLE_FOR_LOOP,
};

static bsp_led_private_info_t   g_led_cfg[HAL_ISINK_MAX_CHANNEL];
static  const uint16_t          g_led_isink_tm_tb[] = {123,338,523,707,926,1107,1291,1507,1691,1876,2091,2276,2460,2676,2860,3075};


static  uint32_t   led_isink_get_time_sel(uint32_t time,int mul)
{
    uint32_t i   = 0;
    uint32_t tmp = 0;
    uint32_t res = 0;
    uint32_t map_sz = 0;

    map_sz = sizeof(g_led_isink_tm_tb)/sizeof(uint16_t);
    tmp = abs(time - g_led_isink_tm_tb[0]*mul);
    for(i=1;i< map_sz;i++) {
        if( tmp < (res = abs(time - g_led_isink_tm_tb[i]*mul)) ) {
            return i-1;
        }
        tmp = res;
    }
    return i-1;
}

static bsp_led_status_t led_isink_breath_init(uint8_t ledx)
{
    if(ledx > HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    g_led_cfg[ledx].led_id = ledx;
    hal_isink_init(ledx);
    if (HAL_GPT_STATUS_OK != hal_gpt_sw_get_timer(&(g_led_cfg[ledx].gpt_handle))) {
       log_bsp_led_error("[bsp][led][isink] get sw timer failed.\r\n", 0);
       return BSP_LED_STATUS_ERROR;
    }
    return BSP_LED_STATUS_OK;
}

static bsp_led_status_t led_isink_breath_deinit(uint8_t ledx)
{
    if(ledx > HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    hal_gpt_sw_stop_timer_ms(g_led_cfg[ledx].gpt_handle);
    hal_gpt_sw_free_timer(g_led_cfg[ledx].gpt_handle);
    hal_isink_deinit(ledx);
    g_led_cfg[ledx].gpt_handle= 0;
    return BSP_LED_STATUS_OK;
}

static bsp_led_status_t led_isink_breath_config(uint8_t ledx, bsp_led_config_t* cfg)
{
    hal_isink_config_t      isink;
    uint32_t                tm_cyc = 0;

    if(ledx > HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    if(cfg == NULL){
        return BSP_LED_STATUS_ERROR_INVALID_PARAMETER;
    }
    isink.mode = HAL_ISINK_MODE_BREATH;
    isink.config.breath_mode.darker_to_lighter_time1 = led_isink_get_time_sel(cfg->cfg_timing.on_step_time/2, 1);
    isink.config.breath_mode.darker_to_lighter_time2 = led_isink_get_time_sel(cfg->cfg_timing.on_step_time/2, 1);
    isink.config.breath_mode.lightest_time           = led_isink_get_time_sel(cfg->cfg_timing.on_time, 1);
    isink.config.breath_mode.lighter_to_darker_time1 = led_isink_get_time_sel(cfg->cfg_timing.off_step_time/2, 1);
    isink.config.breath_mode.lighter_to_darker_time2 = led_isink_get_time_sel(cfg->cfg_timing.off_step_time/2, 1);
    isink.config.breath_mode.darkest_time            = led_isink_get_time_sel(cfg->cfg_timing.off_time, 2);
    hal_isink_configure(ledx, &isink);
    tm_cyc  =   g_led_isink_tm_tb[isink.config.breath_mode.darker_to_lighter_time1] + \
                g_led_isink_tm_tb[isink.config.breath_mode.darker_to_lighter_time2] + \
                g_led_isink_tm_tb[isink.config.breath_mode.lightest_time]*1 +         \
                g_led_isink_tm_tb[isink.config.breath_mode.lighter_to_darker_time1] + \
                g_led_isink_tm_tb[isink.config.breath_mode.lighter_to_darker_time2] + \
                g_led_isink_tm_tb[isink.config.breath_mode.darkest_time]*2;
    /* led bright */
    g_led_cfg[ledx].bright         = cfg->bright;
    /* cycle time of on->off */
    g_led_cfg[ledx].twk_cycle_time = tm_cyc - ISINK_BREATH_MODE_HW_GAP;
    /* repeat time */
    g_led_cfg[ledx].rpt_twk_times  = cfg->cfg_timing.repeat_cycle_times;
    /* delay time after next repeat */
    g_led_cfg[ledx].dly_tm_for_lp  = cfg->cfg_timing.idle_time_for_loop;
    /* extend loop time :(n*(t1+t2) + t3) */
    g_led_cfg[ledx].ext_cycle_time = (cfg->cfg_timing.repeat_cycle_times * tm_cyc) + \
                                     (cfg->cfg_timing.idle_time_for_loop);

    g_led_cfg[ledx].call_back      = cfg->call_back;
    g_led_cfg[ledx].user_data      = cfg->user_data;

    return BSP_LED_STATUS_OK;
}

static void led_isink_breath_timer_callback(void *argv)
{
    bsp_led_private_info_t  *info = (bsp_led_private_info_t *)argv;

    if(info == NULL){
        return;
    }
    if(info->state_machine == BSP_LED_STATE_REPEAT_TWINKLE){
        if(info->dly_tm_for_lp != 0 ) {
            hal_isink_stop(info->led_id);
            hal_gpt_sw_stop_timer_ms(info->gpt_handle);
            hal_gpt_sw_start_timer_ms(info->gpt_handle, info->dly_tm_for_lp,  \
                                      led_isink_breath_timer_callback, info);
            info->state_machine = BSP_LED_STATE_IDLE_FOR_LOOP;
        } else {
            uint32_t    tm_rpt = 0;
            tm_rpt = (info->twk_cycle_time * info->rpt_twk_times );

            hal_gpt_sw_start_timer_ms(info->gpt_handle, tm_rpt,  \
                                      led_isink_breath_timer_callback, info);
            if(info->call_back != NULL) {
                info->call_back(info->user_data);
            }
            info->state_machine = BSP_LED_STATE_REPEAT_TWINKLE;
        }
    } else if (info->state_machine == BSP_LED_STATE_IDLE_FOR_LOOP) {
        uint32_t    tm_rpt = 0;
        tm_rpt = (info->twk_cycle_time * info->rpt_twk_times );
        hal_isink_start(info->led_id);
        hal_gpt_sw_stop_timer_ms(info->gpt_handle);
        hal_gpt_sw_start_timer_ms(info->gpt_handle, tm_rpt,  \
                                  led_isink_breath_timer_callback, info);
        if(info->call_back != NULL) {
            info->call_back(info->user_data);
        }
        info->state_machine = BSP_LED_STATE_REPEAT_TWINKLE;
    }
}

static bsp_led_status_t led_isink_breath_start(uint8_t ledx)
{
    uint32_t                tm_rpt     = 0;

    if(ledx > HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    hal_isink_start(ledx);
    hal_gpt_sw_stop_timer_ms(g_led_cfg[ledx].gpt_handle);
    if(g_led_cfg[ledx].rpt_twk_times != 0) {
        g_led_cfg[ledx].state_machine = BSP_LED_STATE_REPEAT_TWINKLE;
        tm_rpt = g_led_cfg[ledx].twk_cycle_time * g_led_cfg[ledx].rpt_twk_times;
        hal_gpt_sw_start_timer_ms(g_led_cfg[ledx].gpt_handle, tm_rpt,  \
                                  led_isink_breath_timer_callback, &g_led_cfg[ledx]);
    }
    return BSP_LED_STATUS_OK;
}

static bsp_led_status_t led_isink_breath_stop(uint8_t ledx)
{
    if(ledx > HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    hal_gpt_sw_stop_timer_ms(g_led_cfg[ledx].gpt_handle);
    g_led_cfg[ledx].state_machine = BSP_LED_STATE_IDLE;
    g_led_cfg[ledx].state_machine = BSP_LED_STATE_REPEAT_TWINKLE;
    hal_isink_stop(ledx);

    return BSP_LED_STATUS_OK;
}

/* ******************************************************************************
 *                          PWM mode for isink led
 *
 * *****************************************************************************/
static bsp_led_status_t led_isink_pwm_config(uint8_t ledx, bsp_led_config_t* cfg)
{
    bsp_led_timing_config_t *cfg_tm;
    uint32_t                tm_cyc = 0;
    uint32_t                temp   = 0;
    hal_isink_config_t      isink;

    if(ledx > HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    if(cfg == NULL){
        return BSP_LED_STATUS_ERROR_INVALID_PARAMETER;
    }
    cfg_tm = &cfg->cfg_timing;
    tm_cyc = cfg->cfg_timing.on_step_time  + cfg->cfg_timing.on_time + \
             cfg->cfg_timing.off_step_time + cfg->cfg_timing.off_time;

    if(cfg_tm->on_time == 0){
        g_led_cfg[ledx].op_state |= BSP_ISINK_LED_STATE_ALWAYS_ON_MASK;
    } else if(cfg_tm->off_time == 0) {
        g_led_cfg[ledx].op_state |= BSP_ISINK_LED_STATE_ALWAYS_OFF_MASK;
    }
    g_led_cfg[ledx].bright         = cfg->bright;
    g_led_cfg[ledx].twk_cycle_time = tm_cyc;  /* cycle time of on->off */
    g_led_cfg[ledx].dly_tm_for_lp  = cfg->cfg_timing.idle_time_for_loop; /* delay time after next repeat */
    g_led_cfg[ledx].rpt_twk_times  = cfg->cfg_timing.repeat_cycle_times; /* repeat time */
    g_led_cfg[ledx].call_back      = cfg->call_back;  /* callback */
    g_led_cfg[ledx].user_data      = cfg->user_data;
    /* extend loop time :(n*(t1+t2) + t3) */
    temp =(cfg->cfg_timing.idle_time_for_loop)/tm_cyc;
    if(cfg->cfg_timing.idle_time_for_loop != 0) {
        temp = (temp == 0)?1:temp;
    }
    g_led_cfg[ledx].ext_cycle_time = (cfg->cfg_timing.repeat_cycle_times + temp) * tm_cyc;
    isink.mode = HAL_ISINK_MODE_PWM;
    isink.config.pwm_mode.hi_level_time = cfg_tm->on_step_time  + cfg_tm->on_time;
    isink.config.pwm_mode.lo_level_time = cfg_tm->off_step_time + cfg_tm->off_time;
    isink.config.pwm_mode.blink_nums    = cfg_tm->repeat_cycle_times;
    isink.config.pwm_mode.idle_time     = cfg_tm->idle_time_for_loop;
    hal_isink_configure(ledx, &isink);
    return BSP_LED_STATUS_OK;
}

static bsp_led_status_t led_isink_pwm_init(uint8_t ledx)
{
    if(ledx > HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    g_led_cfg[ledx].led_id = ledx;
    hal_isink_init(ledx);
    if (HAL_GPT_STATUS_OK != hal_gpt_sw_get_timer(&(g_led_cfg[ledx].gpt_handle))) {
       log_bsp_led_error("[bsp][led][isink] get sw timer failed.\r\n", 0);
       return BSP_LED_STATUS_ERROR;
    }
    return BSP_LED_STATUS_OK;
}

static bsp_led_status_t led_isink_pwm_deinit(uint8_t ledx)
{
    if(ledx > HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    hal_isink_deinit(ledx);
    hal_gpt_sw_stop_timer_ms(g_led_cfg[ledx].gpt_handle);
    hal_gpt_sw_free_timer(g_led_cfg[ledx].gpt_handle);
    g_led_cfg[ledx].gpt_handle = 0;
    return BSP_LED_STATUS_OK;
}

static  void    led_isink_pwm_timer_callback(void *argv)
{
    bsp_led_private_info_t  *info = (bsp_led_private_info_t *)argv;

    hal_gpt_sw_stop_timer_ms(info->gpt_handle);
    hal_gpt_sw_start_timer_ms(info->gpt_handle, info->ext_cycle_time, led_isink_pwm_timer_callback, info);
    if(info->call_back != NULL) {
        info->call_back(info->user_data);
    }
}


static bsp_led_status_t led_isink_pwm_start(uint8_t ledx)
{
    if(ledx > HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    hal_gpt_sw_stop_timer_ms(g_led_cfg[ledx].gpt_handle);
    if(g_led_cfg[ledx].rpt_twk_times != 0) {
        hal_gpt_sw_start_timer_ms(g_led_cfg[ledx].gpt_handle, g_led_cfg[ledx].ext_cycle_time,
                led_isink_pwm_timer_callback, &g_led_cfg[ledx]);
    }
    hal_isink_start(ledx);
    return BSP_LED_STATUS_OK;
}

static bsp_led_status_t led_isink_pwm_stop(uint8_t ledx)
{
    if(ledx > HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    hal_gpt_sw_stop_timer_ms(g_led_cfg[ledx].gpt_handle);
    hal_isink_stop(ledx);
    return BSP_LED_STATUS_OK;
}


/* ******************************************************************************
 *                          PWM mode for isink led
 *
 * *****************************************************************************/

static bsp_led_status_t led_isink_register_start(uint8_t ledx, uint8_t bright)
{
    hal_isink_config_t  isink;

    isink.mode = HAL_ISINK_MODE_REGISTER;
    isink.config.register_mode.current = bright/51;
    isink.config.register_mode.enable_double = false;
    hal_isink_configure(ledx, &isink);
    return BSP_LED_STATUS_OK;
}



/* ******************************************************************************
 *                     Public API For Bsp LED Driver
 *
 * *****************************************************************************/
static bsp_led_operate_t    g_led_operator_table[] = {
    {
        .func_init   = led_isink_breath_init,
        .func_deinit = led_isink_breath_deinit,
        .func_config = led_isink_breath_config,
        .func_start  = led_isink_breath_start,
        .func_stop   = led_isink_breath_stop
    },
    {
        .func_init   = led_isink_pwm_init,
        .func_deinit = led_isink_pwm_deinit,
        .func_config = led_isink_pwm_config,
        .func_start  = led_isink_pwm_start,
        .func_stop   = led_isink_pwm_stop
    }
};


bsp_led_status_t    bsp_led_isink_init(uint8_t ledx, bsp_led_mode_t mode)
{
    bsp_led_status_t        status;
    bsp_led_private_info_t  *info;

    if(ledx >= HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    info = &g_led_cfg[ledx];
    log_bsp_led_info("[bsp][led] led %d init\r\n", ledx);
    info->op_state = (BSP_ISINK_LED_STATE_INIT_MASK | BSP_ISINK_LED_STATE_KEY_MASK);
    switch(mode) {
        case BSP_LED_BREATH_MODE: mode = 0; break;
        case BSP_LED_BLINK_MODE:  mode = 1; break;
        default:                  mode = 1; break;
    }
    info->led_interface = &g_led_operator_table[mode];
    if(info->led_interface->func_init != NULL) {
        status = info->led_interface->func_init(ledx);
        if(status != BSP_LED_STATUS_OK){
            log_bsp_led_error("[bsp][led][isink] init led failed:%d\r\n", 1, status);
        }
    }
    return status;
}

bsp_led_status_t    bsp_led_isink_deinit(uint8_t ledx)
{
    bsp_led_status_t        status;
    bsp_led_private_info_t  *info;

    if(ledx >= HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    info = &g_led_cfg[ledx];
    if((info->op_state & BSP_ISINK_LED_STATE_INIT_MASK) == 0){
        return BSP_LED_STATUS_ERROR;
    }
    log_bsp_led_info("[bsp][led] led %d deinit\r\n", ledx);
    info->op_state = 0;
    if(info->led_interface->func_deinit != NULL) {
        status = info->led_interface->func_deinit(ledx);
        if(status != BSP_LED_STATUS_OK){
            log_bsp_led_error("[bsp][led][isink] deinit led failed:%d\r\n", 1, status);
        }
    }
    return status;
}


bsp_led_status_t  bsp_led_isink_config(uint8_t ledx, bsp_led_config_t* cfg)
{
    bsp_led_status_t        status = BSP_LED_STATUS_OK;
    bsp_led_private_info_t  *info;

    if (ledx >= HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    info = &g_led_cfg[ledx];
    if((info->op_state & BSP_ISINK_LED_STATE_INIT_MASK) == 0){
        return BSP_LED_STATUS_ERROR;
    }
    if (cfg == NULL) {
        return BSP_LED_STATUS_ERROR_INVALID_PARAMETER;
    }
    log_bsp_led_info("[bsp][led] led %d config\r\n", ledx);
    if (cfg->cfg_timing.on_time == 0) {
        g_led_cfg[ledx].op_state = BSP_ISINK_LED_STATE_ALWAYS_ON_MASK;
        g_led_cfg[ledx].bright   = cfg->bright;
        log_bsp_led_error("[bsp][led][isink] cfg led to always on\r\n",0);
    } else if (cfg->cfg_timing.off_time == 0) {
        g_led_cfg[ledx].op_state = BSP_ISINK_LED_STATE_ALWAYS_OFF_MASK;
        log_bsp_led_error("[bsp][led][isink] cfg led to always off\r\n",0);
    } else {
        if(g_led_cfg[ledx].led_interface->func_config != NULL) {
            status = g_led_cfg[ledx].led_interface->func_config(ledx, cfg);
            if (status != BSP_LED_STATUS_OK){
                log_bsp_led_error("[bsp][led][isink] config led failed:%d\r\n", 1, status);
            }
        }
    }
    return status;
}


bsp_led_status_t  bsp_led_isink_start( uint8_t ledx)
{
    bsp_led_status_t        status = BSP_LED_STATUS_OK;
    bsp_led_private_info_t  *info;
    if(ledx >= HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    info = &g_led_cfg[ledx];
    if((info->op_state & BSP_ISINK_LED_STATE_INIT_MASK) == 0){
        return BSP_LED_STATUS_ERROR;
    }
    if(g_led_cfg[ledx].op_state & BSP_ISINK_LED_STATE_STARTED_MASK){
        return BSP_LED_STATUS_ERROR_BUSY;
    }
    log_bsp_led_info("[bsp][led] led %d start\r\n", ledx);
    g_led_cfg[ledx].op_state |= BSP_ISINK_LED_STATE_STARTED_MASK;
    if(g_led_cfg[ledx].op_state & BSP_ISINK_LED_STATE_ALWAYS_ON_MASK){
        led_isink_register_start(ledx, g_led_cfg[ledx].bright/51);
    } else if (g_led_cfg[ledx].op_state & BSP_ISINK_LED_STATE_ALWAYS_OFF_MASK) {
        return BSP_LED_STATUS_OK;
    } else {
        if(g_led_cfg[ledx].led_interface->func_start != NULL) {
            status = g_led_cfg[ledx].led_interface->func_start(ledx);
            if(status != BSP_LED_STATUS_OK){
                log_bsp_led_error("[bsp][led][isink] start led failed\r\n", 1, status);
            }
        }
    }
    return status;
}


bsp_led_status_t  bsp_led_isink_stop( uint8_t ledx)
{
    bsp_led_status_t status;
    bsp_led_private_info_t  *info;

    if(ledx >= HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    info = &g_led_cfg[ledx];
    if((info->op_state & BSP_ISINK_LED_STATE_INIT_MASK) == 0){
        return BSP_LED_STATUS_ERROR;
    }
    log_bsp_led_info("[bsp][led] led %d stop\r\n", ledx);
    g_led_cfg[ledx].op_state &= ~BSP_ISINK_LED_STATE_STARTED_MASK;
    if(g_led_cfg[ledx].led_interface->func_stop != NULL) {
        status = g_led_cfg[ledx].led_interface->func_stop(ledx);
        if(status != BSP_LED_STATUS_OK){
            log_bsp_led_error("[bsp][led][isink] stop led failed:%d\r\n", 1, status);
        }
    }
    return status;
}


int     bsp_led_isink_ioctrl(uint32_t ledx, uint32_t cmd, uint32_t option)
{
    int  temp = -1;

    if(ledx >= HAL_ISINK_MAX_CHANNEL){
        return BSP_LED_STATUS_ERROR_CHANNEL;
    }
    switch(cmd){
        case BSP_LED_IOCTRL_GET_CYCLE:{
            if(ledx < HAL_ISINK_MAX_CHANNEL){
                temp = g_led_cfg[ledx].twk_cycle_time;
            }
        }break;
        case BSP_LED_IOCTRL_GET_EXT_CYCLE:{
            if(ledx < HAL_ISINK_MAX_CHANNEL){
                temp = g_led_cfg[ledx].ext_cycle_time;
            }
        }break;

        default:    break;
    }
    return temp;
}

#endif
