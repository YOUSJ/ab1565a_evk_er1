
#ifndef __BSP_LED_ISINK_H__
#define __BSP_LED_ISINK_H__

#ifdef __cplusplus
extern "C" {
#endif
#include "hal_platform.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hal.h"
#include "bsp_led.h"

#ifdef HAL_ISINK_MODULE_ENABLED
bsp_led_status_t	bsp_led_isink_init(uint8_t ledx, bsp_led_mode_t mode);
bsp_led_status_t     bsp_led_isink_deinit(uint8_t ledx);
bsp_led_status_t  	bsp_led_isink_config(uint8_t ledx, bsp_led_config_t* cfg);
bsp_led_status_t  	bsp_led_isink_start( uint8_t ledx);
bsp_led_status_t  	bsp_led_isink_stop( uint8_t ledx);
int		       bsp_led_isink_ioctrl(uint32_t ledx, uint32_t cmd, uint32_t option);
#endif

#ifdef __cplusplus
}
#endif

#endif



