/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __BSP_LED_H__
#define __BSP_LED_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hal.h"
#include "hal_isink.h"

#define log_bsp_led_info(fmt, cnt, ...)  LOG_MSGID_I(bsp_led, fmt, cnt, ##__VA_ARGS__)
#define log_bsp_led_warn(fmt, cnt, ...)  LOG_MSGID_W(bsp_led, fmt, cnt, ##__VA_ARGS__)
#define log_bsp_led_error(fmt, cnt, ...) LOG_MSGID_E(bsp_led, fmt, cnt, ##__VA_ARGS__)

enum {
	BSP_LED_IOCTRL_GET_CYCLE,
	BSP_LED_IOCTRL_GET_EXT_CYCLE
};

typedef void (*bsp_led_callback_t)(void *);

typedef enum {
	BSP_LED_STATUS_ERROR_BUSY		   = -5,
    BSP_LED_STATUS_ERROR_NO_DEVICE     = -4,
    BSP_LED_STATUS_ERROR_CHANNEL       = -3,       /**< The ISINK error channel. */
    BSP_LED_STATUS_ERROR_INVALID_PARAMETER   = -2, /**< An error occurred, invalid parameter was given. */
    BSP_LED_STATUS_ERROR               = -1,		/**< The ISINK function error occurred. */
    BSP_LED_STATUS_OK   = 0                       	/**< The ISINK operation completed successfully.*/
} bsp_led_status_t;

typedef enum {
	BSP_LED_BREATH_MODE,
	BSP_LED_BLINK_MODE,
	BSP_LED_GPIO_MODE,
}bsp_led_mode_t;

typedef struct {
	uint32_t		on_time; //light duration time, unit ms(0 for always dark)
	uint32_t		off_time;//dark duration time,  unit ms(0 for always light)
	uint32_t		repeat_cycle_times;   //blink times,0 always twinkle
	/* only for breath mode */
	uint32_t		on_step_time; //dark ->dim duration time,unit ms
	uint32_t		off_step_time;//light->dim duration time,unit ms

	uint32_t		idle_time_for_loop;  //idle time after repeat cycle in extend cycle
}bsp_led_timing_config_t;

typedef struct{
	uint8_t			            bright;  	  //luminance,(0~255)
	bsp_led_timing_config_t     cfg_timing;   //led hw timing config
    bsp_led_callback_t          call_back;
    void   			            *user_data;
}bsp_led_config_t;

typedef bsp_led_status_t	(*func_led_init)(uint8_t   ledx);
typedef bsp_led_status_t	(*func_led_deinit)(uint8_t   ledx);
typedef bsp_led_status_t	(*func_led_config)(uint8_t ledx, bsp_led_config_t* cfg);
typedef bsp_led_status_t	(*func_led_start)(uint8_t ledx);
typedef bsp_led_status_t	(*func_led_stop) (uint8_t ledx);

typedef struct{
	func_led_init	func_init;
	func_led_deinit	func_deinit;
	func_led_config func_config;
	func_led_start	func_start;
	func_led_stop   func_stop;
}bsp_led_operate_t;

typedef struct {
	uint8_t				led_id;
	bsp_led_mode_t		mode;
	uint8_t				bright;
	uint8_t				state_machine;
	uint16_t		    op_state;
	uint32_t			gpt_handle;
	uint32_t			twk_cycle_time;
	uint32_t			ext_cycle_time;

	uint32_t			rpt_twk_times;
	uint32_t			dly_tm_for_lp;
    bsp_led_callback_t 	call_back;
    void   			  	*user_data;
    bsp_led_operate_t	*led_interface;
}bsp_led_private_info_t;

enum {
	BSP_ISINK_LED_STATE_INIT_MASK = 0x1,
	BSP_ISINK_LED_STATE_STARTED_MASK   = 0x2,
	BSP_ISINK_LED_STATE_ALWAYS_OFF_MASK= 0x4,
	BSP_ISINK_LED_STATE_ALWAYS_ON_MASK= 0x8,
	BSP_ISINK_LED_STATE_KEY_MASK	  = 0xEA00,
};


bsp_led_status_t	bsp_led_init(uint8_t ledx, bsp_led_mode_t mode);
bsp_led_status_t    bsp_led_deinit(uint8_t ledx);
bsp_led_status_t  	bsp_led_config(uint8_t ledx, bsp_led_config_t* cfg);
bsp_led_status_t  	bsp_led_start( uint8_t ledx);
bsp_led_status_t  	bsp_led_stop( uint8_t ledx);
int				  	bsp_led_ioctrl(uint32_t ledx, uint32_t cmd, uint32_t option);
#ifdef __cplusplus
}
#endif

#endif



