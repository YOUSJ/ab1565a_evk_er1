/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "nvkey.h"
#include "hal.h"
#include "nvkey_id_list.h"
#include "syslog.h"
#include "hal_pmu_cali_2565.h"
#include "hal_pmu_charger_2565.h"
#include "assert.h"
#include "hal_flash_disk_internal.h"
/*
#include "os_memory.h"
#include "os.h"
#include "drv_charger.h"
#include "hal_pmu.h"
#include "hal_pmu_platform.h"
*/

#define UNUSED(x)  ((void)(x))

extern void vPortFree(void*);
extern void *pvPortMalloc(size_t);


uint8_t gBuckLvDslp;
uint8_t OTP_flag = 0xFF;
uint8_t mpk_flag = 0xFF;
VBAT_SLOPE_CONFIG vbat_slope;
DAC_SLOPE_CONFIG dac_slope;
CURR_SLOPE_CONFIG chg_curr_4v35_slope;
CURR_SLOPE_CONFIG chg_curr_4v2_slope;
CHG_CC_CURR_CONFIG chg_cc_curr;
VICHG_ADC_CONFIG chg_vichg_adc;


////////////////////////////////////////////////////////////////////////////////
// Function Declarations ///////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/****************transfer function****************/
uint16_t range_protect(uint16_t val, uint16_t min, uint16_t max, uint8_t byte)
{
    uint16_t thrd = 0, result = 0;

    if (byte == 1)
        thrd = 0x80;
    else if (byte == 2)
        thrd = 0x8000;
    else
        assert(0);

    if (min & thrd || min > max)
        assert(0);

    if (val & thrd)
    {
        result = min;
    }
    else
    {
        if (val > max)
            result = max;
        else
            result = val;
    }

    return result;
}

int32_t mpk_round(int32_t val1, int32_t val2)
{
    int32_t result = 0;
    int32_t tmp = 0;

    if (val2 == 0)
        assert(0);

    result = val1/val2;
    tmp = (10*val1/val2)%10;

    if (tmp > 4)
      result++;
    else if (tmp < -4)
      result--;

    //log_hal_msgid_info("mpk_round val1[%d], val2[%d], tmp[%d], result[%d]", 4, val1, val2, tmp, result);

    return result;
}

uint16_t adc_calculation(uint16_t volt1, uint16_t adc1, uint16_t volt2, uint16_t adc2, uint16_t volt_val)
{
    uint16_t adc_val = 0;
    uint16_t volt_diff = 0;
    uint16_t adc_diff = 0;

    if (volt1 > volt2)
    {
        volt_diff = volt1 - volt2;
        adc_diff = adc1 - adc2;
    }
    else
    {
        volt_diff = volt2 - volt1;
        adc_diff = adc2 - adc1;
    }

    if (volt_val > volt2)
    {
        adc_val = adc2 + mpk_round((adc_diff * (volt_val - volt2)), volt_diff);
    }
    else if (volt_val < volt2)
    {
        adc_val = adc2 - mpk_round((adc_diff * (volt2 - volt_val)), volt_diff);
    }
    else
    {
        adc_val = adc2;
    }
    log_hal_msgid_info("adc_calculation V1[%d] A1[%d] V2[%d] A2[%d] VC[%d] AC[%d]", 6, volt1, adc1, volt2,adc2, volt_val, adc_val);

    return adc_val;
}

uint8_t vdig_bg_calculation(uint16_t volt1, uint8_t sel1, uint16_t volt2, uint8_t sel2, uint16_t volt_val)
{
    int8_t sel_val1 = 0;
    uint8_t sel_val2 = 0;

    if (sel1 > sel2)
    {
        sel_val1 = sel1 + mpk_round(((sel1-sel2-32)*(volt_val-volt1)), (volt1-volt2));
    }
    else
    {
        sel_val1 = sel1 + mpk_round(((sel1+32-sel2)*(volt_val-volt1)), (volt1-volt2));
    }

    if (sel_val1 < 0)
    {
        sel_val2 = sel_val1 + 32;
    }
    else if (sel_val1 > 31)
    {
        sel_val2 = sel_val1 - 32;
    }
    else
        sel_val2 = sel_val1;

    log_hal_msgid_info("vdig_bg_calculation, V1[%d] S1[%d] V2[%d] S2[%d] VC[%d] sel_val1[%d], sel_val2[%d]", 7, volt1, sel1, volt2, sel2, volt_val, sel_val1, sel_val2);

    return sel_val2;
}

void* get_nvkey_data(uint16_t nvkey_id)
{
    uint8_t* nvkey_ptr = NULL;
    uint32_t nvkey_size = 0;
    nvkey_status_t status = NVKEY_STATUS_ERROR;

    status = nvkey_data_item_length(nvkey_id, &nvkey_size);
    if (status || !nvkey_size)
    {
        log_hal_msgid_info("get_nvkey_data error, id[0x%x], status[%d], size[%d]", 3, nvkey_id, status, nvkey_size);
        return NULL;
    }
    nvkey_ptr = pvPortMalloc(nvkey_size);
    if (!nvkey_ptr)
    {
        log_hal_msgid_info("get_nvkey_data error, nvkey_ptr is NULL, id[0x%x]", 1, nvkey_id);
        return NULL;
    }
    status = nvkey_read_data(nvkey_id, nvkey_ptr, &nvkey_size);

    if(status)
    {
        log_hal_msgid_info("get_nvkey_data error, id[0x%x], status[%d]", 2, nvkey_id, status);
        vPortFree(nvkey_ptr);
        return NULL;
    }
    return (void*)nvkey_ptr;
}

/****************SLT function****************/
void mpk_vcore_config(uint16_t volt)
{
    uint8_t val = 0;

    BUCK_LV_CONFIG* pBUCK_LV = (BUCK_LV_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_BUCK_LV_CONFIG);

    if (!pBUCK_LV)
        return;

    if(pBUCK_LV->Kflag < FT_TOOL_K)
    {
        vPortFree(pBUCK_LV);
        return;
    }

    val = adc_calculation(pBUCK_LV->ADC1_S, pBUCK_LV->sloep_value1, pBUCK_LV->ADC2_S, pBUCK_LV->sloep_value2, volt);
    val = range_protect(val, 0, 255, 1);
    pmu_set_register_value(BUCK_VSEL_LV_ADDR, BUCK_VSEL_LV_MASK, BUCK_VSEL_LV_SHIFT, val);
    printf("mpk_vcore_config, SLT VCORE:[0.%dV] \r\n", volt);

    vPortFree(pBUCK_LV);
}

/*******************CHARGER********************/
void mpk_CHG_ADC_config(void)
{
    uint16_t ADCval;

    CHG_ADC_CONFIG* pCHG_ADC = (CHG_ADC_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_CHG_ADC_CONFIG);

    if(!pCHG_ADC)
        return;

    ADCval = pCHG_ADC->cc1_threshold_ADC + pCHG_ADC->cc1_thd_ADC_offset;
    pmu_set_register_value(CC1_THRESHOLD_ADDR, CC1_THRESHOLD_MASK, CC1_THRESHOLD_SHIFT, ADCval);

    ADCval = pCHG_ADC->cc2_threshold_ADC + pCHG_ADC->cc2_offset_ADC;
    pmu_set_register_value(CC2_THRESHOLD_ADDR, CC2_THRESHOLD_MASK, CC2_THRESHOLD_SHIFT, ADCval);

    ADCval = pCHG_ADC->cv_threshold_ADC+ pCHG_ADC->cv_thd_ADC_offset;
    pmu_set_register_value(CV_THRESHOLD_ADDR, CV_THRESHOLD_MASK, CV_THRESHOLD_SHIFT, ADCval);

    ADCval = pCHG_ADC->full_bat_ADC + pCHG_ADC->full_bat_ADC_offset;
    pmu_set_register_value(FULL_BAT_THRESHOLD1_ADDR, FULL_BAT_THRESHOLD1_MASK, FULL_BAT_THRESHOLD1_SHIFT, ADCval);

    ADCval = pCHG_ADC->recharge_ADC+ pCHG_ADC->recharge_ADC_offset;
    pmu_set_register_value(RECHARGE_THRESHOLD_ADDR, RECHARGE_THRESHOLD_MASK, RECHARGE_THRESHOLD_SHIFT, ADCval);

    ADCval = pCHG_ADC->full_bat_ADC_2 + pCHG_ADC->full_bat_ADC_2_offset;
    pmu_set_register_value(FULL_BAT_THRESHOLD2_ADDR, FULL_BAT_THRESHOLD2_MASK, FULL_BAT_THRESHOLD2_SHIFT, 0x3FF);
    pmu_set_register_value(CHG_COMPLETE_CHK_NUM2_ADDR, CHG_COMPLETE_CHK_NUM2_MASK, CHG_COMPLETE_CHK_NUM2_SHIFT, 0xF);

    vPortFree(pCHG_ADC);
}

void mpk_CHG_DAC_config(void)
{
    uint16_t ADCval;

    INT_CHG_DAC_CONFIG* pINT_CHG_ADC = (INT_CHG_DAC_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_INT_CHG_DAC_CONFIG);

    if(!pINT_CHG_ADC)
        return;

    ADCval = pINT_CHG_ADC->tricklecurrentDAC + pINT_CHG_ADC->tricklecurrentDACoffset;
    pmu_set_register_value(TRICKLE_DAC_VALUE_ADDR, TRICKLE_DAC_VALUE_MASK, TRICKLE_DAC_VALUE_SHIFT, ADCval);
    pmu_set_register_value(TRICKLE_DAC_OUT_UPDATE_ADDR, TRICKLE_DAC_OUT_UPDATE_MASK, TRICKLE_DAC_OUT_UPDATE_SHIFT, 1);

    ADCval = pINT_CHG_ADC->cc1currentDAC + pINT_CHG_ADC->cc1currentDACoffset;
    pmu_set_register_value(CC1_DAC_VALUE_ADDR, CC1_DAC_VALUE_MASK, CC1_DAC_VALUE_SHIFT, ADCval);
    pmu_set_register_value(CC1_DAC_OUT_UPDATE_ADDR, CC1_DAC_OUT_UPDATE_MASK, CC1_DAC_OUT_UPDATE_SHIFT, 1);

    ADCval = pINT_CHG_ADC->cc2currentDAC + pINT_CHG_ADC->cc2currentDACoffset;
    pmu_set_register_value(CC2_DAC_VALUE_ADDR, CC2_DAC_VALUE_MASK, CC2_DAC_VALUE_SHIFT, ADCval);
    pmu_set_register_value(CC2_DAC_OUT_UPDATE_ADDR, CC2_DAC_OUT_UPDATE_MASK, CC2_DAC_OUT_UPDATE_SHIFT, 1);

    ADCval = pINT_CHG_ADC->cvDAC + pINT_CHG_ADC->cvDACoffset;
    pmu_set_register_value(CV_DAC_VALUE_ADDR, CV_DAC_VALUE_MASK, CV_DAC_VALUE_SHIFT, ADCval);
    pmu_set_register_value(CV_DAC_OUT_UPDATE_ADDR, CV_DAC_OUT_UPDATE_MASK, CV_DAC_OUT_UPDATE_SHIFT, 1);

    vPortFree(pINT_CHG_ADC);
}

void mpk_CHG_trickle_current_config(void)
{
    uint16_t tmpval;

    INT_CHG_TRICKLE_CURRENT_CONFIG* pINTCHG_TRICURRCONFIG = (INT_CHG_TRICKLE_CURRENT_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_INT_CHG_TRICKLE_CURRENT_CONFIG);

    if(!pINTCHG_TRICURRCONFIG)
        return;

    if(pINTCHG_TRICURRCONFIG->CalCount < pINTCHG_TRICURRCONFIG->select)
    {
        vPortFree(pINTCHG_TRICURRCONFIG);
        return;
    }

    tmpval = pINTCHG_TRICURRCONFIG->data[pINTCHG_TRICURRCONFIG->select - 1].rchg_sel;
    pmu_set_register_value(TRICKLE_RCHG_SEL_ADDR, TRICKLE_RCHG_SEL_MASK, TRICKLE_RCHG_SEL_SHIFT, tmpval);
    pmu_set_register_value(TRICKLE_RCHG_SEL_UPDATE_ADDR, TRICKLE_RCHG_SEL_UPDATE_MASK, TRICKLE_RCHG_SEL_UPDATE_SHIFT, 1);

    vPortFree(pINTCHG_TRICURRCONFIG);
}

void mpk_CHG_CC1_current_config(void)
{
    uint16_t tmpval;

    INT_CHG_CC1_CURRENT_CONFIG* pINTCHG_CC1CURRCONFIG = (INT_CHG_CC1_CURRENT_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_INT_CHG_CC1_CURRENT_CONFIG);

    if(!pINTCHG_CC1CURRCONFIG)
        return;

    if(pINTCHG_CC1CURRCONFIG->CalCount < pINTCHG_CC1CURRCONFIG->select)
    {
        vPortFree(pINTCHG_CC1CURRCONFIG);
        return;
    }
    pmu_chg_info.cc1_curr = pINTCHG_CC1CURRCONFIG->data[pINTCHG_CC1CURRCONFIG->select - 1].current;

    tmpval = pINTCHG_CC1CURRCONFIG->data[pINTCHG_CC1CURRCONFIG->select - 1].rchg_sel;
    pmu_set_register_value(CC1_RCHG_SEL_ADDR, CC1_RCHG_SEL_MASK, CC1_RCHG_SEL_SHIFT, tmpval);
    pmu_set_register_value(CC1_RCHG_SEL_UPDATE_ADDR, CC1_RCHG_SEL_UPDATE_MASK, CC1_RCHG_SEL_UPDATE_SHIFT, 1);
    if (vbat_slope.two_step_sel == 0)
    {
        pmu_set_register_value(CV_RCHG_SEL_ADDR, CV_RCHG_SEL_MASK, CV_RCHG_SEL_SHIFT, tmpval);
        pmu_set_register_value(CV_RCHG_SEL_UPDATE_ADDR, CV_RCHG_SEL_UPDATE_MASK, CV_RCHG_SEL_UPDATE_SHIFT, 1);
    }

    vPortFree(pINTCHG_CC1CURRCONFIG);
}

void mpk_CHG_CC2_current_config(void)
{
    uint16_t tmpval;

    INT_CHG_CC2_CURRENT_CONFIG* pINTCHG_CC2CURRCONFIG = (INT_CHG_CC2_CURRENT_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_INT_CHG_CC2_CURRENT_CONFIG);

    if(!pINTCHG_CC2CURRCONFIG)
        return;

    if(pINTCHG_CC2CURRCONFIG->CalCount< pINTCHG_CC2CURRCONFIG->select)
    {
        vPortFree(pINTCHG_CC2CURRCONFIG);
        return;
    }
    pmu_chg_info.cc2_curr = pINTCHG_CC2CURRCONFIG->data[pINTCHG_CC2CURRCONFIG->select - 1].current;

    tmpval = pINTCHG_CC2CURRCONFIG->data[pINTCHG_CC2CURRCONFIG->select - 1].rchg_sel;
    pmu_set_register_value(CC2_RCHG_SEL_ADDR, CC2_RCHG_SEL_MASK, CC2_RCHG_SEL_SHIFT, tmpval);
    pmu_set_register_value(CC2_RCHG_SEL_UPDATE_ADDR, CC2_RCHG_SEL_UPDATE_MASK, CC2_RCHG_SEL_UPDATE_SHIFT, 1);
    if (vbat_slope.two_step_sel == 1)
    {
        pmu_set_register_value(CV_RCHG_SEL_ADDR, CV_RCHG_SEL_MASK, CV_RCHG_SEL_SHIFT, tmpval);
        pmu_set_register_value(CV_RCHG_SEL_UPDATE_ADDR, CV_RCHG_SEL_UPDATE_MASK, CV_RCHG_SEL_UPDATE_SHIFT, 1);
    }

    vPortFree(pINTCHG_CC2CURRCONFIG);
}

void mpk_chg_cv_stop_current_adc(void)
{
    uint16_t tmpval;

    CHG_CV_STOP_CURRENT_ADC* pCHG_CVSTOPCURRCONFIG = (CHG_CV_STOP_CURRENT_ADC*)get_nvkey_data(NVKEYID_MP_CAL_CV_STOP_CURRENT_ADC);

    if(!pCHG_CVSTOPCURRCONFIG)
        return;

    if(pCHG_CVSTOPCURRCONFIG->CV_stop_current_percent > 100)
    {
        vPortFree(pCHG_CVSTOPCURRCONFIG);
        return;
    }

    tmpval = pCHG_CVSTOPCURRCONFIG->CV_stop_current_ADC;
    pmu_set_register_value(CV_STOP_CURRENT_ADDR, CV_STOP_CURRENT_MASK, CV_STOP_CURRENT_SHIFT, tmpval);

    vPortFree(pCHG_CVSTOPCURRCONFIG);
}

void mpk_chg_sys_ldo(void)
{
    uint16_t tmpval;

    CHG_SYS_LDO* pCHG_SYS_LDO = (CHG_SYS_LDO*)get_nvkey_data(NVKEYID_MP_CAL_SYS_LDO);

    if(!pCHG_SYS_LDO)
        return;

    if(pCHG_SYS_LDO->SYSLDO_output_voltage > 5000)
    {
        vPortFree(pCHG_SYS_LDO);
        return;
    }

    tmpval = pCHG_SYS_LDO->CHG_LDO_SEL;
    pmu_set_register_value(CHG_LDO_SEL_ADDR, CHG_LDO_SEL_MASK, CHG_LDO_SEL_SHIFT, tmpval);

    vPortFree(pCHG_SYS_LDO);
}

void mpk_chg_ocp(void)
{
    CHG_OCP* pCHG_OCP = (CHG_OCP*)get_nvkey_data(NVKEYID_MP_CAL_OCP);

    if(!pCHG_OCP)
        return;

    pmu_set_register_value(SW_OC_LMT_ADDR, SW_OC_LMT_MASK, SW_OC_LMT_SHIFT, pCHG_OCP->SW_OC_LMT);
    pmu_set_register_value(I_LIM_TRIM_ADDR, I_LIM_TRIM_MASK, I_LIM_TRIM_SHIFT, pCHG_OCP->I_LIM_TRIM);

    vPortFree(pCHG_OCP);
}

/*void mpk_chg_jeita_wram(void)
{
    JEITA_WARM* pJEITA_WARM =(JEITA_WARM*)get_nvkey_data(NVKEYID_MP_CAL_JEITA_WARM);

    if(!pJEITA_WARM)
        return;

    pmu_set_register_value(CC2_THRESHOLD_ADDR, CC2_THRESHOLD_MASK, CC2_THRESHOLD_SHIFT, pJEITA_WARM->JEITA_warm_cc2_threshold_ADC);
    pmu_set_register_value(CV_THRESHOLD_ADDR, CV_THRESHOLD_MASK, CV_THRESHOLD_SHIFT, pJEITA_WARM->JEITA_warm_cv_threshold_ADC);
    pmu_set_register_value(FULL_BAT_THRESHOLD1_ADDR, FULL_BAT_THRESHOLD1_MASK, FULL_BAT_THRESHOLD1_SHIFT, pJEITA_WARM->JEITA_warm_full_bat_ADC);
    pmu_set_register_value(RECHARGE_THRESHOLD_ADDR, RECHARGE_THRESHOLD_MASK, RECHARGE_THRESHOLD_SHIFT, pJEITA_WARM->JEITA_warm_recharge_ADC);
    pmu_set_register_value(FULL_BAT_THRESHOLD2_ADDR, FULL_BAT_THRESHOLD2_MASK, FULL_BAT_THRESHOLD2_SHIFT, pJEITA_WARM->JEITA_warm_full_bat_ADC_2);
    pmu_set_register_value(CC1_DAC_VALUE_ADDR, CC1_DAC_VALUE_MASK, CC1_DAC_VALUE_SHIFT, pJEITA_WARM->JEITA_warm_cc1_current_DAC);
    pmu_set_register_value(CC1_DAC_OUT_UPDATE_ADDR, CC1_DAC_OUT_UPDATE_MASK, CC1_DAC_OUT_UPDATE_SHIFT, 1);
    pmu_set_register_value(CC2_DAC_VALUE_ADDR, CC2_DAC_VALUE_MASK, CC2_DAC_VALUE_SHIFT, pJEITA_WARM->JEITA_warm_cc2_current_DAC);
    pmu_set_register_value(CC2_DAC_OUT_UPDATE_ADDR, CC2_DAC_OUT_UPDATE_ADDR, CC2_DAC_OUT_UPDATE_ADDR, 1);
    pmu_set_register_value(CV_DAC_VALUE_ADDR, CV_DAC_VALUE_MASK, CV_DAC_VALUE_SHIFT, pJEITA_WARM->JEITA_warm_cv_DAC);
    pmu_set_register_value(CV_DAC_OUT_UPDATE_ADDR, CV_DAC_OUT_UPDATE_MASK, CV_DAC_OUT_UPDATE_SHIFT, 1);
    pmu_set_register_value(CC1_RCHG_SEL_ADDR, CC1_RCHG_SEL_MASK, CC1_RCHG_SEL_SHIFT, pJEITA_WARM->JEITA_warm_rchg_sel_cc1_1);
    pmu_set_register_value(CC1_RCHG_SEL_UPDATE_ADDR, CC1_RCHG_SEL_UPDATE_MASK, CC1_RCHG_SEL_UPDATE_SHIFT, 1);
    pmu_set_register_value(CC2_RCHG_SEL_ADDR, CC2_RCHG_SEL_MASK, CC2_RCHG_SEL_SHIFT, pJEITA_WARM->JEITA_warm_rchg_sel_cc2_1);
    pmu_set_register_value(CC2_RCHG_SEL_UPDATE_ADDR, CC2_RCHG_SEL_UPDATE_MASK, CC2_RCHG_SEL_UPDATE_SHIFT, 1);
    pmu_set_register_value(CV_STOP_CURRENT_ADDR, CV_STOP_CURRENT_MASK, CV_STOP_CURRENT_SHIFT, pJEITA_WARM->JEITA_warm_CV_stop_current_ADC);

    vPortFree(pJEITA_WARM);
}

void mpk_chg_jeita_cool(void)
{
    JEITA_COOL* pJEITA_COOL =(JEITA_COOL*)get_nvkey_data(NVKEYID_MP_CAL_JEITA_COOL);

    if(!pJEITA_COOL)
        return;

    pmu_set_register_value(CC2_THRESHOLD_ADDR, CC2_THRESHOLD_MASK, CC2_THRESHOLD_SHIFT, pJEITA_COOL->JEITA_cool_cc2_threshold_ADC);
    pmu_set_register_value(CV_THRESHOLD_ADDR, CV_THRESHOLD_MASK, CV_THRESHOLD_SHIFT, pJEITA_COOL->JEITA_cool_cv_threshold_ADC);
    pmu_set_register_value(FULL_BAT_THRESHOLD1_ADDR, FULL_BAT_THRESHOLD1_MASK, FULL_BAT_THRESHOLD1_SHIFT, pJEITA_COOL->JEITA_cool_full_bat_ADC);
    pmu_set_register_value(RECHARGE_THRESHOLD_ADDR, RECHARGE_THRESHOLD_MASK, RECHARGE_THRESHOLD_SHIFT, pJEITA_COOL->JEITA_cool_recharge_ADC);
    pmu_set_register_value(FULL_BAT_THRESHOLD2_ADDR, FULL_BAT_THRESHOLD2_MASK, FULL_BAT_THRESHOLD2_SHIFT, pJEITA_COOL->JEITA_cool_full_bat_ADC_2);
    pmu_set_register_value(CC1_DAC_VALUE_ADDR, CC1_DAC_VALUE_MASK, CC1_DAC_VALUE_SHIFT, pJEITA_COOL->JEITA_cool_cc1_current_DAC);
    pmu_set_register_value(CC1_DAC_OUT_UPDATE_ADDR, CC1_DAC_OUT_UPDATE_MASK, CC1_DAC_OUT_UPDATE_SHIFT, 1);
    pmu_set_register_value(CC2_DAC_VALUE_ADDR, CC2_DAC_VALUE_MASK, CC2_DAC_VALUE_SHIFT, pJEITA_COOL->JEITA_cool_cc2_current_DAC);
    pmu_set_register_value(CC2_DAC_OUT_UPDATE_ADDR, CC2_DAC_OUT_UPDATE_MASK, CC2_DAC_OUT_UPDATE_SHIFT, 1);
    pmu_set_register_value(CV_DAC_VALUE_ADDR, CV_DAC_VALUE_MASK, CV_DAC_VALUE_SHIFT, pJEITA_COOL->JEITA_cool_cv_DAC);
    pmu_set_register_value(CV_DAC_OUT_UPDATE_ADDR, CV_DAC_OUT_UPDATE_MASK, CV_DAC_OUT_UPDATE_SHIFT, 1);
    pmu_set_register_value(CC1_RCHG_SEL_ADDR, CC1_RCHG_SEL_MASK, CC1_RCHG_SEL_SHIFT, pJEITA_COOL->JEITA_cool_rchg_sel_cc1_1);
    pmu_set_register_value(CC1_RCHG_SEL_UPDATE_ADDR, CC1_RCHG_SEL_UPDATE_MASK, CC1_RCHG_SEL_UPDATE_SHIFT, 1);
    pmu_set_register_value(CC2_RCHG_SEL_ADDR, CC2_RCHG_SEL_MASK, CC2_RCHG_SEL_SHIFT, pJEITA_COOL->JEITA_cool_rchg_sel_cc2_1);
    pmu_set_register_value(CC2_RCHG_SEL_UPDATE_ADDR, CC2_RCHG_SEL_UPDATE_MASK, CC2_RCHG_SEL_UPDATE_SHIFT, 1);
    pmu_set_register_value(CV_STOP_CURRENT_ADDR, CV_STOP_CURRENT_MASK, CV_STOP_CURRENT_SHIFT, pJEITA_COOL->JEITA_cool_CV_stop_current_ADC);

    vPortFree(pJEITA_COOL);
}*/

void mpk_chg_config(void)
{
    CHG_CONFIG* pChg_config = (CHG_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_CHG_CONFIG);

    if(!pChg_config)
        return;

    if(pChg_config->Kflag == 0)
    {
        vPortFree(pChg_config);
        return;
    }

    if( pChg_config->two_step_sel)
    {
        pmu_set_register_value(CC2_EN_ADDR, CC2_EN_MASK, CC2_EN_SHIFT, 1);
    }
    vbat_slope.two_step_sel = pChg_config->two_step_sel;
    vPortFree(pChg_config);

    mpk_CHG_ADC_config();
    mpk_CHG_DAC_config();
    mpk_CHG_trickle_current_config();
    mpk_CHG_CC1_current_config();
    mpk_CHG_CC2_current_config();
    mpk_chg_cv_stop_current_adc();
    mpk_chg_sys_ldo();
    mpk_chg_ocp();
}

/*******************BUCK********************/
void mpk_buck_mv(void)
{
    uint16_t tmp_val;

    BUCK_MV_CONFIG* pBUCK_MV = (BUCK_MV_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_BUCK_MV_CONFIG);

    log_hal_msgid_info("mpk_buck_mv ptr[%d], CalCount[%d], volselect[%d], Kflag[%d]", 4, pBUCK_MV, pBUCK_MV->CalCount, pBUCK_MV->volselect, pBUCK_MV->Kflag);

    if(!pBUCK_MV)
        return;

    if (pBUCK_MV->CalCount < pBUCK_MV->volselect)
    {
        vPortFree(pBUCK_MV);
        return;
    }
    if((pBUCK_MV->Kflag == FT_TOOL_K)||(pBUCK_MV->Kflag == FT_FW_K))
    {
        tmp_val = pBUCK_MV->data[pBUCK_MV->volselect - 1].value;
        pmu_set_register_value(BUCK_VSEL_MV_ADDR, BUCK_VSEL_MV_MASK, BUCK_VSEL_MV_SHIFT, tmp_val);
        log_hal_msgid_info("mpk_buck_mv, tmp_val[%d]", 1, tmp_val);
    }
    vPortFree(pBUCK_MV);
}

void mpk_buck_mv_stb(void)
{
    uint16_t tmp_val;

    BUCK_MV_STB_CONFIG* pBUCK_MV_STB = (BUCK_MV_STB_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_BUCK_MV_STB_CONFIG);

    if(!pBUCK_MV_STB)
        return;

    if (pBUCK_MV_STB->CalCount < pBUCK_MV_STB->volselect)
    {
        vPortFree(pBUCK_MV_STB);
        return;
    }
    if((pBUCK_MV_STB->Kflag == FT_TOOL_K)||(pBUCK_MV_STB->Kflag == FT_FW_K))
    {
        tmp_val = pBUCK_MV_STB->data[pBUCK_MV_STB->volselect - 1].value;
        pmu_set_register_value(MV_STB_SEL_ADDR, MV_STB_SEL_MASK, MV_STB_SEL_SHIFT, tmp_val);
    }
    vPortFree(pBUCK_MV_STB);
}

void mpk_buck_mv_freq(void)
{
    BUCK_MV_FREQ* pBUCK_MV_FREQ = (BUCK_MV_FREQ*)get_nvkey_data(NVKEYID_MP_CAL_BUCK_MV_FREQ);

    if(!pBUCK_MV_FREQ)
        return;

    if((pBUCK_MV_FREQ->Kflag == FT_TOOL_K)||(pBUCK_MV_FREQ->Kflag == FT_FW_K))
    {
        pmu_set_register_value(BUCK_FREQ_MV_ADDR, BUCK_FREQ_MV_MASK, BUCK_FREQ_MV_SHIFT, pBUCK_MV_FREQ->BUCK_FREQ_MV);
        pmu_set_register_value(OSC_FREQK_MV_ADDR, OSC_FREQK_MV_MASK, OSC_FREQK_MV_SHIFT, pBUCK_MV_FREQ->OSC_FREQK_MV);
    }
    vPortFree(pBUCK_MV_FREQ);
}

void mpk_buck_lv(void)
{
    uint16_t tmp_val;

    BUCK_LV_CONFIG* pBUCK_LVCONFIG = (BUCK_LV_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_BUCK_LV_CONFIG);

    if(!pBUCK_LVCONFIG)
        return;

    if(pBUCK_LVCONFIG->CalCount < pBUCK_LVCONFIG->volselect)
    {
        vPortFree(pBUCK_LVCONFIG);
        return;
    }

    if((pBUCK_LVCONFIG->Kflag == FT_TOOL_K)||(pBUCK_LVCONFIG->Kflag == FT_FW_K))
    {
        tmp_val = pBUCK_LVCONFIG->data[pBUCK_LVCONFIG->volselect-1].value;
        pmu_set_register_value(BUCK_VSEL_LV_ADDR, BUCK_VSEL_LV_MASK, BUCK_VSEL_LV_SHIFT, tmp_val);
        log_hal_msgid_info("mpk_buck_lv cnt[%d], sel[%d], flag[%d], tmp_val[0x%x]", 4, pBUCK_LVCONFIG->CalCount, pBUCK_LVCONFIG->volselect
            , pBUCK_LVCONFIG->Kflag, tmp_val);
    }
    uint16_t tmp = pmu_get_register_value(BUCK_VSEL_LV_ADDR, BUCK_VSEL_LV_MASK, BUCK_VSEL_LV_SHIFT);
    log_hal_msgid_info("mpk_buck_lv tmp_val 2[0x%x]", 1, tmp);

    vPortFree(pBUCK_LVCONFIG);
}

void mpk_buck_lv_lpm(void)
{
    uint16_t tmp_val;

    BUCK_LV_LPM* pBUCK_LV_LPM = (BUCK_LV_LPM*)get_nvkey_data(NVKEYID_MP_CAL_BUCK_LV_LPM);

    if(!pBUCK_LV_LPM)
        return;

    if(pBUCK_LV_LPM->CalCount < pBUCK_LV_LPM->volselect)
    {
        vPortFree(pBUCK_LV_LPM);
        return;
    }

    if((pBUCK_LV_LPM->Kflag == FT_TOOL_K)||(pBUCK_LV_LPM->Kflag == FT_FW_K))
    {
        tmp_val = pBUCK_LV_LPM->data[pBUCK_LV_LPM->volselect-1].value;
        pmu_set_register_value(BUCK_VSEL_LV_ADDR, BUCK_VSEL_LV_MASK, BUCK_VSEL_LV_SHIFT, tmp_val);
    }

    vPortFree(pBUCK_LV_LPM);
}

void mpk_buck_lv_freq(void)
{
    BUCK_LV_FREQ* pBUCK_LV_FREQ = (BUCK_LV_FREQ*)get_nvkey_data(NVKEYID_MP_CAL_BUCK_LV_FREQ);

    if(!pBUCK_LV_FREQ)
        return;

    if((pBUCK_LV_FREQ->Kflag == FT_TOOL_K)||(pBUCK_LV_FREQ->Kflag == FT_FW_K))
    {
        pmu_set_register_value(BUCK_FREQ_LV_ADDR, BUCK_FREQ_LV_MASK, BUCK_FREQ_LV_SHIFT, pBUCK_LV_FREQ->BUCK_FREQ_LV);
        pmu_set_register_value(OSC_FREQK_LV_ADDR, OSC_FREQK_LV_MASK, OSC_FREQK_LV_SHIFT, pBUCK_LV_FREQ->OSC_FREQK_LV);
    }
    vPortFree(pBUCK_LV_FREQ);
}

void mpk_buck_config(void)
{
    mpk_buck_mv();
    mpk_buck_mv_stb();
    mpk_buck_mv_freq();
    mpk_buck_lv();
    mpk_buck_lv_lpm();
    mpk_buck_lv_freq();
}

/*******************LDO********************/
void mpk_ldo_vdd33_reg(void)
{
    uint16_t tmp_val;

    LDO_VDD33_REG_CONFIG* pLDO_VDD33_REG = (LDO_VDD33_REG_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LDO_VDD33_REG_CONFIG);

    if(!pLDO_VDD33_REG)
        return;

    if(pLDO_VDD33_REG->CalCount < pLDO_VDD33_REG->volselect)
    {
        vPortFree(pLDO_VDD33_REG);
        return;
    }
    if((pLDO_VDD33_REG->Kflag == FT_TOOL_K)||(pLDO_VDD33_REG->Kflag == FT_FW_K))
    {
        tmp_val = pLDO_VDD33_REG->data[pLDO_VDD33_REG->volselect - 1].value;
        pmu_set_register_value(REGHV_SEL_NM_ADDR, REGHV_SEL_NM_MASK, REGHV_SEL_NM_SHIFT, tmp_val);
    }
    vPortFree(pLDO_VDD33_REG);
}

void mpk_ldo_vdd33_reg_ret(void)
{
    uint16_t tmp_val;

    LDO_VDD33_REG_RET_CONFIG* pLDO_VDD33_REG_RET = (LDO_VDD33_REG_RET_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LDO_VDD33_REG_RET_CONFIG);

    if(!pLDO_VDD33_REG_RET)
        return;

    if(pLDO_VDD33_REG_RET->CalCount < pLDO_VDD33_REG_RET->volselect)
    {
        vPortFree(pLDO_VDD33_REG_RET);
        return;
    }
    if((pLDO_VDD33_REG_RET->Kflag == FT_TOOL_K)||(pLDO_VDD33_REG_RET->Kflag == FT_FW_K))
    {
        tmp_val = pLDO_VDD33_REG_RET->data[pLDO_VDD33_REG_RET->volselect - 1].value;
        pmu_set_register_value(REGHV_SEL_RET_ADDR, REGHV_SEL_RET_MASK, REGHV_SEL_RET_SHIFT, tmp_val);
    }
    vPortFree(pLDO_VDD33_REG_RET);
}

void mpk_ldo_vdd33_ret(void)
{
    uint16_t tmp_val;

    LDO_VDD33_RET_CONFIG* pLDO_VDD33_RET = (LDO_VDD33_RET_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LDO_VDD33_RET_CONFIG);

    if(!pLDO_VDD33_RET)
        return;

    if(pLDO_VDD33_RET->CalCount < pLDO_VDD33_RET->volselect)
    {
        vPortFree(pLDO_VDD33_RET);
        return;
    }
    if((pLDO_VDD33_RET->Kflag == FT_TOOL_K)||(pLDO_VDD33_RET->Kflag == FT_FW_K))
    {
        tmp_val = pLDO_VDD33_RET->data[pLDO_VDD33_RET->volselect - 1].value;
        pmu_set_register_value(HVSTBSEL_ADDR, HVSTBSEL_MASK, HVSTBSEL_SHIFT, tmp_val);
    }
    vPortFree(pLDO_VDD33_RET);
}

void mpk_ldo_vrf_reg(void)
{
    uint16_t tmp_val;

    LDO_VRF_REG_CONFIG* pLDO_VRF_REG = (LDO_VRF_REG_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LDO_VRF_REG_CONFIG);

    if(!pLDO_VRF_REG)
        return;

    if(pLDO_VRF_REG->CalCount < pLDO_VRF_REG->volselect)
    {
        vPortFree(pLDO_VRF_REG);
        return;
    }
    if((pLDO_VRF_REG->Kflag == FT_TOOL_K)||(pLDO_VRF_REG->Kflag == FT_FW_K))
    {
        tmp_val = pLDO_VRF_REG->data[pLDO_VRF_REG->volselect - 1].value;
        pmu_set_register_value(REGLV2_SEL_NM_ADDR, REGLV2_SEL_NM_MASK, REGLV2_SEL_NM_SHIFT, tmp_val);
    }
    vPortFree(pLDO_VRF_REG);
}

void mpk_ldo_vrf_reg_ret(void)
{
    uint16_t tmp_val;

    LDO_VRF_REG_RET_CONFIG* pLDO_VRF_REG_RET = (LDO_VRF_REG_RET_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LDO_VRF_REG_RET_CONFIG);

    if(!pLDO_VRF_REG_RET)
        return;

    if(pLDO_VRF_REG_RET->CalCount < pLDO_VRF_REG_RET->volselect)
    {
        vPortFree(pLDO_VRF_REG_RET);
        return;
    }
    if((pLDO_VRF_REG_RET->Kflag == FT_TOOL_K)||(pLDO_VRF_REG_RET->Kflag == FT_FW_K))
    {
        tmp_val = pLDO_VRF_REG_RET->data[pLDO_VRF_REG_RET->volselect - 1].value;
        pmu_set_register_value(REGLV2_SEL_RET_ADDR, REGLV2_SEL_RET_MASK, REGLV2_SEL_RET_SHIFT, tmp_val);
    }
    vPortFree(pLDO_VRF_REG_RET);
}

void mpk_ldo_vrf_ret(void)
{
    uint16_t tmp_val;

    LDO_VRF_RET_CONFIG* pLDO_VRF_RET = (LDO_VRF_RET_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LDO_VRF_RET_CONFIG);

    if(!pLDO_VRF_RET)
        return;

    if(pLDO_VRF_RET->CalCount < pLDO_VRF_RET->volselect)
    {
        vPortFree(pLDO_VRF_RET);
        return;
    }
    if((pLDO_VRF_RET->Kflag == FT_TOOL_K)||(pLDO_VRF_RET->Kflag == FT_FW_K))
    {
        tmp_val = pLDO_VRF_RET->data[pLDO_VRF_RET->volselect - 1].value;
        pmu_set_register_value(LV3_STB_REGSEL_ADDR, LV3_STB_REGSEL_MASK, LV3_STB_REGSEL_SHIFT, tmp_val);
    }
    vPortFree(pLDO_VRF_RET);
}

void mpk_ldo_vdig18(void)
{
    uint16_t tmp_val;

    LDO_VDIG18_CONFIG* pLDO_VDIG18 = (LDO_VDIG18_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_VDIG18_CONFIG);

    if(!pLDO_VDIG18)
        return;

    if(pLDO_VDIG18->CalCount < pLDO_VDIG18->volselect)
    {
        vPortFree(pLDO_VDIG18);
        return;
    }
    if((pLDO_VDIG18->Kflag == FT_TOOL_K)||(pLDO_VDIG18->Kflag == FT_FW_K))
    {
        tmp_val = pLDO_VDIG18->data[pLDO_VDIG18->volselect - 1].value;
        pmu_set_register_value(VDIG18_SEL_ADDR, VDIG18_SEL_MASK, VDIG18_SEL_SHIFT, tmp_val);
    }
    vPortFree(pLDO_VDIG18);
}

void mpk_ldo_config(void)
{
    mpk_ldo_vdd33_reg();
    mpk_ldo_vdd33_reg_ret();
    mpk_ldo_vdd33_ret();
    mpk_ldo_vrf_reg();
    mpk_ldo_vrf_reg_ret();
    mpk_ldo_vrf_ret();
    mpk_ldo_vdig18();
}

/*******************VRF********************/
void mpk_hpbg(void)
{
    uint16_t tmp_val;

    HPBG_CONFIG* pVREF_HPBG = (HPBG_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_HPBG_CONFIG);

    if(!pVREF_HPBG)
        return;

    if(pVREF_HPBG->CalCount < pVREF_HPBG->volselect)
    {
        vPortFree(pVREF_HPBG);
        return;
    }
    if((pVREF_HPBG->Kflag == FT_TOOL_K)||(pVREF_HPBG->Kflag == FT_FW_K))
    {
        tmp_val = pVREF_HPBG->data[pVREF_HPBG->volselect - 1].value;
        pmu_set_register_value(BGR_TRIM_ADDR, BGR_TRIM_MASK, BGR_TRIM_SHIFT, tmp_val);
    }
    vPortFree(pVREF_HPBG);
}

void mpk_lpbg(void)
{
    uint16_t tmp_val;

    LPBG_CONFIG* pVREF_LPBG = (LPBG_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LPBG_CONFIG);

    if(!pVREF_LPBG)
        return;

    if(pVREF_LPBG->CalCount < pVREF_LPBG->volselect)
    {
        vPortFree(pVREF_LPBG);
        return;
    }
    if((pVREF_LPBG->Kflag == FT_TOOL_K)||(pVREF_LPBG->Kflag == FT_FW_K))
    {
        tmp_val = pVREF_LPBG->data[pVREF_LPBG->volselect - 1].value;
        pmu_set_register_value(BGR_TRIM_ADDR, BGR_TRIM_MASK, BGR_TRIM_SHIFT, tmp_val);
    }
    vPortFree(pVREF_LPBG);
}

void mpk_vref_config(void)
{
    mpk_hpbg();
    //mpk_lpbg();
}

/*******************RF********************/
#if 0
void CRTSTAL_TRIM_24M(void)
{
    XO24M_CRYSTAL_TRIM* Xo24mcrtstaltrim = get_nvkey_data(NVKEYID_MP_CAL_XO_26M_CRTSTAL_TRIM);

    if(!Xo24mcrtstaltrim)
        return;
    //DRV_3WIRE_Write(0x4E,BIT_FIELD_INSERT16(DRV_3WIRE_Read(0x4E),7,9,Xo24mcrtstaltrim.cap_val));
}

void GC_Offset_Calibration(void)
{
    uint16_t gc_offset = 0;

    NVKEY_PWR_CTL_K_STRU_PTR* pPWR_CTL_K =(NVKEY_PWR_CTL_K_STRU_PTR)get_nvkey_data(NVKEYID_MP_CAL_PWR_CTL_MP_K);

    if(!pPWR_CTL_K)
    {
        return;
    }

    if ((pPWR_CTL_K->TxGc_BR < 12) || (pPWR_CTL_K->TxGc_BR & 0x8000))
        pPWR_CTL_K->TxGc_BR = 12;
    else if (pPWR_CTL_K->TxGc_BR > 63)
        pPWR_CTL_K->TxGc_BR = 63;

    if ((pPWR_CTL_K->TxGc_EDR < 12) || (pPWR_CTL_K->TxGc_EDR & 0x8000))
        pPWR_CTL_K->TxGc_EDR = 12;
    else if (pPWR_CTL_K->TxGc_EDR > 63)
        pPWR_CTL_K->TxGc_EDR = 63;

    if ((pPWR_CTL_K->TxGc_LE1M < 12) || (pPWR_CTL_K->TxGc_LE1M & 0x8000))
        pPWR_CTL_K->TxGc_LE1M = 12;
    else if (pPWR_CTL_K->TxGc_LE1M > 63)
        pPWR_CTL_K->TxGc_LE1M = 63;

    if ((pPWR_CTL_K->TxGc_LE2M < 12) || (pPWR_CTL_K->TxGc_LE2M & 0x8000))
        pPWR_CTL_K->TxGc_LE2M = 12;
    else if (pPWR_CTL_K->TxGc_LE2M > 63)
        pPWR_CTL_K->TxGc_LE2M = 63;

    nvkey_write_data(NVKEYID_MP_CAL_PWR_CTL_MP_K, pPWR_CTL_K, sizeof(NVKEY_PWR_CTL_K_STRU));
    vPortFree(pPWR_CTL_K);
    log_hal_msgid_info("MP Calibration TxGc_BR[%d] TxGc_EDR[%d] TxGc_LE1M[%d] TxGc_LE2M[%d] gc_offset[%d]", 5,
        pPWR_CTL_K->TxGc_BR, pPWR_CTL_K->TxGc_EDR, pPWR_CTL_K->TxGc_LE1M, pPWR_CTL_K->TxGc_LE2M, gc_offset);
}
#endif
/*******************mpk_init********************/
void mpk_init(void)
{
    mpk_chg_config();
    mpk_buck_config();
    mpk_ldo_config();
    mpk_vref_config();
    log_hal_msgid_info("mpk_init, done", 0);
}

/*******************otp_calibration********************/
void otp_cal_vbat_adc(VBAT_ADC_CALIBRATION_TABLE* pVBAT_ADC ,OTP_VBAT_CONFIG* pOTP_VBAT)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t volt2 = 0;
    uint16_t adc1 = 0;
    uint16_t adc2 = 0;

    if ((!pVBAT_ADC) || (pVBAT_ADC->Kflag != DEFAULT_TAG))
        return;

    if(vbat_slope.voltage_sel)//4.35v
    {
        volt1 = pOTP_VBAT->vol_adc[0].Voltage;
        adc1 = pOTP_VBAT->vol_adc[0].adc;
    }
    else //4.2v
    {
        volt1 = pOTP_VBAT->vol_adc[1].Voltage;
        adc1 = pOTP_VBAT->vol_adc[1].adc;
    }
    volt2 = pOTP_VBAT->vol_adc[2].Voltage; //3.0v
    adc2 = pOTP_VBAT->vol_adc[2].adc;

    vbat_slope.volt1 = volt1;
    vbat_slope.adc1  = adc1;
    vbat_slope.volt2 = volt2;
    vbat_slope.adc2  = adc2;

    for(i = 0; i < pVBAT_ADC->CalCount; i++)
    {
        pVBAT_ADC->data[i].adc = adc_calculation(volt1, adc1, volt2, adc2, pVBAT_ADC->data[i].voltage);
        pVBAT_ADC->data[i].adc = range_protect(pVBAT_ADC->data[i].adc, 0, 1023, 2);
    }
    pVBAT_ADC->Kflag = FT_FW_K;

    nvkey_write_data(NVKEYID_MP_CAL_VBAT_ADC_CALIBRATION_TABLE, pVBAT_ADC, sizeof(VBAT_ADC_CALIBRATION_TABLE));
}

void otp_cal_vbat_volt(VBAT_VOLTAGE_CONFIG* pVBAT_VOLT ,VBAT_ADC_CALIBRATION_TABLE* pVBAT_ADC)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t volt2 = 0;
    uint16_t adc1 = 0;
    uint16_t adc2 = 0;

    if((!pVBAT_VOLT) || (pVBAT_VOLT->Kflag != DEFAULT_TAG) || (!pVBAT_ADC) || (pVBAT_ADC->Kflag != DEFAULT_TAG))
        return;

    volt1 = pVBAT_ADC->data[0].voltage;
    adc1 = pVBAT_ADC->data[0].adc;

    volt2 = pVBAT_ADC->data[1].voltage;
    adc2 = pVBAT_ADC->data[1].adc;

    pVBAT_VOLT->initial_bat_ADC  = adc_calculation(volt1, adc1, volt2, adc2, pVBAT_VOLT->initial_bat);
    pVBAT_VOLT->low_bat_ADC      = adc_calculation(volt1, adc1, volt2, adc2, pVBAT_VOLT->low_bat);
    pVBAT_VOLT->Shutdown_bat_ADC = adc_calculation(volt1, adc1, volt2, adc2, pVBAT_VOLT->Shutdown_bat);

    for(i=0 ; i<18 ;i++)
    {
        if (pVBAT_VOLT->data[i].check_point == 0)
        {
            if (i <= 9)
                assert(0);
            continue;
        }
        pVBAT_VOLT->data[i].adc = adc_calculation(volt1, adc1, volt2, adc2, pVBAT_VOLT->data[i].check_point);
        pVBAT_VOLT->data[i].adc = range_protect(pVBAT_VOLT->data[i].adc, 0, 1023, 2);
    }
    pVBAT_VOLT->Kflag = FT_FW_K;
    nvkey_write_data(NVKEYID_MP_CAL_VBAT_VOLTAGE_CONFIG, pVBAT_VOLT, sizeof(VBAT_VOLTAGE_CONFIG));
}


void otp_cal_chg_adc(CHG_ADC_CONFIG* pCHG_ADC ,OTP_VBAT_CONFIG* pOTP_VBAT)
{
    uint16_t volt1 = 0;
    uint16_t volt2 = 0;
    uint16_t adc1 = 0;
    uint16_t adc2 = 0;

    if(!pCHG_ADC)
        return;

    if(vbat_slope.voltage_sel)//4.35v
    {
        volt1 = pOTP_VBAT->vol_adc[0].Voltage;
        adc1 = pOTP_VBAT->vol_adc[0].adc;
    }
    else //4.2v
    {
        volt1 = pOTP_VBAT->vol_adc[1].Voltage;
        adc1 = pOTP_VBAT->vol_adc[1].adc;
    }
    volt2 = pOTP_VBAT->vol_adc[2].Voltage; //3.0v
    adc2 = pOTP_VBAT->vol_adc[2].adc;

    pmu_chg_info.cc1_thrd_volt = pCHG_ADC->cc1_threshold_voltage + pCHG_ADC->cc1_thd_voltage_offset;
    pmu_chg_info.cc2_thrd_volt = pCHG_ADC->cc2_threshold_voltage + pCHG_ADC->cc2_offset_voltage;
    pmu_chg_info.full_bat_volt = pCHG_ADC->full_bat_voltage + pCHG_ADC->full_bat_voltage_offset;
    pmu_chg_info.rechg_volt = pCHG_ADC->recharge_voltage + pCHG_ADC->recharge_voltage_offset;

    pCHG_ADC->cc1_threshold_ADC = adc_calculation(volt1, adc1, volt2, adc2, pCHG_ADC->cc1_threshold_voltage + pCHG_ADC->cc1_thd_voltage_offset);
    pCHG_ADC->cc2_threshold_ADC = adc_calculation(volt1, adc1, volt2, adc2, pCHG_ADC->cc2_threshold_voltage + pCHG_ADC->cc2_offset_voltage);
    pCHG_ADC->cv_threshold_ADC  = adc_calculation(volt1, adc1, volt2, adc2, pCHG_ADC->cv_threshold_voltage + pCHG_ADC->cv_thd_voltage_offset);
    pCHG_ADC->full_bat_ADC      = adc_calculation(volt1, adc1, volt2, adc2, pCHG_ADC->full_bat_voltage + pCHG_ADC->full_bat_voltage_offset);
    pCHG_ADC->recharge_ADC      = adc_calculation(volt1, adc1, volt2, adc2, pCHG_ADC->recharge_voltage + pCHG_ADC->recharge_voltage_offset);
    pCHG_ADC->full_bat_ADC_2    = adc_calculation(volt1, adc1, volt2, adc2, pCHG_ADC->full_bat_voltage_2 + pCHG_ADC->full_bat_voltage_2_offset);

    pCHG_ADC->cc1_threshold_ADC = range_protect(pCHG_ADC->cc1_threshold_ADC, 0, 1023, 2);
    pCHG_ADC->cc2_threshold_ADC = range_protect(pCHG_ADC->cc2_threshold_ADC, 0, 1023, 2);
    pCHG_ADC->cv_threshold_ADC  = range_protect(pCHG_ADC->cv_threshold_ADC, 0, 1023, 2);
    pCHG_ADC->full_bat_ADC      = range_protect(pCHG_ADC->full_bat_ADC, 0, 1023, 2);
    pCHG_ADC->recharge_ADC      = range_protect(pCHG_ADC->recharge_ADC, 0, 1023, 2);
    pCHG_ADC->full_bat_ADC_2    = range_protect(pCHG_ADC->full_bat_ADC_2, 0, 1023, 2);

    nvkey_write_data(NVKEYID_MP_CAL_CHG_ADC_CONFIG, pCHG_ADC, sizeof(CHG_ADC_CONFIG));
}

void otp_cal_vbat_config(void)
{
    CHG_CONFIG* pChg_config = get_nvkey_data(NVKEYID_MP_CAL_CHG_CONFIG);
    if(!pChg_config)
        return;
    if (pChg_config->Kflag != DEFAULT_TAG)
    {
        vPortFree(pChg_config);
        return;
    }
    mpk_flag = pChg_config->Kflag;
    vbat_slope.voltage_sel = pChg_config->Battery_voltage_sel;
    vPortFree(pChg_config);

    OTP_VBAT_CONFIG* pOTP_VBAT = (OTP_VBAT_CONFIG*)pvPortMalloc(sizeof(OTP_VBAT_CONFIG));
    if (!pOTP_VBAT)
        return;
    hal_flash_otp_read(OTP_VBAT_ADDR, (uint8_t *)pOTP_VBAT, sizeof(OTP_VBAT_CONFIG));
    if(pOTP_VBAT->FT_Kflag != 1)
    {
        vPortFree(pOTP_VBAT);
        return;
    }
    OTP_flag = pOTP_VBAT->FT_Kflag;

    VBAT_ADC_CALIBRATION_TABLE* pVBAT_ADC = (VBAT_ADC_CALIBRATION_TABLE*)get_nvkey_data(NVKEYID_MP_CAL_VBAT_ADC_CALIBRATION_TABLE);
    VBAT_VOLTAGE_CONFIG* pVBAT_VOLT = (VBAT_VOLTAGE_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_VBAT_VOLTAGE_CONFIG);
    CHG_ADC_CONFIG* pCHG_ADC = (CHG_ADC_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_CHG_ADC_CONFIG);

    otp_cal_vbat_adc(pVBAT_ADC, pOTP_VBAT);
    otp_cal_vbat_volt(pVBAT_VOLT, pVBAT_ADC);
    otp_cal_chg_adc(pCHG_ADC, pOTP_VBAT);

    vPortFree(pOTP_VBAT);
    vPortFree(pVBAT_ADC);
    vPortFree(pVBAT_VOLT);
    vPortFree(pCHG_ADC);
}

void otp_cal_buck_mv(void)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_BUCK_MV_CONFIG* pOTP_BUCK_MV = (OTP_BUCK_MV_CONFIG*)pvPortMalloc(sizeof(OTP_BUCK_MV_CONFIG));
    BUCK_MV_CONFIG* pBUCK_MV = (BUCK_MV_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_BUCK_MV_CONFIG);

    hal_flash_otp_read(OTP_BUCK_MV_ADDR, (uint8_t *)pOTP_BUCK_MV, sizeof(OTP_BUCK_MV_CONFIG));

    if((pOTP_BUCK_MV->FT_Kflag != 1) || (pBUCK_MV->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_BUCK_MV);
        vPortFree(pBUCK_MV);
        return;
    }

    volt1 = pOTP_BUCK_MV->vol_sel[0].Voltage;
    sel1  = pOTP_BUCK_MV->vol_sel[0].Sel;
    volt2 = pOTP_BUCK_MV->vol_sel[1].Voltage;
    sel2  = pOTP_BUCK_MV->vol_sel[1].Sel;

    for(i = 0; i < pBUCK_MV->CalCount; i++)
    {
        pBUCK_MV->data[i].value = adc_calculation(volt1, sel1, volt2, sel2, pBUCK_MV->data[i].voltage);
        pBUCK_MV->data[i].value = range_protect(pBUCK_MV->data[i].value, 0, 255, 1);
    }
    pBUCK_MV->Kflag = FT_FW_K;
    pBUCK_MV->sloep_value1 = sel1;
    pBUCK_MV->sloep_value2 = sel2;
    pBUCK_MV->ADC1_S = volt1;
    pBUCK_MV->ADC2_S = volt2;
    nvkey_write_data(NVKEYID_MP_CAL_BUCK_MV_CONFIG, pBUCK_MV, sizeof(BUCK_MV_CONFIG));

    vPortFree(pOTP_BUCK_MV);
    vPortFree(pBUCK_MV);
}

void otp_cal_buck_mv_ret(void)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_BUCK_MV_RET_CONFIG* pOTP_BUCK_MV_RET = (OTP_BUCK_MV_RET_CONFIG*)pvPortMalloc(sizeof(OTP_BUCK_MV_RET_CONFIG));
    BUCK_MV_STB_CONFIG* pBUCK_MV_STB = (BUCK_MV_STB_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_BUCK_MV_STB_CONFIG);

    hal_flash_otp_read(OTP_BUCK_MV_RET_ADDR, (uint8_t *)pOTP_BUCK_MV_RET, sizeof(OTP_BUCK_MV_RET_CONFIG));

    if((pOTP_BUCK_MV_RET->FT_Kflag != 1) || (pBUCK_MV_STB->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_BUCK_MV_RET);
        vPortFree(pBUCK_MV_STB);
        return;
    }

    volt1 = pOTP_BUCK_MV_RET->vol_sel[0].Voltage;
    sel1  = pOTP_BUCK_MV_RET->vol_sel[0].Sel;
    volt2 = pOTP_BUCK_MV_RET->vol_sel[1].Voltage;
    sel2  = pOTP_BUCK_MV_RET->vol_sel[1].Sel;

    for(i = 0; i < pBUCK_MV_STB->CalCount; i++)
    {
        pBUCK_MV_STB->data[i].value = adc_calculation(volt1, sel1, volt2, sel2, pBUCK_MV_STB->data[i].voltage);
        pBUCK_MV_STB->data[i].value = range_protect(pBUCK_MV_STB->data[i].value, 0, 31, 1);
    }
    pBUCK_MV_STB->Kflag = FT_FW_K;
    pBUCK_MV_STB->sloep_value1 = sel1;
    pBUCK_MV_STB->sloep_value2 = sel2;
    pBUCK_MV_STB->ADC1_S = volt1;
    pBUCK_MV_STB->ADC2_S = volt2;
    nvkey_write_data(NVKEYID_MP_CAL_BUCK_MV_STB_CONFIG, pBUCK_MV_STB, sizeof(BUCK_MV_STB_CONFIG));

    vPortFree(pOTP_BUCK_MV_RET);
    vPortFree(pBUCK_MV_STB);
}

void otp_cal_buck_mv_freq(void)
{
    OTP_BUCK_MV_FREQ_CONFIG* pOTP_BUCK_MV_FREQ = (OTP_BUCK_MV_FREQ_CONFIG*)pvPortMalloc(sizeof(OTP_BUCK_MV_FREQ_CONFIG));
    BUCK_MV_FREQ* pBUCK_MV_FREQ = (BUCK_MV_FREQ*)get_nvkey_data(NVKEYID_MP_CAL_BUCK_MV_FREQ);

    hal_flash_otp_read(OTP_BUCK_MV_FREQ_ADDR, (uint8_t *)pOTP_BUCK_MV_FREQ, sizeof(OTP_BUCK_MV_FREQ_CONFIG));

    if((pOTP_BUCK_MV_FREQ->FT_Kflag != 1) || (pBUCK_MV_FREQ->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_BUCK_MV_FREQ);
        vPortFree(pBUCK_MV_FREQ);
        return;
    }

    pBUCK_MV_FREQ->BUCK_FREQ_MV = range_protect(pOTP_BUCK_MV_FREQ->Freq_Trim, 0, 3, 1);
    pBUCK_MV_FREQ->OSC_FREQK_MV = range_protect(pOTP_BUCK_MV_FREQ->Freqk_Trim, 0, 7, 1);
    pBUCK_MV_FREQ->Kflag = FT_FW_K;
    nvkey_write_data(NVKEYID_MP_CAL_BUCK_MV_FREQ, pBUCK_MV_FREQ, sizeof(BUCK_MV_FREQ));

    vPortFree(pOTP_BUCK_MV_FREQ);
    vPortFree(pBUCK_MV_FREQ);
}

void otp_cal_buck_lv(void)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_BUCK_LV_CONFIG* pOTP_BUCK_LV = (OTP_BUCK_LV_CONFIG*)pvPortMalloc(sizeof(OTP_BUCK_LV_CONFIG));
    BUCK_LV_CONFIG* pBUCK_LV = (BUCK_LV_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_BUCK_LV_CONFIG);

    hal_flash_otp_read(OTP_BUCK_LV_ADDR, (uint8_t *)pOTP_BUCK_LV, sizeof(OTP_BUCK_LV_CONFIG));

    if((pOTP_BUCK_LV->FT_Kflag != 1) || (pBUCK_LV->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_BUCK_LV);
        vPortFree(pBUCK_LV);
        return;
    }

    volt1 = pOTP_BUCK_LV->vol_sel[0].Voltage;
    sel1  = pOTP_BUCK_LV->vol_sel[0].Sel;
    volt2 = pOTP_BUCK_LV->vol_sel[1].Voltage;
    sel2  = pOTP_BUCK_LV->vol_sel[1].Sel;

    for(i = 0; i < pBUCK_LV->CalCount; i++)
    {
        pBUCK_LV->data[i].value = adc_calculation(volt1, sel1, volt2, sel2, pBUCK_LV->data[i].voltage);
        pBUCK_LV->data[i].value = range_protect(pBUCK_LV->data[i].value, 0, 255, 1);
    }
    pBUCK_LV->Kflag = FT_FW_K;
    pBUCK_LV->sloep_value1 = sel1;
    pBUCK_LV->sloep_value2 = sel2;
    pBUCK_LV->ADC1_S = volt1;
    pBUCK_LV->ADC2_S = volt2;
    nvkey_write_data(NVKEYID_MP_CAL_BUCK_LV_CONFIG, pBUCK_LV, sizeof(BUCK_LV_CONFIG));

    vPortFree(pOTP_BUCK_LV);
    vPortFree(pBUCK_LV);
}

void otp_cal_buck_lv_lpm(void)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_BUCK_LV_CONFIG* pOTP_BUCK_LV = (OTP_BUCK_LV_CONFIG*)pvPortMalloc(sizeof(OTP_BUCK_LV_CONFIG));
    BUCK_LV_LPM* pBUCK_LV_LPM = (BUCK_LV_LPM*)get_nvkey_data(NVKEYID_MP_CAL_BUCK_LV_LPM);

    hal_flash_otp_read(OTP_BUCK_LV_ADDR, (uint8_t *)pOTP_BUCK_LV, sizeof(OTP_BUCK_LV_CONFIG));

    if((pOTP_BUCK_LV->FT_Kflag != 1) || (pBUCK_LV_LPM->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_BUCK_LV);
        vPortFree(pBUCK_LV_LPM);
        return;
    }

    volt1 = pOTP_BUCK_LV->vol_sel[0].Voltage;
    sel1  = pOTP_BUCK_LV->vol_sel[0].Sel;
    volt2 = pOTP_BUCK_LV->vol_sel[1].Voltage;
    sel2  = pOTP_BUCK_LV->vol_sel[1].Sel;

    for(i = 0; i < pBUCK_LV_LPM->CalCount; i++)
    {
        pBUCK_LV_LPM->data[i].value = adc_calculation(volt1, sel1, volt2, sel2, pBUCK_LV_LPM->data[i].voltage);
        pBUCK_LV_LPM->data[i].value = range_protect(pBUCK_LV_LPM->data[i].value, 0, 255, 1);
    }
    pBUCK_LV_LPM->Kflag = FT_FW_K;
    pBUCK_LV_LPM->sloep_value1 = sel1;
    pBUCK_LV_LPM->sloep_value2 = sel2;
    pBUCK_LV_LPM->ADC1_S = volt1;
    pBUCK_LV_LPM->ADC2_S = volt2;
    nvkey_write_data(NVKEYID_MP_CAL_BUCK_LV_LPM, pBUCK_LV_LPM, sizeof(BUCK_LV_LPM));

    vPortFree(pOTP_BUCK_LV);
    vPortFree(pBUCK_LV_LPM);
}

void otp_cal_buck_lv_freq(void)
{
    OTP_BUCK_LV_FREQ_CONFIG* pOTP_BUCK_LV_FREQ = (OTP_BUCK_LV_FREQ_CONFIG*)pvPortMalloc(sizeof(OTP_BUCK_LV_FREQ_CONFIG));
    BUCK_LV_FREQ* pBUCK_LV_FREQ = (BUCK_LV_FREQ*)get_nvkey_data(NVKEYID_MP_CAL_BUCK_LV_FREQ);

    hal_flash_otp_read(OTP_BUCK_LV_FREQ_ADDR, (uint8_t *)pOTP_BUCK_LV_FREQ, sizeof(OTP_BUCK_LV_FREQ_CONFIG));

    if((pOTP_BUCK_LV_FREQ->FT_Kflag != 1) || (pBUCK_LV_FREQ->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_BUCK_LV_FREQ);
        vPortFree(pBUCK_LV_FREQ);
        return;
    }

    pBUCK_LV_FREQ->BUCK_FREQ_LV = range_protect(pOTP_BUCK_LV_FREQ->Freq_Trim, 0, 3, 1);
    pBUCK_LV_FREQ->OSC_FREQK_LV = range_protect(pOTP_BUCK_LV_FREQ->Freqk_Trim, 0, 7, 1);
    pBUCK_LV_FREQ->Kflag = FT_FW_K;
    nvkey_write_data(NVKEYID_MP_CAL_BUCK_LV_FREQ, pBUCK_LV_FREQ, sizeof(BUCK_LV_FREQ));

    vPortFree(pOTP_BUCK_LV_FREQ);
    vPortFree(pBUCK_LV_FREQ);
}

void otp_cal_buck_config(void)
{
    otp_cal_buck_mv();
    otp_cal_buck_mv_ret();
    otp_cal_buck_mv_freq();
    otp_cal_buck_lv();
    otp_cal_buck_lv_lpm();
    otp_cal_buck_lv_freq();
}

void otp_cal_ldo_vdd33_reg(void)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_LDO_VDD33_REG_CONFIG* pOTP_LDO_VDD33_REG = (OTP_LDO_VDD33_REG_CONFIG*)pvPortMalloc(sizeof(OTP_LDO_VDD33_REG_CONFIG));
    LDO_VDD33_REG_CONFIG* pLDO_VDD33_REG = (LDO_VDD33_REG_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LDO_VDD33_REG_CONFIG);

    hal_flash_otp_read(OTP_LDO_VDD33_REG_ADDR, (uint8_t *)pOTP_LDO_VDD33_REG, sizeof(OTP_LDO_VDD33_REG_CONFIG));

    if((pOTP_LDO_VDD33_REG->FT_Kflag != 1) || (pLDO_VDD33_REG->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_LDO_VDD33_REG);
        vPortFree(pLDO_VDD33_REG);
        return;
    }

    volt1 = pOTP_LDO_VDD33_REG->vol_sel[0].Voltage;
    sel1  = pOTP_LDO_VDD33_REG->vol_sel[0].Sel;
    volt2 = pOTP_LDO_VDD33_REG->vol_sel[1].Voltage;
    sel2  = pOTP_LDO_VDD33_REG->vol_sel[1].Sel;

    for(i = 0; i < pLDO_VDD33_REG->CalCount; i++)
    {
        pLDO_VDD33_REG->data[i].value = adc_calculation(volt1, sel1, volt2, sel2, pLDO_VDD33_REG->data[i].voltage);
        pLDO_VDD33_REG->data[i].value = range_protect(pLDO_VDD33_REG->data[i].value, 0, 127, 1);
    }
    pLDO_VDD33_REG->Kflag = FT_FW_K;
    pLDO_VDD33_REG->sloep_value1 = sel1;
    pLDO_VDD33_REG->sloep_value2 = sel2;
    pLDO_VDD33_REG->ADC1_S = volt1;
    pLDO_VDD33_REG->ADC2_S = volt2;
    nvkey_write_data(NVKEYID_MP_CAL_LDO_VDD33_REG_CONFIG, pLDO_VDD33_REG, sizeof(LDO_VDD33_REG_CONFIG));

    vPortFree(pOTP_LDO_VDD33_REG);
    vPortFree(pLDO_VDD33_REG);
}

void otp_cal_ldo_vdd33_reg_ret(void)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_LDO_VDD33_REG_CONFIG* pOTP_LDO_VDD33_REG = (OTP_LDO_VDD33_REG_CONFIG*)pvPortMalloc(sizeof(OTP_LDO_VDD33_REG_CONFIG));
    LDO_VDD33_REG_RET_CONFIG* pLDO_VDD33_REG_RET = (LDO_VDD33_REG_RET_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LDO_VDD33_REG_RET_CONFIG);

    hal_flash_otp_read(OTP_LDO_VDD33_REG_ADDR, (uint8_t *)pOTP_LDO_VDD33_REG, sizeof(OTP_LDO_VDD33_REG_CONFIG));

    if((pOTP_LDO_VDD33_REG->FT_Kflag != 1) || (pLDO_VDD33_REG_RET->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_LDO_VDD33_REG);
        vPortFree(pLDO_VDD33_REG_RET);
        return;
    }

    volt1 = pOTP_LDO_VDD33_REG->vol_sel[0].Voltage;
    sel1  = pOTP_LDO_VDD33_REG->vol_sel[0].Sel;
    volt2 = pOTP_LDO_VDD33_REG->vol_sel[1].Voltage;
    sel2  = pOTP_LDO_VDD33_REG->vol_sel[1].Sel;

    for(i = 0; i < pLDO_VDD33_REG_RET->CalCount; i++)
    {
        pLDO_VDD33_REG_RET->data[i].value = adc_calculation(volt1, sel1, volt2, sel2, pLDO_VDD33_REG_RET->data[i].voltage);
        pLDO_VDD33_REG_RET->data[i].value = range_protect(pLDO_VDD33_REG_RET->data[i].value, 0, 127, 1);
    }
    pLDO_VDD33_REG_RET->Kflag = FT_FW_K;
    pLDO_VDD33_REG_RET->sloep_value1 = sel1;
    pLDO_VDD33_REG_RET->sloep_value2 = sel2;
    pLDO_VDD33_REG_RET->ADC1_S = volt1;
    pLDO_VDD33_REG_RET->ADC2_S = volt2;
    nvkey_write_data(NVKEYID_MP_CAL_LDO_VDD33_REG_RET_CONFIG, pLDO_VDD33_REG_RET, sizeof(LDO_VDD33_REG_RET_CONFIG));

    vPortFree(pOTP_LDO_VDD33_REG);
    vPortFree(pLDO_VDD33_REG_RET);
}

void otp_cal_ldo_vdd33_ret(void)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_LDO_VDD33_RET_CONFIG* pOTP_LDO_VDD33_RET = (OTP_LDO_VDD33_RET_CONFIG*)pvPortMalloc(sizeof(OTP_LDO_VDD33_RET_CONFIG));
    LDO_VDD33_RET_CONFIG* pLDO_VDD33_RET = (LDO_VDD33_RET_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LDO_VDD33_RET_CONFIG);

    hal_flash_otp_read(OTP_LDO_VDD33_RET_ADDR, (uint8_t *)pOTP_LDO_VDD33_RET, sizeof(OTP_LDO_VDD33_RET_CONFIG));

    if((pOTP_LDO_VDD33_RET->FT_Kflag != 1) || (pLDO_VDD33_RET->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_LDO_VDD33_RET);
        vPortFree(pLDO_VDD33_RET);
        return;
    }

    volt1 = pOTP_LDO_VDD33_RET->vol_sel[0].Voltage;
    sel1  = pOTP_LDO_VDD33_RET->vol_sel[0].Sel;
    volt2 = pOTP_LDO_VDD33_RET->vol_sel[1].Voltage;
    sel2  = pOTP_LDO_VDD33_RET->vol_sel[1].Sel;

    for(i = 0; i < pLDO_VDD33_RET->CalCount; i++)
    {
        pLDO_VDD33_RET->data[i].value = adc_calculation(volt1, sel1, volt2, sel2, pLDO_VDD33_RET->data[i].voltage);
        pLDO_VDD33_RET->data[i].value = range_protect(pLDO_VDD33_RET->data[i].value, 0, 63, 1);
    }
    pLDO_VDD33_RET->Kflag = FT_FW_K;
    pLDO_VDD33_RET->sloep_value1 = sel1;
    pLDO_VDD33_RET->sloep_value2 = sel2;
    pLDO_VDD33_RET->ADC1_S = volt1;
    pLDO_VDD33_RET->ADC2_S = volt2;
    nvkey_write_data(NVKEYID_MP_CAL_LDO_VDD33_RET_CONFIG, pLDO_VDD33_RET, sizeof(LDO_VDD33_RET_CONFIG));

    vPortFree(pOTP_LDO_VDD33_RET);
    vPortFree(pLDO_VDD33_RET);
}

void otp_cal_ldo_vrf_reg(void)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_LDO_VRF_REG_CONFIG* pOTP_LDO_VRF_REG = (OTP_LDO_VRF_REG_CONFIG*)pvPortMalloc(sizeof(OTP_LDO_VRF_REG_CONFIG));
    LDO_VRF_REG_CONFIG* pLDO_VRF_REG = (LDO_VRF_REG_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LDO_VRF_REG_CONFIG);

    hal_flash_otp_read(OTP_LDO_VRF_REG_ADDR, (uint8_t *)pOTP_LDO_VRF_REG, sizeof(OTP_LDO_VRF_REG_CONFIG));

    if((pOTP_LDO_VRF_REG->FT_Kflag != 1) || (pLDO_VRF_REG->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_LDO_VRF_REG);
        vPortFree(pLDO_VRF_REG);
        return;
    }

    volt1 = pOTP_LDO_VRF_REG->vol_sel[0].Voltage;
    sel1  = pOTP_LDO_VRF_REG->vol_sel[0].Sel;
    volt2 = pOTP_LDO_VRF_REG->vol_sel[1].Voltage;
    sel2  = pOTP_LDO_VRF_REG->vol_sel[1].Sel;

    for(i = 0; i < pLDO_VRF_REG->CalCount; i++)
    {
        pLDO_VRF_REG->data[i].value = adc_calculation(volt1, sel1, volt2, sel2, pLDO_VRF_REG->data[i].voltage);
        pLDO_VRF_REG->data[i].value = range_protect(pLDO_VRF_REG->data[i].value, 0, 63, 1);
    }
    pLDO_VRF_REG->Kflag = FT_FW_K;
    pLDO_VRF_REG->sloep_value1 = sel1;
    pLDO_VRF_REG->sloep_value2 = sel2;
    pLDO_VRF_REG->ADC1_S = volt1;
    pLDO_VRF_REG->ADC2_S = volt2;
    nvkey_write_data(NVKEYID_MP_CAL_LDO_VRF_REG_CONFIG, pLDO_VRF_REG, sizeof(LDO_VRF_REG_CONFIG));

    vPortFree(pOTP_LDO_VRF_REG);
    vPortFree(pLDO_VRF_REG);
}

void otp_cal_ldo_vrf_reg_ret(void)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_LDO_VRF_REG_CONFIG* pOTP_LDO_VRF_REG = (OTP_LDO_VRF_REG_CONFIG*)pvPortMalloc(sizeof(OTP_LDO_VRF_REG_CONFIG));
    LDO_VRF_REG_RET_CONFIG* pLDO_VRF_REG_RET = (LDO_VRF_REG_RET_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LDO_VRF_REG_RET_CONFIG);

    hal_flash_otp_read(OTP_LDO_VRF_REG_ADDR, (uint8_t *)pOTP_LDO_VRF_REG, sizeof(OTP_LDO_VRF_REG_CONFIG));

    if((pOTP_LDO_VRF_REG->FT_Kflag != 1) || (pLDO_VRF_REG_RET->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_LDO_VRF_REG);
        vPortFree(pLDO_VRF_REG_RET);
        return;
    }

    volt1 = pOTP_LDO_VRF_REG->vol_sel[0].Voltage;
    sel1  = pOTP_LDO_VRF_REG->vol_sel[0].Sel;
    volt2 = pOTP_LDO_VRF_REG->vol_sel[1].Voltage;
    sel2  = pOTP_LDO_VRF_REG->vol_sel[1].Sel;

    for(i = 0; i < pLDO_VRF_REG_RET->CalCount; i++)
    {
        pLDO_VRF_REG_RET->data[i].value = adc_calculation(volt1, sel1, volt2, sel2, pLDO_VRF_REG_RET->data[i].voltage);
        pLDO_VRF_REG_RET->data[i].value = range_protect(pLDO_VRF_REG_RET->data[i].value, 0, 63, 1);
    }
    pLDO_VRF_REG_RET->Kflag = FT_FW_K;
    pLDO_VRF_REG_RET->sloep_value1 = sel1;
    pLDO_VRF_REG_RET->sloep_value2 = sel2;
    pLDO_VRF_REG_RET->ADC1_S = volt1;
    pLDO_VRF_REG_RET->ADC2_S = volt2;
    nvkey_write_data(NVKEYID_MP_CAL_LDO_VRF_REG_RET_CONFIG, pLDO_VRF_REG_RET, sizeof(LDO_VRF_REG_RET_CONFIG));

    vPortFree(pOTP_LDO_VRF_REG);
    vPortFree(pLDO_VRF_REG_RET);
}

void otp_cal_ldo_vrf_ret(void)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_LDO_VRF_RET_CONFIG* pOTP_LDO_VRF_RET = (OTP_LDO_VRF_RET_CONFIG*)pvPortMalloc(sizeof(OTP_LDO_VRF_RET_CONFIG));
    LDO_VRF_RET_CONFIG* pLDO_VRF_RET = (LDO_VRF_RET_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LDO_VRF_RET_CONFIG);

    hal_flash_otp_read(OTP_LDO_VRF_RET_ADDR, (uint8_t *)pOTP_LDO_VRF_RET, sizeof(OTP_LDO_VRF_RET_CONFIG));

    if((pOTP_LDO_VRF_RET->FT_Kflag != 1) || (pLDO_VRF_RET->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_LDO_VRF_RET);
        vPortFree(pLDO_VRF_RET);
        return;
    }

    volt1 = pOTP_LDO_VRF_RET->vol_sel[0].Voltage;
    sel1  = pOTP_LDO_VRF_RET->vol_sel[0].Sel;
    volt2 = pOTP_LDO_VRF_RET->vol_sel[1].Voltage;
    sel2  = pOTP_LDO_VRF_RET->vol_sel[1].Sel;

    for(i = 0; i < pLDO_VRF_RET->CalCount; i++)
    {
        pLDO_VRF_RET->data[i].value = adc_calculation(volt1, sel1, volt2, sel2, pLDO_VRF_RET->data[i].voltage);
        pLDO_VRF_RET->data[i].value = range_protect(pLDO_VRF_RET->data[i].value, 0, 31, 1);
    }
    pLDO_VRF_RET->Kflag = FT_FW_K;
    pLDO_VRF_RET->sloep_value1 = sel1;
    pLDO_VRF_RET->sloep_value2 = sel2;
    pLDO_VRF_RET->ADC1_S = volt1;
    pLDO_VRF_RET->ADC2_S = volt2;
    nvkey_write_data(NVKEYID_MP_CAL_LDO_VRF_RET_CONFIG, pLDO_VRF_RET, sizeof(LDO_VRF_RET_CONFIG));

    vPortFree(pOTP_LDO_VRF_RET);
    vPortFree(pLDO_VRF_RET);
}

void otp_cal_ldo_vdig18(void)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_LDO_VDIG18_CONFIG* pOTP_LDO_VDIG18 = (OTP_LDO_VDIG18_CONFIG*)pvPortMalloc(sizeof(OTP_LDO_VDIG18_CONFIG));
    LDO_VDIG18_CONFIG* pLDO_VDIG18 = (LDO_VDIG18_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_VDIG18_CONFIG);

    hal_flash_otp_read(OTP_LDO_VDIG18_ADDR, (uint8_t *)pOTP_LDO_VDIG18, sizeof(OTP_LDO_VDIG18_CONFIG));

    if((pOTP_LDO_VDIG18->FT_Kflag != 1) || (pLDO_VDIG18->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_LDO_VDIG18);
        vPortFree(pLDO_VDIG18);
        return;
    }

    volt1 = pOTP_LDO_VDIG18->vol_sel[0].Voltage;
    sel1  = pOTP_LDO_VDIG18->vol_sel[0].Sel;
    volt2 = pOTP_LDO_VDIG18->vol_sel[1].Voltage;
    sel2  = pOTP_LDO_VDIG18->vol_sel[1].Sel;

    for(i = 0; i < pLDO_VDIG18->CalCount; i++)
    {
        if (pLDO_VDIG18->data[i].voltage == 0)
        {
            pLDO_VDIG18->data[i].value = 0;
            continue;
        }
        if ((sel1 >= 16 && sel2 >= 16) || (sel1 < 16 && sel2 < 16))
        {
            /*pLDO_VDIG18->data[i].value = adc_calculation(volt1, sel1, volt2, sel2, pLDO_VDIG18->data[i].voltage);
            if (pLDO_VDIG18->data[i].value & 0x80)
            {
                pLDO_VDIG18->data[i].value += 32;
            }
            else if (pLDO_VDIG18->data[i].value > 31)
            {
                pLDO_VDIG18->data[i].value -= 32;
            }*/
            assert(0);
        }
        else
        {
            pLDO_VDIG18->data[i].value = vdig_bg_calculation(volt1, sel1, volt2, sel2, pLDO_VDIG18->data[i].voltage);
        }
        pLDO_VDIG18->data[i].value = (uint8_t)range_protect(pLDO_VDIG18->data[i].value, 0, 31, 1);
    }
    pLDO_VDIG18->Kflag = FT_FW_K;
    pLDO_VDIG18->sloep_value1 = sel1;
    pLDO_VDIG18->sloep_value2 = sel2;
    pLDO_VDIG18->ADC1_S = volt1;
    pLDO_VDIG18->ADC2_S = volt2;
    nvkey_write_data(NVKEYID_MP_CAL_VDIG18_CONFIG, pLDO_VDIG18, sizeof(LDO_VDIG18_CONFIG));

    vPortFree(pOTP_LDO_VDIG18);
    vPortFree(pLDO_VDIG18);
}

void otp_cal_ldo_config(void)
{
    otp_cal_ldo_vdd33_reg();
    otp_cal_ldo_vdd33_reg_ret();
    otp_cal_ldo_vdd33_ret();
    otp_cal_ldo_vrf_reg();
    otp_cal_ldo_vrf_reg_ret();
    otp_cal_ldo_vrf_ret();
    otp_cal_ldo_vdig18();
}

void otp_cal_hpbg(void)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_HPBG_CONFIG* pOTP_HPBG = (OTP_HPBG_CONFIG*)pvPortMalloc(sizeof(OTP_HPBG_CONFIG));
    HPBG_CONFIG* pHPBG = (HPBG_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_HPBG_CONFIG);

    hal_flash_otp_read(OTP_HPBG_ADDR, (uint8_t *)pOTP_HPBG, sizeof(OTP_HPBG_CONFIG));

    if((pOTP_HPBG->FT_Kflag != 1) || (pHPBG->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_HPBG);
        vPortFree(pHPBG);
        return;
    }

    volt1 = pOTP_HPBG->vol_sel[0].Voltage;
    sel1  = pOTP_HPBG->vol_sel[0].Sel;
    volt2 = pOTP_HPBG->vol_sel[1].Voltage;
    sel2  = pOTP_HPBG->vol_sel[1].Sel;

    for(i = 0; i < pHPBG->CalCount; i++)
    {
        if ((sel1 >= 16 && sel2 >= 16) || (sel1 < 16 && sel2 < 16))
        {
            /*pHPBG->data[i].value = adc_calculation(volt1, sel1, volt2, sel2, pHPBG->data[i].voltage);
            if (pHPBG->data[i].value & 0x80)
            {
                pHPBG->data[i].value += 32;
            }
            else if (pHPBG->data[i].value > 31)
            {
                pHPBG->data[i].value -= 32;
            }*/
            assert(0);
        }
        else
        {
            pHPBG->data[i].value = vdig_bg_calculation(volt1, sel1, volt2, sel2, pHPBG->data[i].voltage);
        }
        pHPBG->data[i].value = (uint8_t)range_protect(pHPBG->data[i].value, 0, 31, 1);
    }
    pHPBG->Kflag = FT_FW_K;
    pHPBG->sloep_value1 = sel1;
    pHPBG->sloep_value2 = sel2;
    pHPBG->ADC1_S = volt1;
    pHPBG->ADC2_S = volt2;
    nvkey_write_data(NVKEYID_MP_CAL_HPBG_CONFIG, pHPBG, sizeof(HPBG_CONFIG));

    vPortFree(pOTP_HPBG);
    vPortFree(pHPBG);
}

void otp_cal_lpbg(void)
{
    uint8_t i;
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_LPBG_CONFIG* pOTP_LPBG = (OTP_LPBG_CONFIG*)pvPortMalloc(sizeof(OTP_LPBG_CONFIG));
    LPBG_CONFIG* pLPBG = (LPBG_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_LPBG_CONFIG);

    hal_flash_otp_read(OTP_LPBG_ADDR, (uint8_t *)pOTP_LPBG, sizeof(OTP_LPBG_CONFIG));

    if((pOTP_LPBG->FT_Kflag != 1) || (pLPBG->Kflag != DEFAULT_TAG))
    {
        vPortFree(pOTP_LPBG);
        vPortFree(pLPBG);
        return;
    }

    volt1 = pOTP_LPBG->vol_sel[0].Voltage;
    sel1  = pOTP_LPBG->vol_sel[0].Sel;
    volt2 = pOTP_LPBG->vol_sel[1].Voltage;
    sel2  = pOTP_LPBG->vol_sel[1].Sel;

    for(i = 0; i < pLPBG->CalCount; i++)
    {
        if ((sel1 >= 16 && sel2 >= 16) || (sel1 < 16 && sel2 < 16))
        {
            /*pLPBG->data[i].value = adc_calculation(volt1, sel1, volt2, sel2, pLPBG->data[i].voltage);
            if (pLPBG->data[i].value & 0x80)
            {
                pLPBG->data[i].value += 32;
            }
            else if (pLPBG->data[i].value > 31)
            {
                pLPBG->data[i].value -= 32;
            }*/
            assert(0);
        }
        else
        {
            pLPBG->data[i].value = vdig_bg_calculation(volt1, sel1, volt2, sel2, pLPBG->data[i].voltage);
        }
        pLPBG->data[i].value = (uint8_t)range_protect(pLPBG->data[i].value, 0, 31, 1);
    }
    pLPBG->Kflag = FT_FW_K;
    pLPBG->sloep_value1 = sel1;
    pLPBG->sloep_value2 = sel2;
    pLPBG->ADC1_S = volt1;
    pLPBG->ADC2_S = volt2;
    nvkey_write_data(NVKEYID_MP_CAL_LPBG_CONFIG, pLPBG, sizeof(LPBG_CONFIG));

    vPortFree(pOTP_LPBG);
    vPortFree(pLPBG);
}

void otp_cal_bg_config(void)
{
    otp_cal_hpbg();
    otp_cal_lpbg();
}

void otp_cal_chg_dac(void)
{
    uint16_t chg_dac_val;

    OTP_CHG_DAC_CONFIG* pOTP_CHG_DAC = (OTP_CHG_DAC_CONFIG*)pvPortMalloc(sizeof(OTP_CHG_DAC_CONFIG));
    INT_CHG_DAC_CONFIG* pINT_CHG_DAC = (INT_CHG_DAC_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_INT_CHG_DAC_CONFIG);

    hal_flash_otp_read(OTP_CHG_DAC_ADDR, (uint8_t *)pOTP_CHG_DAC, sizeof(OTP_CHG_DAC_CONFIG));

    if((pOTP_CHG_DAC->FT_Kflag != 1))
    {
        vPortFree(pOTP_CHG_DAC);
        vPortFree(pINT_CHG_DAC);
        return;
    }

    if (vbat_slope.voltage_sel)
    {
        chg_dac_val = pOTP_CHG_DAC->DAC_4_35V;
    }
    else
    {
        chg_dac_val = pOTP_CHG_DAC->DAC_4_2V;
    }
    dac_slope.volt1 = 4350;
    dac_slope.dac1 = pOTP_CHG_DAC->DAC_4_35V;
    dac_slope.volt2 = 4200;
    dac_slope.dac2 = pOTP_CHG_DAC->DAC_4_2V;

    chg_dac_val = range_protect(chg_dac_val, 0, 1023, 2);

    pINT_CHG_DAC->tricklecurrentDAC = chg_dac_val;
    pINT_CHG_DAC->cc1currentDAC = chg_dac_val;
    pINT_CHG_DAC->cc2currentDAC = chg_dac_val;
    pINT_CHG_DAC->cvDAC = chg_dac_val;
    nvkey_write_data(NVKEYID_MP_CAL_INT_CHG_DAC_CONFIG, pINT_CHG_DAC, sizeof(INT_CHG_DAC_CONFIG));

    vPortFree(pOTP_CHG_DAC);
    vPortFree(pINT_CHG_DAC);
}

void otp_cal_chg_sys_ldo(void)
{
    uint16_t volt1 = 0;
    uint16_t sel1 = 0;
    uint16_t volt2 = 0;
    uint16_t sel2 = 0;

    OTP_VSYS_LDO_CONFIG* pOTP_VSYS_LDO = (OTP_VSYS_LDO_CONFIG*)pvPortMalloc(sizeof(OTP_VSYS_LDO_CONFIG));
    CHG_SYS_LDO* pSYS_LDO = (CHG_SYS_LDO*)get_nvkey_data(NVKEYID_MP_CAL_SYS_LDO);

    hal_flash_otp_read(OTP_VSYS_LDO_ADDR, (uint8_t *)pOTP_VSYS_LDO, sizeof(OTP_VSYS_LDO_CONFIG));

    if((pOTP_VSYS_LDO->FT_Kflag != 1))
    {
        vPortFree(pOTP_VSYS_LDO);
        vPortFree(pSYS_LDO);
        return;
    }

    volt1 = pOTP_VSYS_LDO->vol_sel[0].Voltage;
    sel1  = pOTP_VSYS_LDO->vol_sel[0].Sel;
    volt2 = pOTP_VSYS_LDO->vol_sel[1].Voltage;
    sel2  = pOTP_VSYS_LDO->vol_sel[1].Sel;

    pSYS_LDO->CHG_LDO_SEL = adc_calculation(volt1, sel1, volt2, sel2, pSYS_LDO->SYSLDO_output_voltage);
    pSYS_LDO->CHG_LDO_SEL = range_protect(pSYS_LDO->CHG_LDO_SEL, 0, 7, 1);

    nvkey_write_data(NVKEYID_MP_CAL_SYS_LDO, pSYS_LDO, sizeof(CHG_SYS_LDO));

    vPortFree(pOTP_VSYS_LDO);
    vPortFree(pSYS_LDO);
}

void otp_cal_chg_ocp(void)
{
    OTP_OCP_CONFIG* pOTP_OCP = (OTP_OCP_CONFIG*)pvPortMalloc(sizeof(OTP_OCP_CONFIG));
    CHG_OCP* pOCP = (CHG_OCP*)get_nvkey_data(NVKEYID_MP_CAL_OCP);

    hal_flash_otp_read(OTP_OCP_ADDR, (uint8_t *)pOTP_OCP, sizeof(OTP_OCP_CONFIG));

    if((pOTP_OCP->FT_Kflag != 1))
    {
        vPortFree(pOTP_OCP);
        vPortFree(pOCP);
        return;
    }
    pOCP->SW_OC_LMT = range_protect(pOCP->SW_OC_LMT, 0, 15, 1);
    pOCP->I_LIM_TRIM = range_protect(pOCP->I_LIM_TRIM, 0, 7, 1);
    nvkey_write_data(NVKEYID_MP_CAL_OCP, pOCP, sizeof(CHG_OCP));

    vPortFree(pOTP_OCP);
    vPortFree(pOCP);
}

void otp_cal_chg_trickle_current(uint16_t curr1, uint16_t val1, uint16_t curr2, uint16_t val2)
{
    uint8_t i;

    INT_CHG_TRICKLE_CURRENT_CONFIG* pCHG_TRICKLE_CURRENT = (INT_CHG_TRICKLE_CURRENT_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_INT_CHG_TRICKLE_CURRENT_CONFIG);

    if(!pCHG_TRICKLE_CURRENT)
        return;

    for(i = 0; i < pCHG_TRICKLE_CURRENT->CalCount; i++)
    {
        pCHG_TRICKLE_CURRENT->data[i].rchg_sel = adc_calculation(curr1, val1, curr2, val2, pCHG_TRICKLE_CURRENT->data[i].current);
        pCHG_TRICKLE_CURRENT->data[i].rchg_sel = range_protect(pCHG_TRICKLE_CURRENT->data[i].rchg_sel, 0, 1023, 2);
    }
    nvkey_write_data(NVKEYID_MP_CAL_INT_CHG_TRICKLE_CURRENT_CONFIG, pCHG_TRICKLE_CURRENT, sizeof(INT_CHG_TRICKLE_CURRENT_CONFIG));

    vPortFree(pCHG_TRICKLE_CURRENT);
}

void otp_cal_chg_cc1_current(uint16_t curr1, uint16_t val1, uint16_t curr2, uint16_t val2)
{
    uint8_t i;

    INT_CHG_CC1_CURRENT_CONFIG* pCHG_CC1_CURRENT = (INT_CHG_CC1_CURRENT_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_INT_CHG_CC1_CURRENT_CONFIG);

    if(!pCHG_CC1_CURRENT)
        return;

    for(i = 0; i < pCHG_CC1_CURRENT->CalCount; i++)
    {
        pCHG_CC1_CURRENT->data[i].rchg_sel = adc_calculation(curr1, val1, curr2, val2, pCHG_CC1_CURRENT->data[i].current);
        pCHG_CC1_CURRENT->data[i].rchg_sel = range_protect(pCHG_CC1_CURRENT->data[i].rchg_sel, 0, 1023, 2);
    }
    chg_cc_curr.cc1_curr = pCHG_CC1_CURRENT->data[pCHG_CC1_CURRENT->select - 1].current;
    nvkey_write_data(NVKEYID_MP_CAL_INT_CHG_CC1_CURRENT_CONFIG, pCHG_CC1_CURRENT, sizeof(INT_CHG_CC1_CURRENT_CONFIG));

    vPortFree(pCHG_CC1_CURRENT);
}

void otp_cal_chg_cc2_current(uint16_t curr1, uint16_t val1, uint16_t curr2, uint16_t val2)
{
    uint8_t i;

    INT_CHG_CC2_CURRENT_CONFIG* pCHG_CC2_CURRENT = (INT_CHG_CC2_CURRENT_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_INT_CHG_CC2_CURRENT_CONFIG);

    if(!pCHG_CC2_CURRENT)
        return;

    for(i = 0; i < pCHG_CC2_CURRENT->CalCount; i++)
    {
        pCHG_CC2_CURRENT->data[i].rchg_sel = adc_calculation(curr1, val1, curr2, val2, pCHG_CC2_CURRENT->data[i].current);
        pCHG_CC2_CURRENT->data[i].rchg_sel = range_protect(pCHG_CC2_CURRENT->data[i].rchg_sel, 0, 1023, 2);
    }
    chg_cc_curr.cc2_curr = pCHG_CC2_CURRENT->data[pCHG_CC2_CURRENT->select - 1].current;
    nvkey_write_data(NVKEYID_MP_CAL_INT_CHG_CC2_CURRENT_CONFIG, pCHG_CC2_CURRENT, sizeof(INT_CHG_CC2_CURRENT_CONFIG));

    vPortFree(pCHG_CC2_CURRENT);
}

void otp_cal_chg_cv_stop_current(void)
{
    uint16_t val = 0;

    OTP_VICHG_ADC_CONFIG* pOTP_VICHG_ADC = (OTP_VICHG_ADC_CONFIG*)pvPortMalloc(sizeof(OTP_VICHG_ADC_CONFIG));
    CHG_CV_STOP_CURRENT_ADC* pCHG_CV_STOP_CURRENT = (CHG_CV_STOP_CURRENT_ADC*)get_nvkey_data(NVKEYID_MP_CAL_CV_STOP_CURRENT_ADC);

    hal_flash_otp_read(OTP_VICHG_ADC_VALUE_ADDR, (uint8_t *)pOTP_VICHG_ADC, sizeof(OTP_VICHG_ADC_CONFIG));

    if((pOTP_VICHG_ADC->FT_Kflag != 1))
    {
        vPortFree(pOTP_VICHG_ADC);
        vPortFree(pCHG_CV_STOP_CURRENT);
        return;
    }

    if (vbat_slope.voltage_sel)
    {
        val = pOTP_VICHG_ADC->ADC_4_35V;
    }
    else
    {
        val = pOTP_VICHG_ADC->ADC_4_2V;
    }
    chg_vichg_adc.ADC_4_35V = pOTP_VICHG_ADC->ADC_4_35V;
    chg_vichg_adc.ADC_4_2V = pOTP_VICHG_ADC->ADC_4_2V;
    chg_vichg_adc.CV_stop_current_percent = pCHG_CV_STOP_CURRENT->CV_stop_current_percent;
    pCHG_CV_STOP_CURRENT->CV_stop_current_ADC = pCHG_CV_STOP_CURRENT->CV_stop_current_percent * val / 100;
    pCHG_CV_STOP_CURRENT->CV_stop_current_ADC = range_protect(pCHG_CV_STOP_CURRENT->CV_stop_current_ADC, 0, 1023, 2);

    nvkey_write_data(NVKEYID_MP_CAL_CV_STOP_CURRENT_ADC, pCHG_CV_STOP_CURRENT, sizeof(CHG_CV_STOP_CURRENT_ADC));

    vPortFree(pOTP_VICHG_ADC);
    vPortFree(pCHG_CV_STOP_CURRENT);
}

void otp_cal_chg_current(void)
{
    uint16_t curr1 = 0;
    uint16_t val1 = 0;
    uint16_t curr2 = 0;
    uint16_t val2 = 0;

    OTP_CHG_4V2_CURRENT_CONFIG* pOTP_CHG_4V2 = (OTP_CHG_4V2_CURRENT_CONFIG*)pvPortMalloc(sizeof(OTP_CHG_4V2_CURRENT_CONFIG));
    OTP_CHG_4V35_CURRENT_CONFIG* pOTP_CHG_4V35 = (OTP_CHG_4V35_CURRENT_CONFIG*)pvPortMalloc(sizeof(OTP_CHG_4V35_CURRENT_CONFIG));

    hal_flash_otp_read(OTP_CHG_4V2_CURRENT_ADDR, (uint8_t *)pOTP_CHG_4V2, sizeof(OTP_CHG_4V2_CURRENT_CONFIG));
    hal_flash_otp_read(OTP_CHG_4V35_CURRENT_ADDR, (uint8_t *)pOTP_CHG_4V35, sizeof(OTP_CHG_4V35_CURRENT_CONFIG));

    if((pOTP_CHG_4V2->FT_Kflag != 1) || (pOTP_CHG_4V35->FT_Kflag != 1))
    {
        vPortFree(pOTP_CHG_4V2);
        vPortFree(pOTP_CHG_4V35);
        return;
    }

    if (vbat_slope.voltage_sel)
    {
        curr1 = pOTP_CHG_4V35->current_value[0].dedicated_current;
        val1  = pOTP_CHG_4V35->current_value[0].estimated_measured_value;
        curr2 = pOTP_CHG_4V35->current_value[1].dedicated_current;
        val2  = pOTP_CHG_4V35->current_value[1].estimated_measured_value;
    }
    else
    {
        curr1 = pOTP_CHG_4V2->current_value[0].dedicated_current;
        val1  = pOTP_CHG_4V2->current_value[0].estimated_measured_value;
        curr2 = pOTP_CHG_4V2->current_value[1].dedicated_current;
        val2  = pOTP_CHG_4V2->current_value[1].estimated_measured_value;
    }

    chg_curr_4v35_slope.curr1 = pOTP_CHG_4V35->current_value[0].dedicated_current;
    chg_curr_4v35_slope.val1  = pOTP_CHG_4V35->current_value[0].estimated_measured_value;
    chg_curr_4v35_slope.curr2 = pOTP_CHG_4V35->current_value[1].dedicated_current;
    chg_curr_4v35_slope.val2  = pOTP_CHG_4V35->current_value[1].estimated_measured_value;
    chg_curr_4v2_slope.curr1  = pOTP_CHG_4V2->current_value[0].dedicated_current;
    chg_curr_4v2_slope.val1   = pOTP_CHG_4V2->current_value[0].estimated_measured_value;
    chg_curr_4v2_slope.curr2  = pOTP_CHG_4V2->current_value[1].dedicated_current;
    chg_curr_4v2_slope.val2   = pOTP_CHG_4V2->current_value[1].estimated_measured_value;

    otp_cal_chg_trickle_current(curr1, val1, curr2, val2);
    otp_cal_chg_cc1_current(curr1, val1, curr2, val2);
    otp_cal_chg_cc2_current(curr1, val1, curr2, val2);
    otp_cal_chg_cv_stop_current();

    vPortFree(pOTP_CHG_4V2);
    vPortFree(pOTP_CHG_4V35);
}

void otp_cal_chg_config(void)
{
    otp_cal_chg_dac();
    otp_cal_chg_current();
    otp_cal_chg_sys_ldo();
    otp_cal_chg_ocp();
}

void otp_cal_chg_jeita_warm(void)
{
    uint16_t sel_volt, curr_dac;
    uint16_t cc1_rsel_4v35, cc1_rsel_4v2, cc2_rsel_4v35, cc2_rsel_4v2, cv_stop_curr_adc;

    CHG_ADC_CONFIG* pCHG_ADC =(CHG_ADC_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_CHG_ADC_CONFIG);
    JEITA* pJEITA =(JEITA*)get_nvkey_data(NVKEYID_MP_CAL_JEITA);
    JEITA_WARM* pJEITA_WARM =(JEITA_WARM*)get_nvkey_data(NVKEYID_MP_CAL_JEITA_WARM);

    if(OTP_flag != 1)
    {
        vPortFree(pCHG_ADC);
        vPortFree(pJEITA);
        vPortFree(pJEITA_WARM);
        return;
    }

    pJEITA_WARM->JEITA_warm_cc2_threshold_voltage = pCHG_ADC->cc2_threshold_voltage + pCHG_ADC->cc2_offset_voltage - pJEITA->Warm_State_DAC_decrease;
    pJEITA_WARM->JEITA_warm_cv_threshold_voltage  = pCHG_ADC->cv_threshold_voltage+ pCHG_ADC->cv_thd_voltage_offset - pJEITA->Warm_State_DAC_decrease;
    pJEITA_WARM->JEITA_warm_full_bat_voltage      = pCHG_ADC->full_bat_voltage + pCHG_ADC->full_bat_voltage_offset - pJEITA->Warm_State_DAC_decrease;
    pJEITA_WARM->JEITA_warm_recharge_voltage      = pCHG_ADC->recharge_voltage+ pCHG_ADC->recharge_voltage_offset - pJEITA->Warm_State_DAC_decrease;
    pJEITA_WARM->JEITA_warm_full_bat_voltage_2    = pCHG_ADC->full_bat_voltage_2 + pCHG_ADC->full_bat_voltage_2_offset - pJEITA->Warm_State_DAC_decrease;

    pJEITA_WARM->JEITA_warm_cc2_threshold_ADC = adc_calculation(vbat_slope.volt1, vbat_slope.adc1, vbat_slope.volt2, vbat_slope.adc2, pJEITA_WARM->JEITA_warm_cc2_threshold_voltage);
    pJEITA_WARM->JEITA_warm_cv_threshold_ADC  = adc_calculation(vbat_slope.volt1, vbat_slope.adc1, vbat_slope.volt2, vbat_slope.adc2, pJEITA_WARM->JEITA_warm_cv_threshold_voltage);
    pJEITA_WARM->JEITA_warm_full_bat_ADC      = adc_calculation(vbat_slope.volt1, vbat_slope.adc1, vbat_slope.volt2, vbat_slope.adc2, pJEITA_WARM->JEITA_warm_full_bat_voltage);
    pJEITA_WARM->JEITA_warm_recharge_ADC      = adc_calculation(vbat_slope.volt1, vbat_slope.adc1, vbat_slope.volt2, vbat_slope.adc2, pJEITA_WARM->JEITA_warm_recharge_voltage);
    pJEITA_WARM->JEITA_warm_full_bat_ADC_2    = adc_calculation(vbat_slope.volt1, vbat_slope.adc1, vbat_slope.volt2, vbat_slope.adc2, pJEITA_WARM->JEITA_warm_full_bat_voltage_2);

    pJEITA_WARM->JEITA_warm_cc2_threshold_ADC = range_protect(pJEITA_WARM->JEITA_warm_cc2_threshold_ADC, 0, 1023, 2);
    pJEITA_WARM->JEITA_warm_cv_threshold_ADC  = range_protect(pJEITA_WARM->JEITA_warm_cv_threshold_ADC, 0, 1023, 2);
    pJEITA_WARM->JEITA_warm_full_bat_ADC      = range_protect(pJEITA_WARM->JEITA_warm_full_bat_ADC, 0, 1023, 2);
    pJEITA_WARM->JEITA_warm_recharge_ADC      = range_protect(pJEITA_WARM->JEITA_warm_recharge_ADC, 0, 1023, 2);
    pJEITA_WARM->JEITA_warm_full_bat_ADC_2    = range_protect(pJEITA_WARM->JEITA_warm_full_bat_ADC_2, 0, 1023, 2);

    if(vbat_slope.voltage_sel)//4.35v
    {
        sel_volt = 4350 - pJEITA->Warm_State_DAC_decrease;
    }
    else
    {
        sel_volt = 4200 - pJEITA->Warm_State_DAC_decrease;
    }
    curr_dac = adc_calculation(dac_slope.volt1, dac_slope.dac1, dac_slope.volt2, dac_slope.dac2, sel_volt);
    curr_dac = range_protect(curr_dac, 0, 1023, 2);

    pJEITA_WARM->JEITA_warm_cc1_current_DAC = curr_dac;
    pJEITA_WARM->JEITA_warm_cc2_current_DAC = curr_dac;
    pJEITA_WARM->JEITA_warm_cv_DAC          = curr_dac;

    pJEITA_WARM->JEITA_warm_cc1_current_1 = chg_cc_curr.cc1_curr * pJEITA->Warm_State_Current_Percent / 100;
    pJEITA_WARM->JEITA_warm_cc2_current_1 = chg_cc_curr.cc2_curr * pJEITA->Warm_State_Current_Percent / 100;

    cc1_rsel_4v2  = adc_calculation(chg_curr_4v2_slope.curr1, chg_curr_4v2_slope.val1, chg_curr_4v2_slope.curr2, chg_curr_4v2_slope.val2, pJEITA_WARM->JEITA_warm_cc1_current_1);
    cc1_rsel_4v2  = range_protect(cc1_rsel_4v35, 0, 1023, 2);
    cc1_rsel_4v35 = adc_calculation(chg_curr_4v35_slope.curr1, chg_curr_4v35_slope.val1, chg_curr_4v35_slope.curr2, chg_curr_4v35_slope.val2, pJEITA_WARM->JEITA_warm_cc1_current_1);
    cc1_rsel_4v35 = range_protect(cc1_rsel_4v35, 0, 1023, 2);
    pJEITA_WARM->JEITA_warm_rchg_sel_cc1_1 = adc_calculation(dac_slope.volt1, cc1_rsel_4v35, dac_slope.volt2, cc1_rsel_4v2, sel_volt);
    pJEITA_WARM->JEITA_warm_rchg_sel_cc1_1 = range_protect(pJEITA_WARM->JEITA_warm_rchg_sel_cc1_1, 0, 1023, 2);

    cc2_rsel_4v2  = adc_calculation(chg_curr_4v2_slope.curr1, chg_curr_4v2_slope.val1, chg_curr_4v2_slope.curr2, chg_curr_4v2_slope.val2, pJEITA_WARM->JEITA_warm_cc2_current_1);
    cc2_rsel_4v2  = range_protect(cc2_rsel_4v35, 0, 1023, 2);
    cc2_rsel_4v35 = adc_calculation(chg_curr_4v35_slope.curr1, chg_curr_4v35_slope.val1, chg_curr_4v35_slope.curr2, chg_curr_4v35_slope.val2, pJEITA_WARM->JEITA_warm_cc2_current_1);
    cc2_rsel_4v35 = range_protect(cc2_rsel_4v35, 0, 1023, 2);
    pJEITA_WARM->JEITA_warm_rchg_sel_cc2_1 = adc_calculation(4350, cc2_rsel_4v35, 4200, cc2_rsel_4v2, sel_volt);
    pJEITA_WARM->JEITA_warm_rchg_sel_cc2_1 = range_protect(pJEITA_WARM->JEITA_warm_rchg_sel_cc2_1, 0, 1023, 2);

    cv_stop_curr_adc = adc_calculation(4350, chg_vichg_adc.ADC_4_35V, 4200, chg_vichg_adc.ADC_4_2V, sel_volt);
    cv_stop_curr_adc = range_protect(cv_stop_curr_adc, 0, 1023, 2);
    pJEITA_WARM->JEITA_warm_CV_stop_current_ADC = mpk_round((cv_stop_curr_adc * chg_vichg_adc.CV_stop_current_percent), 100);

    nvkey_write_data(NVKEYID_MP_CAL_JEITA_WARM, pJEITA_WARM, sizeof(JEITA_WARM));

    vPortFree(pCHG_ADC);
    vPortFree(pJEITA);
    vPortFree(pJEITA_WARM);
}

void otp_cal_chg_jeita_cool(void)
{
    uint16_t sel_volt, curr_dac;
    uint16_t cc1_rsel_4v35, cc1_rsel_4v2, cc2_rsel_4v35, cc2_rsel_4v2, cv_stop_curr_adc;

    CHG_ADC_CONFIG* pCHG_ADC =(CHG_ADC_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_CHG_ADC_CONFIG);
    JEITA* pJEITA =(JEITA*)get_nvkey_data(NVKEYID_MP_CAL_JEITA);
    JEITA_COOL* pJEITA_COOL =(JEITA_COOL*)get_nvkey_data(NVKEYID_MP_CAL_JEITA_COOL);

    if(OTP_flag != 1)
    {
        vPortFree(pCHG_ADC);
        vPortFree(pJEITA);
        vPortFree(pJEITA_COOL);
        return;
    }

    pJEITA_COOL->JEITA_cool_cc2_threshold_voltage = pCHG_ADC->cc2_threshold_voltage + pCHG_ADC->cc2_offset_voltage - pJEITA->Cool_State_DAC_decrease;
    pJEITA_COOL->JEITA_cool_cv_threshold_voltage  = pCHG_ADC->cv_threshold_voltage+ pCHG_ADC->cv_thd_voltage_offset - pJEITA->Cool_State_DAC_decrease;
    pJEITA_COOL->JEITA_cool_full_bat_voltage      = pCHG_ADC->full_bat_voltage + pCHG_ADC->full_bat_voltage_offset - pJEITA->Cool_State_DAC_decrease;
    pJEITA_COOL->JEITA_cool_recharge_voltage      = pCHG_ADC->recharge_voltage+ pCHG_ADC->recharge_voltage_offset - pJEITA->Cool_State_DAC_decrease;
    pJEITA_COOL->JEITA_cool_full_bat_voltage_2    = pCHG_ADC->full_bat_voltage_2 + pCHG_ADC->full_bat_voltage_2_offset - pJEITA->Cool_State_DAC_decrease;

    pJEITA_COOL->JEITA_cool_cc2_threshold_ADC = adc_calculation(vbat_slope.volt1, vbat_slope.adc1, vbat_slope.volt2, vbat_slope.adc2, pJEITA_COOL->JEITA_cool_cc2_threshold_voltage);
    pJEITA_COOL->JEITA_cool_cv_threshold_ADC  = adc_calculation(vbat_slope.volt1, vbat_slope.adc1, vbat_slope.volt2, vbat_slope.adc2, pJEITA_COOL->JEITA_cool_cv_threshold_voltage);
    pJEITA_COOL->JEITA_cool_full_bat_ADC      = adc_calculation(vbat_slope.volt1, vbat_slope.adc1, vbat_slope.volt2, vbat_slope.adc2, pJEITA_COOL->JEITA_cool_full_bat_voltage);
    pJEITA_COOL->JEITA_cool_recharge_ADC      = adc_calculation(vbat_slope.volt1, vbat_slope.adc1, vbat_slope.volt2, vbat_slope.adc2, pJEITA_COOL->JEITA_cool_recharge_voltage);
    pJEITA_COOL->JEITA_cool_full_bat_ADC_2    = adc_calculation(vbat_slope.volt1, vbat_slope.adc1, vbat_slope.volt2, vbat_slope.adc2, pJEITA_COOL->JEITA_cool_full_bat_voltage_2);

    pJEITA_COOL->JEITA_cool_cc2_threshold_ADC = range_protect(pJEITA_COOL->JEITA_cool_cc2_threshold_ADC, 0, 1023, 2);
    pJEITA_COOL->JEITA_cool_cv_threshold_ADC  = range_protect(pJEITA_COOL->JEITA_cool_cv_threshold_ADC, 0, 1023, 2);
    pJEITA_COOL->JEITA_cool_full_bat_ADC      = range_protect(pJEITA_COOL->JEITA_cool_full_bat_ADC, 0, 1023, 2);
    pJEITA_COOL->JEITA_cool_recharge_ADC      = range_protect(pJEITA_COOL->JEITA_cool_recharge_ADC, 0, 1023, 2);
    pJEITA_COOL->JEITA_cool_full_bat_ADC_2    = range_protect(pJEITA_COOL->JEITA_cool_full_bat_ADC_2, 0, 1023, 2);

    if(vbat_slope.voltage_sel)//4.35v
    {
        sel_volt = 4350 - pJEITA->Cool_State_DAC_decrease;
    }
    else
    {
        sel_volt = 4200 - pJEITA->Cool_State_DAC_decrease;
    }
    curr_dac = adc_calculation(dac_slope.volt1, dac_slope.dac1, dac_slope.volt2, dac_slope.dac2, sel_volt);
    curr_dac = range_protect(curr_dac, 0, 1023, 2);

    pJEITA_COOL->JEITA_cool_cc1_current_DAC = curr_dac;
    pJEITA_COOL->JEITA_cool_cc2_current_DAC = curr_dac;
    pJEITA_COOL->JEITA_cool_cv_DAC          = curr_dac;

    pJEITA_COOL->JEITA_cool_cc1_current_1 = chg_cc_curr.cc1_curr * pJEITA->Cool_State_Current_Percent / 100;
    pJEITA_COOL->JEITA_cool_cc2_current_1 = chg_cc_curr.cc2_curr * pJEITA->Cool_State_Current_Percent / 100;

    cc1_rsel_4v2  = adc_calculation(chg_curr_4v2_slope.curr1, chg_curr_4v2_slope.val1, chg_curr_4v2_slope.curr2, chg_curr_4v2_slope.val2, pJEITA_COOL->JEITA_cool_cc1_current_1);
    cc1_rsel_4v2  = range_protect(cc1_rsel_4v35, 0, 1023, 2);
    cc1_rsel_4v35 = adc_calculation(chg_curr_4v35_slope.curr1, chg_curr_4v35_slope.val1, chg_curr_4v35_slope.curr2, chg_curr_4v35_slope.val2, pJEITA_COOL->JEITA_cool_cc1_current_1);
    cc1_rsel_4v35 = range_protect(cc1_rsel_4v35, 0, 1023, 2);
    pJEITA_COOL->JEITA_cool_rchg_sel_cc1_1 = adc_calculation(dac_slope.volt1, cc1_rsel_4v35, dac_slope.volt2, cc1_rsel_4v2, sel_volt);
    pJEITA_COOL->JEITA_cool_rchg_sel_cc1_1 = range_protect(pJEITA_COOL->JEITA_cool_rchg_sel_cc1_1, 0, 1023, 2);

    cc2_rsel_4v2  = adc_calculation(chg_curr_4v2_slope.curr1, chg_curr_4v2_slope.val1, chg_curr_4v2_slope.curr2, chg_curr_4v2_slope.val2, pJEITA_COOL->JEITA_cool_cc2_current_1);
    cc2_rsel_4v2  = range_protect(cc2_rsel_4v35, 0, 1023, 2);
    cc2_rsel_4v35 = adc_calculation(chg_curr_4v35_slope.curr1, chg_curr_4v35_slope.val1, chg_curr_4v35_slope.curr2, chg_curr_4v35_slope.val2, pJEITA_COOL->JEITA_cool_cc2_current_1);
    cc2_rsel_4v35 = range_protect(cc2_rsel_4v35, 0, 1023, 2);
    pJEITA_COOL->JEITA_cool_rchg_sel_cc2_1 = adc_calculation(4350, cc2_rsel_4v35, 4200, cc2_rsel_4v2, sel_volt);
    pJEITA_COOL->JEITA_cool_rchg_sel_cc2_1 = range_protect(pJEITA_COOL->JEITA_cool_rchg_sel_cc2_1, 0, 1023, 2);

    cv_stop_curr_adc = adc_calculation(4350, chg_vichg_adc.ADC_4_35V, 4200, chg_vichg_adc.ADC_4_2V, sel_volt);
    cv_stop_curr_adc = range_protect(cv_stop_curr_adc, 0, 1023, 2);
    pJEITA_COOL->JEITA_cool_CV_stop_current_ADC = cv_stop_curr_adc * chg_vichg_adc.CV_stop_current_percent / 100;

    nvkey_write_data(NVKEYID_MP_CAL_JEITA_COOL, pJEITA_COOL, sizeof(JEITA_COOL));

    vPortFree(pCHG_ADC);
    vPortFree(pJEITA);
    vPortFree(pJEITA_COOL);
}

void otp_cal_chg_jeita(void)
{
    otp_cal_chg_jeita_warm();
    otp_cal_chg_jeita_cool();
}

void otp_cal_done(void)
{
    CHG_CONFIG* pChg_config = (CHG_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_CHG_CONFIG);

    if(!pChg_config)
        return;

    if(pChg_config->Kflag != DEFAULT_TAG)
    {
        vPortFree(pChg_config);
        return;
    }
    pChg_config->Kflag = FT_FW_K;

    nvkey_write_data(NVKEYID_MP_CAL_CHG_CONFIG, pChg_config, sizeof(CHG_CONFIG));
    vPortFree(pChg_config);
}

void otp_calibration(void)
{
    otp_cal_vbat_config();
    if ((mpk_flag != DEFAULT_TAG) || (OTP_flag != 1))
        return;
    otp_cal_buck_config();
    otp_cal_ldo_config();
    otp_cal_bg_config();
    otp_cal_chg_config();
    otp_cal_chg_jeita();
    otp_cal_done();
}

