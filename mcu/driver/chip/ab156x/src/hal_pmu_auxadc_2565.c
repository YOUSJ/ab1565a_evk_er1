/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */


#include "hal.h"
#ifdef HAL_PMU_MODULE_ENABLED
#include "assert.h"
#include <string.h>
auxadc_efuse_data_stru gAuxadcEfuseInfo; /*data : auxadc efuse struct*/

/*========[Auxadc basic setting]========*/
void pmu_auxadc_init(void) {
    //OS_Semaphore_Init(pAux_Semaphore, 1);
    pmu_set_register_value_2565(ADC_ONE_SHOT_START_ADDR, ADC_ONE_SHOT_START_MASK, ADC_ONE_SHOT_START_SHIFT, 1);//set ready bit
}


/*========[Auxadc basic function]========*/
uint32_t pmu_auxadc_get_channel_value(pmu_adc_channel_t Channel)
{
    uint16_t adc_val = 0xFFFF;
    uint16_t tmp, adc_sta, err_sta = 0;
    uint16_t rg_0x418 = 0, rg_0x604 = 0;
    uint32_t time_start = 0;
    uint32_t time_diff = 0;
    uint32_t time0 = 0;
    uint32_t time1 = 0;
    uint32_t time_diff1 = 0;
    uint32_t time_diff2 = 0;
    uint32_t time_diff3 = 0;
    //uint32_t oldPriority;

    //OS_Semaphore_Take(pAux_Semaphore);

    if ((Channel >= PMU_AUX_MAX) || (Channel < PMU_AUX_VICHG)) {
        log_hal_msgid_info("pmu_auxadc_get_channel_value, [PMIC]Error auxadc chnnel[%d]", 1, Channel);
        return adc_val;
    }

    /*oldPriority = uxTaskPriorityGet(NULL);
    if ((oldPriority < LC_PROCESS_TASK_PRIORITY) && (Channel & PMU_AUX_VCHG))
        vTaskPrioritySet(NULL,LC_PROCESS_TASK_PRIORITY);*/

    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time_start);
    while(1)
    {
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time1);
        adc_sta = pmu_get_register_value_2565(ADC_RDY_STS0_ADDR, ADC_RDY_STS0_MASK, ADC_RDY_STS0_SHIFT);
        if (adc_sta == 1)
        {
            time_diff1 = time1 - time_start;
            break;
        }
        else if (time1 - time_start > 10000)
        {
            time_diff1 = time1 - time_start;
            err_sta |= 1;
            goto RETURN_ERRCODE;
        }
    }

    tmp = pmu_get_register_value_2565(ADC_CON3, 0xFFFF, 0);
    tmp &= 0x14;//clean all ch & w1c
    tmp |= ((1 << (Channel-10)) | 0x0001);//set ch & one shot start
    pmu_force_set_register_value_2565(ADC_CON3, tmp);

    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time0);
    while(1)
    {
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time1);
        adc_sta = pmu_get_register_value_2565(ADC_RDY_STS0_ADDR, ADC_RDY_STS0_MASK, ADC_RDY_STS0_SHIFT);
        if (adc_sta == 0)
        {
            time_diff2 = time1 - time0;
            break;
        }
        else if (time1 - time0 > 200)
        {
            time_diff2 = time1 - time0;
            err_sta |= 2;
            break;
        }
    }

    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time0);
    while(1)
    {
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time1);
        adc_sta = pmu_get_register_value_2565(ADC_RDY_STS0_ADDR, ADC_RDY_STS0_MASK, ADC_RDY_STS0_SHIFT);
        if (adc_sta == 1)
        {
            time_diff3 = time1 - time0;
            break;
        }
        else if (time1 - time0 > 10000)
        {
            time_diff3 = time1 - time0;
            err_sta |= 4;
            goto RETURN_ERRCODE;
        }
    }

    /*if ((oldPriority < LC_PROCESS_TASK_PRIORITY) && (Channel & PMU_AUX_VCHG))
        vTaskPrioritySet(NULL,oldPriority);*/

    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time_diff);
    time_diff -= time_start;

    switch(Channel) {
        case PMU_AUX_VICHG:
            adc_val = pmu_get_register_value_2565(ADC_VALUE0_ADDR, ADC_VALUE0_MASK, ADC_VALUE0_SHIFT);
            log_hal_msgid_info("pmu_auxadc_get_channel_value time:[%dus], VICHG:[%d]", 2, time_diff, adc_val);
        break;

        case PMU_AUX_VBAT:
            adc_val = pmu_get_register_value_2565(ADC_VALUE1_ADDR, ADC_VALUE1_MASK, ADC_VALUE1_SHIFT);
            log_hal_msgid_info("pmu_auxadc_get_channel_value time:[%dus], VBAT:[%d]", 2, time_diff, adc_val);
        break;

        case PMU_AUX_VCHG:
            adc_val = pmu_get_register_value_2565(ADC_VALUE2_ADDR, ADC_VALUE2_MASK, ADC_VALUE2_SHIFT);
            log_hal_msgid_info("pmu_auxadc_get_channel_value time:[%dus], VCHG:[%d]", 2, time_diff, adc_val);
        break;

        case PMU_AUX_LDO_BUCK:
            adc_val = pmu_get_register_value_2565(ADC_VALUE3_ADDR, ADC_VALUE3_MASK, ADC_VALUE3_SHIFT);
            log_hal_msgid_info("pmu_auxadc_get_channel_value time:[%dus], LDO_BUCK:[%d]", 2, time_diff, adc_val);
        break;

        case PMU_AUX_VBAT_CAL:
            adc_val = pmu_get_register_value_2565(ADC_VALUE4_ADDR, ADC_VALUE4_MASK, ADC_VALUE4_SHIFT);
            log_hal_msgid_info("pmu_auxadc_get_channel_value time:[%dus], VBAT_CAL:[%d]", 2, time_diff, adc_val);
        break;

        case PMU_AUX_VIN:
            adc_val = pmu_get_register_value_2565(ADC_VALUE5_ADDR, ADC_VALUE5_MASK, ADC_VALUE5_SHIFT);
            log_hal_msgid_info("pmu_auxadc_get_channel_value time:[%dus], VIN:[%d]", 2, time_diff, adc_val);
        break;

        default:
        break;
    }
    RETURN_ERRCODE:
    if (err_sta > 0)
    {
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time_diff);
        time_diff -= time_start;
        rg_0x418 = pmu_get_register_value_2565(ADC_STATE0, 0xFFFF, 0);
        rg_0x604 = pmu_get_register_value_2565(PMU_TEST2, 0xFFFF, 0);
        log_hal_msgid_info("pmu_auxadc_get_channel_value error, time:[%dus], VAL:[%d], state:[0x%x], 0x418:[0x%x], 0x604:[0x%x], diff1:[%d], diff2:[%d], diff3:[%d]", 8,
            time_diff, adc_val, err_sta, rg_0x418, rg_0x604,time_diff1,time_diff2,time_diff3);

        if (time_diff > 2000000)
            assert(0);
    }
    //OS_Semaphore_Give(pAux_Semaphore);

    return adc_val;
}


/*========[Auxadc Algorithm]========*/
uint32_t pmu_auxadc_voltage_transfer(int16_t value) {
    uint32_t miniVoltage = -1;
    miniVoltage = (value * 1800) / 4096;
    return miniVoltage;
}







#endif /* HAL_PMU_AUXADC_MODULE_ENABLED */