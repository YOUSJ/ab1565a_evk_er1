/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_captouch.h" 

#ifdef HAL_CAPTOUCH_MODULE_ENABLED
#include "hal_captouch_internal.h"
#include "hal_rtc_internal.h"

#include "hal_pmu.h"
#include "hal_gpt.h"
#include <string.h>
#include "hal_log.h"
#include "hal_nvic.h"
#define UNUSED(p) ((void)(p))

//HW basic configuration
extern uint8_t pmu_get_power_on_reason();
extern GPIO_CFG0_REGISTER_T *gpio_cfg0;
extern TimerHandle_t captouch_fine_base_delay100ms_timer;
extern TimerHandle_t captouch_debounce_timer_ch0;
extern TimerHandle_t captouch_debounce_timer_ch1;
extern TimerHandle_t captouch_debounce_timer_ch2;
extern TimerHandle_t captouch_debounce_timer_ch3;

hal_captouch_status_t hal_captouch_set_avg(hal_captouch_channel_t channel,uint8_t mavg_val, uint8_t avg_val)
{
    if (mavg_val > CAPTOUCH_MAVG_MAX) {
        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }
    if (avg_val > CAPTOUCH_AVG_MAX) {
        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }
    captouch_channel_sense_control((1<<channel), captouch_disable);
    captouch_set_mavg(channel,mavg_val);
    captouch_set_avg(channel,avg_val);
    captouch_channel_sense_control((1<<channel), captouch_enable);
    return HAL_CAPTOUCH_STATUS_OK;
}

hal_captouch_status_t hal_captouch_init(hal_captouch_config_t *config)
{
    uint32_t i;
    hal_captouch_status_t ret;
    bool capcon_state;
    hal_captouch_tune_data_t tune_data;
    //hal_captouch_config_t config_temp;
    if (captouch_context.has_initilized == true) {
        return HAL_CAPTOUCH_STATUS_INITIALIZED;
    }

    if (config->callback.callback == NULL) {
        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }
    capcon_state = rtc_internal_captouch_get_capcon_state();
    memset(&captouch_context, 0, sizeof(captouch_context_t));
    //get ept channel bit map
    captouch_context.used_channel_map = config->channel_bit_map;
    captouch_context.captouch_callback.callback  = config->callback.callback;
    captouch_context.captouch_callback.user_data = config->callback.user_data;
    //open captouch annalog clock

    ret = captouch_rtc_clk_control(captouch_enable);
    if (ret != HAL_CAPTOUCH_STATUS_OK) {
        return ret;
    }

    captouch_analog_init();
    captouch_channel_sense_control(captouch_context.used_channel_map, captouch_disable);
    //set threshold and coarse_cap
    for(i=0;i<4;i++) {

        if (captouch_context.used_channel_map & (1<<i)) {
            captouch_context.avg_s[i] = config->avg_s[i];
            captouch_context.mavg_r[i] = config->mavg_r[i];
            ret = hal_captouch_set_threshold(i,config->high_thr[i],config->low_thr[i]);
            if (ret != HAL_CAPTOUCH_STATUS_OK) {
                return ret;
            }
            ret = hal_captouch_set_coarse_cap(i,config->coarse_cap[i]);
            if (ret != HAL_CAPTOUCH_STATUS_OK) {
                return ret;
            }
            ret = hal_captouch_set_avg(i,config->mavg_r[i],config->avg_s[i]);
            if (ret != HAL_CAPTOUCH_STATUS_OK) {
                return ret;
            }
            if( capcon_state ){
                captouch_set_fine_cap(i,config->fine_cap[i]);
                captouch_set_control_manual(i,true);
                //printf("captouch_set_fine_cap =%d channel =%d",config->fine_cap[i],i);
            }
            if(i==1) {
                gpio_cfg0->GPIO_G.CLR = (1<<3);
            }
            if(i==2) {
                gpio_cfg0->GPIO_G.CLR = (1<<4);
            }
            if(i==3) {
                gpio_cfg0->GPIO_G.CLR = (1<<5);
            }
        }
    }
    captouch_channel_sense_control(captouch_context.used_channel_map, captouch_enable);
    //ret = hal_captouch_set_avg(i,config->mavg_r[i],config->avg_s[i]);
    //captouch_channel_sense_control(captouch_context.used_channel_map, captouch_enable);
    captouch_channel_int_control(captouch_context.used_channel_map, captouch_enable);

#if (PRODUCT_VERSION == 2822)
    pmu_set_register_value(PMU_CAP_LPSD_CON0,0x3,0,0x1);
    pmu_set_register_value(PMU_CAP_LPSD_CON1,0x3,0,0);
#endif

    captouch_set_dynamic_threshold(true,true,0x28,0);
    captouch_longpress_int_control(true);
    captouch_longpress_channel_control(HAL_CAPTOUCH_LONGPRESS_SHUTDOWN,128000);
    captouch_longpress_channel_control(HAL_CAPTOUCH_LONGPRESS_WAKEUP,96000);
    captouch_clk_control(true);

    hal_gpt_delay_ms(10);

    if( !capcon_state ){
        captouch_set_autok_suspend(config->channel_bit_map,false);
        for(i=0;i<4;i++) {
            if (config->channel_bit_map & (1<<i)) {
                hal_captouch_set_avg(i,5,config->avg_s[i]);
                captouch_find_baseline(i);
                tune_data.coarse_cap = config->coarse_cap[i];
                hal_captouch_tune_control(i, HAL_CAPTOUCH_TUNE_HW_AUTO,&tune_data);
                hal_captouch_set_avg(i,config->mavg_r[i],config->avg_s[i]);
            }
        }
    }

    for(i=0;i<4;i++) {
        if(config->channel_bit_map & (1<<i)){
            switch(i){
                case HAL_CAPTOUCH_CHANNEL_0:
                    captouch_debounce_timer_ch0 = xTimerCreate("captouchdeb_ch0", pdMS_TO_TICKS(40), pdFALSE, NULL, captouch_key_press_event_handler);//normal_setting->swDebounceTime
                break;
                case HAL_CAPTOUCH_CHANNEL_1:
                    captouch_debounce_timer_ch1 = xTimerCreate("captouchdeb_ch1", pdMS_TO_TICKS(40), pdFALSE, NULL, captouch_key_press_event_handler);
                break;
                case HAL_CAPTOUCH_CHANNEL_2:
                    captouch_debounce_timer_ch2 = xTimerCreate("captouchdeb_ch2", pdMS_TO_TICKS(40), pdFALSE, NULL, captouch_key_press_event_handler);
                break;
                case HAL_CAPTOUCH_CHANNEL_3:
                    captouch_debounce_timer_ch3 = xTimerCreate("captouchdeb_ch3", pdMS_TO_TICKS(40), pdFALSE, NULL, captouch_key_press_event_handler);
                break;
                default:
                break;
            }
        }
    }

    //register nvic irq handler and enable irq
    captouch_register_nvic_callback();


    //auto suspend
    captouch_set_autok_suspend(captouch_context.used_channel_map,true);

    if( capcon_state && (!captouch_get_channel_trigger(0)) ){
        //printf("captouch after rtc close captouch_tune_auto_control ");
        for(i=0;i<4;i++){
            if (config->channel_bit_map & (1<<i)){
                captouch_set_control_manual(i,false);
                hal_gpt_delay_ms(10);
                hal_captouch_set_avg(i,5,config->avg_s[i]);
                captouch_find_baseline(i);
                //printf("captouch rtc write captouch_get_fine_cap =%d ",captouch_get_fine_cap(i));
                captouch_write_nvdm(CAPTOUCH_FINE_CAP_ID,i,captouch_get_fine_cap(i));
                hal_captouch_set_avg(i,config->mavg_r[i],config->avg_s[i]);
            }
        }
    }

    captouch_fine_base_delay100ms_timer = xTimerCreate("fine_base_delay100ms", pdMS_TO_TICKS(100), pdFALSE, NULL, captouch_fine_base_delay100ms_handler);

    //captouch_set_autok_Nsuspend(captouch_context.used_channel_map,true);

    captouch_int_control(true);

    //2822 sony
#if (PRODUCT_VERSION == 2822)
    captouch_set_lpsd_chk_vbus(true);
#endif

    captouch_context.has_initilized = true;

    //printf("captouch setting end");
    return HAL_CAPTOUCH_STATUS_OK;

}



hal_captouch_status_t hal_captouch_channel_enable(hal_captouch_channel_t channel)
{
    if (channel>= HAL_CAPTOUCH_CHANNEL_MAX) {

        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    //touch channel enable&aio en
    captouch_channel_sense_control(1<<channel, captouch_enable);

    //enable high and low threhold wakeup&int interrupt
    captouch_channel_int_control(1<<channel, captouch_enable);

    return HAL_CAPTOUCH_STATUS_OK;
}

hal_captouch_status_t hal_captouch_channel_disable(hal_captouch_channel_t channel)
{
    if (channel>= HAL_CAPTOUCH_CHANNEL_MAX) {

        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    //disable chanel sensing
    captouch_channel_sense_control(channel, captouch_disable);

    //mask hign and low threhold wakeup&int interrupt
    captouch_channel_int_control(1<<channel, captouch_disable);

    return HAL_CAPTOUCH_STATUS_OK;
}

hal_captouch_status_t hal_captouch_get_event(hal_captouch_event_t *event)
{
    if (event == NULL) {
        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    if (captouch_get_buffer_data_size()<=0) {
        return HAL_CAPTOUCH_STATUS_NO_EVENT;
    }

    captouch_pop_one_event_from_buffer(event);

    return HAL_CAPTOUCH_STATUS_OK;
}


hal_captouch_status_t hal_captouch_translate_channel_to_symbol(hal_captouch_channel_t channel, uint8_t *symbol)
{
    if (channel>= HAL_CAPTOUCH_CHANNEL_MAX) {

        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    *symbol = captouch_mapping_keydata[channel];

    return HAL_CAPTOUCH_STATUS_OK;
}

hal_captouch_status_t hal_captouch_set_threshold(hal_captouch_channel_t channel,int32_t high_thr, int32_t low_thr)
{
    if (channel >= HAL_CAPTOUCH_CHANNEL_MAX) {
        return HAL_CAPTOUCH_STATUS_CHANNEL_ERROR;
    }

    if ((high_thr > 255) || (high_thr < (-256)) ||(low_thr>255) || (low_thr< (-256))) {
        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    captouch_set_threshold(channel, high_thr, low_thr);

    return HAL_CAPTOUCH_STATUS_OK;
}

#ifdef HAL_CPT_FEATURE_4CH
hal_captouch_status_t hal_captouch_set_Nthreshold(hal_captouch_channel_t channel,int32_t high_thr, int32_t low_thr)
{
    if (channel >= HAL_CAPTOUCH_CHANNEL_MAX) {
        return HAL_CAPTOUCH_STATUS_CHANNEL_ERROR;
    }

    if ((high_thr > 255) || (high_thr < (-256)) ||(low_thr>255) || (low_thr< (-256))) {
        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    captouch_set_nthreshold(channel, high_thr, low_thr);

    return HAL_CAPTOUCH_STATUS_OK;
}
#endif

hal_captouch_status_t hal_captouch_set_fine_cap(hal_captouch_channel_t channel,int32_t fine)
{
    if (channel >= HAL_CAPTOUCH_CHANNEL_MAX) {
        return HAL_CAPTOUCH_STATUS_CHANNEL_ERROR;
    }

    if ((fine > 63) || (fine<(-64))) {
        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    captouch_set_fine_cap(channel,fine);
    return HAL_CAPTOUCH_STATUS_OK;
}

hal_captouch_status_t hal_captouch_set_coarse_cap(hal_captouch_channel_t channel, uint32_t coarse)
{
    if (channel >= HAL_CAPTOUCH_CHANNEL_MAX) {
        return HAL_CAPTOUCH_STATUS_CHANNEL_ERROR;
    }

    if (coarse > CAPTOUCH_COARSE_CAP_MAX) {
        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    captouch_set_coarse_cap(channel,(uint8_t)coarse);

    return HAL_CAPTOUCH_STATUS_OK;
}


hal_captouch_status_t hal_captouch_tune_control(hal_captouch_channel_t channel,hal_captouch_tune_type_t tune_type, hal_captouch_tune_data_t* data)
{
    hal_captouch_status_t ret;
    uint32_t count;
    UNUSED(tune_type);
    ret =0;
    count = 0;
    while(1) {
        ++count;
        ret = captouch_hw_auto_tune(channel,data);
        if(ret)break;
        if(count>=3)break;
    }
    //printf("captouch coarse cap control =%d  fine cap = %d channel =%d ", config->coarse_cap[channel],captouch_get_fine_cap(channel),channel);
    captouch_write_nvdm(CAPTOUCH_COARSE_CAP_ID,channel,data->coarse_cap);
    captouch_write_nvdm(CAPTOUCH_FINE_CAP_ID,channel,captouch_get_fine_cap(channel));

    return ret;
}


#if 0
hal_captouch_status_t hal_captouch_tune_control(hal_captouch_channel_t channel,hal_captouch_tune_type_t tune_type, hal_captouch_tune_data_t* data)
{
    hal_captouch_status_t ret;

    if (channel >= HAL_CAPTOUCH_CHANNEL_MAX) {
        return HAL_CAPTOUCH_STATUS_CHANNEL_ERROR;
    }

    if (data == NULL) {
        return HAL_CAPTOUCH_STATUS_ERROR;
    }

    if (captouch_context.has_initilized != true) {
        return HAL_CAPTOUCH_STATUS_UNINITIALIZED;
    }

    ret = HAL_CAPTOUCH_STATUS_ERROR;

    hal_nvic_disable_irq(CAP_TOUCH_IRQn);
    captouch_set_autok_suspend(captouch_context.used_channel_map,false);

    //mask interrupt & wakeup
    captouch_channel_int_control(1<<channel, captouch_disable);
    hal_gpt_delay_ms(2);

    captouch_switch_debug_sel(channel);

    if (tune_type == HAL_CAPTOUCH_TUNE_HW_AUTO) {
        if (captouch_hw_auto_tune(channel,data) == true) {
            ret = HAL_CAPTOUCH_STATUS_OK;
        }
    }
    else if(tune_type == HAL_CAPTOUCH_TUNE_SW_AUTO) {
        if (captouch_sw_auto_tune(channel,data) == true) {
            ret = HAL_CAPTOUCH_STATUS_OK;
        }
    }

    captouch_channel_int_control(1<<channel, captouch_enable);
    hal_gpt_delay_ms(2);

    captouch_hif->TOUCH_HIF_CON0.CELLS.HIF_SOFT_RST = 1;
    hal_gpt_delay_ms(3);

    hal_nvic_enable_irq(CAP_TOUCH_IRQn);
    captouch_set_autok_suspend(captouch_context.used_channel_map,true);

    return ret;
}
#endif
hal_captouch_status_t hal_captouch_lowpower_control(hal_captouch_lowpower_type_t lowpower_type)
{
    if (captouch_context.has_initilized != true) {
        return HAL_CAPTOUCH_STATUS_UNINITIALIZED;
    }

    captouch->TOUCH_CON0.CON0 = 0;

    if (HAL_CAPTOUCH_MODE_NORMAL == lowpower_type) {
        captouch_rtc_lpm_control(HAL_CAPTOUCH_MODE_NORMAL);
        captouch->TOUCH_ANACFG0.ANACFG0 = (1<<16) | 0x2e;
        captouch_set_clk(HAL_CAPTOUCH_MODE_NORMAL,HAL_CAPTOUCH_CLK_32K);
        captouch_channel_sense_control(captouch_context.used_channel_map, captouch_enable);
        captouch_clk_control(true);
        captouch_int_control(true);
    }
    else if (HAL_CAPTOUCH_MODE_LOWPOWER == lowpower_type) {
        captouch_rtc_lpm_control(HAL_CAPTOUCH_MODE_LOWPOWER);
        captouch->TOUCH_ANACFG0.ANACFG0 = (1<<16) | 0x3f;
        captouch_set_clk(HAL_CAPTOUCH_MODE_NORMAL,HAL_CAPTOUCH_CLK_4K);
        hal_captouch_set_avg(HAL_CAPTOUCH_CHANNEL_0,4,5);
        captouch_channel_sense_control(1<<HAL_CAPTOUCH_CHANNEL_0, captouch_enable);
        captouch_clk_control(true);
        hal_gpt_delay_ms(200);
        captouch_int_control(true);
        captouch_set_autok_suspend(0x1,true);
    }

    return HAL_CAPTOUCH_STATUS_OK;

}

hal_captouch_status_t hal_captouch_deinit(void)
{
    captouch_analog_deinit();
    captouch_rtc_clk_control(captouch_disable);
    
    //disable channel interrupt
    captouch_channel_int_control(captouch_context.used_channel_map, captouch_disable);

    //disable channel calibration
    captouch_channel_sense_control(captouch_context.used_channel_map, captouch_disable);

    //close the captouch clock
    captouch_clk_control(captouch_disable);

    //disable the captouch inerrupt
    captouch_int_control(captouch_disable);

    captouch_context.has_initilized = false;

    memset(&captouch_context, 0, sizeof(captouch_context_t));

    return HAL_CAPTOUCH_STATUS_OK;
}




#endif //HAL_CAPTOUCH_MODULE_ENABLED

