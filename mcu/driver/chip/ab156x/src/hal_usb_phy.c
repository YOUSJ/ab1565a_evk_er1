/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#include "hal_usb.h"
#include "hal_usb_internal.h"
#include "hal_eint.h"
#include "hal_log.h"
#include "hal_gpt.h"


#ifdef HAL_USB_MODULE_ENABLED

void hal_usbphy_poweron_initialize(void)
{
    #if 1
    DRV_WriteReg32(0xA2090070, 0x01220121);
    hal_gpt_delay_ms(1);
    DRV_WriteReg32(0xA2090070, 0x00220021);
    #endif
    DRV_WriteReg32((USB_PHY_MSTAR+0x868), 0x1420A620);	//U2PLL ebable
    DRV_WriteReg32((USB_PHY_MSTAR+0x808), 0x80404084);	//utmi
    DRV_WriteReg32((USB_PHY_MSTAR+0x81C), 0x008880A1);	//utmi
    DRV_WriteReg32((USB_PHY_MSTAR+0x800), 0x94006BC3);	//utmi
    hal_gpt_delay_us(2000);
    DRV_WriteReg32((USB_PHY_MSTAR+0x800), 0x940069C3);	//utmi
    hal_gpt_delay_us(2000);
    DRV_WriteReg32((USB_PHY_MSTAR+0x800), 0x94000001);	//utmi
    hal_gpt_delay_us(2000);
    DRV_WriteReg32((USB_PHY_MSTAR+0x810), 0x01FF0040);	//utmi
    DRV_WriteReg32((USB_PHY_MSTAR+0x810), 0x00FF0040);	//utmi
    DRV_WriteReg32((USB_PHY_MSTAR+0x808), 0x80404004);	//utmi
    hal_gpt_delay_us(7000);
    DRV_WriteReg32((USB_PHY_MSTAR+0x838), 0x00000001);	//utmi
    DRV_WriteReg32((USB_PHY_MSTAR+0x838), 0x00000000);	//utmi
    
    uint32_t test = 0;
    test = DRV_Reg32(USB_PHY_MSTAR+0x838);
    
    while((test & 0x00000002) != 0x2)
    {
        test = DRV_Reg32(USB_PHY_MSTAR+0x838);
        //do something.
    }

    DRV_WriteReg32((USB_PHY_MSTAR+0x800), 0x28840001);	//utmi
    DRV_WriteReg32((USB_PHY_MSTAR+0x804), 0x00403064);	//utmi
    DRV_WriteReg32((USB_PHY_MSTAR+0x808), 0x8040D507);	//utmi
    DRV_WriteReg32((USB_PHY_MSTAR+0x804), 0x00433064);	//utmi
    DRV_WriteReg32((USB_PHY_MSTAR+0x804), 0x00403064);	//utmi
    DRV_WriteReg32((USB_PHY_MSTAR+0x814), 0x0000000B);	//utmi
    USB_DRV_SetBits32((USB_PHY_MSTAR+0x828), (1<<9));
    USB_DRV_SetBits32((USB_PHY_MSTAR+0x828), (1<<7));
}


void hal_usbphy_save_current(void)
{

}

void hal_usbphy_save_current_for_charge(void)
{

}


void hal_usbphy_recover(void)
{

}



#define USB_HS_SLEW_RATE_CAL_TIME_WINDOW 0x400
#define USB_HS_SLEW_RATE_CAL_A 32
#define USB_HS_SLEW_RATE_CAL_FRA (1000)

void hal_usbphy_slew_rate_calibration(void)
{

}

#endif /*HAL_USB_MODULE_ENABLED*/

void hal_usbphy_deinit_case(void)
{
    DRV_WriteReg32(0xA2090070, 0x01220121);
    hal_gpt_delay_ms(1);
    DRV_WriteReg32(0xA2090070, 0x00220021); 
    USB_DRV_SetBits32((USB_PHY_MSTAR+0x86C), (1<<3));
}
