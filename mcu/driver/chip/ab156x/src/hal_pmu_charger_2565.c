/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
#include "hal.h"
#include "hal_pmu.h"
#ifdef HAL_PMU_MODULE_ENABLED
#include "hal_usb.h"
#include "hal_usb_internal.h"
#include "hal_sleep_manager_platform.h"
#include "hal_sleep_manager_internal.h"
#include "nvkey_id_list.h"
#include "assert.h"

uint8_t pmu_charger_type;                                                                           /*DATA : restore charger type*/
uint8_t gChargerStatus = CHG_NONE;
uint8_t pmu_chg_intr_flag = 0;
PMU_CHG_INFO pmu_chg_info;


extern int32_t mpk_round(int32_t val1, int32_t val2);

/*==========[Basic function]==========*/
/*static void pmu_charger_callback1(void)
{
    log_hal_msgid_info("pmu_charger_callback1\n", 0);
}

static void pmu_charger_callback2(void)
{
    log_hal_msgid_info("pmu_charger_callback2\n", 0);
}

static void pmu_charger_callback3(void)
{
    log_hal_msgid_info("pmu_charger_callback3\n", 0);
}

static void pmu_charger_callback4(void)
{
    log_hal_msgid_info("pmu_charger_callback4\n", 0);
}*/

void pmu_charger_init_2565(uint16_t precc_cur,uint16_t cv_termination){

    uint16_t reg_320;

    log_hal_msgid_info("pmu_charger_init_2565 enter", 0);
    //chg default setting, TBD
    //OSMQ_Init(&OSMQ_CHG_INTR_Flag);

    pmu_set_register_value(FULL_BAT_THRESHOLD1_ADDR, FULL_BAT_THRESHOLD1_MASK, FULL_BAT_THRESHOLD1_SHIFT, 0x3FF);
    pmu_set_register_value(FULL_BAT_THRESHOLD2_ADDR, FULL_BAT_THRESHOLD2_MASK, FULL_BAT_THRESHOLD2_SHIFT, 0x3FF);
    pmu_set_register_value(BYP_CV_CHK_VBAT_ADDR, BYP_CV_CHK_VBAT_MASK, BYP_CV_CHK_VBAT_SHIFT, 0x1);

    /*pmu_register_callback_2565(RG_INT_CHG_IN, (pmu_callback_t)pmu_charger_callback1, NULL);
    pmu_register_callback_2565(RG_INT_CHG_OUT, (pmu_callback_t)pmu_charger_callback2, NULL);
    pmu_register_callback_2565(RG_INT_CHG_COMPL, (pmu_callback_t)pmu_charger_callback3, NULL);
    pmu_register_callback_2565(RG_INT_CHG_RECHG, (pmu_callback_t)pmu_charger_callback4, NULL);*/

    pmu_chg_deb_time(PMU_CHG_OUT_DEB_10MS, PMU_CHG_IN_DEB_40MS);

    pmu_set_register_value(CHG_FORCE_OFF_ADDR, CHG_FORCE_OFF_MASK, CHG_FORCE_OFF_SHIFT, PMU_OFF);
}

/*==========[charger]==========*/
uint8_t pmu_enable_charger_2565(uint8_t isEnableCharging){
    uint8_t value=isEnableCharging;
    if (isEnableCharging)
    {
        pmu_set_register_value(CHG_FORCE_OFF_ADDR, CHG_FORCE_OFF_MASK, CHG_FORCE_OFF_SHIFT, 0);
    }
    else
    {
        pmu_set_register_value(CHG_FORCE_OFF_ADDR, CHG_FORCE_OFF_MASK, CHG_FORCE_OFF_SHIFT, 1);
    }
    log_hal_msgid_info("pmu_enable_charger_2565, oper[%d]\r\n", 1, isEnableCharging);

    return value;
}

void pmu_eoc_ctrl (pmu_power_operate_t oper)
{
    uint16_t reg_320, reg_deb, reg_000, vchg1, vchg2;

    vchg1 = pmu_auxadc_get_channel_value(PMU_AUX_VCHG);

    reg_000 = pmu_get_register_value(PMU_CON0, 0xFFFF, 0);
    reg_320 = pmu_get_register_value(CHARGER_CON1, 0xFFFF, 0);
    log_hal_msgid_info("pmu_eoc_ctrl, oper[%d], reg_320[0x%x], reg_000[0x%x]", 3, oper, reg_320, reg_000);
    reg_deb = reg_320 & 0xF00;
    reg_320 &= 0x1F00;
    pmu_force_set_register_value(CHARGER_CON1, reg_320);

    reg_320 &= 0x1000;
    pmu_force_set_register_value(CHARGER_CON1, reg_320);

    reg_320 = (oper << 12);
    pmu_force_set_register_value(CHARGER_CON1, reg_320);
    hal_gpt_delay_us(250);

    reg_320 |= reg_deb;
    pmu_force_set_register_value(CHARGER_CON1, reg_320);

    reg_320 |= 0xAA;
    pmu_force_set_register_value(CHARGER_CON1, reg_320);

    vchg2 = pmu_auxadc_get_channel_value(PMU_AUX_VCHG);

    if ((vchg1 > 370) && (vchg2 < 300))
    {
        log_hal_msgid_info("pmu_eoc_ctrl, add chg out intr", 0);
        if (pmu_chg_intr_flag == CHG_INTR_NONE)
        {
            pmu_charger_out(pmu_chg_intr_flag |= CHG_INTR_OUT);
        }
        else
        {
            log_hal_msgid_info("pmu_eoc_ctrl, add chg out intr, exist other intr[0x%x]", 1, pmu_chg_intr_flag);
            assert(0);
        }
    }
    else if ((vchg1 < 300) && (vchg2 > 370))
    {
        log_hal_msgid_info("pmu_eoc_ctrl, add chg in intr", 0);
        if (pmu_chg_intr_flag == CHG_INTR_NONE)
            pmu_charger_in(pmu_chg_intr_flag |= CHG_INTR_IN);
        else
        {
            log_hal_msgid_info("pmu_eoc_ctrl, add chg in intr, exist other intr[0x%x]", 1, pmu_chg_intr_flag);
            assert(0);
        }
    }
}

pmu_operate_status_t pmu_chg_deb_time(pmu_chg_deb_time_t out_deb_time, pmu_chg_deb_time_t in_deb_time)
{
    uint16_t reg_320, deb_time;

    deb_time = (out_deb_time << 10) + (in_deb_time << 8);
    reg_320 = pmu_get_register_value(CHARGER_CON1, 0xFFFF, 0);
    reg_320 &= 0x10AA;
    reg_320 |= deb_time;
    pmu_force_set_register_value(CHARGER_CON1, reg_320);

    return PMU_OK;
}

bool pmu_charger_is_plug(void)
{
    uint16_t vchg = pmu_auxadc_get_channel_value(PMU_AUX_VCHG);

    if (vchg > 370)
        return TRUE;
    else if (vchg < 300)
        return FALSE;
    else
    {
        log_hal_msgid_info("pmu_charger_is_plug, unexpected", 0);
        if (gChargerStatus == CHG_EXIST)
            return FALSE;
        else
            return TRUE;
    }
}

bool pmu_charger_is_compl(void)
{
    uint16_t chg_state = pmu_get_register_value(CHG_STATE_ADDR, CHG_STATE_MASK, CHG_STATE_SHIFT);

    if (chg_state == CHG_COMPL)
        return TRUE;
    else
        return FALSE;
}

uint16_t pmu_charger_get_chg_state(void)
{
    uint16_t chg_state = 0;

    if (pmu_charger_is_plug())
        chg_state = pmu_get_register_value(CHG_STATE_ADDR, CHG_STATE_MASK, CHG_STATE_SHIFT);

    return chg_state;
}

void pmu_charger_in(void)
{
    if ((gChargerStatus == CHG_NONE) && (pmu_chg_intr_flag & CHG_INTR_IN))
    {
        gChargerStatus = CHG_EXIST;
        log_hal_msgid_info("pmu_charger_in, intr_flag[%x]\r\n", 1, pmu_chg_intr_flag);
        //to do
        if (pmu_function_table_2565[RG_INT_CHG_IN].pmu_callback)
            pmu_function_table_2565[RG_INT_CHG_IN].pmu_callback();
    }

    pmu_chg_intr_flag &= (uint8_t)(~CHG_INTR_IN);

    if (pmu_chg_intr_flag == CHG_INTR_NONE)
        pmu_chg_intr_flag = 0;
    else if (pmu_chg_intr_flag & CHG_INTR_COMPL)
        pmu_charger_compl();
    else if (pmu_chg_intr_flag & CHG_INTR_OUT)
        pmu_charger_out();
    else if (pmu_chg_intr_flag & CHG_INTR_RECHG)
        pmu_charger_rechg();
    else
        assert(0);

#if 0
    KERNEL_IPC_MSG_CHG_HDR_STRU_PTR pMsg = (KERNEL_IPC_MSG_CHG_HDR_STRU_PTR)ptr;

    if (gChargerStatus == CHG_NONE && pMsg->IntrFlag & CHG_INTR_IN)
    {
        CHG_MSGID_I("DRV Charger Plug In Intr, ChgState[%x], IntrFlag[%d]", 2, gChargerStatus, pMsg->IntrFlag);
        gChargerStatus = CHG_EXIST;
        //USB_PHY_Set_WakeUp();
        //Drv_USB_Core_Update_Plug(USB_CORE_USBPLUGIN);
        if (gCHG_CTRL.type < CHG_CASE_SMART_V1)
            MSG_MessageSendEx(gMSG_HandlerRegisteredForCHARGERIN, DRV_CHARGER_MSG_CHARGERIN, NULL, 0);
        else
        {
#if(DRV_SMARTCHARGER_VERSION_1)
            /* set clock to 144M if esco exist */
            if(Source_blks[SOURCE_TYPE_SCO] != NULL)
            {
                SYS_CLK_Config(SYS_HIGH_POWER_MODE, DVFS_SMART_CHG);
            }
#else
            /* set clock to 144 if charger in */
            SYS_CLK_Config(SYS_HIGH_POWER_MODE, DVFS_SMART_CHG);
#endif
        }
    }
    pMsg->IntrFlag &= (~(uint16_t)CHG_INTR_IN);

    if (pMsg->IntrFlag == CHG_INTR_NONE)
        OSMEM_Put(pMsg);
    else if (pMsg->IntrFlag & CHG_INTR_COMPL)
        DRV_CHARGER_Complete_State(ptr);
    else if (pMsg->IntrFlag & CHG_INTR_OUT)
        DRV_CHARGER_UnPlug_State(ptr);
    else if (pMsg->IntrFlag & CHG_INTR_RECHG)
        DRV_CHARGER_ReCharge_State(ptr);
    else
        assert(0);
#endif
}

void pmu_charger_out(void)
{
    if ((gChargerStatus == CHG_EXIST) && (pmu_chg_intr_flag & CHG_INTR_OUT))
    {
        gChargerStatus = CHG_NONE;
        log_hal_msgid_info("pmu_charger_out, intr_flag[%x]\r\n", 1, pmu_chg_intr_flag);
        //to do
        if (pmu_function_table_2565[RG_INT_CHG_OUT].pmu_callback)
            pmu_function_table_2565[RG_INT_CHG_OUT].pmu_callback();
    }

    pmu_chg_intr_flag &= (uint8_t)(~CHG_INTR_OUT);

    if (pmu_chg_intr_flag == CHG_INTR_NONE)
        pmu_chg_intr_flag = 0;
    else if (pmu_chg_intr_flag & CHG_INTR_IN)
        pmu_charger_in();
    else
        assert(0);

#if 0
    //uint8_t UnPlugSkip = 0;
    KERNEL_IPC_MSG_CHG_HDR_STRU_PTR pMsg = (KERNEL_IPC_MSG_CHG_HDR_STRU_PTR)ptr;

    if (gChargerStatus == CHG_EXIST && pMsg->IntrFlag & CHG_INTR_OUT)
    {
        /*if (gCHG_CTRL.enable && gCHG_CTRL.type < CHG_CASE_SMART_V1)
        {
            uint32_t CurrTimer = DRV_TIMER_READ_COUNTER();
            uint16_t adcValue = 0;

            adcValue = DRV_ADC0_ReadVChg();
            if (adcValue > gCHG_CTRL.adc_thrd)
            {
                if (CurrTimer - pMsg->StartTimer < gCHG_CTRL.timeout_us)
                {
                    CHG_MSGID_I("DRV_CHARGER_UnPlug_State, VCHG Read Fail", 0);
                    OSMQ_PutFront(&OSMQ_CHG_INTR_Flag, ptr);
                    return;
                }
                CHG_MSGID_I("DRV Charger Plug Out Intr, SKIP", 0);
                UnPlugSkip = 1;
            }
        }
        if (UnPlugSkip == 0)*/
        {
            CHG_MSGID_I("DRV Charger Plug Out Intr, ChgState[%x], IntrFlag[%d]", 2, gChargerStatus, pMsg->IntrFlag);
            gChargerStatus = CHG_NONE;
            //Drv_USB_Core_Update_Plug(USB_CORE_USBPLUGOUT);
            //USB_PHY_Set_Sleep();
            if (gCHG_CTRL.type < CHG_CASE_SMART_V1)
                MSG_MessageSendEx(gMSG_HandlerRegisteredForCHARGEROUT, DRV_CHARGER_MSG_CHARGEROUT, NULL, 0);
        }
    }
    pMsg->IntrFlag &= (~(uint16_t)CHG_INTR_OUT);

    if (pMsg->IntrFlag == CHG_INTR_NONE)
        OSMEM_Put(pMsg);
    else if (pMsg->IntrFlag & CHG_INTR_IN)
        DRV_CHARGER_Plug_State(ptr);
    else
        assert(0);
#endif
}

void pmu_charger_compl(void)
{
    if ((gChargerStatus == CHG_EXIST) && (pmu_chg_intr_flag & CHG_INTR_COMPL))
    {
        log_hal_msgid_info("pmu_charger_compl, intr_flag[%x]\r\n", 1, pmu_chg_intr_flag);
        //to do
        if (pmu_function_table_2565[RG_INT_CHG_COMPL].pmu_callback)
            pmu_function_table_2565[RG_INT_CHG_COMPL].pmu_callback();
    }

    pmu_chg_intr_flag &= (uint8_t)(~CHG_INTR_COMPL);

    if (pmu_chg_intr_flag == CHG_INTR_NONE)
        pmu_chg_intr_flag = 0;
    else if (pmu_chg_intr_flag & CHG_INTR_RECHG)
        pmu_charger_rechg();
    else if (pmu_chg_intr_flag & CHG_INTR_OUT)
        pmu_charger_out();
    else
        assert(0);

#if 0
    KERNEL_IPC_MSG_CHG_HDR_STRU_PTR pMsg = (KERNEL_IPC_MSG_CHG_HDR_STRU_PTR)ptr;

    if (gChargerStatus == CHG_EXIST && pMsg->IntrFlag & CHG_INTR_COMPL)
    {
        CHG_MSGID_I("DRV Charger Complete Intr, ChgState[%x], IntrFlag[%d]", 2, gChargerStatus, pMsg->IntrFlag);
        if (gCHG_CTRL.type < CHG_CASE_SMART_V1)
            MSG_MessageSendEx(gMSG_HandlerRegisteredForCHARGERCPL, DRV_CHARGER_MSG_CHARGERCPL, NULL, 0);
    }
    pMsg->IntrFlag &= (~(uint16_t)CHG_INTR_COMPL);

    if (pMsg->IntrFlag == CHG_INTR_NONE)
        OSMEM_Put(pMsg);
    else if (pMsg->IntrFlag & CHG_INTR_RECHG)
        DRV_CHARGER_ReCharge_State(ptr);
    else if (pMsg->IntrFlag & CHG_INTR_OUT)
        DRV_CHARGER_UnPlug_State(ptr);
    else
        assert(0);
#endif
}

void pmu_charger_rechg(void)
{
    if ((gChargerStatus == CHG_EXIST) && (pmu_chg_intr_flag & CHG_INTR_RECHG))
    {
        log_hal_msgid_info("pmu_charger_rechg, intr_flag[%x]\r\n", 1, pmu_chg_intr_flag);
        //to do
        if (pmu_function_table_2565[RG_INT_CHG_RECHG].pmu_callback)
            pmu_function_table_2565[RG_INT_CHG_RECHG].pmu_callback();
    }

    pmu_chg_intr_flag &= (uint8_t)(~CHG_INTR_RECHG);

    if (pmu_chg_intr_flag == CHG_INTR_NONE)
        pmu_chg_intr_flag = 0;
    else if (pmu_chg_intr_flag & CHG_INTR_COMPL)
        pmu_charger_rechg();
    else if (pmu_chg_intr_flag & CHG_INTR_OUT)
        pmu_charger_out();
    else
        assert(0);


#if 0
    KERNEL_IPC_MSG_CHG_HDR_STRU_PTR pMsg = (KERNEL_IPC_MSG_CHG_HDR_STRU_PTR)ptr;

    if (gChargerStatus == CHG_EXIST && pMsg->IntrFlag & CHG_INTR_RECHG)
    {
        CHG_MSGID_I("DRV Charger ReCharge Intr, ChgState[%x], IntrFlag[%d]", 2, gChargerStatus, pMsg->IntrFlag);
        if (gCHG_CTRL.type < CHG_CASE_SMART_V1)
            MSG_MessageSendEx(gMSG_HandlerRegisteredForRECHARGER, DRV_CHARGER_MSG_RECHARGER, NULL, 0);
    }
    pMsg->IntrFlag &= (~(uint16_t)CHG_INTR_RECHG);

    if (pMsg->IntrFlag == CHG_INTR_NONE)
        OSMEM_Put(pMsg);
    else if (pMsg->IntrFlag & CHG_INTR_COMPL)
        DRV_CHARGER_Complete_State(ptr);
    else if (pMsg->IntrFlag & CHG_INTR_OUT)
        DRV_CHARGER_UnPlug_State(ptr);
    else
        assert(0);
#endif
}

void pmu_charger_handler(uint16_t chg_flag)
{
    if (chg_flag & (1 << PMU_CHG_IN_INT_FLAG))
    {
        pmu_chg_intr_flag |= CHG_INTR_IN;
    }
    if (chg_flag & (1 << PMU_CHG_OUT_INT_FLAG))
    {
        if (pmu_charger_is_plug() == FALSE)
            pmu_chg_intr_flag |= CHG_INTR_OUT;
        else
            log_hal_msgid_info("pmu_charger_handler, ignore chg out", 0);
    }
    if (chg_flag & (1 << PMU_CHG_COMPLETE_INT_FLAG))
    {
        pmu_chg_intr_flag |= CHG_INTR_COMPL;
    }
    if (chg_flag & (1 << PMU_CHG_RECHG_INT_FLAG))
    {
        pmu_chg_intr_flag |= CHG_INTR_RECHG;
    }
    log_hal_msgid_info("pmu_charger_handler, intr_flag[%x]\r\n", 1, pmu_chg_intr_flag);

    if(gChargerStatus == CHG_NONE)
    {
        pmu_charger_in();
    }
    else
    {
        if (pmu_chg_intr_flag & CHG_INTR_IN)
            pmu_charger_out();
        else
            pmu_charger_compl();
    }
}

/*==========[BC 1.2 behavior]==========*/

void pmu_bc12_init(void) {
    pmu_set_register_value_ddie(PMU_BC12_IBIAS_EN_V12_ADDR, PMU_BC12_IBIAS_EN_V12_MASK, PMU_BC12_IBIAS_EN_V12_SHIFT, 0x1);
    hal_gpt_delay_ms(10);
    pmu_set_register_value_ddie(PMU_BC12_SRC_EN_V12_ADDR, PMU_BC12_SRC_EN_V12_MASK, PMU_BC12_SRC_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_VREF_VTH_EN_V12_ADDR, PMU_BC12_VREF_VTH_EN_V12_MASK, PMU_BC12_VREF_VTH_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_CMP_EN_V12_ADDR, PMU_BC12_CMP_EN_V12_MASK, PMU_BC12_CMP_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_IPU_EN_V12_ADDR, PMU_BC12_IPU_EN_V12_MASK, PMU_BC12_IPU_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_IPD_EN_V12_ADDR, PMU_BC12_IPD_EN_V12_MASK, PMU_BC12_IPD_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_IPD_HALF_EN_V12_ADDR, PMU_BC12_IPD_HALF_EN_V12_MASK, PMU_BC12_IPD_HALF_EN_V12_SHIFT, 0);
}

void pmu_bc12_end(void) {
    pmu_set_register_value_ddie(PMU_BC12_IBIAS_EN_V12_ADDR, PMU_BC12_IBIAS_EN_V12_MASK, PMU_BC12_IBIAS_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_SRC_EN_V12_ADDR, PMU_BC12_SRC_EN_V12_MASK, PMU_BC12_SRC_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_VREF_VTH_EN_V12_ADDR, PMU_BC12_VREF_VTH_EN_V12_MASK, PMU_BC12_VREF_VTH_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_CMP_EN_V12_ADDR, PMU_BC12_CMP_EN_V12_MASK, PMU_BC12_CMP_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_IPU_EN_V12_ADDR, PMU_BC12_IPU_EN_V12_MASK, PMU_BC12_IPU_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_IPD_EN_V12_ADDR, PMU_BC12_IPD_EN_V12_MASK, PMU_BC12_IPD_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_IPD_HALF_EN_V12_ADDR, PMU_BC12_IPD_HALF_EN_V12_MASK, PMU_BC12_IPD_HALF_EN_V12_SHIFT, 0);
}

uint8_t pmu_bc12_dcd(void) {
    uint8_t value = 0xFF;
    pmu_set_register_value_ddie(PMU_BC12_SRC_EN_V12_ADDR, PMU_BC12_SRC_EN_V12_MASK, PMU_BC12_SRC_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_VREF_VTH_EN_V12_ADDR, PMU_BC12_VREF_VTH_EN_V12_MASK, PMU_BC12_VREF_VTH_EN_V12_SHIFT, 1);
    pmu_set_register_value_ddie(PMU_BC12_CMP_EN_V12_ADDR, PMU_BC12_CMP_EN_V12_MASK, PMU_BC12_CMP_EN_V12_SHIFT, 2);
    pmu_set_register_value_ddie(PMU_BC12_IPU_EN_V12_ADDR, PMU_BC12_IPU_EN_V12_MASK, PMU_BC12_IPU_EN_V12_SHIFT, 2);
    pmu_set_register_value_ddie(PMU_BC12_IPD_EN_V12_ADDR, PMU_BC12_IPD_EN_V12_MASK, PMU_BC12_IPD_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_DCD_EN_V12_ADDR, PMU_BC12_DCD_EN_V12_MASK, PMU_BC12_DCD_EN_V12_SHIFT, 1);
    pmu_set_register_value_ddie(PMU_BC12_IPD_HALF_EN_V12_ADDR, PMU_BC12_IPD_HALF_EN_V12_MASK, PMU_BC12_IPD_HALF_EN_V12_SHIFT, 0);
    hal_gpt_delay_ms(10);
    value = pmu_get_register_value_ddie(PMU_AQ_QI_BC12_CMP_OUT_V12_ADDR, PMU_AQ_QI_BC12_CMP_OUT_V12_MASK, PMU_AQ_QI_BC12_CMP_OUT_V12_SHIFT);;
    hal_gpt_delay_ms(10);
    return value;
}

uint8_t pmu_bc12_primary_detction(void) {
    uint8_t compareResult = 0xFF;
    pmu_set_register_value_ddie(PMU_BC12_SRC_EN_V12_ADDR, PMU_BC12_SRC_EN_V12_MASK, PMU_BC12_SRC_EN_V12_SHIFT, 2);
    pmu_set_register_value_ddie(PMU_BC12_VREF_VTH_EN_V12_ADDR, PMU_BC12_VREF_VTH_EN_V12_MASK, PMU_BC12_VREF_VTH_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_CMP_EN_V12_ADDR, PMU_BC12_CMP_EN_V12_MASK, PMU_BC12_CMP_EN_V12_SHIFT, 1);
    pmu_set_register_value_ddie(PMU_BC12_IPU_EN_V12_ADDR, PMU_BC12_IPU_EN_V12_MASK, PMU_BC12_IPU_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_IPD_EN_V12_ADDR, PMU_BC12_IPD_EN_V12_MASK, PMU_BC12_IPD_EN_V12_SHIFT, 1);
    pmu_set_register_value_ddie(PMU_BC12_IPD_HALF_EN_V12_ADDR, PMU_BC12_IPD_HALF_EN_V12_MASK, PMU_BC12_IPD_HALF_EN_V12_SHIFT, 0);
    hal_gpt_delay_ms(40);
    compareResult = pmu_get_register_value_ddie(PMU_AQ_QI_BC12_CMP_OUT_V12_ADDR, PMU_AQ_QI_BC12_CMP_OUT_V12_MASK, PMU_AQ_QI_BC12_CMP_OUT_V12_SHIFT);
    hal_gpt_delay_ms(20);
    return compareResult;
}

uint8_t pmu_bc12_secondary_detection(void) {
    uint8_t compareResult = 0xFF;
    pmu_set_register_value_ddie(PMU_BC12_SRC_EN_V12_ADDR, PMU_BC12_SRC_EN_V12_MASK, PMU_BC12_SRC_EN_V12_SHIFT, 1);
    pmu_set_register_value_ddie(PMU_BC12_VREF_VTH_EN_V12_ADDR, PMU_BC12_VREF_VTH_EN_V12_MASK, PMU_BC12_VREF_VTH_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_CMP_EN_V12_ADDR, PMU_BC12_CMP_EN_V12_MASK, PMU_BC12_CMP_EN_V12_SHIFT, 2);
    pmu_set_register_value_ddie(PMU_BC12_IPU_EN_V12_ADDR, PMU_BC12_IPU_EN_V12_MASK, PMU_BC12_IPU_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_IPD_EN_V12_ADDR, PMU_BC12_IPD_EN_V12_MASK, PMU_BC12_IPD_EN_V12_SHIFT, 2);
    pmu_set_register_value_ddie(PMU_BC12_IPD_HALF_EN_V12_ADDR, PMU_BC12_IPD_HALF_EN_V12_MASK, PMU_BC12_IPD_HALF_EN_V12_SHIFT, 0);
    hal_gpt_delay_ms(40);
    compareResult = pmu_get_register_value_ddie(PMU_AQ_QI_BC12_CMP_OUT_V12_ADDR, PMU_AQ_QI_BC12_CMP_OUT_V12_MASK, PMU_AQ_QI_BC12_CMP_OUT_V12_SHIFT);
    hal_gpt_delay_ms(20);
    return compareResult;
}

uint8_t pmu_bc12_check_DCP(void) {
    uint8_t compareResult = 0xFF;
    pmu_set_register_value_ddie(PMU_BC12_SRC_EN_V12_ADDR, PMU_BC12_SRC_EN_V12_MASK, PMU_BC12_SRC_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_VREF_VTH_EN_V12_ADDR, PMU_BC12_VREF_VTH_EN_V12_MASK, PMU_BC12_VREF_VTH_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_CMP_EN_V12_ADDR, PMU_BC12_CMP_EN_V12_MASK, PMU_BC12_CMP_EN_V12_SHIFT, 1);
    pmu_set_register_value_ddie(PMU_BC12_IPU_EN_V12_ADDR, PMU_BC12_IPU_EN_V12_MASK, PMU_BC12_IPU_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_IPD_EN_V12_ADDR, PMU_BC12_IPD_EN_V12_MASK, PMU_BC12_IPD_EN_V12_SHIFT, 2);
    pmu_set_register_value_ddie(PMU_BC12_IPD_HALF_EN_V12_ADDR, PMU_BC12_IPD_HALF_EN_V12_MASK, PMU_BC12_IPD_HALF_EN_V12_SHIFT, 0);
    hal_gpt_delay_ms(10);
    pmu_set_register_value_ddie(PMU_BC12_IPD_EN_V12_ADDR, PMU_BC12_IPD_EN_V12_MASK, PMU_BC12_IPD_EN_V12_SHIFT, 0);
    hal_gpt_delay_us(200);
    compareResult = pmu_get_register_value_ddie(PMU_AQ_QI_BC12_CMP_OUT_V12_ADDR, PMU_AQ_QI_BC12_CMP_OUT_V12_MASK, PMU_AQ_QI_BC12_CMP_OUT_V12_SHIFT);
    return compareResult;
}

uint8_t pmu_bc12_check_dp(void) {
    uint8_t compareResult = 0xFF;
    pmu_set_register_value_ddie(PMU_BC12_SRC_EN_V12_ADDR, PMU_BC12_SRC_EN_V12_MASK, PMU_BC12_SRC_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_VREF_VTH_EN_V12_ADDR, PMU_BC12_VREF_VTH_EN_V12_MASK, PMU_BC12_VREF_VTH_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_CMP_EN_V12_ADDR, PMU_BC12_CMP_EN_V12_MASK, PMU_BC12_CMP_EN_V12_SHIFT, 2);
    pmu_set_register_value_ddie(PMU_BC12_IPU_EN_V12_ADDR, PMU_BC12_IPU_EN_V12_MASK, PMU_BC12_IPU_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_IPD_EN_V12_ADDR, PMU_BC12_IPD_EN_V12_MASK, PMU_BC12_IPD_EN_V12_SHIFT, 2);
    pmu_set_register_value_ddie(PMU_BC12_IPD_HALF_EN_V12_ADDR, PMU_BC12_IPD_HALF_EN_V12_MASK, PMU_BC12_IPD_HALF_EN_V12_SHIFT, 0);
    hal_gpt_delay_ms(10);
    compareResult = pmu_get_register_value_ddie(PMU_AQ_QI_BC12_CMP_OUT_V12_ADDR, PMU_AQ_QI_BC12_CMP_OUT_V12_MASK, PMU_AQ_QI_BC12_CMP_OUT_V12_SHIFT);
    hal_gpt_delay_ms(10);
    return compareResult;
}

uint8_t pmu_bc12_check_dm(void) {
    uint8_t compareResult = 0xFF;
    pmu_set_register_value_ddie(PMU_BC12_SRC_EN_V12_ADDR, PMU_BC12_SRC_EN_V12_MASK, PMU_BC12_SRC_EN_V12_SHIFT, 0);
   // pmu_bc12_set_vref_vth_en_v12(0);
    pmu_set_register_value_ddie(PMU_BC12_CMP_EN_V12_ADDR, PMU_BC12_CMP_EN_V12_MASK, PMU_BC12_CMP_EN_V12_SHIFT, 1);
    pmu_set_register_value_ddie(PMU_BC12_IPU_EN_V12_ADDR, PMU_BC12_IPU_EN_V12_MASK, PMU_BC12_IPU_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_IPD_EN_V12_ADDR, PMU_BC12_IPD_EN_V12_MASK, PMU_BC12_IPD_EN_V12_SHIFT, 1);
    pmu_set_register_value_ddie(PMU_BC12_IPD_HALF_EN_V12_ADDR, PMU_BC12_IPD_HALF_EN_V12_MASK, PMU_BC12_IPD_HALF_EN_V12_SHIFT, 0);
    hal_gpt_delay_ms(10);
    compareResult = pmu_get_register_value_ddie(PMU_AQ_QI_BC12_CMP_OUT_V12_ADDR, PMU_AQ_QI_BC12_CMP_OUT_V12_MASK, PMU_AQ_QI_BC12_CMP_OUT_V12_SHIFT);
    hal_gpt_delay_ms(10);
    return compareResult;
}

uint8_t pmu_bc12_check_floating(void) {
    uint8_t compareResult = 0xFF;
    pmu_set_register_value_ddie(PMU_BC12_SRC_EN_V12_ADDR, PMU_BC12_SRC_EN_V12_MASK, PMU_BC12_SRC_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_VREF_VTH_EN_V12_ADDR, PMU_BC12_VREF_VTH_EN_V12_MASK, PMU_BC12_VREF_VTH_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_CMP_EN_V12_ADDR, PMU_BC12_CMP_EN_V12_MASK, PMU_BC12_CMP_EN_V12_SHIFT, 1);
    pmu_set_register_value_ddie(PMU_BC12_IPU_EN_V12_ADDR, PMU_BC12_IPU_EN_V12_MASK, PMU_BC12_IPU_EN_V12_SHIFT, 0);
    pmu_set_register_value_ddie(PMU_BC12_IPD_EN_V12_ADDR, PMU_BC12_IPD_EN_V12_MASK, PMU_BC12_IPD_EN_V12_SHIFT, 1);
    pmu_set_register_value_ddie(PMU_BC12_IPD_HALF_EN_V12_ADDR, PMU_BC12_IPD_HALF_EN_V12_MASK, PMU_BC12_IPD_HALF_EN_V12_SHIFT, 1);
    hal_gpt_delay_ms(10);
    compareResult = pmu_get_register_value_ddie(PMU_AQ_QI_BC12_CMP_OUT_V12_ADDR, PMU_AQ_QI_BC12_CMP_OUT_V12_MASK, PMU_AQ_QI_BC12_CMP_OUT_V12_SHIFT);
    hal_gpt_delay_ms(10);
    return compareResult;
}

uint8_t pmu_get_bc12_charger_type_2565(void) {
    pmu_bc12_init();
    if (pmu_bc12_dcd() == 0) {                       //SDP/CDP/DCP/S charger
        if (pmu_bc12_primary_detction() == 0) {      //SDP
            pmu_bc12_end();
            log_hal_msgid_info("BC12 SDP Charger\r\n", 0);
            pmu_charger_type=SDP_CHARGER;
            return SDP_CHARGER;
        } else {                                                    //CDP/DCP/S charger
            if(pmu_bc12_secondary_detection()==0){   //CDP
                pmu_bc12_end();
                log_hal_msgid_info("BC12 CDP Charger\r\n", 0);
                pmu_charger_type=CDP_CHARGER;
                return CDP_CHARGER;
            }else{
                 if(pmu_bc12_check_DCP()==0){          //DCP
                     pmu_bc12_end();
                     log_hal_msgid_info("BC12 DCP Charger\r\n", 0);
                     pmu_charger_type=DCP_CHARGER;
                     return DCP_CHARGER;
                 }else{                                             //S charger
                     pmu_bc12_end();
                     log_hal_msgid_info("BC12 SS Charger\r\n", 0);
                     pmu_charger_type=SS_TABLET_CHARGER;
                     return SS_TABLET_CHARGER;
                 }
            }
        }
    } else {                                                        //apple /non-standard/ DP&DM floating charger
        uint8_t dp_out, dm_out;
        dp_out = pmu_bc12_check_dp();
        dm_out = pmu_bc12_check_dm();
        if((dp_out==0)&&(dm_out==0)){
            if(pmu_bc12_check_floating()==0){                 //DP&DM floating charger
                pmu_bc12_end();
                log_hal_msgid_info("BC12 DP&DM Floating\r\n", 0);
                pmu_charger_type=DP_DM_FLOATING;
                return DP_DM_FLOATING;
            }else{                                                      //non-standard charger
                pmu_bc12_end();
                log_hal_msgid_info("BC12 NON-STD charger\r\n", 0);
                pmu_charger_type=NON_STD_CHARGER;
                return NON_STD_CHARGER;
            }
        }else if((dp_out==0)&&(dm_out==1)){                          //apple 5V 1A charger
                pmu_bc12_end();
                log_hal_msgid_info("BC12 IPHONE_5V_1A Charger\r\n", 0);
                pmu_charger_type=IPHONE_5V_1A_CHARGER;
                return IPHONE_5V_1A_CHARGER;
        }else{                                                      //apple ipad2/ipad4 charger
            pmu_bc12_end();
                log_hal_msgid_info("BC12 IPAD2_IPAD4 Charger\r\n", 0);
                pmu_charger_type=IPAD2_IPAD4_CHARGER;
            return IPAD2_IPAD4_CHARGER;
        }
    }
}

/*==========[battery]==========*/
#define MMI_IPHONE_BATTERY_LEVELS 9

VBAT_VOLTAGE_CONFIG* p_vbat_volt = NULL;
VBAT_ADC_CALIBRATION_TABLE* p_vbat_adc = NULL;

void pmu_bat_init(void)
{
    p_vbat_volt = (VBAT_VOLTAGE_CONFIG*)get_nvkey_data(NVKEYID_MP_CAL_VBAT_VOLTAGE_CONFIG);

    if(!p_vbat_volt)
    {
        log_hal_msgid_info("pmu_bat_init, NVKEYID_MP_CAL_VBAT_VOLTAGE_CONFIG no data", 0);
        //assert(0);
    }

    p_vbat_adc = (VBAT_ADC_CALIBRATION_TABLE*)get_nvkey_data(NVKEYID_MP_CAL_VBAT_ADC_CALIBRATION_TABLE);

    if(!p_vbat_adc)
    {
        log_hal_msgid_info("pmu_bat_init, NVKEYID_MP_CAL_VBAT_ADC_CALIBRATION_TABLE no data", 0);
        //assert(0);
    }

    log_hal_msgid_info("pmu_bat_init, initial[%d], low[%d], shutdown[%d]", 3, p_vbat_volt->initial_bat_ADC, p_vbat_volt->low_bat_ADC, p_vbat_volt->Shutdown_bat_ADC);
    log_hal_msgid_info("pmu_bat_init, 0%[%d], 10%[%d], 20%[%d], 30%[%d], 40%[%d], 50%[%d], 60%[%d], 70%[%d], 80%[%d], 90%[%d], 100%[%d]", 11,
        p_vbat_volt->Shutdown_bat_ADC, p_vbat_volt->data[0].adc, p_vbat_volt->data[1].adc, p_vbat_volt->data[2].adc,
        p_vbat_volt->data[3].adc, p_vbat_volt->data[4].adc, p_vbat_volt->data[5].adc, p_vbat_volt->data[6].adc,
        p_vbat_volt->data[7].adc, p_vbat_volt->data[8].adc, p_vbat_volt->data[9].adc);

    /*uint32_t tmp = DRV_BAT_ADCVal_Coverter_Voltage(p_vbat_volt->data[8].adc);
    DRV_BAT_Voltage_Coverter_ADCVal(tmp);*/
}

void DRV_BAT_GetLevelInDisplay(uint16_t ADCvalue)
{
    uint16_t VBig, VSmall,Vfinal2;

    VBig = p_vbat_adc->data[0].adc - p_vbat_adc->data[1].adc;
    VSmall = ADCvalue - p_vbat_adc->data[1].adc;
    Vfinal2 = mpk_round((VSmall * (p_vbat_adc->data[0].voltage - p_vbat_adc->data[1].voltage)), VBig) + p_vbat_adc->data[1].voltage;
}

static uint32_t DRV_BAT_Get_ADC_Tran_Percent_internal(uint32_t adcval, int fullscale_value)
{
    uint8_t i;
    uint16_t lowBd, highBd;
    uint32_t mul=fullscale_value / 10;
    uint32_t result = 0;

    for( i = 0; i < MMI_IPHONE_BATTERY_LEVELS; i++)
    {
        if(adcval < p_vbat_volt->data[i].adc)
            break;
    }

    if(i == 0)
    {
        lowBd = p_vbat_volt->Shutdown_bat_ADC;
        highBd = p_vbat_volt->data[0].adc;

        if(adcval < lowBd)
        {
            return 0;
        }
    }
    else if(i == MMI_IPHONE_BATTERY_LEVELS)
    {
        lowBd = p_vbat_volt->data[MMI_IPHONE_BATTERY_LEVELS-1].adc;
        //highBd = 2 * p_vbat_volt->vbat_data[MMI_IPHONE_BATTERY_LEVELS-1].adc - p_vbat_volt->vbat_data[MMI_IPHONE_BATTERY_LEVELS-2].adc;
        highBd = p_vbat_volt->data[MMI_IPHONE_BATTERY_LEVELS].adc;

        if(adcval >= highBd)
        {
            log_hal_msgid_info("DRV_BAT_Get_ADC_Tran_Percent_internal, i=%d, adcval = %d, highBd = %d", 3, i, adcval, highBd);
            return fullscale_value;
        }
    }
    else
    {
        lowBd = p_vbat_volt->data[i-1].adc;
        highBd = p_vbat_volt->data[i].adc;
    }

    result = (uint32_t)(mpk_round((mul * (adcval - lowBd)), (highBd - lowBd))+ (i*mul));

    log_hal_msgid_info("DRV_BAT_Get_ADC_Tran_Percent_internal i=%d adcval=%d lowBd=%d highBd=%d result = %d, adcval = %d", 6, i, adcval, lowBd, highBd, result, adcval);

    return result;
}

uint8_t DRV_BAT_Get_ADC_Tran_Percent(uint32_t adcval)
{
    return DRV_BAT_Get_ADC_Tran_Percent_internal(adcval, 100);
}

uint32_t DRV_BAT_GetAvgVbatAdc(void)
{
    return pmu_auxadc_get_channel_value(PMU_AUX_VBAT);
}

uint8_t DRV_BAT_GetLevelInPercent(void)
{
    uint32_t val = DRV_BAT_GetAvgVbatAdc();
    uint8_t perc = DRV_BAT_Get_ADC_Tran_Percent(val);

    log_hal_msgid_info("DRV_BAT_GetLevelInPercent VBAT[%u], PERC[%u]", 2, val, perc);

    return perc;
}

uint8_t DRV_BAT_IsBelowInitBat(void)
{
    uint16_t batLevel = (uint16_t)DRV_BAT_GetAvgVbatAdc();

    if(batLevel < p_vbat_volt->initial_bat_ADC)
        return TRUE;
    else
        return FALSE;
}


uint8_t DRV_BAT_IsBelowLowBat(void)
{
    uint16_t batLevel = (uint16_t)DRV_BAT_GetAvgVbatAdc();

    if(batLevel < p_vbat_volt->low_bat_ADC)
        return TRUE;
    else
        return FALSE;
}


uint8_t DRV_BAT_IsBelowShutDownBat(void)
{
    uint16_t batLevel = (uint16_t)DRV_BAT_GetAvgVbatAdc();
    log_hal_msgid_info("DRV_BAT_IsBelowShutDownBat, batLevel[%d], shutdownbat[%d]", 2, batLevel, p_vbat_volt->Shutdown_bat_ADC);

    if(batLevel < p_vbat_volt->Shutdown_bat_ADC)
        return TRUE;
    else
        return FALSE;
}

uint8_t DRV_BAT_3v3_proc(void)
{
    uint16_t vbat_3v3_adc;
    uint16_t batLevel = (uint16_t)DRV_BAT_GetAvgVbatAdc();
    log_hal_msgid_info("DRV_BAT_IsAbove3v3, volt[%d]", 1, p_vbat_adc->data[1].voltage);

    if (p_vbat_adc->data[1].voltage == 3300)
    {
        vbat_3v3_adc = p_vbat_adc->data[1].adc;
    }
    else
        return FALSE;


    if(batLevel > vbat_3v3_adc)
    {
        if (Adie_Version == PMU_ECO3)
            pmu_set_register_value(VLDO33_CLAMP_EN_ADDR, VLDO33_CLAMP_EN_MASK, VLDO33_CLAMP_EN_SHIFT, 0);
        else
            pmu_set_register_value(VLDO33_CLAMP_EN_E2_ADDR, VLDO33_CLAMP_EN_E2_MASK, VLDO33_CLAMP_EN_E2_SHIFT, 0);
    }
    else
    {
        if (Adie_Version == PMU_ECO3)
            pmu_set_register_value(VLDO33_CLAMP_EN_ADDR, VLDO33_CLAMP_EN_MASK, VLDO33_CLAMP_EN_SHIFT, 1);
        else
            pmu_set_register_value(VLDO33_CLAMP_EN_E2_ADDR, VLDO33_CLAMP_EN_E2_MASK, VLDO33_CLAMP_EN_E2_SHIFT, 1);
    }
    return TRUE;
}

uint32_t DRV_BAT_Voltage_Coverter_ADCVal(uint32_t Voltage)
{
    uint32_t VBig, VSmall;
    uint32_t AdcBig,Adcfinal;

    if(p_vbat_adc->CalCount > 2)
    {
        AdcBig = p_vbat_adc->data[0].adc - p_vbat_adc->data[2].adc;
        VBig = p_vbat_adc->data[0].voltage - p_vbat_adc->data[2].voltage;

        if(Voltage > p_vbat_adc->data[2].voltage)
        {
            VSmall = Voltage - p_vbat_adc->data[2].voltage;
            Adcfinal = (uint32_t)(mpk_round((VSmall * AdcBig), VBig) + p_vbat_adc->data[2].adc);
        }
        else
        {
            VSmall = p_vbat_adc->data[2].voltage - Voltage;
            Adcfinal = (uint32_t)(p_vbat_adc->data[2].adc - mpk_round((VSmall * AdcBig), VBig));
        }

    }
    else
    {
        AdcBig = p_vbat_adc->data[0].adc - p_vbat_adc->data[1].adc;
        VBig = p_vbat_adc->data[0].voltage - p_vbat_adc->data[1].voltage;

        if(Voltage > p_vbat_adc->data[1].voltage)
        {
            VSmall = Voltage - p_vbat_adc->data[1].voltage;
            Adcfinal = (uint32_t)(mpk_round((VSmall * AdcBig), VBig) + p_vbat_adc->data[1].adc);
        }
        else
        {
            VSmall = p_vbat_adc->data[1].voltage - Voltage;
            Adcfinal = (uint32_t)(p_vbat_adc->data[1].adc - mpk_round((VSmall * AdcBig), VBig));
        }
    }
    log_hal_msgid_info("DRV_BAT_Voltage_Coverter_ADCVal, Voltage[%d], Adcfinal[%d]", 2, Voltage, Adcfinal);

    return Adcfinal;
}

uint32_t DRV_BAT_ADCVal_Coverter_Voltage(uint32_t adcval)
{
    uint32_t VBig,Vfinal;
    uint32_t AdcBig,AdcSmall;

    if(p_vbat_adc->CalCount > 2)
    {
        AdcBig = p_vbat_adc->data[0].adc - p_vbat_adc->data[2].adc;
        VBig = p_vbat_adc->data[0].voltage - p_vbat_adc->data[2].voltage;

        if(adcval > p_vbat_adc->data[2].adc)
        {
            AdcSmall = adcval - p_vbat_adc->data[2].adc;
            Vfinal = (uint32_t)(mpk_round((AdcSmall * VBig), AdcBig) + p_vbat_adc->data[2].voltage);
        }
        else
        {
            AdcSmall = p_vbat_adc->data[2].adc - adcval;
            Vfinal = (uint32_t)(p_vbat_adc->data[2].voltage - mpk_round((AdcSmall * VBig), AdcBig));
        }

    }
    else
    {
        AdcBig = p_vbat_adc->data[0].adc - p_vbat_adc->data[1].adc;
        VBig = p_vbat_adc->data[0].voltage - p_vbat_adc->data[1].voltage;

        if(adcval > p_vbat_adc->data[1].adc)
        {
            AdcSmall = adcval - p_vbat_adc->data[1].adc;
            Vfinal = (uint32_t)(mpk_round((AdcSmall * VBig), AdcBig) + p_vbat_adc->data[1].voltage);
        }
        else
        {
            AdcSmall = p_vbat_adc->data[1].adc - adcval;
            Vfinal = (uint32_t)(p_vbat_adc->data[1].voltage - mpk_round((AdcSmall * VBig), AdcBig));
        }
    }
    log_hal_msgid_info("DRV_BAT_ADCVal_Coverter_Voltage, adcval[%d], Vfinal[%d]", 2, adcval, Vfinal);

    return Vfinal;
}

uint32_t DRV_BAT_Voltage_Get_Percent(uint32_t Voltage)
{
    uint8_t Per;
    uint32_t ADCval;

    ADCval = DRV_BAT_Voltage_Coverter_ADCVal(Voltage);
    Per = DRV_BAT_Get_ADC_Tran_Percent(ADCval);

    return Per;
}

uint16_t DRV_BAT_Get_ADC_Tran_ThousandPercent(uint32_t adcval)
{
    return DRV_BAT_Get_ADC_Tran_Percent_internal(adcval, 1000);
}

uint16_t DRV_BAT_GetThousandPercent(void)
{
    uint32_t val = DRV_BAT_GetAvgVbatAdc();
    uint16_t perc = DRV_BAT_Get_ADC_Tran_ThousandPercent(val);

    log_hal_msgid_info("DRV_BAT_GetThousandPercent VBAT[%u]", 1, perc);

    return perc;
}

#endif /* HAL_PMU_MODULE_ENABLED */
