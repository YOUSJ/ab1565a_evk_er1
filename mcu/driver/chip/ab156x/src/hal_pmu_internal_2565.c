/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal.h"
#ifdef HAL_PMU_MODULE_ENABLED

#include "hal_i2c_master.h"
#include "hal_sleep_manager_platform.h"
#include "hal_sleep_manager_internal.h"
#include "hal_sleep_manager.h"
#include "nvkey_id_list.h"

#include "assert.h"

#define UNUSED(x)  ((void)(x))

/*
 * Basic parameters */
int old_index=0;                                                        /* DATA : restore old vcore voltage index */
int pmic_irq0=-1,pmic_irq1=-1,pmic_irq2=-1,pmic_irq3=-1;                /* DATA : restore irq status */
int first_boot_up =0;                                                   /* FLAG : PK check is turned on for the first time */
int pk_next =PMU_PK_PRESS;                                              /* DATA : power key default value*/

uint8_t pmu_basic_index=0;                                              /* DATA : project init basic vocre index */
uint8_t pmu_lock_status=0;                                              /* DATA : lock status*/
uint8_t va18_flag=0;                                                    /* FLAG : When VA18 doesn't want to be disable */
uint8_t pmu_init_flag=0;                                                /* FLAG : check init setting is done */
uint8_t event_con0=0,event_con1=0,event_con2=0,event_con3=0;            /* DATA : restore irq times */
uint8_t pmu_charger_status;                                             /* FLAG : for vbus exist */

uint32_t pmu_register_interrupt ;                                       /* DATA : Record irq index 0~31 */
uint32_t pmu_register_interrupt_2 ;                                     /* DATA : Record irq index >31 */
uint32_t pmu_irq_enable_com0 =0;                                        /* DATA : restore irq enable status,con0 */
uint32_t pmu_irq_enable_com1 =0;                                        /* DATA : restore irq enable status,con1 */
uint32_t pmu_irq_enable_com2 =0;                                        /* DATA : restore irq enable status,con2 */
uint32_t pmu_irq_enable_com3 =0;                                        /* DATA : restore irq enable status,con3 */

pmu_function_t pmu_function_table[PMU_INT_MAX];                         /* DATA : restore callback function */
pmu_function_t pmu_function_table_2565[PMU_INT_MAX_2565];                         /* DATA : restore callback function */
volatile int pmu_i2c_init_sta = 0;                                      /* FLAG : init setting flag */
static unsigned char Vcore_Resource_Ctrl[8];                            /* resource control : use in pmu_lock_vcore for resource control */
extern auxadc_efuse_data_stru gAuxadcEfuseInfo;                         /* DATA : get auxadc data struct*/

uint8_t pmu_pwrkey_status = 0;
uint8_t Adie_Version = 0xFF;
PMU_POWER_ON_REASON_UNION pmu_power_on_reason;
uint8_t pmu_power_off_reason = 0;

/*==========[Basic function]==========*/
pmu_power_vcore_voltage_t pmu_lock_vcore_ab2565(pmu_power_stage_t mode,pmu_power_vcore_voltage_t vol,pmu_lock_parameter_t lock) {
    log_hal_msgid_info("pmu_lock_vcore_ab2565, mode = %d, vol = %d, lock = %d \r\n", 3, mode, vol, lock);

    int vol_index = 0;
    if(vol>=PMIC_VCORE_FAIL_V) {
        return PMIC_VCORE_FAIL_V;
    }
    if(mode == PMU_SLEEP) {
    } else {
        if (lock == PMU_LOCK) {
            Vcore_Resource_Ctrl[vol]++;
            pmu_lock_status++;
        } else {
            if (Vcore_Resource_Ctrl[vol] != 0) {
                Vcore_Resource_Ctrl[vol]--;
                pmu_lock_status--;
            }
            if ((vol == old_index) && Vcore_Resource_Ctrl[vol] == 0) {
                old_index = 0;
            }
        }
        /*Find Highest Vcore Voltage*/
        for (vol_index = 6; vol_index > 0; vol_index--) {
            if (Vcore_Resource_Ctrl[vol_index] != 0) {
                break;
            }
        }
        /*if not module lock ,return default setting*/
        if (pmu_lock_status == 0) {
            pmu_select_vcore_voltage_ab2565(PMU_DVS, pmu_basic_index);
            log_hal_msgid_info("PMU lock basic: %d ", 1,vol_index);
        } else {
            if (old_index < vol_index) {
                pmu_select_vcore_voltage_ab2565(PMU_DVS, vol_index);
                log_hal_msgid_info("PMU lock : %d ", 1,vol_index);
                old_index = vol_index;
            }
        }
    }
    log_hal_msgid_info("pmu locked count %d, vol_index = %d \r\n",2,pmu_lock_status, vol_index);
    return vol_index;
}

pmu_status_t pmu_register_callback(pmu_interrupt_index_t pmu_int_ch, pmu_callback_t callback, void *user_data)
{
    UNUSED(pmu_int_ch);
    UNUSED(callback);
    UNUSED(user_data);
    log_hal_msgid_info("pmu_register_callback not support", 0);
    return -1;
#if 0
    pmu_status_t status = PMU_STATUS_ERROR;
    if(pmu_int_ch >= PMU_INT_MAX || callback == NULL)
    {
        return PMU_STATUS_INVALID_PARAMETER;
    }
    pmu_function_table[pmu_int_ch].init_status = PMU_INIT;
    pmu_function_table[pmu_int_ch].pmu_callback= callback;
    pmu_function_table[pmu_int_ch].user_data = user_data;
    pmu_function_table[pmu_int_ch].isMask= false;
    if(pmu_int_ch>31){
        pmu_register_interrupt_2 |=(1<<(pmu_int_ch-32)) ;
    }else{
        pmu_register_interrupt |=(1<<pmu_int_ch) ;
    }
    pmu_control_enable_interrupt(pmu_int_ch, 1);
    pmu_control_mask_interrupt(pmu_int_ch, 0);

    status = PMU_STATUS_SUCCESS;
    return status;
#endif
}

pmu_status_t pmu_deregister_callback(pmu_interrupt_index_t pmu_int_ch)
{
    UNUSED(pmu_int_ch);
    log_hal_msgid_info("pmu_deregister_callback not support", 0);
    return -1;
#if 0
    pmu_status_t status = PMU_STATUS_ERROR;

    if(pmu_int_ch >= PMU_INT_MAX )
    {
        return PMU_STATUS_INVALID_PARAMETER;
    }
    pmu_function_table[pmu_int_ch].init_status = PMU_NOT_INIT;
    pmu_function_table[pmu_int_ch].pmu_callback= NULL;
    pmu_function_table[pmu_int_ch].user_data = NULL;
    pmu_function_table[pmu_int_ch].isMask= true;

    pmu_control_enable_interrupt(pmu_int_ch, 0);
    pmu_control_mask_interrupt(pmu_int_ch, 1);
    status = PMU_STATUS_SUCCESS;

    return status;
#endif
}

pmu_status_t pmu_register_callback_2565(pmu_interrupt_index_2565_t pmu_int_ch, pmu_callback_t callback, void *user_data)
{
    if(pmu_int_ch >= PMU_INT_MAX_2565 || callback == NULL)
    {
        return PMU_STATUS_INVALID_PARAMETER;
    }
    pmu_function_table_2565[pmu_int_ch].init_status = PMU_INIT;
    pmu_function_table_2565[pmu_int_ch].pmu_callback = callback;
    pmu_function_table_2565[pmu_int_ch].user_data = user_data;
    pmu_function_table_2565[pmu_int_ch].isMask = false;

    return PMU_STATUS_SUCCESS;
}

pmu_status_t pmu_deregister_callback_2565(pmu_interrupt_index_2565_t pmu_int_ch)
{
    if(pmu_int_ch >= PMU_INT_MAX )
    {
        return PMU_STATUS_INVALID_PARAMETER;
    }
    pmu_function_table_2565[pmu_int_ch].init_status = PMU_NOT_INIT;
    pmu_function_table_2565[pmu_int_ch].pmu_callback = NULL;
    pmu_function_table_2565[pmu_int_ch].user_data = NULL;
    pmu_function_table_2565[pmu_int_ch].isMask= true;

    return PMU_STATUS_SUCCESS;
}

void pmu_power_off_sequence(pmu_power_stage_t stage) {
    uint16_t tmp;
    uint16_t reg_320, pwrkey_reg;
    log_hal_msgid_info("pmu_power_off_sequence, stage[%d]", 1, stage);

    switch(stage) {
        case PMU_PWROFF:
            pmu_set_register_value_2565(PMU_CON2, PWR_OFF_MASK, PWR_OFF_SHIFT, 0x1);
        break;
        case PMU_RTC:
            reg_320 = pmu_get_register_value(CHARGER_CON1, 0xFFFF, 0);
            reg_320 &= 0x0FFF;
            reg_320 |= 0x1000;
            pmu_force_set_register_value(CHARGER_CON1, reg_320);

            pwrkey_reg = pmu_get_register_value(PMU_CON3, 0xFFFF, 0);
            pwrkey_reg &=  0x3F;
            pmu_force_set_register_value(PMU_CON3, pwrkey_reg);

            /*tmp = pmu_get_register_value(CHARGER_CON1, 0xFFFF, 0);
            tmp &= 0x1FAA;
            tmp |= 0x1000;
            pmu_force_set_register_value(CHARGER_CON1, tmp);*/

            pmu_set_register_value_2565(CHARGER_CON4, SW_OCP_ENB_MASK, SW_OCP_ENB_SHIFT, 0x1);

            tmp = pmu_get_register_value_2565(CFG_RESERVE, RESERVE_MASK, RESERVE_SHIFT);
            tmp |= 0x20;//0x60
            pmu_set_register_value_2565(RESERVE_ADDR, RESERVE_MASK, RESERVE_SHIFT, tmp);

            if (Adie_Version == PMU_ECO1)
                pmu_set_register_value_2565(PMU_IVGEN1, FAST_BUFFER_ENB_MASK, FAST_BUFFER_ENB_SHIFT, 0x0);
            else
                pmu_set_register_value_2565(PMU_IVGEN1, FAST_BUFFER_ENB_MASK, FAST_BUFFER_ENB_SHIFT, 0x1);

            if (Adie_Version == PMU_ECO3 || Adie_Version == PMU_NO_FTK)
                pmu_set_register_value_2565(PMU_IVGEN1, BGR_TRIM_ENB_MASK, BGR_TRIM_ENB_SHIFT, 0x1);
            else
                pmu_set_register_value_2565(PMU_IVGEN1, BGR_TRIM_ENB_MASK, BGR_TRIM_ENB_SHIFT, 0x0);

            tmp = pmu_get_register_value_2565(CHG_STATE_ADDR, CHG_STATE_MASK, CHG_STATE_SHIFT);
            if (tmp == 0)
               pmu_set_register_value_2565(CFG_RESERVE, 0x1, 6, 0x0);
            else
                pmu_set_register_value_2565(CFG_RESERVE, 0x1, 6, 0x1);

            pmu_set_register_value_2565(PMU_CON1, UART_PSWMAIN_ENB_MASK, UART_PSWMAIN_ENB_SHIFT, 1);
            pmu_set_register_value_2565(PMU_CON2, RTC_MODE_MASK, RTC_MODE_SHIFT, 0x1);
        break;
        case PMU_SLEEP:
            if (Adie_Version == PMU_ECO1 || Adie_Version == PMU_ECO2)
            {
                pmu_set_register_value_2565(PMU_CON2, 0xF, 5, 0);
            }
            if (Adie_Version == PMU_ECO2 || Adie_Version == PMU_ECO3 || Adie_Version == PMU_NO_FTK)
                pmu_set_register_value_2565(PMU_IVGEN1, FAST_BUFFER_ENB_MASK, FAST_BUFFER_ENB_SHIFT, 0x1);
            else
                pmu_set_register_value_2565(PMU_IVGEN1, FAST_BUFFER_ENB_MASK, FAST_BUFFER_ENB_SHIFT, 0x1);

            pmu_set_register_value_2565(PMU_IVGEN1, 0x1, 8, 0x0);
        break;
        case PMU_NORMAL:
        break;
        case PMU_DVS:
        break;
    }
}

uint8_t pmu_get_power_key_status(void){
#if defined(AB1565)
    if (Adie_Version == PMU_ECO3)
        return pmu_get_register_value(AD_REGEN_ADDR, AD_REGEN_MASK, AD_REGEN_SHIFT);
    else
        return pmu_get_register_value(AD_REGEN_E2_ADDR, AD_REGEN_E2_MASK, AD_REGEN_E2_SHIFT);
#else
    return pmu_get_register_value(PMU_PSSTS2, PMU_PWRKEY_VAL_MASK, PMU_PWRKEY_VAL_SHIFT);
#endif
}
/*==========[BUCK/LDO control]==========*/



/*
 * In normal mode , switch control mode
 * set RG_BUCK_Vxxx_ON_MODE = 0: SW mode  ; set 1: HW mode
 */
void pmu_switch_control_mode_2568(pmu_power_domain_t domain, pmu_control_mode_t mode) {
    UNUSED(domain);
    UNUSED(mode);
    log_hal_msgid_info("pmu_switch_control_mode_2568 not support", 0);
#if 0
    switch(domain) {
        case PMU_BUCK_VCORE:
            pmu_set_register_value_ab2568(PMU_BUCK_VCORE_OP_MODE, PMU_RG_BUCK_VCORE_ON_MODE_MASK, PMU_RG_BUCK_VCORE_ON_MODE_SHIFT, mode);
            break;
        case PMU_BUCK_VIO18:
            pmu_set_register_value_ab2568(PMU_BUCK_VIO18_OP_MODE, PMU_RG_BUCK_VIO18_ON_MODE_MASK, PMU_RG_BUCK_VIO18_ON_MODE_SHIFT, mode);
            break;
        case PMU_BUCK_VRF:
            pmu_set_register_value_ab2568(PMU_BUCK_VRF_OP_MODE, PMU_RG_BUCK_VRF_ON_MODE_MASK, PMU_RG_BUCK_VRF_ON_MODE_SHIFT, mode);
            break;
        case PMU_BUCK_VAUD18:
            pmu_set_register_value_ab2568(PMU_BUCK_VAUD18_OP_MODE, PMU_RG_BUCK_VAUD18_ON_MODE_MASK, PMU_RG_BUCK_VAUD18_ON_MODE_SHIFT, mode);
            break;
        case PMU_LDO_VA18:
            pmu_set_register_value_ab2568(PMU_LDO_VA18_OP_MODE, PMU_RG_LDO_VA18_ON_MODE_MASK, PMU_RG_LDO_VA18_ON_MODE_SHIFT, mode);
            break;
        case PMU_LDO_VLDO33:
            pmu_set_register_value_ab2568(PMU_LDO_VLDO33_OP_MODE, PMU_RG_LDO_VLDO33_ON_MODE_MASK, PMU_RG_LDO_VLDO33_ON_MODE_SHIFT, mode);
            break;
        case PMU_LDO_VRF:
            pmu_set_register_value_ab2568(PMU_LDO_VRF11_OP_MODE, PMU_RG_LDO_VRF11_ON_MODE_MASK, PMU_RG_LDO_VRF11_ON_MODE_SHIFT, mode);
            break;
        case PMU_LDO_VSRAM:
            pmu_set_register_value_ab2568(PMU_LDO_VSRAM_OP_MODE, PMU_RG_LDO_VSRAM_ON_MODE_MASK, PMU_RG_LDO_VSRAM_ON_MODE_SHIFT, mode);
            break;
    }
#endif
}

/* SW mode lp enable/disable
 * 1'b0: wo LP; 1'b1: wi LP
 * */
void pmu_enable_sw_lp_mode_2568(pmu_power_domain_t domain, pmu_control_mode_t mode) {
    UNUSED(domain);
    UNUSED(mode);
    log_hal_msgid_info("pmu_enable_sw_lp_mode_2568 not support", 0);
#if 0
    switch(domain) {
        case PMU_BUCK_VCORE:
            pmu_set_register_value_ab2568(PMU_BUCK_VCORE_OP_MODE, PMU_RG_BUCK_VCORE_LP_MODE_MASK, PMU_RG_BUCK_VCORE_LP_MODE_SHIFT, mode);
            pmu_set_register_value_ab2568(PMU_BUCK_VCORE_CON0, PMU_RG_BUCK_VCORE_LP_MASK, PMU_RG_BUCK_VCORE_LP_SHIFT, mode);
            break;
        case PMU_BUCK_VIO18:
            pmu_set_register_value_ab2568(PMU_BUCK_VIO18_OP_MODE, PMU_RG_BUCK_VIO18_LP_MODE_MASK, PMU_RG_BUCK_VIO18_LP_MODE_SHIFT, mode);
            pmu_set_register_value_ab2568(PMU_BUCK_VIO18_CON0, PMU_RG_BUCK_VIO18_LP_MASK, PMU_RG_BUCK_VIO18_LP_SHIFT, mode);
            break;
        case PMU_BUCK_VRF:
            pmu_set_register_value_ab2568(PMU_BUCK_VRF_OP_MODE, PMU_RG_BUCK_VRF_LP_MODE_MASK, PMU_RG_BUCK_VRF_LP_MODE_SHIFT, mode);
            pmu_set_register_value_ab2568(PMU_BUCK_VRF_CON0, PMU_RG_BUCK_VRF_LP_MODE_MASK, PMU_RG_BUCK_VRF_LP_SHIFT, mode);
            break;
        case PMU_BUCK_VAUD18:
            pmu_set_register_value_ab2568(PMU_BUCK_VAUD18_OP_MODE, PMU_RG_BUCK_VAUD18_LP_MODE_MASK, PMU_RG_BUCK_VAUD18_LP_MODE_SHIFT, mode);
            pmu_set_register_value_ab2568(PMU_BUCK_VAUD18_CON0, PMU_RG_BUCK_VAUD18_LP_MASK, PMU_RG_BUCK_VAUD18_LP_SHIFT, mode);
            break;
        case PMU_LDO_VA18:
            pmu_set_register_value_ab2568(PMU_LDO_VA18_OP_MODE, PMU_RG_LDO_VA18_LP_MODE_MASK, PMU_RG_LDO_VA18_LP_MODE_SHIFT, mode);
            pmu_set_register_value_ab2568(PMU_LDO_VA18_CON0, PMU_RG_LDO_VA18_LP_MASK, PMU_RG_LDO_VA18_LP_SHIFT, mode);
            break;
        case PMU_LDO_VLDO33:
            pmu_set_register_value_ab2568(PMU_LDO_VLDO33_OP_MODE, PMU_RG_LDO_VLDO33_LP_MODE_MASK, PMU_RG_LDO_VLDO33_LP_MODE_SHIFT, mode);
            pmu_set_register_value_ab2568(PMU_LDO_VLDO33_CON0, PMU_RG_LDO_VLDO33_LP_MASK, PMU_RG_LDO_VLDO33_LP_SHIFT, mode);
            break;
    }
#endif
}

/*==========[Buck/Ldo voltage]==========*/
pmu_operate_status_t pmu_select_vcore_voltage_ab2565(pmu_power_stage_t mode, pmu_power_vcore_voltage_t vol) {
    if ((mode > PMU_DVS) | (mode < PMU_SLEEP) | (vol > PMIC_VCORE_0P9_V) | (vol < PMIC_VCORE_0P5_V)) {
        log_hal_msgid_error("[PMU] vcore_voltage Error input", 0);
        return PMU_ERROR;
    }
    if ((mode !=PMU_DVS) && (pmu_lock_status > 0)) {
        log_hal_msgid_error("[PMU] VCORE in locked ", 0);
        return PMU_ERROR;
    }

    switch (vol)
    {
        /*case PMIC_VCORE_0P6_V:
            pmu_set_register_value_ab2568(PMU_BUCK_VCORE_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x10);
            break;
        case PMIC_VCORE_0P7_V:
            pmu_set_register_value_ab2568(PMU_BUCK_VCORE_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x20);
            break;
        case PMIC_VCORE_0P73_V:
            pmu_set_register_value_ab2568(PMU_BUCK_VCORE_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x25);
            break;
        case PMIC_VCORE_0P75_V:
            pmu_set_register_value_ab2568(PMU_BUCK_VCORE_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x28);
            break;*/
        case PMIC_VCORE_0P8_V:
            pmu_set_register_value(BUCK_LVOUTSEL_LV_NORM_ADDR, BUCK_LVOUTSEL_LV_NORM_MASK, BUCK_LVOUTSEL_LV_NORM_SHIFT, 0x1);
            break;
        case PMIC_VCORE_0P9_V:
            pmu_set_register_value(BUCK_LVOUTSEL_LV_NORM_ADDR, BUCK_LVOUTSEL_LV_NORM_MASK, BUCK_LVOUTSEL_LV_NORM_SHIFT, 0x0);
            break;
        /*case PMIC_VCORE_1P2_V:
            pmu_set_register_value_ab2568(PMU_BUCK_VCORE_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x70);
            break;*/
        default:
            //pmu_set_register_value_ab2568(PMU_BUCK_VCORE_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x40);
            break;
    }

    return PMU_OK;
}
/*For DVFS select vsram voltage*/

pmu_operate_status_t pmu_select_vsram_voltage_ab2568(pmu_power_stage_t mode, pmu_power_vsram_voltage_t vol) {
    UNUSED(mode);
    UNUSED(vol);
    log_hal_msgid_info("pmu_select_vsram_voltage_ab2568 not support", 0);
    return -1;
#if 0
    if (mode == PMU_NORMAL) {
        pmu_set_register_value_ab2568(PMU_LDO_SRAM_CON3, PMU_RG_VSRAM_VOSEL_MASK, PMU_RG_VSRAM_VOSEL_SHIFT, vol);
        return PMU_OK;
    }else{
        return PMU_ERROR;
    }
#endif
}

pmu_power_vcore_voltage_t pmu_get_vcore_setting_index(uint16_t vcore) {
    UNUSED(vcore);
    log_hal_msgid_info("pmu_get_vcore_setting_index not support", 0);
    return -1;
#if 0
    uint8_t vcbuck_voval[7] = { 0x0, 0x20, 0x25, 0x28, 0x30, 0x40, 0x70 };
    int vosel = 0;
    for (vosel = 0; vosel < 7; vosel++) {
        if (vcore == vcbuck_voval[vosel]) {
            return ((pmu_power_vcore_voltage_t) vosel);
        }
    }
    return (PMU_ERROR);
#endif
}

pmu_power_vcore_voltage_t pmu_get_vcore_voltage_ab2568(void) {
    log_hal_msgid_info("pmu_get_vcore_voltage_ab2568 not support", 0);
    return -1;
#if 0
    uint32_t temp = 0;
    temp = pmu_get_register_value_ab2568(PMU_BUCK_VCORE_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT);
    return pmu_get_vcore_setting_index(temp);
#endif
}

void pmu_vcroe_voltage_turing(int symbol, int num) {
    UNUSED(symbol);
    UNUSED(num);
    log_hal_msgid_info("pmu_vcroe_voltage_turing not support", 0);
#if 0
    uint32_t cur_v;
    if (symbol) {
        cur_v = pmu_get_register_value_ab2568(PMU_BUCK_VCORE_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT);
        cur_v += num;
        pmu_set_register_value_ab2568(PMU_BUCK_VCORE_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, cur_v);
    } else {
        cur_v = pmu_get_register_value_ab2568(PMU_BUCK_VCORE_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT);
        cur_v -= num;
        pmu_set_register_value_ab2568(PMU_BUCK_VCORE_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, cur_v);
    }
#endif
}

void pmu_vaud18_voltage_turing(int symbol, int num) {
    UNUSED(symbol);
    UNUSED(num);
    log_hal_msgid_info("pmu_vaud18_voltage_turing not support", 0);
#if 0
    uint32_t cur_v;
    if (symbol) {
        cur_v = pmu_get_register_value_ab2568(PMU_BUCK_VAUD18_ELR0, PMU_RG_BUCK_VAUD18_VOSEL_MASK, PMU_RG_BUCK_VAUD18_VOSEL_SHIFT);
        cur_v += num;
        pmu_set_register_value_ab2568(PMU_BUCK_VAUD18_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, cur_v);
    } else {
        cur_v = pmu_get_register_value_ab2568(PMU_BUCK_VAUD18_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT);
        cur_v -= num;
        pmu_set_register_value_ab2568(PMU_BUCK_VAUD18_ELR0, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, cur_v);
    }
#endif
}

void pmu_vio18_voltage_turing(int symbol, int num) {
    UNUSED(symbol);
    UNUSED(num);
    log_hal_msgid_info("pmu_vio18_voltage_turing not support", 0);
#if 0
    uint32_t cur_v;
    if (symbol) {
        cur_v = pmu_get_register_value_ab2568(PMU_BUCK_VIO18_ELR0, PMU_RG_BUCK_VIO18_VOSEL_MASK, PMU_RG_BUCK_VIO18_VOSEL_SHIFT);
        cur_v += num;
        pmu_set_register_value_ab2568(PMU_BUCK_VIO18_ELR0, PMU_RG_BUCK_VIO18_VOSEL_MASK, PMU_RG_BUCK_VIO18_VOSEL_SHIFT, cur_v);
    } else {
        cur_v = pmu_get_register_value_ab2568(PMU_BUCK_VIO18_ELR0, PMU_RG_BUCK_VIO18_VOSEL_MASK, PMU_RG_BUCK_VIO18_VOSEL_SHIFT);
        cur_v -= num;
        pmu_set_register_value_ab2568(PMU_BUCK_VIO18_ELR0, PMU_RG_BUCK_VIO18_VOSEL_MASK, PMU_RG_BUCK_VIO18_VOSEL_SHIFT, cur_v);
    }
#endif
}

void pmu_selet_voltage_2568(pmu_power_stage_t mode,pmu_power_domain_t domain,uint32_t vol) {
    UNUSED(mode);
    UNUSED(domain);
    UNUSED(vol);
    log_hal_msgid_info("pmu_selet_voltage_2568 not support", 0);
#if 0
    switch (domain) {
        case PMU_BUCK_VIO18: //VIO18 1.12V~1.96V default setting Normal:1.8V ;Sleep :1.8V
            if (mode == PMU_NORMAL) {
                pmu_set_register_value_ab2568(PMU_BUCK_VIO18_ELR0, PMU_RG_BUCK_VIO18_VOSEL_MASK, PMU_RG_BUCK_VIO18_VOSEL_SHIFT, vol);
            }
            break;
        case PMU_BUCK_VRF: //VRF 0.7V
            pmu_set_register_value_ab2568(PMU_BUCK_VRF_ELR0, PMU_RG_BUCK_VRF_VOSEL_MASK, PMU_RG_BUCK_VRF_VOSEL_SHIFT, vol);
            pmu_set_register_value_ab2568(PMU_BUCK_VRF_CON1, PMU_RG_BUCK_VRF_VOSEL_SLEEP_MASK, PMU_RG_BUCK_VRF_VOSEL_SLEEP_SHIFT, vol);
            break;
        default:
            log_hal_msgid_info("Not support",0);
            break;
    }
#endif
}

/*==========[Power key & Cap touch]==========*/
pmu_operate_status_t pmu_pwrkey_enable(pmu_power_operate_t oper) {
    UNUSED(oper);
    log_hal_msgid_info("pmu_pwrkey_enable not support", 0);
    return -1;
    //return pmu_set_register_value_ab2568(PMU_RSTCFG3, PMU_RG_PWRKEY_RST_EN_MASK, PMU_RG_PWRKEY_RST_EN_SHIFT, oper);
}

pmu_operate_status_t pmu_pwrkey_duration_time(pmu_pwrkey_time_t tmr) {
    UNUSED(tmr);
    log_hal_msgid_info("pmu_pwrkey_duration_time not support", 0);
    return -1;
    //return pmu_set_register_value_ab2568(PMU_RSTCFG3, PMU_RG_PWRKEY_RST_TD_MASK, PMU_RG_PWRKEY_RST_TD_SHIFT, tmr);
}

pmu_operate_status_t pmu_long_press_shutdown_function_sel(pmu_pwrkey_scenario_t oper) {
    UNUSED(oper);
    log_hal_msgid_info("pmu_long_press_shutdown_function_sel not support", 0);
    return -1;
#if 0
    if (oper == PMU_RESET_DEFAULT) {
        pmu_set_register_value_ab2568(PMU_RSTCFG3, PMU_RG_STRUP_LONG_PRESS_EXT_EN_MASK, PMU_RG_STRUP_LONG_PRESS_EXT_EN_SHIFT, PMU_ON);
        return pmu_set_register_value_ab2568(PMU_RSTCFG3, PMU_RG_STRUP_LONG_PRESS_EXT_SEL_MASK, PMU_RG_STRUP_LONG_PRESS_EXT_SEL_SHIFT, 0);
    } else {
        pmu_set_register_value_ab2568(PMU_RSTCFG3, PMU_RG_STRUP_LONG_PRESS_EXT_EN_MASK, PMU_RG_STRUP_LONG_PRESS_EXT_EN_SHIFT, PMU_ON);
        return pmu_set_register_value_ab2568(PMU_RSTCFG3, PMU_RG_STRUP_LONG_PRESS_EXT_SEL_MASK, PMU_RG_STRUP_LONG_PRESS_EXT_SEL_SHIFT, oper);
    }
#endif
}

pmu_operate_status_t pmu_cap_touch_enable(pmu_power_operate_t oper) {
    UNUSED(oper);
    log_hal_msgid_info("pmu_cap_touch_enable not support", 0);
    return -1;
    //return pmu_set_register_value_ab2568(PMU_CAP_LPSD_CON0, PMU_RG_CAP_LPSD_EN_MASK, PMU_RG_CAP_LPSD_EN_SHIFT, oper);
}

pmu_operate_status_t pmu_cap_touch_shutdown_enable(pmu_power_operate_t oper) {
    UNUSED(oper);
    log_hal_msgid_info("pmu_cap_touch_shutdown_enable not support", 0);
    return -1;
    //return pmu_set_register_value_ab2568(PMU_CAP_LPSD_CON1, PMU_RG_CAP_LPSD_MASK, PMU_RG_CAP_LPSD_SHIFT, oper);
}

pmu_operate_status_t pmu_pk_filter(uint8_t pk_sta) {
    UNUSED(pk_sta);
    log_hal_msgid_info("pmu_pk_filter not support", 0);
    return -1;
#if 0
    if (pk_sta == 0) { //Press section : pk_next should be Press
        if (pk_next == PMU_PK_RELEASE && pmu_function_table[0].init_status != 0) {
            log_hal_msgid_info("PMIC INT[0]  [1]",0);
            pmu_function_table[0].pmu_callback();
        }
        pk_next = PMU_PK_RELEASE;
        return PMU_OK;
    } else if (pk_sta == 1) { //Release section : pk_next should be Press
        if ((first_boot_up==1) && pk_next == PMU_PK_PRESS && pmu_function_table[0].init_status != 0) {
            log_hal_msgid_info("PMIC INT[0]  [0]",0);
            pmu_function_table[0].pmu_callback();
        }
        first_boot_up=1; /*For first time boot up power key single*/
        pk_next = PMU_PK_PRESS;
        return PMU_OK;
    }else{
        log_hal_msgid_info("PMIC POWERKEY HW error [%d]", 1,pk_sta);
        return PMU_ERROR;
    }
    return PMU_OK;
#endif
}

pmu_operate_status_t pmu_pwrkey_normal_key_init(pmu_pwrkey_config_t *config)
{
    pmu_status_t status = PMU_STATUS_ERROR;
    status = pmu_register_callback_2565(RG_INT_PWRKEY_FALL, config->callback1,config->user_data1);
    if (status != PMU_STATUS_SUCCESS) {
        return PMU_STATUS_ERROR;
    }

    status = pmu_register_callback_2565(RG_INT_PWRKEY_RISE,config->callback2,config->user_data2);
    if (status != PMU_STATUS_SUCCESS) {
        return PMU_STATUS_ERROR;
    }
    return PMU_STATUS_SUCCESS;
}

/*==========[Get PMIC hw informantion]==========*/
void pmu_get_adie_version(void)
{
    //hal_flash_otp_read(0x191, (uint8_t *)&Adie_Version, 1);
    Adie_Version = PMU_ECO3;
    log_hal_msgid_info("pmu_get_adie_version, Version[0x%x]", 1, Adie_Version);
}

uint8_t pmu_get_power_on_reason(void) {
    uint16_t reg;
    pmu_get_adie_version();

    reg = pmu_get_register_value_2565(PMU_CON3, 0xFFFF, 0);
    reg &= 0x7FA;

    if (pmu_power_on_reason.value == 0 && (reg & 0x7C0))
    {
        if (reg & (1 << PMU_REGEN_PON_FLAG))
        {
            pmu_power_on_reason.field.off_norm_regen_pon = 1;
        }
        if (reg & (1 << PMU_RTC_ALARM_FLAG))
        {
            pmu_power_on_reason.field.rtc_norm_rtc_alarm = 1;
        }
        if (reg & (1 << PMU_CHG_PON_FLAG))
        {
            pmu_power_on_reason.field.off_norm_chg_pon = 1;
        }
        if (reg & (1 << PMU_CHG_ALARM_FLAG))
        {
            //pmu_power_on_reason.field.rtc_norm_chg_alarm = 1;
        }
        if (reg & (1 << PMU_REGEN_ALARM_FLAG))
        {
            //pmu_power_on_reason.field.rtc_norm_regen_alarm = 1;
        }
        pmu_force_set_register_value(PMU_CON3, reg);

        if (Adie_Version == PMU_ECO3)
            pmu_set_register_value_2565(OFF2IDLE_FLAG_ADDR, OFF2IDLE_FLAG_MASK, OFF2IDLE_FLAG_SHIFT, 0x1);
    }
    log_hal_msgid_info("pmu_get_power_on_reason, reason:[0x%x]", 1, pmu_power_on_reason.value);

    return (uint8_t)pmu_power_on_reason.value;
}

uint8_t pmu_get_power_off_reason(void) {
    uint16_t reg;

    reg = pmu_get_register_value_2565(PMU_CON3, 0xFFFF, 0);
    reg &= 0xF83A;

    if (pmu_power_off_reason == 0 && (reg & 0xF800))
    {
        if (reg & (1 << PMU_RTC_MODE_FLAG))
        {
            pmu_power_off_reason = 1;
        }
        if (reg & (1 << PMU_CAP_LPSD_FLAG))
        {
            pmu_power_off_reason = 14;
        }
        if (reg & (1 << PMU_REGEN_LPSD_FLAG))
        {
            pmu_power_off_reason = 10;
        }
        if (reg & (1 << PMU_SYS_RST_FLAG))
        {
            pmu_power_off_reason = 13;
        }
        if (reg & (1 << PMU_WD_RST_FLAG))
        {
            pmu_power_off_reason = 8;
        }
        pmu_force_set_register_value(PMU_CON3, reg);
    }
    log_hal_msgid_info("pmu_get_power_off_reason, reason:[0x%x]", 1, pmu_power_off_reason);

    return pmu_power_off_reason;
}

uint8_t pmu_get_usb_input_status_2565(void) // For usb driver get usb put in status
{
    log_hal_msgid_info("pmu_get_usb_input_status not support", 0);
    return -1;
    //return pmu_get_register_value_ab2568(PMU_CHR_AO_DBG0, PMU_DA_QI_CHR_REF_EN_MASK, PMU_DA_QI_CHR_REF_EN_SHIFT);
}

/*==========[Other]==========*/
void pmu_srclken_control_mode_2568(pmu_power_operate_t mode)
{
    UNUSED(mode);
    log_hal_msgid_info("pmu_srclken_control_mode_2568 not support", 0);
#if 0
    if(mode==PMU_ON) {
        pmu_set_register_value_ab2568(PMU_TPO_CON1, PMU_RG_SRCLKEN_HW_MODE_MASK, PMU_RG_SRCLKEN_HW_MODE_SHIFT, 1);
    } else {
        pmu_set_register_value_ab2568(PMU_TPO_CON1, PMU_RG_SRCLKEN_HW_MODE_MASK, PMU_RG_SRCLKEN_HW_MODE_SHIFT, 0);
    }
#endif
}
void pmu_latch_power_key_for_bootloader(void)
{
    log_hal_msgid_info("pmu_latch_power_key_for_bootloader not support", 0);
#if 0
    pmu_set_register_value_ab2568(PMU_I2C_OUT_TYPE, PMU_I2C_CONFIG_MASK, PMU_I2C_CONFIG_SHIFT, 1); //D2D need to setting in PP mode, first priority, AB1555 no need, AB1558 D2D nessary
    pmu_set_register_value_ab2568(PMU_IOCFG3,PMU_RG_I2C_CLK_IC_MASK,PMU_RG_I2C_CLK_IC_SHIFT,0x4); //turn on smitt trigger for stability
    pmu_set_register_value_ab2568(PMU_IOCFG3,PMU_RG_I2C_DAT_IC_MASK,PMU_RG_I2C_DAT_IC_SHIFT,0x4); //turn on smitt trigger for stability
    pmu_set_register_value_ab2568(PMU_PWRHOLD, PMU_RG_PWRHOLD_MASK, PMU_RG_PWRHOLD_SHIFT, 1);//Power hold
#endif
}

void pmu_efuse_enable_reading(void) {
    log_hal_msgid_info("pmu_efuse_enable_reading not support", 0);
#if 0
    //Get Original MatchKey
    gAuxadcEfuseInfo.matchKey = pmu_get_register_value_ab2568(PMU_OTP_CON7, PMU_RG_OTP_RD_PKEY_MASK, PMU_RG_OTP_RD_PKEY_SHIFT);
    // RG_EFUSE_CK_PDN
    pmu_set_register_value_ab2568(PMU_CKCFG1, PMU_RG_EFUSE_CK_PDN_MASK, PMU_RG_EFUSE_CK_PDN_SHIFT, 0);
    //RG_EFUSE_CK_PDN_HWEN
    pmu_set_register_value_ab2568(PMU_CKCFG1, PMU_RG_EFUSE_CK_PDN_HWEN_MASK, PMU_RG_EFUSE_CK_PDN_HWEN_SHIFT, 0);
    // OTP set Match Key
    pmu_set_register_value_ab2568(PMU_OTP_CON7, PMU_RG_OTP_RD_PKEY_MASK, PMU_RG_OTP_RD_PKEY_SHIFT, 0x0289);
    /*Set SW trigger read mode
     * Set HW Mode ;
     * 0: non SW trigger read mode
     * 1: SW trigger read mode*/
    pmu_set_register_value_ab2568(PMU_OTP_CON11, PMU_RG_OTP_RD_SW_MASK, PMU_RG_OTP_RD_SW_SHIFT, 1);
#endif
}

void pmu_efuse_disable_reading(void) {
    log_hal_msgid_info("pmu_efuse_disable_reading not support", 0);
#if 0
    // RG_EFUSE_CK_PDN
    pmu_set_register_value_ab2568(PMU_CKCFG1, PMU_RG_EFUSE_CK_PDN_MASK, PMU_RG_EFUSE_CK_PDN_SHIFT, 1);
    //RG_EFUSE_CK_PDN_HWEN
    pmu_set_register_value_ab2568(PMU_CKCFG1, PMU_RG_EFUSE_CK_PDN_HWEN_MASK, PMU_RG_EFUSE_CK_PDN_HWEN_SHIFT, 1);
    // OTP set Original Match Key
    pmu_set_register_value_ab2568(PMU_OTP_CON7, PMU_RG_OTP_RD_PKEY_MASK, PMU_RG_OTP_RD_PKEY_SHIFT, gAuxadcEfuseInfo.matchKey);
    /*Set SW trigger read mode
     * Set HW Mode ;
     * 0: non SW trigger read mode
     * 1: SW trigger read mode*/
    pmu_set_register_value_ab2568(PMU_OTP_CON11, PMU_RG_OTP_RD_SW_MASK, PMU_RG_OTP_RD_SW_SHIFT, 0);
#endif
}

/*==========[PMIC irq]==========*/
void pmu_csr_write(int *data_ptr,int wdata)
{
    int *i;
    i=data_ptr;
    *i=wdata;
}

int pmu_csr_read(int *data_ptr)
{
    int *i;
    i=data_ptr;
    return *i;
}

/*when boot up,press power key need more than the specific time*/
void pmu_press_pk_time(void) {
    uint8_t pk_sta;
    uint32_t pmu_gpt_start, pmu_get_press_time, pmu_get_duration_time = 0;
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &pmu_gpt_start);
    while (1) {
        pk_sta = pmu_get_power_key_status();
        if (pk_sta == 1) {
            hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &pmu_get_press_time);
            hal_gpt_get_duration_count(pmu_gpt_start, pmu_get_press_time, &pmu_get_duration_time);
        } else {
            pmu_get_press_time = 0;
            log_hal_msgid_error("ON[%x]OFF[%x]DT[%d]PT[%d]",4,pmu_get_power_on_reason(),pmu_get_power_off_reason(),pmu_get_duration_time,PMU_PRESS_PK_TIME);
            log_hal_msgid_error("Boot up fail , press pk need more than the specific time %d or PMIC OP", 1,PMU_PRESS_PK_TIME);
            hal_gpt_delay_ms(1);
            pmu_power_off_sequence(PMU_RTC);
        }
        if (pmu_get_duration_time > PMU_PRESS_PK_TIME) {
            break;
        }
    }
}

void pmu_pwrkey_handler(uint16_t pwrkey_flag)
{
    uint16_t ad_reg;

    if (pwrkey_flag & (1 << PMU_PWRKEY_IRQ_RISE_FLAG))
    {
        if (pmu_pwrkey_status == 0)
        {
            if (pmu_function_table_2565[RG_INT_PWRKEY_FALL].pmu_callback)
                pmu_function_table_2565[RG_INT_PWRKEY_FALL].pmu_callback();
            pmu_pwrkey_status = 1;
        }
        else
        {
            log_hal_msgid_info("pmu_eint_handler, power key press error", 0);
            hal_gpt_delay_us(100);
            //assert(0);
        }
    }
    if (pwrkey_flag & (1 << PMU_PWRKEY_IRQ_FALL_FLAG))
    {
        if (pmu_pwrkey_status == 1)
        {
            ad_reg = pmu_get_register_value(AD_REGEN_ADDR, AD_REGEN_MASK, AD_REGEN_SHIFT);
            if (!ad_reg)
            {
                if (pmu_function_table_2565[RG_INT_PWRKEY_RISE].pmu_callback)
                    pmu_function_table_2565[RG_INT_PWRKEY_RISE].pmu_callback();
                pmu_pwrkey_status = 0;
            }
            else
            {
                log_hal_msgid_info("pmu_eint_handler, power key release ignore", 0);
            }
        }
        else
        {
            log_hal_msgid_info("pmu_eint_handler, power key release error", 0);
            hal_gpt_delay_us(100);
            //assert(0);
        }
    }
}

void pmu_eint_handler(void *parameter)
{
    UNUSED(parameter);
    uint16_t chg_reg, pwrkey_reg, chg_mask, pwrkey_mask, chg_flag, pwrkey_flag, tmp, reg_000;
    uint32_t time0 = 0;
    uint32_t time1 = 0;

    chg_reg = pmu_get_register_value(CHARGER_CON1, 0xFFFF, 0);
    pwrkey_reg = pmu_get_register_value(PMU_CON3, 0xFFFF, 0);
    reg_000 = pmu_get_register_value(PMU_CON0, 0xFFFF, 0);

    log_hal_msgid_info("pmu_eint_handler enter, chg_reg[0x%x], pwrkey_reg[0x%x]", 2, chg_reg, pwrkey_reg);

    chg_mask = chg_reg & 0x55;
    pwrkey_mask = pwrkey_reg & 0x5;
    chg_flag = chg_mask;
    pwrkey_flag = pwrkey_mask;

    if (chg_mask)
    {
        #if 0
        pmu_force_set_register_value(CHARGER_CON1, chg_reg);
        #else
        tmp = chg_reg & 0x1F00;
        pmu_force_set_register_value(CHARGER_CON1, tmp);
        tmp = chg_reg & 0x1F55;
        pmu_force_set_register_value(CHARGER_CON1, tmp);
        hal_gpt_delay_us(250);
        tmp = chg_reg & 0x1FAA;
        pmu_force_set_register_value(CHARGER_CON1, tmp);
        #endif
    }

    if (pwrkey_mask)
        pmu_force_set_register_value(PMU_CON3, pwrkey_reg);

    if (chg_flag)
    {
        pmu_charger_handler(chg_flag);
    }

    if (pwrkey_flag)
    {
        pmu_pwrkey_handler(pwrkey_flag);
    }

    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time0);
    while (1)
    {
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time1);

        if (chg_mask)
        {
            chg_reg = pmu_get_register_value(CHARGER_CON1, 0xFFFF, 0);
            chg_mask &= chg_reg;
        }
        if (pwrkey_mask)
        {
            pwrkey_reg = pmu_get_register_value(PMU_CON3, 0xFFFF, 0);
            pwrkey_mask &= pwrkey_reg;
        }
        if (((chg_mask == 0) && (pwrkey_mask == 0)) || ((time1 - time0) > 500))
            break;
    }
    hal_eint_unmask(HAL_EINT_PMU);

    log_hal_msgid_info("pmu_eint_handler, w1c_time:[%d us], reg_320[0x%x], reg_006[0x%x], chg_mask[0x%x], pwrkey_mask[0x%x], reg_000[0x%x]", 6,
        (time1 - time0), chg_reg, pwrkey_reg, chg_mask, pwrkey_mask, reg_000);

    if ((time1 - time0) > 500)
    {
        log_hal_msgid_info("pmu_eint_handler enter, W1C fail", 0);
        hal_gpt_delay_us(100);
        assert(0);
    }
#if 0
    if ((chg_flag == 0) && (pwrkey_flag == 0))
    {
        pmu_csr_write((int *)0xA2120600, 0x2);
        pmu_csr_write((int *)0xA2200030, 0x2);
        pmu_csr_write((int *)0xA2010008, 0x14);
        int tmp1 = pmu_csr_read((int *)0xA2010004);
        int tmp2 = pmu_csr_read((int *)0xA21205E4);
        log_hal_msgid_info("pmu_eint_handler, error intr, tmp1:[0x%x], tmp2:[0x%x]", 2, tmp1, tmp2);
        assert(0);
    }

    if ((chg_mask == 0) && (pwrkey_mask == 0))
        return;

    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time0);
    while (1)
    {
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time1);

        if (chg_mask)
        {
            chg_reg = pmu_get_register_value(CHARGER_CON1, 0xFFFF, 0);
            chg_mask &= chg_reg;
            if (chg_mask)
            {
                #if 0
                pmu_force_set_register_value(CHARGER_CON1, chg_reg);
                #else
                tmp = chg_reg & 0x1F00;
                pmu_force_set_register_value(CHARGER_CON1, tmp);
                tmp = chg_reg & 0x1F55;
                pmu_force_set_register_value(CHARGER_CON1, tmp);
                hal_gpt_delay_us(250);
                tmp = chg_reg & 0x1FAA;
                pmu_force_set_register_value(CHARGER_CON1, tmp);
                #endif
            }
        }
        if (pwrkey_mask)
        {
            pwrkey_reg = pmu_get_register_value(PMU_CON3, 0xFFFF, 0);
            pwrkey_mask &= pwrkey_reg;
            if (pwrkey_mask)
                pmu_force_set_register_value(PMU_CON3, pwrkey_reg);
        }
        if (((chg_mask == 0) && (pwrkey_mask == 0)))
            break;

        uint16_t reg_322 = pmu_get_register_value(0x322, 0xFFFF, 0);
        uint16_t reg_000 = pmu_get_register_value(0x000, 0xFFFF, 0);
        uint16_t reg_test;
        if (Adie_Version == PMU_ECO3)
            reg_test = pmu_get_register_value(0x60A, 0xFFFF, 0);
        else
            reg_test = pmu_get_register_value(0x604, 0xFFFF, 0);

        log_hal_msgid_info("pmu_eint_handler, time:[%d us], reg_320[0x%x], reg_006[0x%x], chg_mask[0x%x], pwrkey_mask[0x%x], , reg_322[0x%x], reg_test[0x%x], reg_000[x%x]", 8,
            (time1 - time0), chg_reg, pwrkey_reg, chg_mask, pwrkey_mask, reg_322, reg_test, reg_000);
        if ((time1-time0) > 100000)
            assert(0);
    }
#endif
}

void pmu_eint_init(void)
{
    log_hal_msgid_info("pmu_eint_init enter", 0);
    hal_eint_config_t config;
    config.trigger_mode = HAL_EINT_LEVEL_LOW;//HAL_EINT_EDGE_FALLING;
    config.debounce_time = 0;
    hal_eint_init(HAL_EINT_PMU, &config);    /*set EINT trigger mode and debounce time.*/
    hal_eint_register_callback(HAL_EINT_PMU, pmu_eint_handler, NULL);  /*register a user callback.*/
    hal_eint_unmask(HAL_EINT_PMU);
}

void pmu_get_all_int_status(void) {
    log_hal_msgid_info("pmu_get_all_int_status not support", 0);
#if 0
    if (pmu_irq_enable_com0 != 0) {
        pmic_irq0 = pmu_get_register_value_ab2568(PMU_INT_STATUS0, 0xffff, 0);
    }
    if (pmu_irq_enable_com1 != 0) {
        pmic_irq1 = pmu_get_register_value_ab2568(PMU_INT_STATUS1, 0xffff, 0);
    }
    if (pmu_irq_enable_com2 != 0) {
        pmic_irq2 = pmu_get_register_value_ab2568(PMU_INT_STATUS2, 0xffff, 0);
    }
    if (pmu_irq_enable_com3 != 0) {
        pmic_irq3 = pmu_get_register_value_ab2568(PMU_INT_STATUS3, 0xffff, 0);
    }
#endif
}

int pmu_get_status_interrupt(pmu_interrupt_index_t int_channel)
{
    UNUSED(int_channel);
    log_hal_msgid_info("pmu_get_status_interrupt not support", 0);
    return -1;
#if 0
    int statusValue = -1;
    if ((int_channel >= 0) && (int_channel <= 8)) {
        statusValue = pmu_get_register_value_ddie((uint32_t)&pmic_irq0, 1, int_channel);
    } else if (int_channel == 9) {
        statusValue = pmu_get_register_value_ab2568(PMU_INT_STATUS0, PMU_RG_INT_STATUS_CHRDET_MASK, PMU_RG_INT_STATUS_CHRDET_SHIFT);
    } else if ((int_channel >= 10) && (int_channel <= 15)) {
        statusValue = pmu_get_register_value_ddie((uint32_t)&pmic_irq0, 1, int_channel);
    }else if ((int_channel >= 16) && (int_channel <= 31)) {
        statusValue = pmu_get_register_value_ddie((uint32_t)&pmic_irq1, 1, (int_channel - 11));
    } else if ((int_channel >= 32) && (int_channel <= 39)) {
        statusValue = pmu_get_register_value_ddie((uint32_t)&pmic_irq2, 1, (int_channel - 24));
    } else if ((int_channel >= 40) && (int_channel <= 43)) {
        statusValue = pmu_get_register_value_ddie((uint32_t)&pmic_irq3, 1, (int_channel - 30));
    } else {
        log_hal_msgid_info("Error interrupt index", 0);
        return PMU_STATUS_INVALID_PARAMETER;
    }
    return statusValue;
#endif
}

pmu_status_t pmu_clear_interrupt(pmu_interrupt_index_t int_channel) {
    UNUSED(int_channel);
    log_hal_msgid_info("pmu_clear_interrupt not support", 0);
    return -1;
#if 0
    if (int_channel == PMU_INT_MAX) {
        if ((pmu_irq_enable_com0 != 0) || (event_con0 != 0)) {
            pmu_set_register_value_ab2568(PMU_INT_STATUS0, 0xffff, 0, 0xffff);
        }
        if ((pmu_irq_enable_com1 != 0) || (event_con1 != 0)) {
            pmu_set_register_value_ab2568(PMU_INT_STATUS1, 0xffff, 0, 0xffff);
        }
        if ((pmu_irq_enable_com2 != 0) || (event_con2 != 0)) {
            pmu_set_register_value_ab2568(PMU_INT_STATUS2, 0x3f, 0, 0x3f);
        }
        if ((pmu_irq_enable_com3 != 0) || (event_con3 != 0)) {
            pmu_set_register_value_ab2568(PMU_INT_STATUS3, 0xf, 0, 0xf);
        }
    } else {
        pmu_control_enable_interrupt(int_channel, 0);
        hal_gpt_delay_us(150);
        pmu_control_enable_interrupt(int_channel, 1);
    }
    return PMU_STATUS_SUCCESS;
#endif
}

pmu_status_t pmu_control_enable_interrupt(pmu_interrupt_index_t int_channel, int isEnable) {
    UNUSED(int_channel);
    UNUSED(isEnable);
    log_hal_msgid_info("pmu_control_enable_interrupt not support", 0);
    return -1;
#if 0
    if ((int_channel >= 0) && (int_channel <= 15)) {
        pmu_set_register_value_ab2568(PMU_INT_CON0, 0x1, int_channel, isEnable);
        if (isEnable) {
            pmu_irq_enable_com0 |= 0x1 << int_channel;
        } else {
            pmu_set_register_value_ddie((uint32_t)&pmu_irq_enable_com0, 0x1, int_channel, 0);
        }
    } else if ((int_channel >= 16) && (int_channel <= 31)) {
        pmu_set_register_value_ab2568(PMU_INT_CON1, 0x1, (int_channel - 11), isEnable);

        if (isEnable) {
            pmu_irq_enable_com1 |= 0x1 << (int_channel - 16);
        } else {
            pmu_set_register_value_ddie((uint32_t)&pmu_irq_enable_com1, 0x1, (int_channel - 11), 0);
        }
    } else if ((int_channel >= 32) && (int_channel <= 39)) {
        pmu_set_register_value_ab2568(PMU_INT_CON2, 0x1, (int_channel - 24), isEnable);

        if (isEnable) {
            pmu_irq_enable_com2 |= 0x1 << (int_channel - 32);
        } else {
            pmu_set_register_value_ddie((uint32_t)&pmu_irq_enable_com2, 0x1, (int_channel - 24), 0);
        }
    } else if ((int_channel >= 40) && (int_channel <= 43)) {
        pmu_set_register_value_ab2568(PMU_INT_CON3, 0x1, (int_channel - 30), isEnable);
        if (isEnable) {
            pmu_irq_enable_com3 |= 0x1 << (int_channel - 40);
        } else {
            pmu_set_register_value_ddie((uint32_t)&pmu_irq_enable_com3, 0x1, (int_channel - 30), 0);
        }
    } else {
        log_hal_msgid_info("Error interrupt index", 0);
        return PMU_STATUS_ERROR;
    }

    return PMU_STATUS_SUCCESS;
#endif
}

pmu_status_t pmu_control_mask_interrupt(pmu_interrupt_index_t int_channel, int isEnable)
{
    UNUSED(int_channel);
    UNUSED(isEnable);
    log_hal_msgid_info("pmu_control_mask_interrupt not support", 0);
    return -1;
#if 0
    if ((int_channel >= 0) && (int_channel <= 15)) {
        pmu_set_register_value_ab2568(PMU_INT_MASK_CON0, 0x1, int_channel, isEnable);
    } else if ((int_channel >= 16) && (int_channel <= 31)) {
        pmu_set_register_value_ab2568(PMU_INT_MASK_CON1, 0x1, (int_channel - 11), isEnable);
    } else if ((int_channel >= 32) && (int_channel <= 39)) {
        pmu_set_register_value_ab2568(PMU_INT_MASK_CON2, 0x1, (int_channel - 24), isEnable);
    } else if ((int_channel >= 40) && (int_channel <= 43)) {
        pmu_set_register_value_ab2568(PMU_INT_MASK_CON3, 0x1, (int_channel - 30), isEnable);
    } else {
        log_hal_msgid_info("Error interrupt index", 0);
        return PMU_STATUS_ERROR;
    }
    return PMU_STATUS_SUCCESS;
#endif
}

void pmu_irq_count(int int_channel) {
    UNUSED(int_channel);
    log_hal_msgid_info("pmu_irq_count not support", 0);
#if 0
    if ((int_channel >= 0) && (int_channel <= 15)) {
        event_con0 = 1;
    } else if ((int_channel >= 16) && (int_channel <= 31)) {
        event_con1 = 1;
    } else if ((int_channel >= 32) && (int_channel <= 39)) {
        event_con2 = 1;
    } else if ((int_channel >= 40) && (int_channel <= 43)) {
        event_con3 = 1;
    }
#endif
}

void pmu_irq_init(void) {
    log_hal_msgid_info("pmu_irq_init not support", 0);
#if 0
    if (pmu_init_flag == 0) {
        pmu_set_register_value_ab2568(PMU_INT_CON0, 0xffff, 0, 0);
        pmu_set_register_value_ab2568(PMU_INT_CON1, 0xffff, 0, 0);
        pmu_set_register_value_ab2568(PMU_INT_CON2, 0xffff, 0, 0);
        pmu_set_register_value_ab2568(PMU_INT_CON3, 0xffff, 0, 0);
        pmu_irq_enable_com0 = 0;
        pmu_irq_enable_com1 = 0;
        pmu_irq_enable_com2 = 0;
        pmu_irq_enable_com3 = 0;
    }
#endif
}

void pmu_scan_interrupt_status(void)
{
    log_hal_msgid_info("pmu_scan_interrupt_status not support", 0);
    return -1;
#if 0
    uint8_t index = 0xFF;
    uint8_t value = 0;
    for(index = RG_INT_PWRKEY; index < PMU_INT_MAX; index++)
    {
        value = pmu_get_status_interrupt(index);
        if(value == 1)
        {
            if (pmu_function_table[index].isMask == false) {
                if (pmu_function_table[index].pmu_callback)
                    pmu_function_table[index].pmu_callback();
                //Clear Interrupt
                pmu_control_enable_interrupt(index, 0);
                pmu_control_enable_interrupt(index, 1);
            }
        }
    }
#endif
}

/*==========[PMIC Basic setting]==========*/
void pmu_init_2565() {
    log_hal_msgid_info("pmu_init_2565 enter", 0);
    //pmu_force_set_register_value(0x0000, 0x4007);
    pmu_get_power_on_reason();
    pmu_get_power_off_reason();

    if (Adie_Version == PMU_ECO3 || Adie_Version == PMU_NO_FTK)
    {
        pmu_set_register_value_2565(0x0004, 0x7, 9, 0x7);
        pmu_set_register_value_2565(0x0010, 0xFF, 8, 0x0);
        pmu_set_register_value_2565(0x000E, 0x1, 2, 0x1);
        pmu_set_register_value_2565(0x0004, 0xF, 5, 0xF);//NVKEY OPT
        pmu_set_register_value_2565(0x0106, 0x1, 0, 0x1);
        pmu_set_register_value_2565(0x0100, 0x1, 0, 0x1);
        pmu_set_register_value_2565(0x0002, 0x1, 3, 0x1);
        pmu_set_register_value_2565(0x0016, 0x7, 12, 0x0);
        pmu_set_register_value_2565(0x0328, 0xF, 8, 0x0);
        pmu_set_register_value_2565(0x0020, 0x1F, 4, 0x1F);
        pmu_set_register_value_2565(0x0020, 0x3, 0, 0x3);
    }
    else
    {
        pmu_set_register_value_2565(0x0004, 0x7, 9, 0x6);
        pmu_set_register_value_2565(0x0010, 0xFF, 8, 0xFF);
        if (Adie_Version == PMU_ECO1)
            pmu_set_register_value_2565(0x000E, 0x1, 2, 0x1);
        else
            pmu_set_register_value_2565(0x000E, 0x1, 2, 0x0);
        pmu_set_register_value_2565(0x0004, 0xF, 5, 0xF);//NVKEY OPT
        pmu_set_register_value_2565(BUCK_DISQ_MV_OFF_E2_ADDR, BUCK_DISQ_MV_OFF_E2_MASK, BUCK_DISQ_MV_OFF_E2_SHIFT, 0x1);
        pmu_set_register_value_2565(BUCK_DISQ_LV_OFF_E2_ADDR, BUCK_DISQ_LV_OFF_E2_MASK, BUCK_DISQ_LV_OFF_E2_SHIFT, 0x1);
        pmu_set_register_value_2565(0x0002, 0x1, 3, 0x1);
        pmu_set_register_value_2565(0x0016, 0x7, 12, 0x0);
    }

    uint16_t tmp = pmu_get_register_value_2565(0x320, 0xFFFF, 0);
    tmp &= 0xFAA;
    pmu_force_set_register_value(0x320, tmp);

    pmu_eint_init();
    //pmu_bat_init();
    pmu_charger_init(0, 0);
    pmu_auxadc_init();

#ifdef HAL_SLEEP_MANAGER_ENABLED
    sleep_management_register_suspend_callback(SLEEP_BACKUP_RESTORE_PMU, (sleep_management_suspend_callback_t)hal_pmu_sleep_backup, NULL);
    sleep_management_register_resume_callback(SLEEP_BACKUP_RESTORE_PMU, (sleep_management_suspend_callback_t)hal_pmu_sleep_resume, NULL);
#endif
    //pmu_assert_exception_dump();
    if (pmu_get_power_on_reason() & 0x1)
        pmu_press_pk_time();
}

uint32_t pmu_d2d_i2c_read(unsigned char *ptr_send, unsigned char *ptr_read, int type) {
    hal_i2c_send_to_receive_config_t config;
    unsigned char retry_cnt = 0, result_read;
    if (type == 1) {
        *(ptr_send) = *(ptr_send) | 0x40;
        config.receive_length = 1;
    } else {
        config.receive_length = 2;
    }
    config.slave_address = PMIC_SLAVE_ADDR;
    config.send_data = ptr_send;
    config.send_length = 2;
    config.receive_buffer = ptr_read;
    do {
        result_read = hal_i2c_master_send_to_receive_polling(HAL_I2C_MASTER_AO, &config);
        retry_cnt++;
    } while ((result_read != 0) && (retry_cnt <= 60));

    if (result_read != 0)
    {
        log_hal_msgid_info("pmu_d2d_i2c_read fail", 0);
        assert(0);
        //return PMU_ERROR;
    }

    retry_cnt--;
    return (retry_cnt);
}

uint32_t pmu_get_register_value_ab2568(uint32_t address, uint32_t mask, uint32_t shift) {
    UNUSED(address);
    UNUSED(mask);
    UNUSED(shift);
    log_hal_msgid_info("pmu_get_register_value_ab2568 not support", 0);
    return -1;
#if 0
    unsigned char send_buffer[4], receive_buffer[2];
    uint32_t value;
    pmic_i2c_init();
    send_buffer[1] = address & 0x00FF; //D2D 2Byte
    send_buffer[0] = ((address >> 8) & 0x00FF) & 0x0F;
    pmu_d2d_i2c_read(send_buffer, receive_buffer, 2);
    value = (receive_buffer[1] << 8) + receive_buffer[0];
    return ((value >> shift) & mask);
    return 0;
#endif
}

pmu_operate_status_t pmu_set_register_value_ab2568(uint32_t address, uint32_t mask, uint32_t shift, uint32_t value) {
    UNUSED(address);
    UNUSED(mask);
    UNUSED(shift);
    UNUSED(value);
    log_hal_msgid_info("pmu_set_register_value_ab2568 not support", 0);
    return -1;
#if 0
    unsigned char send_buffer[4], receive_buffer[2];
    uint32_t data;
    pmic_i2c_init();
    send_buffer[1] = address & 0x00FF;
    send_buffer[0] = ((address >> 8) & 0x00FF) & 0x0F;
    pmu_d2d_i2c_read(send_buffer, receive_buffer, 2);

    data = receive_buffer[1];
    data = (data << 8) | receive_buffer[0];
    data &= (~(mask << shift));
    data = data | (value << shift);

    send_buffer[0] = ((address >> 8) & 0x00FF) | 0x00;
    send_buffer[1] = (address) & 0x00FF;
    send_buffer[2] = (data & 0xFF);
    send_buffer[3] = ((data >> 8) & 0xFF);

    unsigned char retry_cnt = 0, result_read;
    do {
        result_read = hal_i2c_master_send_polling(HAL_I2C_MASTER_AO, PMIC_SLAVE_ADDR, send_buffer, 4);
        retry_cnt++;
    } while ((result_read != 0) && (retry_cnt <= 60));
    return PMU_OK;
#endif
}

void pmic_i2c_init(void) {
    int status;
    hal_i2c_config_t config;
    uint32_t mask_pri;
    hal_nvic_save_and_set_interrupt_mask_special(&mask_pri);

    if(pmu_i2c_init_sta == 1){
    hal_nvic_restore_interrupt_mask_special(mask_pri);
    return;
    }

#ifdef AB1555
        config.frequency =HAL_I2C_FREQUENCY_400K;
#else
        config.frequency = HAL_I2C_FREQUENCY_1M;
#endif
    status = hal_i2c_master_init(HAL_I2C_MASTER_AO, &config);
    if (status != HAL_I2C_STATUS_OK) {
        assert(0);
    }
    pmu_i2c_init_sta = 1;

#if defined(AB1565)
    pmu_set_register_value_2565_init();
#endif
    hal_i2c_master_set_io_config(HAL_I2C_MASTER_AO, HAL_I2C_IO_PUSH_PULL);
#if defined(AB1565)
    i2c_set_frequency(HAL_I2C_MASTER_AO, HAL_I2C_FREQUENCY_3M);
#endif
    hal_nvic_restore_interrupt_mask_special(mask_pri);
}

void pmic_i2c_deinit(void) {
    uint32_t mask_pri;
    hal_nvic_save_and_set_interrupt_mask(&mask_pri);
    hal_i2c_master_deinit(HAL_I2C_MASTER_AO);
    pmu_i2c_init_sta=0;
    hal_nvic_restore_interrupt_mask(mask_pri);
}

uint32_t pmu_get_register_value_2565(uint32_t address, uint32_t mask, uint32_t shift)
{
    unsigned char send_buffer[4], receive_buffer[2];
    uint32_t value;
    pmic_i2c_init();
    send_buffer[1] = address & 0x00FF;
    send_buffer[0] = ((address >> 8) & 0x00FF) & 0x0F;
    pmu_d2d_i2c_read(send_buffer, receive_buffer, 2);
    value = (receive_buffer[1] << 8) + receive_buffer[0];
    return ((value >> shift) & mask);
}

pmu_operate_status_t pmu_set_register_value_2565_init(void)
{
    unsigned char send_buffer[4], receive_buffer[2];
    uint32_t data;
    unsigned char retry_cnt = 0, result_read;

   /* send_buffer[1] = I2C_DRV_SEL_ADDR & 0x00FF;
    send_buffer[0] = ((I2C_DRV_SEL_ADDR >> 8) & 0x00FF) & 0x0F;
    pmu_d2d_i2c_read(send_buffer, receive_buffer, 2);
    log_hal_msgid_info("pmu_set_register_value_2565_init, receive_buffer1[0x%x], receive_buffer0[0x%x]",
        2, receive_buffer[1], receive_buffer[0]);*/

    data = 0x00;//receive_buffer[1];
    data = (data << 8) | 0x07;//receive_buffer[0];
    data &= (~(I2C_DRV_SEL_MASK << I2C_DRV_SEL_SHIFT));
    data = data | (0x1 << I2C_DRV_SEL_SHIFT);

    send_buffer[0] = ((I2C_DRV_SEL_ADDR >> 8) & 0x00FF) | 0x00;
    send_buffer[1] = (I2C_DRV_SEL_ADDR) & 0x00FF;
    send_buffer[2] = (data & 0xFF);
    send_buffer[3] = ((data >> 8) & 0xFF);
    /*log_hal_msgid_info("pmu_set_register_value_2565_init, send_buffer3[0x%x], send_buffer2[0x%x], send_buffer1[0x%x], send_buffer0[0x%x]",
        4, send_buffer[3], send_buffer[2], send_buffer[1], send_buffer[0]);*/

    do {
        result_read = hal_i2c_master_send_polling(HAL_I2C_MASTER_AO, PMIC_SLAVE_ADDR, send_buffer, 4);
        retry_cnt++;
    } while ((result_read != 0) && (retry_cnt <= 60));

    if (result_read != 0)
    {
        log_hal_msgid_info("pmu_set_register_value_2565_init fail", 0);
        assert(0);
        //return PMU_ERROR;
    }
    return PMU_OK;
}

pmu_operate_status_t pmu_set_register_value_2565(uint32_t address, uint32_t mask, uint32_t shift, uint32_t value)
{
    unsigned char send_buffer[4], receive_buffer[2];
    uint32_t data;
    unsigned char retry_cnt = 0, result_read;

    pmic_i2c_init();
    send_buffer[1] = address & 0x00FF;
    send_buffer[0] = ((address >> 8) & 0x00FF) & 0x0F;
    pmu_d2d_i2c_read(send_buffer, receive_buffer, 2);

    data = receive_buffer[1];
    data = (data << 8) | receive_buffer[0];
    data &= (~(mask << shift));
    data = data | (value << shift);

    send_buffer[0] = ((address >> 8) & 0x00FF) | 0x00;
    send_buffer[1] = (address) & 0x00FF;
    send_buffer[2] = (data & 0xFF);
    send_buffer[3] = ((data >> 8) & 0xFF);

    do {
        result_read = hal_i2c_master_send_polling(HAL_I2C_MASTER_AO, PMIC_SLAVE_ADDR, send_buffer, 4);
        retry_cnt++;
    } while ((result_read != 0) && (retry_cnt <= 60));

    if (result_read != 0)
    {
        log_hal_msgid_info("pmu_set_register_value_2565 fail, addr[0x%x], val[0x%x]", 2, address, value);
        assert(0);
        //return PMU_ERROR;
    }

    return PMU_OK;
}

pmu_operate_status_t pmu_force_set_register_value_2565(uint32_t address, uint32_t value)
{
    unsigned char send_buffer[4];
    unsigned char retry_cnt = 0, result_read;

    pmic_i2c_init();
    send_buffer[0] = ((address >> 8) & 0x00FF) | 0x00;
    send_buffer[1] = (address) & 0x00FF;
    send_buffer[2] = (value & 0xFF);
    send_buffer[3] = ((value >> 8) & 0xFF);

    do {
        result_read = hal_i2c_master_send_polling(HAL_I2C_MASTER_AO, PMIC_SLAVE_ADDR, send_buffer, 4);
        retry_cnt++;
    }while ((result_read != 0) && (retry_cnt <= 60));

    if (result_read != 0)
    {
        log_hal_msgid_info("pmu_force_set_register_value_2565 fail, addr[0x%x], val[0x%x]", 2, address, value);
        assert(0);
        //return PMU_ERROR;
    }

    return PMU_OK;
}

void pmu_read_rg(unsigned short int address)
{
    uint16_t reg = 0;
    reg = pmu_get_register_value_2565(address, 0xFFFF, 0);
    log_hal_msgid_info("pmu_read_rg, addr[0x%x], val[0x%x]", 2, address, reg);
}

void hal_pmu_sleep_backup(void) {
    pmu_power_off_sequence(PMU_SLEEP);
    pmic_i2c_deinit();
}

void hal_pmu_sleep_resume(void) {
    pmu_set_register_value_2565(PMU_CON2, 0xF, 5, 0xF);
}

void pmu_set_register_value_ddie(uint32_t address, short int mask, short int shift, short int value) {
    UNUSED(address);
    UNUSED(mask);
    UNUSED(shift);
    UNUSED(value);
    log_hal_msgid_info("pmu_set_register_value_ddie not support", 0);
    /*uint32_t mask_buffer,target_value;
    mask_buffer = (~(mask << shift));
    target_value = *((volatile uint32_t *)(address));
    target_value &= mask_buffer;
    target_value |= (value << shift);
    *((volatile uint32_t *)(address)) = target_value;*/
}

uint32_t pmu_get_register_value_ddie(uint32_t address, short int mask, short int shift) {
    UNUSED(address);
    UNUSED(mask);
    UNUSED(shift);
    log_hal_msgid_info("pmu_get_register_value_ddie not support", 0);
    return PMU_STATUS_INVALID_PARAMETER;
    /*uint32_t change_value, mask_buffer;
    mask_buffer = (mask << shift);
    change_value = *((volatile uint32_t *)(address));
    change_value &=mask_buffer;
    change_value = (change_value>> shift);
    return change_value;*/
}

void pmu_lock_va18_2568(int oper){
    UNUSED(oper);
    log_hal_msgid_info("pmu_get_register_value_ddie not support", 0);
#if 0
    if(oper){
        va18_flag =PMU_ON;
    }else{
        va18_flag =PMU_OFF;
    }
#endif
}



#endif /* HAL_PMU_MODULE_ENABLED */
