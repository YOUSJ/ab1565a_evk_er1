/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hal_platform.h"

#ifdef HAL_ESC_MODULE_ENABLED
/* hal includes */
#include <memory_attribute.h>
#include "syslog.h"
#include "hal_esc_config.h"
#include "hal_esc.h"
#include "hal_esc_internal.h"

#include "hal_nvic.h"
#include "hal_gpt.h"

#ifndef min
#define min(_a, _b)   (((_a)<(_b))?(_a):(_b))
#endif

#ifndef max
#define max(_a, _b)   (((_a)>(_b))?(_a):(_b))
#endif

extern hal_esc_mode_t mode_sram;

#ifdef ESC_SFLASH
static int32_t flash_write_en(void);
#endif

//it should be on AO domain
ESC_REGISTER_T *esc_register = (ESC_REGISTER_T *)ESC_BASE;

static int32_t esc_reset(void);


//-----------------------------------------------------------------------------
/*!
  @brief
    ESC abort ISR
*/
ATTR_TEXT_IN_TCM static void esc_isr(hal_nvic_irq_t irq)
{
    (void)irq;
    //ack IRQ bit 16
    if ((esc_register->ESC_MAC_IRQ & 0x00000100) == 0x00000100) {
        esc_register->ESC_MAC_IRQ = esc_register->ESC_MAC_IRQ | 0x00010000;
        esc_register->ESC_MAC_IRQ = esc_register->ESC_MAC_IRQ & 0xFFFEFFFF;
    }
    //ack IRQ bit 17
    if ((esc_register->ESC_MAC_IRQ & 0x00000200) == 0x00000200) {
        esc_register->ESC_MAC_IRQ = esc_register->ESC_MAC_IRQ | 0x00020000;
        esc_register->ESC_MAC_IRQ = esc_register->ESC_MAC_IRQ & 0xFFFBFFFF;
    }

    //ack abort bit28
    if ((esc_register->ESC_MAC_IRQ & 0x02000000) == 0x02000000) {
        esc_register->ESC_MAC_IRQ = esc_register->ESC_MAC_IRQ | 0x10000000;
        esc_register->ESC_MAC_IRQ = esc_register->ESC_MAC_IRQ & 0xEFFFFFFF;
    }
    return;
}

ATTR_TEXT_IN_TCM static uint32_t ESC_ReverseByteOrder(uint32_t num)
{
    unsigned int ret = 0;

    ret |= ((num >> ESC_GENERIC_24_BIT_OFFSET) & ESC_GENERIC_0x000000FF_MASK);
    ret |= ((num >> ESC_GENERIC_8_BIT_OFFSET)  & ESC_GENERIC_0x0000FF00_MASK);
    ret |= ((num << ESC_GENERIC_8_BIT_OFFSET)  & ESC_GENERIC_0x00FF0000_MASK);
    ret |= ((num << ESC_GENERIC_24_BIT_OFFSET) & ESC_GENERIC_0xFF000000_MASK);

    return ret;
}


//-----------------------------------------------------------------------------
/*!
  @brief: Enter macro mode
*/
ATTR_TEXT_IN_TCM void ESC_MacEnable(void)
{
    esc_register->ESC_MAC_CTL |= ESC_MAC_EN;
}

//-----------------------------------------------------------------------------
/*!
  @brief: Leaves macro mode, and returns to direct read mode
*/
ATTR_TEXT_IN_TCM void ESC_MacLeave(void)
{
    uint32_t val;

    // clear ESC_MAC_TRIG and leave MACRO mode
    val = esc_register->ESC_MAC_CTL;
    val &= ~(ESC_MAC_TRIG | ESC_MAC_EN);
    esc_register->ESC_MAC_CTL = val;
    while (esc_register->ESC_MAC_CTL & ESC_MAC_WAIT_DONE);
    while (esc_register->ESC_MAC_CTL & ESC_MAC_EN);
}

//-----------------------------------------------------------------------------
/*!
  @brief: Send commands placed in GPRAM (in macro mode)
*/
ATTR_TEXT_IN_TCM uint32_t ESC_MacTrigger(void)
{
    uint32_t  val = 0;

    val = esc_register->ESC_MAC_CTL;
    // trigger ESC
    val |= (ESC_MAC_TRIG | ESC_MAC_EN);
    esc_register->ESC_MAC_CTL = val;
    // wait for ESC ready
    while (esc_register->ESC_MAC_CTL & ESC_MAC_WAIT_DONE);
    return 0;
}

//-----------------------------------------------------------------------------
/*!
  @brief
    Set trigger and send GPRAM data toward serial Flash
    Leave macro mode when done
*/
ATTR_TEXT_IN_TCM void ESC_MacWaitReady(void)
{
    ESC_MacTrigger();
    ESC_MacLeave();
}


//-----------------------------------------------------------------------------
/*!
  @brief
    Issue generic command to serial Flash (max: 4 bytes)

  @param[in] cmd The command to be sent, the first shift out byte is MSB.
  @remarks This function is only called by bootloader.
           Other callers must be aware of interrupts during the MAC mode
*/
ATTR_TEXT_IN_TCM void ESC_Dev_Command(const uint16_t CS, const uint32_t cmd)
{
    uint32_t cmdi, cmdo;
    uint8_t  len;
    uint32_t mask;

    for (cmdi = cmd, len = 0, cmdo = 0; cmdi != 0; cmdi = cmdi >> ESC_GENERIC_8_BIT_OFFSET, len++) {
        cmdo = (cmdo << ESC_GENERIC_8_BIT_OFFSET) | (cmdi & ESC_GENERIC_0xFF_MASK);
    }

    if (len == 0) {
        return;
    }

    *(volatile uint32_t *)ESC_GPRAM_BASE = cmdo;
    esc_register->ESC_MAC_OUTL = len;
    esc_register->ESC_MAC_INL = 0;
    ESC_MacEnable();
    ESC_MacWaitReady();

    return;
}

//-----------------------------------------------------------------------------
/*!
  @brief
    Send command and address to device

  @param[in] cmd The command to be sent
  @param[in] address The address to be sent, followed by cmd.
  @remarks callers must be aware of the code execute region as it will enter MAC mode
*/
ATTR_TEXT_IN_TCM void ESC_Dev_CommandAddress(const uint16_t CS, const uint8_t cmd, const uint32_t address, const uint32_t address_bytes)
{
    uint32_t cmd1, cmd2;
    uint32_t mask;

    cmd2 = ESC_ReverseByteOrder(address);

    if (address_bytes == 3) {
        cmd1 = (cmd2 & ESC_GENERIC_0xFFFFFF00_MASK) | cmd;
    } else {
        cmd1 = (cmd2 << ESC_GENERIC_8_BIT_OFFSET) | cmd;
        cmd2 = cmd2 >> ESC_GENERIC_24_BIT_OFFSET;
    }

    hal_nvic_save_and_set_interrupt_mask_special(&mask);
    *(volatile uint32_t *)ESC_GPRAM_BASE = cmd1;
    *(volatile uint32_t *)(ESC_GPRAM_BASE + 4) = cmd2; //It is for 4bytes address
    esc_register->ESC_MAC_OUTL = address_bytes + 1;
    esc_register->ESC_MAC_INL = 0;

    ESC_MacEnable();
    ESC_MacWaitReady();
    hal_nvic_restore_interrupt_mask_special(mask);
    return;
}

//-----------------------------------------------------------------------------
/*!
  @brief
    Issue generic command to device, and read results.

  @param[in] cmd Pointer to the commands that to be sent
  @param[out] data Pointer to the data buffer that to be stored
  @param[in] outl Length of commands (in bytes)
  @param[in] intl Length of read data

  @remarks This function shall only be invoked in bootloader.
           Other callers must be aware of interrupts during the MAC mode
*/
ATTR_TEXT_IN_TCM void ESC_Dev_Command_Ext(const uint16_t CS, const uint8_t *cmd, uint8_t *data, const uint16_t outl, const uint16_t inl)
{
    uint32_t tmp;
    uint32_t i, j;
    uint8_t *p_data, *p_tmp;
    uint32_t mask;

    p_tmp = (uint8_t *)(&tmp);

    // hal_nvic_save_and_set_interrupt_mask_special(&mask);
    for (i = 0, p_data = ((uint8_t *)ESC_GPRAM_DATA); i < outl; p_data += 4) {
        for (j = 0, tmp = 0; i < outl && j < 4; i++, j++) {
            p_tmp[j] = cmd[i];
        }
        ESC_WriteReg32(p_data, tmp);
    }
    esc_register->ESC_MAC_OUTL = outl;
    esc_register->ESC_MAC_INL = inl;
    ESC_MacEnable();
    ESC_MacWaitReady();

    for (i = 0, p_data = ((uint8_t *)ESC_GPRAM_DATA + outl); i < inl; ++i, ++data, ++p_data) {
        *data = ESC_ReadReg8(p_data);
    }
    // hal_nvic_restore_interrupt_mask_special(mask);
    return;
}

#if defined(ESC_SFLASH) || defined(ASP_CHIP_1604M_SQ_PSRAM)
/*
     bit 7     6     5    4  3  2  1  0
         W/R  W/R    -
         MODE MODE   -

           0   0   byte mode
           1   0   page mode
           0   1   sequential mode(default operation)
           1   1   reserved
*/

ATTR_TEXT_IN_TCM void read_mode_register(uint8_t *sr)
{
    uint8_t cmd[4];

    cmd[0] = ESC_CMD_MODE_READ;
#ifdef ESC_SFLASH
    //uint32_t mask;
    //hal_nvic_save_and_set_interrupt_mask_special(&mask);
    ESC_Dev_Command_Ext(0, &cmd[0], sr, 1, 1);
    //hal_nvic_restore_interrupt_mask_special(mask);
#endif

#ifdef MICRO_CHIP_23A1024
    ESC_Dev_Command_Ext(0, &cmd[0], sr, 1, 1);
#endif
#ifdef ASP_CHIP_1604M_SQ_PSRAM
    esc_register->ESC_MAC_CTL = esc_register->ESC_MAC_CTL | (0x71 << 24);
    cmd[1] = 0;
    cmd[2] = 0;
    cmd[3] = 0;
    ESC_Dev_Command_Ext(0, &cmd[0], sr, 4, 1);
#endif
    return;
}

#ifdef W25Q32JW
ATTR_TEXT_IN_TCM void read_mode_register2(uint8_t *sr)
{
    uint8_t cmd[4];
    //uint32_t mask;
    //hal_nvic_save_and_set_interrupt_mask_special(&mask);
    cmd[0] = ESC_CMD_READ_SR2;
    ESC_Dev_Command_Ext(0, &cmd[0], sr, 1, 1);
    //hal_nvic_restore_interrupt_mask_special(mask);
    return;
}

ATTR_TEXT_IN_TCM void write_mode_register2(uint8_t sr)
{
    uint8_t cmd[4];
    //uint32_t mask;

    //hal_nvic_save_and_set_interrupt_mask_special(&mask);
    flash_write_en();
    cmd[0] = ESC_CMD_MODE_WRITE_SR2;
    cmd[1] = sr;
    ESC_Dev_Command_Ext(0, &cmd[0], NULL, 2, 0);
    //hal_nvic_restore_interrupt_mask_special(mask);
    return;
}
#endif

ATTR_TEXT_IN_TCM int32_t esc_wait_ready(uint32_t ms)
{
    uint32_t count;
    uint8_t sr = 0xff;

    hal_gpt_delay_ms(1);

    /* one chip guarantees max 5 msec wait here after page writes,
     * but potentially three seconds (!) after page erase.
     */
    for (count = 0;  count < ((ms + 1) * 1000); count++) {
#ifdef ESC_SFLASH
        read_mode_register(&sr);
        //printf("esc_wait_ready sr = 0x%x \r\n", sr);
        if (!(sr & 1)) {
            return 0;
        }
        hal_gpt_delay_ms(1);
#else
        hal_gpt_delay_us(100);
        return 0;
#endif
    }

    return -1;
}


/*!
  @brief
    Read 3 bytes ID (Vendor + Density + Device) in request mode (SPI/QPI)
  @param[out] id The pointer to the array that ID to be stored
  @remarks This function shall only be invoked in bootloader.
*/

ATTR_TEXT_IN_TCM void esc_read_device_id(uint8_t *id)
{
    uint32_t cmd = ESC_CMD_READ_ID;

#ifdef ESC_SFLASH
    //uint32_t mask;
    //hal_nvic_save_and_set_interrupt_mask_special(&mask);
    ESC_Dev_Command_Ext(0, &cmd, id, 1, 3);
    printf("device id = 0x%x, 0x%x, 0x%x\r\n", id[0], id[1], id[2]);
    //hal_nvic_restore_interrupt_mask_special(mask);
#endif

#ifdef ASP_CHIP_1604M_SQ_PSRAM
    ESC_Dev_Command_Ext(0, &cmd, id, 4, 2);
    printf("device id = 0x%x, 0x%x\r\n", id[0], id[1]);
#endif

    return;
}
#endif //defined(ESC_SFLASH) || defined(ASP_CHIP_1604M_SQ_PSRAM)

ATTR_TEXT_IN_TCM void esc_set_mode(hal_esc_mode_t mode)
{
    uint32_t val;

    //MAC dummy enable + dummy clcyle
    //val = esc_register->ESC_MAC_CTL & (0x00FFFFFF | 0x1000000 | 0x10000000);
    //esc_register->ESC_MAC_CTL = val;
    if (mode == ESC_SPI_MODE) {
        esc_register->ESC_MAC_CTL = 0x71000000;
        val = 0x0B027002;
        esc_register->ESC_DIRECT_CTL = val;    //set fast read
    } else if (mode == ESC_SPIQ_MODE) {
        /* should mind: dummy cycle = 1 + RDDUMMY_CYC
           set default R/W CMD | read dummy cycle 6 | QIO | read dummy enable */
        val = 0xEB385042;
        esc_register->ESC_DIRECT_CTL = val;    //set QIO
    } else if (mode == ESC_QPI_MODE) {
        /* should mind: dummy cycle = 1 + RDDUMMY_CYC
           set default R/W CMD | read dummy cycle 6 | QPI | read dummy enable */
        val = 0xEB385052;
        esc_register->ESC_DIRECT_CTL = val;
    }
    esc_reset();
}

ATTR_TEXT_IN_TCM void ESC_Switch_mode(const uint16_t CS, hal_esc_mode_t mode)
{
    //uint32_t val, mask;
    //hal_nvic_save_and_set_interrupt_mask_special(&mask);

    esc_register->ESC_DLY_CTL1 = ((ESC_DLY_CTL1_CYCLE << 24) | (ESC_DLY_CTL1_CYCLE << 16) | (ESC_DLY_CTL1_CYCLE << 8) | (ESC_DLY_CTL1_CYCLE));
    esc_register->ESC_DLY_CTL3 = ESC_DLY_CTL3_CYCLE;
    esc_register->ESC_DLY_CTL2 = ((ESC_DLY_CTL2_CYCLE << 24) | (ESC_DLY_CTL2_CYCLE << 16) | (ESC_DLY_CTL2_CYCLE << 8) | (ESC_DLY_CTL2_CYCLE));
    esc_register->ESC_DLY_CTL4 =  ESC_DLY_CTL4_CYCLE;

    switch (mode) {
        case ESC_QPI_MODE: {
#ifdef ESC_SFLASH
            write_mode_register2(2);   //set Q bit

#endif
            esc_set_mode(mode);
            ESC_Dev_Command(0, ESC_CMD_ENTER_QUAD_MODE);
        }
        break;
        case ESC_SPIQ_MODE: {
#ifdef ESC_SFLASH
            write_mode_register2(2);
#endif
            esc_set_mode(mode);
        }
        break;
        case ESC_SPI_MODE: {
            esc_set_mode(mode);
        }
        break;
        default:
            break;
    }
    //hal_nvic_restore_interrupt_mask_special(mask);
}


ATTR_TEXT_IN_TCM void reset_esc_register(void)
{
    esc_register->ESC_MAC_CTL    = 0x00000000;
    esc_register->ESC_DIRECT_CTL = 0x03027000;
    esc_register->ESC_MISC_CTL1  = 0x00000100;
    esc_register->ESC_MISC_CTL2  = 0x00000000;
    esc_register->ESC_MAC_OUTL   = 0x00000000;
    esc_register->ESC_MAC_INL    = 0x00000000;
    esc_register->ESC_STA1_CTL   = 0x00000000;
    esc_register->ESC_STA2_CTL   = 0x00000000;
    esc_register->ESC_DLY_CTL1   = 0x00000000;
    esc_register->ESC_DLY_CTL2   = 0x00000000;
    esc_register->ESC_DLY_CTL3   = 0x00000003;
    esc_register->ESC_DLY_CTL4   = 0x00000100;
#ifdef W25Q32JW
    esc_register->ESC_QIO_CTRL   = 0xFFFFFFFF;    //for winbond
#else
    esc_register->ESC_QIO_CTRL   = 0xF0F0F0F0;
#endif
    esc_register->ESC_STA3       = 0x00000000;
    esc_register->ESC_DEBUG_1    = 0x00000000;
    esc_register->ESC_DEBUG_MUX  = 0x00000000;
    esc_register->ESC_MISC_CTL3  = 0xFFFFFFFF;
    esc_register->ESC_MAC_IRQ    = 0x00000000;
    hal_nvic_register_isr_handler(ESC_IRQn, esc_isr);
    hal_nvic_enable_irq(ESC_IRQn);
}


//it is only for first power on
//it is no use if it is not in spi mode
ATTR_TEXT_IN_RAM static int32_t esc_reset(void)
{
#ifdef MICRO_CHIP_23A1024
    ESC_Dev_Command(0, ESC_CMD_EXIT_QUAD_MODE);
#endif
#ifdef ASP_CHIP_1604M_SQ_PSRAM
    ESC_Dev_Command(0, ESC_CMD_RESET_ENABLE);
    ESC_Dev_Command(0, ESC_CMD_RESET);
#endif
    return 0;
}

//MICRO_CHIP_23A1024 only supports 32Byte warp or 1K wrap; it will switch 32 or 1k by cmd 0xC0
ATTR_RWDATA_IN_TCM static uint32_t esc_wrap_en = 0;


ATTR_TEXT_IN_TCM void esc_set_wrap32(uint32_t wrap_en)
{
    if (wrap_en) {
#ifdef ESC_ASP6404L_SQ_PSRAM
        if (esc_wrap_en == 0) {
            ESC_Dev_Command(0, ESC_CMD_BURST_LEN);
            esc_wrap_en = 1;
        }
        esc_register->ESC_MISC_CTL2 = 0x101;    //wrap enable & critical byte first
#endif
#ifdef ASP_CHIP_1604M_SQ_PSRAM
        if (esc_wrap_en == 0) {
            uint8_t mode_sr = 0;
            /*
                 MR[6:5]: 01     wrap 32
                          11     default 512
                 MR[1:0]         default 00   output driving strength
            */
            mode_sr = 0x10;
            write_mode_register(mode_sr);
            ESC_Dev_Command(0, ESC_CMD_BURST_LEN);
            esc_wrap_en = 1;
        }
        esc_register->ESC_MISC_CTL2 = 0x101;    //wrap enable & critical byte first
#endif
    } else {
#ifdef ASP_CHIP_1604M_SQ_PSRAM
        //set 0xC0 will reset wrap to default
        //ESC_Dev_Command(0, ESC_CMD_BURST_LEN);
        esc_wrap_en = 0;
        esc_register->ESC_MISC_CTL2 = 0x0;    //wrap enable & critical byte first
#endif
    }
}

//-----------------------------------------------------------------------------
/*!
  @brief
    GPRAM access: Write data to GPRAM

  @param[in] gpram_offset The offset of the GPRAM in terms of byte, it must be
             multiple of four.
  @param[in] buff The pointer to the data buffer
  @param[in] length The length of the data buffer.
  @remarks The given gpram offset must be 4 bytes aligned, and the function will
           copy the data buffer to the GPRAM WORD/HALF-WORD/BYTE wise
           according to the starting address of the buffer.
*/
ATTR_TEXT_IN_TCM void ESC_GPRAM_Write(uint32_t gpram_offset, void *buff, int32_t length)
{
    uint32_t i;
    uint8_t *p_data = (uint8_t *)buff;
    esc_uint tmp;

    gpram_offset += ESC_GPRAM_DATA;

    // source address is 4-byte aligned, and gpram_offset is 4-bytes aligned, too
    if (0 == ((uint32_t)buff & 0x3)) {
        for (; length > 0; gpram_offset += 4, p_data += 4, length -= 4) {
            ESC_WriteReg32(gpram_offset, *(uint32_t *)p_data);
        }
    }
    // source address is 2-byte aligned
    else if (0 == ((uint32_t)p_data & 0x1)) {
        for (; length > 0; gpram_offset += 4, p_data += 4, length -= 4) {
            tmp.u16[0] = *(uint16_t *)p_data;
            tmp.u16[1] = *((uint16_t *)p_data + 1);
            ESC_WriteReg32(gpram_offset, tmp.u32); // save local_data to SFI GPRAM
        }
    }
    // source data is NOT 2-byte aligned
    else {
        for (; length > 0; gpram_offset += 4, length -= 4) {
            for (i = 0; i < 4 && i < length; i++) {
                tmp.u8[i] = *p_data++;
            }
            ESC_WriteReg32(gpram_offset, tmp.u32); // save local_data to SFI GPRAM
        }
    }
}

#if defined(ESC_SFLASH) || defined(ASP_CHIP_1604M_SQ_PSRAM)
ATTR_TEXT_IN_TCM void write_mode_register(uint8_t sr)
{
    uint8_t cmd[5];
    cmd[0] = ESC_CMD_MODE_WRITE;
#ifdef MICRO_CHIP_23A1024
    cmd[1] = sr;
    ESC_Dev_Command_Ext(0, cmd, sr, 2, 0);
#endif

#ifdef ASP_CHIP_1604M_SQ_PSRAM
    cmd[1] = 0;
    cmd[2] = 0;
    cmd[3] = 0;
    cmd[4] = 1;
    ESC_Dev_Command_Ext(0, &cmd[0], sr, 5, 0);
#endif

#ifdef ESC_SFLASH
#ifndef W25Q32JW
    flash_write_en();

    cmd[0] = 0x01;
    cmd[1] = 0;
    if (sr == 1) {
        cmd[2] = 0x02;  //QE = 1
    } else if (sr == 0) {
        cmd[2] = 0x0;   //QE = 0
    }

    ESC_Dev_Command_Ext(0, &cmd[0], NULL, 3, 0);
#endif
#endif
}
#endif

#ifdef ESC_SFLASH

ATTR_RWDATA_IN_TCM static bool esc_suspended = false;
ATTR_RWDATA_IN_TCM static bool esc_busy = false;

ATTR_TEXT_IN_TCM void esc_flash_return_ready(void)
{
    uint32_t irq_mask;
    uint8_t device_status = 0xFF;
    uint8_t suspend_cmd = ESC_CMD_SP_SUS;

    hal_nvic_save_and_set_interrupt_mask_special(&irq_mask);

    if ((esc_suspended == false) && (esc_busy == true)) {
        read_mode_register(&device_status);
        if (1 == (device_status & 0x1)) {
            /* device still busy */

            /* 1. Issue suspend command */
            ESC_Dev_Command_Ext(0, &suspend_cmd, NULL, 1, 0);

            /* 2. Wait for device ready */
            for (;;) {
                read_mode_register(&device_status);
                if (0 == (device_status & 0x1)) {
                    break;
                }
            }

            esc_suspended = true;
        } else {

            /* device is idle now */
            esc_busy = false;
        }
    }

    hal_nvic_restore_interrupt_mask_special(irq_mask);
}


ATTR_TEXT_IN_TCM int32_t esc_check_ready_and_resume(void)
{
    uint8_t device_status[2], check_status = 0;
    uint32_t irq_mask;
    int32_t result = 0;
    uint8_t resume_cmd = ESC_CMD_SP_RSM;

    hal_nvic_save_and_set_interrupt_mask_special(&irq_mask);

    read_mode_register(&device_status[0]);

    if (0 == (device_status[0] & 0x1)) {
        /* current not busy */

        /* for winbond */
        /* check_status = 0x80; */
        check_status = 0x0;

        if (  //(check_status == (device_status[1] & check_status)) ||
            ((0 == check_status) && (esc_suspended == true))
        ) {
            /* only use software suspend flag */

            /* check device suspend flag or
             * driver recorded suspend flag for devices not has SUSPEND flag */

            /* issue resume command */
            ESC_Dev_Command_Ext(0, &resume_cmd, NULL, 1, 0);

            esc_suspended = false;
            result = -1;
        } else {
            /* device is neither busy nor suspeded */
            esc_suspended = false;
            esc_busy = false;
            result = 0;
        }
    } else {
        result = -1;    /* still busy */
    }

    hal_nvic_restore_interrupt_mask_special(irq_mask);
    return result;
}

//-----------------------------------------------------------------------------
/*!
  @brief
    GPRAM access: Write 1 byte command and 3 bytes address

  @param[in] cmd The command to be sent
  @param[in] address The address followed by the command.
*/
uint32_t ESC_GPRAM_Write_C1A3(const uint32_t cmd, const uint32_t address)
{
    return ((ESC_ReverseByteOrder(address) & ESC_GENERIC_0xFFFFFF00_MASK) | (cmd & ESC_GENERIC_0xFF_MASK));
}


ATTR_TEXT_IN_TCM static int32_t flash_write_en(void)
{
    ESC_Dev_Command(0, ESC_CMD_WRITE_EN);
    return 0;
}

ATTR_TEXT_IN_TCM int32_t hal_esc_read_flash_data(uint32_t address, uint8_t *buffer, uint32_t len)
{
    int32_t can_work;

    do {
        can_work = esc_check_ready_and_resume();
    } while (can_work != 0);

    memcpy(buffer, (void *)(address | 0x5000000), len);

    return 0;
}

ATTR_TEXT_IN_TCM int32_t hal_esc_write_flash_data(uint32_t address, uint8_t *data, uint32_t len)
{
    unsigned long page_offset, /*max_size,*/ page_size;
    int retlen = 0;
    int mode;
    unsigned char cmd[5];
    int32_t result = 0, can_work;
    uint32_t mask;

    /* if pointer data point to ESC flash region, read data first and then write to dest */
    uint8_t buffer[ESC_GPRAM_SIZE];

    uint32_t To  = (uint32_t)address;
    const uint8_t *Buf = data;
    const uint8_t *p_data_first;

    /* sanity checks */
    if ((len == 0) || (data == 0)) {
        return -1 ;
    }

    /* what page do we start with? */
    page_offset = address % ESC_GPRAM_SIZE;

    /* write everything in PAGESIZE chunks */
    while (len > 0) {
        page_size = min(len, (ESC_GPRAM_SIZE - page_offset));
        page_offset = 0;
        p_data_first = Buf;

retry:
        do {
            can_work = esc_check_ready_and_resume();
        } while (can_work != 0);

        if (0x05000000 == ((uint32_t)data & 0xFF000000)) {
            memcpy(buffer, Buf, page_size);    /* direct read */
        }

        hal_nvic_save_and_set_interrupt_mask_special(&mask);

        if (esc_suspended == true) {
            hal_nvic_restore_interrupt_mask_special(mask);
            printf("\r\n hal_esc_write_flash_data retry...\r\n");
            goto retry;
        }

        flash_write_en();

        /* write the next page to flash */
        cmd[0] = ESC_CMD_WRITE;
        cmd[1] = To >> 16;
        cmd[2] = To >> 8;
        cmd[3] = To;
        ESC_GPRAM_Write(0, cmd, 4);
        if (0x05000000 == ((uint32_t)data & 0xFF000000)) {
            ESC_GPRAM_Write(4, buffer, page_size);
        } else {
            ESC_GPRAM_Write(4, Buf, page_size);
        }
        esc_register->ESC_MAC_OUTL = page_size + 4;
        esc_register->ESC_MAC_INL = 0;

        esc_busy = true;
        ESC_MacEnable();
        ESC_MacWaitReady();
        len -= page_size;
        To += page_size;
        Buf += page_size;

        hal_nvic_restore_interrupt_mask_special(mask);

#if 0
        result = esc_wait_ready(4);
        hal_gpt_delay_ms(1);
        if (result != 0) {
            return result;
        }
#else
        for (;;) {
            result = esc_check_ready_and_resume();
            if (result == 0) {
                break;
            }
        }
#endif
    }

    return result;
}

ATTR_TEXT_IN_TCM int32_t hal_esc_erase_flash_block(uint32_t address, uint32_t type)
{
    uint8_t cmd[5];
    uint32_t mask;
    int32_t result, can_work = 0;

    cmd[1] = address >> 16;
    cmd[2] = address >> 8;
    cmd[3] = address;
    if (type == 0) {
        cmd[0] = ESC_CMD_ERASE_4K;
    } else if (type == 1) {
        cmd[0] = ESC_CMD_ERASE_32K;
    } else if (type == 2) {
        cmd[0] = ESC_CMD_ERASE_64K;
    } else if (type == 3) {
        cmd[0] = ESC_CMD_ERASE_CHIP;
    }

retry:
    do {
        can_work = esc_check_ready_and_resume();
    } while (can_work != 0);

    hal_cache_invalidate_all_cache_lines();

    hal_nvic_save_and_set_interrupt_mask_special(&mask);
    if (esc_suspended == true) {
        hal_nvic_restore_interrupt_mask_special(mask);
        printf("\r\n hal_esc_erase_flash_block retry...\r\n");
        goto retry;
    }

    esc_busy = true;

    flash_write_en();
    ESC_Dev_Command_Ext(0, cmd, NULL, 4, 0);

    hal_nvic_restore_interrupt_mask_special(mask);

#if 0
    result = esc_wait_ready(4);
    hal_gpt_delay_ms(100);
#else
    for (;;) {
        result = esc_check_ready_and_resume();
        if (result == 0) {
            break;
        }
    }
#endif
    return result;
}

#endif//ESC_FLASH

#endif//HAL_ESC_MODULE_ENABLED

