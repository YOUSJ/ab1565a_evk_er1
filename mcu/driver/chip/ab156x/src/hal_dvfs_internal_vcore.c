/* Copyright Statement:
 *
 * (C) 2020  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#include "hal_dvfs_internal.h"

#ifdef HAL_DVFS_MODULE_ENABLED
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "hal.h"
#include "hal_flash_sf.h"
#include "hal_gpt.h"
#include "hal_clock.h"
#include "hal_pmu.h"
#include "hal_dvfs_internal.h"
#include "hal_clock_internal.h"

#ifdef  HAL_DVFS_DEBUG_ENABLE
#define dvfs_debug(_message,...) log_hal_info("[DVFS] "_message, ##__VA_ARGS__)
#else
#define dvfs_debug(_message,...)
#endif

#define VCORE_DYNAMIC_SCALING(PTR_CUR,PTR_NEXT)     pmu_lock_vcore(PMU_NORMAL,PTR_NEXT->vcore,PMU_LOCK);\
                                                    pmu_lock_vcore(PMU_NORMAL,PTR_CUR->vcore,PMU_UNLOCK)



#define PMIC_DYNAMIC_SCALING(PTR_CUR,PTR_NEXT)      VCORE_DYNAMIC_SCALING(PTR_CUR,PTR_NEXT)

hal_dvfs_status_t dvfs_vcore_switch_frequency( unsigned int cur_opp, unsigned int next_opp);
void dvfs_vcore_switch_voltage(unsigned int c_voltage, unsigned int n_voltage);
typedef enum {
    DVFS_VCORE_MODE_LOW_SPEED = 26000,
    DVFS_VCORE_MODE_HELF_SPEED = 52000,
    DVFS_VCORE_MODE_FULL_SPEED = 104000,
    DVFS_VCORE_MODE_HIGH_SPEED = 208000
}dvfs_vcore_mode_t;
//------------[DVFS Variable Declaration]
static const uint32_t dvfs_vcore_voltage[HAL_DVFS_MAX_SPEED] =   { 800000,800000,800000,900000 };

static const uint32_t dvfs_vcore_frequency[HAL_DVFS_MAX_SPEED] = { DVFS_VCORE_MODE_LOW_SPEED,
                                                               DVFS_VCORE_MODE_HELF_SPEED,
                                                               DVFS_VCORE_MODE_FULL_SPEED,
                                                               DVFS_VCORE_MODE_HIGH_SPEED };


typedef struct {
    pmu_power_vcore_voltage_t vcore;
}dvfs_vsram_vcore_mapping_t;


ATTR_RWDATA_IN_TCM static dvfs_vsram_vcore_mapping_t pmu_mapping_tbl_[HAL_DVFS_MAX_SPEED] = {
    {.vcore = PMIC_VCORE_0P8_V},// 26Mhz
    {.vcore = PMIC_VCORE_0P8_V},// 52Mhz
    {.vcore = PMIC_VCORE_0P8_V},// 104Mhz
    {.vcore = PMIC_VCORE_0P9_V}// 208Mhz
};


ATTR_RWDATA_IN_TCM static dvfs_opp_t domain={
    //domain.name = __stringify("DVFS_VCORE");
    .opp_num = HAL_DVFS_MAX_SPEED, //max number about can choise voltage
    .module_num =1,// VCORE/DSP
    .cur_opp_index = DVFS_0P8V_VOLT_LV,
    .switch_voltage = dvfs_vcore_switch_voltage,//function about switch voltage
    .switch_frequency = dvfs_vcore_switch_frequency,// function about switch frequency
    .frequency = dvfs_vcore_frequency,//set all frequency
    .voltage = dvfs_vcore_voltage,  // set all voltage
#ifdef MTK_SYSTEM_CLOCK_104M
    .basic_opp_index = DVFS_0P8V_VOLT_LV,
#elif defined(MTK_SYSTEM_CLOCK_208M)
    .basic_opp_index = DVFS_0P9V_VOLT_LV,
#elif defined(MTK_SYSTEM_CLOCK_52M)
    .basic_opp_index = DVFS_0P73V_VOLT_LV,
#endif
};

//------------[DVFS basic setting api]
void dvfs_vcore_switch_voltage(unsigned int c_voltage, unsigned int n_voltage){
    dvfs_vsram_vcore_mapping_t *pmu_current = &pmu_mapping_tbl_[c_voltage];
    dvfs_vsram_vcore_mapping_t *pmu_next = &pmu_mapping_tbl_[n_voltage];
#ifdef HAL_PMU_MODULE_ENABLED
    PMIC_DYNAMIC_SCALING(pmu_current,pmu_next);
#endif
    if(c_voltage < n_voltage)
        domain.cur_opp_index = n_voltage;
}

dvfs_opp_t *dvfs_domain_init(){
    uint8_t clk_sys_clk_sel = clock_mux_cur_sel(CLK_DSP_SEL);
    switch(clk_sys_clk_sel){
    case DVFS_DSP_CLOCK_83M:
        pmu_lock_vcore(PMU_NORMAL,PMIC_VCORE_0P8_V,PMU_LOCK);
        domain.cur_opp_index = HAL_DVFS_HALF_SPEED_52M_W_LDSP;
        break;
    case DVFS_DSP_CLOCK_104M:
        pmu_lock_vcore(PMU_NORMAL,PMIC_VCORE_0P8_V,PMU_LOCK);
        domain.cur_opp_index = HAL_DVFS_HALF_SPEED_52M;
        break;
    case DVFS_DSP_CLOCK_208M:
        pmu_lock_vcore(PMU_NORMAL,PMIC_VCORE_0P8_V,PMU_LOCK);
        domain.cur_opp_index = HAL_DVFS_FULL_SPEED_104M;
        break;
    case DVFS_DSP_CLOCK_416M:
        pmu_lock_vcore(PMU_NORMAL,PMIC_VCORE_0P9_V,PMU_LOCK);
        domain.cur_opp_index = HAL_DVFS_HIGH_SPEED_208M;
        break;
    default :
        return NULL;
    }
    return &domain;
}
#define OSC_DIV_SW_STATUS (clock_domain.lposc_domain.field.osc)->field
ATTR_TEXT_IN_TCM hal_dvfs_status_t dvfs_vcore_switch_frequency( unsigned int cur_opp, unsigned int next_opp)
{
    if(cur_opp == next_opp)
        return HAL_DVFS_STATUS_OK;

    uint32_t irq_mask=0;
    hal_dvfs_status_t result = HAL_DVFS_STATUS_OK;
    hal_nvic_save_and_set_interrupt_mask(&irq_mask);
    hal_cache_flush_all_cache_lines();
    SFI_MaskAhbChannel(1);

    switch (next_opp) {
        case HAL_DVFS_HIGH_SPEED_208M:
        case HAL_DVFS_FULL_SPEED_104M:
        case HAL_DVFS_HALF_SPEED_52M:
            CKSYS_CLK_DIV_REG.CLK_OSC1_D2_EN = 1;
			if(cur_opp > next_opp)
                domain.cur_opp_index = next_opp;
            dvfs_switch_clock_freq(next_opp);
            break;
        case HAL_DVFS_HALF_SPEED_52M_W_LDSP:
            CKSYS_CLK_DIV_REG.CLK_OSC1_D5_EN = 1;
            CKSYS_CLK_DIV_REG.CLK_OSC1_D2_EN = 1;
            if(cur_opp > next_opp)
                domain.cur_opp_index = next_opp;
            dvfs_switch_clock_freq(next_opp);
            break;
        default:
            assert(0);
    }
    SFI_MaskAhbChannel(0);
    SystemCoreClockUpdate();
    hal_nvic_restore_interrupt_mask(irq_mask);
    return result;
}

#endif /* HAL_DVFS_MODULE_ENABLED */

