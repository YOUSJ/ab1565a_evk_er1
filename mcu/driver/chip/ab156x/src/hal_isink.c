/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
#include "hal_platform.h"
#ifdef HAL_ISINK_MODULE_ENABLED

#include "hal_isink.h"
#include "hal.h"
#include "hal_isink_internal.h"
#include "hal_pmu.h"
#include "hal_pmu_wrap_interface.h"
#if defined(AB1565)
#include "hal_pmu_internal_2565.h"
#else
#include "hal_pmu_internal_2568.h"
#endif
/* Weak symbol declaration */
#if _MSC_VER >= 1500
#pragma comment(linker, "/alternatename:_pmu_set_register_value=_pmu_set_register_value_default")
#elif defined(__GNUC__) || defined(__ICCARM__) || defined(__CC_ARM)
#pragma weak pmu_set_register_value = pmu_set_register_value_default
#else
#error "Unsupported Platform"
#endif
#define ISINK_DEBUG_ENABLE

#ifdef ISINK_DEBUG_ENABLE
    #define log_isink_info(_message, cnt, ...)    printf(_message, ##__VA_ARGS__)
    #define log_isink_warn(_message, cnt, ...)    printf(_message, ##__VA_ARGS__)
    #define log_isink_error(_message, cnt, ...)   printf(_message, ##__VA_ARGS__)
#endif

void hal_isink_dump_register();

//-------------------------------------------define section start
#if defined(AB1565)

#define LED_MASK_16bit_2565             0xFFFF
#define LED_MASK_10bit_2565             0x3FF
#define LED_MASK_8bit_2565              0xFF
#define LED_MASK_1bit_2565              0x1

#define LED_EN_CTR_REG_2565             0x0500
#define LED_EN_CTR_SYS_SHIFT_2565      (8)
#define LED_EN_CTR_EN_MASK_2565        (0x3)
#define LED_EN_CTR_EN_SHIFT_2565       (0)
/*******************************/
#define LED0_CON0_REG_2565             0x0502
#define LED0_UNIT_REG_2565             0x0504
#define LED0_PERIOD0_REG_2565          0x0506
#define LED0_PERIOD1_REG_2565          0x0508
#define LED0_PWM0_REG_2565             0x050A
#define LED0_PWM1_REG_2565             0x050C
#define LED0_PWM2_REG_2565             0x050E
/*******************************/
#define LED1_CON0_REG_2565             0x0510
#define LED1_UNIT_REG_2565             0x0512
#define LED1_PERIOD0_REG_2565          0x0514
#define LED1_PERIOD1_REG_2565          0x0516
#define LED1_PWM0_REG_2565             0x0518
#define LED1_PWM1_REG_2565             0x050A
#define LED1_PWM2_REG_2565             0x050C

#define LED0_PWM0_MASK_2565            (0x1)
#define LED0_PWM0_EN_SHIFT_2565        (0)
#define LED1_PWM0_MASK_2565            (0x1)
#define LED1_PWM0_EN_SHIFT_2565        (0)

#define AB1565_ISINK_CH0_BIAS_EN_SHIFT    0
#define AB1565_ISINK_CH1_BIAS_EN_SHIFT    1

#define Isink_Unity         32
#define Isink_MaxMsTime   2048
#define ISINK_DEFAULT_UNIT  255         /* 1/32K = 30.517ns, set to 255 for aligning = 7.781ms */
#define ISINK_DEFAULT_MS    8
#define ISINK_DEFAULT_XM    3
#define ISINK_DEFAULT_Time  0

uint8_t isink_breath_mode_adjust_on[16] = {2,5,8,11,14,17,20,23,26,29,32,35,38,41,44,48};
uint8_t isink_breath_mode_adjust_off[16] = {4,10,16,22,28,34,40,47,52,58,65,71,76,83,89,96};

#endif



//-------------------------------------------define section end
static hal_isink_status_t  isink_config_pwm_mode(hal_isink_channel_t channel, uint32_t cycle_ms, uint32_t duty_persent)
{
#if defined(AB1565)
    log_isink_info("isink isink_config_pwm_mode start ch[%d]\r\n",1, channel);

    return HAL_ISINK_STATUS_OK;
#else
    uint32_t    freq_val = 0;
    uint32_t    duty_val = 0;
    bool        status = true;

    freq_val = cycle_ms;
    duty_val = (duty_persent*32)/100;

    if(channel == HAL_ISINK_CHANNEL_0){
        status &= pmu_set_register_value(PMU_ISINK0_CON1, PMU_ISINK_DIM0_DUTY_MASK, PMU_ISINK_DIM0_DUTY_SHIFT, duty_val);
        status &= pmu_set_register_value(PMU_ISINK0_CON0, PMU_ISINK_DIM0_FSEL_MASK, PMU_ISINK_DIM0_FSEL_SHIFT, (freq_val-1));
    }
    else if(channel == HAL_ISINK_CHANNEL_1){
        status &= pmu_set_register_value(PMU_ISINK1_CON1, PMU_ISINK_DIM1_DUTY_MASK, PMU_ISINK_DIM1_DUTY_SHIFT, duty_val);
        status &= pmu_set_register_value(PMU_ISINK1_CON0, PMU_ISINK_DIM1_FSEL_MASK, PMU_ISINK_DIM1_FSEL_SHIFT, (freq_val-1));
    }
    if (true == status) {
        return HAL_ISINK_STATUS_OK;
    } else {
        return HAL_ISINK_STATUS_ERROR;
    }
#endif
}

static  hal_isink_status_t  isink_set_pwm_blink_cycle(hal_isink_channel_t channel, uint8_t cycle_on, uint8_t	cycle_off)
{
#if defined(AB1565)
        log_isink_info("isink isink_set_pwm_blink_cycle start ch[%d]\r\n", 1, channel);
    
        return HAL_ISINK_STATUS_OK;
#else

    uint32_t    address;
    bool     status = 0;

    if(channel == HAL_ISINK_CHANNEL_0){
        status &= pmu_set_register_value(PMU_ISINK_MODE_CTRL, PMU_RG_ISINK0_REPEAT_EN_MASK, PMU_RG_ISINK0_REPEAT_EN_SHIFT,1);
        address = PMU_ISINK0_REPT_CYCLE;
    }else {
        status &= pmu_set_register_value(PMU_ISINK_MODE_CTRL, PMU_RG_ISINK1_REPEAT_EN_MASK, PMU_RG_ISINK1_REPEAT_EN_SHIFT,1);
        address = PMU_ISINK1_REPT_CYCLE;
    }
    status &= pmu_set_register_value(address, PMU_RG_ISINK0_ON_CYCLE_MASK,  PMU_RG_ISINK0_ON_CYCLE_SHIFT,  cycle_on);
    status &= pmu_set_register_value(address, PMU_RG_ISINK0_OFF_CYCLE_MASK, PMU_RG_ISINK0_OFF_CYCLE_SHIFT, cycle_off);
    if (true == status) {
        return HAL_ISINK_STATUS_OK;
    } else {
        return HAL_ISINK_STATUS_ERROR;
    }
#endif
}

static  hal_isink_status_t  isink_config_current(hal_isink_channel_t channel, hal_isink_current_t current, bool double_en)
{
#if defined(AB1565)
    log_isink_info("isink isink_config_current start ch[%d]\r\n", 1, channel);
        
    return HAL_ISINK_STATUS_OK;
#else

    bool     status = true;

    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value(PMU_ISINK0_CON1,     PMU_ISINK_CH0_STEP_MASK,   PMU_ISINK_CH0_STEP_SHIFT, (uint16_t)current);
            status &= pmu_set_register_value(PMU_DRIVER_ANA_CON0, PMU_RG_ISINK0_DOUBLE_MASK, PMU_RG_ISINK0_DOUBLE_SHIFT, (double_en & 0x1));
            break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value(PMU_ISINK1_CON1,     PMU_ISINK_CH1_STEP_MASK,   PMU_ISINK_CH1_STEP_SHIFT,   (uint16_t)current);
            status &= pmu_set_register_value(PMU_DRIVER_ANA_CON0, PMU_RG_ISINK1_DOUBLE_MASK, PMU_RG_ISINK1_DOUBLE_SHIFT, (double_en & 0x1));
            break;
        default:
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }
    if (true == status) {
        return HAL_ISINK_STATUS_OK;
    }
    else {
        return HAL_ISINK_STATUS_ERROR;
    }
#endif
}


bool pmu_set_register_value_default(unsigned short int address, unsigned short int mask, unsigned short int shift, unsigned short int value) {
    return true;
}

hal_isink_status_t  hal_isink_init(hal_isink_channel_t  channel)
{
#if defined(AB1565)
    log_isink_info("isink hal_isink_init start ch[%d]\r\n", 1, channel);

    bool    status = true;

    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value_2565(LED0_CON0_REG_2565, LED_MASK_10bit_2565, 0, 0);
            status &= pmu_set_register_value_2565(LED0_UNIT_REG_2565, LED_MASK_10bit_2565, 0, 0);
            status &= pmu_set_register_value_2565(LED0_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0, 0);
            status &= pmu_set_register_value_2565(LED0_PERIOD1_REG_2565, LED_MASK_16bit_2565, 0, 0);
            //status &= pmu_set_register_value_2565(LED0_PWM0_REG_2565, LED_MASK_1bit_2565, 0, 0);
            //status &= pmu_set_register_value_2565(LED0_PWM1_REG_2565, LED_MASK_16bit_2565, 0, 0);
            //status &= pmu_set_register_value_2565(LED0_PWM2_REG_2565, LED_MASK_16bit_2565, 0, 0);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value_2565(LED1_CON0_REG_2565, LED_MASK_10bit_2565, 0, 0);
            status &= pmu_set_register_value_2565(LED1_UNIT_REG_2565, LED_MASK_10bit_2565, 0, 0);
            status &= pmu_set_register_value_2565(LED1_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0, 0);
            status &= pmu_set_register_value_2565(LED1_PERIOD1_REG_2565, LED_MASK_16bit_2565, 0, 0);
            //status &= pmu_set_register_value_2565(LED1_PWM0_REG_2565, LED_MASK_1bit_2565, 0, 0);
            //status &= pmu_set_register_value_2565(LED1_PWM1_REG_2565, LED_MASK_16bit_2565, 0, 0);
            //status &= pmu_set_register_value_2565(LED1_PWM2_REG_2565, LED_MASK_16bit_2565, 0, 0);
            break;
        default:
        return HAL_ISINK_STATUS_ERROR_CHANNEL;
    } 

    if (status != true) {
        return HAL_ISINK_STATUS_ERROR;
    }
    else {
        return HAL_ISINK_STATUS_OK;
    }       
#else
    bool    status = true;

    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value(PMU_ISINK_MODE_CTRL, PMU_ISINK_CH0_MODE_MASK,PMU_ISINK_CH0_MODE_SHIFT, HAL_ISINK_MODE_REGISTER);
            status &= pmu_set_register_value(PMU_CKCFG2, PMU_RG_DRV_ISINK0_CK_PDN_MASK,   PMU_RG_DRV_ISINK0_CK_PDN_SHIFT,0);
            status &= pmu_set_register_value(PMU_CKCFG3,PMU_RG_BUCK_F2M_CK_PDN_MASK,PMU_RG_BUCK_F2M_CK_PDN_SHIFT,0);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value(PMU_ISINK_MODE_CTRL, PMU_ISINK_CH1_MODE_MASK,PMU_ISINK_CH1_MODE_SHIFT, HAL_ISINK_MODE_REGISTER);
            status &= pmu_set_register_value(PMU_CKCFG2, PMU_RG_DRV_ISINK1_CK_PDN_MASK,   PMU_RG_DRV_ISINK1_CK_PDN_SHIFT, 0);
            status &= pmu_set_register_value(PMU_CKCFG3,PMU_RG_BUCK_F2M_CK_PDN_MASK,PMU_RG_BUCK_F2M_CK_PDN_SHIFT,0);
            break;
        default:
        return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }

    if (status != true) {
        return HAL_ISINK_STATUS_ERROR;
    }
    else {
        return HAL_ISINK_STATUS_OK;
    }
#endif
}


hal_isink_status_t hal_isink_deinit(hal_isink_channel_t channel)
{
#if defined(AB1565)

    log_isink_info("isink hal_isink_deinit start ch[%d]\r\n", 1, channel);
    bool    status = true;

    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value_2565(LED0_CON0_REG_2565, LED_MASK_10bit_2565, 0, 0);
            status &= pmu_set_register_value_2565(LED0_UNIT_REG_2565, LED_MASK_10bit_2565, 0, 0);
            status &= pmu_set_register_value_2565(LED0_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0, 0);
            status &= pmu_set_register_value_2565(LED0_PERIOD1_REG_2565, LED_MASK_16bit_2565, 0, 0);
            //status &= pmu_set_register_value_2565(LED0_PWM0_REG_2565, LED_MASK_1bit_2565, 0, 0);
            //status &= pmu_set_register_value_2565(LED0_PWM1_REG_2565, LED_MASK_16bit_2565, 0, 0);
            //status &= pmu_set_register_value_2565(LED0_PWM2_REG_2565, LED_MASK_16bit_2565, 0, 0);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value_2565(LED1_CON0_REG_2565, LED_MASK_10bit_2565, 0, 0);
            status &= pmu_set_register_value_2565(LED1_UNIT_REG_2565, LED_MASK_10bit_2565, 0, 0);
            status &= pmu_set_register_value_2565(LED1_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0, 0);
            status &= pmu_set_register_value_2565(LED1_PERIOD1_REG_2565, LED_MASK_16bit_2565, 0, 0);
            //status &= pmu_set_register_value_2565(LED1_PWM0_REG_2565, LED_MASK_1bit_2565, 0, 0);
            //status &= pmu_set_register_value_2565(LED1_PWM1_REG_2565, LED_MASK_16bit_2565, 0, 0);
            //status &= pmu_set_register_value_2565(LED1_PWM2_REG_2565, LED_MASK_16bit_2565, 0, 0);
            break;
        default:
        return HAL_ISINK_STATUS_ERROR_CHANNEL;
    } 

    if (status != true) {
        return HAL_ISINK_STATUS_ERROR;
    }
    else {
        return HAL_ISINK_STATUS_OK;
    } 

#else
    bool    status = true;

    /*init default mode setting, and power down*/
    switch(channel){
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL, PMU_ISINK_CH0_EN_MASK, PMU_ISINK_CH0_EN_SHIFT, 0);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL, PMU_ISINK_CH0_BIAS_EN_MASK, PMU_ISINK_CH0_BIAS_EN_SHIFT,0);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL, PMU_ISINK_CHOP0_EN_MASK,    PMU_ISINK_CHOP0_EN_SHIFT, 0);
            status &= pmu_set_register_value(PMU_DRIVER_ANA_CON0,PMU_RG_ISINK0_DOUBLE_MASK, PMU_RG_ISINK0_DOUBLE_SHIFT, 0);
            status &= pmu_set_register_value(PMU_ISINK_MODE_CTRL,PMU_ISINK_CH0_MODE_MASK,   PMU_ISINK_CH0_MODE_SHIFT, HAL_ISINK_MODE_PWM);
            /*power down isink0*/
            status &= pmu_set_register_value(PMU_CKCFG2, PMU_RG_DRV_ISINK0_CK_PDN_MASK, PMU_RG_DRV_ISINK0_CK_PDN_SHIFT, 1);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL, PMU_ISINK_CH1_EN_MASK,     PMU_ISINK_CH1_EN_SHIFT, 0);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL, PMU_ISINK_CH1_BIAS_EN_MASK,PMU_ISINK_CH1_BIAS_EN_SHIFT, 0);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL, PMU_ISINK_CHOP1_EN_MASK,   PMU_ISINK_CHOP1_EN_SHIFT, 0);
            status &= pmu_set_register_value(PMU_DRIVER_ANA_CON0,PMU_RG_ISINK1_DOUBLE_MASK,PMU_RG_ISINK1_DOUBLE_SHIFT, 0);
            status &= pmu_set_register_value(PMU_ISINK_MODE_CTRL,PMU_ISINK_CH1_MODE_MASK,  PMU_ISINK_CH1_MODE_SHIFT, HAL_ISINK_MODE_PWM);
            /*power down isink1*/
            status &= pmu_set_register_value(PMU_CKCFG2, PMU_RG_DRV_ISINK1_CK_PDN_MASK, PMU_RG_DRV_ISINK1_CK_PDN_SHIFT, 1);
            break;
    }
    if (status == false) {
        return HAL_ISINK_STATUS_ERROR;
    }
    else {
        return HAL_ISINK_STATUS_OK;
    }
#endif
}

hal_isink_status_t hal_isink_set_clock_source(hal_isink_channel_t channel, hal_isink_clock_source_t source_clock)
{
    return HAL_ISINK_STATUS_OK;
}


hal_isink_status_t hal_isink_set_mode(hal_isink_channel_t channel, hal_isink_mode_t mode)
{
#if defined(AB1565) 
    log_isink_info("isink hal_isink_set_mode start ch[%d]\r\n",1, channel);
    
    //don't need to implement isink mode

    return HAL_ISINK_STATUS_OK;    
#else
    bool    status = true;

    if(mode > HAL_ISINK_MODE_REGISTER){
        return HAL_ISINK_STATUS_ERROR_INVALID_PARAMETER;
    }

    hal_gpt_delay_us(5);
    switch(channel){
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value(PMU_ISINK_MODE_CTRL, PMU_ISINK_CH0_MODE_MASK, PMU_ISINK_CH0_MODE_SHIFT, (uint16_t)mode);
        break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value(PMU_ISINK_MODE_CTRL, PMU_ISINK_CH1_MODE_MASK, PMU_ISINK_CH1_MODE_SHIFT, (uint16_t)mode);
        break;
        default:
            return HAL_ISINK_STATUS_ERROR_INVALID_PARAMETER;
    }
    if(status == false){
        return HAL_ISINK_STATUS_ERROR;
    }
    else{
        return HAL_ISINK_STATUS_OK;
    }
#endif
}

hal_isink_status_t hal_isink_set_step_current(hal_isink_channel_t channel, hal_isink_current_t current)
{
#if defined(AB1565)
    log_isink_info("isink hal_isink_set_step_current start ch\r\n", 0);

    return HAL_ISINK_STATUS_OK; 
#else
    bool status = true;

    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value(PMU_ISINK0_CON1, PMU_ISINK_CH0_STEP_MASK, PMU_ISINK_CH0_STEP_SHIFT, (uint16_t)current);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL,    PMU_ISINK_CH0_BIAS_EN_MASK,PMU_ISINK_CH0_BIAS_EN_SHIFT,1);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL,    PMU_ISINK_CHOP0_EN_MASK,   PMU_ISINK_CHOP0_EN_SHIFT,   1);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL,    PMU_ISINK_CH0_EN_MASK,     PMU_ISINK_CH0_EN_SHIFT,     1);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value(PMU_ISINK1_CON1, PMU_ISINK_CH1_STEP_MASK, PMU_ISINK_CH1_STEP_SHIFT, (uint16_t)current);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL, PMU_ISINK_CH1_BIAS_EN_MASK,PMU_ISINK_CH1_BIAS_EN_SHIFT,  1);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL, PMU_ISINK_CHOP1_EN_MASK, PMU_ISINK_CHOP1_EN_SHIFT, 1);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL, PMU_ISINK_CH1_EN_MASK,   PMU_ISINK_CH1_EN_SHIFT,    1);

            break;
        default:
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }

    if (true == status) {
        return HAL_ISINK_STATUS_OK;
    }
    else {
        return HAL_ISINK_STATUS_ERROR;
    }
#endif
}

hal_isink_status_t hal_isink_set_double_current(hal_isink_channel_t channel, hal_isink_current_t current)
{
#if defined(AB1565)
    if(current > 3){
        log_isink_info("isink hal_isink_set_double_current error ch[%d]curr[%d]\r\n", 2, channel, current);
        return HAL_ISINK_STATUS_ERROR;
    }else{
        log_isink_info("isink hal_isink_set_double_current normal ch[%d]curr[%d]\r\n", 2, channel, current);
    }
    
    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            pmu_set_register_value_2565(LED0_CON0_REG_2565, LED0_RES_SEL_MASK, LED0_RES_SEL_SHIFT, current);
            break;
        case HAL_ISINK_CHANNEL_1:
            pmu_set_register_value_2565(LED1_CON0_REG_2565, LED1_RES_SEL_MASK, LED1_RES_SEL_SHIFT, current);
            break;
        default:
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }
    
    return HAL_ISINK_STATUS_OK; 
#else

    bool status = false;
    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value(PMU_ISINK0_CON1,     PMU_ISINK_CH0_STEP_MASK,   PMU_ISINK_CH0_STEP_SHIFT,  (uint16_t)current);
            status &= pmu_set_register_value(PMU_DRIVER_ANA_CON0, PMU_RG_ISINK0_DOUBLE_MASK, PMU_RG_ISINK0_DOUBLE_SHIFT, 1);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL,   PMU_ISINK_CH0_BIAS_EN_MASK,PMU_ISINK_CH0_BIAS_EN_SHIFT,1);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL,   PMU_ISINK_CHOP0_EN_MASK,   PMU_ISINK_CHOP0_EN_SHIFT,   1);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL,   PMU_ISINK_CH0_EN_MASK,     PMU_ISINK_CH0_EN_SHIFT,     1);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value(PMU_ISINK1_CON1,     PMU_ISINK_CH1_STEP_MASK,   PMU_ISINK_CH1_STEP_SHIFT,   (uint16_t)current);
            status &= pmu_set_register_value(PMU_DRIVER_ANA_CON0, PMU_RG_ISINK1_DOUBLE_MASK, PMU_RG_ISINK1_DOUBLE_SHIFT, 1);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL,   PMU_ISINK_CH1_BIAS_EN_MASK,PMU_ISINK_CH1_BIAS_EN_SHIFT,1);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL,   PMU_ISINK_CHOP1_EN_MASK,   PMU_ISINK_CHOP1_EN_SHIFT,   1);
            status &= pmu_set_register_value(PMU_ISINK_EN_CTRL,   PMU_ISINK_CH1_EN_MASK,     PMU_ISINK_CH1_EN_SHIFT,     1);
            break;
        default:
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }

    if (true == status) {
        return HAL_ISINK_STATUS_OK;
    }
    else {
        return HAL_ISINK_STATUS_ERROR;
    }
#endif
}

hal_isink_status_t hal_isink_enable_breath_mode(hal_isink_channel_t channel, hal_isink_breath_mode_t breath_mode)
{
#if defined(AB1565)
    uint16_t time_t0 = ISINK_DEFAULT_Time;   //Set to default: 0
    uint16_t time_t1 = 0;   
    uint16_t time_t2 = 0;
    uint16_t time_t3 = ISINK_DEFAULT_Time;   //Set to default: 0
    uint16_t uint_uint = ISINK_DEFAULT_UNIT; 
    uint16_t uint_xn = ISINK_DEFAULT_XM;

    log_isink_info("isink hal_isink_enable_breath_mode start ch[%d]\r\n", 1, channel);
    log_isink_info("isink hal_isink_enable_breath_mode start 2 dark2lig1[%d]dark2lig2[%d]lig2dak1[%d]lig2dak2[%d]ligh[%d]dark[%d]", 6,
           breath_mode.darker_to_lighter_time1, breath_mode.darker_to_lighter_time2, breath_mode.lighter_to_darker_time1, breath_mode.lighter_to_darker_time2, \
           breath_mode.lightest_time, breath_mode.darkest_time);

    uint16_t light, dark;
    light = breath_mode.lightest_time;
    dark  = breath_mode.darkest_time;

    uint16_t isink_led_uint = 0;
    uint16_t isink_led_period0 = 0;
    uint16_t isink_led_period1 = 0;
    uint16_t isink_led_con = 0;
    
    if((light > Isink_MaxMsTime) || (dark > Isink_MaxMsTime))
    {
        log_isink_info("isink error exceed maximum time\r\n", 0);
    }else{
        time_t1 = isink_breath_mode_adjust_on[(uint8_t)breath_mode.lightest_time];
        time_t2 = isink_breath_mode_adjust_off[(uint8_t)breath_mode.darkest_time];

        isink_led_uint = (uint_xn << LED0_XN_SHIFT ) | (uint_uint); 
        isink_led_period0 = (time_t0 << LED0_T0_SHIFT) | (time_t1);
        isink_led_period1 = (time_t2 << LED0_T2_SHIFT) | (time_t3);
        isink_led_con = (LED0_REPEAT_MASK<<LED0_REPEAT_SHIFT);
    
        log_isink_info("isink uint[%x]per0[%x]per1[%x]\r\n",3, isink_led_uint, isink_led_period0, isink_led_period1);

        switch (channel) {
            case HAL_ISINK_CHANNEL_0:
                pmu_set_register_value_2565(LED0_UNIT_REG_2565, LED_MASK_16bit_2565, 0, isink_led_uint);
                    //log_isink_info("isink hal_isink_start LED0_UNIT[%d]\r\n",1, pmu_get_register_value_2565(LED0_UNIT_REG_2565, LED_MASK_16bit_2565, 0));
                pmu_set_register_value_2565(LED0_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0, isink_led_period0);
                    //log_isink_info("isink hal_isink_start LED0_PERIOD0_REG_2565[%d]\r\n",1, pmu_get_register_value_2565(LED0_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0));
                pmu_set_register_value_2565(LED0_PERIOD1_REG_2565, LED_MASK_16bit_2565, 0, isink_led_period1);
                    //log_isink_info("isink hal_isink_start LED0_PERIOD1_REG_2565[%d]\r\n",1, pmu_get_register_value_2565(LED0_PERIOD1_REG_2565, LED_MASK_16bit_2565, 0));
                pmu_set_register_value_2565(LED0_CON0_REG_2565, LED_MASK_16bit_2565, 0, isink_led_con);
                    //log_isink_info("isink hal_isink_start LED0_CON0_REG_2565[%d]\r\n",1, pmu_get_register_value_2565(LED0_CON0_REG_2565, LED_MASK_16bit_2565, 0));
                break;
            case HAL_ISINK_CHANNEL_1:
                pmu_set_register_value_2565(LED1_UNIT_REG_2565, LED_MASK_16bit_2565, 0, isink_led_uint);
                    //log_isink_info("isink hal_isink_start LED1_UNIT_REG_2565[%d]\r\n",1, pmu_get_register_value_2565(LED1_UNIT_REG_2565, LED_MASK_16bit_2565, 0));
                pmu_set_register_value_2565(LED1_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0, isink_led_period0);
                    //log_isink_info("isink hal_isink_start LED1_PERIOD0_REG_2565[%d]\r\n",1, pmu_get_register_value_2565(LED1_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0));
                pmu_set_register_value_2565(LED1_PERIOD1_REG_2565, LED_MASK_16bit_2565, 0, isink_led_period1);
                    //log_isink_info("isink hal_isink_start LED1_PERIOD1_REG_2565[%d]\r\n",1, pmu_get_register_value_2565(LED1_PERIOD1_REG_2565, LED_MASK_16bit_2565, 0));
                pmu_set_register_value_2565(LED1_CON0_REG_2565, LED_MASK_16bit_2565, 0, isink_led_con);
                    //log_isink_info("isink hal_isink_start LED1_CON0_REG_2565[%d]\r\n",1, pmu_get_register_value_2565(LED1_CON0_REG_2565, LED_MASK_16bit_2565, 0));
                break;
            default :
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
        }
        
        log_isink_info("isink hal_isink_enable_breath_mode end\r\n", 0);
    }

    return HAL_ISINK_STATUS_OK;
#else
    bool status = true;

    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            status &=pmu_set_register_value(PMU_ISINK0_CON2, PMU_ISINK_BREATH0_TR1_SEL_MASK, PMU_ISINK_BREATH0_TR1_SEL_SHIFT, breath_mode.darker_to_lighter_time1);
            status &=pmu_set_register_value(PMU_ISINK0_CON2, PMU_ISINK_BREATH0_TR2_SEL_MASK, PMU_ISINK_BREATH0_TR2_SEL_SHIFT, breath_mode.darker_to_lighter_time2);
            status &=pmu_set_register_value(PMU_ISINK0_CON2, PMU_ISINK_BREATH0_TF1_SEL_MASK, PMU_ISINK_BREATH0_TF1_SEL_SHIFT, breath_mode.lighter_to_darker_time1);
            status &=pmu_set_register_value(PMU_ISINK0_CON2, PMU_ISINK_BREATH0_TF2_SEL_MASK, PMU_ISINK_BREATH0_TF2_SEL_SHIFT, breath_mode.lighter_to_darker_time2);
            status &=pmu_set_register_value(PMU_ISINK0_CON3, PMU_ISINK_BREATH0_TON_SEL_MASK, PMU_ISINK_BREATH0_TON_SEL_SHIFT, breath_mode.lightest_time);
            status &=pmu_set_register_value(PMU_ISINK0_CON3, PMU_ISINK_BREATH0_TOFF_SEL_MASK, PMU_ISINK_BREATH0_TOFF_SEL_SHIFT, breath_mode.darkest_time);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &=pmu_set_register_value(PMU_ISINK1_CON2, PMU_ISINK_BREATH1_TR1_SEL_MASK, PMU_ISINK_BREATH1_TR1_SEL_SHIFT, breath_mode.darker_to_lighter_time1);
            status &=pmu_set_register_value(PMU_ISINK1_CON2, PMU_ISINK_BREATH1_TR2_SEL_MASK, PMU_ISINK_BREATH1_TR2_SEL_SHIFT, breath_mode.darker_to_lighter_time2);
            status &=pmu_set_register_value(PMU_ISINK1_CON2, PMU_ISINK_BREATH1_TF1_SEL_MASK, PMU_ISINK_BREATH1_TF1_SEL_SHIFT, breath_mode.lighter_to_darker_time1);
            status &=pmu_set_register_value(PMU_ISINK1_CON2, PMU_ISINK_BREATH1_TF2_SEL_MASK, PMU_ISINK_BREATH1_TF2_SEL_SHIFT, breath_mode.lighter_to_darker_time2);
            status &=pmu_set_register_value(PMU_ISINK1_CON3, PMU_ISINK_BREATH1_TON_SEL_MASK, PMU_ISINK_BREATH1_TON_SEL_SHIFT, breath_mode.lightest_time);
            status &=pmu_set_register_value(PMU_ISINK1_CON3, PMU_ISINK_BREATH1_TOFF_SEL_MASK, PMU_ISINK_BREATH1_TOFF_SEL_SHIFT, breath_mode.darkest_time);
            break;
        default :
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }

    if (true == status) {
        return HAL_ISINK_STATUS_OK;
    } else {
        return HAL_ISINK_STATUS_ERROR;
    }
#endif
}

hal_isink_status_t hal_isink_enable_pwm_mode(hal_isink_channel_t channel, hal_isink_pwm_mode_t pwm_mode)
{
#if defined(AB1565)
    uint8_t t0,t1,t2,t3,uint;
    
    //align pwm mode. 
    isink_config_pwm_mode(channel, 0, 0);
    isink_set_pwm_blink_cycle(channel, 0, 0);

    if((pwm_mode.hi_level_time > 2000) || (pwm_mode.lo_level_time > 2000) || (pwm_mode.blink_nums > 7))
    {
        log_isink_info("isink hal_isink_enable_pwm_mode error ch[%d]hi[%d]low[%d]num[%d]\r\n", 4, 
                        channel, pwm_mode.hi_level_time, pwm_mode.lo_level_time, pwm_mode.blink_nums);
    }else{
        log_isink_info("isink hal_isink_enable_pwm_mode normal ch[%d]hi[%d]low[%d]num[%d]\r\n", 4, 
                        channel, pwm_mode.hi_level_time, pwm_mode.lo_level_time, pwm_mode.blink_nums);
    }

    uint = ISINK_DEFAULT_UNIT; //8ms
    t0 = ISINK_DEFAULT_Time;
    t1 = (pwm_mode.hi_level_time / ISINK_DEFAULT_MS);
    t2 = (pwm_mode.lo_level_time / ISINK_DEFAULT_MS); 
    t3 = (pwm_mode.idle_time / ISINK_DEFAULT_MS) ;
    
    if(channel == HAL_ISINK_CHANNEL_0){
        pmu_set_register_value_2565(LED0_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0, ((t0<<LED0_T0_SHIFT)|(t1)));
        pmu_set_register_value_2565(LED0_PERIOD1_REG_2565, LED_MASK_16bit_2565, 0, ((t2<<LED0_T2_SHIFT)|(t3)));
        pmu_set_register_value_2565(LED1_UNIT_REG_2565, LED0_UNIT_MASK, LED0_UNIT_SHIFT, uint);
    }else if (channel == HAL_ISINK_CHANNEL_1){
        pmu_set_register_value_2565(LED1_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0, ((t0<<LED0_T0_SHIFT)|(t1)));
        pmu_set_register_value_2565(LED1_PERIOD1_REG_2565, LED_MASK_16bit_2565, 0, ((t2<<LED0_T2_SHIFT)|(t3)));
        pmu_set_register_value_2565(LED1_UNIT_REG_2565, LED0_UNIT_MASK, LED0_UNIT_SHIFT, uint);
    }
    return HAL_ISINK_STATUS_OK;
#else
    uint32_t cycle_ms = 0;
    uint32_t duty     = 0;
    uint32_t cycle_on = 0,cycle_off = 0;
    hal_isink_pwm_mode_t *config = &pwm_mode;

    if(channel >= HAL_ISINK_MAX_CHANNEL) {
        return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }
    if(config == NULL || (config->hi_level_time == 0 && config->lo_level_time == 0)){
        return HAL_ISINK_STATUS_ERROR_INVALID_PARAMETER;
    }
    cycle_ms  = config->hi_level_time + config->lo_level_time;
    duty      = (config->hi_level_time *100)/cycle_ms;
    cycle_on  = config->blink_nums;
    cycle_off = config->idle_time/cycle_ms;
    if(config->idle_time != 0) {
        cycle_off = (cycle_off == 0)?1:cycle_off;
    }
    isink_config_pwm_mode(channel, cycle_ms, duty);
    isink_set_pwm_blink_cycle(channel, cycle_on, cycle_off);
    return HAL_ISINK_STATUS_OK;
#endif
}


hal_isink_status_t  hal_isink_configure(hal_isink_channel_t channel, hal_isink_config_t *cfg)
{
    hal_isink_status_t  status = HAL_ISINK_STATUS_ERROR;

    log_isink_info("isink hal_isink_configure start ch[%d]mode[%d]\r\n",2, channel, cfg->mode);

    hal_isink_set_mode(channel, cfg->mode);
    switch(cfg->mode){
        case HAL_ISINK_MODE_PWM: {
            status = hal_isink_enable_pwm_mode(channel,  cfg->config.pwm_mode);
        }break;
        case HAL_ISINK_MODE_BREATH: {
            status = hal_isink_enable_breath_mode(channel, cfg->config.breath_mode);
        }break;
        case HAL_ISINK_MODE_REGISTER: {
            hal_isink_register_mode_t *para = (hal_isink_register_mode_t*)(&cfg->config);
            status = isink_config_current(channel, para->current, para->enable_double);
        }break;
    }

    return  status;
}


hal_isink_status_t hal_isink_start(uint32_t channel)
{
	log_isink_info("isink hal_isink_start ch[%d]\r\n",1, (int)channel);
#if defined(AB1565)
    uint32_t mask = 0;
    uint16_t insink_status = 0;     

    switch(channel){
        case HAL_ISINK_CHANNEL_0: {
            mask |= (LED0_EN_MASK<<LED0_EN_SHIFT); 
        }break;
        case HAL_ISINK_CHANNEL_1: {
            mask |= (LED1_EN_MASK<<LED1_EN_SHIFT); 
        }break;
    }
    
    log_isink_info("isink hal_isink_start ch[%d]mask[%d]\r\n",1, (int)channel, (int)mask);

    insink_status = pmu_get_register_value_2565(LED_EN_CTR_REG_2565, LED_MASK_10bit_2565, 0);

    while((insink_status & 0xf) != ((insink_status>>LED_EN_CTR_SYS_SHIFT_2565) & 0xf))
    {
        insink_status = pmu_get_register_value_2565(LED_EN_CTR_REG_2565, LED_MASK_10bit_2565, 0);
    }
    //log_isink_info("isink hal_isink_start write[%x]\r\n",1, pmu_get_register_value_2565(LED_EN_CTR_REG_2565, LED_MASK_10bit_2565, 0));

    pmu_set_register_value_2565(LED_EN_CTR_REG_2565, LED_EN_CTR_EN_MASK_2565, 0, mask);

    //log_isink_info("isink hal_isink_start after[%x]\r\n",1, pmu_get_register_value_2565(LED_EN_CTR_REG_2565, LED_MASK_10bit_2565, 0));

    hal_isink_dump_register();
#else
    uint32_t mask = 0;

    switch(channel){
        case HAL_ISINK_CHANNEL_0: {
            mask |= (PMU_ISINK_CH0_BIAS_EN_MASK<<PMU_ISINK_CH0_BIAS_EN_SHIFT) |
                    (PMU_ISINK_CHOP0_EN_MASK<<PMU_ISINK_CHOP0_EN_SHIFT) |
                    (PMU_ISINK_CH0_EN_MASK<<PMU_ISINK_CH0_EN_SHIFT);
        }break;
        case HAL_ISINK_CHANNEL_1: {
            mask |= (PMU_ISINK_CH1_BIAS_EN_MASK<<PMU_ISINK_CH1_BIAS_EN_SHIFT) |
                    (PMU_ISINK_CHOP1_EN_MASK<<PMU_ISINK_CHOP1_EN_SHIFT) |
                    (PMU_ISINK_CH1_EN_MASK<<PMU_ISINK_CH1_EN_SHIFT);
        }break;
       default:   return HAL_ISINK_STATUS_ERROR;
    }
    pmu_set_register_value(PMU_ISINK_EN_CTRL, mask, 0, mask);
#endif
    return 0;
}

hal_isink_status_t hal_isink_stop(uint32_t channel)
{
#if defined(AB1565)

    log_isink_info("isink hal_isink_stop start ch[%d]\r\n",1, (int) channel);

    uint16_t mask = 0;
    uint16_t insink_status = 0;     

    switch(channel){
        case HAL_ISINK_CHANNEL_0: {
            mask |= (LED0_EN_MASK<<LED0_EN_SHIFT); 
        }break;
        case HAL_ISINK_CHANNEL_1: {
            mask |= (LED1_EN_MASK<<LED1_EN_SHIFT); 
        }break;
    }
    
    insink_status = pmu_get_register_value_2565(LED_EN_CTR_REG_2565, LED_MASK_10bit_2565, 0);
        //log_isink_info("isink hal_isink_stop 0 [0x%x]\r\n",1, insink_status);
    while((insink_status & 0xf) != ((insink_status>>LED_EN_CTR_SYS_SHIFT_2565) & 0xf))
    {
        insink_status = pmu_get_register_value_2565(LED_EN_CTR_REG_2565, LED_MASK_10bit_2565, 0);
    }
        //log_isink_info("isink hal_isink_stop 0-1 [0x%x]\r\n",1, insink_status);

    insink_status &= ~(mask);
        //log_isink_info("isink hal_isink_stop 1 [0x%x]mask[%x]\r\n",1, insink_status, mask);
    pmu_set_register_value_2565(LED_EN_CTR_REG_2565, LED_EN_CTR_EN_MASK_2565, 0, insink_status);
        //log_isink_info("isink hal_isink_stop 1 [0x%x]\r\n",1, pmu_get_register_value_2565(LED_EN_CTR_REG_2565, LED_MASK_10bit_2565, 0));
#else
    uint32_t mask = 0;

    switch(channel){
        case HAL_ISINK_CHANNEL_0: {
            mask |= (PMU_ISINK_CH0_BIAS_EN_MASK<<PMU_ISINK_CH0_BIAS_EN_SHIFT) |
                    (PMU_ISINK_CHOP0_EN_MASK<<PMU_ISINK_CHOP0_EN_SHIFT) |
                    (PMU_ISINK_CH0_EN_MASK<<PMU_ISINK_CH0_EN_SHIFT);
        }break;
        case HAL_ISINK_CHANNEL_1: {
            mask |= (PMU_ISINK_CH1_BIAS_EN_MASK<<PMU_ISINK_CH1_BIAS_EN_SHIFT) |
                    (PMU_ISINK_CHOP1_EN_MASK<<PMU_ISINK_CHOP1_EN_SHIFT) |
                    (PMU_ISINK_CH1_EN_MASK<<PMU_ISINK_CH1_EN_SHIFT);
        }break;
        default: return HAL_ISINK_STATUS_ERROR;
    }
    pmu_set_register_value(PMU_ISINK_EN_CTRL, mask, 0, 0);
#endif
    return 0;
}

#ifdef ISINK_DEBUG_ENABLE
void hal_isink_dump_register()
{
#if defined(AB1565)
    log_isink_info("===========================================\r\n", 0);
    log_isink_info("ISINK 0x0500=[%x]\r\n", 1, (unsigned int) pmu_get_register_value_2565(LED_EN_CTR_REG_2565, LED_MASK_16bit_2565, 0));
    log_isink_info("ISINK 0x0502=[%x], 0x0504 = [%x] 0x0506 = [%x] 0x0508 = [%x]", 4,           \
                (unsigned int)pmu_get_register_value_2565(LED0_CON0_REG_2565, LED_MASK_16bit_2565, 0),        \
                (unsigned int)pmu_get_register_value_2565(LED0_UNIT_REG_2565, LED_MASK_16bit_2565, 0),        \
                (unsigned int)pmu_get_register_value_2565(LED0_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0),     \
                (unsigned int)pmu_get_register_value_2565(LED0_PERIOD1_REG_2565, LED_MASK_16bit_2565, 0));
    log_isink_info("ISINK 0x050A=[%x], 0x050C = [%x] 0x050E = [%x] ", 3,                        \
                (unsigned int)pmu_get_register_value_2565(LED0_PWM0_REG_2565, LED_MASK_16bit_2565, 0),        \
                (unsigned int)pmu_get_register_value_2565(LED0_UNIT_REG_2565, LED_MASK_16bit_2565, 0),        \
                (unsigned int)pmu_get_register_value_2565(LED0_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0));

    log_isink_info("ISINK 0x0510=[%x], 0x0512 = [%x] 0x0514 = [%x] 0x0516 = [%x]", 4,           \
                (unsigned int)pmu_get_register_value_2565(LED1_CON0_REG_2565, LED_MASK_16bit_2565, 0),        \
                (unsigned int)pmu_get_register_value_2565(LED1_UNIT_REG_2565, LED_MASK_16bit_2565, 0),        \
                (unsigned int)pmu_get_register_value_2565(LED1_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0),     \
                (unsigned int)pmu_get_register_value_2565(LED1_PERIOD1_REG_2565, LED_MASK_16bit_2565, 0));
    log_isink_info("ISINK 0x0518=[%x], 0x051A = [%x] 0x051C = [%x] ", 3,                        \
                (unsigned int)pmu_get_register_value_2565(LED1_PWM0_REG_2565, LED_MASK_16bit_2565, 0),        \
                (unsigned int)pmu_get_register_value_2565(LED1_UNIT_REG_2565, LED_MASK_16bit_2565, 0),        \
                (unsigned int)pmu_get_register_value_2565(LED1_PERIOD0_REG_2565, LED_MASK_16bit_2565, 0));
#else
    log_isink_info("===========================================\r\n", 0);
    log_isink_info("ISINK0_CON0 = %x, ISINK0_CON1= %x, ISINK0_CON2= %x, ISINK0_CON3= %x", 4, \
                   (unsigned int) pmu_get_register_value(PMU_ISINK0_CON0, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value(PMU_ISINK0_CON1, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value(PMU_ISINK0_CON2, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value(PMU_ISINK0_CON3, 0xFFFF, 0));
    log_isink_info("ISINK1_CON0 = %x, ISINK1_CON1= %x, ISINK1_CON2= %x, ISINK1_CON3= %x", 4, \
                   (unsigned int) pmu_get_register_value(PMU_ISINK1_CON0, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value(PMU_ISINK1_CON1, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value(PMU_ISINK1_CON2, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value(PMU_ISINK1_CON3, 0xFFFF, 0));
     log_isink_info("ANA_CON0 = %x, ISINK0_RPT= %x, ISINK1_RPT= %x, ISINK_EN= %x", 4, \
                   (unsigned int) pmu_get_register_value(PMU_DRIVER_ANA_CON0, 0xFFFF, 0),   \
                   (unsigned int) pmu_get_register_value(PMU_ISINK0_REPT_CYCLE, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value(PMU_ISINK1_REPT_CYCLE, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value(PMU_ISINK_EN_CTRL, 0xFFFF, 0));
     log_isink_info("ISINK_MODE = %x,CKCFG2 = %x, CKCFG3 = %x", 3,         \
                   (unsigned int) pmu_get_register_value(PMU_ISINK_MODE_CTRL, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value(PMU_CKCFG2, 0xFFFF, 0 ),         \
                   (unsigned int) pmu_get_register_value(PMU_CKCFG3, 0xFFFF, 0 ));
#endif
}
#endif

hal_isink_status_t hal_isink_get_running_status(hal_isink_channel_t channel, hal_isink_running_status_t *running_status)
{
#if defined(AB1565)
    log_isink_info("isink hal_isink_get_running_status start ch[%d]\r\n",1, channel);
    return HAL_ISINK_STATUS_OK;
#else
    uint32_t    power_st= 1;
    uint32_t    en_flg  = 0;
    uint32_t    mask    = 0;
    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            power_st = pmu_get_register_value(PMU_CKCFG2, PMU_RG_DRV_ISINK0_CK_PDN_MASK, PMU_RG_DRV_ISINK0_CK_PDN_SHIFT);
            en_flg   = pmu_get_register_value(PMU_ISINK_EN_CTRL, 0xFF, 0);
            mask     = 0x15;
            //value = pmu_get_register_value_2byte(PMU_ISINK1_CON1, PMU_ISINK_CH1_STEP_MASK, PMU_ISINK_CH1_STEP_SHIFT);
            //status = pmu_get_register_value_2byte(PMU_ISINK_EN_CTRL, 0xFF, 0);

            //printf("ISINK1( CON1= %d EN_CTRL= %d)\r\n",value, status);
            break;
        case HAL_ISINK_CHANNEL_1:
            power_st = pmu_get_register_value(PMU_CKCFG2, PMU_RG_DRV_ISINK1_CK_PDN_MASK, PMU_RG_DRV_ISINK1_CK_PDN_SHIFT);
            en_flg   = pmu_get_register_value(PMU_ISINK_EN_CTRL, 0xFF, 0);
            mask     = 0x2A;
            break;
        default:
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }

    if (power_st == 0 && en_flg == mask) {
        *running_status = HAL_ISINK_BUSY;
    } else {
        *running_status = HAL_ISINK_IDLE;
    }
    return HAL_ISINK_STATUS_OK;
#endif
}


#endif
