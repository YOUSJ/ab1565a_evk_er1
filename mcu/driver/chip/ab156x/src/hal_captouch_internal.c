/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
#include "hal_captouch.h"
#ifdef HAL_CAPTOUCH_MODULE_ENABLED
#include "hal_captouch_internal.h" 
#include "hal_nvic.h"
#include "hal_gpt.h"
#include "hal_clock.h"
#include "hal_rtc.h"
#include "hal_rtc_internal.h"
#include "hal_log.h"
#include "nvkey_id_list.h"
#include "nvkey.h"

#define captouch_rtc

log_create_module(Captouch, PRINT_LEVEL_INFO);
#define HAL_CAPTOUCH_LOG_PRINT(fmt,cnt,arg...)             LOG_MSGID_I(Captouch,fmt,cnt,##arg)
#define UNUSED(p) ((void)(p))

captouch_buffer_t captouch_buffer;
captouch_context_t captouch_context;
const uint8_t captouch_mapping_keydata[] = {KEYPAD_MAPPING};
CAPTOUCH_REGISTER_T *captouch = (CAPTOUCH_REGISTER_T *)(CAPTOUCH_BASE);
CAPTOUCH_REGISTERHIF_T *captouch_hif = ((CAPTOUCH_REGISTERHIF_T*)CAPTOUCH_HIFBASE);

TimerHandle_t captouch_fine_base_delay100ms_timer;
TimerHandle_t captouch_debounce_timer_ch0;
TimerHandle_t captouch_debounce_timer_ch1;
TimerHandle_t captouch_debounce_timer_ch2;
TimerHandle_t captouch_debounce_timer_ch3;

uint32_t event_time_stamp[4];
uint8_t noise_cnt = 0;

bool gdummyflag = true;

void captouch_push_one_event_to_buffer(uint32_t channel,hal_captouch_key_state_t state,uint32_t time_stamp)
{
    if (captouch_context.has_initilized != true) {
        return;
    }

    captouch_buffer.data[captouch_buffer.write_index].state         = state;
    captouch_buffer.data[captouch_buffer.write_index].key_data       = channel;
    captouch_buffer.data[captouch_buffer.write_index].time_stamp    = time_stamp;

    captouch_buffer.write_index++;
    captouch_buffer.write_index &= (CAPTOUCH_BUFFER_SIZE - 1);
}

void captouch_pop_one_event_from_buffer(hal_captouch_event_t *key_event)
{
    key_event->state     = captouch_buffer.data[captouch_buffer.read_index].state;
    key_event->key_data  = captouch_buffer.data[captouch_buffer.read_index].key_data;
    key_event->time_stamp= captouch_buffer.data[captouch_buffer.read_index].time_stamp;
    captouch_buffer.read_index++;
    captouch_buffer.read_index &= (CAPTOUCH_BUFFER_SIZE - 1);
}

uint32_t captouch_get_buffer_left_size(void)
{
    if (captouch_buffer.write_index >= captouch_buffer.read_index) {
        return (CAPTOUCH_BUFFER_SIZE - (captouch_buffer.write_index - captouch_buffer.read_index));
    } else {
        return (captouch_buffer.read_index - captouch_buffer.write_index);

    }
}

uint32_t captouch_get_buffer_data_size(void)
{
    return (CAPTOUCH_BUFFER_SIZE - captouch_get_buffer_left_size());
}

bool captouch_get_event_from_fifo(uint32_t *event,uint32_t *timestap)
{
    if (captouch_hif->TOUCH_HIF_CON0.CELLS.FIFO_OVERFLOW) {
        log_hal_info("event overflow occured\r\n");
    }

    if (captouch_hif->TOUCH_HIF_CON1.CELLS.EVENT_PENDING == 1) {

        /*read timestamp*/
        *timestap = captouch_hif->TOUCH_HIF_CON2;

        /*read event type*/
        *event    = (uint32_t)captouch_hif->TOUCH_HIF_CON1.CELLS.EVENT_TYPE;

        /*pop event*/
        captouch_hif->TOUCH_HIF_CON1.CELLS.EVENT_POP = 1;

        return true;
    }
    else {
        return false;
    }

}

void captouch_interrupt_handler(hal_nvic_irq_t irq_number)
{
    uint32_t event;
    uint32_t time_stamp;
    hal_captouch_key_state_t state;
    hal_captouch_channel_t channel;

    while (captouch_hif->TOUCH_HIF_CON1.CELLS.EVENT_PENDING) {
        if (captouch_hif->TOUCH_HIF_CON0.CELLS.FIFO_OVERFLOW) {
            //log_hal_info("event overflow occured\r\n");
            captouch_hif->TOUCH_HIF_CON0.CELLS.HIF_SOFT_RST = 1;
            hal_gpt_delay_us(1);
        }
        /*read timestamp*/
        time_stamp = captouch_hif->TOUCH_HIF_CON2;
        if (gdummyflag){
            if(time_stamp > 0x10000) {
                gdummyflag = !gdummyflag;
            }
            else{
                captouch_hif->TOUCH_HIF_CON0.CELLS.HIF_SOFT_RST = 1;
                hal_gpt_delay_us(10);
                break;
            }
        }
        /*read event type*/
        event    = (uint32_t)captouch_hif->TOUCH_HIF_CON1.CELLS.EVENT_TYPE;
        if (event <= 3) {
            state    = HAL_CAP_TOUCH_KEY_PRESS;
            channel = event;
        }
        else {
            state = HAL_CAP_TOUCH_KEY_RELEASE;
            channel = event -4 ;
        }
       if(!captouch_get_channel_trigger(0) && captouch_get_control_manual_state(0) ){
            for (int i =0;i<4;i++ ){
                if (captouch_context.used_channel_map & (1<<i)) {
                    captouch_set_control_manual(i,false);
                    hal_captouch_set_avg(i,5,captouch_context.avg_s[i]);
                }
            }
            xTimerStartFromISR(captouch_fine_base_delay100ms_timer, 0);
        }

        /*pop event*/
        captouch_hif->TOUCH_HIF_CON1.CELLS.EVENT_POP = 1;
        //captouch_push_one_event_to_buffer(channel,state,time_stamp);
        captouch_channel_debounce_check(channel, state, time_stamp);
    }

    if(captouch->TOUCH_LPWUFLAG.LPWUINT) {
        captouch->TOUCH_LPWUCLR.LPWUIN_CLR=1;
        while(captouch->TOUCH_LPWUFLAG.LPWUINT);
        #ifdef RTC_CAPTOUCH_SUPPORTED
        captouch->TOUCH_LPWUCLR.LPWUIN_CLR=0;
        rtc_internal_clear_wakeup_status();
        #endif
    }

    //HAL_CAPTOUCH_LOG_PRINT("captouch_get_channel_trigger=%d captouch_get_tune_auto_control_state =%d ",2,captouch_get_channel_trigger(0),captouch_get_tune_auto_control_state(0));

    //if (captouch_context.has_initilized == true) {
       // captouch_call_user_callback();
    //}
}

void captouch_call_user_callback(void)
{
    hal_captouch_callback_context_t *context;

    context = &captouch_context.captouch_callback;

    if (captouch_context.has_initilized != true) {
        return;
    }

    context->callback(context->user_data);
}

void captouch_channel_debounce_check(hal_captouch_channel_t channel, hal_captouch_key_state_t state, uint32_t time_stamp)
{
    hal_captouch_event_t hal_captouch_event;
    hal_captouch_status_t ret;
    bool is_noise=false;

    HAL_CAPTOUCH_LOG_PRINT("captouch channel debounce check channel:%d, state:%d, time_stamp:0x%x",3 , channel, state, time_stamp);

   // if(channel == captouch_context.ear_detect) return;

    if(state)
        event_time_stamp[channel] = time_stamp;
    else
        is_noise = captouch_intr_bounce_check(channel, time_stamp);

    switch(channel)
    {
        case HAL_CAPTOUCH_CHANNEL_0:
            if(!is_noise) //sw debounce 40ms
            {
                if(state) //press
                {
                    HAL_CAPTOUCH_LOG_PRINT("captouch debounce - press intr - start debounce timer", 0);
                    captouch_push_one_event_to_buffer(channel,state,time_stamp);
                    xTimerStartFromISR(captouch_debounce_timer_ch0, 0);
                }
                else//release
                {
                    HAL_CAPTOUCH_LOG_PRINT("captouch debounce - release intr - send event to top", 0);
                    captouch_push_one_event_to_buffer(channel,state,time_stamp);
                    captouch_key_release_event_handler(captouch_debounce_timer_ch0);
                }
            }
            else
            {
                HAL_CAPTOUCH_LOG_PRINT("captouch debounce timer is active - release intr noise", 0);
                xTimerStopFromISR(captouch_debounce_timer_ch0, 0);
                ret = hal_captouch_get_event(&hal_captouch_event);
                if(!ret) HAL_CAPTOUCH_LOG_PRINT("captouch debounce timer is active - release intr noise fail ret:%d", 1, ret);
            }
        break;
        case HAL_CAPTOUCH_CHANNEL_1:
            if(!is_noise) //sw debounce 40ms
            {
                if(state) //press
                {
                    HAL_CAPTOUCH_LOG_PRINT("captouch debounce - press intr - start debounce timer", 0);
                    captouch_push_one_event_to_buffer(channel,state,time_stamp);
                    xTimerStartFromISR(captouch_debounce_timer_ch1, 0);
                }
                else//release
                {
                    HAL_CAPTOUCH_LOG_PRINT("captouch debounce - release intr - send event to top", 0);
                    captouch_push_one_event_to_buffer(channel,state,time_stamp);
                    captouch_key_release_event_handler(captouch_debounce_timer_ch1);
                }
            }
            else
            {
                HAL_CAPTOUCH_LOG_PRINT("captouch debounce timer is active - release intr noise", 0);
                xTimerStopFromISR(captouch_debounce_timer_ch1, 0);
                ret = hal_captouch_get_event(&hal_captouch_event);
                if(!ret) HAL_CAPTOUCH_LOG_PRINT("captouch debounce timer is active - release intr noise fail ret:%d", 1, ret);
            }
        break;
        case HAL_CAPTOUCH_CHANNEL_2:
            if(!is_noise) //sw debounce 40ms
            {
                if(state) //press
                {
                    HAL_CAPTOUCH_LOG_PRINT("captouch debounce - press intr - start debounce timer", 0);
                    captouch_push_one_event_to_buffer(channel,state,time_stamp);
                    xTimerStartFromISR(captouch_debounce_timer_ch2, 0);
                }
                else//release
                {
                    HAL_CAPTOUCH_LOG_PRINT("captouch debounce - release intr - send event to top", 0);
                    captouch_push_one_event_to_buffer(channel,state,time_stamp);
                    captouch_key_release_event_handler(captouch_debounce_timer_ch2);
                }
            }
        else
        {
            HAL_CAPTOUCH_LOG_PRINT("captouch debounce timer is active - release intr noise", 0);
            xTimerStopFromISR(captouch_debounce_timer_ch2, 0);
            ret = hal_captouch_get_event(&hal_captouch_event);
            if(!ret) HAL_CAPTOUCH_LOG_PRINT("captouch debounce timer is active - release intr noise fail ret:%d", 1, ret);
        }
        break;
        case HAL_CAPTOUCH_CHANNEL_3:
            if(!is_noise) //sw debounce 40ms
            {
                if(state) //press
                {
                    HAL_CAPTOUCH_LOG_PRINT("captouch debounce - press intr - start debounce timer", 0);
                    captouch_push_one_event_to_buffer(channel,state,time_stamp);
                    xTimerStartFromISR(captouch_debounce_timer_ch3, 0);
                }
                else//release
                {
                    HAL_CAPTOUCH_LOG_PRINT("captouch debounce - release intr - send event to top", 0);
                    captouch_push_one_event_to_buffer(channel,state,time_stamp);
                    captouch_key_release_event_handler(captouch_debounce_timer_ch3);
                }
            }
        else
        {
            HAL_CAPTOUCH_LOG_PRINT("captouch debounce timer is active - release intr noise", 0);
            xTimerStopFromISR(captouch_debounce_timer_ch3, 0);
            ret = hal_captouch_get_event(&hal_captouch_event);
            if(!ret) HAL_CAPTOUCH_LOG_PRINT("captouch debounce timer is active - release intr noise fail ret:%d", 1, ret);
        }
        break;
        default:
        break;
    }
	
}

bool captouch_intr_bounce_check(hal_captouch_channel_t channel, uint32_t time_stamp)
{
    bool is_noise = true;
    int32_t temp;

    temp = time_stamp - event_time_stamp[channel];
    if(temp < 0)
        temp = (time_stamp + 0x3FFFFFF) - event_time_stamp[channel];
    if(temp*30/1000 > 40) //temp*30/1000 > normal_setting->swDebounceTime
        is_noise = false;

    return is_noise;
}

void captouch_key_press_event_handler(TimerHandle_t xTimer)
{
    UNUSED(xTimer);
    hal_captouch_event_t	hal_captouch_event;

    if(noise_cnt != 0)
    {
        noise_cnt--;
        return;
    }
    //hal_captouch_get_event(&hal_captouch_event);
    //HAL_CAPTOUCH_LOG_PRINT("captouch key press event handler channel:%d, state:%d",2, hal_captouch_event.channel, hal_captouch_event.state);
    //HAL_CAPTOUCH_LOG_PRINT("==== captouch send msg to app channel:%d, state:%d,====",2,hal_captouch_event.key_data,hal_captouch_event.state );
    if (captouch_context.has_initilized == true) {
        captouch_call_user_callback();
    }
    //captouch_context.debounce_state[hal_captouch_event.channel] = hal_captouch_event.state*100;
}

void captouch_key_release_event_handler(TimerHandle_t channel_timer)
{
    hal_captouch_event_t	hal_captouch_event;

    hal_captouch_get_event(&hal_captouch_event);
    //HAL_CAPTOUCH_LOG_PRINT("captouch key release event handler channel:%d, state:%d",2, hal_captouch_event.channel, hal_captouch_event.state);

    if(hal_captouch_event.state== 1)
    {
        hal_captouch_get_event(&hal_captouch_event);
        xTimerStopFromISR(channel_timer, 0);
        //hal_captouch_get_event(&hal_captouch_event);
        noise_cnt++;
    }
    else
    {
        captouch_push_one_event_to_buffer(hal_captouch_event.key_data,hal_captouch_event.state,hal_captouch_event.time_stamp);
        //HAL_CAPTOUCH_LOG_PRINT("==== captouch send msg to app channel:%d, state:%d ",2,hal_captouch_event.key_data,hal_captouch_event.state );
        if (captouch_context.has_initilized == true) {
            captouch_call_user_callback();
        }
        //captouch_context.debounce_state[hal_captouch_event.channel] = hal_captouch_event.state*100;		
    }
}


bool captouch_lpsd_handler(void)
{
    bool shutd_flag;

    shutd_flag = false;

    if(captouch->TOUCH_LPSDFLAG.LPSDINT) {
        shutd_flag = true;
        captouch->TOUCH_LPSDCLR.LPSDIN_CLR=1;
        while(captouch->TOUCH_LPSDFLAG.LPSDINT);
        captouch->TOUCH_LPSDCLR.LPSDIN_CLR =0;
    }
    return shutd_flag;
}


void captouch_register_nvic_callback(void)
{
    uint32_t mask;
    static bool register_flag;

    if (register_flag == false) {
        hal_nvic_save_and_set_interrupt_mask(&mask);
        hal_nvic_register_isr_handler(CAP_TOUCH_IRQn, captouch_interrupt_handler);
        hal_nvic_enable_irq(CAP_TOUCH_IRQn);
        hal_nvic_restore_interrupt_mask(mask);
        register_flag = true;
    }
}
void captouch_clk_control(bool is_clock_on)
{
    if (is_clock_on == true) {
         captouch->TOUCH_CON0.CON0 |= (1<<8);
    }
    else {
         captouch->TOUCH_CON0.CON0 &= ~(1<<8);
    }
}

void captouch_set_clk(hal_captouch_lowpower_type_t power_type,hal_captouch_clock_t clock)
{
    uint32_t tmp;
    tmp = captouch->TOUCH_CON1.CON1;
    if (power_type == HAL_CAPTOUCH_MODE_NORMAL) {
        tmp &= ~(0x7);
        tmp |= clock;
        captouch->TOUCH_CON1.CON1 = tmp;
    }
    else{
        tmp &= ~(0x70);
        tmp |= clock;
        captouch->TOUCH_CON1.CON1 = tmp;
    }
}

void captouch_set_mavg(hal_captouch_channel_t channel,uint8_t mavg)
{
    uint32_t tmp;
    tmp = captouch->TOUCH_CON3.CON3;
    tmp &= ~(0xf << (channel*4));
    tmp |= (mavg << (channel*4));
    captouch->TOUCH_CON3.CON3 = tmp;
}

void captouch_set_avg(hal_captouch_channel_t channel,uint8_t avg)
{
    uint32_t tmp;
    tmp = captouch->TOUCH_CON2.CON2;
    tmp &= ~(0x7 << (channel*3));
    tmp |= (avg << (channel*3));
    captouch->TOUCH_CON2.CON2 = tmp;
}


void captouch_set_fine_cap(hal_captouch_channel_t channel,int32_t fine_tune)
{
    uint32_t tmp;
    tmp = captouch->TOUCH_FINECAP.FINECAP;
    tmp &= ~(0x7f << (channel*8));
    tmp |= (fine_tune << (channel*8));
    captouch->TOUCH_FINECAP.FINECAP = tmp;
}

int8_t captouch_get_fine_cap(hal_captouch_channel_t channel)
{
    uint32_t tmp;
    uint16_t data=0;
    captouch_switch_debug_sel(channel);
    tmp = captouch->TOUCH_DBG1.DBG1;
    data = (tmp>>16) & 0x7f;
    return captouch_to16signed(7,data);


}

void captouch_set_coarse_cap(hal_captouch_channel_t channel, uint8_t coarse_tune)
{
    uint32_t tmp;
    tmp = captouch->TOUCH_CRSCFG.CRSCFG;
    tmp &= ~(0x7 << (channel*3));
    tmp |= (coarse_tune << (channel*3));
    captouch->TOUCH_CRSCFG.CRSCFG = tmp;
}

uint8_t captouch_get_coarse_cap(hal_captouch_channel_t channel)
{
    uint32_t tmp;
    uint8_t ret;
    tmp = captouch->TOUCH_CRSCFG.CRSCFG;
    ret  =(uint8_t)((tmp >> (channel*3)) & 0x7);

    return ret; 
}

void captouch_set_threshold(hal_captouch_channel_t channel,int32_t high_thr, int32_t low_thr)
{
    switch (channel) {
        case HAL_CAPTOUCH_CHANNEL_0:
            captouch->TOUCH_THR0.THR = low_thr | (high_thr<<16);
        break;
        case HAL_CAPTOUCH_CHANNEL_1:
            captouch->TOUCH_THR1.THR = low_thr | (high_thr<<16);
        break;
        case HAL_CAPTOUCH_CHANNEL_2:
            captouch->TOUCH_THR2.THR = low_thr | (high_thr<<16);
        break;
         case HAL_CAPTOUCH_CHANNEL_3:
            captouch->TOUCH_THR3.THR = low_thr | (high_thr<<16);
        break;
        default:
        break;
    }

}

void captouch_set_nthreshold(hal_captouch_channel_t channel,int32_t high_thr, int32_t low_thr)
{
    switch (channel) {
        case HAL_CAPTOUCH_CHANNEL_0:
            captouch->TOUCH_NTHR0.THR = low_thr | (high_thr<<16);
        break;
        case HAL_CAPTOUCH_CHANNEL_1:
            captouch->TOUCH_NTHR1.THR = low_thr | (high_thr<<16);
        break;
        case HAL_CAPTOUCH_CHANNEL_2:
            captouch->TOUCH_NTHR2.THR = low_thr | (high_thr<<16);
        break;
         case HAL_CAPTOUCH_CHANNEL_3:
            captouch->TOUCH_NTHR3.THR = low_thr | (high_thr<<16);
        break;
        default:
        break;
    }

}

void captouch_set_dynamic_threshold(bool nthr_en,bool thr_en,int16_t rangeH, int16_t rangeL)
{
    uint32_t tmp;
    tmp = captouch->TOUCH_BASELINE.BASERANGE;
    tmp &= ~0xfffffff;

    if (nthr_en == true) {
        tmp |= (TOUCH_NTHRH_EN | TOUCH_NTHRL_EN);
    }
    else {
        tmp &= ~(TOUCH_NTHRH_EN | TOUCH_NTHRL_EN);
    }
    if (thr_en == true) {
        tmp |= (TOUCH_THRH_EN | TOUCH_THRL_EN);
    }
    else {
        tmp &= ~(TOUCH_THRH_EN | TOUCH_THRL_EN);
    }

    rangeH &= 0x1ff;
    rangeL &= 0x1ff;
    tmp |= (rangeL | (rangeH<<16));
    captouch->TOUCH_BASELINE.BASERANGE = tmp;
}

void captouch_set_control_manual(hal_captouch_channel_t channel,bool is_auto)
{
    uint32_t rdata;

    rdata = captouch->TOUCH_CON1.CON1;
    if (is_auto == true) {
        captouch->TOUCH_CON1.CON1 = BIT_FIELD_INSERT32(rdata,(channel+8),1,1);
    }
    else {
        captouch->TOUCH_CON1.CON1 = BIT_FIELD_INSERT32(rdata,(channel+8),1,0);
    }

}
bool captouch_get_control_manual_state(hal_captouch_channel_t channel)
{
    uint32_t rdata;
    rdata = captouch->TOUCH_CON1.CON1;
    //printf("captouch->TOUCH_CON1.CON1 %x ",rdata);
    return  BIT_FIELD_EXTRACT32(rdata,(channel+8),1);
}

void captouch_set_autok_suspend(uint8_t channel_bit_map, bool en)
{
    uint32_t rdata;

    rdata = captouch->TOUCH_CON0.CON0;
    if (en == true) {
        rdata |= (channel_bit_map<<24);
    }
    else {
        rdata &= ~(channel_bit_map<<24);
    }
    captouch->TOUCH_CON0.CON0 = rdata;
}

void captouch_set_autok_Nsuspend(uint8_t channel_bit_map, bool en)
{
    uint32_t rdata;

    rdata = captouch->TOUCH_CON0.CON0;
    if (en == true) {
        rdata |= (channel_bit_map<<28);
    }
    else {
        rdata &= ~(channel_bit_map<<28);
    }
    captouch->TOUCH_CON0.CON0 = rdata;
}

void captouch_channel_sense_control(uint8_t channel_bit_map, bool en)
{
    uint32_t temp;
	
    temp = captouch->TOUCH_CON0.CON0;
    if (en == true) {
        temp |= (channel_bit_map | (channel_bit_map<<16));
    }
    else {
        temp &= ~(channel_bit_map | (channel_bit_map<<16));
    }
    captouch->TOUCH_CON0.CON0 = temp;
}

void captouch_channel_sensing_control(uint8_t channel_bit_map, bool en)
{
    uint32_t temp;
    temp = captouch->TOUCH_CON0.CON0;
    if (en == true) {
        temp |= (channel_bit_map);
    }
    else {
        temp &= ~(channel_bit_map);
    }
    captouch->TOUCH_CON0.CON0 = temp;
}

void captouch_int_control(bool en)
{
    uint32_t temp;
    temp = captouch->TOUCH_CON0.CON0;

    if (en==true) {
        temp |= (TOUCH_INT_EN | TOUCH_WAKE_EN);
    }
    else {
        temp &= ~(TOUCH_INT_EN | TOUCH_WAKE_EN);
    }
    captouch->TOUCH_CON0.CON0 = temp;
}

void captouch_longpress_int_control(bool en)
{
    uint32_t temp;
    temp = captouch->TOUCH_LPWUCON.LPWUCON;

    if (en==true) {
        temp |= (TOUCH_LPWU_INT_EN | TOUCH_LPWU_WAKE_EN);
    }
    else {
        temp &= ~(TOUCH_LPWU_INT_EN | TOUCH_LPWU_WAKE_EN);
    }    
    captouch->TOUCH_LPWUCON.LPWUCON = temp;  
}

void captouch_channel_int_control(uint8_t channel_bit_map, bool en)
{
    uint32_t temp;
    temp = captouch->TOUCH_CHMASK.CHMASK;
    if (en == true) {
        temp |= ((channel_bit_map<<8) | (channel_bit_map<<12));
    }
    else {
        temp &= ~((channel_bit_map<<8) | (channel_bit_map<<12));
    }
    captouch->TOUCH_CHMASK.CHMASK = temp;
}

void captouch_wakeup_setting(uint8_t channel_bit_map, bool en)
{
    uint32_t temp;
    temp = captouch->TOUCH_CHMASK.CHMASK;
    if (en == true) {
        temp |= (channel_bit_map | (channel_bit_map<<4));
    }
    else {
        temp &= ~(channel_bit_map | (channel_bit_map<<4));
    }
    captouch->TOUCH_CHMASK.CHMASK = temp;
}

void captouch_longpress_channel_control(hal_captouch_longpress_type_t type, uint32_t count)
{
    uint32_t temp;

    if(type == HAL_CAPTOUCH_LONGPRESS_SHUTDOWN) {
        temp = captouch->TOUCH_LPSDCON.LPSDCON;
        temp |= TOUCH_LPSD_MASK;
        captouch->TOUCH_LPSDCON.LPSDCON = temp;
        captouch->TOUCH_LPSDTAR.LPSDTAR = count;
    }
    else {
        temp = captouch->TOUCH_LPWUCON.LPWUCON;
        temp |= TOUCH_LPWU_MASK;
        captouch->TOUCH_LPWUCON.LPWUCON = temp;
        captouch->TOUCH_LPWUTAR.LPWUTAR = count;
    }
}

void captouch_switch_debug_sel(hal_captouch_channel_t channel)
{
    uint32_t temp;
    temp = captouch->TOUCH_CON1.CON1;
    temp &= ~(0x3<<24);
    temp |= (channel<<24);
    captouch->TOUCH_CON1.CON1 = temp;
}

int16_t captouch_to16signed(uint16_t bit, int16_t data)
{
    data <<= (16-bit);
    data >>= (16-bit);

    return data;
}
void captouch_get_tune_state(hal_captouch_channel_t channel,hal_captouch_tune_data_t *tune_data)
{
    uint32_t temp;
    uint16_t data=0;

    captouch_switch_debug_sel(channel);
    tune_data->coarse_cap = captouch_get_coarse_cap(channel);//coares cap value

    temp = captouch->TOUCH_DBG1.DBG1;
    data = (temp>>16) & 0x7f;
    tune_data->fine_cap = captouch_to16signed(7,data);

    temp = captouch->TOUCH_DBG0.DBG0;
    data = (temp & 0xffff);
    tune_data->avg_adc = captouch_to16signed(9,data);

    temp = captouch->TOUCH_DBG2.DBG2;
    data = (temp & 0xffff);
    tune_data->vadc = captouch_to16signed(9,data);

    temp = captouch->TOUCH_FINECAP.FINECAP;
    tune_data->man  = temp & (0xff<<channel);

    temp = captouch->TOUCH_DBG1.DBG1;
    data =(temp & 0xffff);
    tune_data->mavg_dbg = captouch_to16signed(9,data);
    //printf("captouch_get_tune_state  dbg0 adc %d,finecap db=%d\r\n", tune_data->avg_adc,tune_data->fine_cap); 

}
#if 0
bool captouch_sw_auto_tune(hal_captouch_channel_t channel, hal_captouch_tune_data_t *tune_data)
{
    uint16_t data;
    uint16_t temp;
    int16_t vadc_dbg;
    //int8_t cal_out;
    uint8_t coare_index;
    int16_t  find_cap[3];
    bool is_tune_ok;

    //captouch_tune_auto_control(channel,true);

    coare_index = 0;
    find_cap[0] = -64;
    find_cap[1] = 0;
    find_cap[2] = 63;

    is_tune_ok = false;
    while(1) {

        captouch_set_fine_cap(channel,find_cap[1]);
        captouch_set_coarse_cap(channel,coare_index);

        hal_gpt_delay_ms(2);

        temp = captouch->TOUCH_DBG2.DBG2;   
        data =(temp & 0xffff);
        vadc_dbg = captouch_to16signed(9,data);

        // the adc value should be +-32
        if (vadc_dbg>=10) {             //if adc is bigger, the fine_cap should be bigger to decrease the adc value
            find_cap[0] =  find_cap[1]; //the lower limit should move to the current value
            find_cap[1] = (find_cap[2] + find_cap[1])/2;
        }
        else if(vadc_dbg<=(-20)) {      //if adc is smller, the fine_cap should be smller to increase the adc value
            find_cap[2] = find_cap[1];  //the upper limit should move to the current value
            find_cap[1] = (find_cap[1] + find_cap[0])/2;    
        }
        else {
            is_tune_ok = true;
            break;
        }

        if ((find_cap[1]>35) || (find_cap[1]<=(-35))){
            ++coare_index;
            if (coare_index>=8) {
                is_tune_ok = false;
                break;
            }   
            find_cap[0] = -64;
            find_cap[1] = 0;
            find_cap[2] = 63;
        }
        
    }

    captouch_get_tune_state(channel,tune_data);
    log_hal_info("captouch_sw_auto_tune ch=%d,fine_cap=%d, crs=%d, vadc_dbg=%d\r\n",channel,find_cap[1],coare_index,vadc_dbg);
    //captouch_tune_auto_control(channel,false);

    return is_tune_ok;

}
#endif
bool captouch_hw_auto_tune(hal_captouch_channel_t channel, hal_captouch_tune_data_t *tune_data )
{
    uint32_t time1,time2,duration;
    uint32_t temp;
    uint16_t data=0;
    int16_t cal_out, vadc_dbg;
    bool is_tune_ok =true;

    captouch_switch_debug_sel(channel);
	temp = captouch->TOUCH_DBG1.DBG1;
    data = (temp>>16) & 0x7f;
    cal_out = captouch_to16signed(7,data);

    temp = captouch->TOUCH_DBG0.DBG0;
    data = (temp & 0xffff);
    vadc_dbg = captouch_to16signed(9,data);
    HAL_CAPTOUCH_LOG_PRINT("captouch coarse cap =%d  channel =%d ", 2, tune_data->coarse_cap,channel); 
    if( (cal_out >44 || vadc_dbg >= 40) && tune_data->coarse_cap<7) {
        //LOG_I(LOG_OS, "captouch_sw_auto_tune 0xdb >44[%d]  ", 1,cal_out); 
        HAL_CAPTOUCH_LOG_PRINT("captouch_hw_auto_tune try set_coarse_cap ",0);
        captouch_channel_sensing_control( 1<<channel , captouch_disable);
        captouch_set_coarse_cap(channel,++tune_data->coarse_cap);
        captouch_channel_sensing_control(1<<channel, captouch_enable);
    }
    else if( (cal_out<=(-44)|| vadc_dbg <= (-40))&& tune_data->coarse_cap>0) {
        //LOG_I(LOG_OS, "captouch_sw_auto_tune 0xdb <44[%d]  ", 1,cal_out);   
        HAL_CAPTOUCH_LOG_PRINT("captouch_hw_auto_tune try set_coarse_cap ",0);
        captouch_channel_sensing_control(1<<channel, captouch_disable); 
        captouch_set_coarse_cap(channel,--tune_data->coarse_cap);
        captouch_channel_sensing_control(1<<channel, captouch_enable);
    }
    else {
        HAL_CAPTOUCH_LOG_PRINT("captouch_hw_auto_tune  first choose finecap base =%d ", 1, cal_out); 
    }

    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time1);

    while(1){

        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time2);
        hal_gpt_get_duration_count(time1, time2, &duration);

        if((duration>100000)) {
            temp = captouch->TOUCH_DBG1.DBG1;
            data = (temp>>16) & 0x7f;
            cal_out = captouch_to16signed(7,data);

            temp = captouch->TOUCH_DBG0.DBG0;
            data = (temp & 0xffff);
            vadc_dbg = captouch_to16signed(9,data);

            if(cal_out <50 && (cal_out>=(-50)) &&  vadc_dbg <=40 && vadc_dbg >=(-40)) {
                is_tune_ok = true;
                captouch_context.fine_cap_base[channel] = cal_out;
                HAL_CAPTOUCH_LOG_PRINT("captouch_hw_auto_tune sucess coarse cap = %d fine_cap_base = %d adc = %d",3,\
                                       tune_data->coarse_cap,captouch_context.fine_cap_base[channel],vadc_dbg);
            }else{
                is_tune_ok = false;
                captouch_context.fine_cap_base[channel] = cal_out;
                HAL_CAPTOUCH_LOG_PRINT("captouch_hw_auto_tune fail coarse cap = %d finecap = %d adc = %d ",3,\
                                       tune_data->coarse_cap,cal_out,vadc_dbg);
            }
            break;
         }
    }


    HAL_CAPTOUCH_LOG_PRINT("captouch_hw_auto_tune end", 0);

    return is_tune_ok;


#if 0
    uint8_t  coare_temp;
    uint32_t temp;
    int8_t cal_dbg;
    uint16_t data;
    uint32_t time_start,time_current,duration;

    coare_temp = 0;
    data = 0;
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time_start);
    captouch_set_coarse_cap(channel,coare_temp);
    hal_gpt_delay_ms(100);

    while(1) {

        temp = captouch->TOUCH_DBG1.DBG1;
        data = (temp>>16) & 0x7f;
        cal_dbg = captouch_to16signed(7,data);

        //log_hal_info("[c_tune ch%d]:crs_cap=%d cnt=%d, cal_dbg=%d, cal_out=%d, vadc_dbg=%d\r\n",channel,i,count,cal_dbg,cal_out,(int)vadc_dbg);
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time_current);
        hal_gpt_get_duration_count(time_start, time_current, &duration);

        if (((cal_dbg>(-44)) && (cal_dbg<44))) {
            break;
        }
        if(duration>100000) {
            ++coare_temp;
            if(coare_temp>7) {
                break;
            }
            time_start = time_current;
            captouch_set_coarse_cap(channel,coare_temp);
        }
    }
    log_hal_info("captouch_hw_auto_tune ch=%d,cal_dbg=%d,crs=%d\r\n",channel,cal_dbg,coare_temp);

    captouch_get_tune_state(channel,tune_data);

    if (coare_temp>7) {
        return false;
    }
    else {
        return true;
    }
#endif
}

void captouch_find_baseline(hal_captouch_channel_t channel)
{
    uint32_t temp;
    uint16_t data=0;
    int16_t vadc_dbg;
    uint32_t time1,time2,duration;

    captouch_switch_debug_sel(channel);
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time1);

    while(1)
    {
        temp = captouch->TOUCH_DBG0.DBG0;
        data = (temp & 0xffff);
        vadc_dbg = captouch_to16signed(9,data);
       
        hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &time2);
        hal_gpt_get_duration_count(time1, time2, &duration);

         if(vadc_dbg <=40 && vadc_dbg >=(-40)) {
             HAL_CAPTOUCH_LOG_PRINT("captouch_find_baseline done vadc_dbg =%d fine cap = %d  channel= %d ",3,vadc_dbg,captouch_get_fine_cap(channel),channel);
             break;
         }

         if(duration>100000) {
             HAL_CAPTOUCH_LOG_PRINT("captouch_find_baseline fail vadc_dbg =%d fine cap = %d channel =%d",3,vadc_dbg,captouch_get_fine_cap(channel),channel);
             break;
         }
    }

}

bool captouch_rtc_clk_control(bool is_en)
{
    #ifdef RTC_CAPTOUCH_SUPPORTED
    hal_rtc_status_t rtc_ret;

    if (is_en == true) {
        rtc_ret = hal_rtc_captouch_init();
    }
    else {
        rtc_ret = hal_rtc_captouch_deinit();
    }

    if (rtc_ret != HAL_RTC_STATUS_OK) {
        //log_hal_info("[Captouch] RTC init/deinit:%d fail!\r\n",is_en);
        return HAL_CAPTOUCH_STATUS_ERROR;
    }
    else {
        return HAL_CAPTOUCH_STATUS_OK;
    }
    #endif
    return HAL_CAPTOUCH_STATUS_OK;
}

bool captouch_rtc_lpm_control(hal_captouch_lowpower_type_t lowpower_type)
{
#ifdef RTC_CAPTOUCH_SUPPORTED
    hal_rtc_status_t rtc_ret;

    if (HAL_CAPTOUCH_MODE_LOWPOWER == lowpower_type) {
        rtc_ret = rtc_internal_captouch_lowpower(HAL_CAPTOUCH_MODE_LOWPOWER);
    }
    else {
        rtc_ret = rtc_internal_captouch_lowpower(HAL_CAPTOUCH_MODE_NORMAL);
    }
    
    if (rtc_ret != HAL_RTC_STATUS_OK) {
       log_hal_info("lpm fail\r\n");
        return HAL_CAPTOUCH_STATUS_ERROR;
    }
    else {
        log_hal_info("lpm sucess\r\n");
        return HAL_CAPTOUCH_STATUS_OK;
    }
#endif
}


void captouch_analog_init(void)
{
    uint32_t temp;
    temp = captouch->TOUCH_CON1.CON1;
    temp |= (TOUCH_BACK2BACK_EN | TOUCH_AUTO_DISABLE_CH | TOUCH_PER_CH_GATING_EN);
    captouch->TOUCH_CON1.CON1 = temp;
    captouch_set_clk(HAL_CAPTOUCH_MODE_NORMAL,HAL_CAPTOUCH_CLK_32K);

    temp = captouch->TOUCH_ANACFG1.ANACFG1;
    captouch->TOUCH_ANACFG1.ANACFG1 = temp | TOUCH_LDO_EN;
    hal_gpt_delay_us(50);
    captouch->TOUCH_ANACFG1.ANACFG1 = temp | TOUCH_LDO_EN | TOUCH_OP_EN;
    hal_gpt_delay_us(65);
    captouch->TOUCH_ANACFG1.ANACFG1 = temp | TOUCH_LDO_EN | TOUCH_OP_EN | TOUCH_ADC_EN;
    hal_gpt_delay_us(5);
    captouch->TOUCH_ANACFG0.ANACFG0 = (1<<16) | 0x2e;
}

void captouch_analog_deinit(void)
{
    uint32_t temp;
    temp = captouch->TOUCH_ANACFG1.ANACFG1;

    captouch->TOUCH_ANACFG0.ANACFG0 = (0<<16) | 0x2e;
    hal_gpt_delay_us(5);
    temp &= ~(TOUCH_ADC_EN);
    captouch->TOUCH_ANACFG1.ANACFG1= temp;
    hal_gpt_delay_us(5);
    temp &= ~(TOUCH_ADC_EN | TOUCH_OP_EN);
    captouch->TOUCH_ANACFG1.ANACFG1= temp;
    hal_gpt_delay_us(5);
    temp &= ~(TOUCH_ADC_EN | TOUCH_OP_EN | TOUCH_LDO_EN);
    captouch->TOUCH_ANACFG1.ANACFG1= temp;
}

void captouch_set_lpsd_chk_vbus(bool is_en)
{
	if(is_en)
		captouch->TOUCH_LPSDCON.LPSDCON |= (1 << 9);
	else
		captouch->TOUCH_LPSDCON.LPSDCON &= ~(1 << 9);
}

void captouch_set_vbus_flag(bool is_chgin)
{
	if(is_chgin)
		captouch->TOUCH_LPSDCON.LPSDCON |= (1 << 24);
	else
		captouch->TOUCH_LPSDCON.LPSDCON &= ~(1 << 24);
}

bool captouch_get_channel_trigger(hal_captouch_channel_t channel)
{
    uint32_t temp;
    int16_t touch_trig;

    captouch_switch_debug_sel(channel);
    temp = captouch->TOUCH_DBG0.DBG0;
    touch_trig = BIT_FIELD_EXTRACT32(temp,16,4);
    //HAL_CAPTOUCH_LOG_PRINT("captouch captouch_event_check channel:%d, touch_trig:%x", 2, channel, touch_trig);
    return touch_trig & (1<<channel);

}

void captouch_write_nvdm (hal_captouch_nvdm_id_t id ,hal_captouch_channel_t channel ,int16_t value ){

    hal_captouch_nvdm_data captouch_data;
    uint32_t length;
    //nvdm_status_t ret = NVDM_STATUS_ERROR;
    length = sizeof(hal_captouch_nvdm_data);
    nvkey_read_data (NVKEYID_CAPTOUCH_CALI_4CHDATA, (uint8_t *)(&captouch_data), &length);

    switch (id) {
        case CAPTOUCH_COARSE_CAP_ID:
            captouch_data.coarse_cap[channel]= (uint8_t)value;
        break;
        case CAPTOUCH_FINE_CAP_ID:
            captouch_data.fine_cap[channel]= value;
        break;

        default:
        break;
    }

    nvkey_write_data(NVKEYID_CAPTOUCH_CALI_4CHDATA, (uint8_t *)(&captouch_data), length);

}
void captouch_fine_base_delay100ms_handler(TimerHandle_t xTimer)
{
    UNUSED(xTimer);

    for (int i =0;i<4;i++ ){
        if (captouch_context.used_channel_map & (1<<i)) {
             hal_captouch_set_avg(i,captouch_context.mavg_r[i],captouch_context.avg_s[i]);
             //HAL_CAPTOUCH_LOG_PRINT("captouch fine base delay end and write cfine =%d channel =%d",2,captouch_get_fine_cap(i),i);
             captouch_write_nvdm(CAPTOUCH_FINE_CAP_ID,i,captouch_get_fine_cap(i));
        }
    }
    HAL_CAPTOUCH_LOG_PRINT("captouch fine base delay end and write cfine  ",0);
}

#if 0
VOID captouch_find_baseline(hal_captouch_channel_t channel)
{
    U16 temp;
    int16_t vadc_dbg;
    U32 time1;
    U32 time2;
    captouch_switch_debug_sel(channel);
    HAL_CAPTOUCH_LOG_PRINT("captouch_find_baseline channel %d ", 1,channel);

    time1 =DRV_TIMER_READ_COUNTER();
    HAL_CAPTOUCH_LOG_PRINT("captouch_find_baseline time1 %d ", 1,time1);
    while(1)
    {

        temp = captouch_get_adc_val();
        vadc_dbg  =captouch_to16signed(9,temp);
        //LOG_I(LOG_OS, "test1 0xdd[%d]  ", 1,DRV_3WIRE_Read(TOUCH_AVG_DBG));
        //LOG_I(LOG_OS, "test1 0xdd [%d]  ", 1,vadc_dbg);
        time2 =DRV_TIMER_READ_COUNTER();

         if(vadc_dbg <=40 && vadc_dbg >=(-40)) {
             HAL_CAPTOUCH_LOG_PRINT( "captouch_find_baseline time1 %d", 1,time2);
             break;
         }
         if((time2 - time1)>500000) {
             //LOG_I(LOG_OS, "captouch_find_baseline time1 [%d]  ", 1,time2);
             HAL_CAPTOUCH_LOG_PRINT("captouch_find_baseline fail", 0);
             break;
         }
    }

}
#endif


#endif //HAL_CAPTOUCH_MODULE_ENABLED
 
