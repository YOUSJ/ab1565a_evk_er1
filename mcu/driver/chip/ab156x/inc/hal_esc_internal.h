/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#ifndef __HAL_ESC_INTERNAL_H__
#define __HAL_ESC_INTERNAL_H__

#ifdef HAL_ESC_MODULE_ENABLED
#include "hal_esc.h"
#include "hal_esc_config.h"

/* Private typedef ---------------------------------------------------*/

// GPRAM 4-byte alined access union
typedef union {
    uint32_t   u32;
    uint16_t   u16[2];
    uint8_t    u8[4];
} esc_uint;

typedef struct {
    uint32_t DEVICE_MODE;
    uint32_t ESC_MAC_CTL;
    uint32_t ESC_DIRECT_CTL;
    uint32_t ESC_MISC_CTL1;
    uint32_t ESC_MISC_CTL2;
    uint32_t ESC_DLY_CTL1;
    uint32_t ESC_DLY_CTL2;
    uint32_t ESC_DLY_CTL3;
    uint32_t ESC_DLY_CTL4;
} ESC_SETTINGS;


/* Private macro ---------------------------------------------*/
#define ESC_GPRAM_SIZE     (128)
#define ESC_GPRAM_BASE     (ESC_BASE + 0x800)
#define ESC_MAC_EN         (0x00000100)
#define ESC_MAC_TRIG       (0x00010000)
#define ESC_MAC_WAIT_DONE  (0x00000001)

#define ESC_GENERIC_1_BIT_OFFSET      (1)
#define ESC_GENERIC_2_BIT_OFFSET      (2)
#define ESC_GENERIC_4_BIT_OFFSET      (4)
#define ESC_GENERIC_8_BIT_OFFSET      (8)
#define ESC_GENERIC_10_BIT_OFFSET    (10)
#define ESC_GENERIC_16_BIT_OFFSET    (16)
#define ESC_GENERIC_24_BIT_OFFSET    (24)
#define ESC_GENERIC_31_BIT_OFFSET    (31)

/* ESC generic mask definition */
#define ESC_GENERIC_0x1_MASK         (0x1)
#define ESC_GENERIC_0x0F_MASK        (0x0F)
#define ESC_GENERIC_0xF0_MASK        (0xF0)
#define ESC_GENERIC_0xFF_MASK        (0xFF)
#define ESC_GENERIC_0xF000_MASK      (0xF000)
#define ESC_GENERIC_0x00FF_MASK      (0x00FF)
#define ESC_GENERIC_0x0FFFFFFF_MASK  (0x0FFFFFFF)
#define ESC_GENERIC_0x000000FF_MASK  (0x000000FF)
#define ESC_GENERIC_0x0000FF00_MASK  (0x0000FF00)
#define ESC_GENERIC_0x00FF0000_MASK  (0x00FF0000)
#define ESC_GENERIC_0xFF000000_MASK  (0xFF000000)
#define ESC_GENERIC_0xFFFFFF00_MASK  (0xFFFFFF00)
#define ESC_GENERIC_SRAM_BANK_MASK   (0x05000000)

#define ESC_DIRECT_REG_DEFAULT       (0x03027000)
#define ESC_GPRAM_DATA               (ESC_BASE + 0x0800)

#ifdef MICRO_CHIP_23A1024
//device no ID
#define ESC_CMD_READ             (0x03)
#define ESC_CMD_WRITE            (0x02)

#define ESC_CMD_ENTER_QUAD_MODE  (0x3B)
#define ESC_CMD_EXIT_QUAD_MODE   (0xFF)
#define ESC_CMD_MODE_READ        (0x05)
#define ESC_CMD_MODE_WRITE       (0x01)
#endif


#ifdef ASP_CHIP_1604M_SQ_PSRAM
#define ESC_CMD_READ_ID          (0x9F)
#define ESC_CMD_READ             (0x03)
#define ESC_CMD_FAST_READ        (0x0B)
#define ESC_CMD_QUAD_READ        (0xEB)
#define ESC_CMD_WRAP_READ        (0x8B)
#define ESC_CMD_WRITE            (0x02)
#define ESC_CMD_WRITE_QUAD       (0x38)
#define ESC_CMD_WRAP_WRITE       (0x82)
#define ESC_CMD_MODE_READ        (0xB5)
#define ESC_CMD_MODE_WRITE       (0xB1)
#define ESC_CMD_ENTER_QUAD_MODE  (0x35)
#define ESC_CMD_EXIT_QUAD_MODE   (0xF5)
#define ESC_CMD_RESET_ENABLE     (0x66)
#define ESC_CMD_RESET            (0x99)
#define ESC_CMD_BURST_LEN        (0xC0)
#endif

#ifdef ESC_ASP6404L_SQ_PSRAM
#define ESC_CMD_READ_ID          (0x9F)
#define ESC_CMD_READ             (0x03)
#define ESC_CMD_FAST_READ        (0x0B)
#define ESC_CMD_QUAD_READ        (0xEB)
#define ESC_CMD_WRITE            (0x02)
#define ESC_CMD_WRITE_QUAD       (0x38)
#define ESC_CMD_ENTER_QUAD_MODE  (0x35)
#define ESC_CMD_EXIT_QUAD_MODE   (0xF5)
#define ESC_CMD_RESET_ENABLE     (0x66)
#define ESC_CMD_RESET            (0x99)
#define ESC_CMD_BURST_LEN        (0xC0)
#endif

#ifdef ESC_SFLASH
#define ESC_CMD_READ_ID          (0x9F)
#define ESC_CMD_READ_SR          (0x05)
#define ESC_CMD_FAST_READ        (0x0B)
#define ESC_CMD_MODE_READ        (ESC_CMD_READ_SR)
#define ESC_CMD_READ_SR2         (0x35)
#define ESC_CMD_MODE_WRITE       (0x01)
#define ESC_CMD_MODE_WRITE_SR2   (0x31)
#define ESC_CMD_WRITE_EN         (0x06)
#define ESC_CMD_WRITE_DISABLE    (0x04)
#define ESC_CMD_READ             (0x03)
#define ESC_CMD_WRITE            (0x02)
#define ESC_CMD_QUAD_READ        (0xEB)

#define ESC_CMD_ERASE_CHIP       (0xC7)
#define ESC_CMD_ERASE_4K         (0x20)
#define ESC_CMD_ERASE_32K        (0x52)
#define ESC_CMD_ERASE_64K        (0xD8)

#define ESC_CMD_SP_SUS           (0x75)
#define ESC_CMD_SP_RSM           (0x7A)
#define ESC_CMD_ENTER_QUAD_MODE  (0x38)
#endif


/* Private functions ---------------------------------------------------------*/
#define ESC_ReadReg8(addr)          *((volatile unsigned char *)(addr))
#define ESC_ReadReg32(addr)         *((volatile unsigned int *)(addr))
#define ESC_WriteReg32(addr, data)  *((volatile unsigned int *)(addr)) = (unsigned int)(data)


void ESC_Switch_mode(const uint16_t CS, hal_esc_mode_t mode);
void reset_esc_register(void);
void esc_set_wrap32(uint32_t wrap_en);

#ifdef ESC_SFLASH
void write_mode_register(uint8_t sr);
void read_mode_register2(uint8_t *sr);
void read_mode_register(uint8_t *sr);
int32_t hal_esc_read_flash_data(uint32_t address, uint8_t *buffer, uint32_t len);
int32_t hal_esc_write_flash_data(uint32_t address, uint8_t *data, uint32_t len);
int32_t hal_esc_erase_flash_block(uint32_t address, uint32_t type);
#endif

#endif /* HAL_ESC_MODULE_ENABLED */

#endif /* __HAL_ESC_INTERNAL_H__ */

