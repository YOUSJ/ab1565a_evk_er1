/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#ifndef __HAL_ESC_CONFIG_H__
#define __HAL_ESC_CONFIG_H__

#ifdef HAL_ESC_MODULE_ENABLED

#define SUPPORT_52MHZ

#ifdef HAL_ESC_WITH_FLASH
#define ESC_SFLASH
#define W25Q32JW
#endif

#ifdef HAL_ESC_WITH_PSRAM
#undef ESC_SFLASH
#define ASP_CHIP_1604M_SQ_PSRAM
#endif

#if defined(HAL_ESC_WITH_FLASH) && defined(HAL_ESC_WITH_PSRAM)
#error "ESC can't support Flash and PSRAM at the same time."
#endif

#if (!defined(HAL_ESC_WITH_FLASH)) && (!defined(HAL_ESC_WITH_PSRAM))
#error "Please Specify the device type (Flash or PSRAM) that is attached to the ESC."
#endif

#ifdef SUPPORT_52MHZ
#ifdef ASP_CHIP_1604M_SQ_PSRAM
#define ESC_DLY_CTL1_CYCLE       (0x0)
#define ESC_DLY_CTL2_CYCLE       (0x2)
#define ESC_DLY_CTL3_CYCLE       (0x4)
#define ESC_DLY_CTL4_CYCLE       (0x1)
#define ESC_DLY_CTL_INVERSE_MASK (0x0)
#elif defined(ESC_SFLASH)
#define ESC_DLY_CTL_INVERSE_MASK (0x100)
#define ESC_DLY_CTL1_CYCLE       (0x0)
#define ESC_DLY_CTL2_CYCLE       (0x1)
#define ESC_DLY_CTL3_CYCLE       (0x4)
#define ESC_DLY_CTL4_CYCLE       (0x0)
#endif
#else
//26Mhz use defualt value
#define ESC_DLY_CTL_INVERSE_MASK (0x100)
#define ESC_DLY_CTL1_CYCLE       (0x0)
#define ESC_DLY_CTL2_CYCLE       (0x0)
#define ESC_DLY_CTL3_CYCLE       (0x3)
#define ESC_DLY_CTL4_CYCLE       (ESC_DLY_CTL_INVERSE_MASK)
#endif

#endif /* HAL_ESC_MODULE_ENABLED */

#endif /* __HAL_ESC_CONFIG_H__ */

