/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __HAL_AUDIO_MESSAGE_STRUCT_H__
#define __HAL_AUDIO_MESSAGE_STRUCT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include "hal_audio.h"

//--------------------------------------------
// Message queue
//--------------------------------------------
#define AUDIO_MESSAGE_QUEUE_SIZE 8

typedef struct {
    uint16_t message16;
    uint16_t data16;
    uint32_t data32;
} audio_message_element_t;

typedef struct {
    uint32_t                read_index;
    uint32_t                write_index;
    audio_message_element_t message[AUDIO_MESSAGE_QUEUE_SIZE];
} audio_message_queue_t;

//--------------------------------------------
// Share buffer information
//--------------------------------------------
typedef enum {
    SHARE_BUFFER_INFO_INDEX_BT_AUDIO_DL,
    SHARE_BUFFER_INFO_INDEX_BT_VOICE_UL,
    SHARE_BUFFER_INFO_INDEX_BT_VOICE_DL,
#ifdef MTK_BT_CODEC_BLE_ENABLED
    SHARE_BUFFER_INFO_INDEX_BLE_AUDIO_UL,
    SHARE_BUFFER_INFO_INDEX_BLE_AUDIO_DL,
#endif
    SHARE_BUFFER_INFO_INDEX_PROMPT,
    SHARE_BUFFER_INFO_INDEX_RECORD,
    SHARE_BUFFER_INFO_INDEX_RINGTONE,
    SHARE_BUFFER_INFO_INDEX_NVKEY_PARAMETER,
    SHARE_BUFFER_INFO_INDEX_MAX
} n9_dsp_share_info_index_t;

// N9-DSP share information buffer (32 bytes)
typedef struct {
    uint32_t start_addr;       // start address of N9-DSP share buffer
    uint32_t read_offset;      // read pointer of N9-DSP share buffer
    uint32_t write_offset;     // write pointer of N9-DSP share buffer
    uint32_t next;             // next read position in buf for DSP
    uint32_t sampling_rate;    // for AWS clock skew
    uint16_t length;           // total length of N9-DSP share buffer
    uint8_t  bBufferIsFull;    // buffer full flag, when N9 find there is no free buffer for putting a packet,
                               // set this flag = 1, DSP will reset this flag when data be taken by DSP
    uint8_t  notify_count;     // notify count
    int32_t  drift_comp_val;   // long term drift compensation value
    uint32_t anchor_clk;       // long term drift anchor clk
    uint32_t asi_base;
    uint32_t asi_cur;
} n9_dsp_share_info_t;

typedef struct {
    uint8_t  aws_role;
    uint8_t  info_status;
    uint16_t cur_seqn;
    uint32_t cur_asi;
} n9_dsp_audio_sync_info_t;

typedef struct {
uint32_t StartAddr;       // start address of share buffer
uint16_t ReadIndex;  // read pointer of share buffer  : DSP monitor
uint16_t WriteIndex; // write pointer of share buffer : Controller monitor
uint32_t SampleRate; // sample rate for clock skew counting
uint16_t MemBlkSize; // block length for each frame
uint16_t MemBlkNum;  // number of block for frame usage
uint32_t DbgInfoAddr; // start address of controller packet address table
uint16_t FrameSampleNum;  // DSP notify audio
uint16_t codec_type;      // Codec information
uint16_t codec_config;    // Codec config information
uint16_t NotifyCount;  // notify count of DSP notify controller not to sleep
uint32_t ForwarderAddr; // forwarder buffer address
uint32_t SinkLatency; // a2dp sink latency
uint32_t reserved[1]; // reserved
} avm_share_buf_info_t,*avm_share_buf_info_ptr;
#define AVM_SHAEE_BUF_INFO

#define SHARE_BUFFER_BT_AUDIO_DL_SIZE      (40*1024)
#ifdef MTK_BT_A2DP_VENDOR_CODEC_BC_ENABLE
#define SHARE_BUFFER_VENDOR_CODEC_SIZE (7*1024)
#endif

#define SHARE_BUFFER_BT_VOICE_UL_SIZE      (480) /* 60*4 */
#define SHARE_BUFFER_BT_VOICE_DL_SIZE      (640) /* 60*8 + 20*8 */
#ifdef MTK_BT_CODEC_BLE_ENABLED
#define SHARE_BUFFER_BLE_AUDIO_UL_SIZE     (20*1024)
#define SHARE_BUFFER_BLE_AUDIO_DL_SIZE     (20*1024)
#endif
#define SHARE_BUFFER_PROMPT_SIZE           (4*1024)
#define SHARE_BUFFER_RECORD_SIZE           (2*1024)
#define SHARE_BUFFER_RINGTONE_SIZE         (2*1024)
#ifdef MTK_BT_SPEAKER_ENABLE
#define SHARE_BUFFER_FEC_SIZE              (10*700)
#endif
#define SHARE_BUFFER_MCU2DSP_PARAMETER_SIZE (1*1024)
#define SHARE_BUFFER_CLK_INFO_SIZE          (8)
#define SHARE_BUFFER_AIRDUMP_SIZE           (400)
#define SHARE_BUFFER_DSP2MCU_PARAMETER_SIZE (1*1024-SHARE_BUFFER_CLK_INFO_SIZE-SHARE_BUFFER_AIRDUMP_SIZE)
#if defined(MTK_INEAR_ENHANCEMENT) || defined(MTK_DUALMIC_INEAR)
#define SHARE_BUFFER_NVKEY_PARAMETER_SIZE   (3*1024+512)
#else
#define SHARE_BUFFER_NVKEY_PARAMETER_SIZE   (3*1024)
#endif
#define SHARE_BUFFER_AUDIO_SYNC_INFO_SIZE   (sizeof(n9_dsp_audio_sync_info_t))
#define SHARE_BUFFER_RX_AUDIO_FORWARDER_BUF_SIZE (760)
#define SHARE_BUFFER_TX_AUDIO_FORWARDER_BUF_SIZE (720)

typedef struct {
    uint32_t bt_audio_dl[(SHARE_BUFFER_BT_AUDIO_DL_SIZE+3)/4];
    uint32_t bt_voice_ul[(SHARE_BUFFER_BT_VOICE_UL_SIZE+3)/4];
    uint32_t bt_voice_dl[(SHARE_BUFFER_BT_VOICE_DL_SIZE+3)/4];
#ifdef MTK_BT_CODEC_BLE_ENABLED
    uint32_t ble_audio_ul[(SHARE_BUFFER_BLE_AUDIO_UL_SIZE+3)/4];
    uint32_t ble_audio_dl[(SHARE_BUFFER_BLE_AUDIO_DL_SIZE+3)/4];
#endif
    uint32_t prompt[(SHARE_BUFFER_PROMPT_SIZE+3)/4];
    uint32_t record[(SHARE_BUFFER_RECORD_SIZE+3)/4];
    uint32_t ringtone[(SHARE_BUFFER_RINGTONE_SIZE+3)/4];
    uint32_t mcu2dsp_param[(SHARE_BUFFER_MCU2DSP_PARAMETER_SIZE+3)/4];
    uint32_t dsp2mcu_param[(SHARE_BUFFER_DSP2MCU_PARAMETER_SIZE+3)/4];
    uint32_t clk_info[(SHARE_BUFFER_CLK_INFO_SIZE+3)/4];
    uint32_t airdump[(SHARE_BUFFER_AIRDUMP_SIZE+3)/4];
    uint32_t nvkey_param[(SHARE_BUFFER_NVKEY_PARAMETER_SIZE+3)/4];
    uint32_t audio_sync_info[(SHARE_BUFFER_AUDIO_SYNC_INFO_SIZE+3)/4];
    uint32_t rx_audio_forwarder_buf[(SHARE_BUFFER_RX_AUDIO_FORWARDER_BUF_SIZE+3)/4];
    uint32_t tx_audio_forwarder_buf[(SHARE_BUFFER_TX_AUDIO_FORWARDER_BUF_SIZE+3)/4];
} audio_share_buffer_t;

//--------------------------------------------
// Error report
//--------------------------------------------
typedef enum {
    DSP_ERROR_REPORT_ERROR,
    DSP_ERROR_REPORT_END,
} dsp_error_report_t;

//--------------------------------------------
// Codec type
//--------------------------------------------
typedef enum {
    AUDIO_DSP_CODEC_TYPE_CVSD = 0,
    AUDIO_DSP_CODEC_TYPE_MSBC,
    AUDIO_DSP_CODEC_TYPE_LC3,

    AUDIO_DSP_CODEC_TYPE_PCM = 0x100,
    AUDIO_DSP_CODEC_TYPE_SBC,
    AUDIO_DSP_CODEC_TYPE_MP3,
    AUDIO_DSP_CODEC_TYPE_AAC,
    AUDIO_DSP_CODEC_TYPE_VENDOR,
    AUDIO_DSP_CODEC_TYPE_OPUS,
    AUDIO_DSP_CODEC_TYPE_ANC_LC, //for leakage compensation
    AUDIO_DSP_CODEC_TYPE_ANC_USER_TRIGGER_FF,
    AUDIO_DSP_CODEC_TYPE_PCM_WWE,
} audio_dsp_codec_type_t;

typedef enum {
    WWE_MODE_NONE =    0,
    WWE_MODE_AMA =     1,
    WWE_MODE_GSOUND =   2,
    WWE_MODE_VENDOR1 = 3,
    WWE_MODE_MAX =     4
} wwe_mode_t;

typedef enum
{
    ENCODER_BITRATE_16KBPS  = 16,
    ENCODER_BITRATE_32KBPS  = 32,
    ENCODER_BITRATE_64KBPS  = 64,
    ENCODER_BITRATE_MAX     = 0xFFFFFFFF,
} encoder_bitrate_t;

//--------------------------------------------
// Start Parameters
//--------------------------------------------
typedef struct {
    uint32_t                    start_time_stamp;
    uint32_t                    time_stamp_ratio;
    uint32_t                    start_asi;
    uint32_t                    start_bt_clk;
    uint32_t                    start_bt_intra_clk;
    bool                        content_protection_exist;
    bool                        alc_enable;
    bool                        latency_monitor_enable;
} audio_dsp_a2dp_dl_start_param_t, *audio_dsp_a2dp_dl_start_param_p;

typedef struct {
    hal_audio_bits_per_sample_t bit_type;
    uint32_t                    sampling_rate;
    uint32_t                    channel_number;
} audio_dsp_file_pcm_param_t;

typedef struct {
    n9_dsp_share_info_t         *p_share_info;
    hal_audio_channel_number_t  channel_number;
    hal_audio_bits_per_sample_t bit_type;
    hal_audio_sampling_rate_t   sampling_rate;
    uint8_t                     codec_type;  //KH: should use audio_dsp_codec_type_t
} audio_dsp_playback_info_t, *audio_dsp_playback_info_p;

typedef struct {
    uint32_t                    header;
} audio_dsp_file_mp3_param_t;

typedef struct {
    uint32_t                    header;
} audio_dsp_file_aac_param_t;

typedef struct {
    uint32_t                    header;
} audio_dsp_file_vendor_param_t;

//===CM4 to DSP message structure==
/* Open message member parameter structure */
typedef enum {
    STREAM_IN_AFE  = 0,
    STREAM_IN_HFP,
    STREAM_IN_A2DP,
    STREAM_IN_PLAYBACK,
    STREAM_IN_VP,
    STREAM_IN_GSENSOR,
    STREAM_IN_DUMMY = 0xFFFFFFFF,
} mcu2dsp_stream_in_selection;

typedef enum {
    STREAM_OUT_AFE  = 0,
    STREAM_OUT_HFP,
    STREAM_OUT_RECORD,
    STREAM_OUT_GSENSOR,
    STREAM_OUT_VIRTUAL,
    STREAM_OUT_DATA_UL,
    STREAM_OUT_DUMMY = 0xFFFFFFFF,
} mcu2dsp_stream_out_selection;

typedef struct {
    mcu2dsp_stream_in_selection     stream_in;
    mcu2dsp_stream_out_selection    stream_out;
    uint32_t                        *Feature;
}  mcu2dsp_param_t, *mcu2dsp_param_p;

#ifdef ENABLE_HWSRC_CLKSKEW
//--------------------------------------------
// CLK SKEW Mode
//--------------------------------------------
typedef enum {
    CLK_SKEW_V1 = 0, /* sw clk skew */
    CLK_SKEW_V2 = 1, /* hwsrc clk skew */
    CLK_SKEW_DUMMY = 0xFFFFFFFF,
} clkskew_mode_t;
#endif

typedef struct {
    hal_audio_device_t               audio_device;
    hal_audio_device_t               audio_device1;
    hal_audio_device_t               audio_device2;
    hal_audio_device_t               audio_device3;
    hal_audio_channel_selection_t    stream_channel;
    hal_audio_memory_t                      memory;
    hal_audio_interface_t                   audio_interface;
#ifdef ENABLE_2A2D_TEST
    hal_audio_interface_t                   audio_interface1;
    hal_audio_interface_t                   audio_interface2;
    hal_audio_interface_t                   audio_interface3;
#endif
    hal_audio_format_t                      format;
    uint32_t                                misc_parms;
    uint32_t                                sampling_rate;
    uint32_t                                stream_out_sampling_rate;
    uint16_t                                frame_size;
    uint8_t                                 frame_number;
    uint8_t                                 irq_period;
    uint8_t                                 sw_channels;
    bool                                    hw_gain;
    hal_audio_analog_mdoe_t                 adc_mode;
    hal_audio_performance_mode_t            performance;
#ifdef ENABLE_HWSRC_CLKSKEW
    clkskew_mode_t                          clkskew_mode;
#endif
} au_afe_open_param_t,*au_afe_open_param_p;

typedef struct {
    uint8_t param[20];//TEMP!! align bt_codec_a2dp_audio_t
} audio_dsp_a2DP_codec_param_t;

typedef struct
{
    uint8_t enable;
} dsp_audio_plc_ctrl_t, *dsp_audio_plc_ctrl_p;

typedef struct {
    audio_dsp_a2DP_codec_param_t    codec_info;
    #ifdef AVM_SHAEE_BUF_INFO
    avm_share_buf_info_t            *p_share_info;
    #else
    n9_dsp_share_info_t             *p_share_info;
    #endif
    uint32_t                        *p_asi_buf;
    uint32_t                        *p_min_gap_buf;
    uint32_t                        *p_current_bit_rate;
    uint32_t                        sink_latency;
    uint32_t                        bt_inf_address;
    #ifdef AVM_SHAEE_BUF_INFO
    uint32_t                        *p_afe_buf_report;
	#else
    uint32_t                        *clk_info_address;
    #endif
    uint32_t                        *p_lostnum_report;
    n9_dsp_audio_sync_info_t        *p_audio_sync_info;
    uint32_t                        *p_pcdc_anchor_info_buf;
#ifdef MTK_AUDIO_PLC_ENABLE
    dsp_audio_plc_ctrl_t            audio_plc;
#endif
} audio_dsp_a2dp_dl_open_param_t, *audio_dsp_a2dp_dl_open_param_p;

typedef uint8_t audio_dsp_hfp_codec_param_t;

typedef struct {
    audio_dsp_hfp_codec_param_t     codec_type;
    #ifdef AVM_SHAEE_BUF_INFO
    avm_share_buf_info_t            *p_share_info;
    #else
    n9_dsp_share_info_t             *p_share_info;
    #endif
    uint32_t                        bt_inf_address;
    uint32_t                        *clk_info_address;
    uint32_t                        *p_air_dump_buf;
    uint32_t                        *p_rx_audio_forwarder_buf;
    uint32_t                        *p_tx_audio_forwarder_buf;
} audio_dsp_hfp_open_param_t, *audio_dsp_hfp_open_param_p;

typedef uint8_t audio_dsp_ble_codec_param_t;

typedef struct {
    audio_dsp_ble_codec_param_t     codec_type;
    n9_dsp_share_info_t             *p_share_info;
    uint32_t                        bt_inf_address;
    uint32_t                        *clk_info_address;
    uint32_t                        *p_air_dump_buf;
} audio_dsp_ble_open_param_t, *audio_dsp_ble_open_param_p;

typedef struct {
    n9_dsp_share_info_t *p_share_info;
    uint32_t frames_per_message;
    encoder_bitrate_t bitrate;
    bool interleave;
} cm4_record_open_param_t, *cm4_record_open_param_p;

typedef union {
    au_afe_open_param_t             afe;
    audio_dsp_hfp_open_param_t      hfp;
    audio_dsp_ble_open_param_t      ble;
    audio_dsp_a2dp_dl_open_param_t  a2dp;
    audio_dsp_playback_info_t       playback;
} mcu2dsp_open_stream_in_param_t, *mcu2dsp_open_stream_in_param_p;

typedef struct {
    ;
} audio_dsp_multi_mic_open_param_t, *audio_dsp_multi_mic_open_param_p;

typedef struct{
    n9_dsp_share_info_t *p_share_info;
    uint16_t payload_size;
    uint16_t data_notification_frequency;
    uint16_t data_format;
    union{
        audio_dsp_multi_mic_open_param_t  a2dp_source;
    };
}audio_dsp_data_uplink_open_param_t;

typedef union {
    au_afe_open_param_t             afe;
    audio_dsp_hfp_open_param_t      hfp;
    audio_dsp_ble_open_param_t      ble;
    cm4_record_open_param_t         record;
    audio_dsp_data_uplink_open_param_t   data_uplink;
} mcu2dsp_open_stream_out_param_t, *mcu2dsp_open_stream_out_param_p;

/* Open message parameter structure */
typedef struct {
    mcu2dsp_param_t                 param;
    mcu2dsp_open_stream_in_param_t  stream_in_param;
    mcu2dsp_open_stream_out_param_t stream_out_param;
} mcu2dsp_open_param_t, *mcu2dsp_open_param_p;


/* Start message member parameter structure */
typedef struct {
    bool                        aws_flag;
    bool                        aws_sync_request;
    uint32_t                    aws_sync_time;
} audio_dsp_afe_start_param_t, *audio_dsp_afe_start_param_p;

typedef union {
    audio_dsp_a2dp_dl_start_param_t a2dp;
    audio_dsp_afe_start_param_t     afe;
} mcu2dsp_start_stream_in_param_t, *mcu2dsp_start_stream_in_param_p;

typedef union {
    audio_dsp_afe_start_param_t     afe;
} mcu2dsp_start_stream_out_param_t, *mcu2dsp_start_stream_out_param_p;

/* Start message parameter structure */
typedef struct {
    mcu2dsp_param_t                     param;
    mcu2dsp_start_stream_in_param_t     stream_in_param;
    mcu2dsp_start_stream_out_param_t    stream_out_param;
} mcu2dsp_start_param_t, *mcu2dsp_start_param_p;

/* SideTone message parameter structure */
typedef struct {
    hal_audio_device_t                      in_device;
    hal_audio_interface_t                   in_interface;
    uint32_t                                in_misc_parms;
    hal_audio_device_t                      out_device;
    hal_audio_interface_t                   out_interface;
    uint32_t                                out_misc_parms;
    hal_audio_channel_selection_t           in_channel; /*HW out channel default R+L*/
    uint32_t                                gain;
    uint32_t                                sample_rate;
} mcu2dsp_sidetone_param_t, *mcu2dsp_sidetone_param_p;

#if defined(MTK_PEQ_ENABLE) || defined(MTK_LINEIN_PEQ_ENABLE)
#define PEQ_DIRECT      (0)
#define PEQ_SYNC        (1)
typedef struct {
    uint8_t         *nvkey_addr;
    uint16_t        peq_nvkey_id;
    uint8_t         drc_enable;
    uint8_t         setting_mode;
    uint32_t        target_bt_clk;
    uint8_t         phase_id;
    uint8_t         drc_force_disable;
} mcu2dsp_peq_param_t, *mcu2dsp_peq_param_p;
#endif


typedef struct {
    uint8_t  ENABLE;             /**< @Value   0x01 @Desc 1 */
    uint8_t  REVISION;           /**< @Value   0x01 @Desc 1 */
    uint16_t WWE_MODE;           /**< @Value 0x0000 @Desc 1 */
    uint16_t skip_frame_num ;    /**< @Value 0x0000 @Desc 1 */
    uint16_t noisy_thr_h ;       /**< @Value 0x0000 @Desc 1 */
    uint16_t noisy_thr_l ;       /**< @Value 0x0000 @Desc 1 */
    uint16_t noisy_debounce_cnt; /**< @Value 0x0000 @Desc 1 */
    uint16_t silent_thr_h ;      /**< @Value 0x0000 @Desc 1 */
    uint16_t silent_thr_l ;      /**< @Value 0x0000 @Desc 1 */
    uint16_t silent_debounce_cnt;/**< @Value 0x0000 @Desc 1 */
    uint16_t vit_pre ;           /**< @Value 0x0000 @Desc 1 */
    uint16_t RESERVE_1 ;         /**< @Value 0x0000 @Desc 1 */
    uint16_t RESERVE_2 ;         /**< @Value 0x0000 @Desc 1 */
    uint16_t RESERVE_3 ;         /**< @Value 0x0000 @Desc 1 */
    uint16_t RESERVE_4 ;         /**< @Value 0x0000 @Desc 1 */
} DSP_NVKEY_VAD_COMM;

typedef struct {
    int16_t en_vad;                /**< @Value 0x0000 @Desc 1 */
    int16_t gamma;                 /**< @Value 0x0000 @Desc 1 */
    int16_t diff_fac;              /**< @Value 0x0000 @Desc 1 */
    int16_t diff_facb;             /**< @Value 0x0000 @Desc 1 */
    int16_t vad_fac;               /**< @Value 0x0000 @Desc 1 */
    int16_t vad_ph_cc;             /**< @Value 0x0000 @Desc 1 */
    int16_t vad_ph_init;           /**< @Value 0x0000 @Desc 1 */
    int16_t vad_ph_range;          /**< @Value 0x0000 @Desc 1 */
    int16_t vad_corr_th;           /**< @Value 0x0000 @Desc 1 */
    int16_t vad_pitch_th;          /**< @Value 0x0000 @Desc 1 */
    int16_t win_sm_1st;            /**< @Value 0x0000 @Desc 1 */
    int16_t sil_thr   ;            /**< @Value 0x0000 @Desc 1 */
    int16_t sil_thr2  ;            /**< @Value 0x0000 @Desc 1 */
    int16_t fr_thr    ;            /**< @Value 0x0000 @Desc 1 */
    int16_t fr_thr2   ;            /**< @Value 0x0000 @Desc 1 */
    int16_t rec_thr   ;            /**< @Value 0x0000 @Desc 1 */
    int16_t rec_thr2  ;            /**< @Value 0x0000 @Desc 1 */
    int16_t end_thr   ;            /**< @Value 0x0000 @Desc 1 */
    int16_t gar_thr   ;            /**< @Value 0x0000 @Desc 1 */
    int16_t rel_thr   ;            /**< @Value 0x0000 @Desc 1 */
    int16_t ss_thr    ;            /**< @Value 0x0000 @Desc 1 */
    int16_t ms_thr    ;            /**< @Value 0x0000 @Desc 1 */
    int16_t ss_rel_thr;            /**< @Value 0x0000 @Desc 1 */
    int16_t ss_g_thr  ;            /**< @Value 0x0000 @Desc 1 */
    int16_t fa_end_thr;            /**< @Value 0x0000 @Desc 1 */
    int16_t fa_beg_thr;            /**< @Value 0x0000 @Desc 1 */
    int16_t short_thr ;            /**< @Value 0x0000 @Desc 1 */
    int16_t rel_st4_thr;           /**< @Value 0x0000 @Desc 1 */
    int16_t rel_oth_thr;           /**< @Value 0x0000 @Desc 1 */
    int16_t dur_thr   ;            /**< @Value 0x0000 @Desc 1 */
    int16_t max_dur_thr;           /**< @Value 0x0000 @Desc 1 */
    int16_t ms_s_thr  ;            /**< @Value 0x0000 @Desc 1 */
    int16_t vad_leave_th;          /**< @Value 0x0000 @Desc 1 */
    int16_t vad_eng_thr;           /**< @Value 0x0000 @Desc 1 */
    uint16_t RESERVE_A ;            /**< @Value 0x0000 @Desc 1 */
    uint16_t RESERVE_B ;            /**< @Value 0x0000 @Desc 1 */
    uint16_t RESERVE_C ;            /**< @Value 0x0000 @Desc 1 */
    uint16_t RESERVE_D ;            /**< @Value 0x0000 @Desc 1 */
} DSP_NVKEY_VAD_PARA;

typedef struct
{
    uint8_t snr_threshold;     /*NVkey_0 0x01:0x0303 0x02:0x4343 0x03:0x7373 */
    uint8_t noise_ignore_bits; /*NVkey_1 0x01:0xFFFF0000 0x02:0xFFF00000 0x03:0xFF000000 0x04:0xF0000000 */
    uint8_t alpha_rise;        /*NVkey_3 0x01 - 0x0F */
    uint8_t enable;            /*NVkey_4 0x00:diable 0x01:enable*/
    uint8_t reserved[4];       /*reserve*/
}DSP_NVKEY_VOW_PARA;

typedef struct {
    DSP_NVKEY_VAD_COMM vad_nvkey_common;
    DSP_NVKEY_VAD_PARA vad_nvkey_1mic_v_mode;
    DSP_NVKEY_VAD_PARA vad_nvkey_1mic_c_mode;
    DSP_NVKEY_VAD_PARA vad_nvkey_2mic_v_mode;
    DSP_NVKEY_VAD_PARA vad_nvkey_2mic_c_mode;
    uint32_t language_mode_address;
    uint32_t language_mode_length;
    DSP_NVKEY_VOW_PARA vow_setting;
} mcu2dsp_vad_param_t, *mcu2dsp_vad_param_p;

//--------------------------------------------
// Config structure
//--------------------------------------------
typedef enum {
    AUDIO_PLAYBACK_CONFIG_EOF
} audio_playback_config_index_t;

//--------------------------------------------
// Audio Sync Parameters
//--------------------------------------------
typedef struct {
    uint32_t                    time_stamp;
    uint32_t                    sample;
} audio_dsp_a2dp_dl_time_param_t;

//--------------------------------------------
// A2DP LTCS data
//--------------------------------------------
typedef struct {
    uint32_t *p_ltcs_asi_buf;
    uint32_t *p_ltcs_min_gap_buf;
} audio_dsp_a2dp_ltcs_report_param_t;

typedef struct {
    int32_t drift_comp_val; // long term drift compensation value
    uint32_t anchor_clk;    // long term drift anchor clk
    uint32_t asi_base;      // 1st time is play_en asi, later is anchor asi
    uint32_t asi_cur;   // asi base for current play
} ltcs_anchor_info_t;

//--------------------------------------------
// Leakage compensation data
//--------------------------------------------
typedef void (*anc_leakage_compensation_callback_t)(uint16_t leakage_status);

typedef struct {
    uint16_t calibration_status;
    uint16_t wz_set;
    anc_leakage_compensation_callback_t api_callback;
    uint8_t enable;
    uint8_t anc_enable;
    uint32_t anc_runtime_info;
} audio_dsp_leakage_compensation_report_param_t;

#ifdef __cplusplus
}
#endif

#endif /*__HAL_AUDIO_MESSAGE_STRUCT_H__ */
