/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_spm.h"

#ifdef HAL_SLEEP_MANAGER_ENABLED

#include <stdio.h>
#include <string.h>
#include "hal_log.h"
#include "memory_attribute.h"
#include "hal_nvic.h"
#include "hal_nvic_internal.h"
#include "assert.h"
#include "hal_gpt.h"
#include "hal_platform.h"
#include "hal_wdt_internal.h"
#include "hal_flash.h"
#include "hal_flash_cfi_internal.h"
#include "hal_flash_cfi.h"
#include "syslog.h"

log_create_module(SPM, PRINT_LEVEL_INFO);
#define SPM_MCE_MSGID_I(fmt,cnt,arg...)        LOG_MSGID_I(SPM,fmt,cnt,##arg)
#define SPM_MCE_MSGID_W(fmt,cnt,arg...)        LOG_MSGID_W(SPM,fmt,cnt,##arg)
#define SPM_MCE_MSGID_E(fmt,cnt,arg...)        LOG_MSGID_E(SPM,fmt,cnt,##arg)

/* SPM Code Rev : 20180320 - pcm_suspend_v57.spm */
const unsigned int pcm_event_vector_parameter[8] = {0x31,0x910033,0x130035,0x600030,0x1a0034,0x0,0x0,0x0};

#define PCM_IM_MAX_LENGTH 951

static const unsigned int spm_im[PCM_IM_MAX_LENGTH] = {
0x18c0001f,0xa2120810,0xe0e0004f,0x18c0001f,0xa21203a0,0xe0e00000,0x18c0001f,0xa21203a4,0xe0e00001,
0x1940001f,0xa2120184,0xe1600000,0xe8208000,0xa2120300,0x0b16ff08,0x1b00001f,0x00090001,0xf0000000,
0x17c07c1f,0x18c0001f,0xa2120810,0xe0e0003d,0x1b00001f,0x00800001,0xf0000000,0x17c07c1f,0x18c0001f,
0xa2120810,0xe0e00031,0x1910001f,0xa21203b0,0x81081001,0x18d0001f,0xa21203e0,0x80c00c01,0xa1000c04,
0xc8c00b84,0x17c07c1f,0xc8e00544,0x17c07c1f,0xf0000000,0x17c07c1f,0x1a10001f,0xa21203ac,0x82042001,
0xd8000988,0x17c07c1f,0x18c0001f,0xa21203a4,0xe0e00001,0x1940001f,0xa2290018,0x1900001f,0x00010001,
0xe1400004,0x1940001f,0xa2290020,0xe1600001,0x1910001f,0xa2290018,0x1950001f,0xa2290020,0x818c1001,
0x81001001,0x81441401,0x81001404,0x81001804,0xd8200744,0x17c07c1f,0x18c0001f,0xa2120810,0xe0e00032,
0x18c0001f,0xa2120204,0x1940001f,0x00000001,0xc8e02d08,0x17c07c1f,0x1940001f,0xa2120cb4,0x92202001,
0xe1400008,0x1910001f,0xa2120b00,0x91201001,0xa0121000,0xa0111000,0xa0109000,0x1b00001f,0x00090001,
0xf0000000,0x17c07c1f,0x1b00001f,0x00200001,0xf0000000,0x17c07c1f,0x18c0001f,0xa2120810,0xe0e00041,
0x1940001f,0xa2120184,0xe1600001,0x1990001f,0xa2120368,0x81841801,0x1910001f,0xa21203b0,0x810c1001,
0xa1001804,0xc8c00e64,0x17c07c1f,0xc8e00fa4,0x17c07c1f,0xf0000000,0x17c07c1f,0x18c0001f,0xa2120810,
0xe0e00044,0x1940001f,0xa2120184,0xe1600000,0x1b00001f,0x00090001,0xf0000000,0x17c07c1f,0x18c0001f,
0xa21203a4,0xe0e00000,0x1910001f,0xa2120b00,0x91201001,0xa0121000,0x1b00001f,0x00020001,0x18c0001f,
0xa21203a0,0xe0e00001,0xe8208000,0xa2120300,0x0b16ff0c,0xe8208000,0xa2120300,0x0b16ff08,0xf0000000,
0x17c07c1f,0x18c0001f,0xa2120810,0xe0e0003f,0x80308400,0x80310400,0x1b80001f,0x200005a7,0x80320400,
0x18c0001f,0xa2120810,0xe0e0003e,0x18c0001f,0xa2120204,0x1940001f,0x00000001,0x1910001f,0xa2120808,
0x81009001,0xc8e017e4,0x17c07c1f,0x1940001f,0xa2290018,0x1900001f,0x00000000,0xe1400004,0x1940001f,
0xa2290020,0xe1600000,0x1910001f,0xa2290018,0x1950001f,0xa2290020,0x818c1001,0x81001001,0x81441401,
0xa1001404,0xa1001804,0xd80015a4,0x17c07c1f,0x18c0001f,0xa21203a4,0xe0e00101,0x1b00001f,0x00300001,
0xf0000000,0x17c07c1f,0xe0e00016,0x1b80001f,0x00000001,0xe0e0001e,0x1b80001f,0x00000001,0x81801401,
0xd8001a66,0x17c07c1f,0x81809401,0xd8001ee6,0x17c07c1f,0x81811401,0xd80021a6,0x17c07c1f,0x81819401,
0xd80027c6,0x17c07c1f,0xd0002be0,0x17c07c1f,0x1900001f,0xa2120254,0x1980001f,0x01000100,0xe1000006,
0x1b80001f,0x20000034,0x1980001f,0x01010100,0xe1000006,0x1b80001f,0x20000003,0x1980001f,0x00010100,
0xe1000006,0x1900001f,0xa2120264,0x1980001f,0x01000100,0xe1000006,0x1b80001f,0x20000034,0x1980001f,
0x01010100,0xe1000006,0x1b80001f,0x20000003,0x1980001f,0x00010100,0xe1000006,0x1900001f,0xa21202b0,
0xe1200001,0xe1200000,0xd0002be0,0x17c07c1f,0x1900001f,0xa2120230,0xe1200301,0xe1200001,0xe1200000,
0x1900001f,0xa2120224,0x1980001f,0x01000100,0xe1000006,0x1b80001f,0x2000001a,0x1980001f,0x01010100,
0xe1000006,0x1b80001f,0x20000003,0x1980001f,0x00010100,0xe1000006,0xd0002be0,0x17c07c1f,0x1900001f,
0xa2120440,0x1940001f,0x00000fff,0xe1000005,0xe12000ff,0xe120007f,0xe120003f,0xe120001f,0xe120000f,
0xe1200007,0xe1200003,0xe1200001,0xe1200000,0x1900001f,0xa2120424,0x1980001f,0x03000102,0xe1000006,
0x1b80001f,0x20000010,0x1980001f,0x03000300,0xe1000006,0x1b80001f,0x20000010,0x1940001f,0xa2120434,
0x1980001f,0x03000102,0xe1400006,0x1b80001f,0x2000000a,0x1980001f,0x03000300,0xe1400006,0x1b80001f,
0x2000000a,0x1b80001f,0x20000018,0x1a00001f,0xa2120450,0xe2200001,0x1980001f,0x00010300,0xe1000006,
0xe1400006,0xd0002be0,0x17c07c1f,0x1900001f,0xa2120524,0x1980001f,0x03000102,0xe1000006,0x1b80001f,
0x2000000a,0x1980001f,0x03000300,0xe1000006,0x1b80001f,0x2000000a,0x1940001f,0xa2120534,0x1980001f,
0x03000102,0xe1400006,0x1b80001f,0x20000004,0x1980001f,0x03000300,0xe1400006,0x1b80001f,0x20000004,
0x1b80001f,0x20000018,0x1a00001f,0xa2120550,0xe2200001,0x1980001f,0x00010300,0xe1000006,0xe1400006,
0xe0e0000e,0xe0e0000c,0xe0e0001c,0x1b80001f,0x20000004,0xe0e0001d,0xe0e0000d,0xf0000000,0x17c07c1f,
0xe0e0001d,0xe0e0001f,0x81801401,0xd8002f06,0x17c07c1f,0x81809401,0xd8003386,0x17c07c1f,0x81811401,
0xd8003646,0x17c07c1f,0x81819401,0xd8003b66,0x17c07c1f,0xd0003f40,0x17c07c1f,0x1900001f,0xa2120254,
0x1980001f,0x01010100,0xe1000006,0x1980001f,0x01000100,0xe1000006,0x1b80001f,0x20000004,0x1980001f,
0x01000001,0xe1000006,0x1b80001f,0x2000000e,0x1900001f,0xa2120264,0x1980001f,0x01010100,0xe1000006,
0x1980001f,0x01000100,0xe1000006,0x1b80001f,0x20000004,0x1980001f,0x01000001,0xe1000006,0x1b80001f,
0x2000000e,0x1900001f,0xa21202b0,0xe1200001,0xe1200101,0xd0003f40,0x17c07c1f,0x1900001f,0xa2120230,
0xe1200300,0xe1200f00,0xe1200f01,0x1900001f,0xa2120224,0x1980001f,0x01010100,0xe1000006,0x1980001f,
0x01000100,0xe1000006,0x1b80001f,0x20000004,0x1980001f,0x01000001,0xe1000006,0x1b80001f,0x2000000e,
0xd0003f40,0x17c07c1f,0x1900001f,0xa2120424,0x1940001f,0xa2120434,0x1980001f,0x03000300,0xe1000006,
0xe1400006,0x1a00001f,0xa2120450,0xe2200000,0x1980001f,0x03000201,0xe1000006,0x1b80001f,0x20000010,
0x1980001f,0x03000003,0xe1000006,0x1b80001f,0x20000010,0x1980001f,0x03000201,0xe1400006,0x1b80001f,
0x2000000a,0x1980001f,0x03000003,0xe1400006,0x1b80001f,0x2000000a,0x1900001f,0xa2120440,0xe120000f,
0xe12000ff,0xe1200fff,0x1940001f,0x0000ffff,0xe1000005,0xd0003f40,0x17c07c1f,0x1900001f,0xa2120524,
0x1940001f,0xa2120534,0x1980001f,0x03000300,0xe1000006,0xe1400006,0x1a00001f,0xa2120550,0xe2200000,
0x1980001f,0x03000201,0xe1000006,0x1b80001f,0x2000000a,0x1980001f,0x03000003,0xe1000006,0x1b80001f,
0x2000000a,0x1980001f,0x03000201,0xe1400006,0x1b80001f,0x20000004,0x1980001f,0x03000003,0xe1400006,
0x1b80001f,0x20000004,0xe0e0001e,0xe0e0001a,0xe0e00012,0xf0000000,0x17c07c1f,0x17c07c1f,0x1840001f,
0x00000001,0x18c0001f,0xa2120810,0xe0e00001,0x81f08407,0xa1d00407,0x18c0001f,0xa2120380,0xe0f07fff,
0xe8208000,0xa2120300,0x0b16ff03,0x18c0001f,0xa2120810,0xe0e00017,0x1b00001f,0x07400001,0x1b80001f,
0xd0100001,0x18d0001f,0xa2120930,0x80cd0c01,0xd8006123,0x17c07c1f,0x1b00001f,0x07400001,0x18d0001f,
0xa2120930,0x80cc8c01,0xd8005b83,0x17c07c1f,0x1b00001f,0x07400001,0x18d0001f,0xa2120930,0x80cc0c01,
0xd8005723,0x17c07c1f,0x1b00001f,0x07400001,0x18d0001f,0xa2120930,0x80cb0c01,0x1910001f,0xa21203b0,
0x81001001,0x80e01003,0xd82041a3,0x17c07c1f,0x1910001f,0xa2120c00,0x81001001,0xd82049c4,0x17c07c1f,
0x1900001f,0xa2120810,0xe1200025,0x18d0001f,0xa2120830,0x80ca8c01,0xd80041a3,0x17c07c1f,0x18d0001f,
0xa0020044,0x80c28c01,0xd82046e3,0x17c07c1f,0x1900001f,0xa2120810,0xe1200026,0x1910001f,0xa2120c00,
0x89000004,0xfffffffe,0x18c0001f,0xa2120c00,0xe0c00004,0x18c0001f,0xa2120810,0xe0e00021,0x18c0001f,
0xa21201d0,0xe0e00000,0x18d0001f,0xa21201d4,0x81000c01,0x80c40c01,0xd8004e83,0x17c07c1f,0x18c0001f,
0xa2120810,0xe0e00022,0x18c0001f,0xa0040800,0xe0e000b9,0x18c0001f,0xa0040010,0xe0e00001,0x18c0001f,
0xa0040014,0xe0e00000,0x18c0001f,0xa0040000,0xe0e0000c,0x18d0001f,0xa0040000,0x80c00c01,0xd8004d23,
0x17c07c1f,0x18c0001f,0xa0040000,0xe0e00000,0xa0128400,0x1b80001f,0x20000398,0xd8005484,0x17c07c1f,
0x18c0001f,0xa2120810,0xe0e00023,0x18d0001f,0xa0050010,0x1900001f,0xa21201a0,0xe1000003,0x18d0001f,
0xa0050080,0x1900001f,0xa21201a4,0xe1000003,0x18d0001f,0xa00500b0,0x10c40c1f,0x1900001f,0xa21201b0,
0xe1000003,0x18d0001f,0xa00500c0,0x10c40c1f,0x1900001f,0xa21201b4,0xe1000003,0x18d0001f,0xa00500d0,
0x10c40c1f,0x1900001f,0xa21201b8,0xe1000003,0x18c0001f,0xa00500a0,0x1900001f,0xf0670000,0xe0c00004,
0x18d0001f,0xa0050070,0x80c80c01,0xd8205343,0x17c07c1f,0x18c0001f,0xa2270310,0x1900001f,0x00080000,
0xe0c00004,0x1b00001f,0x00300001,0x1b80001f,0xd0100001,0x1b80001f,0x90100001,0x18d0001f,0xa2120808,
0x81008c01,0x1950001f,0xa2120830,0x814a9401,0x81801404,0xd82054c6,0x17c07c1f,0xd00066c0,0x17c07c1f,
0x18c0001f,0xa2120810,0xe0e000ff,0xf0000000,0x18c0001f,0xa2120810,0xe0e00011,0x18c0001f,0xa2120200,
0x1940001f,0x00000002,0x1910001f,0xa2120808,0x81001001,0xd82059e4,0x17c07c1f,0x1990001f,0xa2120800,
0x81881801,0xd8205a86,0x17c07c1f,0x81f10407,0xc8c02d04,0x17c07c1f,0xd00044e0,0x17c07c1f,0xc8e017e4,
0x17c07c1f,0xa1d10407,0xd00044e0,0x17c07c1f,0x1a00001f,0xa2120b90,0xe2200001,0x1a00001f,0xa2120c80,
0xe2200100,0xd00044e0,0x17c07c1f,0x18c0001f,0xa2120810,0xe0e00012,0x18c0001f,0xa2120400,0x1940001f,
0x00000004,0x1910001f,0xa2120808,0x81021001,0xd8205f44,0x17c07c1f,0x1980001f,0xa2280018,0xe1a00001,
0x1980001f,0xa229001c,0xe1a00001,0x1910001f,0xa229001c,0x81041001,0xd8205dc4,0x17c07c1f,0xc0c02d00,
0x17c07c1f,0x1a00001f,0xa2120c84,0xe2200001,0xd0004400,0x17c07c1f,0xc0c017e0,0x17c07c1f,0x1980001f,
0xa229001c,0xe1a00000,0x1910001f,0xa229001c,0x81041001,0xd8005fe4,0x17c07c1f,0x1980001f,0xa2280018,
0xe1a00000,0xd0004400,0x17c07c1f,0x18c0001f,0xa2120810,0xe0e00013,0x18c0001f,0xa2120500,0x1940001f,
0x00000008,0x1910001f,0xa2120808,0x81029001,0xd82064e4,0x17c07c1f,0x1980001f,0xa2280038,0xe1a00001,
0x1980001f,0xa2290024,0xe1a00001,0x1910001f,0xa2290024,0x81041001,0xd8206364,0x17c07c1f,0xc0c02d00,
0x17c07c1f,0x1a00001f,0xa2120c88,0xe2200001,0xd0004320,0x17c07c1f,0xc0c017e0,0x17c07c1f,0x1980001f,
0xa2290024,0xe1a00000,0x1910001f,0xa2290024,0x81041001,0xd8006584,0x17c07c1f,0x1980001f,0xa2280038,
0xe1a00000,0xd0004320,0x17c07c1f,0x18d0001f,0xa21201d4,0x81000c01,0x80c40c01,0xd8006ae3,0x17c07c1f,
0x18c0001f,0xa2120810,0xe0e0002f,0x80328400,0x18c0001f,0xa0040800,0xe0e000ab,0x18c0001f,0xa0040010,
0xe0e00001,0x18c0001f,0xa0040014,0xe0e00000,0x18c0001f,0xa0040000,0xe0e00008,0xe0e0000c,0x18d0001f,
0xa0040000,0x80c00c01,0xd80069a3,0x17c07c1f,0x18c0001f,0xa0040000,0xe0e00000,0x1b80001f,0x200005a7,
0xd80075e4,0x17c07c1f,0x18c0001f,0xa2120810,0xe0e0002e,0x18c0001f,0xa2270320,0x1900001f,0x00080000,
0xe0c00004,0x18d0001f,0xa21201a0,0x1900001f,0xa0050010,0xe1000003,0x18d0001f,0xa21201a4,0x1900001f,
0xa0050080,0xe1000003,0x18c0001f,0xa00500b0,0x1950001f,0xa21201b0,0x1154141f,0x09400005,0x01010000,
0xe0c00005,0x09400005,0x00000001,0xe0c00005,0x18c0001f,0xa00500c0,0x1950001f,0xa21201b4,0x1154141f,
0x09400005,0x00010000,0xe0c00005,0x09400005,0x00000001,0xe0c00005,0x18c0001f,0xa00500d0,0x1950001f,
0xa21201b8,0x1154141f,0x09400005,0x00010000,0xe0c00005,0x09400005,0x00000001,0xe0c00005,0x18c0001f,
0xa0050070,0xe0e00001,0x18d0001f,0xa0050070,0x81040c01,0x81480c01,0x80e01404,0xd82071e3,0x17c07c1f,
0x1b80001f,0x2000028a,0x1b80001f,0x20000a50,0x18c0001f,0xa0050070,0xe0e00000,0x18c0001f,0xa0050030,
0x1900001f,0x000f0003,0xe0c00004,0x18d0001f,0xa0050034,0x80c00c01,0xd8007443,0x17c07c1f,0x18c0001f,
0xa0050030,0x1900001f,0x000f0007,0xe0c00004,0x1900001f,0x000f0001,0xe0c00004,0x18c0001f,0xa2120810,
0xe0e0002d,0x18c0001f,0xa21201d0,0xe0e00101,0xd00041a0,0x17c07c1f};

extern bool chip_is_ab1552(void);

static struct{
    uint32_t conn;
    uint32_t audio;
    uint32_t dsp0;
    uint32_t dsp1;
}mtcmos_resource;

uint32_t manual_spm_read_im(uint32_t addr);
void manual_spm_write_im(uint32_t addr, uint32_t data);
extern void NOR_ReadID(const uint16_t CS, volatile FLASH_CELL *BaseAddr, uint16_t *flashid);

int spm_init(uint32_t spm_auto_load)
{
    unsigned int system_info;
    int result = 0;
    uint32_t i, status;
    uint16_t flash_id[4];

    /* [4]: Resets PCM */
    *SPM_PCM_CON0 = 0x0B160000 | 0x10;
    hal_gpt_delay_us(10);
    *SPM_PCM_CON0 = 0x0B160000;
    hal_gpt_delay_us(10);

    /* Load PCM_EVENT_VECTOR[0-7] */
    *SPM_PCM_EVENT_VECTOR0 = pcm_event_vector_parameter[0];
    *SPM_PCM_EVENT_VECTOR1 = pcm_event_vector_parameter[1];
    *SPM_PCM_EVENT_VECTOR2 = pcm_event_vector_parameter[2];
    *SPM_PCM_EVENT_VECTOR3 = pcm_event_vector_parameter[3];
    *SPM_PCM_EVENT_VECTOR4 = pcm_event_vector_parameter[4];
    *SPM_PCM_EVENT_VECTOR5 = pcm_event_vector_parameter[5];
    *SPM_PCM_EVENT_VECTOR6 = pcm_event_vector_parameter[6];
    *SPM_PCM_EVENT_VECTOR7 = pcm_event_vector_parameter[7];

    *((volatile uint32_t*)(SPM_BASE + 0x0010)) = 0;             /* POWER_ON_VAL0 */
    *((volatile uint32_t*)(SPM_BASE + 0x0014)) = 0x84;          /* POWER_ON_VAL1 */
    /* [0]: sc_xo_cg_en default =1 */
    *((volatile uint32_t*)(SPM_BASE + 0x0338)) = *((volatile uint32_t*)(SPM_BASE + 0x0010)) | 0x1;

    *((volatile uint32_t*)(SPM_BASE + 0x0360)) = 0x00010000;    /* for R0 */
    *((volatile uint32_t*)(SPM_BASE + 0x0360)) = 0x00000000;
    *((volatile uint32_t*)(SPM_BASE + 0x0338)) = *((volatile uint32_t*)(SPM_BASE + 0x0014));
    *((volatile uint32_t*)(SPM_BASE + 0x0360)) = 0x00800000;    /* for R7 */
    *((volatile uint32_t*)(SPM_BASE + 0x0360)) = 0x00000000;
    *((volatile uint32_t*)(SPM_BASE + 0x03C0)) = 0x3;           /* CLK_SETTLE */
    *((volatile uint32_t*)(SPM_BASE + 0x0360)) = 0x81;
    *((volatile uint32_t*)(SPM_BASE + 0x0380)) = 0x0;           /* mask all AP wakeup event */

    /* Init IM Length and pointer */
    *((volatile uint32_t*)(SPM_BASE + 0x0324)) = PCM_IM_MAX_LENGTH;
    *((volatile uint32_t*)(SPM_BASE + 0x0320)) = (uint32_t)spm_im;
    if (spm_auto_load != 0) {
        /* Enables IM slave mode */
        *((volatile uint32_t*)(SPM_BASE + 0x0304)) = 0x0B160001;
        /* Kick IM process */
        *SPM_PCM_CON0 = 0x0B160002;
    } else {
        /* manual load spm code */
        for (i = 0; i < PCM_IM_MAX_LENGTH; i++) {
            manual_spm_write_im(i, spm_im[i]);
        }
        *((volatile uint32_t*)(SPM_BASE + 0x0304)) = 0x0B163C49;
        /* Kick IM process */
        *SPM_PCM_CON0 = 0x0B160002;
    }

    /* Wait ready state */
    do {
        status = (*((volatile uint32_t*)(SPM_BASE + 0x09E4)) >> 9) & 0x1;
    } while (status != 0x01);

    *((volatile uint32_t*)(SPM_BASE + 0x0360)) = 0x0081;        // enable R0 & R7 output
    *((volatile uint32_t*)(SPM_BASE + 0x0144)) = 0x1B;

    /* DCXO Setting */
#ifdef MTK_HAL_EXT_32K_ENABLE
    /* use external 32K */
    /* DCXO=off when xo_en=0 */
    *((volatile uint32_t*)(SPM_BASE + 0x0104)) = 132;       /* rg_active_guard_time */
    *((volatile uint32_t*)(SPM_BASE + 0x0108)) = 8;         /* rg_sleep_guard_time */
    *((volatile uint32_t*)(SPM_BASE + 0x010C)) = 0x840084;  /* pmu_en_delay = 0,osc_en_delay = 0,xo_en_delay = 0 */
    *((volatile uint32_t*)(SPM_BASE + 0x0110)) = 132;       /* rg_vcore_xo_settle_time */
#else
    /* use internal 32K */
    /* DCXO=LPM when xo_en=0, no ACL */
    *((volatile uint32_t*)(SPM_BASE + 0x0104)) = 0x14D;     /* rg_active_guard_time */
    *((volatile uint32_t*)(SPM_BASE + 0x0108)) = 10;        /* rg_sleep_guard_time */
    *((volatile uint32_t*)(SPM_BASE + 0x010C)) = 0x840084;  /* pmu_en_delay = 0,osc_en_delay = 0,xo_en_delay = 7 */
    *((volatile uint32_t*)(SPM_BASE + 0x0110)) = 132;       /* rg_vcore_xo_settle_time */
#endif

    if (chip_is_ab1552() == true) {
        log_hal_info("This is AB1552, switch to internal 32k SPM setting\r\n");
        /* use internal 32K */
        /* DCXO=LPM when xo_en=0, no ACL */
        *((volatile uint32_t*)(SPM_BASE + 0x0104)) = 0x14D;     /* rg_active_guard_time */
        *((volatile uint32_t*)(SPM_BASE + 0x0108)) = 10;        /* rg_sleep_guard_time */
        *((volatile uint32_t*)(SPM_BASE + 0x010C)) = 0x840084;  /* pmu_en_delay = 0,osc_en_delay = 0,xo_en_delay = 7 */
        *((volatile uint32_t*)(SPM_BASE + 0x0110)) = 132;       /* rg_vcore_xo_settle_time */
    }

    /* Wakeup Source mask setting */
    *SPM_CM4_WAKEUP_SOURCE_MASK = 0x10;     //mask AUDIO Wakeup Source
    *SPM_DSP0_WAKEUP_SOURCE_MASK = 0x0;
    *SPM_DSP1_WAKEUP_SOURCE_MASK = 0x10;    //mask AUDIO Wakeup Source

    /* AUDIO_ANC_SRAM_CONTROL */
    *((volatile uint32_t*)(SPM_BASE + 0x0700)) = 0x30F;

    system_info = *((volatile uint32_t *)(0xA2010040)); /* SYSTEM_INFOD */
    if(((system_info >> 8) & 0x1) == 0) {
        /* bond_psram_sip == 0 */
        *SPM_SKIP_SFC_EMI_TASK |= (1 << 0);    /* SKIP PSRAM */
    }

    if(((system_info >> 9) & 0x1) == 0) {
        /* bond_sf_sip == 0 */
        *SPM_SKIP_SFC_EMI_TASK |= (1 << 8);    /* SKIP SFC */
    }

    NOR_ReadID(0, (void *)(SFC_GENERIC_FLASH_BANK_MASK), (uint16_t *)(&flash_id));
    if ((flash_id[0] == 0xEF) && (flash_id[1] == 0x60) && (flash_id[2] == 0x16)) {
        /* Only WB Flash SPM Skip SFC DPD mode */
        *SPM_SKIP_SFC_EMI_TASK |= (1 << 8);    /* SKIP SFC */
    }

    #if 1
    uint32_t im_check_buf;
    for (i = 0; i < PCM_IM_MAX_LENGTH; i++) {
        /* Read back spm code */
        im_check_buf = manual_spm_read_im(i);
        /* Check SPM Code */
        if(im_check_buf != spm_im[i]) {
            SPM_MCE_MSGID_E("SPM Code loading Fail\r\n", 0);
            return(result++);
        }
    }
    #endif

    *((volatile uint32_t*)(0xA2060010)) |= 0x1000000;           /* xo ready enable */
    *((volatile uint32_t*)(SPM_BASE + 0x0110)) = 0x1D;          /* rg_vcore_xo_settle_time */

    /* [4]: Resets PCM */
    *SPM_PCM_CON0 = 0x0B160000 | 0x10;
    hal_gpt_delay_us(10);
    *SPM_PCM_CON0 = 0x0B160000;
    hal_gpt_delay_us(10);

    /* SPM Kick Start */
    *SPM_PCM_CON0 = 0x0B160103;

    SPM_CLEAR_LOCK_INFRA;
    SPM_INFRA_OFF_FLAG = 0;

    return(result);
}
#include "hal_ccni.h"
#include "hal_ccni_config.h"
void spm_control_mtcmos_internal(spm_mtcmos_type_t mtcmos, spm_mtcmos_ctrl_t ctrl)
{
    uint32_t audio_control;
    static uint32_t dsp0_wakeup_source_mask = 0x800,dsp1_wakeup_source_mask = 0x800;
    if (mtcmos == SPM_MTCMOS_CONN) {
        if (ctrl == SPM_MTCMOS_PWR_DISABLE) {
            if ((*SPM_PWR_STATUS & 0x4) == 0) {
                return;
            }
            /* CONN MTCMOS OFF */
            *((volatile uint32_t*)(SPM_BASE + 0x0208)) = 0x1D;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0208)) = 0x1F;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0208)) = 0x1E;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0208)) = 0x1A;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0208)) = 0x12;
            hal_gpt_delay_us(1);
        } else {
            if ((*SPM_PWR_STATUS & 0x4) != 0) {
                return;
            }
            /* CONN MTCMOS ON */
            *((volatile uint32_t*)(SPM_BASE + 0x0208)) = 0x16;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0208)) = 0x1E;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0208)) = 0x0E;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0208)) = 0x0C;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0208)) = 0x1C;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0208)) = 0x1D;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0208)) = 0x0D;
            hal_gpt_delay_us(1);
        }
        return;
    }

    if (mtcmos == SPM_MTCMOS_AUDIO) {
        if (ctrl == SPM_MTCMOS_PWR_DISABLE) {
            if ((*SPM_PWR_STATUS & 0x8) == 0) {
                return;
            }
            /* AUDIO MTCMOS OFF */
            *((volatile uint32_t*)(0xA2290020)) = 1;                        /* INFRA_CFG_MTCMOS2[0] : audiosys_prot_en */
            while((*((volatile uint32_t*)(0xA2290020))&0x100) != 0x100);    /* INFRA_CFG_MTCMOS2[8] : audiosys_prot_rdy */

            *((volatile uint32_t*)(SPM_BASE + 0x020C)) = 0x1D;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x020C)) = 0x1F;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x020C)) = 0x1E;
            hal_gpt_delay_us(1);

            audio_control = *((volatile uint32_t*)(SPM_BASE + 0x0700));
            *((volatile uint32_t*)(SPM_BASE + 0x0700)) = audio_control | 0x01;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0700)) = audio_control | 0x03;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0700)) = audio_control | 0x07;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0700)) = audio_control | 0x0F;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0700)) = 0x30F;
            hal_gpt_delay_us(1);

            *((volatile uint32_t*)(SPM_BASE + 0x020C)) = 0x1A;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x020C)) = 0x12;
            hal_gpt_delay_us(1);
        } else {
            if ((*SPM_PWR_STATUS & 0x8) != 0) {
                    return;
            }

            /* AUDIO MTCMOS ON */
            *((volatile uint32_t*)(0xA2030B20)) |= 0x04;

            *((volatile uint32_t*)(SPM_BASE + 0x020C)) = 0x16;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x020C)) = 0x1E;
            hal_gpt_delay_us(1);

            *((volatile uint32_t*)(SPM_BASE + 0x020C)) = 0x0E;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x020C)) = 0x0C;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x020C)) = 0x1C;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x020C)) = 0x1D;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x020C)) = 0x0D;
            hal_gpt_delay_us(1);

            audio_control = *((volatile uint32_t*)(SPM_BASE + 0x0700));
            *((volatile uint32_t*)(SPM_BASE + 0x0700)) = audio_control & 0xFF07;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0700)) = audio_control & 0xFF03;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0700)) = audio_control & 0xFF01;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0700)) = audio_control & 0xFF00;
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0700)) = 0;
            hal_gpt_delay_us(1);

            *((volatile uint32_t*)(0xA2290020)) = 0;                    /* INFRA_CFG_MTCMOS2[0] : audiosys_prot_en */
            while((*((volatile uint32_t*)(0xA2290020))&0x100) != 0);    /* INFRA_CFG_MTCMOS2[8] : audiosys_prot_rdy */
        }
        return;
    }

 if (mtcmos == SPM_MTCMOS_DSP0) {
        if (ctrl == SPM_MTCMOS_PWR_DISABLE) {
            /* DSP0 Power OFF Flow */
            MCU_CFG_PRI->DSP0CFG_STALL = 1;
            *((volatile uint32_t*)(SPM_BASE + 0x0150)) |= 0x00100;   //IGNORE_DSP0_ACTIVE
            if((dsp0_wakeup_source_mask & 0x80000000) == 0) {
                dsp0_wakeup_source_mask = *SPM_DSP0_WAKEUP_SOURCE_MASK;
                *SPM_DSP0_WAKEUP_SOURCE_MASK = 0x3FFF;
                dsp0_wakeup_source_mask |= 0x80000000;
            }

            /* set protect_en */
            *((volatile uint32_t*)(0xA229001C)) = 0x1;
            while(((*((volatile uint32_t*)(0xA229001C)) >> 8)&0x01) != 1);

            /* MTCMOS OFF flow part 1 */
            *((volatile uint32_t*)(SPM_BASE + 0x0400)) = 0x1D;
            *((volatile uint32_t*)(SPM_BASE + 0x0400)) = 0x1F;
            *((volatile uint32_t*)(SPM_BASE + 0x0400)) = 0x1E;
            /* MTCMOS OFF flow part 2 */

            *((volatile uint32_t*)(SPM_BASE + 0x0420)) = 0xFFFFFFFF;
            *((volatile uint32_t*)(SPM_BASE + 0x0430)) = 0xFFFF;

            *((volatile uint32_t*)(SPM_BASE + 0x0424)) = 0x3000300; /* [25:24]: mem_iso_en [9:8]:SLEEPB, [1:0]:PD */
            *((volatile uint32_t*)(SPM_BASE + 0x0434)) = 0x3000300;
            *((volatile uint32_t*)(SPM_BASE + 0x0450)) = 0x0;

            *((volatile uint32_t*)(SPM_BASE + 0x0424)) = 0x3000201;
            hal_gpt_delay_us(1); /* DRAM0 */
            *((volatile uint32_t*)(SPM_BASE + 0x0424)) = 0x3000003;
            hal_gpt_delay_us(1); /* DRAM1 */

            *((volatile uint32_t*)(SPM_BASE + 0x0434)) = 0x3000201;
            hal_gpt_delay_us(1); /* IRAM0 */
            *((volatile uint32_t*)(SPM_BASE + 0x0434)) = 0x3000003;
            hal_gpt_delay_us(1); /* IRAM1 */

            *((volatile uint32_t*)(SPM_BASE + 0x0440)) = 0xF;       /* ICACHE */
            *((volatile uint32_t*)(SPM_BASE + 0x0440)) = 0xFF;      /* DCACHE */
            *((volatile uint32_t*)(SPM_BASE + 0x0440)) = 0xFFF;     /* ITAG */
            *((volatile uint32_t*)(SPM_BASE + 0x0440)) = 0xFFFF;

            /* MTCMOS OFF flow part 3 */
            *((volatile uint32_t*)(SPM_BASE + 0x0400)) = 0x1A;
            *((volatile uint32_t*)(SPM_BASE + 0x0400)) = 0x12;
            /* DSP0 Power OFF Flow End*/
        } else {
            MCU_CFG_PRI->DSP0CFG_STALL = 1;
            /* DSP0 Power ON Flow */
            *((volatile uint32_t*)(SPM_BASE + 0x0150)) &= 0xFFFFFEFF;   //CLEAR IGNORE_DSP0_ACTIVE
            MCU_CFG_PRI->DSP0_DEEP_SLEEP = 0;

            /* MTCMOS ON flow part 1 */
            /* pwr_on/pwr_on_2nd */
            *((volatile uint32_t*)(SPM_BASE + 0x0400)) = 0x16;
            *((volatile uint32_t*)(SPM_BASE + 0x0400)) = 0x1E;

            /* MTCMOS ON flow part 2 */
            /* SRAM ON */
            *((volatile uint32_t*)(SPM_BASE + 0x0440)) = 0x0FFF;
            *((volatile uint32_t*)(SPM_BASE + 0x0440)) = 0xFF;      /* ITAG */
            *((volatile uint32_t*)(SPM_BASE + 0x0440)) = 0xF;       /* DCACHE */
            *((volatile uint32_t*)(SPM_BASE + 0x0440)) = 0x0;       /* ICACHE */

            *((volatile uint32_t*)(SPM_BASE + 0x0424)) = 0x3000102; /* [25:24]: mem_iso_en [9:8]:SLEEPB, [1:0]:PD */
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0424)) = 0x3000300; /* [25:24]: mem_iso_en [9:8]:SLEEPB, [1:0]:PD */
            hal_gpt_delay_us(1);

            *((volatile uint32_t*)(SPM_BASE + 0x0434)) = 0x3000102; /* [25:24]: mem_iso_en [9:8]:SLEEPB, [1:0]:PD */
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0434)) = 0x3000300; /* [25:24]: mem_iso_en [9:8]:SLEEPB, [1:0]:PD */
            hal_gpt_delay_us(1);

            hal_gpt_delay_us(1);  /* wait shift reg, wait sleepb to isointb > 0.9u */

            *((volatile uint32_t*)(SPM_BASE + 0x0450)) = 0x1;

            *((volatile uint32_t*)(SPM_BASE + 0x0424)) = 0x0010300;
            *((volatile uint32_t*)(SPM_BASE + 0x0434)) = 0x0010300;

            /* MTCMOS ON flow part 3 */
            /* pwr_clk_dis/pwr_rstb/pwr_iso */
            *((volatile uint32_t*)(SPM_BASE + 0x0400)) = 0xE;
            *((volatile uint32_t*)(SPM_BASE + 0x0400)) = 0xC;
            *((volatile uint32_t*)(SPM_BASE + 0x0400)) = 0x1C;
            *((volatile uint32_t*)(SPM_BASE + 0x0400)) = 0x1D;
            *((volatile uint32_t*)(SPM_BASE + 0x0400)) = 0xD;

            *((volatile uint32_t*)(SPM_BASE + 0x0420)) = 0x0;
            *((volatile uint32_t*)(SPM_BASE + 0x0430)) = 0x0;

            /* clear protect_en */
            *((volatile uint32_t*)(0xA229001C)) = 0x0;
            while(((*((volatile uint32_t*)(0xA229001C)) >> 8)&0x01) != 0);

            dsp0_wakeup_source_mask &= 0x7FFFFFFF;
            *SPM_DSP0_WAKEUP_SOURCE_MASK = dsp0_wakeup_source_mask;
            /* DSP0 Power ON Flow End */

            #ifdef HAL_WDT_MODULE_ENABLED
            wdt_clear_reset_status(DSP0_CORE_RESET);
            #endif
        }
        return;
    }

    if (mtcmos == SPM_MTCMOS_DSP1) {
        if (ctrl == SPM_MTCMOS_PWR_DISABLE) {
            /* DSP1 Power OFF Flow */
            MCU_CFG_PRI->DSP1CFG_STALL = 1;
            *((volatile uint32_t*)(SPM_BASE + 0x0150)) |= 0x10000;   //IGNORE_DSP1_ACTIVE
            if((dsp1_wakeup_source_mask & 0x80000000) == 0) {
                dsp1_wakeup_source_mask = *SPM_DSP1_WAKEUP_SOURCE_MASK;
                *SPM_DSP1_WAKEUP_SOURCE_MASK = 0x3FFF;
                dsp1_wakeup_source_mask |= 0x80000000;
            }

            /* set protect_en */
            *((volatile uint32_t*)(0xA2290024)) = 0x1;
            while(((*((volatile uint32_t*)(0xA2290024)) >> 8)&0x01) != 1);

            /* MTCMOS OFF flow part 1 */
            *((volatile uint32_t*)(SPM_BASE + 0x0500)) = 0x1D;
            *((volatile uint32_t*)(SPM_BASE + 0x0500)) = 0x1F;
            *((volatile uint32_t*)(SPM_BASE + 0x0500)) = 0x1E;
            /* MTCMOS OFF flow part 2 */

            *((volatile uint32_t*)(SPM_BASE + 0x0520)) = 0xFFFF;
            *((volatile uint32_t*)(SPM_BASE + 0x0530)) = 0xFF;

            *((volatile uint32_t*)(SPM_BASE + 0x0524)) = 0x3000300; /* [25:24]: mem_iso_en [9:8]:SLEEPB, [1:0]:PD */
            *((volatile uint32_t*)(SPM_BASE + 0x0534)) = 0x3000300;

            *((volatile uint32_t*)(SPM_BASE + 0x0550)) = 0x0;

            *((volatile uint32_t*)(SPM_BASE + 0x0524)) = 0x3000201;
            hal_gpt_delay_us(1); /* DRAM0 */
            *((volatile uint32_t*)(SPM_BASE + 0x0524)) = 0x3000003;
            hal_gpt_delay_us(1); /* DRAM1 */

            *((volatile uint32_t*)(SPM_BASE + 0x0534)) = 0x3000201;
            hal_gpt_delay_us(1); /* IRAM0 */
            *((volatile uint32_t*)(SPM_BASE + 0x0534)) = 0x3000003;
            hal_gpt_delay_us(1); /* IRAM1 */

            /* MTCMOS OFF flow part 3 */
            *((volatile uint32_t*)(SPM_BASE + 0x0500)) = 0x1A;
            *((volatile uint32_t*)(SPM_BASE + 0x0500)) = 0x12;
            /* DSP1 Power OFF Flow End */
        } else {
            MCU_CFG_PRI->DSP1CFG_STALL = 1;
            /* DSP1 Power ON Flow */
            *((volatile uint32_t*)(SPM_BASE + 0x0150)) &= 0xFFFEFFFF;   //CLEAR IGNORE_DSP1_ACTIVE
            MCU_CFG_PRI->DSP1_DEEP_SLEEP = 0;

            /* MTCMOS ON flow part 1 */
            /* pwr_on/pwr_on_2nd */
            *((volatile uint32_t*)(SPM_BASE + 0x0500)) = 0x16;
            *((volatile uint32_t*)(SPM_BASE + 0x0500)) = 0x1E;

            /* MTCMOS ON flow part 2 */
            /* SRAM ON */
            *((volatile uint32_t*)(SPM_BASE + 0x0524)) = 0x3000102; /* [25:24]: mem_iso_en [9:8]:SLEEPB, [1:0]:PD */
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0524)) = 0x3000300; /* [25:24]: mem_iso_en [9:8]:SLEEPB, [1:0]:PD */
            hal_gpt_delay_us(1);

            *((volatile uint32_t*)(SPM_BASE + 0x0534)) = 0x3000102; /* [25:24]: mem_iso_en [9:8]:SLEEPB, [1:0]:PD */
            hal_gpt_delay_us(1);
            *((volatile uint32_t*)(SPM_BASE + 0x0534)) = 0x3000300; /* [25:24]: mem_iso_en [9:8]:SLEEPB, [1:0]:PD */
            hal_gpt_delay_us(1);

            hal_gpt_delay_us(1);  /* wait shift reg, wait sleepb to isointb > 0.9u */
            *((volatile uint32_t*)(SPM_BASE + 0x0550)) = 0x1;

            *((volatile uint32_t*)(SPM_BASE + 0x0524)) = 0x0010300;
            *((volatile uint32_t*)(SPM_BASE + 0x0534)) = 0x0010300;

            /* MTCMOS ON flow part 3 */
            /* pwr_clk_dis/pwr_rstb/pwr_iso */
            *((volatile uint32_t*)(SPM_BASE + 0x0500)) = 0xE;
            *((volatile uint32_t*)(SPM_BASE + 0x0500)) = 0xC;
            *((volatile uint32_t*)(SPM_BASE + 0x0500)) = 0x1C;
            *((volatile uint32_t*)(SPM_BASE + 0x0500)) = 0x1D;
            *((volatile uint32_t*)(SPM_BASE + 0x0500)) = 0xD;

            *((volatile uint32_t*)(SPM_BASE + 0x0520)) = 0x0;
            *((volatile uint32_t*)(SPM_BASE + 0x0530)) = 0x0;

            /* clear protect_en */
            *((volatile uint32_t*)(0xA2290024)) = 0x0;
            while(((*((volatile uint32_t*)(0xA2290024)) >> 8)&0x01) != 0);

            dsp1_wakeup_source_mask &= 0x7FFFFFFF;
            *SPM_DSP1_WAKEUP_SOURCE_MASK = dsp1_wakeup_source_mask;
            /* DSP1 Power ON Flow End */

            #ifdef HAL_WDT_MODULE_ENABLED
            wdt_clear_reset_status(DSP1_CORE_RESET);
            #endif
        }
        return;
    }
}

uint32_t spm_control_mtcmos(spm_mtcmos_type_t mtcmos, spm_mtcmos_ctrl_t ctrl)
{
    if (mtcmos == SPM_MTCMOS_CONN) {
        if (ctrl == SPM_MTCMOS_PWR_DISABLE) {
            if (mtcmos_resource.conn == 0) {
                spm_control_mtcmos_internal(SPM_MTCMOS_CONN, SPM_MTCMOS_PWR_DISABLE);
                return 0;
            }
            mtcmos_resource.conn--;
            if (mtcmos_resource.conn == 0) {
                spm_control_mtcmos_internal(SPM_MTCMOS_CONN, SPM_MTCMOS_PWR_DISABLE);
            }
        } else {
            mtcmos_resource.conn++;
            if (mtcmos_resource.conn == 1) {
                spm_control_mtcmos_internal(SPM_MTCMOS_CONN, SPM_MTCMOS_PWR_ENABLE);
            }
        }
        return (mtcmos_resource.conn);
    }

    if (mtcmos == SPM_MTCMOS_AUDIO) {
        if (ctrl == SPM_MTCMOS_PWR_DISABLE) {
            if (mtcmos_resource.audio == 0) {
                spm_control_mtcmos_internal(SPM_MTCMOS_AUDIO, SPM_MTCMOS_PWR_DISABLE);
                return 0;
            }
            mtcmos_resource.audio--;
            if (mtcmos_resource.audio == 0) {
                spm_control_mtcmos_internal(SPM_MTCMOS_AUDIO, SPM_MTCMOS_PWR_DISABLE);
            }
        } else {
            mtcmos_resource.audio++;
            if (mtcmos_resource.audio == 1) {
                spm_control_mtcmos_internal(SPM_MTCMOS_AUDIO, SPM_MTCMOS_PWR_ENABLE);
            }
        }
        return (mtcmos_resource.audio);
    }

    if (mtcmos == SPM_MTCMOS_DSP0) {
        if (ctrl == SPM_MTCMOS_PWR_DISABLE) {
            if (mtcmos_resource.dsp0 == 0) {
                spm_control_mtcmos_internal(SPM_MTCMOS_DSP0, SPM_MTCMOS_PWR_DISABLE);
                hal_core_status_write(HAL_CORE_DSP0, HAL_CORE_OFF);
                return 0;
            }
            mtcmos_resource.dsp0--;
            if (mtcmos_resource.dsp0 == 0) {
                spm_control_mtcmos_internal(SPM_MTCMOS_DSP0, SPM_MTCMOS_PWR_DISABLE);
                hal_core_status_write(HAL_CORE_DSP0, HAL_CORE_OFF);
            }
        } else {
            mtcmos_resource.dsp0++;
            if (mtcmos_resource.dsp0 == 1) {
                spm_control_mtcmos_internal(SPM_MTCMOS_DSP0, SPM_MTCMOS_PWR_ENABLE);
            }
        }
        return (mtcmos_resource.dsp0);
    }

    if (mtcmos == SPM_MTCMOS_DSP1) {
        if (ctrl == SPM_MTCMOS_PWR_DISABLE) {
            if (mtcmos_resource.dsp1 == 0) {
                spm_control_mtcmos_internal(SPM_MTCMOS_DSP1, SPM_MTCMOS_PWR_DISABLE);
                hal_core_status_write(HAL_CORE_DSP1, HAL_CORE_OFF);
                return 0;
            }
            mtcmos_resource.dsp1--;
            if (mtcmos_resource.dsp1 == 0) {
                spm_control_mtcmos_internal(SPM_MTCMOS_DSP1, SPM_MTCMOS_PWR_DISABLE);
                hal_core_status_write(HAL_CORE_DSP1, HAL_CORE_OFF);
            }
        } else {
            mtcmos_resource.dsp1++;
            if (mtcmos_resource.dsp1 == 1) {
                spm_control_mtcmos_internal(SPM_MTCMOS_DSP1, SPM_MTCMOS_PWR_ENABLE);
            }
        }
        return (mtcmos_resource.dsp1);
    }
    return 0;
}

void spm_mask_wakeup_source(uint32_t wakeup_source)
{
    if (wakeup_source == HAL_SLEEP_MANAGER_WAKEUP_SOURCE_ALL) {
        *SPM_CM4_WAKEUP_SOURCE_MASK = 0xFF;
    } else {
        *SPM_CM4_WAKEUP_SOURCE_MASK |= (1 << wakeup_source);
    }
}

void spm_unmask_wakeup_source(uint32_t wakeup_source)
{
    if (wakeup_source == HAL_SLEEP_MANAGER_WAKEUP_SOURCE_ALL) {
        *SPM_CM4_WAKEUP_SOURCE_MASK = 0x00;
    } else {
        *SPM_CM4_WAKEUP_SOURCE_MASK &= ~(1 << wakeup_source);
    }
}

void manual_spm_write_im(uint32_t addr, uint32_t data)
{
    *((volatile uint32_t*)(SPM_BASE + 0x0330)) = 0x01010000 | addr;
    *((volatile uint32_t*)(SPM_BASE + 0x0334)) = data;
    return;
}

uint32_t manual_spm_read_im(uint32_t addr)
{
    uint32_t data;
    *((volatile uint32_t*)(SPM_BASE + 0x0330)) = 0x01000000 | addr;
    data = *((volatile uint32_t*)(SPM_BASE + 0x0334));
    *((volatile uint32_t*)(SPM_BASE + 0x0330)) = 0;
    return (data);
}

void spm_audio_lowpower_setting(spm_sleep_state_t sleep_state,spm_ctrl_t enable)
{
    if(sleep_state == SPM_STATE1) {
        if(enable == SPM_ENABLE) {
            *((volatile uint32_t*)(0xA21203B0)) |= 0x1;         /* Lock S1 */

            /* CM4 notify DSP0 lock sleep , if CM4 have any audio request */
            *((volatile uint32_t*)(0xA2120B04)) |= 0x80000000; /* use bit31 */

        } else {
            *((volatile uint32_t*)(0xA21203B0)) &= 0xFFFFFFFE;  /* Lock S1 */

            /* CM4 notify DSP0 unlock sleep , if CM4 have not any audio request */
            *((volatile uint32_t*)(0xA2120B04)) &= 0x7FFFFFFF; /* use bit31 */
        }
        return;
    }

    if(sleep_state == SPM_STATE4) {
        if(enable == SPM_ENABLE) {
            *((volatile uint32_t*)(0xA2120B00)) |= 0x1;                 /* SYS CLK Force on */
            *((volatile uint32_t*)(0xA21203B0)) |= 0x01000000;      /* Lock S4 */
        } else {
            *((volatile uint32_t*)(0xA2120B00)) &= 0xFFFFFFFE;          /* SYS CLK Force on */
            *((volatile uint32_t*)(0xA21203B0)) &= 0xFEFFFFFF;      /* Unlock S4 */
        }
        return;
    }
}

void spm_force_on_pmic(spm_request_t spm_request,spm_ctrl_t enable)
{
    static uint8_t resource_control = 0;

    if(enable == SPM_ENABLE) {
        resource_control |= (1 << spm_request);
    }else{
        resource_control &= ~(1 << spm_request);
    }

    if(resource_control == 0){
        *((volatile uint32_t*) 0xA2120100) &= 0xFFFFFEFF;       /* Unforce PMIC On */
    }else{
        *((volatile uint32_t*) 0xA2120100) |= 0x100;            /* Force PMIC On */
    }
}

void spm_debug_io(unsigned int debug_bus)
{
    SPM_MCE_MSGID_I("spm_debug_io\r\n", 0);
#if 0
    /* 1558 */
    hal_pinmux_set_function(HAL_GPIO_40,8);
    hal_pinmux_set_function(HAL_GPIO_61,8);
    hal_pinmux_set_function(HAL_GPIO_22,8);
    hal_pinmux_set_function(HAL_GPIO_23,8);

    hal_pinmux_set_function(HAL_GPIO_64,8);
    hal_pinmux_set_function(HAL_GPIO_65,8);
    hal_pinmux_set_function(HAL_GPIO_26,8);
    hal_pinmux_set_function(HAL_GPIO_67,8);

    hal_pinmux_set_function(HAL_GPIO_68,8);
    hal_pinmux_set_function(HAL_GPIO_69,8);
    hal_pinmux_set_function(HAL_GPIO_30,8);
    hal_pinmux_set_function(HAL_GPIO_31,8);

    hal_pinmux_set_function(HAL_GPIO_32,8);
    hal_pinmux_set_function(HAL_GPIO_33,8);
    hal_pinmux_set_function(HAL_GPIO_34,8);
    hal_pinmux_set_function(HAL_GPIO_35,8);
#endif

#if 0
    /* 1555 */
    hal_pinmux_set_function(HAL_GPIO_13,8);
   // hal_pinmux_set_function(HAL_GPIO_21,8);
   // hal_pinmux_set_function(HAL_GPIO_22,8);
    hal_pinmux_set_function(HAL_GPIO_23,8);

    hal_pinmux_set_function(HAL_GPIO_24,8);
    hal_pinmux_set_function(HAL_GPIO_25,8);
    hal_pinmux_set_function(HAL_GPIO_26,8);
    hal_pinmux_set_function(HAL_GPIO_27,8);

    hal_pinmux_set_function(HAL_GPIO_11,8);
    hal_pinmux_set_function(HAL_GPIO_29,8);
    hal_pinmux_set_function(HAL_GPIO_30,8);
    hal_pinmux_set_function(HAL_GPIO_31,8);

    hal_pinmux_set_function(HAL_GPIO_7,8);
    hal_pinmux_set_function(HAL_GPIO_4,8);
    hal_pinmux_set_function(HAL_GPIO_3,8);
    hal_pinmux_set_function(HAL_GPIO_2,8);
#endif
    //FPGA debug setting:
    //*SPM_SPM_DEBUG_CON =0x1;
    *((volatile uint32_t*)(SPM_BASE + 0x03D4)) =0x1;
    //*PCM_DEBUG_CON(0xA21203D4)=0x1; (enable debug)
    SPM_MCE_MSGID_I("0xA21203D4=0x%x\r\n\r\n", 1, *(volatile unsigned int *)(0xA21203D4));

    //*INFRA_CFG_DBGMON0=0x0;  (0xA2290010)
   *(volatile uint32_t *)(0xA2290010) = 0x0;
    //*(volatile uint32_t *)(0xA2290010) = 0x3;   //EINT
    SPM_MCE_MSGID_I("0xA2290010=0x%x\r\n\r\n", 1, *(volatile unsigned int *)(0xA2290010));

    //ASIC need more:
    //*TOP_DEBUG = 0x14;   (0xA2010060)
    *(volatile uint32_t *)(0xA2010060) = 0x14;
    SPM_MCE_MSGID_I("0xA2010060=0x%x\r\n\r\n", 1, *(volatile unsigned int *)(0xA2010060));

    //*SPM_SPM_DEBUG_SELECT = debug_bus;
    *((volatile uint32_t*)(SPM_BASE + 0x03D0)) = debug_bus;

    SPM_MCE_MSGID_I("0xA21203D0=0x%x\r\n\r\n", 1, *(volatile unsigned int *)(0xA21203D0));

    SPM_MCE_MSGID_I("SPM_SPM_DEBUG_SELECT=0x%x\r\n\r\n", 1, *((volatile unsigned int*)(SPM_BASE + 0x03D0)));
}
#endif
