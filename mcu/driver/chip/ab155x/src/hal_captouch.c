/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_captouch.h"

#ifdef HAL_CAPTOUCH_MODULE_ENABLED
#include "hal_captouch_internal.h"
#include "hal_pmu.h"
#include "hal_gpt.h"
#include <string.h>
#include "hal_log.h"
#include "hal_nvic.h"


//HW basic configuration
extern uint8_t pmu_get_power_on_reason();

hal_captouch_status_t hal_captouch_init(hal_captouch_config_t *config)
{
    uint32_t i;
    hal_captouch_status_t ret;

    if (captouch_context.has_initilized == true) {
        log_hal_msgid_info("hal_captouch_init has been initialized!\r\n", 0);
        return HAL_CAPTOUCH_STATUS_INITIALIZED;
    }

    if (config->callback.callback == NULL) {
        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    log_hal_msgid_info("hal_captouch_init start,ch=%x,r=%d,s=%d\r\n",3,\
                  config->channel_bit_map,config->mavg_r,config->avg_s);

    memset(&captouch_context, 0, sizeof(captouch_context_t));
    memset(&captouch_buffer, 0, sizeof(captouch_buffer_t));

    //get ept channel bit map
    captouch_context.used_channel_map = config->channel_bit_map;

    captouch_context.captouch_callback.callback  = config->callback.callback;
    captouch_context.captouch_callback.user_data = config->callback.user_data;

    //register nvic irq handler and enable irq
    captouch_register_nvic_callback();

    //clear pdn, open hif clock source
    captouch_control_pdn_clock(captouch_enable);

    //open captouch annalog clock
    ret = captouch_rtc_clk_control(captouch_enable);
    if (ret != HAL_CAPTOUCH_STATUS_OK) {
        return ret;
    }

    //if first power on, reset fifo
    if(pmu_get_power_on_reason() == 0) {
        captouch->TOUCH_HIF_CON0.CELLS.HIF_SOFT_RST = 1;
    }

    //default to set the wire3 clock to the highest 1/2 bus clock
    captouch->WIRE3_CON2 = CAPTOUCH_WIRE3_CLOCK_1_2;

    //default to set the timestap enable
    captouch->TOUCH_HIF_CON0.CELLS.TIMESTAMP_EN = 1;

    captouch_clk_control(captouch_disable);

    //analog parameter setting
    captouch_analog_init(config);

    //set threshold and coarse_cap
    for(i=0;i<8;i++) {
        if (captouch_context.used_channel_map & (1<<i)) {
            ret = hal_captouch_set_threshold(i,config->high_thr[i],config->low_thr[i]);
            if (ret != HAL_CAPTOUCH_STATUS_OK) {
                return ret;
            }

            ret = hal_captouch_set_coarse_cap(i,config->coarse_cap[i]);
            if (ret != HAL_CAPTOUCH_STATUS_OK) {
                return ret;
            }
        }
    }

    captouch_channel_int_control(captouch_context.used_channel_map, captouch_enable);

    //enable touch analog
    captouch_clk_control(captouch_enable);

    //reset fifo
    hal_nvic_disable_irq(CAP_TOUCH_IRQn);
    captouch->TOUCH_HIF_CON0.CELLS.HIF_SOFT_RST = 1;
    hal_gpt_delay_ms(3);
    hal_nvic_enable_irq(CAP_TOUCH_IRQn);

    captouch_int_control(captouch_enable);

    captouch_context.has_initilized = true;

    log_hal_msgid_info("hal_captouch_init end\r\n", 0);

    return HAL_CAPTOUCH_STATUS_OK;

}

/*
//analog advanced configuration
hal_captouch_status_t hal_captouch_advanced_config(hal_captouch_advanced_config_t *config)
{
    return HAL_CAPTOUCH_STATUS_OK;
}*/

hal_captouch_status_t hal_captouch_channel_enable(hal_captouch_channel_t channel)
{
    if (channel>= HAL_CAPTOUCH_CHANNEL_MAX) {

        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    captouch_clk_control(captouch_disable);

    //enable chanel sensing
    captouch_channel_sense_control(1<<channel, captouch_enable);

    //enable high and low threhold wakeup&int interrupt
    captouch_channel_int_control(1<<channel, captouch_enable);

    //enable captouch module wakeup&int interrupt
    captouch_int_control(captouch_enable);

    captouch_clk_control(captouch_enable);

    return HAL_CAPTOUCH_STATUS_OK;
}

hal_captouch_status_t hal_captouch_channel_disable(hal_captouch_channel_t channel)
{
    if (channel>= HAL_CAPTOUCH_CHANNEL_MAX) {

        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    //disable chanel sensing
    captouch_channel_sense_control(1<<channel, captouch_disable);

    //mask hign and low threhold wakeup&int interrupt
    captouch_channel_int_control(1<<channel, captouch_disable);

    return HAL_CAPTOUCH_STATUS_OK;

}

hal_captouch_status_t hal_captouch_get_event(hal_captouch_event_t *event)
{
    if (event == NULL) {
        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    if (captouch_get_buffer_data_size()<=0) {
        return HAL_CAPTOUCH_STATUS_NO_EVENT;
    }

    captouch_pop_one_event_from_buffer(event);

    return HAL_CAPTOUCH_STATUS_OK;
}


hal_captouch_status_t hal_captouch_translate_channel_to_symbol(hal_captouch_channel_t channel, uint8_t *symbol)
{
    if (channel>= HAL_CAPTOUCH_CHANNEL_MAX) {

        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    *symbol = captouch_mapping_keydata[channel];

    return HAL_CAPTOUCH_STATUS_OK;
}

hal_captouch_status_t hal_captouch_set_threshold(hal_captouch_channel_t channel,int32_t high_thr, int32_t low_thr)
{
    int16_t thr_h,thr_l;
    bool bak_en;

    if (channel >= HAL_CAPTOUCH_CHANNEL_MAX) {
        return HAL_CAPTOUCH_STATUS_CHANNEL_ERROR;
    }

    if ((high_thr > 255) || (high_thr < (-256)) ||(low_thr>255) || (low_thr< (-256))) {
        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    bak_en = captouch_clk_control(captouch_disable);
    captouch_int_control(captouch_disable);

    thr_h = captouch_9signed_to_16signed((uint16_t)high_thr);
    thr_l = captouch_9signed_to_16signed((uint16_t)low_thr);

    captouch_set_threshold(channel, thr_h, thr_l);

    captouch_int_control(captouch_enable);
    captouch_clk_control(bak_en);

    return HAL_CAPTOUCH_STATUS_OK;

}

hal_captouch_status_t hal_captouch_set_fine_cap(hal_captouch_channel_t channel,int32_t fine)
{
    int16_t fine_data;
    bool bak_en;

    if (channel >= HAL_CAPTOUCH_CHANNEL_MAX) {
        return HAL_CAPTOUCH_STATUS_CHANNEL_ERROR;
    }

    if ((fine > 63) || (fine<(-64))) {
        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    bak_en = captouch_clk_control(captouch_disable);

    fine_data = captouch_7signed_to_16signed((uint16_t)fine);
    captouch_set_fine_cap(channel,fine_data);
    captouch_clk_control(bak_en);

    return HAL_CAPTOUCH_STATUS_OK;
}

hal_captouch_status_t hal_captouch_set_coarse_cap(hal_captouch_channel_t channel, uint32_t coarse)
{
    bool bak_en;

    if (channel >= HAL_CAPTOUCH_CHANNEL_MAX) {
        return HAL_CAPTOUCH_STATUS_CHANNEL_ERROR;
    }

    if (coarse > CAPTOUCH_COARSE_CAP_MAX) {
        return HAL_CAPTOUCH_STATUS_INVALID_PARAMETER;
    }

    bak_en = captouch_clk_control(captouch_disable);
    captouch_int_control(captouch_disable);
    hal_gpt_delay_ms(2);

    captouch_set_coarse_cap(channel,(uint8_t)coarse);

    hal_gpt_delay_ms(2);
    captouch_int_control(captouch_enable);

    captouch_clk_control(bak_en);

    return HAL_CAPTOUCH_STATUS_OK;
}

hal_captouch_status_t hal_captouch_tune_control(hal_captouch_channel_t channel,hal_captouch_tune_type_t tune_type, hal_captouch_tune_data_t* data)
{
    uint16_t rdata,count;
    uint16_t backup[4];
    int16_t  temp[2];
    bool bak_en;
    hal_captouch_status_t ret;

    if (channel >= HAL_CAPTOUCH_CHANNEL_MAX) {
        return HAL_CAPTOUCH_STATUS_CHANNEL_ERROR;
    }

    if (data == NULL) {
        return HAL_CAPTOUCH_STATUS_ERROR;
    }

    if (captouch_context.has_initilized != true) {
        return HAL_CAPTOUCH_STATUS_UNINITIALIZED;
    }

    ret = HAL_CAPTOUCH_STATUS_ERROR;

    hal_nvic_disable_irq(CAP_TOUCH_IRQn);

    bak_en = captouch_clk_control(captouch_disable);

    backup[3] = captouch_analog_read_data(CAPTOUCH_ANALOG.THR_L[channel]);

    //switch to only one channel sensing
    rdata  = captouch_analog_read_data(TOUCH_CON0);
    backup[0] = rdata;
    rdata  &= ~((0xff<<8)+TOUCH_INT_EN + TOUCH_WAKE_EN+TOUCH_CAL_AUTOSUSPEND);
    rdata  |= (1<<(8+channel));
    captouch_analog_write_data(TOUCH_CON0,rdata);

    //mask all hign and low threhold wakeup
    rdata = captouch_analog_read_data(TOUCH_CH_WAKEUP_EN);
    backup[1] = rdata;
    captouch_analog_write_data(TOUCH_CH_WAKEUP_EN,0);

    //mask all hign and low threhold interrupt
    rdata = captouch_analog_read_data(TOUCH_CH_INT_EN);
    backup[2] = rdata;
    captouch_analog_write_data(TOUCH_CH_INT_EN,0);
    hal_gpt_delay_ms(2);

    captouch_switch_debug_sel(channel);

    //printf("TOUCH_CON0:0x%x\r\n",captouch_analog_read_data(TOUCH_CON0));
    //printf("TOUCH_CON1:0x%x\r\n",captouch_analog_read_data(TOUCH_CON1));

    if (tune_type == HAL_CAPTOUCH_TUNE_MAN) {
        captouch_man_tune(channel,data);
        ret = HAL_CAPTOUCH_STATUS_OK;
    }
    else if (tune_type == HAL_CAPTOUCH_TUNE_HW_AUTO) {
        if (captouch_hw_auto_tune(channel,data) == true) {
            ret = HAL_CAPTOUCH_STATUS_OK;
        }
    }
    else if(tune_type == HAL_CAPTOUCH_TUNE_SW_AUTO) {
        if (captouch_sw_auto_tune(channel,data) == true) {
            ret = HAL_CAPTOUCH_STATUS_OK;
        }
    }
    captouch_tune_auto_control(true);
    hal_gpt_delay_ms(10);
    count = 0;
    temp[1] = captouch_9signed_to_16signed(backup[3]);
    while(1) {
        count++;
        hal_gpt_delay_ms(1);
        temp[0] = captouch_9signed_to_16signed(captouch_analog_read_data(TOUCH_AVG_DBG));
        log_hal_msgid_info("count=%d,temp[0]=%d,temp[1]=%d\r\n",3,count,temp[0],temp[1]);
        if (temp[0]<temp[1])break;
        if (count>800)break;
    }

    captouch_clk_control(captouch_disable);
    captouch_analog_write_data(TOUCH_CON0,backup[0]);
    captouch_analog_write_data(TOUCH_CH_WAKEUP_EN,backup[1]);
    captouch_analog_write_data(TOUCH_CH_INT_EN,backup[2]);
    captouch_clk_control(bak_en);
    //captouch_tune_auto_control(false);
    hal_gpt_delay_ms(2);

    captouch->TOUCH_HIF_CON0.CELLS.HIF_SOFT_RST = 1;
    hal_gpt_delay_ms(3);

    hal_nvic_enable_irq(CAP_TOUCH_IRQn);

    return ret;
}

hal_captouch_status_t hal_captouch_lowpower_control(hal_captouch_lowpower_type_t lowpower_type)
{
    uint16_t rdata;
    uint32_t i;

    captouch_control_pdn_clock(captouch_enable);
    captouch_clk_control(captouch_disable);

    if (HAL_CAPTOUCH_MODE_NORMAL == lowpower_type){

        //set mavg_s to 0x4
        rdata = captouch_analog_read_data(TOUCH_CON1);
        rdata &= ~(0x7+(0x7<<7)+(0xf<<3));
        rdata |= (5+(CAPTOUCH_ANA_TOUCH_CLK_32K<<7) + (10<<3));
        captouch_analog_write_data(TOUCH_CON1, rdata);

        //enable normal mode, enable OP normal mode
        rdata = TOUCH_DA_RST_N + TOUCH_EN_LP_NORMAL + TOUCH_EN_OP_LP_NORMAL;
        captouch_analog_write_data(TOUCH_LP_CON,rdata);
        hal_gpt_delay_us(40);

        //enable used channel sensing
        for (i=1;i<8;i++) {
            if (captouch_context.used_channel_map&(1<<i)) {
                hal_captouch_channel_enable((hal_captouch_channel_t)i);
            }
        }

        log_hal_msgid_info("[captouch]enter normal mode\r\n", 0);

    }
    else if (HAL_CAPTOUCH_MODE_LOWPOWER == lowpower_type) {

        //disable chanel 1~7 sensing, channel 0 is used to wakeup, so we do not disable it.
        captouch_channel_int_control(0xfe, captouch_disable);
        captouch_channel_sense_control(0xfe, captouch_disable);

        //enable lowpower mode, enable OP lowpower mode
        rdata = TOUCH_DA_RST_N + TOUCH_EN_LP_LOWPOWER + TOUCH_EN_OP_LP_LOWPOWER;
        captouch_analog_write_data(TOUCH_LP_CON,rdata);
        hal_gpt_delay_us(60);

        //set mavg_s to 0xa,switch to 1k clk
        rdata = captouch_analog_read_data(TOUCH_CON1);
        rdata &= ~(0x7+(0x7<<7)+(0xf<<3));
        rdata |= (4+(CAPTOUCH_ANA_TOUCH_CLK_1K<<7) + (10<<3));
        captouch_analog_write_data(TOUCH_CON1, rdata);

        log_hal_msgid_info("[captouch]enter lowpower mode\r\n", 0);

    }

    captouch_clk_control(captouch_enable);

    return HAL_CAPTOUCH_STATUS_OK;

}

hal_captouch_status_t hal_captouch_deinit(void)
{
    uint16_t rdata;
    hal_captouch_status_t ret;

    //enable wire3 clock source
    captouch_control_pdn_clock(captouch_enable);

    //disable channel interrupt
    captouch_channel_int_control(captouch_context.used_channel_map, captouch_disable);

    //disable the captouch inerrupt
    captouch_int_control(captouch_disable);

    //disable analog touch circuit, disable adc circuit, disable OP circuit
    rdata = captouch_analog_read_data(TOUCH_ANA_CFG0);
    rdata &= ~(TOUCH_ADC_EN + TOUCH_EN + TOUCH_EN_OP);
    captouch_analog_write_data(TOUCH_ANA_CFG0,rdata);

    //disable sensing
    captouch_channel_sense_control(captouch_context.used_channel_map, captouch_disable);
    hal_gpt_delay_ms(2);

    hal_nvic_disable_irq(CAP_TOUCH_IRQn);
    captouch->TOUCH_HIF_CON0.CELLS.HIF_SOFT_RST = 1;
    hal_gpt_delay_ms(3);
    hal_nvic_enable_irq(CAP_TOUCH_IRQn);

    hal_gpt_delay_ms(2);

    //close the captouch clock
    captouch_clk_control(captouch_disable);

    //close captouch rtc clock
    ret = captouch_rtc_clk_control(captouch_disable);
    if (ret != HAL_CAPTOUCH_STATUS_OK) {
        return ret;
    }
    //close wire3 clock source
    captouch_control_pdn_clock(captouch_disable);

	hal_nvic_disable_irq(CAP_TOUCH_IRQn);
	hal_nvic_clear_pending_irq(CAP_TOUCH_IRQn);

    captouch_context.has_initilized = false;

    memset(&captouch_context, 0, sizeof(captouch_context_t));

    return HAL_CAPTOUCH_STATUS_OK;
}



#endif //HAL_CAPTOUCH_MODULE_ENABLED

