/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_gpio.h"

#ifdef HAL_GPIO_MODULE_ENABLED
#include "hal_gpio_internal.h"
#include "hal_log.h"


const hal_gpio_cfg_reg_t gpio_cfg_table[] = {
    //DRV_REG         shift     IES_REG     shift       PD_REG      PU_REG      shift     PUPD_REG      R0_REG         R1_REG       shift        RDSEL_RG    shift    SMT_REG        SR_REG    shift
    {   0xa20e0000, 26, 0xA20E0024, 13, 0xa20e0030, 0xA20E003C, 13, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0048, 26, 0xa20e0060, 0xa20e006c, 13, },  // HAL_GPIO_0
    {   0xa20e0000, 28, 0xA20E0024, 14, 0xa20e0030, 0xA20E003C, 14, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0048, 28, 0xa20e0060, 0xa20e006c, 14, },  // HAL_GPIO_1
    {   0xa20e0000, 30, 0xA20E0024, 15, 0xa20e0030, 0xA20E003C, 15, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0048, 30, 0xa20e0060, 0xa20e006c, 15, },  // HAL_GPIO_2
    {   0xa20e000c, 0,  0xA20E0024, 16, 0xa20e0030, 0xA20E003C, 16, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 0,  0xa20e0060, 0xa20e006c, 16, },  // HAL_GPIO_3
    {   0xa20e000c, 2,  0xA20E0024, 17, 0xa20e0030, 0xA20E003C, 17, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 2,  0xa20e0060, 0xa20e006c, 17, },  // HAL_GPIO_4
    {   0xa20e000c, 4,  0xA20E0024, 18, 0xa20e0030, 0xA20E003C, 18, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 4,  0xa20e0060, 0xa20e006c, 18, },  // HAL_GPIO_5
    {   0xa20e000c, 6,  0xA20E0024, 19, 0xa20e0030, 0xA20E003C, 19, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 6,  0xa20e0060, 0xa20e006c, 19, },  // HAL_GPIO_6
    {   0xa20e000c, 16, 0xA20E0024, 24, 0xa20e0030, 0xA20E003C, 24, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 16, 0xa20e0060, 0xa20e006c, 24, },  // HAL_GPIO_7
    {   0xa20e000c, 18, 0xA20E0024, 25, 0xa20e0030, 0xA20E003C, 25, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 18, 0xa20e0060, 0xa20e006c, 25, },  // HAL_GPIO_8
    {   0xa20e000c, 20, 0xA20E0024, 26, 0xa20e0030, 0xA20E003C, 26, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 20, 0xa20e0060, 0xa20e006c, 26, },  // HAL_GPIO_9
    {   0xa20e000c, 22, 0xA20E0024, 27, 0xa20e0030, 0xA20E003C, 27, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 22, 0xa20e0060, 0xa20e006c, 27, },  // HAL_GPIO_10
    {   0xa20c0000, 24, 0xA20C0024, 12, 0xA20C003C, 0xA20C0060, 12, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 24, 0xa20c00b4, 0xa20c00cc, 12, },  // HAL_GPIO_11
    {   0xa20c0000, 26, 0xA20C0024, 13, 0xA20C003C, 0xA20C0060, 13, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 26, 0xa20c00b4, 0xa20c00cc, 13, },  // HAL_GPIO_12
    {   0xa20c0000, 28, 0xA20C0024, 14, 0xA20C003C, 0xA20C0060, 14, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 28, 0xa20c00b4, 0xa20c00cc, 14, },  // HAL_GPIO_13
    {   0xa20c000c, 6,  0xA20C0024, 19, 0xA20C003C, 0xA20C0060, 19, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 6,  0xa20c00b4, 0xa20c00cc, 19, },  // HAL_GPIO_14
    {   0xa20c000c, 8,  0xA20C0024, 20, 0xA20C003C, 0xA20C0060, 20, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 8,  0xa20c00b4, 0xa20c00cc, 20, },  // HAL_GPIO_15
    {   0xa20c000c, 10, 0xA20C0024, 21, 0xA20C003C, 0xA20C0060, 21, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 10, 0xa20c00b4, 0xa20c00cc, 21, },  // HAL_GPIO_16
    {   0xa20c000c, 12, 0xA20C0024, 22, 0xA20C003C, 0xA20C0060, 22, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 12, 0xa20c00b4, 0xa20c00cc, 22, },  // HAL_GPIO_17
    {   0xa20c000c, 14, 0xA20C0024, 23, 0xA20C003C, 0xA20C0060, 23, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 14, 0xa20c00b4, 0xa20c00cc, 23, },  // HAL_GPIO_18
    {   0xa20c000c, 16, 0xA20C0024, 24, 0xA20C003C, 0xA20C0060, 24, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 16, 0xa20c00b4, 0xa20c00cc, 24, },  // HAL_GPIO_19
    {   0xa20c000c, 18, 0xA20C0024, 25, 0xA20C003C, 0xA20C0060, 25, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 18, 0xa20c00b4, 0xa20c00cc, 25, },  // HAL_GPIO_20
    {   0xa20c000c, 20, 0xA20C0024, 26, 0xA20C003C, 0xA20C0060, 26, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 20, 0xa20c00b4, 0xa20c00cc, 26, },  // HAL_GPIO_21
    {   0xa20c000c, 22, 0xA20C0024, 27, 0xA20C003C, 0xA20C0060, 27, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 22, 0xa20c00b4, 0xa20c00cc, 27, },  // HAL_GPIO_22
    {   0xa20c000c, 24, 0xA20C0024, 28, 0xA20C003C, 0xA20C0060, 28, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 24, 0xa20c00b4, 0xa20c00cc, 28, },  // HAL_GPIO_23
    {   0xa20d0000, 20, 0xA20D0024, 10, 0xffffffff, 0xffffffff, 0xff,   0xa20d003c, 0xa20d0054, 0xa20d0060, 0,  0xa20d006c, 20, 0xa20d0084, 0xa20d0090, 10, },  // HAL_GPIO_24
    {   0xa20d0000, 22, 0xA20D0024, 11, 0xffffffff, 0xffffffff, 0xff,   0xa20d003c, 0xa20d0054, 0xa20d0060, 1,  0xa20d006c, 22, 0xa20d0084, 0xa20d0090, 11, },  // HAL_GPIO_25
    {   0xa20d0000, 24, 0xA20D0024, 12, 0xffffffff, 0xffffffff, 0xff,   0xa20d003c, 0xa20d0054, 0xa20d0060, 2,  0xa20d006c, 24, 0xa20d0084, 0xa20d0090, 12, },  // HAL_GPIO_26
    {   0xa20d0000, 26, 0xA20D0024, 13, 0xffffffff, 0xffffffff, 0xff,   0xa20d003c, 0xa20d0054, 0xa20d0060, 3,  0xa20d006c, 26, 0xa20d0084, 0xa20d0090, 13, },  // HAL_GPIO_27
    {   0xa20d0000, 28, 0xA20D0024, 14, 0xffffffff, 0xffffffff, 0xff,   0xa20d003c, 0xa20d0054, 0xa20d0060, 4,  0xa20d006c, 28, 0xa20d0084, 0xa20d0090, 14, },  // HAL_GPIO_28
    {   0xa20d0000, 30, 0xA20D0024, 15, 0xffffffff, 0xffffffff, 0xff,   0xa20d003c, 0xa20d0054, 0xa20d0060, 5,  0xa20d006c, 30, 0xa20d0084, 0xa20d0090, 15, },  // HAL_GPIO_29
    {   0xa20d000c, 0,  0xA20D0024, 16, 0xA20d0030, 0xA20d0048, 10, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20d0078, 0,  0xa20d0084, 0xa20d0090, 16, },  // HAL_GPIO_30
    {   0xa20d000c, 2,  0xA20D0024, 17, 0xA20d0030, 0xA20d0048, 11, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20d0078, 2,  0xa20d0084, 0xa20d0090, 17, },  // HAL_GPIO_31
    {   0xa20d000c, 4,  0xA20D0024, 18, 0xA20d0030, 0xA20d0048, 12, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20d0078, 4,  0xa20d0084, 0xa20d0090, 18, },  // HAL_GPIO_32
    {   0xa20d000c, 6,  0xA20D0024, 19, 0xA20d0030, 0xA20d0048, 13, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20d0078, 6,  0xa20d0084, 0xa20d0090, 19, },  // HAL_GPIO_33
    {   0xa20d000c, 8,  0xA20D0024, 20, 0xA20d0030, 0xA20d0048, 14, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20d0078, 8,  0xa20d0084, 0xa20d0090, 20, },  // HAL_GPIO_34
    {   0xa20d000c, 10, 0xA20D0024, 21, 0xA20d0030, 0xA20d0048, 15, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20d0078, 10, 0xa20d0084, 0xa20d0090, 21, },  // HAL_GPIO_35
    {   0xa20d000c, 12, 0xA20D0024, 22, 0xA20d0030, 0xA20d0048, 16, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20d0078, 12, 0xa20d0084, 0xa20d0090, 22, },  // HAL_GPIO_36
    {   0xa20e000c, 8,  0xA20E0024, 20, 0xa20e0030, 0xA20E003C, 20, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 8,  0xa20e0060, 0xa20e006c, 20, },  // HAL_GPIO_37
    {   0xa20e000c, 10, 0xA20E0024, 21, 0xa20e0030, 0xA20E003C, 21, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 10, 0xa20e0060, 0xa20e006c, 21, },  // HAL_GPIO_38
    {   0xa20e000c, 12, 0xA20E0024, 22, 0xa20e0030, 0xA20E003C, 22, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 12, 0xa20e0060, 0xa20e006c, 22, },  // HAL_GPIO_39
    {   0xa20e000c, 14, 0xA20E0024, 23, 0xa20e0030, 0xA20E003C, 23, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 14, 0xa20e0060, 0xa20e006c, 23, },  // HAL_GPIO_40
    {   0xa20e000c, 24, 0xA20E0024, 28, 0xa20e0030, 0xA20E003C, 28, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 24, 0xa20e0060, 0xa20e006c, 28, },  // HAL_GPIO_41
    {   0xa20e000c, 26, 0xA20E0024, 29, 0xa20e0030, 0xA20E003C, 29, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 26, 0xa20e0060, 0xa20e006c, 29, },  // HAL_GPIO_42
    {   0xa20e000c, 28, 0xA20E0024, 30, 0xa20e0030, 0xA20E003C, 30, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0054, 28, 0xa20e0060, 0xa20e006c, 30, },  // HAL_GPIO_43
    {   0xa20c0000, 0,  0xA20C0024, 0,  0xA20C003C, 0xA20C0060, 0,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 0,  0xa20c00b4, 0xa20c00cc, 0,  },  // HAL_GPIO_44
    {   0xa20c0000, 2,  0xA20C0024, 1,  0xA20C003C, 0xA20C0060, 1,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 2,  0xa20c00b4, 0xa20c00cc, 1,  },  // HAL_GPIO_45
    {   0xa20c0000, 4,  0xA20C0024, 2,  0xA20C003C, 0xA20C0060, 2,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 4,  0xa20c00b4, 0xa20c00cc, 2,  },  // HAL_GPIO_46
    {   0xa20c0000, 6,  0xA20C0024, 3,  0xA20C003C, 0xA20C0060, 3,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 6,  0xa20c00b4, 0xa20c00cc, 3,  },  // HAL_GPIO_47
    {   0xa20c0000, 8,  0xA20C0024, 4,  0xA20C003C, 0xA20C0060, 4,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 8,  0xa20c00b4, 0xa20c00cc, 4,  },  // HAL_GPIO_48
    {   0xa20c0000, 10, 0xA20C0024, 5,  0xA20C003C, 0xA20C0060, 5,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 10, 0xa20c00b4, 0xa20c00cc, 5,  },  // HAL_GPIO_49
    {   0xa20c0000, 12, 0xA20C0024, 6,  0xA20C003C, 0xA20C0060, 6,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 12, 0xa20c00b4, 0xa20c00cc, 6,  },  // HAL_GPIO_50
    {   0xa20c0000, 14, 0xA20C0024, 7,  0xA20C003C, 0xA20C0060, 7,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 14, 0xa20c00b4, 0xa20c00cc, 7,  },  // HAL_GPIO_51
    {   0xa20c0000, 16, 0xA20C0024, 8,  0xA20C003C, 0xA20C0060, 8,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 16, 0xa20c00b4, 0xa20c00cc, 8,  },  // HAL_GPIO_52
    {   0xa20c0000, 18, 0xA20C0024, 9,  0xA20C003C, 0xA20C0060, 9,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 18, 0xa20c00b4, 0xa20c00cc, 9,  },  // HAL_GPIO_53
    {   0xa20c0000, 20, 0xA20C0024, 10, 0xA20C003C, 0xA20C0060, 10, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 20, 0xa20c00b4, 0xa20c00cc, 10, },  // HAL_GPIO_54
    {   0xa20c0000, 22, 0xA20C0024, 11, 0xA20C003C, 0xA20C0060, 11, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 22, 0xa20c00b4, 0xa20c00cc, 11, },  // HAL_GPIO_55
    {   0xa20c0000, 30, 0xA20C0024, 15, 0xA20C003C, 0xA20C0060, 16, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c0090, 30, 0xa20c00b4, 0xa20c00cc, 15, },  // HAL_GPIO_56
    {   0xa20c000c, 0,  0xA20C0024, 16, 0xA20C003C, 0xA20C0060, 17, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 0,  0xa20c00b4, 0xa20c00cc, 16, },  // HAL_GPIO_57
    {   0xa20c000c, 2,  0xA20C0024, 17, 0xA20C003C, 0xA20C0060, 18, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 2,  0xa20c00b4, 0xa20c00cc, 17, },  // HAL_GPIO_58
    {   0xa20c000c, 4,  0xA20C0024, 18, 0xA20C003C, 0xA20C0060, 19, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 4,  0xa20c00b4, 0xa20c00cc, 18, },  // HAL_GPIO_59
    {   0xa20c000c, 26, 0xA20C0024, 29, 0xA20C003C, 0xA20C0060, 29, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 26, 0xa20c00b4, 0xa20c00cc, 29, },  // HAL_GPIO_60
    {   0xa20c000c, 28, 0xA20C0024, 30, 0xA20C003C, 0xA20C0060, 30, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 28, 0xa20c00b4, 0xa20c00cc, 30, },  // HAL_GPIO_61
    {   0xa20c000c, 30, 0xA20C0024, 31, 0xA20C003C, 0xA20C0060, 31, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c009c, 30, 0xa20c00b4, 0xa20c00cc, 31, },  // HAL_GPIO_62
    {   0xa20c0018, 0,  0xA20C0030, 0,  0xA20C0048, 0xA20C006c, 0,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c00a8, 0,  0xa20c00c0, 0xa20c00d8, 0,  },  // HAL_GPIO_63
    {   0xa20c0018, 2,  0xA20C0030, 1,  0xA20C0048, 0xA20C006c, 1,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c00a8, 2,  0xa20c00c0, 0xa20c00d8, 1,  },  // HAL_GPIO_64
    {   0xa20c0018, 4,  0xA20C0030, 2,  0xA20C0048, 0xA20C006c, 2,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20c00a8, 4,  0xa20c00c0, 0xa20c00d8, 2,  },  // HAL_GPIO_65
    {   0xa20d0000, 8,  0xA20D0024, 4,  0xA20d0030, 0xA20d0048, 4,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20d006c, 8,  0xa20d0084, 0xa20d0090, 4,  },  // HAL_GPIO_66
    {   0xa20d0000, 10, 0xA20D0024, 5,  0xA20d0030, 0xA20d0048, 5,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20d006c, 10, 0xa20d0084, 0xa20d0090, 5,  },  // HAL_GPIO_67
    {   0xa20d0000, 12, 0xA20D0024, 6,  0xA20d0030, 0xA20d0048, 6,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20d006c, 12, 0xa20d0084, 0xa20d0090, 6,  },  // HAL_GPIO_68
    {   0xa20d0000, 14, 0xA20D0024, 7,  0xA20d0030, 0xA20d0048, 7,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20d006c, 14, 0xa20d0084, 0xa20d0090, 7,  },  // HAL_GPIO_69
    {   0xa20d0000, 16, 0xA20D0024, 8,  0xA20d0030, 0xA20d0048, 8,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20d006c, 16, 0xa20d0084, 0xa20d0090, 8,  },  // HAL_GPIO_70
    {   0xa20d0000, 18, 0xA20D0024, 9,  0xA20d0030, 0xA20d0048, 9,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20d006c, 18, 0xa20d0084, 0xa20d0090, 9,  },  // HAL_GPIO_71
    {   0xa20e0000, 24, 0xA20E0024, 12, 0xa20e0030, 0xA20E003C, 12, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0048, 24, 0xa20e0060, 0xa20e006c, 12, },  // HAL_GPIO_72
    {   0xa20c0018, 6,  0xA20C0030, 3,  0xffffffff, 0xffffffff, 0xff,   0xa20c0054, 0xa20c0078, 0xa20c0084, 0,  0xa20c00a8, 6,  0xa20c00c0, 0xa20c00d8, 3,  },  // HAL_GPIO_73
    {   0xa20c0018, 8,  0xA20C0030, 4,  0xffffffff, 0xffffffff, 0xff,   0xa20c0054, 0xa20c0078, 0xa20c0084, 1,  0xa20c00a8, 8,  0xa20c00c0, 0xa20c00d8, 4,  },  // HAL_GPIO_74
    {   0xa20c0018, 10, 0xA20C0030, 5,  0xffffffff, 0xffffffff, 0xff,   0xa20c0054, 0xa20c0078, 0xa20c0084, 2,  0xa20c00a8, 10, 0xa20c00c0, 0xa20c00d8, 5,  },  // HAL_GPIO_75
    {   0xa20c0018, 12, 0xA20C0030, 6,  0xffffffff, 0xffffffff, 0xff,   0xa20c0054, 0xa20c0078, 0xa20c0084, 3,  0xa20c00a8, 12, 0xa20c00c0, 0xa20c00d8, 6,  },  // HAL_GPIO_76
    {   0xa20c0018, 14, 0xA20C0030, 7,  0xffffffff, 0xffffffff, 0xff,   0xa20c0054, 0xa20c0078, 0xa20c0084, 4,  0xa20c00a8, 14, 0xa20c00c0, 0xa20c00d8, 7,  },  // HAL_GPIO_77
    {   0xa20c0018, 16, 0xA20C0030, 8,  0xffffffff, 0xffffffff, 0xff,   0xa20c0054, 0xa20c0078, 0xa20c0084, 5,  0xa20c00a8, 16, 0xa20c00c0, 0xa20c00d8, 8,  },  // HAL_GPIO_78
    {   0xa20c0018, 18, 0xA20C0030, 9,  0xffffffff, 0xffffffff, 0xff,   0xa20c0054, 0xa20c0078, 0xa20c0084, 6,  0xa20c00a8, 18, 0xa20c00c0, 0xa20c00d8, 9,  },  // HAL_GPIO_79
    {   0xa20e0000, 12, 0xA20E0024, 6,  0xa20e0030, 0xA20E003C, 6,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0048, 12, 0xa20e0060, 0xa20e006c, 6,  },  // HAL_GPIO_80
    {   0xa20e0000, 14, 0xA20E0024, 7,  0xa20e0030, 0xA20E003C, 7,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0048, 14, 0xa20e0060, 0xa20e006c, 7,  },  // HAL_GPIO_81
    {   0xa20e0000, 16, 0xA20E0024, 8,  0xa20e0030, 0xA20E003C, 8,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0048, 16, 0xa20e0060, 0xa20e006c, 8,  },  // HAL_GPIO_82
    {   0xa20e0000, 18, 0xA20E0024, 9,  0xa20e0030, 0xA20E003C, 9,  0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0048, 18, 0xa20e0060, 0xa20e006c, 9,  },  // HAL_GPIO_83
    {   0xa20e0000, 20, 0xA20E0024, 10, 0xa20e0030, 0xA20E003C, 10, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0048, 20, 0xa20e0060, 0xa20e006c, 10, },  // HAL_GPIO_84
    {   0xa20e0000, 22, 0xA20E0024, 11, 0xa20e0030, 0xA20E003C, 11, 0xffffffff, 0xffffffff, 0xffffffff, 0xff,   0xa20e0048, 22, 0xa20e0060, 0xa20e006c, 11, }   // HAL_GPIO_85
};

void gpio_get_state(hal_gpio_pin_t gpio_pin, gpio_state_t *gpio_state)
{

    uint32_t mode;
    uint32_t dir;
    uint32_t din;
    uint32_t dout;
    uint32_t pu;
    uint32_t pd;
    uint32_t pupd;
    uint32_t r0;
    uint32_t r1;

    gpio_pull_type_t pull_type;
    uint32_t temp;
    uint32_t shift;
    uint32_t reg_index;
    uint32_t bit_index;
    hal_gpio_driving_current_t driving_value;

    //const char *direct[2] = {"input", "output"};
    //const char *pull_state[10] = {"disable_pull", "PU_R", "PD_R", "PU_R0","PD_R0", "PU_R0_R1", "PD_R0_R1", "PUPD_Error","PU_R1","PD_R1"};


    reg_index = gpio_pin / 8;
    bit_index = (gpio_pin % 8) * 4;
    mode = (gpio_base->GPIO_MODE.RW[reg_index] >> (bit_index) & 0xf);

    reg_index = gpio_pin / 32;
    bit_index = gpio_pin % 32;
    dir  = (gpio_base->GPIO_DIR.RW[reg_index] >> (bit_index) & 0x1);
    din  = (gpio_base->GPIO_DIN.R[reg_index] >> (bit_index) & 0x1);
    dout = (gpio_base->GPIO_DOUT.RW[reg_index] >> (bit_index) & 0x1);

    pu = 0xf;
    pd = 0xf;
    pupd = 0xf;
    r0   = 0xf;
    r1   = 0xf;

    shift = 0xff;
    pull_type = GPIO_PUPD_ERR;

    if (gpio_cfg_table[gpio_pin].pupd_shift != 0xff) {
        shift = gpio_cfg_table[gpio_pin].pupd_shift;
        pu = (GPIO_REG32(gpio_cfg_table[gpio_pin].pu_reg) >> shift) & 0x01;
        pd = (GPIO_REG32(gpio_cfg_table[gpio_pin].pd_reg) >> shift) & 0x01;

        temp = (pu << 4) + pd;

        //printf("pu=%d pd=%d, temp=%.3x\r\n",pu,pd,temp);

        if (temp == 0x00) {
            pull_type = GPIO_NO_PULL;
        } else if (temp == 0x10) {
            pull_type = GPIO_PU_R;
        } else if (temp == 0x01) {
            pull_type = GPIO_PD_R;
        } else if (temp == 0xff) {
            pull_type = GPIO_PUPD_ERR;
            log_hal_msgid_info("error pu = %x, pd= %x\r\n",2, pu, pd);
        }
    } else if (gpio_cfg_table[gpio_pin].pupd_r0_r1_shift != 0xff) {
        shift = gpio_cfg_table[gpio_pin].pupd_r0_r1_shift;
        pupd = (GPIO_REG32(gpio_cfg_table[gpio_pin].pupd_reg) >> shift) & 0x01;
        r0 = (GPIO_REG32(gpio_cfg_table[gpio_pin].r0_reg) >> shift) & 0x01;
        r1 = (GPIO_REG32(gpio_cfg_table[gpio_pin].r1_reg) >> shift) & 0x01;

        temp = (pupd << 8) + (r0 << 4) + r1;
        //printf("pupd=%d r0=%d, r1=%d, temp=%.3x\r\n",pupd,r0,r1,temp);

        if (temp == 0x010) {
            pull_type = GPIO_PU_R0;
        } else if (temp == 0x001) {
            pull_type = GPIO_PU_R1;
        } else if (temp == 0x110) {
            pull_type = GPIO_PD_R0;
        } else if (temp == 0x101) {
            pull_type = GPIO_PD_R1;
        } else if (temp == 0x011) {
            pull_type = GPIO_PU_R0_R1;
        } else if (temp == 0x111) {
            pull_type = GPIO_PD_R0_R1;
        } else if ((temp == 0x100) || (temp == 0x000)) {
            pull_type = GPIO_NO_PULL;
        } else if (temp == 0xfff) {
            pull_type = GPIO_PUPD_ERR;
            log_hal_msgid_info("error pupd-r0-r1 = %x\r\n",1, temp);
        }

    }


    hal_gpio_get_driving_current((hal_gpio_pin_t)gpio_pin, &driving_value);

    gpio_state->mode = mode;
    gpio_state->dir  = dir;
    gpio_state->din  = din;
    gpio_state->dout = dout;
    gpio_state->pull_type = pull_type;
    gpio_state->current_type = (uint8_t)driving_value;

    //printf("GPIO%d, mode=%d, %s, din=%d, dout=%d, %s\r\n",gpio_pin, mode, direct[dir], din,dout,pull_state[pull_type]);

    // dvt_eint_pirntf("%d-GPIO MODE0      = 0x%x\r\n", index, REG32(GPIO_MODE0));
    // dvt_eint_pirntf("%d-GPIO DOUT0      = 0x%x\r\n", index, REG32(GPIO_DOUT0));
    // dvt_eint_pirntf("%d-GPIO PUPD_CFG0  = 0x%x\r\n", index, REG32(PUPD_CFG0));
    // dvt_eint_pirntf("%d-GPIO R0_CFG0    = 0x%x\r\n", index, REG32(R0_CFG0));
    // dvt_eint_pirntf("%d-GPIO R1_CFG0    = 0x%x\r\n", index, REG32(R1_CFG0));
    // dvt_eint_pirntf("\r\n");

}

#endif

