/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_isink.h"
#include "hal_pmu.h"
#include "hal_platform.h"
#include "hal_isink_internal.h"
#include "hal_log.h"
#include "hal_nvic.h"
#include "hal_spm.h"
#ifdef HAL_ISINK_MODULE_ENABLED

/*if want isink alive when system in sleep, please enable this macro*/
#define     HAL_ISINK_SLEEP_ALIVE_ENABLED

extern pmu_operate_status_t pmu_set_register_value_2byte(unsigned short int address, unsigned short int mask, unsigned short int shift, unsigned short int value);
extern uint32_t pmu_get_register_value_2byte(unsigned short int address, unsigned short int mask, unsigned short int shift);

#ifdef HAL_ISINK_SLEEP_ALIVE_ENABLED
static  int    g_isink_channel_state = 0;
#endif

hal_isink_status_t  hal_isink_init(hal_isink_channel_t  channel)
{
    bool    status = true;

    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value_2byte(PMU_ISINK_MODE_CTRL, PMU_ISINK_CH0_MODE_MASK,PMU_ISINK_CH0_MODE_SHIFT, HAL_ISINK_MODE_REGISTER);
            status &= pmu_set_register_value_2byte(PMU_CKCFG2, PMU_RG_DRV_ISINK0_CK_PDN_MASK,   PMU_RG_DRV_ISINK0_CK_PDN_SHIFT,0);
            status &= pmu_set_register_value_2byte(PMU_RG_BUCK_F2M_CK_PDN_ADDR,PMU_RG_BUCK_F2M_CK_PDN_MASK,PMU_RG_BUCK_F2M_CK_PDN_SHIFT,0);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value_2byte(PMU_ISINK_MODE_CTRL, PMU_ISINK_CH1_MODE_MASK,PMU_ISINK_CH1_MODE_SHIFT, HAL_ISINK_MODE_REGISTER);
            status &= pmu_set_register_value_2byte(PMU_CKCFG2, PMU_RG_DRV_ISINK1_CK_PDN_MASK,   PMU_RG_DRV_ISINK1_CK_PDN_SHIFT, 0);
            status &= pmu_set_register_value_2byte(PMU_RG_BUCK_F2M_CK_PDN_ADDR,PMU_RG_BUCK_F2M_CK_PDN_MASK,PMU_RG_BUCK_F2M_CK_PDN_SHIFT,0);
            break;
        default:
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }

#ifdef HAL_ISINK_SLEEP_ALIVE_ENABLED
{
    uint32_t save_mask;

    hal_nvic_save_and_set_interrupt_mask(&save_mask);
    #ifdef HAL_SLEEP_MANAGER_ENABLED
    spm_force_on_pmic(SPM_ISINK_REQUEST,SPM_ENABLE);
    #endif
    g_isink_channel_state |= 1<<channel;
    hal_nvic_restore_interrupt_mask(save_mask);
    log_hal_msgid_info("[hal][isink] turn on pmic pwr protect\r\n", 0);
}
#endif
    if (status != true) {
        return HAL_ISINK_STATUS_ERROR;
    }
    else {
        return HAL_ISINK_STATUS_OK;
    }
}

hal_isink_status_t  hal_isink_deinit(hal_isink_channel_t channel)
{
    bool    status = true;

    /*init default mode setting, and power down*/
    switch(channel){
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, PMU_ISINK_CH0_EN_MASK, PMU_ISINK_CH0_EN_SHIFT, 0);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, PMU_ISINK_CH0_BIAS_EN_MASK, PMU_ISINK_CH0_BIAS_EN_SHIFT,0);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, PMU_ISINK_CHOP0_EN_MASK,    PMU_ISINK_CHOP0_EN_SHIFT, 0);
            status &= pmu_set_register_value_2byte(PMU_DRIVER_ANA_CON0,PMU_RG_ISINK0_DOUBLE_MASK, PMU_RG_ISINK0_DOUBLE_SHIFT, 0);
            status &= pmu_set_register_value_2byte(PMU_ISINK_MODE_CTRL,PMU_ISINK_CH0_MODE_MASK,   PMU_ISINK_CH0_MODE_SHIFT, HAL_ISINK_MODE_PWM);
            /*power down isink0*/
            status &= pmu_set_register_value_2byte(PMU_CKCFG2, PMU_RG_DRV_ISINK0_CK_PDN_MASK, PMU_RG_DRV_ISINK0_CK_PDN_SHIFT, 1);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, PMU_ISINK_CH1_EN_MASK,     PMU_ISINK_CH1_EN_SHIFT, 0);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, PMU_ISINK_CH1_BIAS_EN_MASK,PMU_ISINK_CH1_BIAS_EN_SHIFT, 0);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, PMU_ISINK_CHOP1_EN_MASK,   PMU_ISINK_CHOP1_EN_SHIFT, 0);
            status &= pmu_set_register_value_2byte(PMU_DRIVER_ANA_CON0,PMU_RG_ISINK1_DOUBLE_MASK,PMU_RG_ISINK1_DOUBLE_SHIFT, 0);
            status &= pmu_set_register_value_2byte(PMU_ISINK_MODE_CTRL,PMU_ISINK_CH1_MODE_MASK,  PMU_ISINK_CH1_MODE_SHIFT, HAL_ISINK_MODE_PWM);
            /*power down isink1*/
            status &= pmu_set_register_value_2byte(PMU_CKCFG2, PMU_RG_DRV_ISINK1_CK_PDN_MASK, PMU_RG_DRV_ISINK1_CK_PDN_SHIFT, 1);
            break;
        default:
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }
#ifdef HAL_ISINK_SLEEP_ALIVE_ENABLED
    uint32_t save_mask;

    hal_nvic_save_and_set_interrupt_mask(&save_mask);
    g_isink_channel_state &= ~(1<<channel);
    hal_nvic_restore_interrupt_mask(save_mask);

    if(g_isink_channel_state == 0){
        #ifdef HAL_SLEEP_MANAGER_ENABLED
        spm_force_on_pmic(SPM_ISINK_REQUEST,SPM_DISABLE);
        #endif
        log_hal_msgid_info("[hal][isink] turn off pmic pwr protect\r\n", 0);
    }
#endif
    if (status == false) {
        return HAL_ISINK_STATUS_ERROR;
    }
    else {
        return HAL_ISINK_STATUS_OK;
    }
}

hal_isink_status_t  hal_isink_set_clock_source(hal_isink_channel_t channel, hal_isink_clock_source_t source_clock)
{
    return HAL_ISINK_STATUS_OK;
}

hal_isink_status_t  hal_isink_set_mode(hal_isink_channel_t channel, hal_isink_mode_t mode)
{
    bool    status = true;

    if(mode > HAL_ISINK_MODE_REGISTER){
        return HAL_ISINK_STATUS_ERROR_INVALID_PARAMETER;
    }

    switch(channel){
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value_2byte(PMU_ISINK_MODE_CTRL, PMU_ISINK_CH0_MODE_MASK, PMU_ISINK_CH0_MODE_SHIFT, (uint16_t)mode);
        break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value_2byte(PMU_ISINK_MODE_CTRL, PMU_ISINK_CH1_MODE_MASK, PMU_ISINK_CH1_MODE_SHIFT, (uint16_t)mode);
        break;
        default:
            return HAL_ISINK_STATUS_ERROR_INVALID_PARAMETER;
    }

    if(mode == HAL_ISINK_MODE_PWM) {
        if(channel == HAL_ISINK_CHANNEL_0){
            //status &= pmu_set_register_value_2byte(PMU_ISINK0_CON1, PMU_ISINK_DIM0_DUTY_MASK, PMU_ISINK_DIM0_DUTY_SHIFT,15);
            //status &= pmu_set_register_value_2byte(PMU_ISINK0_CON0, PMU_ISINK_DIM0_FSEL_MASK, PMU_ISINK_DIM0_FSEL_SHIFT, 4);
            //status &= pmu_set_register_value_2byte(PMU_ISINK_MODE_CTRL,PMU_ISINK0_PWM_MODE_MASK, PMU_ISINK0_PWM_MODE_SHIFT, 1);
        }
        else if(channel == HAL_ISINK_CHANNEL_1){
            //status &= pmu_set_register_value_2byte(PMU_ISINK1_CON1, PMU_ISINK_DIM1_DUTY_MASK, PMU_ISINK_DIM1_DUTY_SHIFT,15);
            //status &= pmu_set_register_value_2byte(PMU_ISINK1_CON0, PMU_ISINK_DIM1_FSEL_MASK, PMU_ISINK_DIM1_FSEL_SHIFT, 4);
            //status &= pmu_set_register_value_2byte(PMU_ISINK_MODE_CTRL,PMU_ISINK1_PWM_MODE_MASK, PMU_ISINK1_PWM_MODE_SHIFT, 1);
        }
        else{
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
        }
    }

    if(status == false){
        return HAL_ISINK_STATUS_ERROR;
    }
    else{
        return HAL_ISINK_STATUS_OK;
    }
}

hal_isink_status_t  hal_isink_set_step_current(hal_isink_channel_t channel, hal_isink_current_t current)
{
    bool status = true;

    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value_2byte(PMU_ISINK0_CON1, PMU_ISINK_CH0_STEP_MASK, PMU_ISINK_CH0_STEP_SHIFT, (uint16_t)current);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,    PMU_ISINK_CH0_BIAS_EN_MASK,PMU_ISINK_CH0_BIAS_EN_SHIFT,1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,    PMU_ISINK_CHOP0_EN_MASK,   PMU_ISINK_CHOP0_EN_SHIFT,   1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,    PMU_ISINK_CH0_EN_MASK,     PMU_ISINK_CH0_EN_SHIFT,     1);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value_2byte(PMU_ISINK1_CON1, PMU_ISINK_CH1_STEP_MASK, PMU_ISINK_CH1_STEP_SHIFT, (uint16_t)current);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, PMU_ISINK_CH1_BIAS_EN_MASK,PMU_ISINK_CH1_BIAS_EN_SHIFT,  1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, PMU_ISINK_CHOP1_EN_MASK, PMU_ISINK_CHOP1_EN_SHIFT, 1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, PMU_ISINK_CH1_EN_MASK,   PMU_ISINK_CH1_EN_SHIFT,    1);

            break;
        default:
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }
    if (true == status) {
        return HAL_ISINK_STATUS_OK;
    }
    else {
        return HAL_ISINK_STATUS_ERROR;
    }
}

hal_isink_status_t  hal_isink_set_double_current(hal_isink_channel_t channel, hal_isink_current_t current)
{

    bool status = true;
    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value_2byte(PMU_ISINK0_CON1,     PMU_ISINK_CH0_STEP_MASK,   PMU_ISINK_CH0_STEP_SHIFT,  (uint16_t)current);
            status &= pmu_set_register_value_2byte(PMU_DRIVER_ANA_CON0, PMU_RG_ISINK0_DOUBLE_MASK, PMU_RG_ISINK0_DOUBLE_SHIFT, 1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,   PMU_ISINK_CH0_BIAS_EN_MASK,PMU_ISINK_CH0_BIAS_EN_SHIFT,1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,   PMU_ISINK_CHOP0_EN_MASK,   PMU_ISINK_CHOP0_EN_SHIFT,   1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,   PMU_ISINK_CH0_EN_MASK,     PMU_ISINK_CH0_EN_SHIFT,     1);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value_2byte(PMU_ISINK1_CON1,     PMU_ISINK_CH1_STEP_MASK,   PMU_ISINK_CH1_STEP_SHIFT,   (uint16_t)current);
            status &= pmu_set_register_value_2byte(PMU_DRIVER_ANA_CON0, PMU_RG_ISINK1_DOUBLE_MASK, PMU_RG_ISINK1_DOUBLE_SHIFT, 1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,   PMU_ISINK_CH1_BIAS_EN_MASK,PMU_ISINK_CH1_BIAS_EN_SHIFT,1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,   PMU_ISINK_CHOP1_EN_MASK,   PMU_ISINK_CHOP1_EN_SHIFT,   1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,   PMU_ISINK_CH1_EN_MASK,     PMU_ISINK_CH1_EN_SHIFT,     1);
            break;
        default:
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }

    if (true == status) {
        return HAL_ISINK_STATUS_OK;
    }
    else {
        return HAL_ISINK_STATUS_ERROR;
    }
}

hal_isink_status_t  hal_isink_enable_breath_mode(hal_isink_channel_t channel, hal_isink_breath_mode_t breath_mode)
{

    bool status = true;

    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            status &=pmu_set_register_value_2byte(PMU_ISINK0_CON2, PMU_ISINK_BREATH0_TR1_SEL_MASK, PMU_ISINK_BREATH0_TR1_SEL_SHIFT, breath_mode.darker_to_lighter_time1);
            status &=pmu_set_register_value_2byte(PMU_ISINK0_CON2, PMU_ISINK_BREATH0_TR2_SEL_MASK, PMU_ISINK_BREATH0_TR2_SEL_SHIFT, breath_mode.darker_to_lighter_time2);
            status &=pmu_set_register_value_2byte(PMU_ISINK0_CON2, PMU_ISINK_BREATH0_TF1_SEL_MASK, PMU_ISINK_BREATH0_TF1_SEL_SHIFT, breath_mode.lighter_to_darker_time1);
            status &=pmu_set_register_value_2byte(PMU_ISINK0_CON2, PMU_ISINK_BREATH0_TF2_SEL_MASK, PMU_ISINK_BREATH0_TF2_SEL_SHIFT, breath_mode.lighter_to_darker_time2);
            status &=pmu_set_register_value_2byte(PMU_ISINK0_CON3, PMU_ISINK_BREATH0_TON_SEL_MASK, PMU_ISINK_BREATH0_TON_SEL_SHIFT, breath_mode.lightest_time);
            status &=pmu_set_register_value_2byte(PMU_ISINK0_CON3, PMU_ISINK_BREATH0_TOFF_SEL_MASK, PMU_ISINK_BREATH0_TOFF_SEL_SHIFT, breath_mode.darkest_time);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &=pmu_set_register_value_2byte(PMU_ISINK1_CON2, PMU_ISINK_BREATH1_TR1_SEL_MASK, PMU_ISINK_BREATH1_TR1_SEL_SHIFT, breath_mode.darker_to_lighter_time1);
            status &=pmu_set_register_value_2byte(PMU_ISINK1_CON2, PMU_ISINK_BREATH1_TR2_SEL_MASK, PMU_ISINK_BREATH1_TR2_SEL_SHIFT, breath_mode.darker_to_lighter_time2);
            status &=pmu_set_register_value_2byte(PMU_ISINK1_CON2, PMU_ISINK_BREATH1_TF1_SEL_MASK, PMU_ISINK_BREATH1_TF1_SEL_SHIFT, breath_mode.lighter_to_darker_time1);
            status &=pmu_set_register_value_2byte(PMU_ISINK1_CON2, PMU_ISINK_BREATH1_TF2_SEL_MASK, PMU_ISINK_BREATH1_TF2_SEL_SHIFT, breath_mode.lighter_to_darker_time2);
            status &=pmu_set_register_value_2byte(PMU_ISINK1_CON3, PMU_ISINK_BREATH1_TON_SEL_MASK, PMU_ISINK_BREATH1_TON_SEL_SHIFT, breath_mode.lightest_time);
            status &=pmu_set_register_value_2byte(PMU_ISINK1_CON3, PMU_ISINK_BREATH1_TOFF_SEL_MASK, PMU_ISINK_BREATH1_TOFF_SEL_SHIFT, breath_mode.darkest_time);
            break;
        default :
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }

    if (true == status) {
        return HAL_ISINK_STATUS_OK;
    } else {
        return HAL_ISINK_STATUS_ERROR;
    }

}

hal_isink_status_t  hal_isink_get_running_status(hal_isink_channel_t channel, hal_isink_running_status_t *running_status)
{
    uint32_t    power_st= 1;
    uint32_t    en_flg  = 0;
    uint32_t    mask    = 0;
    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            power_st = pmu_get_register_value_2byte(PMU_CKCFG2, PMU_RG_DRV_ISINK0_CK_PDN_MASK, PMU_RG_DRV_ISINK0_CK_PDN_SHIFT);
            en_flg   = pmu_get_register_value_2byte(PMU_ISINK_EN_CTRL, 0xFF, 0);
            mask     = 0x15;
            //value = pmu_get_register_value_2byte(PMU_ISINK1_CON1, PMU_ISINK_CH1_STEP_MASK, PMU_ISINK_CH1_STEP_SHIFT);
            //status = pmu_get_register_value_2byte(PMU_ISINK_EN_CTRL, 0xFF, 0);
            //printf("ISINK1( CON1= %d EN_CTRL= %d)\r\n",value, status);
            break;
        case HAL_ISINK_CHANNEL_1:
            power_st = pmu_get_register_value_2byte(PMU_CKCFG2, PMU_RG_DRV_ISINK1_CK_PDN_MASK, PMU_RG_DRV_ISINK1_CK_PDN_SHIFT);
            en_flg   = pmu_get_register_value_2byte(PMU_ISINK_EN_CTRL, 0xFF, 0);
            mask     = 0x2A;
            break;
        default:
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }

    if (power_st == 0 && en_flg == mask) {
        *running_status = HAL_ISINK_BUSY;
    } else {
        *running_status = HAL_ISINK_IDLE;
    }
    return HAL_ISINK_STATUS_OK;
}

hal_isink_status_t  hal_isink_enable_pwm_mode(hal_isink_channel_t channel, uint32_t cycle_ms, uint32_t duty_persent)
{
    uint32_t    freq_val = 0;
    uint32_t    duty_val = 0;
    bool        status = true;

    freq_val = cycle_ms;
    duty_val = (duty_persent*32)/100;
    freq_val = (freq_val == 0)?1:freq_val;
    duty_val = (duty_val == 0)?1:duty_val;
    if(channel == HAL_ISINK_CHANNEL_0){
        status &= pmu_set_register_value_2byte(PMU_ISINK0_CON1,    PMU_ISINK_DIM0_DUTY_MASK, PMU_ISINK_DIM0_DUTY_SHIFT, (duty_val-1));
        status &= pmu_set_register_value_2byte(PMU_ISINK0_CON0,    PMU_ISINK_DIM0_FSEL_MASK, PMU_ISINK_DIM0_FSEL_SHIFT, (freq_val-1));
        status &= pmu_set_register_value_2byte(PMU_ISINK_MODE_CTRL,PMU_ISINK0_PWM_MODE_MASK, PMU_ISINK0_PWM_MODE_SHIFT, 0);
    }
    else if(channel == HAL_ISINK_CHANNEL_1){
        status &= pmu_set_register_value_2byte(PMU_ISINK1_CON1,     PMU_ISINK_DIM1_DUTY_MASK, PMU_ISINK_DIM1_DUTY_SHIFT, (duty_val-1));
        status &= pmu_set_register_value_2byte(PMU_ISINK1_CON0,     PMU_ISINK_DIM1_FSEL_MASK, PMU_ISINK_DIM1_FSEL_SHIFT, (freq_val-1));
        status &= pmu_set_register_value_2byte(PMU_ISINK_MODE_CTRL, PMU_ISINK1_PWM_MODE_MASK, PMU_ISINK1_PWM_MODE_SHIFT, 0);
    }
    if (true == status) {
        return HAL_ISINK_STATUS_OK;
    } else {
        return HAL_ISINK_STATUS_ERROR;
    }
}

hal_isink_status_t	hal_isink_start(hal_isink_channel_t channel)
{
    bool  status = true;
    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,   PMU_ISINK_CH0_BIAS_EN_MASK, PMU_ISINK_CH0_BIAS_EN_SHIFT,1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,   PMU_ISINK_CHOP0_EN_MASK,    PMU_ISINK_CHOP0_EN_SHIFT,   1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,   PMU_ISINK_CH0_EN_MASK,      PMU_ISINK_CH0_EN_SHIFT,     1);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, 	PMU_ISINK_CH1_BIAS_EN_MASK,	PMU_ISINK_CH1_BIAS_EN_SHIFT, 1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, 	PMU_ISINK_CHOP1_EN_MASK, 	PMU_ISINK_CHOP1_EN_SHIFT,    1);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, 	PMU_ISINK_CH1_EN_MASK,   	PMU_ISINK_CH1_EN_SHIFT,      1);
            break;
        default:
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }
    return HAL_ISINK_STATUS_OK;
}

hal_isink_status_t	hal_isink_stop(hal_isink_channel_t channel)
{
    bool  status = true;
    switch (channel) {
        case HAL_ISINK_CHANNEL_0:
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,    PMU_ISINK_CH0_BIAS_EN_MASK,PMU_ISINK_CH0_BIAS_EN_SHIFT,0);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,    PMU_ISINK_CHOP0_EN_MASK,   PMU_ISINK_CHOP0_EN_SHIFT,   0);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL,    PMU_ISINK_CH0_EN_MASK,     PMU_ISINK_CH0_EN_SHIFT,     0);
            break;
        case HAL_ISINK_CHANNEL_1:
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, PMU_ISINK_CH1_BIAS_EN_MASK,PMU_ISINK_CH1_BIAS_EN_SHIFT,  0);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, PMU_ISINK_CHOP1_EN_MASK, PMU_ISINK_CHOP1_EN_SHIFT, 0);
            status &= pmu_set_register_value_2byte(PMU_ISINK_EN_CTRL, PMU_ISINK_CH1_EN_MASK,   PMU_ISINK_CH1_EN_SHIFT,   0);
            break;
        default:
            return HAL_ISINK_STATUS_ERROR_CHANNEL;
    }
    return HAL_ISINK_STATUS_OK;
}

void hal_isink_dump_register()
{
	log_hal_msgid_info("===========================================\r\n", 0);
	log_hal_msgid_info("ISINK0_CON0 = %x, ISINK0_CON1= %x, ISINK0_CON2= %x, ISINK0_CON3= %x", 4, \
                   (unsigned int) pmu_get_register_value_2byte(PMU_ISINK0_CON0, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value_2byte(PMU_ISINK0_CON1, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value_2byte(PMU_ISINK0_CON2, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value_2byte(PMU_ISINK0_CON3, 0xFFFF, 0));
	log_hal_msgid_info("ISINK1_CON0 = %x, ISINK1_CON1= %x, ISINK1_CON2= %x, ISINK1_CON3= %x", 4, \
                   (unsigned int) pmu_get_register_value_2byte(PMU_ISINK1_CON0, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value_2byte(PMU_ISINK1_CON1, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value_2byte(PMU_ISINK1_CON2, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value_2byte(PMU_ISINK1_CON3, 0xFFFF, 0));
	log_hal_msgid_info("ANA_CON0 = %x, ISINK0_RPT= %x, ISINK1_RPT= %x, ISINK_EN= %x", 4, \
                   (unsigned int) pmu_get_register_value_2byte(PMU_DRIVER_ANA_CON0, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value_2byte(PMU_ISINK_PHASE_DLY, 0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value_2byte(PMU_ISINK_SFSTR,     0xFFFF, 0), \
                   (unsigned int) pmu_get_register_value_2byte(PMU_ISINK_EN_CTRL,   0xFFFF, 0));
	log_hal_msgid_info("ISINK_MODE = %x,CKCFG2 = %x, CKCFG3 = %x", 3,         \
                   (unsigned int) pmu_get_register_value_2byte(PMU_ISINK_MODE_CTRL, 0xFFFF, 0),  \
                   (unsigned int) pmu_get_register_value_2byte(PMU_CKCFG2,          0xFFFF, 0 ), \
                   (unsigned int) pmu_get_register_value_2byte(PMU_CKCFG3,          0xFFFF, 0 ));
}

#endif

