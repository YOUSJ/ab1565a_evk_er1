/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_rtc.h"

#if defined(HAL_RTC_MODULE_ENABLED)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hal_rtc_internal.h"
#include "hal_eint.h"
#include "hal_log.h"
#include "hal_gpt.h"
#include "hal_platform.h"
#include "hal_pmu.h"
#include "hal_pmu_wrap_interface.h"
#include "hal_nvic.h"
#include "hal.h"
#ifdef HAL_CAPTOUCH_MODULE_ENABLED
#include "hal_captouch.h"
#endif

#ifndef __UBL__
#include "ept_gpio_drv.h"
#include "ept_eint_drv.h"
#else
#define RTC_GPIO_PORT0_MODE 0
#define RTC_GPIO_PORT0_DIR  0
#define RTC_GPIO_PORT1_MODE 1
#define RTC_GPIO_PORT1_DIR  0
#define RTC_GPIO_PORT2_MODE 1
#define RTC_GPIO_PORT2_DIR  0
#define RTC_GPIO_PORT0_OUTPUT_LEVEL 0
#define RTC_GPIO_PORT1_OUTPUT_LEVEL 0
#define RTC_GPIO_PORT2_OUTPUT_LEVEL 0
#endif


/*macro for force using eosc in normal mode*/
//#define RTC_EOSC_FOR_NORMAL_ENABLE
/*macro for rtc test mode, eosc32 will output throught gpio12*/
//#define RTC_EOSC_TEST_MODE_ENABLE

#define RTC_RESV_RG_SIZE    2
#define RTC_SPAR_RG_SIZE    12
//#define RTC_32K_DEBUG
#define RTC_LOG_DEBUG
#define RTC_SETTING_CG_FEATURE
#define RTC_32K_OFF_FEATURE

enum {
    RTC_OSC_DCXO = 0,
    RTC_OSC_EOSC,
    RTC_OSC_XOSC,
};

#ifdef RTC_LOG_DEBUG
#define RTC_LOG_INFO(_message,...)            log_hal_info(_message, ##__VA_ARGS__)
#define RTC_LOG_WARNING(_message,...)         log_hal_warning(_message, ##__VA_ARGS__)
#define RTC_LOG_ERROR(_message,...)           log_hal_error(_message, ##__VA_ARGS__)

#define RTC_LOG_MSGID_INFO(_message,...)            log_hal_msgid_info(_message, ##__VA_ARGS__)
#define RTC_LOG_MSGID_WARNING(_message,...)         log_hal_msgid_warning(_message, ##__VA_ARGS__)
#define RTC_LOG_MSGID_ERROR(_message,...)           log_hal_msgid_error(_message, ##__VA_ARGS__)
#else
#define RTC_LOG_INFO(_message,...)
#define RTC_LOG_WARNING(_message,...)
#define RTC_LOG_ERROR(_message,...)

#define RTC_LOG_MSGID_INFO(_message,...)
#define RTC_LOG_MSGID_WARNING(_message,...)
#define RTC_LOG_MSGID_ERROR(_message,...)
#endif

static void rtc_wait_busy(void);
static void rtc_unlock_PROT(void);
static void rtc_write_trigger(void);
static void rtc_write_trigger_wait(void);
static void rtc_setting_cg_config(bool is_to_enable);
void rtc_32k_off(void);
static void rtc_reload(void);

static void rtc_write_osc32con0(uint32_t value, uint32_t operation_type);
static void rtc_write_osc32con1(uint32_t value);
static void rtc_write_osc32con2(uint32_t value, uint32_t operation_type);
static uint32_t rtc_read_osc32con1(void);

static void rtc_lpd_init(void);
static void rtc_set_power_key(void);
static bool rtc_is_time_valid(const hal_rtc_time_t *time);
static void rtc_eint_callback(void *user_data);
static void rtc_register_init(void);
static void rtc_init_eint(void);
static void rtc_dump_register(char *tag);
static void f32k_eosc32_calibration(void);

hal_rtc_status_t rtc_set_data(uint16_t offset, const char *buf, uint16_t len, bool access_hw);

//static void f32k_osc32_Init(void);
uint32_t f32k_measure_clock(uint32_t fixed_clock, uint32_t tested_clock, uint32_t window_setting);

#ifdef HAL_RTC_FEATURE_TIME_CALLBACK
static hal_rtc_time_callback_t rtc_time_callback_function;
static void *rtc_time_user_data;
#endif
static hal_rtc_alarm_callback_t rtc_alarm_callback_function;
static void *rtc_alarm_user_data;
static bool rtc_in_test = false;
static char rtc_spare_register_backup[RTC_SPAR_RG_SIZE];
bool use_xosc = false;
bool rtc_init_done = false;
uint8_t     g_rtc_osc_mode = RTC_OSC_DCXO;

extern uint8_t pmu_get_power_off_reason();
extern bool chip_is_ab1552(void);
extern uint32_t chip_eco_version(void);

RTC_REGISTER_T *rtc_register = (RTC_REGISTER_T *)RTC_BASE;
ABIST_FQMTR_REGISTER_T *abist_fqmtr_register = (ABIST_FQMTR_REGISTER_T *)ABIST_FQMTR_BASE;


struct rtc_spare_register_information {
    uint8_t *address;
};

static struct rtc_spare_register_information rtc_spare_register_table[RTC_SPAR_RG_SIZE] = {
    {(uint8_t *) &(((RTC_REGISTER_T *)RTC_BASE)->RTC_SPAR0_UNION.RTC_SPAR0_CELLS.RTC_SPAR0_0)},
    {(uint8_t *) &(((RTC_REGISTER_T *)RTC_BASE)->RTC_SPAR0_UNION.RTC_SPAR0_CELLS.RTC_SPAR0_1)},
    {(uint8_t *) &(((RTC_REGISTER_T *)RTC_BASE)->RTC_SPAR1_UNION.RTC_SPAR1_CELLS.RTC_SPAR1_0)},
    {(uint8_t *) &(((RTC_REGISTER_T *)RTC_BASE)->RTC_SPAR1_UNION.RTC_SPAR1_CELLS.RTC_SPAR1_1)},
    {(uint8_t *) &(((RTC_REGISTER_T *)RTC_BASE)->RTC_SPAR2_UNION.RTC_SPAR2_CELLS.RTC_SPAR2_0)},
    {(uint8_t *) &(((RTC_REGISTER_T *)RTC_BASE)->RTC_SPAR2_UNION.RTC_SPAR2_CELLS.RTC_SPAR2_1)},
    {(uint8_t *) &(((RTC_REGISTER_T *)RTC_BASE)->RTC_SPAR3_UNION.RTC_SPAR3_CELLS.RTC_SPAR3_0)},
    {(uint8_t *) &(((RTC_REGISTER_T *)RTC_BASE)->RTC_SPAR3_UNION.RTC_SPAR3_CELLS.RTC_SPAR3_1)},
    {(uint8_t *) &(((RTC_REGISTER_T *)RTC_BASE)->RTC_SPAR4_UNION.RTC_SPAR4_CELLS.RTC_SPAR4_0)},
    {(uint8_t *) &(((RTC_REGISTER_T *)RTC_BASE)->RTC_SPAR4_UNION.RTC_SPAR4_CELLS.RTC_SPAR4_1)},
    {(uint8_t *) &(((RTC_REGISTER_T *)RTC_BASE)->RTC_SPAR5_UNION.RTC_SPAR5_CELLS.RTC_SPAR5_0)},
    {(uint8_t *) &(((RTC_REGISTER_T *)RTC_BASE)->RTC_SPAR5_UNION.RTC_SPAR5_CELLS.RTC_SPAR5_1)}
};

#define FQMTR_FCKSEL_RTC_32K            1
#define FQMTR_FCKSEL_EOSC_F32K_CK       4
#define FQMTR_FCKSEL_DCXO_F32K_CK       5
#define FQMTR_FCKSEL_XOSC_F32K_CK       6

#define FQMTR_TCKSEL_XO_CK      1


#ifdef RTC_EOSC_TEST_MODE_ENABLE
static void rtc_set_debug_mode()
{
    *((volatile uint32_t*)(0xA2010060)) = 0x12;
    hal_gpio_init(HAL_GPIO_12);
    hal_pinmux_set_function(HAL_GPIO_12, 8);
    *((volatile uint32_t*)(0xA2080100)) = 0x140;
}
#endif




static void rtc_wait_busy(void)
{
    uint32_t count = 0;

    while (count < 0x6EEEEE) {
        if ((RTC_CBUSY_MASK & rtc_register->RTC_WRTGR_UNION.RTC_WRTGR_CELLS.RTC_STA) == 0) {
            break;
        }
        count++;
    }

    if (count >= 0x6EEEEE) {
        RTC_LOG_MSGID_ERROR("rtc_wait_busy timeout, RTC_BBPU = %x!\r\n",1, rtc_register->RTC_BBPU);
        RTC_LOG_MSGID_ERROR("rtc_wait_busy timeout, RTC_32K = %u, EOSC = %u, DCXO = %u, XOSC = %u\r\n",4,
                      (unsigned int)f32k_measure_clock(FQMTR_FCKSEL_RTC_32K, FQMTR_TCKSEL_XO_CK, 99),
                      (unsigned int)f32k_measure_clock(FQMTR_FCKSEL_EOSC_F32K_CK, FQMTR_TCKSEL_XO_CK, 99),
                      (unsigned int)f32k_measure_clock(FQMTR_FCKSEL_DCXO_F32K_CK, FQMTR_TCKSEL_XO_CK, 99),
                      (unsigned int)f32k_measure_clock(FQMTR_FCKSEL_XOSC_F32K_CK, FQMTR_TCKSEL_XO_CK, 99));
    }
}

static void rtc_unlock_PROT(void)
{
    //rtc_setting_cg_config(true);
    rtc_register->RTC_PROT = RTC_PROTECT1;
    rtc_write_trigger_wait();

    rtc_register->RTC_PROT = RTC_PROTECT2;
    rtc_write_trigger_wait();
    // rtc_setting_cg_config(false);
}

static void rtc_write_trigger(void)
{
    rtc_register->RTC_WRTGR_UNION.RTC_WRTGR_CELLS.WRTGR = RTC_WRTGR_MASK;
}

static void rtc_write_trigger_wait(void)
{
    rtc_write_trigger();
    rtc_wait_busy();
}

static void rtc_setting_cg_config(bool is_to_enable)
{
    if (is_to_enable) {
        rtc_write_osc32con2(0x1, RTC_HIGH_BYTE_OPERATION);
    } else {
        rtc_write_osc32con2(0x0, RTC_HIGH_BYTE_OPERATION);
    }
}

void rtc_32k_off(void)
{
#ifdef RTC_32K_OFF_FEATURE
    rtc_setting_cg_config(true);
    rtc_register->RTC_RESET_CON_UNION.RTC_RESET_CON_CELLS.RSTB_MASK = 0x0;
    rtc_write_trigger_wait();
    rtc_setting_cg_config(false);
#endif
}

#if defined(AB1552)
#else
static void rtc_1st_init(void)
{
    /* Basic RTC init before setting register */
    uint32_t value;
    rtc_setting_cg_config(true);
    rtc_unlock_PROT();
    rtc_reload();

    rtc_register->RTC_BBPU = RTC_KEY_BBPU | RTC_RTC_PU_MASK;
    rtc_write_osc32con0(0x101, RTC_WORD_OPERATION);
    rtc_write_osc32con1(0xf07);
    rtc_write_osc32con2(0x103, RTC_WORD_OPERATION);
    rtc_register->RTC_RESET_CON_UNION.RTC_RESET_CON = 0x1;
    /* workaround, disable LPD firstly */
    rtc_register->RTC_LPD_CON_UNION.RTC_LPD_CON = 0x100;
    rtc_write_trigger_wait();
    rtc_register->RTC_LPD_CON_UNION.RTC_LPD_CON = 0x101;
    rtc_write_trigger_wait();
    /*may need to record powerkey and RTC_INITB status before change it */
    rtc_set_power_key();
    /* clear init_b */
    rtc_register->RTC_RESET_CON_UNION.RTC_RESET_CON_CELLS.RTC_INITB = 0x1;
    rtc_write_trigger_wait();

    /* set EOSC32_STP_PWD */
    value = rtc_read_osc32con1();
    value |= 0x80;
    rtc_write_osc32con1(value);

    /* LPD init again */
    rtc_lpd_init();

    /* enable LPD */
    rtc_register->RTC_LPD_CON_UNION.RTC_LPD_CON_CELLS.RTC_LPD_OPT = 0x0;
    rtc_write_trigger_wait();
    rtc_setting_cg_config(false);
}
#endif

static void rtc_reload(void)
{
    rtc_register->RTC_BBPU = RTC_KEY_BBPU_RELOAD;
    rtc_write_trigger_wait();
}
#if RTC_READ_OSC32_CON_ENABLED
static uint32_t rtc_read_osc32con0(void)
{
    rtc_reload();
    return rtc_register->RTC_OSC32CON0_UNION.RTC_OSC32CON0;
}
#endif
static void rtc_write_osc32con0(uint32_t value, uint32_t operation_type)
{
    rtc_register->RTC_OSC32CON0_UNION.RTC_OSC32CON0 = RTC_OSC32CON0_MAGIC_KEY_1;
    rtc_wait_busy();
    rtc_register->RTC_OSC32CON0_UNION.RTC_OSC32CON0 = RTC_OSC32CON0_MAGIC_KEY_2;
    rtc_wait_busy();
    switch(operation_type) {
        case RTC_LOW_BYTE_OPERATION:
            rtc_register->RTC_OSC32CON0_UNION.RTC_OSC32CON0_CELLS.RTC_32K_SEL = value;
            break;
        case RTC_HIGH_BYTE_OPERATION:
            rtc_register->RTC_OSC32CON0_UNION.RTC_OSC32CON0_CELLS.RTC_TIMER_CG_EN = value;
            break;
        case RTC_WORD_OPERATION:
            rtc_register->RTC_OSC32CON0_UNION.RTC_OSC32CON0 = value;
            break;
        default:
            break;
    }
    rtc_wait_busy();
}

static uint32_t rtc_read_osc32con1(void)
{
    rtc_reload();
    return rtc_register->RTC_OSC32CON1;
}

static void rtc_write_osc32con1(uint32_t value)
{
    rtc_register->RTC_OSC32CON1 = RTC_OSC32CON1_MAGIC_KEY_1;
    rtc_wait_busy();
    rtc_register->RTC_OSC32CON1 = RTC_OSC32CON1_MAGIC_KEY_2;
    rtc_wait_busy();
    rtc_register->RTC_OSC32CON1 = value;
    rtc_wait_busy();
}
#if RTC_READ_OSC32_CON_ENABLED
static uint32_t rtc_read_osc32con2(void)
{
    rtc_reload();
    return rtc_register->RTC_OSC32CON2_UNION.RTC_OSC32CON2;
}
#endif
static void rtc_write_osc32con2(uint32_t value, uint32_t operation_type)
{
    rtc_register->RTC_OSC32CON2_UNION.RTC_OSC32CON2 = RTC_OSC32CON2_MAGIC_KEY_1;
    rtc_wait_busy();
    rtc_register->RTC_OSC32CON2_UNION.RTC_OSC32CON2 = RTC_OSC32CON2_MAGIC_KEY_2;
    rtc_wait_busy();
    switch(operation_type) {
        case RTC_LOW_BYTE_OPERATION:
            rtc_register->RTC_OSC32CON2_UNION.RTC_OSC32CON2_CELLS.OSC32_PW_DB = value;
            break;
        case RTC_HIGH_BYTE_OPERATION:
            rtc_register->RTC_OSC32CON2_UNION.RTC_OSC32CON2_CELLS.RTC_CG_EN = value;
            break;
        case RTC_WORD_OPERATION:
            rtc_register->RTC_OSC32CON2_UNION.RTC_OSC32CON2 = value;
            break;
        default:
            break;
    }
    rtc_wait_busy();
}

static void rtc_lpd_init(void)
{
    rtc_register->RTC_LPD_CON_UNION.RTC_LPD_CON_CELLS.LPD_CON = RTC_EOSC32_LPEN_MASK;
    rtc_write_trigger_wait();

    rtc_register->RTC_LPD_CON_UNION.RTC_LPD_CON_CELLS.LPD_CON = RTC_EOSC32_LPEN_MASK | RTC_LPRST_MASK;
    rtc_write_trigger_wait();

    /* designer suggests delay at least 1 ms */
    hal_gpt_delay_us(1000);

    rtc_register->RTC_LPD_CON_UNION.RTC_LPD_CON_CELLS.LPD_CON = RTC_EOSC32_LPEN_MASK;
    rtc_write_trigger_wait();

    if ((rtc_register->RTC_LPD_CON_UNION.RTC_LPD_CON & RTC_LPSTA_RAW_MASK) != 0) {
        RTC_LOG_MSGID_ERROR("rtc_lpd_init fail : RTC_LPD_CON = %x!\r\n",1, rtc_register->RTC_LPD_CON_UNION.RTC_LPD_CON);
        rtc_dump_register("rtc_lpd_init fail");
    }
}
static void rtc_set_power_key(void)
{
    /* Set powerkey0 and powerkey1 */
    rtc_register->RTC_POWERKEY0 = RTC_POWERKEY0_KEY;
    rtc_register->RTC_POWERKEY1 = RTC_POWERKEY1_KEY;
    rtc_write_trigger_wait();
    /* delay for 200us for clock switch */
    hal_gpt_delay_us(200);

    /* Inicialize LPD */
    rtc_lpd_init();

    /* Set powerkey0 and powerkey1 */
    rtc_register->RTC_POWERKEY0 = RTC_POWERKEY0_KEY;
    rtc_register->RTC_POWERKEY1 = RTC_POWERKEY1_KEY;
    rtc_write_trigger_wait();
    /* delay for 200us for clock switch */
    hal_gpt_delay_us(200);

    rtc_reload();
    if ((rtc_register->RTC_WRTGR_UNION.RTC_WRTGR_CELLS.RTC_STA & RTC_POWER_DETECTED_MASK) == 0) {
        RTC_LOG_MSGID_ERROR("rtc_set_power_key fail : rtc_wrtgr = %x!\r\n",1, rtc_register->RTC_WRTGR_UNION.RTC_WRTGR);
    }
}


static bool rtc_is_time_valid(const hal_rtc_time_t *time)
{
    bool result = true;

    if (time->rtc_year > 127) {
        RTC_LOG_MSGID_ERROR("Invalid year : %d\r\n",1, time->rtc_year);
        result = false;
    }

    if ((time->rtc_mon == 0) || (time->rtc_mon > 12)) {
        RTC_LOG_MSGID_ERROR("Invalid month : %d\r\n",1, time->rtc_mon);
        result = false;
    }

    if (time->rtc_week > 6) {
        RTC_LOG_MSGID_ERROR("Invalid day of week : %d\r\n",1, time->rtc_week);
    }

    if ((time->rtc_day == 0) || (time->rtc_day > 31)) {
        RTC_LOG_MSGID_ERROR("Invalid day of month : %d\r\n",1, time->rtc_day);
        result = false;
    }

    if (time->rtc_hour > 23) {
        RTC_LOG_MSGID_ERROR("Invalid hour : %d\r\n",1, time->rtc_hour);
        result = false;
    }

    if (time->rtc_min > 59) {
        RTC_LOG_MSGID_ERROR("Invalid minute : %d\r\n",1, time->rtc_min);
        result = false;
    }

    if (time->rtc_sec > 59) {
        RTC_LOG_MSGID_ERROR("Invalid second : %d\r\n",1, time->rtc_sec);
        result = false;
    }

    return result;

}

static void rtc_eint_callback(void *user_data)
{
    uint32_t value;

    hal_eint_mask(HAL_EINT_RTC);


    /* read clear interrupt status */
    value = rtc_register->RTC_IRQ_STA;
    /* clear alarm power on */
    rtc_register->RTC_BBPU = RTC_KEY_BBPU_1 | RTC_RTC_PU_MASK;
    rtc_write_trigger_wait();

    if ((value & RTC_ALSTA_MASK) != 0) {
        /* disable alarm interrupt */
        rtc_register->RTC_AL_MASK_UNION.RTC_AL_MASK_CELLS.AL_EN = (~RTC_AL_EN_MASK);
        rtc_write_trigger_wait();
        if (rtc_alarm_callback_function != NULL) {
            rtc_alarm_callback_function(rtc_alarm_user_data);
        }
    }

#ifdef HAL_RTC_FEATURE_TIME_CALLBACK
    if ((value & RTC_TCSTA_MASK) != 0) {
        if (rtc_time_callback_function != NULL) {
            rtc_time_callback_function(rtc_time_user_data);
        }
    }
#endif

    hal_eint_unmask(HAL_EINT_RTC);
}


static void rtc_init_irq(void)
{
    hal_eint_config_t eint_config;
    hal_eint_status_t result;

    eint_config.trigger_mode = HAL_EINT_LEVEL_LOW;
    eint_config.debounce_time = 0;
    result = hal_eint_init(HAL_EINT_RTC, &eint_config);
    if (result != HAL_EINT_STATUS_OK) {
        RTC_LOG_MSGID_ERROR("hal_eint_init fail: %d\r\n",1, result);
        return;
    }
    result = hal_eint_register_callback(HAL_EINT_RTC, rtc_eint_callback, NULL);
    if (result != HAL_EINT_STATUS_OK) {
        RTC_LOG_MSGID_ERROR("hal_eint_register_callback fail: %d\r\n",1, result);
        return;
    }
    result = hal_eint_unmask(HAL_EINT_RTC);
    if (result != HAL_EINT_STATUS_OK) {
        RTC_LOG_MSGID_ERROR("hal_eint_unmask fail: %d\r\n",1, result);
        return;
    }
}

static  void    rtc_init_eint()
{
    hal_rtc_eint_config_t rtc_eint_config;

    rtc_eint_config.rtc_gpio = HAL_RTC_GPIO_1;
    rtc_eint_config.is_enable_rtc_eint = false;
    rtc_eint_config.is_falling_edge_active = false;
    rtc_eint_config.is_enable_debounce = true;
    hal_rtc_eint_init(&rtc_eint_config);
    rtc_eint_config.rtc_gpio = HAL_RTC_GPIO_2;
    hal_rtc_eint_init(&rtc_eint_config);
    rtc_setting_cg_config(true);
}

static void rtc_register_init(void)
{
    uint32_t rtc_irq_sta;
    uint32_t value;

    value = rtc_read_osc32con1();
    value |= 0x80;
    rtc_write_osc32con1(value);

    /* Clear BBPU */
    rtc_register->RTC_BBPU = RTC_KEY_BBPU | RTC_RTC_PU_MASK;
    /* Read clear */
    rtc_irq_sta = rtc_register->RTC_IRQ_STA;
    rtc_irq_sta = rtc_irq_sta;
    rtc_register->RTC_IRQ_EN_UNION.RTC_IRQ_EN = 0x0;
    rtc_register->RTC_CII_EN = 0x0;
    rtc_register->RTC_AL_MASK_UNION.RTC_AL_MASK = 0x0;
    rtc_register->RTC_TC0_UNION.RTC_TC0 = 0x0;
    rtc_register->RTC_TC1_UNION.RTC_TC1 = 0x100;
    rtc_register->RTC_TC2_UNION.RTC_TC2 = 0x101;
    rtc_register->RTC_TC3 = 0x0;
    rtc_register->RTC_AL0_UNION.RTC_AL0 = 0x0;
    rtc_register->RTC_AL1_UNION.RTC_AL1 = 0x100;
    rtc_register->RTC_AL2_UNION.RTC_AL2 = 0x101;
    rtc_register->RTC_AL3 = 0x0;
    rtc_register->RTC_LPD_CON_UNION.RTC_LPD_CON = 0x101;
    rtc_register->RTC_EINT_UNION.RTC_EINT = 0x0;
    rtc_write_trigger_wait();
#if defined(AB1552)
#else
    uint32_t chip_version = 0;
    chip_version = chip_eco_version();
    if (chip_version == 1) {
        rtc_register->RTC_GPIO0_CON = 0x182;
        rtc_register->RTC_GPIO1_CON = 0x182;
        rtc_register->RTC_GPIO2_CON = 0x182; //E1:input mode, pull down
    } else if (chip_version == 2) {
        rtc_register->RTC_GPIO0_CON = 0x1c0;
        rtc_register->RTC_GPIO1_CON = 0x1c0;
        rtc_register->RTC_GPIO2_CON = 0x1c0; //E2:IES = 1
    }
    rtc_write_trigger_wait();
#endif
    rtc_write_osc32con0(0x101, RTC_WORD_OPERATION);
    rtc_write_osc32con1(0xf07);
    rtc_write_osc32con2(0x103, RTC_WORD_OPERATION);
    rtc_register->RTC_SPAR0_UNION.RTC_SPAR0 = 0x0;
    rtc_register->RTC_SPAR1_UNION.RTC_SPAR1 = 0x0;
    rtc_register->RTC_SPAR2_UNION.RTC_SPAR2 = 0x0;
    rtc_register->RTC_SPAR3_UNION.RTC_SPAR3 = 0x0;
    rtc_register->RTC_SPAR4_UNION.RTC_SPAR4 = 0x0;
    rtc_register->RTC_SPAR5_UNION.RTC_SPAR5 = 0x0;
    rtc_register->RTC_SPAR_REG_UNION.RTC_SPAR_REG = 0x0;
    rtc_register->RTC_DIFF = 0x0;
    rtc_register->RTC_CALI = 0x0;
    rtc_register->RTC_CAP_CON = 0x2;
    rtc_register->RTC_RESET_CON_UNION.RTC_RESET_CON = 0x1;
    /* reset TC */
    rtc_register->RTC_TC0_UNION.RTC_TC0 = 0x0;
    rtc_register->RTC_TC1_UNION.RTC_TC1 = 0x100;
    rtc_register->RTC_TC2_UNION.RTC_TC2 = 0x101;
    rtc_register->RTC_TC3 = 0x0;
    rtc_write_trigger_wait();
}

static void rtc_dump_register(char *tag)
{
    rtc_reload();
    RTC_LOG_MSGID_INFO("-> rtc_dump_register, RTC_POWERKEY1 = %x, RTC_POWERKEY2 = %x, RTC_DIFF = %x, RTC_OSC32CON0 = %x\r\n",4, \
                            rtc_register->RTC_POWERKEY0, \
                            rtc_register->RTC_POWERKEY1, \
                            rtc_register->RTC_DIFF, \
                            rtc_register->RTC_OSC32CON0_UNION.RTC_OSC32CON0);
    RTC_LOG_MSGID_INFO("-> RTC_BBPU = %x, RTC_LPD_CON = %x, RTC_CON32CON1 = %x, RTC_CON32CON2 = %x\r\n",4, \
                            rtc_register->RTC_BBPU, \
                            rtc_register->RTC_LPD_CON_UNION.RTC_LPD_CON, \
                            rtc_register->RTC_OSC32CON1, \
                            rtc_register->RTC_OSC32CON2_UNION.RTC_OSC32CON2);
    RTC_LOG_MSGID_INFO("-> RTC_WRTGR= %x, CALI = %x, RESETCON = %x, EINT = %x\r\n",4, \
                           rtc_register->RTC_WRTGR_UNION.RTC_WRTGR, \
                           rtc_register->RTC_CALI, \
                           rtc_register->RTC_RESET_CON_UNION.RTC_RESET_CON, \
                           rtc_register->RTC_EINT_UNION.RTC_EINT);
    RTC_LOG_MSGID_INFO("-> RTC_BOND = %x, RTC_WRTRIG = %x, RTC_SPAR = %x\r\n", 3, \
                            rtc_register->RTC_BOND_OPT, \
                            rtc_register->RTC_WRTGR_UNION.RTC_WRTGR, \
                            rtc_register->RTC_SPAR_REG_UNION.RTC_SPAR_REG);
}

static void f32k_eosc32_calibration(void)
{
    uint32_t value;

    uint32_t low_xosccali = 0x00;
    uint32_t high_xosccali = 0x1f;
    uint32_t medium_xosccali;

    uint32_t low_frequency = 0;
    uint32_t high_frequency = 0;
    uint32_t medium_frequency;

    value = rtc_read_osc32con1();
    value &= ~RTC_XOSCCALI_MASK;
    value |= (low_xosccali << RTC_XOSCCALI_OFFSET);
    rtc_write_osc32con1(value);
    high_frequency = f32k_measure_clock(FQMTR_FCKSEL_EOSC_F32K_CK, FQMTR_TCKSEL_XO_CK, 99);
    if (high_frequency <= 32768) {
        RTC_LOG_MSGID_INFO("-> rtc high_frequency <= 32768, frequency = %u, xosccali = %d\r\n",2, (unsigned int)high_frequency, (unsigned int)low_xosccali);
        return;
    }

    value = rtc_read_osc32con1();
    value &= ~RTC_XOSCCALI_MASK;
    value |= (high_xosccali << RTC_XOSCCALI_OFFSET);
    rtc_write_osc32con1(value);
    low_frequency = f32k_measure_clock(FQMTR_FCKSEL_EOSC_F32K_CK, FQMTR_TCKSEL_XO_CK, 99);
    if (low_frequency >= 32768) {
        RTC_LOG_MSGID_INFO("-> rtc low_frequency >= 32768, frequency = %u, xosccali = %d\r\n",2, (unsigned int)low_frequency, (unsigned int)high_xosccali);
        return;
    }

    while ((high_xosccali - low_xosccali) > 1) {
        medium_xosccali = (low_xosccali + high_xosccali) / 2;
        value = rtc_read_osc32con1();
        value &= ~RTC_XOSCCALI_MASK;
        value |= (medium_xosccali << RTC_XOSCCALI_OFFSET);
        rtc_write_osc32con1(value);
        medium_frequency = f32k_measure_clock(FQMTR_FCKSEL_EOSC_F32K_CK, FQMTR_TCKSEL_XO_CK, 99);
        if (medium_frequency > 32768) {
            low_xosccali = medium_xosccali;
            high_frequency = medium_frequency;
        } else if (medium_frequency < 32768) {
            high_xosccali = medium_xosccali;
            low_frequency = medium_frequency;
        } else {
            RTC_LOG_MSGID_INFO("-> rtc xosccali = %d\r\n",1, medium_xosccali);
            return;
        }
    }

    if ((32768 - low_frequency) < (high_frequency - 32768)) {
        value = rtc_read_osc32con1();
        value &= ~RTC_XOSCCALI_MASK;
        value |= (high_xosccali << RTC_XOSCCALI_OFFSET);


        rtc_write_osc32con1(value);
        RTC_LOG_MSGID_INFO("-> rtc frequency = %u, xosccali = %d\r\n",2, (unsigned int)low_frequency, (unsigned int)high_xosccali);
    } else {
        value = rtc_read_osc32con1();
        value &= ~RTC_XOSCCALI_MASK;
        value |= (low_xosccali << RTC_XOSCCALI_OFFSET);
        rtc_write_osc32con1(value);
        RTC_LOG_MSGID_INFO("-> rtc frequency = %u, xosccali = %d\r\n",2, (unsigned int)high_frequency, (unsigned int)low_xosccali);
    }
}

#if RTC_32K_DEBUG
static hal_rtc_status_t rtc_wait_second_changed()
{
    hal_rtc_time_t time;
    uint32_t second;

    hal_rtc_get_time(&time);
    second = time.rtc_sec;

    do {
        hal_rtc_get_time(&time);
    } while (second == time.rtc_sec);

    return HAL_RTC_STATUS_OK;
}
#endif

bool power_exception = false;
bool back_from_rtc_mode = false;
static void power_off_reason_check(void)
{
    uint8_t value;
    value = pmu_get_power_off_reason();
    log_hal_msgid_info("Get power off reason: %x\r\n",1, value);

    /* back_from_rtc_mode variable if bit[1] == 1 */
    if (value == 0x1) {
        back_from_rtc_mode = true;
    } else {
        back_from_rtc_mode = false;
        /* 1st power on name as power_exception to do 1st init */
        if (value == 0)
            power_exception = true;
        /* power_exception variable */
        else if ((value >= 2) && (value <= 3))
            power_exception = true;
        else if ((value >= 10) && (value <= 12))
            power_exception = true;
        else if ((value >= 14) && (value <= 22))
            power_exception = true;
        else
            power_exception = false;
    }
}

void rtc_gpio_setting(hal_rtc_gpio_setting_t *gpio_setting)
{
    uint8_t value = 0x0;
    uint32_t chip_version = 0;
    chip_version = chip_eco_version();

    if (gpio_setting == NULL) {
        log_hal_msgid_error("rtc gpio_setting parameter invalid", 0);
    } else {
        rtc_setting_cg_config(true);
        switch (gpio_setting->rtc_gpio) {
            case HAL_RTC_GPIO_0:
                if (gpio_setting->is_input_direction == true) {
                    /* set as GPIO input mode */
                    rtc_register->RTC_GPIO0_CON = 0x180;
                    rtc_write_trigger_wait();
                } else {
                    if (gpio_setting->is_output_high == false) {
                        value &= (~0x1);
                    } else {
                        value |= (0x1);
                    }
                    rtc_register->RTC_GPIO_CON |= value;
                    rtc_write_trigger_wait();
                    if (chip_version == 1) {
                        rtc_register->RTC_GPIO0_CON = 0x1c0;
                    } else if (chip_version == 2) {
                        rtc_register->RTC_GPIO0_CON = 0x182;
                    }
                    rtc_write_trigger_wait();
                }
                break;
            case HAL_RTC_GPIO_1:
                if (gpio_setting->is_input_direction == true) {
                    /* set as GPIO input mode */
                    rtc_register->RTC_GPIO1_CON = 0x180;
                    rtc_write_trigger_wait();
                } else {
                    if (gpio_setting->is_output_high == false) {
                        value &= (~0x2);
                    } else {
                        value |= (0x2);
                    }
                    rtc_register->RTC_GPIO_CON |= value;
                    rtc_write_trigger_wait();
                    if (chip_version == 1) {
                        rtc_register->RTC_GPIO1_CON = 0x1c0;
                    } else if (chip_version == 2) {
                        rtc_register->RTC_GPIO1_CON = 0x182;
                    }
                    rtc_write_trigger_wait();
                }
                break;
            case HAL_RTC_GPIO_2:
                if (gpio_setting->is_input_direction == true) {
                    /* set as GPIO input mode */
                    rtc_register->RTC_GPIO2_CON = 0x180;
                    rtc_write_trigger_wait();
                } else {
                    if (gpio_setting->is_output_high == false) {
                        value &= (~0x4);
                    } else {
                        value |= (0x4);
                    }
                    rtc_register->RTC_GPIO_CON |= value;
                    rtc_write_trigger_wait();
                    if (chip_version == 1) {
                        rtc_register->RTC_GPIO2_CON = 0x1c0;
                    } else if (chip_version == 2) {
                        rtc_register->RTC_GPIO2_CON = 0x182;
                    }
                    rtc_write_trigger_wait();
                }
                break;
            default:
                break;
        }
        rtc_setting_cg_config(false);
    }
}

void rtc_gpio_get_input(hal_rtc_gpio_t rtc_gpio, hal_rtc_gpio_data_t *data)
{
    uint32_t value = 0x0;

    if (data == NULL) {
        log_hal_msgid_error("rtc_gpio_get_input parameter invalid", 0);
    } else {
        if (rtc_init_done == true) {
            rtc_setting_cg_config(true);
        }
        value = (*(volatile uint32_t *)(0xa2080104));
        //log_hal_info("RTC GPIO input value %x", value);
        switch (rtc_gpio) {
            case HAL_RTC_GPIO_0:
                *data = (value & 0x100) >> 8;
                break;
            case HAL_RTC_GPIO_1:
                *data = (value & 0x200) >> 9;
                break;
            case HAL_RTC_GPIO_2:
                *data = (value & 0x400) >> 10;
                break;
            default:
                break;
        }
        if (rtc_init_done == true) {
            rtc_setting_cg_config(false);
        }
    }
}

static void rtc_ept_gpio_setting_init(void)
{
#if defined(AB1552)
#else
    uint8_t value_gpio_con = 0;
    uint32_t chip_version = 0;

    chip_version = chip_eco_version();
    log_hal_msgid_info("chip version is: %d\r\n",1, chip_version);

    if (RTC_GPIO_PORT0_MODE == 1) {
        rtc_register->RTC_GPIO0_CON = 0x00; //AIO mode
    } else {
        if (RTC_GPIO_PORT0_DIR == 0) {
            if (chip_version == 1) {
                rtc_register->RTC_GPIO0_CON = 0x182; //E1:input mode, pull down
            } else if (chip_version == 2) {
                rtc_register->RTC_GPIO0_CON = 0x1c0; //E2:IES = 1
            }
        } else {
            // output value
            if (RTC_GPIO_PORT0_OUTPUT_LEVEL == 0) {
                value_gpio_con &= (~0x1);
            } else {
                value_gpio_con |= (0x1);
            }
            // direction as output
            if (chip_version == 1) {
                rtc_register->RTC_GPIO0_CON = 0x1c0; //E1: output mode
            } else if (chip_version == 2) {
                rtc_register->RTC_GPIO0_CON = 0x182; //E2: output mode
            }
        }
    }

    if (RTC_GPIO_PORT1_MODE == 1) {
        if (chip_version == 1) {
            rtc_register->RTC_GPIO1_CON = 0x182; //E1:input mode, pull down
        } else if (chip_version == 2) {
            rtc_register->RTC_GPIO1_CON = 0x1c0; //E2:IES = 1
        }
    } else {
        if (RTC_GPIO_PORT1_DIR == 0) {
            if (chip_version == 1) {
                rtc_register->RTC_GPIO1_CON = 0x182; //E1:input mode, pull down
            } else if (chip_version == 2) {
                rtc_register->RTC_GPIO1_CON = 0x1c0; //E2:IES = 1
            }
        } else {
            // output value
            if (RTC_GPIO_PORT1_OUTPUT_LEVEL == 0) {
                value_gpio_con &= (~0x2);

            } else {
                value_gpio_con |= (0x2);
            }
            // direction as output
            if (chip_version == 1) {
                rtc_register->RTC_GPIO1_CON = 0x1c0; //E1: output mode
            } else if (chip_version == 2) {
                rtc_register->RTC_GPIO1_CON = 0x182; //E2: output mode
            }
        }
    }

    if (RTC_GPIO_PORT2_MODE == 1) {
        if (chip_version == 1) {
            rtc_register->RTC_GPIO2_CON = 0x182; //E1:input mode, pull down
        } else if (chip_version == 2) {
            rtc_register->RTC_GPIO2_CON = 0x1c0; //E2:IES = 1
        }
    } else {
        if (RTC_GPIO_PORT2_DIR == 0) {
            if (chip_version == 1) {
                rtc_register->RTC_GPIO2_CON = 0x182; //E1:input mode, pull down
            } else if (chip_version == 2) {
                rtc_register->RTC_GPIO2_CON = 0x1c0; //E2:IES = 1
            }
        } else {
            // output value
            if (RTC_GPIO_PORT2_OUTPUT_LEVEL == 0) {
                value_gpio_con &= (~0x4);
            } else {
                value_gpio_con |= (0x4);
            }
            // direction as output
            if (chip_version == 1) {
                rtc_register->RTC_GPIO2_CON = 0x1c0; //E1: output mode
            } else if (chip_version == 2) {
                rtc_register->RTC_GPIO2_CON = 0x182; //E2: output mode
            }
        }
    }

    rtc_register->RTC_GPIO_CON = value_gpio_con;
    rtc_write_trigger_wait();
#endif
}

static  void    rtc_select_osc_mode(uint8_t  mode)
{
    if(RTC_OSC_XOSC == mode) {
        uint32_t value;
        // Setup XOSC
        value = rtc_read_osc32con1();
        value |= (RTC_AMP_GSEL_MASK | RTC_AMPCTL_EN_MASK);
        rtc_write_osc32con1(value);
        /* select 32k clock source for Top as XOSC*/
        rtc_write_osc32con0(0x2, RTC_LOW_BYTE_OPERATION);
        /* wait 200us for hw switch time */
        hal_gpt_delay_us(200);
        /* turn off EOSC */
        rtc_write_osc32con2(RTC_XOSC_PWDB_MASK, RTC_LOW_BYTE_OPERATION);
    } else {
        /*eosc calibration*/
        f32k_eosc32_calibration();
        if(RTC_OSC_DCXO == mode) {
            /* select 32k clock source for Top: Top is DCXO for normal mode, EOSC for retention mode */
            rtc_write_osc32con0(0x0, RTC_LOW_BYTE_OPERATION);
            rtc_register->RTC_CALI = 0;
        } else {
            int frequency, cali;
            uint16_t temp = 0;

            frequency = f32k_measure_clock(FQMTR_FCKSEL_EOSC_F32K_CK, FQMTR_TCKSEL_XO_CK, 99);
            cali = (32768 - frequency);
            temp = cali;
            if(cali > 4095) {
                temp = 0xFFF;
            } else if((cali +4096) < 0) {
                temp = 0xF000;
            }
            rtc_set_data(0, (char *)(&temp), 2, true);
            rtc_register->RTC_CALI = (temp & 0x1FFF) | RTC_CALI_RW_SEL_MASK;
            log_hal_msgid_info("-> rtc time cali calc value %d(%x),set value 0x%x\r\n",3, cali, cali, (temp&0x1FFF));
        }
        rtc_write_trigger_wait();
        /* wait 200us for hw switch time */
        hal_gpt_delay_us(200);
        /* turn off XOSC*/
        rtc_write_osc32con2(RTC_EOSC_PWDB_MASK, RTC_LOW_BYTE_OPERATION);
    }
}


static  void    rtc_lpd_pwrkey_init()
{
    /* LPD related */
    /* workaround, disable LPD firstly */
    rtc_register->RTC_LPD_CON_UNION.RTC_LPD_CON = 0x100;
    rtc_write_trigger_wait();
    rtc_register->RTC_LPD_CON_UNION.RTC_LPD_CON = 0x101;
    rtc_write_trigger_wait();
    rtc_set_power_key();
    /* clear init_b */
    rtc_register->RTC_RESET_CON_UNION.RTC_RESET_CON_CELLS.RTC_INITB = 0x1;
    rtc_write_trigger_wait();
    /* LPD init again */
    rtc_lpd_init();
    /* enable LPD */
    rtc_register->RTC_LPD_CON_UNION.RTC_LPD_CON_CELLS.RTC_LPD_OPT = 0x0;
    rtc_write_trigger_wait();
}


uint32_t f32k_measure_clock(uint32_t fixed_clock, uint32_t tested_clock, uint32_t window_setting)
{
    uint32_t fqmtr_data;
    uint32_t frequency;

    /* 1) PLL_ABIST_FQMTR_CON0 = 0xCXXX */
    abist_fqmtr_register->ABIST_FQMTR_CON0 |= 0xC000;
    hal_gpt_delay_us(1000);
    while ((abist_fqmtr_register->ABIST_FQMTR_CON1 & 0x8000) != 0);
    /* 2) CKSYS_TST_SEL_1_BASE = 0x0 */
    *CKSYS_TST_SEL_1_BASE = 0;
    /* 3) CKSYS_TST_SEL_1_BASE = 0x0601 */
    *CKSYS_TST_SEL_1_BASE = (fixed_clock << 8) | tested_clock;
    abist_fqmtr_register->ABIST_FQMTR_CON2 = 0;
    /* 4) PLL_ABIST_FQMTR_CON0 = 0x8009 */
    abist_fqmtr_register->ABIST_FQMTR_CON0 = 0x8000 | window_setting;
    hal_gpt_delay_us(1000);
    /* 5) Wait PLL_ABIST_FQMTR_CON1 & 0x8000 == 0x8000 */
    while ((abist_fqmtr_register->ABIST_FQMTR_CON1 & 0x8000) != 0);
    /* 6) Read PLL_ABIST_FQMTR_DATA */
    fqmtr_data = abist_fqmtr_register->ABIST_FQMTR_DATA;
    /* 7) Freq = fxo_clk*10/PLL_ABIST_FQMTR_DATA */
    frequency = 26000000 * (window_setting + 1) / fqmtr_data;
    //RTC_LOG_INFO("fqmtr_data = %u\r\n", fqmtr_data);
    return frequency;
}

uint32_t f32k_measure_count(uint16_t fixed_clock, uint16_t tested_clock, uint16_t window_setting)
{
    uint32_t fqmtr_data;

    /* 1) PLL_ABIST_FQMTR_CON0 = 0xCXXX */
    abist_fqmtr_register->ABIST_FQMTR_CON0 |= 0xC000;
    hal_gpt_delay_us(1000);
    while ((abist_fqmtr_register->ABIST_FQMTR_CON1 & 0x8000) != 0);
    /* 2) CKSYS_TST_SEL_1_BASE = 0x0 */
    *CKSYS_TST_SEL_1_BASE = 0;
    /* 3) CKSYS_TST_SEL_1_BASE = 0x0601 */
    *CKSYS_TST_SEL_1_BASE = (fixed_clock << 8) | tested_clock;
    abist_fqmtr_register->ABIST_FQMTR_CON2 = 0;
    /* 4) PLL_ABIST_FQMTR_CON0 = 0x8009 */
    abist_fqmtr_register->ABIST_FQMTR_CON0 = 0x8000 | window_setting;
    hal_gpt_delay_us(1000);
    /* 5) Wait PLL_ABIST_FQMTR_CON1 & 0x8000 == 0x8000 */
    while ((abist_fqmtr_register->ABIST_FQMTR_CON1 & 0x8000) != 0);
    /* 6) Read PLL_ABIST_FQMTR_DATA */
    fqmtr_data = abist_fqmtr_register->ABIST_FQMTR_DATA;

    return fqmtr_data;
}

static void rtc_gpio_default_init(void)
{
    /* Add RTC_GPIO Input PD patch here */
#if defined(AB1552)
#else
    uint8_t value;
    value = pmu_get_power_off_reason();
    /* Only need this setting when 1st boot up */
    if (value == 0) {
        uint32_t chip_version = 0;
        chip_version = chip_eco_version();
        rtc_1st_init();
        rtc_setting_cg_config(true);
        if (chip_version == 1) {
            rtc_register->RTC_GPIO0_CON = 0x182;
            rtc_register->RTC_GPIO1_CON = 0x182;
            rtc_register->RTC_GPIO2_CON = 0x182; //E1:input mode, pull down
        } else if (chip_version == 2) {
            rtc_register->RTC_GPIO0_CON = 0x1c0;
            rtc_register->RTC_GPIO1_CON = 0x1c0;
            rtc_register->RTC_GPIO2_CON = 0x1c0; //E2:IES = 1
        }
        rtc_write_trigger_wait();
        rtc_setting_cg_config(false);
    }
#endif
}
static void rtc_32kless_init(void)
{
    uint16_t int_counter;
    int16_t cali;
    uint32_t frequency;
    /* Make sure EOSC runs @multiple of 62.5ms*/
    do {
        int_counter = *(volatile uint16_t *)(0xa2080038);
    } while (((int_counter & 0x7fff) % 2048) != 0);

    rtc_setting_cg_config(true);
    /* 32k-less mode, need switch Top 32k source from EOSC to DCXO */
    rtc_write_osc32con0(0x0, RTC_LOW_BYTE_OPERATION);
    /* wait 200us for hw switch time */
    hal_gpt_delay_us(200);
    /* clear RTC_CALI */
    rtc_register->RTC_CALI = 0x0;
    rtc_write_trigger_wait();

    //calibration for EOSC and cali here
    f32k_eosc32_calibration();
    frequency = f32k_measure_clock(FQMTR_FCKSEL_EOSC_F32K_CK, FQMTR_TCKSEL_XO_CK, 99);
    cali = (32768 - frequency);
    if (cali > (int16_t)0xFFF) {
        cali = 0xFFF;
    } else if (cali < (int16_t)0xF000) {
        cali = 0xF000;
    }
    rtc_set_data(0, (const char *)(&cali), 2, true);
    rtc_setting_cg_config(false);
}

bool rtc_query_32kless_cali(void)
{
    rtc_gpio_default_init();
#ifndef MTK_HAL_EXT_32K_ENABLE
    log_hal_msgid_info("32kless_init enter\r\n", 0);
    rtc_32kless_init();
    return true;
#else
    if (chip_is_ab1552() == true) {
        log_hal_msgid_info("32kless_init enter\r\n", 0);
        rtc_32kless_init();
        return true;
    }
    return false;
#endif
}



bool    rtc_powerkey_is_valid()
{
    if((rtc_register->RTC_WRTGR_UNION.RTC_WRTGR_CELLS.RTC_STA & 0x06) != 0x06){
        return false;
    } else {
        return true;
    }
}

void    rtc_print_power_reason(uint32_t irq_status)
{
#ifdef HAL_CAPTOUCH_MODULE_ENABLED
    if(irq_status == 0) {
        log_hal_msgid_info("-> rtc power by captouch!\r\n",0);
        return;
    }
#endif
    if( irq_status & 0x01 ) {
        log_hal_msgid_info("-> rtc power by alarm!\r\n",0);
    }
    if( irq_status & 0x02 ) {
        log_hal_msgid_info("-> rtc power by tick!\r\n",0);
    }
    if( irq_status & 0x04 ) {
        log_hal_msgid_info("-> rtc power by lpd!\r\n",0);
    }
    if( irq_status & 0x08 ) {
        log_hal_msgid_info("-> rtc power by eint1!\r\n",0);
    }
    if( irq_status & 0x10 ) {
        log_hal_msgid_info("-> rtc power by eint2!\r\n",0);
    }
}


hal_rtc_status_t hal_rtc_init(void)
{
    bool    rtc_pwrkey_st = false;

    rtc_setting_cg_config(true);
    rtc_unlock_PROT();
    rtc_reload();
    power_off_reason_check();
    log_hal_msgid_info("###### rtc initialized start... ######\r\n", 0);
#ifdef RTC_EOSC_TEST_MODE_ENABLE
    rtc_set_debug_mode();
    log_hal_msgid_info("Rtc test mode enabled, eosc32k will output throught GPIO12!!!!\r\n", 0);
#endif

#ifdef MTK_HAL_EXT_32K_ENABLE
    log_hal_msgid_info("-> config 32k to XOSC\r\n", 0);
    g_rtc_osc_mode = RTC_OSC_XOSC;
    if (chip_is_ab1552() == true) {
        log_hal_msgid_info("-> chip is AB1552, no ext-32k\r\n", 0);
        g_rtc_osc_mode = RTC_OSC_DCXO;
    }
#else
    g_rtc_osc_mode = RTC_OSC_DCXO;
    log_hal_msgid_info("-> config 32k to DCXO\r\n", 0);
#endif

#ifdef  RTC_EOSC_FOR_NORMAL_ENABLE
    g_rtc_osc_mode = RTC_OSC_EOSC;
    log_hal_msgid_info("-> config 32k to EOSC\r\n", 0);
#endif

#if 0
        f32k_osc32_Init();
#else
    rtc_pwrkey_st = rtc_powerkey_is_valid();
    if(rtc_pwrkey_st == false ) {
        log_hal_msgid_info("-> rtc pwrkey not match,init as 1st\r\n", 0);
        rtc_register_init();
        rtc_lpd_pwrkey_init();
        rtc_init_eint();
    }
    rtc_select_osc_mode(g_rtc_osc_mode);
#endif

    rtc_init_irq();
    /* Prepare flag for Bootrom */
    rtc_register->RTC_SPAR_REG_UNION.RTC_SPAR_REG_CELLS.RTC_SPAR_REG_0 &= (~RTC_STANDARD_1_MASK);
    /*judge back from rtc mode*/
    if ((back_from_rtc_mode == true) &&(rtc_pwrkey_st == true)) {
        uint32_t irq_sta;

        irq_sta = rtc_register->RTC_IRQ_STA;
        rtc_print_power_reason(irq_sta);
        /* Disable 32k-off feature */
        rtc_register->RTC_RESET_CON_UNION.RTC_RESET_CON_CELLS.RSTB_MASK = RTC_STANDARD_1_MASK;
        rtc_register->RTC_BBPU = RTC_KEY_BBPU_1 | RTC_RTC_PU_MASK;
        rtc_write_trigger_wait();
        log_hal_msgid_info("-> back from rtc mode, IRQ_STA=%x,BBPU=%x\r\n", 2, irq_sta, rtc_register->RTC_BBPU);
    }
    rtc_ept_gpio_setting_init();
    if (chip_is_ab1552() == true) {
        rtc_register->RTC_BOND_OPT = 0x2;
    } else {
        rtc_register->RTC_BOND_OPT = 0x1;
    }
    rtc_write_trigger_wait();
    rtc_dump_register(NULL);
    rtc_init_done = true;
    RTC_LOG_MSGID_INFO("-> init_done, RTC_32K = %u, EOSC = %u, DCXO = %u, XOSC = %u\r\n",4,
                 (unsigned int)f32k_measure_clock(FQMTR_FCKSEL_RTC_32K, FQMTR_TCKSEL_XO_CK, 99),
                 (unsigned int)f32k_measure_clock(FQMTR_FCKSEL_EOSC_F32K_CK, FQMTR_TCKSEL_XO_CK, 99),
                 (unsigned int)f32k_measure_clock(FQMTR_FCKSEL_DCXO_F32K_CK, FQMTR_TCKSEL_XO_CK, 99),
                 (unsigned int)f32k_measure_clock(FQMTR_FCKSEL_XOSC_F32K_CK, FQMTR_TCKSEL_XO_CK, 99));
    /*RTC setting CG disable*/
    rtc_setting_cg_config(false);

    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t hal_rtc_deinit(void)
{
    rtc_dump_register("hal_rtc_deinit called");

    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t hal_rtc_set_time(const hal_rtc_time_t *time)
{
    if (!rtc_is_time_valid(time)) {
        return HAL_RTC_STATUS_INVALID_PARAM;
    }
    rtc_register->RTC_TC0_UNION.RTC_TC0 = (time->rtc_min << RTC_TC_MINUTE_OFFSET) | (time->rtc_sec);
    rtc_register->RTC_TC1_UNION.RTC_TC1 = (time->rtc_day << RTC_TC_DOM_OFFSET) | (time->rtc_hour);
    // day-of-week range is 1~7, header file is 0~6
    rtc_register->RTC_TC2_UNION.RTC_TC2 = (time->rtc_mon << RTC_TC_MONTH_OFFSET) | (time->rtc_week + 1);
    rtc_register->RTC_TC3 = time->rtc_year;
    rtc_write_trigger_wait();

    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t hal_rtc_get_time(hal_rtc_time_t *time)
{
    uint16_t value_tc2, value_tc1, value_tc0, int_cnt, int_cnt_pre;
    /* re-read time information if internal millisecond counter has carried */
    do {
        int_cnt_pre = rtc_register->RTC_INT_CNT;
        time->rtc_year = rtc_register->RTC_TC3;
        value_tc2 = rtc_register->RTC_TC2_UNION.RTC_TC2;
        value_tc1 = rtc_register->RTC_TC1_UNION.RTC_TC1;
        value_tc0 = rtc_register->RTC_TC0_UNION.RTC_TC0;
        int_cnt   = rtc_register->RTC_INT_CNT;
    } while (int_cnt < int_cnt_pre);
    time->rtc_week = (value_tc2 & RTC_TC_DOW_MASK) - 1;
    time->rtc_mon = (value_tc2 & RTC_TC_MONTH_MASK) >> RTC_TC_MONTH_OFFSET;
    time->rtc_hour = value_tc1 & RTC_TC_HOUR_MASK;
    time->rtc_day = (value_tc1 & RTC_TC_DOM_MASK) >> RTC_TC_DOM_OFFSET;
    time->rtc_min = (value_tc0 & RTC_TC_MINUTE_MASK) >> RTC_TC_MINUTE_OFFSET;
    time->rtc_sec = value_tc0 & RTC_TC_SECOND_MASK;
    time->rtc_milli_sec = int_cnt;

    return HAL_RTC_STATUS_OK;
}

#ifdef HAL_RTC_FEATURE_TIME_CALLBACK
hal_rtc_status_t hal_rtc_set_time_notification_period(hal_rtc_time_notification_period_t period)
{
    uint32_t enable;
    uint32_t cii_setting;

    switch (period) {
        case HAL_RTC_TIME_NOTIFICATION_NONE:
            enable = 0;
            cii_setting = 0;
            break;
        case HAL_RTC_TIME_NOTIFICATION_EVERY_SECOND:
            enable = 1;
            cii_setting = 3;
            break;
        case HAL_RTC_TIME_NOTIFICATION_EVERY_MINUTE:
            enable = 1;
            cii_setting = 4;
            break;
        case HAL_RTC_TIME_NOTIFICATION_EVERY_HOUR:
            enable = 1;
            cii_setting = 5;
            break;
        case HAL_RTC_TIME_NOTIFICATION_EVERY_DAY:
            enable = 1;
            cii_setting = 6;
            break;
        case HAL_RTC_TIME_NOTIFICATION_EVERY_MONTH:
            enable = 1;
            cii_setting = 8;
            break;
        case HAL_RTC_TIME_NOTIFICATION_EVERY_YEAR:
            enable = 1;
            cii_setting = 9;
            break;
        case HAL_RTC_TIME_NOTIFICATION_EVERY_SECOND_1_2:
            enable = 1;
            cii_setting = 2;
            break;
        case HAL_RTC_TIME_NOTIFICATION_EVERY_SECOND_1_4:
            enable = 1;
            cii_setting = 1;
            break;
        case HAL_RTC_TIME_NOTIFICATION_EVERY_SECOND_1_8:
            enable = 1;
            cii_setting = 0;
            break;
        default:
            return HAL_RTC_STATUS_INVALID_PARAM;
    }

    /*RTC setting CG enable*/
    rtc_setting_cg_config(true);

    rtc_register->RTC_CII_EN = (enable << RTC_TC_EN_OFFSET) | cii_setting;
    /* set TICK_PWREN here */
    rtc_register->RTC_BBPU = RTC_KEY_BBPU_2 | (enable << RTC_TICK_PWREN_OFFSET);
    rtc_write_trigger_wait();

    /*RTC setting CG disable*/
    rtc_setting_cg_config(false);

    return HAL_RTC_STATUS_OK;
}
#endif

hal_rtc_status_t hal_rtc_set_alarm(const hal_rtc_time_t *time)
{
    if (!rtc_is_time_valid(time)) {
        return HAL_RTC_STATUS_INVALID_PARAM;
    }

    /*RTC setting CG enable*/
    rtc_setting_cg_config(true);

    rtc_register->RTC_AL0_UNION.RTC_AL0_CELLS.AL_SECOND = time->rtc_sec;
    if (rtc_register->RTC_AL0_UNION.RTC_AL0_CELLS.AL_MINUTE != time->rtc_min) {
        rtc_register->RTC_AL0_UNION.RTC_AL0_CELLS.AL_MINUTE = time->rtc_min;
    }
    if (rtc_register->RTC_AL1_UNION.RTC_AL1_CELLS.AL_DOM != time->rtc_day) {
        rtc_register->RTC_AL1_UNION.RTC_AL1_CELLS.AL_DOM = time->rtc_day;
    }
    if (rtc_register->RTC_AL1_UNION.RTC_AL1_CELLS.AL_HOUR != time->rtc_hour) {
        rtc_register->RTC_AL1_UNION.RTC_AL1_CELLS.AL_HOUR = time->rtc_hour;
    }
    if (rtc_register->RTC_AL2_UNION.RTC_AL2_CELLS.AL_MONTH != time->rtc_mon) {
        rtc_register->RTC_AL2_UNION.RTC_AL2_CELLS.AL_MONTH = time->rtc_mon;
    }
    if (rtc_register->RTC_AL3 != time->rtc_year) {
        rtc_register->RTC_AL3 = time->rtc_year;
    }

    rtc_write_trigger_wait();

    /*RTC setting CG disable*/
    rtc_setting_cg_config(false);

    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t hal_rtc_get_alarm(hal_rtc_time_t *time)
{
    uint16_t value_al3, value_al2, value_al1, value_al0;

    /*RTC setting CG enable*/
    rtc_setting_cg_config(true);
    value_al3 = rtc_register->RTC_AL3;
    value_al2 = rtc_register->RTC_AL2_UNION.RTC_AL2;
    value_al1 = rtc_register->RTC_AL1_UNION.RTC_AL1;
    value_al0 = rtc_register->RTC_AL0_UNION.RTC_AL0;
    /*RTC setting CG disable*/
    rtc_setting_cg_config(false);

    time->rtc_year = value_al3 & RTC_AL_YEAR_MASK;
    time->rtc_milli_sec = 0; // have no meaning, just init
    time->rtc_week = (value_al2 & RTC_AL_DOW_MASK) - 1;
    time->rtc_mon = (value_al2 & RTC_AL_MONTH_MASK) >> RTC_AL_MONTH_OFFSET;
    time->rtc_hour = value_al1 & RTC_AL_HOUR_MASK;
    time->rtc_day = (value_al1 & RTC_AL_DOM_MASK) >> RTC_AL_DOM_OFFSET;
    time->rtc_min = (value_al0 & RTC_AL_MINUTE_MASK) >> RTC_AL_MINUTE_OFFSET;
    time->rtc_sec = value_al0 & RTC_AL_SECOND_MASK;

    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t hal_rtc_enable_alarm(void)
{
    uint32_t frequency;
    int16_t cali;

    /*RTC setting CG enable*/
    rtc_setting_cg_config(true);
    if (rtc_register->RTC_AL_MASK_UNION.RTC_AL_MASK != ((RTC_AL_EN_MASK << 8) | RTC_AL_MASK_DOW_MASK) ||
        ((rtc_register->RTC_BBPU & RTC_ALARM_PWREN_MASK) != RTC_ALARM_PWREN_MASK)) {

        /* Enable alarm interrupt, mask DOW value for alarm detection */
        rtc_register->RTC_AL_MASK_UNION.RTC_AL_MASK_CELLS.AL_MASK = RTC_AL_MASK_DOW_MASK;
        rtc_register->RTC_AL_MASK_UNION.RTC_AL_MASK_CELLS.AL_EN = RTC_AL_EN_MASK;
        /* Enable alarm power on */
        rtc_register->RTC_BBPU = RTC_KEY_BBPU_0 | RTC_ALARM_PWREN_MASK;

        rtc_write_trigger_wait();
    }
    /*RTC setting CG disable*/
    rtc_setting_cg_config(false);

    if (use_xosc) {
        /* Measure 32K */
        frequency = f32k_measure_clock(FQMTR_FCKSEL_XOSC_F32K_CK, FQMTR_TCKSEL_XO_CK, 99);
        /* Set RTC_CALI */
        cali = (32768 - frequency) << 3;
        if (cali > (int16_t)0xFFF) {
            cali = 0xFFF;
        } else if (cali < (int16_t)0xF000) {
            cali = 0xF000;
        }
        /* normal RTC_CALI */
        rtc_register->RTC_CALI = cali;
        rtc_write_trigger_wait();
        RTC_LOG_MSGID_INFO("!!xosc frequency = %u, RTC_CALI = %u\r\n",2, (unsigned int)frequency, cali);
    }

    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t hal_rtc_disable_alarm(void)
{
    /*RTC setting CG enable*/
    rtc_setting_cg_config(true);
    rtc_register->RTC_AL_MASK_UNION.RTC_AL_MASK_CELLS.AL_EN = (0x0 << RTC_AL_EN_OFFSET);

    rtc_register->RTC_BBPU = RTC_KEY_BBPU_0;

    rtc_write_trigger_wait();

    /*RTC setting CG disable*/
    rtc_setting_cg_config(false);

    return HAL_RTC_STATUS_OK;
}

#ifdef HAL_RTC_FEATURE_TIME_CALLBACK
hal_rtc_status_t hal_rtc_set_time_callback(hal_rtc_time_callback_t callback_function, void *user_data)
{
    rtc_time_callback_function = callback_function;
    rtc_time_user_data = user_data;

    return HAL_RTC_STATUS_OK;
}
#endif

hal_rtc_status_t hal_rtc_set_alarm_callback(const hal_rtc_alarm_callback_t callback_function, void *user_data)
{
    rtc_alarm_callback_function = callback_function;
    rtc_alarm_user_data = user_data;

    return HAL_RTC_STATUS_OK;
}
#if 0
hal_rtc_status_t hal_rtc_set_eint_callback(const hal_rtc_eint_callback_t callback_function, void *user_data)
{
    rtc_eint_callback_function = callback_function;
    rtc_eint_user_data = user_data;

    return HAL_RTC_STATUS_OK;
}
#endif
hal_rtc_status_t hal_rtc_get_f32k_frequency(uint32_t *frequency)
{
    *frequency = f32k_measure_clock(FQMTR_FCKSEL_RTC_32K, FQMTR_TCKSEL_XO_CK, 99);
    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t rtc_set_data(uint16_t offset, const char *buf, uint16_t len, bool access_hw)
{
    uint16_t i = 0;

    if ((offset >= (sizeof(rtc_spare_register_table) / sizeof(rtc_spare_register_table[0]))) ||
        (offset + len > (sizeof(rtc_spare_register_table) / sizeof(rtc_spare_register_table[0]))) || (buf == NULL)) {
        RTC_LOG_MSGID_ERROR("Invalid parameter, offset = %d, len = %d, buf = 0x%x", 3, offset, len, buf);
        return HAL_RTC_STATUS_INVALID_PARAM;
    }


    for (i = 0; i < len; i++) {
        if (access_hw) {
            *(rtc_spare_register_table[i + offset].address) = *(buf + i);
        } else {
            rtc_spare_register_backup[offset + i] = *(buf + i);
        }
    }
    rtc_write_trigger_wait();
    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t hal_rtc_set_data(uint16_t offset, const char *buf, uint16_t len)
{
    if (rtc_in_test) {
        RTC_LOG_MSGID_INFO("hal_rtc_set_data: in rtc test mode.",0);
    }

    rtc_setting_cg_config(true);
    rtc_set_data(offset + RTC_RESV_RG_SIZE, buf, len, true);
    rtc_setting_cg_config(false);

    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t rtc_get_data(uint16_t offset, char *buf, uint16_t len, bool access_hw)
{
    uint16_t i = 0;

    if ((offset >= (sizeof(rtc_spare_register_table) / sizeof(rtc_spare_register_table[0]))) ||
        (offset + len > (sizeof(rtc_spare_register_table) / sizeof(rtc_spare_register_table[0]))) || (buf == NULL)) {
        RTC_LOG_MSGID_ERROR("Invalid parameter, offset = %d, len = %d, buf = 0x%x", 3, offset, len, buf);
        return HAL_RTC_STATUS_INVALID_PARAM;
    }

    for (i = 0; i < len; i++) {
        if (access_hw) {
            rtc_setting_cg_config(true);
            *(buf + i) = *(rtc_spare_register_table[i + offset].address);
            rtc_setting_cg_config(false);
        } else {
            *(buf + i) = rtc_spare_register_backup[offset + i];
        }
    }

    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t hal_rtc_get_data(uint16_t offset, char *buf, uint16_t len)
{
    if (rtc_in_test) {
        RTC_LOG_MSGID_INFO("hal_rtc_get_data: in rtc test mode.", 0);
    }

    rtc_get_data(offset + RTC_RESV_RG_SIZE, buf, len, !rtc_in_test);

    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t rtc_clear_data(uint16_t offset, uint16_t len)
{
    char buf[RTC_SPAR_RG_SIZE];

    memset(buf, 0, sizeof(buf));

    rtc_set_data(offset, buf, len, true);

    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t hal_rtc_clear_data(uint16_t offset, uint16_t len)
{
    char buf[RTC_SPAR_RG_SIZE];

    if (rtc_in_test) {
        RTC_LOG_MSGID_INFO("%s: in rtc test mode",0);
    }

    memset(buf, 0, sizeof(buf));

    rtc_set_data(offset, buf, len, !rtc_in_test);

    return HAL_RTC_STATUS_OK;
}
#if 0
void rtc_set_register(uint16_t address, uint16_t value)
{
    if (address > (uint32_t) & (((RTC_REGISTER_T *)0)->RTC_SPAR5_UNION.RTC_SPAR5)) {
        RTC_LOG_MSGID_ERROR("Invalid address", 0);
    }


    *(uint16_t *)((uint8_t *)rtc_register + address) = value;
}

uint16_t rtc_get_register(uint16_t address)
{
    if (address > (uint32_t) & (((RTC_REGISTER_T *)0)->RTC_SPAR5_UNION.RTC_SPAR5)) {
        RTC_LOG_MSGID_ERROR("Invalid address", 0);
    }

    return *(uint16_t *)((uint8_t *)rtc_register + address);
}
#endif
static void rtc_forward_time(hal_rtc_time_t *time, int second)
{
    int minute = 0;
    int hour = 0;
    int day = 0;
    int remender = 0;
    int max_day;
    const int days_in_month[13] = {
        0,  /* Null */
        31, /* Jan */
        28, /* Feb */
        31, /* Mar */
        30, /* Apr */
        31, /* May */
        30, /* Jun */
        31, /* Jul */
        31, /* Aug */
        30, /* Sep */
        31, /* Oct */
        30, /* Nov */
        31  /* Dec */
    };

    second += time->rtc_sec;
    minute = time->rtc_min;
    hour = time->rtc_hour;

    if (second > 59) {
        /* min */
        minute += second / 60;
        second %= 60;
    }
    time->rtc_sec = second;
    if (minute > 59) {
        /* hour */
        hour += minute / 60;
        minute %= 60;
    }
    time->rtc_min = minute;
    if (hour > 23) {
        /* day of week */
        day = hour / 24;
        hour %= 24;
        /* day of month */
        time->rtc_day += day;
        max_day = days_in_month[time->rtc_mon];
        if (time->rtc_mon == 2) {
            remender = time->rtc_year % 4;
            if (remender == 0) {
                max_day++;
            }
        }
        if (time->rtc_day > max_day) {
            time->rtc_day -= max_day;

            /* month of year */
            time->rtc_mon++;
            if (time->rtc_mon > 12) {
                time->rtc_mon = 1;
                time->rtc_year++;
            }
        }
    }
    time->rtc_hour = hour;
}

static void test_rtc_alarm_callback(void *parameter)
{
    hal_rtc_time_t *alarm_power_on_time;
    RTC_LOG_MSGID_INFO("test_rtc_alarm_callback", 0);

    alarm_power_on_time = (hal_rtc_time_t *)parameter;

    RTC_LOG_MSGID_INFO("target alarm time: 20%d,%d,%d (%d) %d:%d:%d",7, alarm_power_on_time->rtc_year,
                 alarm_power_on_time->rtc_mon, alarm_power_on_time->rtc_day, alarm_power_on_time->rtc_week,
                 alarm_power_on_time->rtc_hour, alarm_power_on_time->rtc_min, alarm_power_on_time->rtc_sec);
    hal_rtc_set_alarm_callback(NULL, NULL);

    hal_rtc_set_alarm(alarm_power_on_time);
    hal_rtc_enable_alarm();
}

hal_rtc_status_t rtc_alarm_power_on_test(hal_rtc_time_t *time)
{
    static hal_rtc_time_t alarm_power_on_time;
    hal_rtc_time_t rtc_get_time;

    memcpy(&alarm_power_on_time, time, sizeof(hal_rtc_time_t));

    RTC_LOG_MSGID_INFO("target alarm time: 20%d,%d,%d (%d) %d:%d:%d",7, time->rtc_year,
                 time->rtc_mon, time->rtc_day, time->rtc_week, time->rtc_hour, time->rtc_min, time->rtc_sec);
    hal_rtc_get_time(&rtc_get_time);
    RTC_LOG_MSGID_INFO("get alarm time: 20%d,%d,%d (%d) %d:%d:%d",7, rtc_get_time.rtc_year,
                 rtc_get_time.rtc_mon, rtc_get_time.rtc_day, rtc_get_time.rtc_week - 1,
                 rtc_get_time.rtc_hour, rtc_get_time.rtc_min, rtc_get_time.rtc_sec);
    rtc_forward_time(&rtc_get_time, 10);
    RTC_LOG_MSGID_INFO("set alarm time: 20%d,%d,%d (%d) %d:%d:%d",7, rtc_get_time.rtc_year,
                 rtc_get_time.rtc_mon, rtc_get_time.rtc_day, rtc_get_time.rtc_week - 1,
                 rtc_get_time.rtc_hour, rtc_get_time.rtc_min, rtc_get_time.rtc_sec);
    hal_rtc_set_alarm(&rtc_get_time);
    hal_rtc_set_alarm_callback(test_rtc_alarm_callback, &alarm_power_on_time);
    hal_rtc_enable_alarm();

    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t rtc_enter_test(bool enter)
{
    RTC_LOG_MSGID_INFO("rtc_enter_test: %d",1, enter);

    if (enter) {
        rtc_get_data(0, rtc_spare_register_backup, RTC_SPAR_RG_SIZE, true);
    } else {
        rtc_set_data(0, rtc_spare_register_backup, RTC_SPAR_RG_SIZE, true);
    }
    rtc_in_test = enter;

    return HAL_RTC_STATUS_OK;
}

#ifdef HAL_RTC_FEATURE_GPIO_EINT
hal_rtc_status_t hal_rtc_eint_init(hal_rtc_eint_config_t *eint_config)
{
    uint8_t value = 0x0;
    uint32_t chip_version = 0;
    chip_version = chip_eco_version();

    if (eint_config == NULL) {
        return HAL_RTC_STATUS_INVALID_PARAM;
    }
    rtc_setting_cg_config(true);
    switch (eint_config->rtc_gpio) {
        case HAL_RTC_GPIO_1:
            if (eint_config->is_enable_rtc_eint == true) {
                /* EINT_EN, and EINT_IRQ_EN set to 0. */
                rtc_register->RTC_EINT_UNION.RTC_EINT_CELLS.EINT1_CON &= (~0x11);
                rtc_write_trigger_wait();
                /* SYNC_EN set to 1. */
                value |= 0x4;
                if (eint_config->is_falling_edge_active == true) {
                    /* If falling edge active setting, need set PU first, default is PD. */
                    rtc_register->RTC_GPIO1_CON = 0x184;
                    rtc_write_trigger_wait();
                    value |= 0x8;
                } else if (eint_config->is_falling_edge_active == false) {
                    /* If rising edge active setting, set to PD in case user set PU before */
                    if (chip_version == 1) {
                        rtc_register->RTC_GPIO1_CON = 0x182;
                    } else if (chip_version == 2) {
                        rtc_register->RTC_GPIO1_CON = 0x1c0;
                    }
                    rtc_write_trigger_wait();
                }
                if (eint_config->is_enable_debounce == true) {
                    value |= 0x2;
                }
                rtc_register->RTC_EINT_UNION.RTC_EINT_CELLS.EINT1_CON = value;
                rtc_register->RTC_BBPU = RTC_KEY_BBPU_3 | RTC_EINT1_PWREN_MASK;
                rtc_write_trigger_wait();
                /* EINT_EN and EINT_IRQ_EN set to 1. */
                rtc_register->RTC_EINT_UNION.RTC_EINT_CELLS.EINT1_CON |= (0x11);
                rtc_write_trigger_wait();
            } else {
                /* EINT_EN, and EINT_IRQ_EN set to 0. */
                rtc_register->RTC_EINT_UNION.RTC_EINT_CELLS.EINT1_CON &= (~0x11);
                rtc_write_trigger_wait();
            }
            break;
        case HAL_RTC_GPIO_2:
            if (eint_config->is_enable_rtc_eint == true) {
                /* EINT_EN, and EINT_IRQ_EN set to 0. */
                rtc_register->RTC_EINT_UNION.RTC_EINT_CELLS.EINT2_CON &= (~0x11);
                rtc_write_trigger_wait();
                /* SYNC_EN set to 1. */
                value |= 0x4;
                if (eint_config->is_falling_edge_active == true) {
                    /* If falling edge active setting, need set PU first, default is PD. */
                    rtc_register->RTC_GPIO2_CON = 0x184;
                    rtc_write_trigger_wait();
                    value |= 0x8;
                } else if (eint_config->is_falling_edge_active == false) {
                    /* If rising edge active setting, set to PD in case user set PU before */
                    if (chip_version == 1) {
                        rtc_register->RTC_GPIO2_CON = 0x182;
                    } else if (chip_version == 2) {
                        rtc_register->RTC_GPIO2_CON = 0x1c0;
                    }
                    rtc_write_trigger_wait();
                }
                if (eint_config->is_enable_debounce == true) {
                    value |= 0x2;
                }
                rtc_register->RTC_EINT_UNION.RTC_EINT_CELLS.EINT2_CON = value;
                rtc_register->RTC_BBPU = RTC_KEY_BBPU_4 | RTC_EINT2_PWREN_MASK;
                rtc_write_trigger_wait();
                /* EINT_EN and EINT_IRQ_EN set to 1. */
                rtc_register->RTC_EINT_UNION.RTC_EINT_CELLS.EINT2_CON |= (0x11);
                rtc_write_trigger_wait();
            } else {
                /* EINT_EN, and EINT_IRQ_EN set to 0. */
                rtc_register->RTC_EINT_UNION.RTC_EINT_CELLS.EINT2_CON &= (~0x11);
                rtc_write_trigger_wait();
            }
            break;
        default:
            break;
    }
    rtc_setting_cg_config(false);
    return HAL_RTC_STATUS_OK;
}
#endif

#ifdef HAL_RTC_FEATURE_CAPTOUCH
hal_rtc_status_t hal_rtc_captouch_init(void)
{
    if (rtc_init_done == false) {
        return HAL_RTC_STATUS_ERROR;
    }
    rtc_setting_cg_config(true);
    /* Release reset: cap_con[0] = 1 */
    rtc_register->RTC_CAP_CON |= RTC_CAP_RST_MASK;
    rtc_write_trigger_wait();

    /* Release clock: cap_con[2] = 1 */
    rtc_register->RTC_CAP_CON |= RTC_CAP_CLOCK_MASK;
    rtc_write_trigger_wait();

    /* Release ISO: cap_con[1] = 0 */
    rtc_register->RTC_CAP_CON &= (~RTC_CAP_ISO_MASK);
    rtc_write_trigger_wait();

    rtc_register->RTC_BBPU = RTC_KEY_BBPU_5 | RTC_CAP_PWREN_MASK;
    rtc_write_trigger_wait();

    rtc_setting_cg_config(false);
    return HAL_RTC_STATUS_OK;
}

hal_rtc_status_t hal_rtc_captouch_deinit(void)
{
    if (rtc_init_done == false) {
        return HAL_RTC_STATUS_ERROR;
    }
    rtc_setting_cg_config(true);
    /* Set ISO: cap_con[1] = 1 */
    rtc_register->RTC_CAP_CON |= RTC_CAP_ISO_MASK;
    rtc_write_trigger_wait();

    /* Gated clock: cap_con[2] = 0 */
    rtc_register->RTC_CAP_CON &= (~RTC_CAP_CLOCK_MASK);
    rtc_write_trigger_wait();

    /* Enable reset: cap_con[0] = 0 */
    rtc_register->RTC_CAP_CON &= (~RTC_CAP_RST_MASK);
    rtc_write_trigger_wait();

    rtc_register->RTC_BBPU = RTC_KEY_BBPU_5;
    rtc_write_trigger_wait();

    rtc_setting_cg_config(false);
    return HAL_RTC_STATUS_OK;
}

void rtc_captouch_ack(void)
{
    rtc_setting_cg_config(true);
    rtc_register->RTC_BBPU = RTC_KEY_BBPU_1 | RTC_RTC_PU_MASK;
    rtc_write_trigger_wait();
    rtc_setting_cg_config(false);
}
#endif

#ifdef HAL_RTC_FEATURE_RTC_MODE
extern void pmu_power_off_sequence(pmu_power_stage_t stage);


hal_rtc_status_t hal_rtc_enter_rtc_mode(void)
{
    rtc_setting_cg_config(true);

    /*switch 32k clock*/
    if(g_rtc_osc_mode != RTC_OSC_XOSC){
        rtc_select_osc_mode(RTC_OSC_EOSC);
    }
#ifdef HAL_CAPTOUCH_MODULE_ENABLED
    hal_captouch_lowpower_control(HAL_CAPTOUCH_MODE_LOWPOWER);
#endif
    log_hal_msgid_info("###### system enter rtc mode... ######\r\n", 0);

    /* Prepare flag for Bootrom */
    rtc_setting_cg_config(true);
    rtc_register->RTC_SPAR_REG_UNION.RTC_SPAR_REG_CELLS.RTC_SPAR_REG_0 |= RTC_STANDARD_1_MASK;
    rtc_write_trigger_wait();
    /*clear power on status*/
    rtc_register->RTC_BBPU = RTC_KEY_BBPU_1 | RTC_RTC_PU_MASK;
    rtc_write_trigger_wait();

    /*print rtc rg*/
    rtc_dump_register(NULL);
    hal_gpt_delay_ms(50); // use delay to make sure log can output before enter RTC mode

    rtc_setting_cg_config(false);

    /* call pmic API to turn off power */
    pmu_power_off_sequence(1);
    hal_gpt_delay_ms(2000);

    rtc_setting_cg_config(true);
    log_hal_msgid_info("enter rtc mode failed, IRQ_STA=%x,BBPU=%x\r\n", 2, rtc_register->RTC_IRQ_STA, rtc_register->RTC_BBPU);
    rtc_dump_register(NULL);
    hal_gpt_delay_ms(50); // use delay to make sure log can output before enter RTC mode
    rtc_setting_cg_config(false);
    return HAL_RTC_STATUS_ERROR;
}
#endif

#endif

