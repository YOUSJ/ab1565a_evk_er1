/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#include <stdio.h>
#include <stdlib.h>

#include "hal_clock_platform.h"
#include "hal_clock.h"
#include "hal_clock_internal.h"
#include "hal.h"
#include "hal_dcxo.h"
volatile uint32_t dcxo_capid = DCXO_DEFAULT_VAL;

uint32_t get_capid(void)
{
    uint32_t dcxo_cap_id;
    dcxo_cap_id = ((*DCXO_CAP_ID) & DCXO_CAP_ID_MASK ) >> DCXO_CAP_ID_BIT;

    return dcxo_cap_id;
}

void set_capid(uint32_t target_capid)
{
    uint32_t dcxo_cap_id;
    dcxo_cap_id = *DCXO_CAP_ID;
    dcxo_cap_id &= (~DCXO_CAP_ID_MASK);
    dcxo_cap_id |= (target_capid << DCXO_CAP_ID_BIT);
    *DCXO_CAP_ID = dcxo_cap_id;

    /* set sel = 1 */
    *DCXO_CAP_ID = (0x00000001 | dcxo_cap_id);

    hal_gpt_delay_us(4);
}
extern bool chip_is_ab1552(void);
void hal_dcxo_init(void)
{
    set_capid(dcxo_capid);
    log_hal_msgid_info("Load CAPID done, CAPID RG = %lx\r\n", 1, (long unsigned int)get_capid());

    //log_hal_info("DCXO init\r\n");
    *DCXO_PCON0__F_GSM_DCXO_CTL_EN = 0x1;  // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0000, enable baseband control
    *DCXO_PCON1__F_EXT_DCXO_CTL_EN = 0x1;  // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0007, enable external control

#ifdef MTK_HAL_EXT_32K_ENABLE
    if(chip_is_ab1552()){
        *DCXO_PCON4__F_DCXO_PWR_EN_TD  = 0x1; //DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0010, 1T 32K to turn on DCXO power after receive enable from SPM
        *DCXO_PCON3__F_DCXO_EN_TD = 0xA; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x000f, set DCXO power settle time to 305.1us
        *DCXO_PCON3__F_DCXO_BUF_EN_TD = 0x52; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x000e, set DCXO settle time to 2500us
        *DCXO_PCON3__F_DCXO_ISO_EN_TD = 0x10; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x000d, set DCXO isolation release time to 510us (>450us)
        *DCXO_PCON4__F_LPM_EN = 0x0; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0012, power down DCXO at sleep mode

        //*ABB_CFG_LPOSC_ANA_CON0[14:10] = 0x1X; // ABB_CFG_BASE (0XA2070000) + 0x0300, update 26M_RDY detect threshold, [14] sel, [13:10] value, need RF DVT update
        *DCXO_PCON5__F_DCXO_ACAL_EFUSE = 0x0; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0016, enable 26M ready auto detect
        *DCXO_PCON5__F_DCXO_ACAL_EFUSE_SEL = 0x1; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0014, enable 26M ready auto detect
        *DCXO_PCON__F_DCXO_26M_RDY_EN = 0x1; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0013, enable 26M ready auto detect
    }else{
        *DCXO_PCON4__F_DCXO_PWR_EN_TD  = 0x1; //DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0010, 1T 32K to turn on DCXO power after receive enable from SPM
        *DCXO_PCON3__F_DCXO_EN_TD = 0x1; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x000f, set DCXO power settle time to 305.1us
        *DCXO_PCON3__F_DCXO_BUF_EN_TD = 0x14; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x000e, set DCXO settle time to 600us
        *DCXO_PCON3__F_DCXO_ISO_EN_TD = 0x10; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x000d, set DCXO isolation release time to 510us (>450us)
        *DCXO_PCON4__F_LPM_EN = 0x1; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0012, keep DCXO power at sleep mode
    }
    //*ABB_CFG_LPOSC_ANA_CON0[14:10] = 0x1X; // ABB_CFG_BASE (0XA2070000) + 0x0300, update 26M_RDY detect threshold, [14] sel, [13:10] value, need RF DVT update
    *DCXO_PCON5__F_DCXO_ACAL_EFUSE = 0x0; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0016, enable 26M ready auto detect
    *DCXO_PCON5__F_DCXO_ACAL_EFUSE_SEL = 0x1; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0014, enable 26M ready auto detect
    *DCXO_PCON__F_DCXO_26M_RDY_EN = 0x1; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0013, enable 26M ready auto detect
#else
    *DCXO_PCON4__F_DCXO_PWR_EN_TD  = 0x1; //DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0010, 1T 32K to turn on DCXO power after receive enable from SPM
    *DCXO_PCON3__F_DCXO_EN_TD = 0x1; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x000f, set DCXO power settle time to 305.1us
    *DCXO_PCON3__F_DCXO_BUF_EN_TD = 0x14; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x000e, set DCXO settle time to 600us
    *DCXO_PCON3__F_DCXO_ISO_EN_TD = 0x10; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x000d, set DCXO isolation release time to 510us (>450us)
    *DCXO_PCON4__F_LPM_EN = 0x1; // DCXO_PWR_CTRL_BASE (0XA2060000) + 0x0012, keep DCXO power at sleep mode
#endif /* MTK_HAL_EXT_32K_ENABLE */
}

//#define DCXO_DVT

#ifdef DCXO_DVT
#define DEBUG_MON_DCXO                  ((volatile uint32_t*)(0xA2060100))
#define TOP_DEBUG                       ((volatile uint32_t*)(0xA2010060))

#include "hal_platform.h"
#include "hal_sleep_manager.h"
#include "hal_spm.h"

static void __CPU_DELAY2(uint32_t ptr)
{
    for(int i = 0;i < ptr;i++){
        __asm volatile(
            "NOP                                    \n"
        );
    }
}

#define DCXO_MON_SEL    3   // 0, 1

void dcxo_test(void)
{
    uint32_t val = 0;

    log_hal_msgid_info("dcxo_test start:\r\n", 0);

#if 0   //GPIO TEST
    //////////////////////////////////
    log_hal_msgid_info("ready\r\n",0);
    __CPU_DELAY2(0xFFFFFF);
    log_hal_msgid_info("go\r\n",0);

    hal_gpio_init(HAL_GPIO_40);
    hal_pinmux_set_function(HAL_GPIO_40, 0);

    hal_gpio_init(HAL_GPIO_61);
    hal_pinmux_set_function(HAL_GPIO_61, 0);

    hal_gpio_init(HAL_GPIO_22);
    hal_pinmux_set_function(HAL_GPIO_22, 0);

    hal_gpio_init(HAL_GPIO_23);
    hal_pinmux_set_function(HAL_GPIO_23, 0);

    hal_gpio_init(HAL_GPIO_64);
    hal_pinmux_set_function(HAL_GPIO_64, 0);

    hal_gpio_init(HAL_GPIO_65);
    hal_pinmux_set_function(HAL_GPIO_65, 0);

    hal_gpio_init(HAL_GPIO_26);
    hal_pinmux_set_function(HAL_GPIO_26, 0);

    hal_gpio_init(HAL_GPIO_67);
    hal_pinmux_set_function(HAL_GPIO_67, 0);

    hal_gpio_set_direction(HAL_GPIO_40, HAL_GPIO_DIRECTION_OUTPUT);
    hal_gpio_set_direction(HAL_GPIO_61, HAL_GPIO_DIRECTION_OUTPUT);
    hal_gpio_set_direction(HAL_GPIO_22, HAL_GPIO_DIRECTION_OUTPUT);
    hal_gpio_set_direction(HAL_GPIO_23, HAL_GPIO_DIRECTION_OUTPUT);
    hal_gpio_set_direction(HAL_GPIO_64, HAL_GPIO_DIRECTION_OUTPUT);
    hal_gpio_set_direction(HAL_GPIO_65, HAL_GPIO_DIRECTION_OUTPUT);
    hal_gpio_set_direction(HAL_GPIO_26, HAL_GPIO_DIRECTION_OUTPUT);
    hal_gpio_set_direction(HAL_GPIO_67, HAL_GPIO_DIRECTION_OUTPUT);

    hal_gpio_set_output(HAL_GPIO_40, 1);
    hal_gpio_set_output(HAL_GPIO_61, 1);
    hal_gpio_set_output(HAL_GPIO_22, 1);
    hal_gpio_set_output(HAL_GPIO_23, 1);
    hal_gpio_set_output(HAL_GPIO_64, 1);
    hal_gpio_set_output(HAL_GPIO_65, 1);
    hal_gpio_set_output(HAL_GPIO_26, 1);
    hal_gpio_set_output(HAL_GPIO_67, 1);
    __CPU_DELAY2(0xFFFF);
    hal_gpio_set_output(HAL_GPIO_40, 0);
    hal_gpio_set_output(HAL_GPIO_61, 0);
    hal_gpio_set_output(HAL_GPIO_22, 0);
    hal_gpio_set_output(HAL_GPIO_23, 0);
    hal_gpio_set_output(HAL_GPIO_64, 0);
    hal_gpio_set_output(HAL_GPIO_65, 0);
    hal_gpio_set_output(HAL_GPIO_26, 0);
    hal_gpio_set_output(HAL_GPIO_67, 0);
    __CPU_DELAY2(0xFFFF);
    hal_gpio_set_output(HAL_GPIO_40, 1);
    hal_gpio_set_output(HAL_GPIO_61, 1);
    hal_gpio_set_output(HAL_GPIO_22, 1);
    hal_gpio_set_output(HAL_GPIO_23, 1);
    hal_gpio_set_output(HAL_GPIO_64, 1);
    hal_gpio_set_output(HAL_GPIO_65, 1);
    hal_gpio_set_output(HAL_GPIO_26, 1);
    hal_gpio_set_output(HAL_GPIO_67, 1);
    __CPU_DELAY2(0xFFFF);
    hal_gpio_set_output(HAL_GPIO_40, 0);
    hal_gpio_set_output(HAL_GPIO_61, 0);
    hal_gpio_set_output(HAL_GPIO_22, 0);
    hal_gpio_set_output(HAL_GPIO_23, 0);
    hal_gpio_set_output(HAL_GPIO_64, 0);
    hal_gpio_set_output(HAL_GPIO_65, 0);
    hal_gpio_set_output(HAL_GPIO_26, 0);
    hal_gpio_set_output(HAL_GPIO_67, 0);
    __CPU_DELAY2(0xFFFF);
    hal_gpio_set_output(HAL_GPIO_40, 1);
    hal_gpio_set_output(HAL_GPIO_61, 1);
    hal_gpio_set_output(HAL_GPIO_22, 1);
    hal_gpio_set_output(HAL_GPIO_23, 1);
    hal_gpio_set_output(HAL_GPIO_64, 1);
    hal_gpio_set_output(HAL_GPIO_65, 1);
    hal_gpio_set_output(HAL_GPIO_26, 1);
    hal_gpio_set_output(HAL_GPIO_67, 1);
    __CPU_DELAY2(0xFFFF);
    hal_gpio_set_output(HAL_GPIO_40, 0);
    hal_gpio_set_output(HAL_GPIO_61, 0);
    hal_gpio_set_output(HAL_GPIO_22, 0);
    hal_gpio_set_output(HAL_GPIO_23, 0);
    hal_gpio_set_output(HAL_GPIO_64, 0);
    hal_gpio_set_output(HAL_GPIO_65, 0);
    hal_gpio_set_output(HAL_GPIO_26, 0);
    hal_gpio_set_output(HAL_GPIO_67, 0);
    __CPU_DELAY2(0xFFFF);

    while(1);
    //////////////////////////////////
#else

    // GPIO40 aux. 8 for DEBUGMON0
    hal_gpio_init(HAL_GPIO_40);
    hal_pinmux_set_function(HAL_GPIO_40, 8);

    // GPIO61 aux. 8 for DEBUGMON1
    hal_gpio_init(HAL_GPIO_61);
    hal_pinmux_set_function(HAL_GPIO_61, 8);

    // GPIO22 aux. 8 for DEBUGMON2
    hal_gpio_init(HAL_GPIO_22);
    hal_pinmux_set_function(HAL_GPIO_22, 8);

    // GPIO23 aux. 8 for DEBUGMON3
    hal_gpio_init(HAL_GPIO_23);
    hal_pinmux_set_function(HAL_GPIO_23, 8);

    // GPIO64 aux. 8 for DEBUGMON4
    hal_gpio_init(HAL_GPIO_64);
    hal_pinmux_set_function(HAL_GPIO_64, 8);

    // GPIO65 aux. 8 for DEBUGMON5
    hal_gpio_init(HAL_GPIO_65);
    hal_pinmux_set_function(HAL_GPIO_65, 8);

    // GPIO26 aux. 8 for DEBUGMON6
    hal_gpio_init(HAL_GPIO_26);
    hal_pinmux_set_function(HAL_GPIO_26, 8);

    // GPIO67 aux. 8 for DEBUGMON7
    hal_gpio_init(HAL_GPIO_67);
    hal_pinmux_set_function(HAL_GPIO_67, 8);

    //set SRCLKENAI mode
    //hal_gpio_init(HAL_GPIO_23);
    //hal_pinmux_set_function(HAL_GPIO_23, 4);

    //set SRCLKENAI mode
    hal_gpio_init(HAL_GPIO_37);
    hal_pinmux_set_function(HAL_GPIO_37, 3);

    // enable debug monitor for idle signal
    *TOP_DEBUG = 0x9;

    val = *DEBUG_MON_DCXO;
    val &= ~(0x00030000);
    val |= (DCXO_MON_SEL << 16);    //  mon_sel, bit[16:17]
    val |= (0x1);   //  mon_en, bit[0]
    *DEBUG_MON_DCXO = val;

    spm_control_mtcmos(SPM_MTCMOS_DSP0, SPM_MTCMOS_PWR_DISABLE);
    spm_control_mtcmos(SPM_MTCMOS_DSP1, SPM_MTCMOS_PWR_DISABLE);
    spm_control_mtcmos(SPM_MTCMOS_AUDIO, SPM_MTCMOS_PWR_DISABLE);
    spm_control_mtcmos(SPM_MTCMOS_CONN, SPM_MTCMOS_PWR_DISABLE);

    log_hal_msgid_info("ready to sleep \r\n",0);
    __CPU_DELAY2(0x1FFFFFF);
    log_hal_msgid_info("go sleep \r\n",0);

    for(int i = 0;i < 32;i++){
        if(NVIC_GetPendingIRQ(i) !=0 ){
            NVIC_ClearPendingIRQ(i);
            //log_hal_msgid_info("NVIC_GetPendingIRQ:%d\r\n",1,i);
        }
    }

    *DCXO_PCON1__F_ENFRC_COCLK_EN = 1;

    hal_rtc_enter_rtc_mode();   // switch 32k to EOSC

    while(1){
    //__CPU_DELAY(0xFFFF);
    __asm volatile("cpsid i");
    hal_sleep_manager_set_sleep_time(2000);
    sleep_management_enter_deep_sleep(0);
    __asm volatile("cpsie i");
    __CPU_DELAY2(0x1FFFFFF);
    }
#endif
    log_hal_msgid_info("dcxo_test end:\r\n", 0);
}

#endif  //DCXO_TEST
