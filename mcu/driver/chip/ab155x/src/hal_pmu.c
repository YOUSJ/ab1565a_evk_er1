/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_pmu.h"

#ifdef HAL_PMU_MODULE_ENABLED
#include "hal_i2c_master.h"
#include "hal_platform.h"
#include "hal_nvic_internal.h"
#include "hal_pmu_mt6388_platform.h"
#include "hal_eint.h"
#include "hal_pmu_auxadc.h"

#include "hal_i2c_master_internal.h"
#include "hal_clock.h"
#include "stdint.h"
#include "hal.h"
#include "assert.h"
#include "hal_sleep_manager_platform.h"
#include "hal_sleep_manager_internal.h"
#include "hal_sleep_manager.h"
#ifdef  HAL_PMU_DEBUG_ENABLE
#define pmu_info(_message,...) log_hal_info("[PMU] "_message, ##__VA_ARGS__)
#define pmu_msgid_info(_message,...) log_hal_msgid_info("[PMU] "_message, ##__VA_ARGS__)
#else
#define pmu_info(_message,...)
#define pmu_msgid_info(_message,...)
#endif

/* Set system clock to 26M Hz */
#ifdef MTK_SYSTEM_CLOCK_26M
static const uint32_t target_freq = 26000;
#else
/* Set system clock to 78M Hz */
#ifdef MTK_SYSTEM_CLOCK_78M
static const uint32_t target_freq = 78000;
#else
/* Set system clock to 156M Hz */
static const uint32_t target_freq = 156000;
#endif
#endif
int old_index;
uint8_t pmu_basic_index;
pmu_function_t pmu_function_table[PMU_INT_MAX];
static unsigned char Vcore_Resource_Ctrl[8];
uint8_t pmu_lock_status=0;
uint8_t pmu_charger_status;
uint32_t pmu_register_interrupt ; //0~31
uint8_t pmu_register_interrupt_2 ; //39~39
uint8_t pmu_audio_is_running=0;
uint8_t cur_pk_sat=-1;
int pk_next =PMU_PK_PRESS;
uint8_t va18_flag=0;
uint32_t pmu_irq_enable_com0 =0;
uint32_t pmu_irq_enable_com1 =0;
uint32_t pmu_irq_enable_com2 =0;
uint32_t pmu_irq_enable_com3 =0;
uint8_t pmu_init_flag=0;
//=====[Address control api]=====
volatile int pmu_i2c_init_sta = 0;
uint8_t event_con0=0,event_con1=0,event_con2=0,event_con3=0;
//D-die control api
void pmu_set_register_value_internal(uint32_t address, short int mask, short int shift, short int value) {
    uint32_t mask_buffer,target_value;
    mask_buffer = (~(mask << shift));
    target_value = *((volatile uint32_t *)(address));
    target_value &= mask_buffer;
    target_value |= (value << shift);
    *((volatile uint32_t *)(address)) = target_value;
}

uint32_t pmu_get_register_value_internal(uint32_t address, short int mask, short int shift) {
    uint32_t change_value, mask_buffer;
    mask_buffer = (mask << shift);
    change_value = *((volatile uint32_t *)(address));
    change_value &=mask_buffer;
    change_value = (change_value>> shift);
    return change_value;
}

unsigned char Wrap_D2D_I2C_Write(unsigned char *ptr_send)
{
    unsigned char retry_cnt = 0, result_read;
    *(ptr_send) = *(ptr_send) | 0x40;
    do {
        result_read = hal_i2c_master_send_polling(HAL_I2C_MASTER_AO, PMIC_SLAVE_ADDR, ptr_send, 3);
        retry_cnt++;
    }while ((result_read != 0) && (retry_cnt <= 60));
    retry_cnt--;
    return (retry_cnt);
}

unsigned char Wrap_D2D_I2C_Read(unsigned char *ptr_send, unsigned char *ptr_read, int type) {
    hal_i2c_send_to_receive_config_t config;
    unsigned char retry_cnt = 0, result_read;
    if (type == 1) {
        *(ptr_send) = *(ptr_send) | 0x40;
        config.receive_length = 1;
    } else {
        config.receive_length = 2;
    }
    config.slave_address = PMIC_SLAVE_ADDR;
    config.send_data = ptr_send;
    config.send_length = 2;
    config.receive_buffer = ptr_read;
    do {
        result_read = hal_i2c_master_send_to_receive_polling(HAL_I2C_MASTER_AO, &config);
        retry_cnt++;
    } while ((result_read != 0) && (retry_cnt <= 60));

    retry_cnt--;
    return (retry_cnt);
}

uint32_t pmu_get_register_value_mt6388(uint32_t address, uint32_t mask, uint32_t shift)
{
    unsigned char send_buffer[2], receive_buffer[1];
    pmic_i2c_init();
    send_buffer[1] = (address) & 0x00FF;
    send_buffer[0] = ((address >> 8) & 0x00FF) | 0x40;  //Addr_H[7:4] = 01xx : read 1B
    Wrap_D2D_I2C_Read(send_buffer, receive_buffer,1);
    return ((receive_buffer[0] >> shift)&mask);
}

pmu_operate_status_t pmu_set_register_value_mt6388(uint32_t address, uint32_t mask, uint32_t shift, uint32_t value)
{
    unsigned char send_buffer[3], receive_buffer[1];
    pmic_i2c_init();
    send_buffer[1] = (address) & 0x00FF;
    send_buffer[0] = ((address >> 8) & 0x00FF) | 0x40;
    Wrap_D2D_I2C_Read(send_buffer, receive_buffer,1);

    receive_buffer[0] &= (~(mask << shift));
    send_buffer[2] = receive_buffer[0] | (value << shift); //data value

    if (Wrap_D2D_I2C_Write(send_buffer) == 0) {
        return PMU_OK;
    } else {
        return PMU_ERROR;
    }
}

uint32_t pmu_get_register_value_2byte_mt6388(uint32_t address, uint32_t mask, uint32_t shift)
{
    unsigned char send_buffer[4], receive_buffer[2];
    uint32_t value;
    pmic_i2c_init();
    send_buffer[1] = address  & 0x00FF;
    send_buffer[0] = ((address  >> 8) & 0x00FF) & 0x0F;
    Wrap_D2D_I2C_Read(send_buffer, receive_buffer,2);
    value = (receive_buffer[1] << 8) + receive_buffer[0];
    return ((value >> shift) & mask);
}

pmu_operate_status_t pmu_set_register_value_2byte_mt6388(uint32_t address, uint32_t mask, uint32_t shift, uint32_t value) {
    unsigned char send_buffer[4], receive_buffer[2];
    uint32_t data;
    pmic_i2c_init();
    send_buffer[1] = address & 0x00FF;
    send_buffer[0] = ((address >> 8) & 0x00FF) & 0x0F;
    Wrap_D2D_I2C_Read(send_buffer, receive_buffer,2);

    data = receive_buffer[1];
    data = (data << 8) | receive_buffer[0];
    data &= (~(mask << shift));
    data = data | (value << shift);

    send_buffer[0] = ((address >> 8) & 0x00FF) | 0x00;
    send_buffer[1] = (address) & 0x00FF;
    send_buffer[2] = (data & 0xFF);
    send_buffer[3] = ((data >> 8) & 0xFF);

    unsigned char retry_cnt = 0, result_read;
    do {
        result_read = hal_i2c_master_send_polling(HAL_I2C_MASTER_AO, PMIC_SLAVE_ADDR, send_buffer, 4);
        retry_cnt++;
    } while ((result_read != 0) && (retry_cnt <= 60));
    return PMU_OK;
}


//=====[power domain stage control]=====
void pmu_sw_enter_sleep(pmu_power_domain_t domain) {
    switch (domain) {
        case PMU_BUCK_VCORE:
            pmu_on_mode_switch_6388(PMU_BUCK_VCORE, PMU_HW_MODE);
            pmu_lp_mode_6388(PMU_BUCK_VCORE, PMU_SW_MODE);
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_LP_ADDR, PMU_RG_BUCK_VCORE_LP_MASK, PMU_RG_BUCK_VCORE_LP_SHIFT, 1);
            break;
        case PMU_BUCK_VIO18:
            pmu_on_mode_switch_6388(PMU_BUCK_VIO18, PMU_HW_MODE);
            pmu_lp_mode_6388(PMU_BUCK_VIO18, PMU_SW_MODE);
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VIO18_LP_ADDR, PMU_RG_BUCK_VIO18_LP_MASK, PMU_RG_BUCK_VIO18_LP_SHIFT, 1);
            break;
        case PMU_BUCK_VRF:
            pmu_on_mode_switch_6388(PMU_BUCK_VRF, PMU_HW_MODE);
            pmu_lp_mode_6388(PMU_BUCK_VRF, PMU_SW_MODE);
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VRF_LP_ADDR, PMU_RG_BUCK_VRF_LP_MASK, PMU_RG_BUCK_VRF_LP_SHIFT, 1);
            pmu_set_register_value_mt6388(PMU_RG_BUCK_SLP_VRF_EN_ADDR, PMU_RG_BUCK_SLP_VRF_EN_MASK, PMU_RG_BUCK_SLP_VRF_EN_SHIFT, 0);
            break;
        case PMU_BUCK_VAUD18:
            pmu_on_mode_switch_6388(PMU_BUCK_VAUD18, PMU_SW_MODE);
            pmu_lp_mode_6388(PMU_BUCK_VAUD18, PMU_SW_MODE);
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VAUD18_LP_ADDR, PMU_RG_BUCK_VAUD18_LP_MASK, PMU_RG_BUCK_VAUD18_LP_SHIFT, 0);
            break;
        case PMU_LDO_VA18:
            pmu_set_register_value_2byte_mt6388(PMU_RG_LDO_VA18_ON_MODE_ADDR, PMU_RG_LDO_VA18_ON_MODE_MASK, PMU_RG_LDO_VA18_ON_MODE_SHIFT, 1);
            pmu_set_register_value_2byte_mt6388(PMU_RG_LDO_VA18_EN_ADDR, PMU_RG_LDO_VA18_EN_MASK, PMU_RG_LDO_VA18_EN_SHIFT, 1);
            hal_gpt_delay_ms(3);
            pmu_set_register_value_2byte_mt6388(PMU_RG_LDO_VA18_LP_MODE_ADDR, PMU_RG_LDO_VA18_LP_MODE_MASK, PMU_RG_LDO_VA18_LP_MODE_SHIFT, 0);
            pmu_set_register_value_2byte_mt6388(PMU_RG_LDO_VA18_LP_ADDR, PMU_RG_LDO_VA18_LP_MASK, PMU_RG_LDO_VA18_LP_SHIFT, 1);
            break;
        case PMU_LDO_VLDO33:
            pmu_on_mode_switch_6388(PMU_LDO_VLDO33, PMU_HW_MODE);
            pmu_lp_mode_6388(PMU_LDO_VLDO33, PMU_SW_MODE);
            pmu_set_register_value_mt6388(PMU_RG_LDO_VLDO33_LP_ADDR, PMU_RG_LDO_VLDO33_LP_MASK, PMU_RG_LDO_VLDO33_LP_SHIFT, 1);
            pmu_set_register_value_mt6388(PMU_RG_LDO_SLP_VLDO33_EN_ADDR, PMU_RG_LDO_SLP_VLDO33_EN_MASK, PMU_RG_LDO_SLP_VLDO33_EN_SHIFT, 0);
            break;
    }
}

/*
 * In sw mode , power domain neter low power mode ,need setting LP Mode ON
 */
void pmu_lp_mode_6388(pmu_power_domain_t domain, pmu_control_mode_t mode) {
    //  1'b0: No LP; 1'b1: LP
    switch(domain) {
        case PMU_BUCK_VCORE:
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_LP_MODE_ADDR, PMU_RG_BUCK_VCORE_LP_MODE_MASK, PMU_RG_BUCK_VCORE_LP_MODE_SHIFT, mode);
            break;
        case PMU_BUCK_VIO18:
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VIO18_LP_MODE_ADDR, PMU_RG_BUCK_VIO18_LP_MODE_MASK, PMU_RG_BUCK_VIO18_LP_MODE_SHIFT, mode);
            break;
        case PMU_BUCK_VRF:
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VRF_LP_MODE_ADDR, PMU_RG_BUCK_VRF_LP_MODE_MASK, PMU_RG_BUCK_VRF_LP_MODE_SHIFT, mode);
            break;
        case PMU_BUCK_VAUD18:
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VAUD18_LP_MODE_ADDR, PMU_RG_BUCK_VAUD18_LP_MODE_MASK, PMU_RG_BUCK_VAUD18_LP_MODE_SHIFT, mode);
            break;
        case PMU_LDO_VA18:
            pmu_set_register_value_mt6388(PMU_RG_LDO_VA18_LP_MODE_ADDR, PMU_RG_LDO_VA18_LP_MODE_MASK, PMU_RG_LDO_VA18_LP_MODE_SHIFT, mode);
            break;
        case PMU_LDO_VLDO33:
            pmu_set_register_value_mt6388(PMU_RG_LDO_VLDO33_LP_MODE_ADDR, PMU_RG_LDO_VLDO33_LP_MODE_MASK, PMU_RG_LDO_VLDO33_LP_MODE_SHIFT, mode);
            break;
    }
}
/*
 *  In sw mode , buck/ldo enable /disable switch
 * set 1 is power on
 * set 0 is power off
 * */
void pmu_power_enable_6388(pmu_power_domain_t pmu_pdm, pmu_power_operate_t operate)
{
    switch (pmu_pdm) {
        case PMU_BUCK_VCORE:
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_EN_ADDR, PMU_RG_BUCK_VCORE_EN_MASK, PMU_RG_BUCK_VCORE_EN_SHIFT, operate);
            break;
        case PMU_BUCK_VIO18:
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VIO18_EN_ADDR, PMU_RG_BUCK_VIO18_EN_MASK, PMU_RG_BUCK_VIO18_EN_SHIFT, operate);
            break;
        case PMU_BUCK_VRF:
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VRF_EN_ADDR, PMU_RG_BUCK_VRF_EN_MASK, PMU_RG_BUCK_VRF_EN_SHIFT, operate);
            break;
        case PMU_BUCK_VAUD18:
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VAUD18_EN_ADDR, PMU_RG_BUCK_VAUD18_EN_MASK, PMU_RG_BUCK_VAUD18_EN_SHIFT, operate);
            break;
        case PMU_LDO_VA18:
            if (va18_flag) {
                log_hal_msgid_info("Warning : VA18 in used can't be close", 0);
            } else {
                pmu_set_register_value_mt6388(PMU_RG_LDO_VA18_EN_ADDR, PMU_RG_LDO_VA18_EN_MASK, PMU_RG_LDO_VA18_EN_SHIFT, operate);
            }
            break;
        case PMU_LDO_VLDO33:
            pmu_set_register_value_mt6388(PMU_RG_LDO_VLDO33_EN_ADDR, PMU_RG_LDO_VLDO33_EN_MASK, PMU_RG_LDO_VLDO33_EN_SHIFT, operate);
            break;
    }
}

uint8_t pmu_get_power_status_6388(pmu_power_domain_t pmu_pdm)
{
    uint8_t sta=0;
    switch (pmu_pdm) {
        case PMU_BUCK_VCORE:
        sta = pmu_get_register_value_mt6388(PMU_RG_BUCK_VCORE_EN_ADDR, PMU_RG_BUCK_VCORE_EN_MASK, PMU_RG_BUCK_VCORE_EN_SHIFT);
        break;
        case PMU_BUCK_VIO18:
        sta =pmu_get_register_value_mt6388(PMU_RG_BUCK_VIO18_EN_ADDR, PMU_RG_BUCK_VIO18_EN_MASK, PMU_RG_BUCK_VIO18_EN_SHIFT);
        break;
        case PMU_BUCK_VRF:
        sta =pmu_get_register_value_mt6388(PMU_RG_BUCK_VRF_EN_ADDR, PMU_RG_BUCK_VRF_EN_MASK, PMU_RG_BUCK_VRF_EN_SHIFT);
        break;
        case PMU_BUCK_VAUD18:
        sta =pmu_get_register_value_mt6388(PMU_RG_BUCK_VAUD18_EN_ADDR, PMU_RG_BUCK_VAUD18_EN_MASK, PMU_RG_BUCK_VAUD18_EN_SHIFT);
        break;
        case PMU_LDO_VA18:
        sta = pmu_get_register_value_mt6388(PMU_RG_LDO_VA18_EN_ADDR, PMU_RG_LDO_VA18_EN_MASK, PMU_RG_LDO_VA18_EN_SHIFT);
        break;
        case PMU_LDO_VLDO33:
        sta = pmu_get_register_value_mt6388(PMU_RG_LDO_VLDO33_EN_ADDR, PMU_RG_LDO_VLDO33_EN_MASK, PMU_RG_LDO_VLDO33_EN_SHIFT);
        break;
    }
    return sta;
}
/*
 * In sw mode , buck/ldo enable /disable switch
 * The interlnal SRCLKEN HW mode
 * If enable, internal SRCLKEN will be from PAD_SRCLKEN
 * If disable, internal SRCLKEN will be from RG_SRCLKEN
 * 1'b0: SW mode
 * 1'b1: HW mode
 * */
void pmu_srclken_control_mode_6388(pmu_power_operate_t mode)
{
    if(mode==PMU_ON) {
        pmu_set_register_value_mt6388(PMU_RG_SRCLKEN_HW_MODE_ADDR, PMU_RG_SRCLKEN_HW_MODE_MASK, PMU_RG_SRCLKEN_HW_MODE_SHIFT, 1);
    } else {
        pmu_set_register_value_mt6388(PMU_RG_SRCLKEN_HW_MODE_ADDR, PMU_RG_SRCLKEN_HW_MODE_MASK, PMU_RG_SRCLKEN_HW_MODE_SHIFT, 0);
    }
}
//=====[Control mode switch]=====
/*
 * set srclken control mode 1'b0: SW mode 1'b1: HW mode
 * */
void pmu_control_srclken(pmu_control_mode_t mode) {
    pmu_set_register_value_mt6388(PMU_RG_SRCLKEN_HW_MODE_ADDR,PMU_RG_SRCLKEN_HW_MODE_MASK,PMU_RG_SRCLKEN_HW_MODE_SHIFT,mode);
}

/*
 * In normal mode , switch control mode
 * set RG_BUCK_Vxxx_ON_MODE = 0: SW mode  ; set 1: HW mode
 */
void pmu_on_mode_switch_6388(pmu_power_domain_t domain, pmu_control_mode_t mode) {
    switch(domain) {
        case PMU_BUCK_VCORE:
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_ON_MODE_ADDR, PMU_RG_BUCK_VCORE_ON_MODE_MASK, PMU_RG_BUCK_VCORE_ON_MODE_SHIFT, mode);
            break;
        case PMU_BUCK_VIO18:
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VIO18_ON_MODE_ADDR, PMU_RG_BUCK_VIO18_ON_MODE_MASK, PMU_RG_BUCK_VIO18_ON_MODE_SHIFT, mode);
            break;
        case PMU_BUCK_VRF:
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VRF_ON_MODE_ADDR, PMU_RG_BUCK_VRF_ON_MODE_MASK, PMU_RG_BUCK_VRF_ON_MODE_SHIFT, mode);
            break;
        case PMU_BUCK_VAUD18:
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VAUD18_ON_MODE_ADDR, PMU_RG_BUCK_VAUD18_ON_MODE_MASK, PMU_RG_BUCK_VAUD18_ON_MODE_SHIFT, mode);
            break;
        case PMU_LDO_VA18:
            pmu_set_register_value_mt6388(PMU_RG_LDO_VA18_ON_MODE_ADDR, PMU_RG_LDO_VA18_ON_MODE_MASK, PMU_RG_LDO_VA18_ON_MODE_SHIFT, mode);
            break;
        case PMU_LDO_VLDO33:
            pmu_set_register_value_mt6388(PMU_RG_LDO_VLDO33_ON_MODE_ADDR, PMU_RG_LDO_VLDO33_ON_MODE_MASK, PMU_RG_LDO_VLDO33_ON_MODE_SHIFT, mode);
            break;
    }
}
/*
 * 1'b0: SW mode
 * 1'b1: HW mode
 * */
void pmu_vaud18drv_control_mode_6388(pmu_control_mode_t mode)
{
    if(mode==PMU_HW_MODE) {
        pmu_set_register_value_mt6388(PMU_RG_VAUD18DRV_VOSEL_HW_MODE_ADDR, PMU_RG_VAUD18DRV_VOSEL_HW_MODE_MASK, PMU_RG_VAUD18DRV_VOSEL_HW_MODE_SHIFT, 1);
    } else {
        pmu_set_register_value_mt6388(PMU_RG_VAUD18DRV_VOSEL_HW_MODE_ADDR, PMU_RG_VAUD18DRV_VOSEL_HW_MODE_MASK, PMU_RG_VAUD18DRV_VOSEL_HW_MODE_SHIFT, 0);
    }
}

/*
 * 1'b0: sleep mode
 * 1'b1: normal mode
 */
void pmu_vaud18drv_power_control_6388(pmu_power_operate_t mode)
{
    if(mode==PMU_ON) {
        pmu_set_register_value_mt6388(PMU_RG_VAUD18DRV_VOSEL_SW_ADDR, PMU_RG_VAUD18DRV_VOSEL_SW_MASK, PMU_RG_VAUD18DRV_VOSEL_SW_SHIFT, 1);
    } else {
        pmu_set_register_value_mt6388(PMU_RG_VAUD18DRV_VOSEL_SW_ADDR, PMU_RG_VAUD18DRV_VOSEL_SW_MASK, PMU_RG_VAUD18DRV_VOSEL_SW_SHIFT, 0);
    }
}
//=====[Voltage control]======

/*
 * VAUD18 2.0V~1.05V default setting Normal:1.8V ;Sleep :0.9V
 * Select output level in LP_mode
 * 00: 0.8V
 * 01: 0.9V
 * 10: 0.85V
 * 11: 0.8V
 * */
pmu_operate_status_t pmu_vaud18_voltage_sel_6388(uint8_t vol) {
    return pmu_set_register_value_mt6388(PMU_RG_BUCK_VAUD18_VOSEL_ADDR, PMU_RG_BUCK_VAUD18_VOSEL_MASK, PMU_RG_BUCK_VAUD18_VOSEL_SHIFT, vol);
}

pmu_operate_status_t pmu_vaud18_sleep_voltage_sel_6388(pmu_power_vaud18_voltage_t vol) {
    switch(vol) {
        case PMIC_VAUD18_0P8_V:
            pmu_set_register_value_mt6388(PMU_RG_VAUD18_SLEEP_VOLTAGE_ADDR, PMU_RG_VAUD18_SLEEP_VOLTAGE_MASK, PMU_RG_VAUD18_SLEEP_VOLTAGE_SHIFT, 0);
            break;
        case PMIC_VAUD18_0P9_V:
            pmu_set_register_value_mt6388(PMU_RG_VAUD18_SLEEP_VOLTAGE_ADDR, PMU_RG_VAUD18_SLEEP_VOLTAGE_MASK, PMU_RG_VAUD18_SLEEP_VOLTAGE_SHIFT, 1);
            break;
        case PMIC_VAUD18_0P85_V:
            pmu_set_register_value_mt6388(PMU_RG_VAUD18_SLEEP_VOLTAGE_ADDR, PMU_RG_VAUD18_SLEEP_VOLTAGE_MASK, PMU_RG_VAUD18_SLEEP_VOLTAGE_SHIFT, 2);
            break;
    }
    return PMU_OK;
}

void pmu_voltage_selet_6388(pmu_power_stage_t mode,pmu_power_domain_t domain,uint32_t vol) {
    switch (domain) {
        case PMU_BUCK_VIO18: //VIO18 1.12V~1.96V default setting Normal:1.8V ;Sleep :1.8V
            if (mode == PMU_NORMAL) {
                pmu_set_register_value_mt6388(PMU_RG_BUCK_VIO18_VOSEL_ADDR, PMU_RG_BUCK_VIO18_VOSEL_MASK, PMU_RG_BUCK_VIO18_VOSEL_SHIFT, vol);
            }
            break;
        case PMU_BUCK_VRF: //VRF 1.12V~1.95V default setting Normal:1.45 ;Sleep :1.4V
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VRF_VOSEL_ADDR, PMU_RG_BUCK_VRF_VOSEL_MASK, PMU_RG_BUCK_VRF_VOSEL_SHIFT, vol);
            pmu_set_register_value_mt6388(PMU_RG_BUCK_VRF_VOSEL_SLEEP_ADDR, PMU_RG_BUCK_VRF_VOSEL_SLEEP_MASK, PMU_RG_BUCK_VRF_VOSEL_SLEEP_SHIFT, vol);
            break;
        case PMU_LDO_VA18:
            break;
        case PMU_LDO_VLDO33:
            break;
    }

}

void pmu_normal_mode_low_power_setting(pmu_power_vcore_voltage_t vol) {
    if(vol == PMIC_VCORE_0P9_V) {
        //Setting for low power : VCORE
        pmu_lp_mode_6388(PMU_BUCK_VCORE, PMU_SW_MODE);
        pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_LP_ADDR, PMU_RG_BUCK_VCORE_LP_MASK, PMU_RG_BUCK_VCORE_LP_SHIFT, 1);
        //Setting for low power : VIO18
        pmu_lp_mode_6388(PMU_BUCK_VIO18, PMU_SW_MODE);
        pmu_set_register_value_mt6388(PMU_RG_BUCK_VIO18_LP_ADDR, PMU_RG_BUCK_VIO18_LP_MASK, PMU_RG_BUCK_VIO18_LP_SHIFT, 1);
    } else { //1.1V & 1.3V
        //Setting for low power : VCORE
        if(pmu_audio_is_running == PMU_OFF){
            pmu_lp_mode_6388(PMU_BUCK_VCORE, PMU_HW_MODE);
        }
        pmu_set_register_value_mt6388(PMU_RG_BUCK_ACT_VCORE_LP_ADDR, PMU_RG_BUCK_ACT_VCORE_LP_MASK, PMU_RG_BUCK_ACT_VCORE_LP_SHIFT,0);
        pmu_set_register_value_mt6388(PMU_RG_BUCK_SLP_VCORE_LP_ADDR, PMU_RG_BUCK_SLP_VCORE_LP_MASK, PMU_RG_BUCK_SLP_VCORE_LP_SHIFT,1);
        //Setting for low power : VIO18
        pmu_lp_mode_6388(PMU_BUCK_VIO18, PMU_HW_MODE);
        pmu_set_register_value_mt6388(PMU_RG_BUCK_ACT_VIO18_LP_ADDR, PMU_RG_BUCK_ACT_VIO18_LP_MASK, PMU_RG_BUCK_ACT_VIO18_LP_SHIFT,0);
        pmu_set_register_value_mt6388(PMU_RG_BUCK_SLP_VIO18_LP_ADDR, PMU_RG_BUCK_SLP_VIO18_LP_MASK, PMU_RG_BUCK_SLP_VIO18_LP_SHIFT,1);
    }
}

void pmu_audio_low_power_setting(int oper){
    if(oper == PMU_OFF){
        pmu_power_enable_6388(PMU_LDO_VA18, PMU_OFF);
        pmu_power_enable_6388(PMU_BUCK_VAUD18, PMU_OFF);
        pmu_lp_mode_6388(PMU_BUCK_VCORE, PMU_HW_MODE);
        pmu_on_mode_switch_6388(PMU_BUCK_VCORE,PMU_HW_MODE);
        /* PMU_RG_BUCK_SLP_VCORE_EN = 1 */
        pmu_set_register_value_mt6388(0x030A, 0x01, 0x1, 1);
        pmu_audio_is_running = PMU_OFF;
    }else{
        pmu_power_enable_6388(PMU_LDO_VA18, PMU_ON);
        pmu_power_enable_6388(PMU_BUCK_VAUD18, PMU_ON);
        pmu_lp_mode_6388(PMU_BUCK_VCORE, PMU_SW_MODE);
        pmu_on_mode_switch_6388(PMU_BUCK_VCORE,PMU_SW_MODE);
        /* PMU_RG_BUCK_SLP_VCORE_EN = 0 */
        pmu_set_register_value_mt6388(0x030A, 0x01, 0x1, 0);
        pmu_audio_is_running = PMU_ON;
    }
}
void pmu_vcore_pfm_ipeak(pmu_power_vcore_voltage_t vol) {
    if(vol == PMIC_VCORE_1P3_V) {
#if defined(AB1558)
        pmu_set_register_value_2byte_mt6388(PMU_RG_BUCK_VCORE_NM_PFM_IPEAK_ADDR,PMU_RG_BUCK_VCORE_NM_PFM_IPEAK_MASK,PMU_RG_BUCK_VCORE_NM_PFM_IPEAK_SHIFT,0x1C);
#else
        pmu_set_register_value_2byte_mt6388(PMU_RG_BUCK_VCORE_NM_PFM_IPEAK_ADDR,PMU_RG_BUCK_VCORE_NM_PFM_IPEAK_MASK,PMU_RG_BUCK_VCORE_NM_PFM_IPEAK_SHIFT,0x14);
#endif
    } else {
        pmu_set_register_value_2byte_mt6388(PMU_RG_BUCK_VCORE_NM_PFM_IPEAK_ADDR,PMU_RG_BUCK_VCORE_NM_PFM_IPEAK_MASK,PMU_RG_BUCK_VCORE_NM_PFM_IPEAK_SHIFT,0xc);
    }
}

/*
 * For sleep mode voltage, there are two register need to set before enter sleep sequence
 * RG_<NAME>_SLEEP_VOLTAGE : Analog V2V voltage
 * RG_<NAME>_VOSEL_SLEEP : R2R DAC voltage
 *
 * RG_VCORE_SLEEP_VOLTAGE  sleep voltage setting :
 * 00: 0.7V
 * 01: 0.9V
 * 10: 1V
 * 11: 0.7V
 */

pmu_operate_status_t pmu_vcore_voltage_sel_6388(pmu_power_stage_t mode ,pmu_power_vcore_voltage_t vol) {
    if ((mode > PMU_DVS) | (mode < PMU_SLEEP) | (vol > PMIC_VCORE_1P4_V) | (vol < PMIC_VCORE_0P7_V)) {
        log_hal_msgid_error("[PMU] vcore_voltage Error input", 0);
    }
    if(mode==PMU_SLEEP) {
        switch (vol) {
            case PMIC_VCORE_0P9_V:
                pmu_set_register_value_mt6388(PMU_RG_VCORE_SLEEP_VOLTAGE_ADDR, PMU_RG_VCORE_SLEEP_VOLTAGE_MASK, PMU_RG_VCORE_SLEEP_VOLTAGE_SHIFT, 0x01);
                pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_SLEEP_ADDR, PMU_RG_BUCK_VCORE_VOSEL_SLEEP_MASK, PMU_RG_BUCK_VCORE_VOSEL_SLEEP_SHIFT, 0x20);
                break;
            case PMIC_VCORE_0P7_V:
                pmu_set_register_value_mt6388(PMU_RG_VCORE_SLEEP_VOLTAGE_ADDR, PMU_RG_VCORE_SLEEP_VOLTAGE_MASK, PMU_RG_VCORE_SLEEP_VOLTAGE_SHIFT, 0x00);
                pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_SLEEP_ADDR, PMU_RG_BUCK_VCORE_VOSEL_SLEEP_MASK, PMU_RG_BUCK_VCORE_VOSEL_SLEEP_SHIFT, 0x0);
                break;
            default :
                pmu_set_register_value_mt6388(PMU_RG_VCORE_SLEEP_VOLTAGE_ADDR, PMU_RG_VCORE_SLEEP_VOLTAGE_MASK, PMU_RG_VCORE_SLEEP_VOLTAGE_SHIFT, 0x10);
                pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_SLEEP_ADDR, PMU_RG_BUCK_VCORE_VOSEL_SLEEP_MASK, PMU_RG_BUCK_VCORE_VOSEL_SLEEP_SHIFT, 0x0);
        }
    } else {
        switch (vol) {
            case PMIC_VCORE_1P4_V:
                pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x70);
                break;
            case PMIC_VCORE_1P3_V:
                pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x60);
                pmu_normal_mode_low_power_setting(PMIC_VCORE_1P3_V);
                pmu_vcore_pfm_ipeak(PMIC_VCORE_1P3_V);
                break;
            case PMIC_VCORE_1P2_V:
                pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x50);
                pmu_vcore_pfm_ipeak(PMIC_VCORE_1P2_V);
                break;
            case PMIC_VCORE_1P1_V:
                pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x40);
                pmu_vcore_pfm_ipeak(PMIC_VCORE_1P1_V);
                pmu_normal_mode_low_power_setting(PMIC_VCORE_1P1_V);
                break;
            case PMIC_VCORE_1P0_V:
                pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x30);
                pmu_vcore_pfm_ipeak(PMIC_VCORE_1P1_V);
                pmu_normal_mode_low_power_setting(PMIC_VCORE_1P1_V);
                break;
            case PMIC_VCORE_0P97_V:
                pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x2b);
                pmu_vcore_pfm_ipeak(PMIC_VCORE_1P1_V);
                pmu_normal_mode_low_power_setting(PMIC_VCORE_1P1_V);
                break;
            case PMIC_VCORE_0P9_V:
                pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x24); //0.922V
                pmu_vcore_pfm_ipeak(PMIC_VCORE_0P9_V);
                pmu_lp_mode_6388(PMU_BUCK_VIO18, PMU_SW_MODE);//Setting for low power : VIO18
                pmu_set_register_value_mt6388(PMU_RG_BUCK_VIO18_LP_ADDR, PMU_RG_BUCK_VIO18_LP_MASK, PMU_RG_BUCK_VIO18_LP_SHIFT, 1);
                break;
            case PMIC_VCORE_0P7_V:
                pmu_vcore_pfm_ipeak(PMIC_VCORE_0P7_V);
                pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, 0x0);
                break;
        }
    }
    return PMU_OK;

}
pmu_power_vcore_voltage_t pmu_get_vcore_setting_index(uint16_t vcore) {
    uint8_t vcbuck_voval[8] = { 0x0, 0x24,0x2b, 0x30, 0x40, 0x50, 0x60, 0x70 };
    int vosel = 0;
    for (vosel = 0; vosel < 8; vosel++) {
        if (vcore == vcbuck_voval[vosel]) {
            return ((pmu_power_vcore_voltage_t)vosel);
        }
    }
    return (PMU_ERROR);
}

//only use HP,LP Mode
pmu_power_vcore_voltage_t pmu_get_vcore_voltage_mt6388(void)
{
    uint32_t temp;
    temp = pmu_get_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT);
    return pmu_get_vcore_setting_index(temp);
}

//=====[Control API]=====
void pmu_power_hold_sequence() {
    pmu_set_register_value_mt6388(PMU_RG_PWRHOLD_ADDR,PMU_RG_PWRHOLD_MASK,PMU_RG_PWRHOLD_SHIFT,1); //Power hold
    pmu_set_register_value_mt6388(PMU_RG_PWROFF_MODE_ADDR,PMU_RG_PWROFF_MODE_MASK,PMU_RG_PWROFF_MODE_SHIFT,0);//PWER set to normal
}

void pmu_power_off_sequence(pmu_power_stage_t stage) {
    switch(stage) {
        case PMU_PWROFF:
            log_hal_msgid_info("PMU power off ", 0);
            hal_gpt_delay_ms(1);
            pmu_set_register_value_mt6388(PMU_RG_PWROFF_MODE_ADDR,PMU_RG_PWROFF_MODE_MASK,PMU_RG_PWROFF_MODE_SHIFT,1);//Power OFF
            break;
        case PMU_RTC:
            log_hal_msgid_info("PMU Enter RTC mode ", 0);
            hal_gpt_delay_ms(1);
            pmu_set_register_value_2byte_mt6388(PMU_RG_POFFSTS_CLR_ADDR, PMU_RG_POFFSTS_CLR_MASK, PMU_RG_POFFSTS_CLR_SHIFT, 0x4400);
            pmu_set_register_value_2byte_mt6388(PMU_RG_POFFSTS_EN_ADDR, PMU_RG_POFFSTS_EN_MASK, PMU_RG_POFFSTS_EN_SHIFT, 0x3fff);
            pmu_set_register_value_mt6388(PMU_RG_PWROFF_MODE_ADDR,PMU_RG_PWROFF_MODE_MASK,PMU_RG_PWROFF_MODE_SHIFT,0);//Power OFF
            pmu_set_register_value_mt6388(PMU_RG_PWRHOLD_ADDR,PMU_RG_PWRHOLD_MASK,PMU_RG_PWRHOLD_SHIFT,0);//Power hold release
        break;
        case PMU_NORMAL:
            break;
        case PMU_SLEEP:
            break;
    }
}

uint8_t pmu_get_usb_input_status(void) // For usb driver get usb put in status
{
    return pmu_get_register_value_2byte_mt6388(PMU_DA_QI_CHR_REF_EN_ADDR, PMU_DA_QI_CHR_REF_EN_MASK, PMU_DA_QI_CHR_REF_EN_SHIFT);
}

uint8_t pmu_get_lock_status() {
    return pmu_lock_status;
}

int pmu_get_lock_index(){
    //Find Highest Vcore Voltage
    int vol_index;
    for (vol_index = PMIC_VCORE_1P4_V; vol_index >= PMIC_VCORE_0P7_V; vol_index--) {
        if (Vcore_Resource_Ctrl[vol_index] != 0) {
            break;
        }
    }
    return vol_index;
}

pmu_power_vcore_voltage_t pmu_vcore_lock_control(pmu_power_stage_t mode,pmu_power_vcore_voltage_t vol,pmu_lock_parameter_t lock) {
    log_hal_msgid_info("PMU lock vol = %d, lock = %d[0:lock 1:unlock] \r\n",2, vol, lock);
    int i = 0;
    int temp = 0;
    int vol_index = 0;
    uint32_t mask_pri;
    if(vol>=PMIC_VCORE_FAIL_V) {
        return PMIC_VCORE_FAIL_V;
    }
    if(mode == PMU_SLEEP) {
    } else {
        hal_nvic_save_and_set_interrupt_mask_special(&mask_pri);
        if (lock == PMU_LOCK) {
            Vcore_Resource_Ctrl[vol]++;
        } else {
            if (Vcore_Resource_Ctrl[vol] != 0) {
                Vcore_Resource_Ctrl[vol]--;
            }
            if ((vol == old_index) && Vcore_Resource_Ctrl[vol] == 0) {
                old_index = 0;
            }
        }
        /*Find Highest Vcore Voltage*/
        for (vol_index = PMIC_VCORE_1P4_V; vol_index > PMIC_VCORE_0P7_V; vol_index--) {
            if (Vcore_Resource_Ctrl[vol_index] != 0) {
                break;
            }
        }
        for (i = PMIC_VCORE_0P7_V; i <= PMIC_VCORE_1P4_V; i++) {
            temp += Vcore_Resource_Ctrl[i];
        }
        if(pmu_lock_status > 15){
            log_hal_msgid_info("ERROR!!!! PMU Lock /unlock isn't match more ten times\r\n",0);
        }
        pmu_lock_status = temp;
        /*if not module lock ,return default setting*/
        if (temp == 0) {
            hal_nvic_restore_interrupt_mask_special(mask_pri);
            pmu_vcore_voltage_sel_6388(PMU_NORMAL, pmu_basic_index);
        } else {
            hal_nvic_restore_interrupt_mask_special(mask_pri);
            if (old_index < vol_index) {
                pmu_vcore_voltage_sel_6388(PMU_NORMAL, vol_index);
                old_index = vol_index;
            }
        }
    }
    log_hal_msgid_info("PMU vcore lock: %d lock state : %d\r\n",2,pmu_get_vcore_voltage_mt6388(),temp);
    return vol_index;
}
//=======[Other]==========
void pmu_latch_power_key_for_bootloader() {
    pmu_set_register_value_mt6388(PMU_I2C_CONFIG_ADDR, PMU_I2C_CONFIG_MASK, PMU_I2C_CONFIG_SHIFT, 1); //D2D need to setting in PP mode, first priority, AB1555 no need, AB1558 D2D nessary
    pmu_set_register_value_mt6388(PMU_RG_I2C_DAT_IC_ADDR, PMU_RG_I2C_DAT_IC_MASK, PMU_RG_I2C_DAT_IC_SHIFT, 0);//I2C_DAT pad input control
    pmu_set_register_value_mt6388(PMU_RG_I2C_CLK_IC_ADDR, PMU_RG_I2C_CLK_IC_MASK, PMU_RG_I2C_CLK_IC_SHIFT, 0);//I2C_CLK pad input control
    pmu_set_register_value_mt6388(PMU_RG_PWRHOLD_ADDR, PMU_RG_PWRHOLD_MASK, PMU_RG_PWRHOLD_SHIFT, 1);//Power hold
}
int first_boot_up =0;
pmu_operate_status_t pmu_pk_filter(uint8_t pk_sta) {
    if (pk_sta == 0) { //Press section : pk_next should be Press
        if (pk_next == PMU_PK_RELEASE && pmu_function_table[0].init_status != 0) {
            log_hal_msgid_info("PMIC INT[0]  [1]",0);
            pmu_function_table[0].pmu_callback();
        }
        pk_next = PMU_PK_RELEASE;
        return PMU_OK;
    } else if (pk_sta == 1) { //Release section : pk_next should be Press
        if ((first_boot_up==1) && pk_next == PMU_PK_PRESS && pmu_function_table[0].init_status != 0) {
            log_hal_msgid_info("PMIC INT[0]  [0]",0);
            pmu_function_table[0].pmu_callback();
        }
        first_boot_up=1; /*For first time boot up power key single*/
        pk_next = PMU_PK_PRESS;
        return PMU_OK;
    }else{
        log_hal_msgid_info("PMIC POWERKEY HW error [%d]", 1,pk_sta);
        return PMU_ERROR;
    }
    return PMU_OK;
}

void pmu_eint_handler(void *parameter)
{
    uint8_t pk_sta;
    int index=0,index_shift=0;
    uint32_t unmask_index;
    hal_eint_mask(HAL_EINT_PMU);
    unmask_index = pmu_register_interrupt;
    pmu_get_all_int_status();
    pk_sta = pmu_get_register_value_2byte_mt6388(PMU_PWRKEY_VAL_ADDR,PMU_PWRKEY_VAL_MASK,PMU_PWRKEY_VAL_SHIFT);
       for (index = 0; index < PMU_INT_MAX; index++) {
            if ((unmask_index >> (index - index_shift)) & (pmu_get_status_interrupt(index))) {
                if (index == 0) {
                    pmu_pk_filter(pk_sta);
                }
                log_hal_msgid_info("PMIC INT[%d] [%d]", 2,index,pk_sta);
                if(pmu_function_table[index].init_status!=0){
                    pmu_function_table[index].pmu_callback();
                }
            pmu_irq_count(index);
            }
            if (index == 31) {
                unmask_index = pmu_register_interrupt_2;
                index_shift = 32;
            }
        }
    pmu_control_clear_interrupt(PMU_INT_MAX);
    hal_eint_unmask(HAL_EINT_PMU);
}

void pmu_eint_init()
{
    hal_eint_config_t config;
    config.trigger_mode = HAL_EINT_EDGE_FALLING_AND_RISING;
    config.debounce_time = 0;

    hal_eint_init(HAL_EINT_PMU, &config);    /*set EINT trigger mode and debounce time.*/
    hal_eint_register_callback(HAL_EINT_PMU, pmu_eint_handler, NULL);  /*register a user callback.*/

    hal_eint_unmask(HAL_EINT_PMU);
}

uint8_t pmu_get_power_on_reason() {
    uint8_t reason;
    reason = pmu_get_register_value_2byte_mt6388(PMU_PONSTS, 0x1F, 0);
    return reason;
}

uint8_t pmu_get_power_off_reason() {
    uint8_t reason;
    reason = pmu_get_register_value_mt6388(PMU_RGS_POFFSTS_ADDR, PMU_RGS_POFFSTS_MASK, PMU_RGS_POFFSTS_SHIFT);
    return reason;
}


void pmic_i2c_init() {
	uint32_t mask_pri;
    hal_nvic_save_and_set_interrupt_mask(&mask_pri);
    if(pmu_i2c_init_sta==1){
		hal_nvic_restore_interrupt_mask(mask_pri);
        return;
    }
    int status;
    hal_i2c_config_t config;
#ifdef AB1555
    config.frequency =HAL_I2C_FREQUENCY_400K;
#else
    config.frequency = HAL_I2C_FREQUENCY_3M;
#endif
    status = hal_i2c_master_init(HAL_I2C_MASTER_AO, &config);
    if (status != HAL_I2C_STATUS_OK) {
        assert(0);
    }
    hal_i2c_master_set_io_config(HAL_I2C_MASTER_AO, HAL_I2C_IO_PUSH_PULL);
    pmu_i2c_init_sta=1;
    hal_nvic_restore_interrupt_mask(mask_pri);
}

void pmic_i2c_deinit() {
    uint32_t mask_pri;
    hal_nvic_save_and_set_interrupt_mask(&mask_pri);
    hal_i2c_master_deinit(HAL_I2C_MASTER_AO);
    pmu_i2c_init_sta=0;
    hal_nvic_restore_interrupt_mask(mask_pri);
}

void PMU_control_all_lp_mode(pmu_control_mode_t mode) {
    if(mode ==PMU_SW_MODE) {
        pmu_lp_mode_6388(PMU_BUCK_VCORE, PMU_SW_MODE);
        pmu_lp_mode_6388(PMU_BUCK_VIO18, PMU_SW_MODE);
        pmu_lp_mode_6388(PMU_BUCK_VRF, PMU_SW_MODE);
        pmu_lp_mode_6388(PMU_BUCK_VAUD18, PMU_SW_MODE);
        pmu_lp_mode_6388(PMU_LDO_VA18, PMU_SW_MODE);
        pmu_lp_mode_6388(PMU_LDO_VLDO33, PMU_SW_MODE);
    } else {
        pmu_lp_mode_6388(PMU_BUCK_VCORE, PMU_HW_MODE);
        pmu_lp_mode_6388(PMU_BUCK_VIO18, PMU_HW_MODE);
        pmu_lp_mode_6388(PMU_BUCK_VRF, PMU_HW_MODE);
        pmu_lp_mode_6388(PMU_BUCK_VAUD18, PMU_HW_MODE);
        pmu_lp_mode_6388(PMU_LDO_VA18, PMU_HW_MODE);
        pmu_lp_mode_6388(PMU_LDO_VLDO33, PMU_HW_MODE);
    }
}

/*when boot up,press power key need more than the specific time*/
void pmu_press_pk_time() {
    uint8_t pk_sta;
    uint32_t pmu_gpt_start, pmu_get_press_time, pmu_get_duration_time = 0;
    hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &pmu_gpt_start);
    while (1) {
        pk_sta = pmu_get_register_value_2byte_mt6388(PMU_PWRKEY_VAL_ADDR, PMU_PWRKEY_VAL_MASK, PMU_PWRKEY_VAL_SHIFT);
        if (pk_sta == 0) {
            hal_gpt_get_free_run_count(HAL_GPT_CLOCK_SOURCE_1M, &pmu_get_press_time);
            hal_gpt_get_duration_count(pmu_gpt_start, pmu_get_press_time, &pmu_get_duration_time);
            pmu_set_register_value_2byte_mt6388(PMU_RG_PWRHOLD_ADDR, PMU_RG_PWRHOLD_MASK, PMU_RG_PWRHOLD_SHIFT, 1);   //Power hold
        } else {
            pmu_get_press_time = 0;
            log_hal_msgid_error("ON[%x]OFF[%x]DT[%d]PT[%d]",4,pmu_get_power_on_reason(),pmu_get_power_off_reason(),pmu_get_duration_time,PMU_PRESS_PK_TIME);
            log_hal_msgid_error("Boot up fail , press pk need more than the specific time %d or PMIC OP", 1,PMU_PRESS_PK_TIME);
            hal_gpt_delay_ms(1);
            pmu_power_off_sequence(1);
        }
        if (pmu_get_duration_time > PMU_PRESS_PK_TIME) {
            break;
        }
    }
}

void pmu_irq_init(void) {
    if (pmu_init_flag == 0) {
        pmu_set_register_value_2byte_mt6388(PMU_INT_CON0, 0xffff, 0, 0);
        pmu_set_register_value_2byte_mt6388(PMU_INT_CON1, 0xffff, 0, 0);
        pmu_set_register_value_2byte_mt6388(PMU_INT_CON2, 0xffff, 0, 0);
        pmu_set_register_value_2byte_mt6388(PMU_INT_CON3, 0xffff, 0, 0);
        pmu_irq_enable_com0 = 0;
        pmu_irq_enable_com1 = 0;
        pmu_irq_enable_com2 = 0;
        pmu_irq_enable_com3 = 0;
    }
}
void hal_pmu_sleep_backup(void)
{
    pmic_i2c_deinit();
}
void pmu_init_6388() {
    pmu_set_register_value_2byte_mt6388(PMU_I2C_CONFIG_ADDR,PMU_I2C_CONFIG_MASK,PMU_I2C_CONFIG_SHIFT,1);
    pmu_set_register_value_2byte_mt6388(PMU_RG_I2C_DAT_IC_ADDR,PMU_RG_I2C_DAT_IC_MASK,PMU_RG_I2C_DAT_IC_SHIFT,0x4);//I2C_DAT pad input control
    pmu_set_register_value_2byte_mt6388(PMU_RG_I2C_CLK_IC_ADDR,PMU_RG_I2C_CLK_IC_MASK,PMU_RG_I2C_CLK_IC_SHIFT,0x4);//I2C_CLK pad input control
    /* Avoid system boot that by user short touch pk or evb layout issue */
    if (pmu_get_power_on_reason() == 0x1 && pmu_get_power_off_reason() !=0xd && pmu_get_power_off_reason() !=0x8 ) {
        pmu_press_pk_time();
    }else{
        pmu_set_register_value_2byte_mt6388(PMU_RG_PWRHOLD_ADDR, PMU_RG_PWRHOLD_MASK, PMU_RG_PWRHOLD_SHIFT, 1);   //Power hold
    }
    pmu_set_register_value_2byte_mt6388(PMU_RG_CLK_TRIM_F32K_CK_PDN_ADDR,PMU_RG_CLK_TRIM_F32K_CK_PDN_MASK,PMU_RG_CLK_TRIM_F32K_CK_PDN_SHIFT,1);//clk_trim_f32k_ck power down
    pmu_set_register_value_2byte_mt6388(PMU_RG_MON_GRP_SEL_ADDR,PMU_RG_MON_GRP_SEL_MASK,PMU_RG_MON_GRP_SEL_SHIFT,0x1f);//monitor flag group select ?
    pmu_set_register_value_2byte_mt6388(PMU_RG_WDTRSTB_EN_ADDR, PMU_RG_WDTRSTB_EN_MASK, PMU_RG_WDTRSTB_EN_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_RG_WDTRSTB_DEB_ADDR, PMU_RG_WDTRSTB_DEB_MASK, PMU_RG_WDTRSTB_DEB_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_RG_STRUP_ENVTEM_CTRL_ADDR, PMU_RG_STRUP_ENVTEM_CTRL_MASK, PMU_RG_STRUP_ENVTEM_CTRL_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_RG_STRUP_LONG_PRESS_EXT_CHR_CTRL_ADDR, PMU_RG_STRUP_LONG_PRESS_EXT_CHR_CTRL_MASK, PMU_RG_STRUP_LONG_PRESS_EXT_CHR_CTRL_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_RG_STRUP_LONG_PRESS_EXT_PWRKEY_CTRL_ADDR, PMU_RG_STRUP_LONG_PRESS_EXT_PWRKEY_CTRL_MASK, PMU_RG_STRUP_LONG_PRESS_EXT_PWRKEY_CTRL_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_RG_STRUP_LONG_PRESS_EXT_RTCA_CTRL_ADDR, PMU_RG_STRUP_LONG_PRESS_EXT_RTCA_CTRL_MASK, PMU_RG_STRUP_LONG_PRESS_EXT_RTCA_CTRL_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_EN_CHGSTATINT_ADDR, PMU_RG_INT_EN_CHGSTATINT_MASK, PMU_RG_INT_EN_CHGSTATINT_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_MASK_PWRKEY_ADDR, PMU_RG_INT_MASK_PWRKEY_MASK, PMU_RG_INT_MASK_PWRKEY_SHIFT, 0);//Mask PWRKEY : 1'b0: un-mask interrupt status
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_MASK_PWRKEY_R_ADDR, PMU_RG_INT_MASK_PWRKEY_R_MASK, PMU_RG_INT_MASK_PWRKEY_R_SHIFT, 0);//Mask PWRKEY_R : 1'b0: un-mask interrupt status
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_MASK_AD_LBAT_LV_ADDR, PMU_RG_INT_MASK_AD_LBAT_LV_MASK, PMU_RG_INT_MASK_AD_LBAT_LV_SHIFT, 0);//Mask AD_LBAT_LV: 1'b0: un-mask interrupt status
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_MASK_CHRDET_ADDR, PMU_RG_INT_MASK_CHRDET_MASK, PMU_RG_INT_MASK_CHRDET_SHIFT, 0);//Mask CHRDET 1'b0: un-mask interrupt status
    pmu_set_register_value_2byte_mt6388(PMU_RG_STRUP_AUXADC_RSTB_SEL_ADDR,PMU_RG_STRUP_AUXADC_RSTB_SEL_MASK,PMU_RG_STRUP_AUXADC_RSTB_SEL_SHIFT,1);//Peter-SW, for sleep no reset;for wakeup ZCV and charger suspend option3
    pmu_set_register_value_2byte_mt6388(PMU_RG_STRUP_AUXADC_RPCNT_MAX_ADDR,PMU_RG_STRUP_AUXADC_RPCNT_MAX_MASK,PMU_RG_STRUP_AUXADC_RPCNT_MAX_SHIFT,0x42);// for wakeup ZCV and charger suspend option3
    pmu_set_register_value_2byte_mt6388(PMU_RG_LDO_VA18_ON_MODE_ADDR, PMU_RG_LDO_VA18_ON_MODE_MASK, PMU_RG_LDO_VA18_ON_MODE_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_RG_BUCK_VCORE_SFCHG_RRATE_ADDR,PMU_RG_BUCK_VCORE_SFCHG_RRATE_MASK,PMU_RG_BUCK_VCORE_SFCHG_RRATE_SHIFT,0x2);//Rising soft change rate Ref clock = 2MHz (0.5us) Step = ( code +1 ) * 0.5 us
    pmu_set_register_value_2byte_mt6388(PMU_RG_BUCK_VAUD18_SFCHG_RRATE_ADDR,PMU_RG_BUCK_VAUD18_SFCHG_RRATE_MASK,PMU_RG_BUCK_VAUD18_SFCHG_RRATE_SHIFT,1);// Rising soft change rate Ref clock = 2MHz (0.5us) Step = ( code +1 ) * 0.5 us
    pmu_set_register_value_2byte_mt6388(PMU_AUXADC_TRIM_CH2_SEL_ADDR,PMU_AUXADC_TRIM_CH2_SEL_MASK,PMU_AUXADC_TRIM_CH2_SEL_SHIFT,1);//ADC trimming source  set ch7
    pmu_set_register_value_2byte_mt6388(PMU_AUXADC_TRIM_CH4_SEL_ADDR,PMU_AUXADC_TRIM_CH4_SEL_MASK,PMU_AUXADC_TRIM_CH4_SEL_SHIFT,1);//ADC trimming source  set ch4
    pmu_set_register_value_2byte_mt6388(PMU_AUXADC_START_SHADE_EN_ADDR,PMU_AUXADC_START_SHADE_EN_MASK,PMU_AUXADC_START_SHADE_EN_SHIFT,1);//AUXADC start shading enable
    pmu_set_register_value_2byte_mt6388(PMU_RG_SRCLKEN_HW_MODE_ADDR, PMU_RG_SRCLKEN_HW_MODE_MASK, PMU_RG_SRCLKEN_HW_MODE_SHIFT, 1);//HW mode
    pmu_set_register_value_2byte_mt6388(PMU_RG_VAUD18DRV_VOSEL_HW_MODE_ADDR, PMU_RG_VAUD18DRV_VOSEL_HW_MODE_MASK, PMU_RG_VAUD18DRV_VOSEL_HW_MODE_SHIFT, 1);
    if(pmu_get_register_value_2byte_mt6388(0,0xffff,0)==0x8810) {
        //Workaround for pmic enter sleep vcore drop , E2 will fix
        pmu_set_register_value_2byte_mt6388(PMU_TST_W_KEY_ADDR, PMU_TST_W_KEY_MASK, PMU_TST_W_KEY_SHIFT, 0x4936);
        pmu_set_register_value_2byte_mt6388(PMU_RG_BATDET_ON_SW_MODE_ADDR, PMU_RG_BATDET_ON_SW_MODE_MASK, PMU_RG_BATDET_ON_SW_MODE_SHIFT, 1);
        pmu_set_register_value_2byte_mt6388(PMU_RG_BATDET_ON_ADDR, PMU_RG_BATDET_ON_MASK, PMU_RG_BATDET_ON_SHIFT, 1);
    }
    pmu_set_register_value_2byte_mt6388(PMU_AUXADC_CK_AON_ADDR,PMU_AUXADC_CK_AON_MASK,PMU_AUXADC_CK_AON_SHIFT,0);    //AUXADC start shading enable
    pmu_set_register_value_2byte_mt6388(PMU_RG_BGR_RSEL_ADDR, PMU_RG_BGR_RSEL_MASK, PMU_RG_BGR_RSEL_SHIFT, 0x2);//performance
    pmu_set_register_value_2byte_mt6388(PMU_RG_BGR_TRIM_EN_ADDR, PMU_RG_BGR_TRIM_EN_MASK, PMU_RG_BGR_TRIM_EN_SHIFT, 0x1);//performance
    pmu_set_register_value_2byte_mt6388(PMU_RG_AO_TRIM_REG_RELOAD_ADDR, PMU_RG_AO_TRIM_REG_RELOAD_MASK, PMU_RG_AO_TRIM_REG_RELOAD_SHIFT, 0x1);//performance
    pmu_set_register_value_2byte_mt6388(PMU_RG_BUCK_VCORE_NM_PFM_IPEAK_ADDR, PMU_RG_BUCK_VCORE_NM_PFM_IPEAK_MASK, PMU_RG_BUCK_VCORE_NM_PFM_IPEAK_SHIFT, 0xC);//performance
    pmu_set_register_value_2byte_mt6388(PMU_RG_BUCK_VIO18_LP_PFM_IPEAK_ADDR, PMU_RG_BUCK_VIO18_LP_PFM_IPEAK_MASK, PMU_RG_BUCK_VIO18_LP_PFM_IPEAK_SHIFT, 0x9);//performance
    pmu_set_register_value_2byte_mt6388(PMU_RG_BUCK_VIO18_NM_PFM_IPEAK_ADDR, PMU_RG_BUCK_VIO18_NM_PFM_IPEAK_MASK, PMU_RG_BUCK_VIO18_NM_PFM_IPEAK_SHIFT, 0x9);//performance
    pmu_set_register_value_2byte_mt6388(PMU_RG_LOOP_CHRLDO_SB_DIS_ADDR, PMU_RG_LOOP_CHRLDO_SB_DIS_MASK, PMU_RG_LOOP_CHRLDO_SB_DIS_SHIFT, 0x80);//performance
    pmu_set_register_value_2byte_mt6388(PMU_AUXADC_SPL_NUM_LARGE_ADDR, PMU_AUXADC_SPL_NUM_LARGE_MASK, PMU_AUXADC_SPL_NUM_LARGE_SHIFT, 0x1f);//performance
    pmu_set_register_value_2byte_mt6388(PMU_AUXADC_SPL_NUM_SEL_ADDR, PMU_AUXADC_SPL_NUM_SEL_MASK, PMU_AUXADC_SPL_NUM_SEL_SHIFT, 0x4);//performance


    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_STATUS_PWRKEY_R_ADDR, PMU_RG_INT_STATUS_PWRKEY_R_MASK, PMU_RG_INT_STATUS_PWRKEY_R_SHIFT, 0x1);//clear irq
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_STATUS_AD_LBAT_LV_ADDR, PMU_RG_INT_STATUS_AD_LBAT_LV_MASK, PMU_RG_INT_STATUS_AD_LBAT_LV_SHIFT, 0x1);//clear irq
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_STATUS_VLDO33_LBAT_DET_ADDR, PMU_RG_INT_STATUS_VLDO33_LBAT_DET_MASK, PMU_RG_INT_STATUS_VLDO33_LBAT_DET_SHIFT, 0x1);//clear irq
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_STATUS_VBAT_RECHG_ADDR, PMU_RG_INT_STATUS_VBAT_RECHG_MASK, PMU_RG_INT_STATUS_VBAT_RECHG_SHIFT, 0x1);//clear irq
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_STATUS_JEITA_HOT_ADDR, PMU_RG_INT_STATUS_JEITA_HOT_MASK, PMU_RG_INT_STATUS_JEITA_HOT_SHIFT, 0x1);//clear irq
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_STATUS_JEITA_WARM_ADDR, PMU_RG_INT_STATUS_JEITA_WARM_MASK, PMU_RG_INT_STATUS_JEITA_WARM_SHIFT, 0x1);//clear irq
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_STATUS_JEITA_COOL_ADDR, PMU_RG_INT_STATUS_JEITA_COOL_MASK, PMU_RG_INT_STATUS_JEITA_COOL_SHIFT, 0x1);//clear irq
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_STATUS_JEITA_COLD_ADDR, PMU_RG_INT_STATUS_JEITA_COLD_MASK, PMU_RG_INT_STATUS_JEITA_COLD_SHIFT, 0x1);//clear irq
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_EN_PWRKEY_ADDR, PMU_RG_INT_EN_PWRKEY_MASK, PMU_RG_INT_EN_PWRKEY_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_EN_PWRKEY_R_ADDR, PMU_RG_INT_EN_PWRKEY_R_MASK, PMU_RG_INT_EN_PWRKEY_R_SHIFT, 1);
#ifdef AB1555
    pmu_set_register_value_2byte_mt6388(PMU_RG_VCORE_RSV_ADDR,PMU_RG_VCORE_RSV_MASK,PMU_RG_VCORE_RSV_SHIFT,0x2000);
    pmu_set_register_value_2byte_mt6388(PMU_RG_VAUD18_RSV_ADDR,PMU_RG_VAUD18_RSV_MASK,PMU_RG_VAUD18_RSV_SHIFT,0x6000);
#else
    pmu_set_register_value_2byte_mt6388(PMU_RG_VCORE_RSV_ADDR,PMU_RG_VCORE_RSV_MASK,PMU_RG_VCORE_RSV_SHIFT,0x0);
#endif
    //pmu_vaud18_sleep_voltage_sel_6388(PMIC_VAUD18_0P85_V); /*for low power setting*/
    PMU_control_all_lp_mode(PMU_HW_MODE);
    pmu_auxadc_init();
    pmu_set_register_value_2byte_mt6388(PMU_INT_STATUS0, 0xFFFF, 0, 0xFFFF);
    pmu_set_register_value_2byte_mt6388(PMU_INT_STATUS1, 0xFFFF, 0, 0xFFFF);
    pmu_set_register_value_2byte_mt6388(PMU_INT_STATUS2, 0xFFFF, 0, 0xFFFF);

    //*VRF init setting for low power*/
    pmu_voltage_selet_6388(PMU_NORMAL,PMU_BUCK_VRF,0x1C);
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_MASK_JEITA_HOT_ADDR, PMU_RG_INT_MASK_JEITA_HOT_MASK, PMU_RG_INT_MASK_JEITA_HOT_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_MASK_JEITA_WARM_ADDR, PMU_RG_INT_MASK_JEITA_WARM_MASK, PMU_RG_INT_MASK_JEITA_WARM_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_MASK_JEITA_COOL_ADDR, PMU_RG_INT_MASK_JEITA_COOL_MASK, PMU_RG_INT_MASK_JEITA_COOL_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_RG_INT_MASK_JEITA_COLD_ADDR, PMU_RG_INT_MASK_JEITA_COLD_MASK, PMU_RG_INT_MASK_JEITA_COLD_SHIFT, 1);
    /*power_off reason clean*/
    pmu_set_register_value_2byte_mt6388(PMU_RG_POFFSTS_CLR_ADDR, PMU_RG_POFFSTS_CLR_MASK, PMU_RG_POFFSTS_CLR_SHIFT, 0x4400);
    pmu_set_register_value_2byte_mt6388(PMU_RG_POFFSTS_EN_ADDR, PMU_RG_POFFSTS_EN_MASK, PMU_RG_POFFSTS_EN_SHIFT, 0x3ffc);
    pmu_basic_index = pmu_get_vcore_voltage_mt6388();
    if (pmu_get_raw_status_interrupt(RG_INT_CHRDET)) {
        pmu_charger_status = pmu_get_register_value_2byte_mt6388(PMU_DA_QI_CHR_REF_EN_ADDR, PMU_DA_QI_CHR_REF_EN_MASK, PMU_DA_QI_CHR_REF_EN_SHIFT);
    }
    pmu_eint_init();
    /*For MT2801D setting*/
#if defined(AB1552)
    pmu_set_register_value_2byte_mt6388(0x33A, 0xffff, 0, 0);
    pmu_set_register_value_2byte_mt6388(0x338, 0xffff, 0, 0x3);
#endif
    pmu_long_press_shutdown_function_sel(PMU_RELEASE_PWRKEY);
    //Setting for E4
    if (pmu_get_register_value_internal(APB_PMU, 0x1, 0) == 1) {
        log_hal_msgid_info("E4 Setting", 0);
        uint32_t temp_value = 0;
        temp_value = pmu_get_register_value_internal(APB_PMU, 0x3f, 2);
        pmu_set_register_value_internal(PMU_ELR_0, 0x3f, 0, temp_value);
    }
    pmu_scan_interrupt_status();
    pmu_irq_init();
    pmu_init_flag=1;
#ifdef HAL_SLEEP_MANAGER_ENABLED
    sleep_management_register_suspend_callback(SLEEP_BACKUP_RESTORE_PMU, (sleep_management_suspend_callback_t)hal_pmu_sleep_backup, NULL);
#endif
}

//================[PMIC Interrupt API]=====================
pmu_status_t pmu_control_enable_interrupt(pmu_interrupt_index_t int_channel, int isEnable) {
    if ((int_channel >= 0) && (int_channel <= 10)) {
        pmu_set_register_value_2byte_mt6388(PMU_INT_CON0, 0x1, int_channel, isEnable);
        if (isEnable) {
            pmu_irq_enable_com0 |= 0x1 << int_channel;
        } else {
            pmu_set_register_value_internal((uint32_t)&pmu_irq_enable_com0, 0x1, int_channel, 0);
        }
    } else if ((int_channel >= 11) && (int_channel <= 23)) {
        pmu_set_register_value_2byte_mt6388(PMU_INT_CON1, 0x1, (int_channel - 11), isEnable);

        if (isEnable) {
            pmu_irq_enable_com1 |= 0x1 << (int_channel - 11);
        } else {
            pmu_set_register_value_internal((uint32_t)&pmu_irq_enable_com1, 0x1, (int_channel - 11), 0);
        }
    } else if ((int_channel >= 24) && (int_channel <= 29)) {
        pmu_set_register_value_2byte_mt6388(PMU_INT_CON2, 0x1, (int_channel - 24), isEnable);

        if (isEnable) {
            pmu_irq_enable_com2 |= 0x1 << (int_channel - 24);
        } else {
            pmu_set_register_value_internal((uint32_t)&pmu_irq_enable_com2, 0x1, (int_channel - 24), 0);
        }
    } else if ((int_channel >= 30) && (int_channel <= 39)) {
        pmu_set_register_value_2byte_mt6388(PMU_INT_CON3, 0x1, (int_channel - 30), isEnable);
        if (isEnable) {
            pmu_irq_enable_com3 |= 0x1 << (int_channel - 30);
        } else {
            pmu_set_register_value_internal((uint32_t)&pmu_irq_enable_com3, 0x1, (int_channel - 30), 0);
        }
    } else {
        log_hal_msgid_info("Error interrupt index", 0);
        return PMU_STATUS_ERROR;
    }
    return PMU_STATUS_SUCCESS;
}

pmu_status_t pmu_control_mask_interrupt(pmu_interrupt_index_t int_channel, int isEnable)
{
    if ((int_channel >= 0) && (int_channel <= 10)) {
        pmu_set_register_value_2byte_mt6388(PMU_INT_MASK_CON0, 0x1, int_channel, isEnable);
    } else if ((int_channel >= 11) && (int_channel <= 23)) {
        pmu_set_register_value_2byte_mt6388(PMU_INT_MASK_CON1, 0x1, (int_channel - 11), isEnable);
    } else if ((int_channel >= 24) && (int_channel <= 29)) {
        pmu_set_register_value_2byte_mt6388(PMU_INT_MASK_CON2, 0x1, (int_channel - 24), isEnable);
    } else if ((int_channel >= 30) && (int_channel <= 39)) {
        pmu_set_register_value_2byte_mt6388(PMU_INT_MASK_CON3, 0x1, (int_channel - 30), isEnable);
    } else {
        log_hal_msgid_info("Error interrupt index", 0);
        return PMU_STATUS_ERROR;
    }
    return PMU_STATUS_SUCCESS;
}
void pmu_irq_count(int int_channel) {
    if ((int_channel >= 0) && (int_channel <= 10)) {
        event_con0 = 1;
    } else if ((int_channel >= 11) && (int_channel <= 23)) {
        event_con1 = 1;
    } else if ((int_channel >= 24) && (int_channel <= 29)) {
        event_con2 = 1;
    } else if ((int_channel >= 30) && (int_channel <= 39)) {
        event_con3 = 1;
    }
}
pmu_status_t pmu_control_clear_interrupt(pmu_interrupt_index_t int_channel) {
    if (int_channel == PMU_INT_MAX) {
        if ((pmu_irq_enable_com0 != 0) || (event_con0 != 0)) {
            pmu_set_register_value_2byte_mt6388(PMU_INT_CON0, 0x7ff, 0, 0);
        }
        if ((pmu_irq_enable_com1 != 0) || (event_con1 != 0)) {
            pmu_set_register_value_2byte_mt6388(PMU_INT_CON1, 0x1fff, 0, 0);
        }
        if ((pmu_irq_enable_com2 != 0) || (event_con2 != 0)) {
            pmu_set_register_value_2byte_mt6388(PMU_INT_CON2, 0x3f, 0, 0);
        }
        if ((pmu_irq_enable_com3 != 0) || (event_con3 != 0)) {
            pmu_set_register_value_2byte_mt6388(PMU_INT_CON3, 0x3ff, 0, 0);

        }
        hal_gpt_delay_us(150);

        if ((pmu_irq_enable_com1 != 0) || (event_con1 != 0)) {
            pmu_set_register_value_2byte_mt6388(PMU_INT_CON1, 0x1fff, 0, pmu_irq_enable_com1);
            event_con1 = 0;
        }
        if ((pmu_irq_enable_com2 != 0) || (event_con2 != 0)) {
            pmu_set_register_value_2byte_mt6388(PMU_INT_CON2, 0x3f, 0, pmu_irq_enable_com2);
            event_con2 = 0;
        }
        if ((pmu_irq_enable_com3 != 0) || (event_con3 != 0)) {
            pmu_set_register_value_2byte_mt6388(PMU_INT_CON3, 0x3ff, 0, pmu_irq_enable_com3);
            event_con3 = 0;
        }
        if ((pmu_irq_enable_com0 != 0) || (event_con0 != 0)) {
            pmu_set_register_value_2byte_mt6388(PMU_INT_CON0, 0x7ff, 0, pmu_irq_enable_com0);
            event_con0 = 0;
        }
    } else {
        pmu_control_enable_interrupt(int_channel, 0);
        hal_gpt_delay_us(150);
        pmu_control_enable_interrupt(int_channel, 1);
    }
    return PMU_STATUS_SUCCESS;
}

uint32_t pmu_get_raw_status_interrupt(pmu_interrupt_index_t int_channel)
{
    return pmu_get_register_value_2byte_mt6388(PMU_RG_INT_RAW_STATUS_CHRDET_ADDR, PMU_RG_INT_RAW_STATUS_CHRDET_MASK,PMU_RG_INT_RAW_STATUS_CHRDET_SHIFT);
}
int pmic_irq0=-1,pmic_irq1=-1,pmic_irq2=-1,pmic_irq3=-1;

void pmu_get_all_int_status() {
    if (pmu_irq_enable_com0 != 0) {
        pmic_irq0 = pmu_get_register_value_2byte_mt6388(PMU_INT_STATUS0, 0xffff, 0);
    }
    if (pmu_irq_enable_com1 != 0) {
        pmic_irq1 = pmu_get_register_value_2byte_mt6388(PMU_INT_STATUS1, 0xffff, 0);
    }
    if (pmu_irq_enable_com2 != 0) {
        pmic_irq2 = pmu_get_register_value_2byte_mt6388(PMU_INT_STATUS2, 0xffff, 0);
    }
    if (pmu_irq_enable_com3 != 0) {
        pmic_irq3 = pmu_get_register_value_2byte_mt6388(PMU_INT_STATUS3, 0xffff, 0);
    }
}

/*irq[32]:ChgStatInt is charger state machine status ,it was different other irq ,Need to do it separately ;30~39 belong con3*/
int pmu_get_status_interrupt(pmu_interrupt_index_t int_channel)
{
    int statusValue = -1;
    if ((int_channel >= 0) && (int_channel <= 10)) {
        statusValue = pmu_get_register_value_internal((uint32_t)&pmic_irq0, 1, int_channel);
    } else if ((int_channel >= 11) && (int_channel <= 23)) {
        statusValue = pmu_get_register_value_internal((uint32_t)&pmic_irq1, 1, (int_channel - 11));
    } else if ((int_channel >= 24) && (int_channel <= 29)) {
        statusValue = pmu_get_register_value_internal((uint32_t)&pmic_irq2, 1, (int_channel - 24));
    } else if ((int_channel >= 30) && (int_channel <= 31)) {
        statusValue = pmu_get_register_value_internal((uint32_t)&pmic_irq3, 1, (int_channel - 30));
    } else if ((int_channel == 32)) {
        statusValue = pmu_get_register_value_2byte_mt6388(PMU_INT_STATUS3, 0x1, 2);
    }else if ((int_channel >= 33) && (int_channel <= 39)) {
        statusValue = pmu_get_register_value_internal((uint32_t)&pmic_irq3, 1, (int_channel - 30));
    }else {
        log_hal_msgid_info("Error interrupt index", 0);
        return PMU_STATUS_INVALID_PARAMETER;
    }

    return statusValue;
}
bool pmu_is_charger_exist_init(void) {
    if (pmu_get_raw_status_interrupt(RG_INT_CHRDET) == 1) {
        return true;
    } else {
        return false;
    }
}

pmu_operate_status_t pmu_pwrkey_normal_key_init(pmu_pwrkey_config_t *config)
{
    pmu_status_t status = PMU_STATUS_ERROR;
    status = pmu_register_callback(RG_INT_PWRKEY, config->callback1,config->user_data1);
    if (status != PMU_STATUS_SUCCESS) {
        return PMU_STATUS_ERROR;
    }
    return PMU_STATUS_SUCCESS;
}

pmu_status_t pmu_register_callback(pmu_interrupt_index_t pmu_int_ch, pmu_callback_t callback, void *user_data)
{
    pmu_status_t status = PMU_STATUS_ERROR;
    if(pmu_int_ch >= PMU_INT_MAX || callback == NULL)
    {
        return PMU_STATUS_INVALID_PARAMETER;
    }
    pmu_function_table[pmu_int_ch].init_status = PMU_INIT;
    pmu_function_table[pmu_int_ch].pmu_callback= callback;
    pmu_function_table[pmu_int_ch].user_data = user_data;
    pmu_function_table[pmu_int_ch].isMask= false;
    if(pmu_int_ch>31){
        pmu_register_interrupt_2 |=(1<<(pmu_int_ch-32)) ;
    }else{
        pmu_register_interrupt |=(1<<pmu_int_ch) ;
    }
    pmu_control_enable_interrupt(pmu_int_ch, 1);
    pmu_control_mask_interrupt(pmu_int_ch, 0);

    status = PMU_STATUS_SUCCESS;
    return status;
}

pmu_status_t pmu_deregister_callback(pmu_interrupt_index_t pmu_int_ch)
{
    pmu_status_t status = PMU_STATUS_ERROR;

    if(pmu_int_ch >= PMU_INT_MAX )
    {
        return PMU_STATUS_INVALID_PARAMETER;
    }
    pmu_function_table[pmu_int_ch].init_status = PMU_NOT_INIT;
    pmu_function_table[pmu_int_ch].pmu_callback= NULL;
    pmu_function_table[pmu_int_ch].user_data = NULL;
    pmu_function_table[pmu_int_ch].isMask= true;

    pmu_control_enable_interrupt(pmu_int_ch, 0);
    pmu_control_mask_interrupt(pmu_int_ch, 1);
    status = PMU_STATUS_SUCCESS;

    return status;
}

pmu_status_t pmu_control_mask(pmu_interrupt_index_t pmu_int_ch, bool isMaskInterrupt)
{
    if(pmu_int_ch >= PMU_INT_MAX || pmu_function_table[pmu_int_ch].pmu_callback == NULL)
    {
        return PMU_STATUS_INVALID_PARAMETER;
    }

    pmu_function_table[pmu_int_ch].isMask = isMaskInterrupt;
    return PMU_STATUS_SUCCESS;
}

void pmu_scan_interrupt_status(void)
{
    uint8_t index = 0xFF;
    uint8_t value = 0;
    pmu_get_all_int_status();
    for(index = RG_INT_PWRKEY; index < PMU_INT_MAX; index++)
    {
        value = pmu_get_status_interrupt(index);

        if(value == 1)
        {
            log_hal_msgid_info("Value = 1, Interupt Index:%d\r\n",1, index);

            if (pmu_function_table[index].isMask == false) {
                if (pmu_function_table[index].pmu_callback)
                    pmu_function_table[index].pmu_callback();

                //Clear Interrupt
                pmu_control_enable_interrupt(index, 0);
                pmu_control_enable_interrupt(index, 1);
            }
        }
    }
}

pmu_operate_status_t pmu_pwrkey_enable(pmu_power_operate_t oper){
    return pmu_set_register_value_2byte_mt6388(PMU_RG_PWRKEY_RST_EN_ADDR, PMU_RG_PWRKEY_RST_EN_MASK, PMU_RG_PWRKEY_RST_EN_SHIFT, oper);
}

pmu_operate_status_t pmu_pwrkey_duration_time(pmu_pwrkey_time_t tmr){
    return pmu_set_register_value_2byte_mt6388(PMU_RG_PWRKEY_RST_TD_ADDR, PMU_RG_PWRKEY_RST_TD_MASK, PMU_RG_PWRKEY_RST_TD_SHIFT, tmr);
}

pmu_operate_status_t pmu_long_press_shutdown_function_sel(pmu_pwrkey_scenario_t oper) {
    if (oper == PMU_RESET_DEFAULT) {
        pmu_set_register_value_2byte_mt6388(PMU_RG_STRUP_LONG_PRESS_EXT_EN_ADDR, PMU_RG_STRUP_LONG_PRESS_EXT_EN_MASK, PMU_RG_STRUP_LONG_PRESS_EXT_EN_SHIFT,PMU_OFF);
        return pmu_set_register_value_2byte_mt6388(PMU_RG_STRUP_LONG_PRESS_EXT_SEL_ADDR, PMU_RG_STRUP_LONG_PRESS_EXT_SEL_MASK,PMU_RG_STRUP_LONG_PRESS_EXT_SEL_SHIFT, 0);
    } else {
        pmu_set_register_value_2byte_mt6388(PMU_RG_STRUP_LONG_PRESS_EXT_EN_ADDR, PMU_RG_STRUP_LONG_PRESS_EXT_EN_MASK, PMU_RG_STRUP_LONG_PRESS_EXT_EN_SHIFT,PMU_ON);
        return pmu_set_register_value_2byte_mt6388(PMU_RG_STRUP_LONG_PRESS_EXT_SEL_ADDR, PMU_RG_STRUP_LONG_PRESS_EXT_SEL_MASK,PMU_RG_STRUP_LONG_PRESS_EXT_SEL_SHIFT, oper);
    }
}

//==================[Debug api]===========================
void pmu_vcroe_voltage_turing(int symbol, int num) {
    uint32_t cur_v;
    if (symbol) {
        cur_v = pmu_get_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT);
        cur_v += num;
        pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, cur_v);
    } else {
        cur_v = pmu_get_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT);
        cur_v -= num;
        pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, cur_v);
    }
}

void pmu_vaud18_voltage_turing(int symbol, int num) {
    uint32_t cur_v;
    if (symbol) {
        cur_v = pmu_get_register_value_mt6388(PMU_RG_BUCK_VAUD18_VOSEL_ADDR, PMU_RG_BUCK_VAUD18_VOSEL_MASK, PMU_RG_BUCK_VAUD18_VOSEL_SHIFT);
        cur_v += num;
        pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, cur_v);
    } else {
        cur_v = pmu_get_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT);
        cur_v -= num;
        pmu_set_register_value_mt6388(PMU_RG_BUCK_VCORE_VOSEL_ADDR, PMU_RG_BUCK_VCORE_VOSEL_MASK, PMU_RG_BUCK_VCORE_VOSEL_SHIFT, cur_v);
    }
}

void pmu_vio18_voltage_turing(int symbol, int num) {
    uint32_t cur_v;
    if (symbol) {
        cur_v = pmu_get_register_value_mt6388(PMU_RG_BUCK_VIO18_VOSEL_ADDR, PMU_RG_BUCK_VIO18_VOSEL_MASK, PMU_RG_BUCK_VIO18_VOSEL_SHIFT);
        cur_v += num;
        pmu_set_register_value_mt6388(PMU_RG_BUCK_VIO18_VOSEL_ADDR, PMU_RG_BUCK_VIO18_VOSEL_MASK, PMU_RG_BUCK_VIO18_VOSEL_SHIFT, cur_v);
    } else {
        cur_v = pmu_get_register_value_mt6388(PMU_RG_BUCK_VIO18_VOSEL_ADDR, PMU_RG_BUCK_VIO18_VOSEL_MASK, PMU_RG_BUCK_VIO18_VOSEL_SHIFT);
        cur_v -= num;
        pmu_set_register_value_mt6388(PMU_RG_BUCK_VIO18_VOSEL_ADDR, PMU_RG_BUCK_VIO18_VOSEL_MASK, PMU_RG_BUCK_VIO18_VOSEL_SHIFT, cur_v);
    }
}

void pmu_lock_va18(int oper){
    if(oper){
        va18_flag =PMU_ON;
    }else{
        va18_flag =PMU_OFF;
    }
}
#endif /* HAL_PMU_MODULE_ENABLED */
