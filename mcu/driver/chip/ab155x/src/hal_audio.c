/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include <string.h>
#include "hal_audio.h"

/*
  In this file, we implement the APIs listed in audio.h.
  If we need to communicate with DSP, it will call APIs provided by hal_audio_dsp_controller.c.
*/


#if defined(HAL_AUDIO_MODULE_ENABLED)

//==== Include header files ====
#include "hal_log.h"
#include "hal_ccni.h"
#include "hal_gpio.h"
#include "memory_attribute.h"
#include "hal_audio_cm4_dsp_message.h"
#include "hal_audio_message_struct.h"
#include "hal_audio_internal.h"
#include "hal_clock_platform.h"
#include "hal_hw_semaphore.h"
#include "hal_resource_assignment.h"

#ifdef HAL_DVFS_MODULE_ENABLED
#include "hal_dvfs.h"
#include "hal_dvfs_internal.h"
#endif

#if defined(HAL_AUDIO_SUPPORT_APLL)
extern ATTR_TEXT_IN_TCM hal_clock_status_t clock_mux_sel(clock_mux_sel_id mux_id, uint32_t mux_sel);
extern  uint8_t clock_set_pll_on(clock_pll_id pll_id);
extern  uint8_t clock_set_pll_off(clock_pll_id pll_id);
extern void platform_assert(const char *expr, const char *file, int line);
static int16_t aud_apll_1_cntr;
static int16_t aud_apll_2_cntr;
static hal_audio_mclk_status_t mclk_status[4]; // 4 is number of I2S interfaces.
extern void ami_hal_audio_status_set_running_flag(audio_message_type_t type, bool is_running);
#endif

//==== Static variables ====
uint16_t g_stream_in_sample_rate = 16000;
uint16_t g_stream_in_code_type   = AUDIO_DSP_CODEC_TYPE_PCM;//modify for opus
encoder_bitrate_t g_bit_rate = ENCODER_BITRATE_32KBPS;
uint16_t g_wwe_mode = 0;

audio_common_t audio_common;
HAL_AUDIO_DVFS_CLK_SELECT_t audio_nvdm_dvfs_config;
#ifdef HAL_AUDIO_SUPPORT_MULTIPLE_STREAM_OUT
#define NVKEY_INDEX1 0xC0
#define NVKEY_INDEX2 0x30
#define NVKEY_INDEX3 0x0C
#define NVKEY_INDEX4 0x03
HAL_AUDIO_CHANNEL_SELECT_t audio_Channel_Select;
HAL_DSP_PARA_AU_AFE_CTRL_t audio_nvdm_HW_config;
bool g_record_airdump = false;
const audio_version_t SW_version = SDK_V1p4; /*Need Change by Owner.*/
volatile audio_version_t nvdm_version = SDK_NONE;
#endif
#define HAL_AUDIO_MAX_OUTPUT_SAMPLING_FREQUENCY 192000
#define HAL_AUDIO_SAMPLING_RATE_MAX HAL_AUDIO_SAMPLING_RATE_192KHZ
//static int default_audio_device_out     = HAL_AUDIO_DEVICE_DAC_DUAL;
//static int default_audio_device_in      = HAL_AUDIO_DEVICE_MAIN_MIC_L;
const char supported_SR_audio_adc_in[HAL_AUDIO_SAMPLING_RATE_MAX+1] = {
    false,  //HAL_AUDIO_SAMPLING_RATE_8KHZ      = 0, /**< 8000Hz  */
    false,  //HAL_AUDIO_SAMPLING_RATE_11_025KHZ = 1, /**< 11025Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_12KHZ     = 2, /**< 12000Hz */
    true,   //HAL_AUDIO_SAMPLING_RATE_16KHZ     = 3, /**< 16000Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_22_05KHZ  = 4, /**< 22050Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_24KHZ     = 5, /**< 24000Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_32KHZ     = 6, /**< 32000Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_44_1KHZ   = 7, /**< 44100Hz */
    true,   //HAL_AUDIO_SAMPLING_RATE_48KHZ     = 8, /**< 48000Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_88_2KHZ   = 9, /**< 88200Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_96KHZ     = 10,/**< 96000Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_176_4KHZ  = 11,/**< 176400Hz */
    false   //HAL_AUDIO_SAMPLING_RATE_192KHZ    = 12,/**< 192000Hz */
    };

const char supported_SR_audio_dac_out[HAL_AUDIO_SAMPLING_RATE_MAX+1] = {
    false,  //HAL_AUDIO_SAMPLING_RATE_8KHZ      = 0, /**< 8000Hz  */
    false,  //HAL_AUDIO_SAMPLING_RATE_11_025KHZ = 1, /**< 11025Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_12KHZ     = 2, /**< 12000Hz */
    true,   //HAL_AUDIO_SAMPLING_RATE_16KHZ     = 3, /**< 16000Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_22_05KHZ  = 4, /**< 22050Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_24KHZ     = 5, /**< 24000Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_32KHZ     = 6, /**< 32000Hz */
    true,   //HAL_AUDIO_SAMPLING_RATE_44_1KHZ   = 7, /**< 44100Hz */
    true,   //HAL_AUDIO_SAMPLING_RATE_48KHZ     = 8, /**< 48000Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_88_2KHZ   = 9, /**< 88200Hz */
    true,   //HAL_AUDIO_SAMPLING_RATE_96KHZ     = 10,/**< 96000Hz */
    false,  //HAL_AUDIO_SAMPLING_RATE_176_4KHZ  = 11,/**< 176400Hz */
    true    //HAL_AUDIO_SAMPLING_RATE_192KHZ    = 12,/**< 192000Hz */
    };

const char supported_SR_audio_i2s_inout[HAL_AUDIO_SAMPLING_RATE_MAX+1] = {
    true,  //HAL_AUDIO_SAMPLING_RATE_8KHZ      = 0, /**< 8000Hz  */
    true,  //HAL_AUDIO_SAMPLING_RATE_11_025KHZ = 1, /**< 11025Hz */
    true,  //HAL_AUDIO_SAMPLING_RATE_12KHZ     = 2, /**< 12000Hz */
    true,  //HAL_AUDIO_SAMPLING_RATE_16KHZ     = 3, /**< 16000Hz */
    true,  //HAL_AUDIO_SAMPLING_RATE_22_05KHZ  = 4, /**< 22050Hz */
    true,  //HAL_AUDIO_SAMPLING_RATE_24KHZ     = 5, /**< 24000Hz */
    true,  //HAL_AUDIO_SAMPLING_RATE_32KHZ     = 6, /**< 32000Hz */
    true,  //HAL_AUDIO_SAMPLING_RATE_44_1KHZ   = 7, /**< 44100Hz */
    true,  //HAL_AUDIO_SAMPLING_RATE_48KHZ     = 8, /**< 48000Hz */
    true,  //HAL_AUDIO_SAMPLING_RATE_88_2KHZ   = 9, /**< 88200Hz */
    true,  //HAL_AUDIO_SAMPLING_RATE_96KHZ     = 10,/**< 96000Hz */
    true,  //HAL_AUDIO_SAMPLING_RATE_176_4KHZ  = 11,/**< 176400Hz */
    true   //HAL_AUDIO_SAMPLING_RATE_192KHZ    = 12,/**< 192000Hz */
    };

//==== Public API ====
hal_audio_status_t hal_audio_init(void)
{
    if (audio_common.init) {
        return HAL_AUDIO_STATUS_OK;
    }

    hal_audio_dsp_controller_init();

#if defined(HAL_AUDIO_SUPPORT_APLL)
    aud_apll_1_cntr = 0;
    aud_apll_2_cntr = 0;
    memset((void *)&mclk_status, 0, 4 * sizeof(hal_audio_mclk_status_t));
#endif

    audio_common.init = true;

    return HAL_AUDIO_STATUS_OK;
}

hal_audio_status_t hal_audio_deinit(void)
{
    if (audio_common.init) {
        hal_audio_dsp_controller_deinit();
    }

    audio_common.init = false;

    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Register callback to copy the content of stream out
  * @ callback : callback function
  * @ user_data : user data (for exampple, handle)
  * @ Retval: HAL_AUDIO_STATUS_OK if operation is successful, others if operation is invalid
  */
hal_audio_status_t hal_audio_register_copied_stream_out_callback(hal_audio_stream_copy_callback_t callback, void *user_data)
{
    //KH: ToDo
    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Updates the audio output frequency
  * @ sample_rate : audio frequency used to play the audio stream
  * @ This API should be called before hal_audio_start_stream_out() to adjust the audio frequency
  * @ Retval: HAL_AUDIO_STATUS_OK if operation is successful, others if sample rate is invalid
  */
hal_audio_status_t hal_audio_set_stream_out_sampling_rate(hal_audio_sampling_rate_t sampling_rate)
{
    switch (sampling_rate) {
        case HAL_AUDIO_SAMPLING_RATE_8KHZ:
        case HAL_AUDIO_SAMPLING_RATE_11_025KHZ:
        case HAL_AUDIO_SAMPLING_RATE_12KHZ:
        case HAL_AUDIO_SAMPLING_RATE_16KHZ:
        case HAL_AUDIO_SAMPLING_RATE_22_05KHZ:
        case HAL_AUDIO_SAMPLING_RATE_24KHZ:
        case HAL_AUDIO_SAMPLING_RATE_32KHZ:
        case HAL_AUDIO_SAMPLING_RATE_44_1KHZ:
        case HAL_AUDIO_SAMPLING_RATE_48KHZ:
        case HAL_AUDIO_SAMPLING_RATE_88_2KHZ:
        case HAL_AUDIO_SAMPLING_RATE_96KHZ:
        case HAL_AUDIO_SAMPLING_RATE_176_4KHZ:
        case HAL_AUDIO_SAMPLING_RATE_192KHZ:
            audio_common.stream_out.stream_sampling_rate = sampling_rate;
            return HAL_AUDIO_STATUS_OK;
        default:
            return HAL_AUDIO_STATUS_INVALID_PARAMETER;
    }
}

/**
  * @ Updates the audio output channel number
  * @ channel_number : audio channel mode to play the audio stream
  * @ This API should be called before hal_audio_start_stream_out() to adjust the output channel number
  * @ Retval: HAL_AUDIO_STATUS_OK if operation success, others if channel number is invalid
  */
hal_audio_status_t hal_audio_set_stream_out_channel_number(hal_audio_channel_number_t channel_number)
{
    hal_audio_status_t result = HAL_AUDIO_STATUS_OK;
    switch (channel_number) {
        case HAL_AUDIO_MONO:
            audio_common.stream_out.stream_channel = HAL_AUDIO_MONO;
            break;
        case HAL_AUDIO_STEREO:
        case HAL_AUDIO_STEREO_BOTH_L_CHANNEL:
        case HAL_AUDIO_STEREO_BOTH_R_CHANNEL:
        case HAL_AUDIO_STEREO_BOTH_L_R_SWAP:
            audio_common.stream_out.stream_channel = HAL_AUDIO_STEREO;
            break;
        default:
            return HAL_AUDIO_STATUS_INVALID_PARAMETER;
    }
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_SET_OUTPUT_DEVICE_CHANNEL, 0, channel_number, false);
    return result;
}

/**
  * @ Updates the audio output channel mode
  * @ channel_mode : audio channel mode to play the audio stream
  * @ This API should be called before hal_audio_start_stream_out() to adjust the output channel mode
  * @ Retval: HAL_AUDIO_STATUS_OK if operation is successful, others if channel mode is invalid
  */
hal_audio_status_t hal_audio_set_stream_out_channel_mode(hal_audio_channel_number_t channel_mode)
{
    hal_audio_status_t result = HAL_AUDIO_STATUS_OK;
    switch (channel_mode) {
        case HAL_AUDIO_MONO:
        case HAL_AUDIO_STEREO:
        case HAL_AUDIO_STEREO_BOTH_L_CHANNEL:
        case HAL_AUDIO_STEREO_BOTH_R_CHANNEL:
        case HAL_AUDIO_STEREO_BOTH_L_R_SWAP:
            audio_common.stream_out.stream_channel_mode = channel_mode;
            break;
        default:
            result = HAL_AUDIO_STATUS_INVALID_PARAMETER;
            break;
    }
    return result;
}

/**
  * @ Start the playback of audio stream
  */
hal_audio_status_t hal_audio_start_stream_out(hal_audio_active_type_t active_type)
{
    //ToDo: limit the scope -- treat it as local playback
    //audio_dsp_playback_info_t temp_param;
    void *p_param_share;
    bool is_running;

    audio_message_type_t msg_type = AUDIO_MESSAGE_TYPE_PLAYBACK;
    //n9_dsp_share_info_t *p_share_buf_info;

    is_running = hal_audio_status_query_running_flag(AUDIO_MESSAGE_TYPE_PLAYBACK);
    if (is_running) {
        // Reentry: don't allow multiple playback
        log_hal_msgid_info("Re-entry\r\n", 0);
    } else {
        hal_audio_status_set_running_flag(AUDIO_MESSAGE_TYPE_PLAYBACK, true);
    }

    // Open playback
    mcu2dsp_open_param_t open_param;

    // Collect parameters
    open_param.param.stream_in  = STREAM_IN_PLAYBACK;
    open_param.param.stream_out = STREAM_OUT_AFE;

    open_param.stream_in_param.playback.bit_type = HAL_AUDIO_BITS_PER_SAMPLING_16;
    open_param.stream_in_param.playback.sampling_rate = audio_common.stream_out.stream_sampling_rate;
    open_param.stream_in_param.playback.channel_number = audio_common.stream_out.stream_channel;
    open_param.stream_in_param.playback.codec_type = 0;  //KH: should use AUDIO_DSP_CODEC_TYPE_PCM
    open_param.stream_in_param.playback.p_share_info = (n9_dsp_share_info_t *)hal_audio_query_share_info(msg_type);

    hal_audio_reset_share_info( open_param.stream_in_param.playback.p_share_info );
#if 0
    open_param.stream_out_param.afe.audio_device    = HAL_AUDIO_DEVICE_DAC_DUAL;
    open_param.stream_out_param.afe.stream_channel  = HAL_AUDIO_DIRECT;
    if(open_param.stream_out_param.afe.audio_device == HAL_AUDIO_DEVICE_I2S_MASTER) {
        open_param.stream_out_param.afe.misc_parms      = I2S_CLK_SOURCE_DCXO;
    } else {
        open_param.stream_out_param.afe.misc_parms      = DOWNLINK_PERFORMANCE_NORMAL;
    }
#else
    hal_audio_get_stream_out_setting_config(AU_DSP_AUDIO, &open_param.stream_out_param);
#endif
    open_param.stream_out_param.afe.memory          = HAL_AUDIO_MEM1;
    open_param.stream_out_param.afe.format          = AFE_PCM_FORMAT_S16_LE;
    open_param.stream_out_param.afe.stream_out_sampling_rate   = 16000;
    open_param.stream_out_param.afe.sampling_rate   = 16000;
    open_param.stream_out_param.afe.irq_period      = 10;
    open_param.stream_out_param.afe.frame_size      = 256;
    open_param.stream_out_param.afe.frame_number    = 2;
    open_param.stream_out_param.afe.hw_gain         = false;
    p_param_share = hal_audio_dsp_controller_put_paramter( &open_param, sizeof(mcu2dsp_open_param_t), msg_type);

    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_PLAYBACK_OPEN, AUDIO_DSP_CODEC_TYPE_PCM, (uint32_t)p_param_share, true);

    // Start playback
    mcu2dsp_start_param_t start_param;

    // Collect parameters
    start_param.param.stream_in     = STREAM_IN_PLAYBACK;
    start_param.param.stream_out    = STREAM_OUT_AFE;

    start_param.stream_out_param.afe.aws_flag   =  false;

    p_param_share = hal_audio_dsp_controller_put_paramter( &start_param, sizeof(mcu2dsp_start_param_t), msg_type);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_PLAYBACK_START, 0, (uint32_t)p_param_share, true);

    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Stop the playback of audio stream
  */
void hal_audio_stop_stream_out(void)
{
    //ToDo: limit the scope -- treat it as local playback
    audio_message_type_t msg_type = AUDIO_MESSAGE_TYPE_PLAYBACK;
    n9_dsp_share_info_t *p_share_buf_info;

    // Stop playback
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_PLAYBACK_STOP, 0, 0, true);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_PLAYBACK_CLOSE, 0, 0, true);

    hal_audio_status_set_running_flag(msg_type, false);

    // Clear buffer
    p_share_buf_info = (n9_dsp_share_info_t *)hal_audio_query_share_info(msg_type);
    hal_audio_reset_share_info(p_share_buf_info);
}

/**
  * @ Updates the audio output volume
  * @ digital_volume_index: digital gain index
  * @ analog_volume_index : analog gain index
  */
hal_audio_status_t hal_audio_set_stream_out_volume(uint32_t digital_volume_index, uint32_t analog_volume_index)
{
    uint32_t data32;
#if defined(HAL_AUDIO_SUPPORT_MULTIPLE_MICROPHONE)
    audio_common.stream_out.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_0] = digital_volume_index;
    audio_common.stream_out.analog_gain_index[INPUT_ANALOG_GAIN_FOR_MIC_L] = analog_volume_index;
#else
    audio_common.stream_out.digital_gain_index = digital_volume_index;
    audio_common.stream_out.analog_gain_index = analog_volume_index;
#endif
    data32 = (analog_volume_index<<16) | (digital_volume_index & 0xFFFF);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_SET_OUTPUT_DEVICE_VOLUME, 0, data32, false);

    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Updates the audio output DL2 volume
  * @ digital_volume_index: digital gain index
  * @ analog_volume_index : analog gain index
  */
hal_audio_status_t hal_audio_set_stream_out_dl2_volume(uint32_t digital_volume_index, uint32_t analog_volume_index)
{
    uint32_t data32;
#if defined(HAL_AUDIO_SUPPORT_MULTIPLE_MICROPHONE)
    audio_common.stream_out_DL2.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_0] = digital_volume_index;
    audio_common.stream_out_DL2.analog_gain_index[INPUT_ANALOG_GAIN_FOR_MIC_L] = analog_volume_index;
#else
    audio_common.stream_out_DL2.digital_gain_index = digital_volume_index;
    audio_common.stream_out_DL2.analog_gain_index = analog_volume_index;
#endif
    data32 = (analog_volume_index<<16) | (digital_volume_index & 0xFFFF);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_SET_OUTPUT_DEVICE_VOLUME, 1, data32, false);

    return HAL_AUDIO_STATUS_OK;
}

#if defined(HAL_AUDIO_SUPPORT_MULTIPLE_STREAM_OUT)
/**
  * @ Mute stream ouput path
  * @ mute: true -> set mute / false -> set unmute
  * @ hw_gain_index: HAL_AUDIO_STREAM_OUT1-> indicate hw gain1 / HAL_AUDIO_STREAM_OUT2-> indicate hw gain2 / HAL_AUDIO_STREAM_OUT_ALL-> indicate hw gain1 and hw gain2
  */
void hal_audio_mute_stream_out(bool mute, hal_audio_hw_stream_out_index_t hw_gain_index)
{
    uint32_t data32;
    if(hw_gain_index == HAL_AUDIO_STREAM_OUT1){
        audio_common.stream_out.mute = mute;
    }else if (hw_gain_index == HAL_AUDIO_STREAM_OUT2){
        audio_common.stream_out_DL2.mute = mute;
    }else{
        audio_common.stream_out.mute = mute;
        audio_common.stream_out_DL2.mute = mute;
    }
    data32 = (hw_gain_index<<16) | (mute & 0xFFFF);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_MUTE_OUTPUT_DEVICE, 0, data32, false);
}
#else
/**
  * @ Mute stream ouput path
  * @ mute: true -> set mute / false -> set unmute
  */
void hal_audio_mute_stream_out(bool mute)
{
    audio_common.stream_out.mute = mute;

    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_MUTE_OUTPUT_DEVICE, 0, mute, false);
}
#endif

/**
  * @ Control the audio output device
  * @ device: output device
  */
hal_audio_status_t hal_audio_set_stream_out_device(hal_audio_device_t device)
{
    audio_common.stream_out.audio_device = device;

    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_SET_OUTPUT_DEVICE, 0, device, false);

    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Write data into audio output stream for playback.
  * @ buffer: Pointer to the buffer
  * @ size : number of audio data [in bytes]
  * @ Retval HAL_AUDIO_STATUS_OK if operation is successful, others if failed.
  */
hal_audio_status_t hal_audio_write_stream_out(const void *buffer, uint32_t size)
{
    //ToDo: limit the scope -- treat it as local playback
    hal_audio_status_t result;

    result = hal_audio_write_stream_out_by_type(AUDIO_MESSAGE_TYPE_PLAYBACK, buffer, size);

    return result;
}

/**
  * @ Query the free space of output stream.
  * @ sample_count : number of free space [in bytes]
  * @ Retval HAL_AUDIO_STATUS_OK if operation is successful, others if failed
  */
hal_audio_status_t hal_audio_get_stream_out_sample_count(uint32_t *sample_count)
{
    //ToDo: limit the scope -- treat it as local playback
    n9_dsp_share_info_t *p_info = hal_audio_query_playback_share_info();

    *sample_count = hal_audio_buf_mgm_get_free_byte_count(p_info);

    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Register the callback of stream out.
  * @ callback : callback function
  * @ user_data : pointer of user data
  * @ Retval HAL_AUDIO_STATUS_OK if operation is successful, others if failed
  */
hal_audio_status_t hal_audio_register_stream_out_callback(hal_audio_stream_out_callback_t callback, void *user_data)
{
    //ToDo: limit the scope -- treat it as local playback

    hal_audio_service_hook_callback(AUDIO_MESSAGE_TYPE_PLAYBACK, callback, user_data);

    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Updates the audio input frequency
  * @ sample_rate : audio frequency used to record the audio stream
  * @ This API should be called before hal_audio_start_stream_in() to adjust the audio frequency
  * @ Retval: HAL_AUDIO_STATUS_OK if operation is successful, others if sample rate is invalid
  */
hal_audio_status_t hal_audio_set_stream_in_sampling_rate(hal_audio_sampling_rate_t sampling_rate)
{
    //ToDo: extend the sampling rate from 8k/16kHz to 8k~48kHz

    switch (sampling_rate) {
        case HAL_AUDIO_SAMPLING_RATE_8KHZ:
        case HAL_AUDIO_SAMPLING_RATE_11_025KHZ:
        case HAL_AUDIO_SAMPLING_RATE_12KHZ:
        case HAL_AUDIO_SAMPLING_RATE_16KHZ:
        case HAL_AUDIO_SAMPLING_RATE_22_05KHZ:
        case HAL_AUDIO_SAMPLING_RATE_24KHZ:
        case HAL_AUDIO_SAMPLING_RATE_32KHZ:
        case HAL_AUDIO_SAMPLING_RATE_44_1KHZ:
        case HAL_AUDIO_SAMPLING_RATE_48KHZ:
            audio_common.stream_in.stream_sampling_rate = sampling_rate;
            return HAL_AUDIO_STATUS_OK;
        default:
            return HAL_AUDIO_STATUS_INVALID_PARAMETER;
    }
}

/**
  * @ Updates the audio input channel number
  * @ channel_number : audio channel mode to record the audio stream
  * @ This API should be called before hal_audio_start_stream_in() to adjust the input channel number
  * @ Retval: HAL_AUDIO_STATUS_OK if operation is successful, others if channel number is invalid
  */
hal_audio_status_t hal_audio_set_stream_in_channel_number(hal_audio_channel_number_t channel_number)
{
    switch (channel_number) {
        case HAL_AUDIO_MONO:
        case HAL_AUDIO_STEREO:
            audio_common.stream_in.stream_channel = channel_number;
            hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_SET_INPUT_DEVICE_CHANNEL, 0, channel_number, false);
            return HAL_AUDIO_STATUS_OK;
        default:
            return HAL_AUDIO_STATUS_INVALID_PARAMETER;
    }
}

/**
  * @ Start the recording of audio stream
  */
hal_audio_status_t hal_audio_start_stream_in(hal_audio_active_type_t active_type)
{
    //ToDo: limit the scope -- treat it as recording
    //audio_dsp_playback_info_t temp_param;
    void *p_param_share;
    bool is_running;
    audio_message_type_t msg_type = AUDIO_MESSAGE_TYPE_RECORD;

    is_running = hal_audio_status_query_running_flag(AUDIO_MESSAGE_TYPE_RECORD);
    if (is_running) {
        // Re-entry: don't allow multiple recording
        //log_hal_msgid_info("Re-entry\r\n", 0);
    } else {
        // Temp protect in APP level    //TODO record middleware.
        //hal_audio_status_set_running_flag(AUDIO_MESSAGE_TYPE_RECORD, true);
    }

    #if 0
    // Collect parameters
    temp_param.bit_type = HAL_AUDIO_BITS_PER_SAMPLING_16;
    temp_param.sampling_rate = audio_common.stream_in.stream_sampling_rate;
    temp_param.channel_number = audio_common.stream_in.stream_channel;
    temp_param.codec_type = 0;  //KH: should use AUDIO_DSP_CODEC_TYPE_PCM
    temp_param.p_share_info = hal_audio_query_record_share_info();

    // Open codec
    p_param_share = hal_audio_dsp_controller_put_paramter( &temp_param, sizeof(audio_dsp_playback_info_t));
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_OPEN, AUDIO_DSP_CODEC_TYPE_PCM, (uint32_t)p_param_share, false);

    // Start recording
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_START, 0, 0, true);
    #else
    // Open playback
    mcu2dsp_open_param_t open_param;

    // Collect parameters
    open_param.param.stream_in  = STREAM_IN_AFE;
    open_param.param.stream_out = STREAM_OUT_RECORD;
#if 0
    open_param.stream_in_param.afe.audio_device    = HAL_AUDIO_DEVICE_MAIN_MIC_DUAL;
    open_param.stream_in_param.afe.stream_channel  = HAL_AUDIO_DIRECT;
    open_param.stream_in_param.afe.audio_interface = HAL_AUDIO_INTERFACE_1;
    if(open_param.stream_in_param.afe.audio_device == HAL_AUDIO_DEVICE_I2S_MASTER) {
        open_param.stream_in_param.afe.misc_parms      = I2S_CLK_SOURCE_DCXO;
    } else {
        open_param.stream_in_param.afe.misc_parms      = MICBIAS_SOURCE_ALL | MICBIAS3V_OUTVOLTAGE_1p85v;
    }
#else
    hal_audio_get_stream_in_setting_config(AU_DSP_RECORD, &open_param.stream_in_param);
#endif

    /*User trigger FF*/
    if (g_stream_in_code_type == AUDIO_DSP_CODEC_TYPE_ANC_USER_TRIGGER_FF) {
#ifdef HAL_DVFS_MODULE_ENABLED
        dvfs_lock_control("AUDIO",DVFS_156M_SPEED, DVFS_LOCK);
        log_hal_msgid_info("[user_trigger_ff]frequency is risen to 1.3V", 0);
#endif
        open_param.stream_in_param.afe.audio_device = HAL_AUDIO_DEVICE_MAIN_MIC_DUAL;
        open_param.stream_in_param.afe.stream_channel  = HAL_AUDIO_DIRECT;
        open_param.stream_in_param.afe.audio_interface = HAL_AUDIO_INTERFACE_1;
        open_param.stream_in_param.afe.misc_parms      = MICBIAS_SOURCE_ALL | MICBIAS3V_OUTVOLTAGE_1p85v;

        open_param.stream_in_param.afe.memory          = HAL_AUDIO_MEM1;
        open_param.stream_in_param.afe.format          = AFE_PCM_FORMAT_S16_LE;
        open_param.stream_in_param.afe.sampling_rate   = 48000;
        open_param.stream_in_param.afe.irq_period      = 10;
        open_param.stream_in_param.afe.frame_size      = 480; // Warning: currently fixed @ 480 in DSP
        open_param.stream_in_param.afe.frame_number    = 2;
        open_param.stream_in_param.afe.hw_gain         = false;

        open_param.stream_out_param.record.p_share_info = (n9_dsp_share_info_t *)hal_audio_query_share_info(msg_type);
        open_param.stream_out_param.record.frames_per_message = 2; // DSP triggers CCNI message after collecting this value of frames
        open_param.stream_out_param.record.bitrate = g_bit_rate;
        open_param.stream_out_param.record.interleave = false;
        hal_audio_reset_share_info( open_param.stream_out_param.record.p_share_info );
        log_hal_msgid_info("[user_trigger_ff]hal_audio_start_stream_in, sr=%d, irq=%d, frame_size=%d, frame_num=%d, format=%d", 5,
                            open_param.stream_in_param.afe.sampling_rate,
                            open_param.stream_in_param.afe.irq_period,
                            open_param.stream_in_param.afe.frame_size,
                            open_param.stream_in_param.afe.frame_number,
                            open_param.stream_in_param.afe.format);

    /*Record*/
    } else {
        open_param.stream_in_param.afe.memory          = HAL_AUDIO_MEM1;
        open_param.stream_in_param.afe.format          = AFE_PCM_FORMAT_S16_LE;
        open_param.stream_in_param.afe.sampling_rate   = g_stream_in_sample_rate;
        open_param.stream_in_param.afe.irq_period      = 8;
#if(MTK_RECORD_INTERLEAVE_ENABLE)
        if (( open_param.stream_in_param.afe.audio_device != 0)&&(open_param.stream_in_param.afe.audio_device1 != 0)) {
            open_param.stream_in_param.afe.frame_size      = 128;
        } else {
            open_param.stream_in_param.afe.frame_size      = 256;
        }
#else
        open_param.stream_in_param.afe.frame_size      = 256; // Warning: currently fixed @ 480 in DSP
#endif
        open_param.stream_in_param.afe.frame_number    = 2;
        open_param.stream_in_param.afe.hw_gain         = false;

        open_param.stream_out_param.record.p_share_info = (n9_dsp_share_info_t *)hal_audio_query_share_info(msg_type);
        open_param.stream_out_param.record.frames_per_message = 4; // DSP triggers CCNI message after collecting this value of frames
        open_param.stream_out_param.record.bitrate = g_bit_rate;
#if(MTK_RECORD_INTERLEAVE_ENABLE)
        if (( open_param.stream_in_param.afe.audio_device != 0)&&(open_param.stream_in_param.afe.audio_device1 != 0)) {
            open_param.stream_out_param.record.interleave = true;
        } else {
            open_param.stream_out_param.record.interleave = false;
        }
#else
        open_param.stream_out_param.record.interleave = false;
#endif
        hal_audio_reset_share_info( open_param.stream_out_param.record.p_share_info );
    }

    p_param_share = hal_audio_dsp_controller_put_paramter( &open_param, sizeof(mcu2dsp_open_param_t), msg_type);

    //hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_OPEN, AUDIO_DSP_CODEC_TYPE_PCM, (uint32_t)p_param_share, true);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_OPEN, g_stream_in_code_type, (uint32_t)p_param_share, true);//modify for opus

    // Start playback
    mcu2dsp_start_param_t start_param;

    // Collect parameters
    start_param.param.stream_in     = STREAM_IN_AFE;
    start_param.param.stream_out    = STREAM_OUT_RECORD;

    start_param.stream_in_param.afe.aws_flag   =  false;

    p_param_share = hal_audio_dsp_controller_put_paramter( &start_param, sizeof(mcu2dsp_open_param_t), msg_type);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_START, 0, (uint32_t)p_param_share, true);
    #endif

    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Start the usb recording of audio stream
  */
hal_audio_status_t hal_audio_start_stream_in_usb(hal_audio_active_type_t active_type)
{
    //ToDo: limit the scope -- treat it as recording
    //audio_dsp_playback_info_t temp_param;
    void *p_param_share;
    bool is_running;
    audio_message_type_t msg_type = AUDIO_MESSAGE_TYPE_RECORD;

    is_running = hal_audio_status_query_running_flag(AUDIO_MESSAGE_TYPE_RECORD);
    if (is_running) {
        // Re-entry: don't allow multiple recording
        //log_hal_msgid_info("Re-entry\r\n", 0);
    } else {
        // Temp protect in APP level    //TODO record middleware.
        //hal_audio_status_set_running_flag(AUDIO_MESSAGE_TYPE_RECORD, true);
    }

    // Open playback
    mcu2dsp_open_param_t open_param;

    // Collect parameters
    open_param.param.stream_in  = STREAM_IN_AFE;
    open_param.param.stream_out = STREAM_OUT_RECORD;

    hal_audio_get_stream_in_setting_config(AU_DSP_RECORD, &open_param.stream_in_param);
    open_param.stream_in_param.afe.memory          = HAL_AUDIO_MEM1;
    open_param.stream_in_param.afe.format          = AFE_PCM_FORMAT_S16_LE;
    open_param.stream_in_param.afe.sampling_rate   = g_stream_in_sample_rate;
    open_param.stream_in_param.afe.irq_period      = 4;
#if(MTK_RECORD_INTERLEAVE_ENABLE)
    if (( open_param.stream_in_param.afe.audio_device != 0)&&(open_param.stream_in_param.afe.audio_device1 != 0)) {
        open_param.stream_in_param.afe.frame_size      = 64;
    } else {
        open_param.stream_in_param.afe.frame_size      = 256;
    }
#else
    open_param.stream_in_param.afe.frame_size      = 256; // Warning: currently fixed @ 480 in DSP
#endif
    open_param.stream_in_param.afe.frame_number    = 2;
    open_param.stream_in_param.afe.hw_gain         = false;

    open_param.stream_out_param.record.p_share_info = (n9_dsp_share_info_t *)hal_audio_query_share_info(msg_type);
    open_param.stream_out_param.record.frames_per_message = 2; // DSP triggers CCNI message after collecting this value of frames
    open_param.stream_out_param.record.bitrate = g_bit_rate;
#if(MTK_RECORD_INTERLEAVE_ENABLE)
    if (( open_param.stream_in_param.afe.audio_device != 0)&&(open_param.stream_in_param.afe.audio_device1 != 0)) {
        open_param.stream_out_param.record.interleave = true;
    } else {
        open_param.stream_out_param.record.interleave = false;
    }
#else
    open_param.stream_out_param.record.interleave = false;
#endif
    hal_audio_reset_share_info( open_param.stream_out_param.record.p_share_info );

    p_param_share = hal_audio_dsp_controller_put_paramter( &open_param, sizeof(mcu2dsp_open_param_t), msg_type);

    //hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_OPEN, AUDIO_DSP_CODEC_TYPE_PCM, (uint32_t)p_param_share, true);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_OPEN, g_stream_in_code_type, (uint32_t)p_param_share, true);//modify for opus

    // Start playback
    mcu2dsp_start_param_t start_param;

    // Collect parameters
    start_param.param.stream_in     = STREAM_IN_AFE;
    start_param.param.stream_out    = STREAM_OUT_RECORD;

    start_param.stream_in_param.afe.aws_flag   =  false;

    p_param_share = hal_audio_dsp_controller_put_paramter( &start_param, sizeof(mcu2dsp_open_param_t), msg_type);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_START, 0, (uint32_t)p_param_share, true);

    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Stop the recording of audio stream
  */
void hal_audio_stop_stream_in(void)
{
    //ToDo: limit the scope -- treat it as recording
    if(hal_audio_status_query_running_flag(AUDIO_MESSAGE_TYPE_RECORD)){
    // Stop recording
        hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_STOP, 0, 0, true);
        hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_CLOSE, 0, 0, true);

        // Temp protect in APP level    //TODO record middleware.
        //hal_audio_status_set_running_flag(AUDIO_MESSAGE_TYPE_RECORD, false);
    }else{
        log_hal_msgid_info("Recording was not existed.", 0);
    }

    if (g_stream_in_code_type == AUDIO_DSP_CODEC_TYPE_ANC_USER_TRIGGER_FF) {
        #ifdef HAL_DVFS_MODULE_ENABLED
        dvfs_lock_control("AUDIO",DVFS_156M_SPEED, DVFS_UNLOCK);
        log_hal_msgid_info("[user_trigger_ff]frequency is set back", 0);
        #endif
    }
}

#if defined(HAL_AUDIO_SUPPORT_MULTIPLE_MICROPHONE)
/**
  * @ Updates the audio input volume for multiple microphones.
  * @ volume_index0: input gain index 0
  * @ volume_index1: input gain index 1
  * @ gain_select  : select which pair of gain to be setting
  */
hal_audio_status_t hal_audio_set_stream_in_volume_for_multiple_microphone(uint32_t volume_index0, uint32_t volume_index1, hal_audio_input_gain_select_t gain_select)
{
    uint32_t data32;

    if (gain_select == HAL_AUDIO_INPUT_GAIN_SELECTION_D0_A0) {
        audio_common.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_0] = volume_index0;
        audio_common.stream_in.analog_gain_index[INPUT_ANALOG_GAIN_FOR_MIC_L] = volume_index1;
    } else if (gain_select == HAL_AUDIO_INPUT_GAIN_SELECTION_D0_D1) {
        audio_common.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_0] = volume_index0;
        audio_common.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_1] = volume_index1;
    } else if (gain_select == HAL_AUDIO_INPUT_GAIN_SELECTION_D2_D3) {
        audio_common.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_2] = volume_index0;
        audio_common.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_3] = volume_index1;
    } else if (gain_select == HAL_AUDIO_INPUT_GAIN_SELECTION_D4) {
        audio_common.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_ECHO_PATH] = volume_index0;
    } else if (gain_select == HAL_AUDIO_INPUT_GAIN_SELECTION_A0_A1) {
        audio_common.stream_in.analog_gain_index[INPUT_ANALOG_GAIN_FOR_MIC_L] = volume_index0;
        audio_common.stream_in.analog_gain_index[INPUT_ANALOG_GAIN_FOR_MIC_R] = volume_index1;
    }

    data32 = (volume_index1 <<16) | (volume_index0 & 0xFFFF);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_SET_INPUT_DEVICE_VOLUME, gain_select, data32, false);

    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Updates the audio input volume
  * @ digital_volume_index: digital gain index
  * @ analog_volume_index : analog gain index
  */
hal_audio_status_t hal_audio_set_stream_in_volume(uint32_t digital_volume_index, uint32_t analog_volume_index)

{
    uint32_t data32;

    audio_common.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_0] = digital_volume_index;
    audio_common.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_1] = digital_volume_index;
    audio_common.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_2] = digital_volume_index;
    audio_common.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_DEVICE_3] = digital_volume_index;
    audio_common.stream_in.digital_gain_index[INPUT_DIGITAL_GAIN_FOR_ECHO_PATH] = digital_volume_index;
    audio_common.stream_in.analog_gain_index[INPUT_ANALOG_GAIN_FOR_MIC_L] = analog_volume_index;
    audio_common.stream_in.analog_gain_index[INPUT_ANALOG_GAIN_FOR_MIC_R] = analog_volume_index;

    data32 = (analog_volume_index<<16) | (digital_volume_index & 0xFFFF);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_SET_INPUT_DEVICE_VOLUME, 0, data32, false);

    return HAL_AUDIO_STATUS_OK;
}
#else
/**
  * @ Updates the audio input volume
  * @ digital_volume_index: digital gain index
  * @ analog_volume_index : analog gain index
  */
hal_audio_status_t hal_audio_set_stream_in_volume(uint32_t digital_volume_index, uint32_t analog_volume_index)

{
    uint32_t data32;

    audio_common.stream_in.digital_gain_index = digital_volume_index;
    audio_common.stream_in.analog_gain_index = analog_volume_index;

    data32 = (analog_volume_index<<16) | (digital_volume_index & 0xFFFF);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_SET_INPUT_DEVICE_VOLUME, 0, data32, false);

    return HAL_AUDIO_STATUS_OK;
}
#endif

/**
  * @ Mute stream in path
  * @ mute: true -> set mute / false -> set unmute
  */
void hal_audio_mute_stream_in(bool mute)
{
    audio_common.stream_in.mute = mute;

    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_MUTE_INPUT_DEVICE, 0, mute, false);
}

/**
  * @ Control the audio input device
  * @ device: input device
  */
hal_audio_status_t hal_audio_set_stream_in_device(hal_audio_device_t device)
{
    audio_common.stream_in.audio_device = device;

    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_COMMON_SET_INPUT_DEVICE, 0, device, false);

    return HAL_AUDIO_STATUS_OK;
}


/**
  * @ Start audio recording
  * @ buffer: buffer pointer for the recorded data storing
  * @ size : number of audio data
  * @ Retval HAL_AUDIO_STATUS_OK if operation is successful, others if failed.
  */
hal_audio_status_t hal_audio_read_stream_in(void *buffer, uint32_t size)
{
    //ToDo: limit the scope -- treat it as recording
    n9_dsp_share_info_t *p_info = hal_audio_query_record_share_info();
    uint32_t data_byte_count;
    hal_audio_status_t result = HAL_AUDIO_STATUS_OK;
    uint32_t i;
    uint8_t *p_dest_buf = (uint8_t *)buffer;
    bool is_notify;

    // Check buffer
    if (buffer == NULL)
        return HAL_AUDIO_STATUS_ERROR;

    // Check data amount
    data_byte_count = hal_audio_buf_mgm_get_data_byte_count(p_info);
    if (size > data_byte_count) {
        return HAL_AUDIO_STATUS_ERROR;
    }

    // When buffer is enough
    for (i=0; (i<2) && size; i++) {
        uint8_t *p_source_buf;
        uint32_t buf_size, segment;

        hal_audio_buf_mgm_get_data_buffer(p_info, &p_source_buf, &buf_size);
        if (size >= buf_size) {
            segment = buf_size;
        } else {
            segment = size;
        }
        memcpy(p_dest_buf, p_source_buf, segment);
#ifdef HAL_AUDIO_SUPPORT_MULTIPLE_STREAM_OUT
        if(g_record_airdump){
            memcpy(p_dest_buf + segment, p_source_buf + p_info->length, segment);
        }
#endif
        hal_audio_buf_mgm_get_read_data_done(p_info, segment);
        p_dest_buf += segment;
        size -= segment;
    }

    if (p_info->bBufferIsFull)
    {
        p_info->bBufferIsFull = 0;
    }

    // Check status and notify DSP
    is_notify = hal_audio_status_query_notify_flag(AUDIO_MESSAGE_TYPE_RECORD);
    if (is_notify) {
        hal_audio_status_set_notify_flag(AUDIO_MESSAGE_TYPE_RECORD, false);
        hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_DATA_NOTIFY_ACK, 0, 0, false);
    }

    return result;
}

/**
  * @ Query the data amount of input stream.
  * @ sample_count : number of data [in bytes]
  * @ Retval HAL_AUDIO_STATUS_OK if operation is successful, others if failed
  */
hal_audio_status_t hal_audio_get_stream_in_sample_count(uint32_t *sample_count)
{
    //ToDo: limit the scope -- treat it as recording
    n9_dsp_share_info_t *p_info = hal_audio_query_record_share_info();

    *sample_count = hal_audio_buf_mgm_get_data_byte_count(p_info);

    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Register the callback of stream in.
  * @ callback : callback function
  * @ user_data : pointer of user data
  * @ Retval HAL_AUDIO_STATUS_OK if operation is successful, others if failed
  */
hal_audio_status_t hal_audio_register_stream_in_callback(hal_audio_stream_in_callback_t callback, void *user_data)
{
    //ToDo: limit the scope -- treat it as recording

    hal_audio_service_hook_callback(AUDIO_MESSAGE_TYPE_RECORD, callback, user_data);

    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Start the recording of audio stream for wearing condition detection
  */
hal_audio_status_t hal_audio_start_stream_in_leakage_compensation(void)
{
    mcu2dsp_open_param_t open_param;
    void *p_param_share;
    audio_message_type_t msg_type = AUDIO_MESSAGE_TYPE_RECORD;

    open_param.param.stream_in  = STREAM_IN_AFE;
    open_param.param.stream_out = STREAM_OUT_RECORD;


#ifdef HAL_DVFS_MODULE_ENABLED
    dvfs_lock_control("AUDIO",DVFS_156M_SPEED, HAL_DVFS_LOCK);
    log_hal_msgid_info("[RECORD_LC]frequency is risen to 1.3V", 0);
#endif

#if 1
    //AMIC mono R
//    open_param.stream_in_param.afe.audio_device = HAL_AUDIO_DEVICE_MAIN_MIC_DUAL;
    hal_audio_get_stream_in_setting_config(AU_DSP_ANC, &open_param.stream_in_param);
    open_param.stream_in_param.afe.audio_device = /*open_param.stream_in_param.afe.audio_device |*/ open_param.stream_in_param.afe.audio_device1;

    open_param.stream_in_param.afe.stream_channel  = HAL_AUDIO_DIRECT;
    open_param.stream_in_param.afe.memory          = HAL_AUDIO_MEM1 | HAL_AUDIO_MEM3;//HAL_AUDIO_MEM3 to enable echo referencr
    open_param.stream_in_param.afe.audio_interface = HAL_AUDIO_INTERFACE_1;
    open_param.stream_in_param.afe.misc_parms      = MICBIAS_SOURCE_ALL | MICBIAS3V_OUTVOLTAGE_1p85v;
#else
    //setting from nvkey
    hal_audio_get_stream_in_setting_config(AU_DSP_VOICE, &open_param.stream_in_param.afe.audio_device, &open_param.stream_in_param.afe.stream_channel, &open_param.stream_in_param.afe.audio_interface);
    if(open_param.stream_in_param.afe.audio_device == HAL_AUDIO_DEVICE_I2S_MASTER) {
        open_param.stream_in_param.afe.misc_parms      = I2S_CLK_SOURCE_DCXO;
    } else {
        open_param.stream_in_param.afe.misc_parms      = MICBIAS_SOURCE_ALL | MICBIAS3V_OUTVOLTAGE_1p85v;
    }
#endif
    open_param.stream_in_param.afe.format          = AFE_PCM_FORMAT_S16_LE;
    open_param.stream_in_param.afe.sampling_rate   = g_stream_in_sample_rate;
    open_param.stream_in_param.afe.irq_period      = 10;
    open_param.stream_in_param.afe.frame_size      = 160; // Warning: currently fixed @ 480 in DSP
    log_hal_msgid_info("[RECORD_LC]hal_audio_start_stream_in_leakage_compensation, device:%d, device1:%d, set frame size:%d\r\n", 2, open_param.stream_in_param.afe.audio_device, open_param.stream_in_param.afe.audio_device1, open_param.stream_in_param.afe.frame_size);
    open_param.stream_in_param.afe.frame_number    = 4;
    open_param.stream_in_param.afe.hw_gain         = false;

    open_param.stream_out_param.record.p_share_info = (n9_dsp_share_info_t *)hal_audio_query_share_info(msg_type);
    open_param.stream_out_param.record.frames_per_message = 4; // DSP triggers CCNI message after collecting this value of frames
    open_param.stream_out_param.record.bitrate = g_bit_rate;
    hal_audio_reset_share_info( open_param.stream_out_param.record.p_share_info );

    p_param_share = hal_audio_dsp_controller_put_paramter( &open_param, sizeof(mcu2dsp_open_param_t), AUDIO_MESSAGE_TYPE_RECORD);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_OPEN, g_stream_in_code_type, (uint32_t)p_param_share, true);

    // Start playback
    mcu2dsp_start_param_t start_param;

    // Collect parameters
    start_param.param.stream_in     = STREAM_IN_AFE;
    start_param.param.stream_out    = STREAM_OUT_RECORD;

    start_param.stream_in_param.afe.aws_flag   =  false;

    p_param_share = hal_audio_dsp_controller_put_paramter( &start_param, sizeof(mcu2dsp_open_param_t), AUDIO_MESSAGE_TYPE_RECORD);
    hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_START, 0, (uint32_t)p_param_share, true);

    return HAL_AUDIO_STATUS_OK;
}

/**
  * @ Stop the recording of audio stream for wearing condition detection
  */
void hal_audio_stop_stream_in_leakage_compensation(void)
{
    //ToDo: limit the scope -- treat it as recording
    if(hal_audio_status_query_running_flag(AUDIO_MESSAGE_TYPE_RECORD)){
    // Stop recording
        hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_STOP, 0, 0, true);
        hal_audio_dsp_controller_send_message(MSG_MCU2DSP_RECORD_CLOSE, 0, 0, true);

        // Temp protect in APP level    //TODO record middleware.
        //hal_audio_status_set_running_flag(AUDIO_MESSAGE_TYPE_RECORD, false);

#ifdef HAL_DVFS_MODULE_ENABLED
        dvfs_lock_control("AUDIO",DVFS_156M_SPEED, DVFS_UNLOCK);
        log_hal_msgid_info("[RECORD_LC]frequency is set back", 0);
#endif
    }else{
        log_hal_msgid_info("Recording was not existed.", 0);
    }
}

/**
 * @ Query the size of needed memory to be allocated for internal use in audio driver
 * @ memory_size : the amount of memory required by the audio driver for an internal use (in bytes).
 * @ Retval HAL_AUDIO_STATUS_OK if operation is successful, others if failed.
 */
hal_audio_status_t hal_audio_get_memory_size(uint32_t *memory_size)
{
    //ToDo: assume that we don't ennd extra memory
    *memory_size = 0;

    return HAL_AUDIO_STATUS_OK;
}

/**
 * @ Hand over allocated memory to audio driver
 * @ memory : the pointer to an allocated memory. It should be 4 bytes aligned.
 * @ Retval HAL_AUDIO_STATUS_OK if operation is successful, others if failed.
 */
hal_audio_status_t hal_audio_set_memory(void *memory)
{
    audio_common.allocated_memory = memory;

    return HAL_AUDIO_STATUS_OK;
}

/**
 * @ Get audio clock.
 * @ sample_count : a pointer to the accumulated audio sample count.
 * @ Retval HAL_AUDIO_STATUS_OK if operation is successful, others if failed.
 */
hal_audio_status_t hal_audio_get_audio_clock(uint32_t *sample_count)
{
    //ToDo: currently, use fake function.
    *sample_count = 0;

    return HAL_AUDIO_STATUS_OK;
}

#ifdef HAL_AUDIO_SUPPORT_MULTIPLE_STREAM_OUT
uint32_t hal_audio_sampling_rate_enum_to_value(hal_audio_sampling_rate_t hal_audio_sampling_rate_enum)
{
    switch (hal_audio_sampling_rate_enum) {
        case HAL_AUDIO_SAMPLING_RATE_8KHZ:
            return   8000;
        case HAL_AUDIO_SAMPLING_RATE_11_025KHZ:
            return  11025;
        case HAL_AUDIO_SAMPLING_RATE_12KHZ:
            return  12000;
        case HAL_AUDIO_SAMPLING_RATE_16KHZ:
            return  16000;
        case HAL_AUDIO_SAMPLING_RATE_22_05KHZ:
            return  22050;
        case HAL_AUDIO_SAMPLING_RATE_24KHZ:
            return  24000;
        case HAL_AUDIO_SAMPLING_RATE_32KHZ:
            return  32000;
        case HAL_AUDIO_SAMPLING_RATE_44_1KHZ:
            return  44100;
        case HAL_AUDIO_SAMPLING_RATE_48KHZ:
            return  48000;
        case HAL_AUDIO_SAMPLING_RATE_88_2KHZ:
            return  88200;
        case HAL_AUDIO_SAMPLING_RATE_96KHZ:
            return  96000;
        case HAL_AUDIO_SAMPLING_RATE_176_4KHZ:
            return 176400;
        case HAL_AUDIO_SAMPLING_RATE_192KHZ:
            return 192000;

        default:
            return 8000;
    }
}

int32_t hal_audio_get_device_out_supported_frequency(hal_audio_device_t audio_out_device,hal_audio_sampling_rate_t freq)
{
    int32_t device_supported_frequency = -1;
    int32_t i = freq;

    switch (audio_out_device){
        case HAL_AUDIO_DEVICE_DAC_L:
        case HAL_AUDIO_DEVICE_DAC_R:
        case HAL_AUDIO_DEVICE_DAC_DUAL:
            while(1){
                if (i > HAL_AUDIO_SAMPLING_RATE_MAX){
                    device_supported_frequency = -1;
                    break;
                }
                if(supported_SR_audio_dac_out[i] == true){
                    device_supported_frequency = i;
                    break;
                }
                i++;
            }
            break;
        case HAL_AUDIO_DEVICE_I2S_MASTER:
        case HAL_AUDIO_DEVICE_I2S_SLAVE:
            while(1){
                if (i > HAL_AUDIO_SAMPLING_RATE_MAX){
                    device_supported_frequency = -1;
                    break;
                }
                if(supported_SR_audio_i2s_inout[i] == true){
                    device_supported_frequency = i;
                    break;
                }
                i++;
            }
            break;
        default:
            break;
    }

    if(device_supported_frequency == -1){
        switch(i-1){
            case HAL_AUDIO_SAMPLING_RATE_88_2KHZ:
                device_supported_frequency = HAL_AUDIO_SAMPLING_RATE_96KHZ;
            break;
            case HAL_AUDIO_SAMPLING_RATE_176_4KHZ:
                device_supported_frequency = HAL_AUDIO_SAMPLING_RATE_192KHZ;
            break;
            default:
                log_hal_msgid_warning("Not found AFE supported rate", 0);
            break;
        }

    }
    return device_supported_frequency;
}

static hal_audio_device_t hal_audio_convert_mic_config(uint8_t Mic_NVkey){
    hal_audio_device_t device;

    /*5 bit for mic config*/
    switch(Mic_NVkey&0x1f){
        case 0x00:{ //----Analog Mic L
            device = HAL_AUDIO_DEVICE_MAIN_MIC_L;
            break;
        }
        case 0x01:{ //----Analog Mic L
            device = HAL_AUDIO_DEVICE_MAIN_MIC_R;
            break;
        }
        case 0x02:{ //----Digital Mic_0 L
            device = HAL_AUDIO_DEVICE_DIGITAL_MIC_L;
            break;
        }
        case 0x03:{ //----Digital Mic_0 R
            device = HAL_AUDIO_DEVICE_DIGITAL_MIC_R;
            break;
        }
        case 0x04:{ //----Digital Mic_1 L
            device = HAL_AUDIO_DEVICE_DIGITAL_MIC_L;
            break;
        }
        case 0x05:{ //----Digital Mic_1 R
            device = HAL_AUDIO_DEVICE_DIGITAL_MIC_R;
            break;
        }
        case 0x06:{ //---- I2S master
            device = HAL_AUDIO_DEVICE_I2S_MASTER;
            break;
        }
        case 0x07:{ //---- I2S slave
            device = HAL_AUDIO_DEVICE_I2S_SLAVE;
            break;
        }
        default:
            device = HAL_AUDIO_DEVICE_NONE;
            break;
    }
    return device;
}

#ifdef MTK_CONFIG_MIC_INTERFACE_ENABLE
static hal_audio_interface_t hal_audio_convert_mic_interface_ex(uint8_t Mic_NVkey)
{
    hal_audio_interface_t audio_interface;

    /*3 bit for mic interface*/
    switch((Mic_NVkey&0xe0) >> 5){
        case 0x00:
            audio_interface = HAL_AUDIO_INTERFACE_1;
            break;
        case 0x01:
            audio_interface = HAL_AUDIO_INTERFACE_2;
            break;
        case 0x02:
            audio_interface = HAL_AUDIO_INTERFACE_3;
            break;
        case 0x03:
            audio_interface = HAL_AUDIO_INTERFACE_4;
            break;
        default:
            audio_interface = HAL_AUDIO_INTERFACE_NONE;
    }

    return audio_interface;
}
#endif

static hal_audio_interface_t hal_audio_convert_mic_interface(hal_audio_device_t device){
    hal_audio_interface_t audio_interface;
    switch(device){
      case HAL_AUDIO_DEVICE_MAIN_MIC_L:
      case HAL_AUDIO_DEVICE_MAIN_MIC_R:{
          audio_interface = HAL_AUDIO_INTERFACE_1;
          break;
      }
      case HAL_AUDIO_DEVICE_DIGITAL_MIC_L:
      case HAL_AUDIO_DEVICE_DIGITAL_MIC_R:{
          audio_interface = HAL_AUDIO_INTERFACE_2;
          break;
      }
      default:
          audio_interface = HAL_AUDIO_INTERFACE_1;
          break;
    }
    return audio_interface;
}

static MICBIAS3V_OUTVOLTAGE_TYPE hal_audio_convert_micbias_level_config(uint8_t Level_NVkey){
    MICBIAS3V_OUTVOLTAGE_TYPE Level;
    switch(Level_NVkey){
        case 0x00:{ //----1.8V
            Level = MICBIAS3V_OUTVOLTAGE_1p8v;
            break;
        }
        case 0x01:{ //----1.85V
            Level = MICBIAS3V_OUTVOLTAGE_1p85v;
            break;
        }
        case 0x02:{ //----1.9V
            Level = MICBIAS3V_OUTVOLTAGE_1p9v;
            break;
        }
        case 0x03:{ //----2.0V
            Level = MICBIAS3V_OUTVOLTAGE_2p0v;
            break;
        }
        case 0x04:{ //----2.1V
            Level = MICBIAS3V_OUTVOLTAGE_2p1v;
            break;
        }
        case 0x05:{ //----2.2V
            Level = MICBIAS3V_OUTVOLTAGE_2p2v;
            break;
        }
        case 0x06:{ //----2.4V
            Level = MICBIAS3V_OUTVOLTAGE_2p4v;
            break;
        }
        case 0x07:{ //----bypass
            Level = MICBIAS3V_OUTVOLTAGE_VCC;
            break;
        }
        default:
            Level = MICBIAS3V_OUTVOLTAGE_1p85v;
            break;
    }
    return Level;
}

hal_audio_status_t hal_audio_get_stream_in_setting_config(audio_scenario_sel_t Audio_or_Voice, mcu2dsp_open_stream_in_param_t *stream_in_open_param)
{
    bool Extended_Mic_Config_Flag = 0;
    hal_audio_device_t *Main_MIC                = &stream_in_open_param->afe.audio_device;
    hal_audio_device_t *Ref_MIC                 = &stream_in_open_param->afe.audio_device1;
    hal_audio_device_t *Ref_MIC_2               = &stream_in_open_param->afe.audio_device2;
    hal_audio_device_t *Ref_MIC_3            = &stream_in_open_param->afe.audio_device3;
    hal_audio_channel_selection_t *MemInterface = &stream_in_open_param->afe.stream_channel;
    hal_audio_interface_t *audio_interface      = &stream_in_open_param->afe.audio_interface;
#ifdef ENABLE_2A2D_TEST
    hal_audio_interface_t *audio_interface1     = &stream_in_open_param->afe.audio_interface1;
    hal_audio_interface_t *audio_interface2     = &stream_in_open_param->afe.audio_interface2;
    hal_audio_interface_t *audio_interface3     = &stream_in_open_param->afe.audio_interface3;
#endif
    uint32_t *misc_param = &stream_in_open_param->afe.misc_parms;
    hal_gpio_status_t status;
    hal_gpio_data_t channel_gpio_data = HAL_GPIO_DATA_LOW;
    uint8_t channel_temp;
    *misc_param = 0;
    *Ref_MIC      = HAL_AUDIO_DEVICE_NONE;
    *Ref_MIC_2    = HAL_AUDIO_DEVICE_NONE;
    *Ref_MIC_3 = HAL_AUDIO_DEVICE_NONE;

    /*Audio HW I/O Configure setting*/
    if(Audio_or_Voice == AU_DSP_VOICE){ //0:Audio, 1:Voice
        switch( (audio_nvdm_HW_config.Voice_InputDev & NVKEY_INDEX1) >> 6){
            case 0x03:{
                Extended_Mic_Config_Flag = true;
                break;
            }
            case 0x02:{ //I2S_Master_In
                *Main_MIC = HAL_AUDIO_DEVICE_I2S_MASTER;
                break;
            }
            case 0x01:{ //Digital Mic
                if((audio_nvdm_HW_config.Voice_InputDev & NVKEY_INDEX4) == 0x02){
                    *Main_MIC = HAL_AUDIO_DEVICE_DIGITAL_MIC_DUAL;
                } else if ((audio_nvdm_HW_config.Voice_InputDev & NVKEY_INDEX4) == 0x01){
                    *Main_MIC = HAL_AUDIO_DEVICE_DIGITAL_MIC_R;
                } else if ((audio_nvdm_HW_config.Voice_InputDev & NVKEY_INDEX4) == 0x00){
                    *Main_MIC = HAL_AUDIO_DEVICE_DIGITAL_MIC_L;
                }
                break;
            }
            case 0x00:{ //Analog Mic
                if(((audio_nvdm_HW_config.Voice_InputDev & NVKEY_INDEX3) >> 2) == 0x02){
                    *Main_MIC = HAL_AUDIO_DEVICE_MAIN_MIC_DUAL;
                } else if (((audio_nvdm_HW_config.Voice_InputDev & NVKEY_INDEX3) >> 2) == 0x01){
                    *Main_MIC = HAL_AUDIO_DEVICE_MAIN_MIC_R;
                } else if (((audio_nvdm_HW_config.Voice_InputDev & NVKEY_INDEX3) >> 2) == 0x00){
                    *Main_MIC = HAL_AUDIO_DEVICE_MAIN_MIC_L;
                }
                break;
            }
            default:
                log_hal_msgid_info("[HAL]Get Voice Stream in Main_MIC error. Defualt", 0);
                *Main_MIC = HAL_AUDIO_DEVICE_MAIN_MIC_L;
                break;
        }
        if(Extended_Mic_Config_Flag){
            *Main_MIC        = hal_audio_convert_mic_config(audio_nvdm_HW_config.MIC_Select_Main);
            *Ref_MIC         = hal_audio_convert_mic_config(audio_nvdm_HW_config.MIC_Select_Ref);
            *Ref_MIC_2       = hal_audio_convert_mic_config(audio_nvdm_HW_config.MIC_Select_Ref_2);
            *Ref_MIC_3    = hal_audio_convert_mic_config(audio_nvdm_HW_config.MIC_Select_Ref_3);
        #ifdef ENABLE_2A2D_TEST
            #ifdef MTK_CONFIG_MIC_INTERFACE_ENABLE
                *audio_interface  = hal_audio_convert_mic_interface_ex(audio_nvdm_HW_config.MIC_Select_Main);
                *audio_interface1 = hal_audio_convert_mic_interface_ex(audio_nvdm_HW_config.MIC_Select_Ref);
                *audio_interface2 = hal_audio_convert_mic_interface_ex(audio_nvdm_HW_config.MIC_Select_Ref_2);
                *audio_interface3 = hal_audio_convert_mic_interface_ex(audio_nvdm_HW_config.MIC_Select_Ref_3);
            #else
                *audio_interface  = hal_audio_convert_mic_interface(*Main_MIC);
                *audio_interface1 = hal_audio_convert_mic_interface(*Ref_MIC);
                *audio_interface2 = hal_audio_convert_mic_interface(*Ref_MIC_2);
                *audio_interface3 = hal_audio_convert_mic_interface(*Ref_MIC_3);
            #endif
        #endif
        } else {
            *audio_interface  = HAL_AUDIO_INTERFACE_1;
        }
        //----Default I2S CLK Source
        if(*Main_MIC == HAL_AUDIO_DEVICE_I2S_MASTER){
            #ifndef MTK_CONFIG_MIC_INTERFACE_ENABLE
                *audio_interface = 1 << ((audio_nvdm_HW_config.Voice_InputDev & NVKEY_INDEX2) >> 4);
            #endif
            if(SW_version == nvdm_version){
                switch((audio_nvdm_HW_config.I2S_PARA_Voice & NVKEY_INDEX1) >> 6){
                    case 0x1:{
                        *misc_param |= I2S_CLK_SOURCE_APLL;
                        break;
                    }
                    case 0x0:
                    default:
                        *misc_param |= I2S_CLK_SOURCE_DCXO;
                        break;
                }
            } else {
                *misc_param |= I2S_CLK_SOURCE_DCXO;
            }
        }
        //----AMIC ACC DCC Setting
        switch(audio_nvdm_HW_config.UL_AFE_PARA & 0x02){
            case 0x02:{  //----AMIC ACC mode
                *misc_param |= AMIC_ACC;
                log_hal_msgid_info("[HAL]Get Voice Stream in AMIC change to ACC mode", 0);
                break;
            }
            case 0x00:  //----AMIC DCC mode
            default:
                if(SW_version == nvdm_version){
                /*VIN0 PU/PD, VIN1 PU/PD, Only setting in AMIC DCC*/
                    if(((audio_nvdm_HW_config.AMIC_PARA & NVKEY_INDEX1) >> 6) == 0x00){
                        //MEMS,
                        *misc_param |= MICBIAS0_AMIC_MEMS;
                    } else if(((audio_nvdm_HW_config.AMIC_PARA & NVKEY_INDEX1) >> 6) == 0x01){
                        //ECM Differential
                        *misc_param |= MICBIAS0_AMIC_ECM_DIFFERENTIAL;
                    } else if(((audio_nvdm_HW_config.AMIC_PARA & NVKEY_INDEX1) >> 6) == 0x02){
                        //ECM Single
                        *misc_param |= MICBIAS0_AMIC_ECM_SINGLE;
                    } else {
                        log_hal_msgid_warning("[HAL]Get Voice Stream in, Not found supported VIN0 PU/PD", 0);
                    }
                    if(((audio_nvdm_HW_config.AMIC_PARA & NVKEY_INDEX2) >> 4) == 0x00){
                        //MEMS,
                        *misc_param |= MICBIAS1_AMIC_MEMS;
                    } else if(((audio_nvdm_HW_config.AMIC_PARA & NVKEY_INDEX2) >> 4) == 0x01){
                        //ECM Differential
                        *misc_param |= MICBIAS1_AMIC_ECM_DIFFERENTIAL;
                    } else if(((audio_nvdm_HW_config.AMIC_PARA & NVKEY_INDEX2) >> 4) == 0x02){
                        //ECM Single
                        *misc_param |= MICBIAS1_AMIC_ECM_SINGLE;
                    } else {
                        log_hal_msgid_warning("[HAL]Get Voice Stream in, Not found supported VIN1 PU/PD", 0);
                    }
                }else{
                    *misc_param |= MICBIAS0_AMIC_MEMS;
                    *misc_param |= MICBIAS1_AMIC_MEMS;
                }
                *misc_param |= AMIC_DCC;
                break;
        }
        //----MICBIAS Enable, Level
        if(SW_version == nvdm_version){
            switch((audio_nvdm_HW_config.MICBIAS_PARA_Voice & (NVKEY_INDEX1|NVKEY_INDEX2)) >> 4){
                case 0x5:{
                    *misc_param |= MICBIAS_SOURCE_ALL;
                    break;
                }
                case 0x1:{
                    *misc_param |= MICBIAS_SOURCE_1;
                    break;
                }
                case 0x4:
                default:
                    *misc_param |= MICBIAS_SOURCE_0;
                    break;
            }
            *misc_param |= hal_audio_convert_micbias_level_config(audio_nvdm_HW_config.MICBIAS_PARA_Voice & (NVKEY_INDEX3|NVKEY_INDEX4));
        }else{
            *misc_param |= MICBIAS_SOURCE_ALL;
            *misc_param |= MICBIAS3V_OUTVOLTAGE_1p85v;
        }
    } else if(Audio_or_Voice == AU_DSP_AUDIO){
        //----MICBIAS Enable, Level
        if(SW_version == nvdm_version){
            switch((audio_nvdm_HW_config.MICBIAS_PARA_Audio& (NVKEY_INDEX1|NVKEY_INDEX2)) >> 4){
                case 0x5:{
                    *misc_param |= MICBIAS_SOURCE_ALL;
                    break;
                }
                case 0x1:{
                    *misc_param |= MICBIAS_SOURCE_1;
                    break;
                }
                case 0x4:
                default:
                    *misc_param |= MICBIAS_SOURCE_0;
                    break;
            }
            *misc_param |= hal_audio_convert_micbias_level_config(audio_nvdm_HW_config.MICBIAS_PARA_Audio & (NVKEY_INDEX3|NVKEY_INDEX4));
        }else{
            *misc_param |= MICBIAS_SOURCE_ALL;
            *misc_param |= MICBIAS3V_OUTVOLTAGE_2p4v;
        }
        log_hal_msgid_info("Get Audio Stream in Device error.", 0);
    } else if (Audio_or_Voice == AU_DSP_ANC) {
        *Main_MIC = hal_audio_convert_mic_config(audio_nvdm_HW_config.MIC_Select_ANC_FF);
        *Ref_MIC = hal_audio_convert_mic_config(audio_nvdm_HW_config.MIC_Select_ANC_FB);
        *audio_interface  = hal_audio_convert_mic_interface(*Main_MIC);
#ifdef ENABLE_2A2D_TEST
        *audio_interface1 = hal_audio_convert_mic_interface(*Ref_MIC);
#endif
        //----MICBIAS Enable, Level
        if(SW_version == nvdm_version){
            switch((audio_nvdm_HW_config.MICBIAS_PARA_Voice & (NVKEY_INDEX1|NVKEY_INDEX2)) >> 4){
                case 0x5:{
                    *misc_param |= MICBIAS_SOURCE_ALL;
                    break;
                }
                case 0x1:{
                    *misc_param |= MICBIAS_SOURCE_1;
                    break;
                }
                case 0x4:
                default:
                    *misc_param |= MICBIAS_SOURCE_0;
                    break;
            }
            *misc_param |= hal_audio_convert_micbias_level_config(audio_nvdm_HW_config.MICBIAS_PARA_Voice & (NVKEY_INDEX3|NVKEY_INDEX4));
        }else{
            *misc_param |= MICBIAS_SOURCE_ALL;
            *misc_param |= MICBIAS3V_OUTVOLTAGE_1p85v;
        }
    } else if (Audio_or_Voice == AU_DSP_RECORD) {
        *Main_MIC = hal_audio_convert_mic_config(audio_nvdm_HW_config.MIC_Select_Record_Main);
        *Ref_MIC = hal_audio_convert_mic_config(audio_nvdm_HW_config.MIC_Select_Record_Ref);
        #ifdef MTK_CONFIG_MIC_INTERFACE_ENABLE
            *audio_interface  = hal_audio_convert_mic_interface_ex(audio_nvdm_HW_config.MIC_Select_Record_Main);
        #else
            *audio_interface  = hal_audio_convert_mic_interface(*Main_MIC);
        #endif
#ifdef ENABLE_2A2D_TEST
        #ifdef MTK_CONFIG_MIC_INTERFACE_ENABLE
            *audio_interface  = hal_audio_convert_mic_interface_ex(audio_nvdm_HW_config.MIC_Select_Record_Ref);
        #else
            *audio_interface1 = hal_audio_convert_mic_interface(*Ref_MIC);
        #endif
#else
        if((*Main_MIC + *Ref_MIC) == HAL_AUDIO_DEVICE_MAIN_MIC_DUAL){
            *Main_MIC = HAL_AUDIO_DEVICE_MAIN_MIC_DUAL;
        } else if ((*Main_MIC + *Ref_MIC) == HAL_AUDIO_DEVICE_DIGITAL_MIC_DUAL){
            *Main_MIC = HAL_AUDIO_DEVICE_DIGITAL_MIC_DUAL;
        }
#endif
        //----MICBIAS Enable, Level
        if(SW_version == nvdm_version){
            switch((audio_nvdm_HW_config.MICBIAS_PARA_Record & (NVKEY_INDEX1|NVKEY_INDEX2)) >> 4){
                case 0x5:{
                    *misc_param |= MICBIAS_SOURCE_ALL;
                    break;
                }
                case 0x1:{
                    *misc_param |= MICBIAS_SOURCE_1;
                    break;
                }
                case 0x4:
                default:
                    *misc_param |= MICBIAS_SOURCE_0;
                    break;
            }
            *misc_param |= hal_audio_convert_micbias_level_config(audio_nvdm_HW_config.MICBIAS_PARA_Record & (NVKEY_INDEX3|NVKEY_INDEX4));
        }else{
            *misc_param |= MICBIAS_SOURCE_ALL;
            *misc_param |= MICBIAS3V_OUTVOLTAGE_1p85v;
        }
    } else {
        log_hal_msgid_info("hal_audio_get_stream_in_setting_config scenario_sel error.", 0);
    }
    //----AMIC ACC DCC Setting
    //if((Audio_or_Voice == AU_DSP_VOICE) || (Audio_or_Voice == AU_DSP_RECORD)){
        switch(audio_nvdm_HW_config.UL_AFE_PARA & 0x02){
            case 0x02:{  //----AMIC ACC mode
                *misc_param |= AMIC_ACC;
                log_hal_msgid_info("[HAL]Get Stream in AMIC change to ACC mode", 0);
                break;
            }
            case 0x00:  //----AMIC DCC mode
            default:
                if(SW_version == nvdm_version){
                    /*VIN0 PU/PD, VIN1 PU/PD, Only setting in AMIC DCC*/
                    if(((audio_nvdm_HW_config.AMIC_PARA & NVKEY_INDEX1) >> 6) == 0x0){
                        //MEMS,
                        *misc_param |= MICBIAS0_AMIC_MEMS;
                    } else if(((audio_nvdm_HW_config.AMIC_PARA & NVKEY_INDEX1) >> 6) == 0x1){
                        //ECM Differential
                        *misc_param |= MICBIAS0_AMIC_ECM_DIFFERENTIAL;
                    } else if(((audio_nvdm_HW_config.AMIC_PARA & NVKEY_INDEX1) >> 6) == 0x2){
                        //ECM Single
                        *misc_param |= MICBIAS0_AMIC_ECM_SINGLE;
                    } else {
                        log_hal_msgid_warning("[HAL]Get Stream in, Not found supported VIN0 PU/PD", 0);
                    }
                    if(((audio_nvdm_HW_config.AMIC_PARA & NVKEY_INDEX2) >> 4) == 0x0){
                        //MEMS,
                        *misc_param |= MICBIAS1_AMIC_MEMS;
                    } else if(((audio_nvdm_HW_config.AMIC_PARA & NVKEY_INDEX2) >> 4) == 0x1){
                        //ECM Differential
                        *misc_param |= MICBIAS1_AMIC_ECM_DIFFERENTIAL;
                    } else if(((audio_nvdm_HW_config.AMIC_PARA & NVKEY_INDEX2) >> 4) == 0x2){
                        //ECM Single
                        *misc_param |= MICBIAS1_AMIC_ECM_SINGLE;
                    } else {
                        log_hal_msgid_warning("[HAL]Get Stream in, Not found supported VIN1 PU/PD", 0);
                    }
                }else{
                    *misc_param |= MICBIAS0_AMIC_MEMS;
                    *misc_param |= MICBIAS1_AMIC_MEMS;
                }
                *misc_param |= AMIC_DCC;
                break;
        }
    //}
    //----ADC, DAC Configure (NM HP mode)
    switch(audio_nvdm_HW_config.UL_AFE_PARA & 0x01){
        case 0x01:{  //----ADC HP
            *misc_param |= UPLINK_PERFORMANCE_HIGH;
            log_hal_msgid_info("UL ADC change to HP mode", 0);
            break;
        }
        case 0x00:   //----ADC NM
        default:
            *misc_param |= UPLINK_PERFORMANCE_NORMAL;
            break;
    }

    /*Audio Channel selection setting*/
    if(audio_Channel_Select.modeForAudioChannel){
        //----HW_mode
        status = hal_gpio_get_input((hal_gpio_pin_t)audio_Channel_Select.hwAudioChannel.gpioIndex, &channel_gpio_data);
        if (status == HAL_GPIO_STATUS_OK) {
            if (channel_gpio_data == HAL_GPIO_DATA_HIGH) {
                channel_temp = (audio_Channel_Select.hwAudioChannel.audioChannelGPIOH & (NVKEY_INDEX1|NVKEY_INDEX2)) >> 4;
            } else {
                channel_temp = (audio_Channel_Select.hwAudioChannel.audioChannelGPIOL & (NVKEY_INDEX1|NVKEY_INDEX2)) >> 4;
            }
        } else {
            channel_temp = AU_DSP_CH_LR; //default.
            log_hal_msgid_info("Get Stream in channel setting false with HW_mode.", 0);
        }
    } else {
        channel_temp = (audio_Channel_Select.audioChannel & (NVKEY_INDEX1|NVKEY_INDEX2)) >> 4;
    }
    switch(channel_temp)
    {
        case AU_DSP_CH_LR: {
            *MemInterface = HAL_AUDIO_DIRECT;
            break;
        }
        case AU_DSP_CH_L: {
            *MemInterface = HAL_AUDIO_BOTH_L;
            break;
        }
        case AU_DSP_CH_R: {
            *MemInterface = HAL_AUDIO_BOTH_R;
            break;
        }
        case AU_DSP_CH_SWAP: {
            *MemInterface = HAL_AUDIO_SWAP_L_R;
            break;
        }
        case AU_DSP_CH_MIX: {
            *MemInterface = HAL_AUDIO_MIX_L_R;
            break;
        }
        case AU_DSP_CH_MIX_SHIFT: {
            *MemInterface = HAL_AUDIO_MIX_SHIFT_L_R;
            break;
        }
        default: {
            *MemInterface = HAL_AUDIO_DIRECT;
            break;
        }
    }
    //For debug
    //log_hal_msgid_info("Get Stream in Main_MIC(%d), Ref1_MIC(%d), Ref2_MIC(%d), Reserve_MIC(%d), ch(%d) memory(%d) audio_inter(%d) misc_parms(%d)",
    //                    7, *Main_MIC, *Ref_MIC, *Ref_MIC_2, *Ref_MIC_3, *MemInterface, *i2s_interface, *misc_param);
    return HAL_AUDIO_STATUS_OK;
}

hal_audio_status_t hal_audio_get_stream_out_setting_config(audio_scenario_sel_t Audio_or_Voice, mcu2dsp_open_stream_out_param_t *stream_out_open_param)
{
    hal_audio_device_t *Device                  = &stream_out_open_param->afe.audio_device;
    stream_out_open_param->afe.audio_device1 = HAL_AUDIO_DEVICE_NONE;
    stream_out_open_param->afe.audio_device2 = HAL_AUDIO_DEVICE_NONE;
    stream_out_open_param->afe.audio_device3 = HAL_AUDIO_DEVICE_NONE;
    hal_audio_channel_selection_t *MemInterface = &stream_out_open_param->afe.stream_channel;
    hal_audio_interface_t *i2s_interface        = &stream_out_open_param->afe.audio_interface;
    uint32_t *misc_param = &stream_out_open_param->afe.misc_parms;
    *misc_param = 0;


    hal_gpio_status_t status;
    hal_gpio_data_t channel_gpio_data = HAL_GPIO_DATA_LOW;
    uint8_t channel_temp;

    /*Audio HW I/O Configure setting */
    if(Audio_or_Voice == AU_DSP_VOICE){ //0:Audio, 1:Voice
        switch( (audio_nvdm_HW_config.Voice_OutputDev & (NVKEY_INDEX1|NVKEY_INDEX2)) >> 4){
            case 0x03:{ //I2S_Master_Out
                *i2s_interface = 1 << ((audio_nvdm_HW_config.Voice_OutputDev & (NVKEY_INDEX3|NVKEY_INDEX4)));
                *Device = HAL_AUDIO_DEVICE_I2S_MASTER;
                if(SW_version == nvdm_version){
                    switch((audio_nvdm_HW_config.I2S_PARA_Voice & NVKEY_INDEX1) >> 6){
                        case 0x1:{
                            *misc_param |= I2S_CLK_SOURCE_APLL;
                            break;
                        }
                        case 0x0:
                        default:
                            *misc_param |= I2S_CLK_SOURCE_DCXO;
                            break;
                    }
                } else {
                    *misc_param |= I2S_CLK_SOURCE_DCXO;
                }
                break;
            }
            case 0x02:{ //Analog_SPK_Out_DUAL
                *Device = HAL_AUDIO_DEVICE_DAC_DUAL;
                break;
            }
            case 0x01:{ //Analog_SPK_Out_R
                *Device = HAL_AUDIO_DEVICE_DAC_R;
                break;
            }
            case 0x00:{ //Analog_SPK_Out_L
                *Device = HAL_AUDIO_DEVICE_DAC_L;
                break;
            }
            default:
                log_hal_msgid_info("Get Voice Stream out Device error. Defualt", 0);
                *Device = HAL_AUDIO_DEVICE_DAC_DUAL;
                break;
        }
    } else if(Audio_or_Voice == AU_DSP_AUDIO) {
        switch( (audio_nvdm_HW_config.Audio_OutputDev & (NVKEY_INDEX1|NVKEY_INDEX2)) >> 4){
            case 0x03:{ //I2S_Master_Out
                *i2s_interface = 1 << ((audio_nvdm_HW_config.Audio_OutputDev & (NVKEY_INDEX3|NVKEY_INDEX4)));
                *Device = HAL_AUDIO_DEVICE_I2S_MASTER;
                if(SW_version == nvdm_version){
                    switch((audio_nvdm_HW_config.I2S_PARA_Audio & NVKEY_INDEX1) >> 6){
                        case 0x1:{
                            *misc_param |= I2S_CLK_SOURCE_APLL;
                            break;
                        }
                        case 0x0:
                        default:
                            *misc_param |= I2S_CLK_SOURCE_DCXO;
                            break;
                    }
                } else {
                    *misc_param |= I2S_CLK_SOURCE_DCXO;
                }
                break;
            }
            case 0x02:{ //Analog_SPK_Out_DUAL
                *Device = HAL_AUDIO_DEVICE_DAC_DUAL;
                break;
            }
            case 0x01:{ //Analog_SPK_Out_R
                *Device = HAL_AUDIO_DEVICE_DAC_R;
                break;
            }
            case 0x00:{ //Analog_SPK_Out_L
                *Device = HAL_AUDIO_DEVICE_DAC_L;
                break;
            }
            default:
                log_hal_msgid_info("Get Voice Stream out Device error. Defualt", 0);
                *Device = HAL_AUDIO_DEVICE_DAC_DUAL;
                break;
        }

    } else if(Audio_or_Voice == AU_DSP_ANC) {
        switch( (audio_nvdm_HW_config.ANC_OutputDev & (NVKEY_INDEX1|NVKEY_INDEX2)) >> 4){
            case 0x02:{ //Analog_SPK_Out_DUAL
                *Device = HAL_AUDIO_DEVICE_DAC_DUAL;
                break;
            }
            case 0x01:{ //Analog_SPK_Out_R
                *Device = HAL_AUDIO_DEVICE_DAC_R;
                break;
            }
            case 0x00:{ //Analog_SPK_Out_L
                *Device = HAL_AUDIO_DEVICE_DAC_L;
                break;
            }
            default:
                log_hal_msgid_info("Get ANC output device error. Defualt", 0);
                *Device = HAL_AUDIO_DEVICE_DAC_DUAL;
                break;
        }
    }

    /*DAC AFE PARA*/
    switch (audio_nvdm_HW_config.DL_AFE_PARA & 0x01){
        case 0x01:{  //----DAC HP
            *misc_param |= DOWNLINK_PERFORMANCE_HIGH;
            log_hal_msgid_info("DL DAC change to HP mode", 0);
            break;
        }
        case 0x00:  //----DAC NM
        default:
            *misc_param |= DOWNLINK_PERFORMANCE_NORMAL;
            break;
    }

    /*Audio Channel selection setting*/
    if(audio_Channel_Select.modeForAudioChannel){
        //----HW_mode
        status = hal_gpio_get_input((hal_gpio_pin_t)audio_Channel_Select.hwAudioChannel.gpioIndex, &channel_gpio_data);
        if (status == HAL_GPIO_STATUS_OK) {
            if (channel_gpio_data == HAL_GPIO_DATA_HIGH) {
                channel_temp = audio_Channel_Select.hwAudioChannel.audioChannelGPIOH & (NVKEY_INDEX3|NVKEY_INDEX4);
            } else {
                channel_temp = audio_Channel_Select.hwAudioChannel.audioChannelGPIOL & (NVKEY_INDEX3|NVKEY_INDEX4);
            }
        } else {
            channel_temp = AU_DSP_CH_LR; //default.
            log_hal_msgid_info("Get Stream out channel setting false with HW_mode.", 0);
        }
    } else {
        channel_temp = audio_Channel_Select.audioChannel & (NVKEY_INDEX3|NVKEY_INDEX4);
    }

//Change to DSP SW Channel select
    switch(channel_temp)
    {
        case AU_DSP_CH_LR: {
            *MemInterface = HAL_AUDIO_DIRECT;
            break;
        }
#if 0
        case AU_DSP_CH_L: {
            *MemInterface = HAL_AUDIO_BOTH_L;
            break;
        }
        case AU_DSP_CH_R: {
            *MemInterface = HAL_AUDIO_BOTH_R;
            break;
        }
        case AU_DSP_CH_SWAP: {
            *MemInterface = HAL_AUDIO_SWAP_L_R;
            break;
        }
        case AU_DSP_CH_MIX: {
            *MemInterface = HAL_AUDIO_MIX_L_R;
            break;
        }
        case AU_DSP_CH_MIX_SHIFT: {
            *MemInterface = HAL_AUDIO_MIX_SHIFT_L_R;
            break;
        }
#endif
        default: {
            *MemInterface = HAL_AUDIO_DIRECT;
            break;
        }
    }
    //For debug
    //log_hal_msgid_info("Get Stream out device(%d) ch(%d) audio_inter(%d) misc_parms(%d)", 4, *Device, *MemInterface, *i2s_interface, *misc_param);
    return HAL_AUDIO_STATUS_OK;
}
#endif

#ifdef HAL_DVFS_MODULE_ENABLED
void hal_audio_set_dvfs_clk(audio_scenario_sel_t Audio_or_Voice, dvfs_frequency_t *dvfs_clk)
{
    if(Audio_or_Voice == AU_DSP_VOICE) //0:Audio, 1:Voice
    {
        switch(audio_nvdm_dvfs_config.HFP_DVFS_CLK)
        {
            case 0x00:
                *dvfs_clk = DVFS_26M_SPEED;
                break;
            case 0x01:
                *dvfs_clk = DVFS_39M_SPEED;
                break;
            case 0x02:
                *dvfs_clk = DVFS_78M_SPEED;
                break;
            case 0x03:
                *dvfs_clk = DVFS_156M_SPEED;
                break;
            default:
                *dvfs_clk = DVFS_26M_SPEED;
                break;
        }
    }
}
#endif

void hal_audio_set_dvfs_control(hal_audio_dvfs_speed_t DVFS_SPEED, hal_audio_dvfs_lock_parameter_t DVFS_lock)
{
#ifdef HAL_DVFS_MODULE_ENABLED
    if (DVFS_lock) {
        dvfs_lock_control( "audio" ,DVFS_SPEED ,DVFS_lock );
    } else {
        dvfs_lock_control( "audio" ,DVFS_SPEED ,DVFS_lock );
    }
#else
    log_hal_msgid_info("[Hal]dvfs module not enable.", 0);
#endif
}

#if defined(HAL_AUDIO_SUPPORT_DEBUG_DUMP)
/**
  * @ Dump audio debug register
  */
void hal_audio_debug_dump(void)
{
    if (hal_audio_status_query_running_flag_value() == false) {
        log_hal_msgid_info("Not do audio debug dump, dsp_controller.running == 0", 0);
        return;
    }

    log_hal_msgid_info("[BASIC]", 0);
    log_hal_msgid_info("[Audio Top Control Register 0]0x70000000 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000000)));
    log_hal_msgid_info("[Audio Top Control Register 1]0x70000004 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000004)));
    log_hal_msgid_info("[AFE Control Register 0]0x70000010 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000010)));
    log_hal_msgid_info("[AFE Control Register 1]0x70000014 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000014)));
    log_hal_msgid_info("[AFE Control Register 2]0x700002E0 = 0x%x\r\n",1, *((volatile uint32_t*)(0x700002E0)));

    log_hal_msgid_info("[Connection]", 0);
    log_hal_msgid_info("[AFE_CONN0]0x70000020 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000020)));
    log_hal_msgid_info("[AFE_CONN1]0x70000024 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000024)));
    log_hal_msgid_info("[AFE_CONN2]0x70000028 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000028)));
    log_hal_msgid_info("[AFE_CONN3]0x7000002c = 0x%x\r\n",1, *((volatile uint32_t*)(0x7000002c)));
    log_hal_msgid_info("[AFE_CONN4]0x70000030 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000030)));
    log_hal_msgid_info("[AFE_CONN5]0x7000005c = 0x%x\r\n",1, *((volatile uint32_t*)(0x7000005c)));
    log_hal_msgid_info("[AFE_CONN6]0x700000bc = 0x%x\r\n",1, *((volatile uint32_t*)(0x700000bc)));
    log_hal_msgid_info("[AFE_CONN7]0x70000420 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000420)));
    log_hal_msgid_info("[AFE_CONN8]0x70000438 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000438)));
    log_hal_msgid_info("[AFE_CONN9]0x70000440 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000440)));
    log_hal_msgid_info("[AFE_CONN10]0x70000444 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000444)));
    log_hal_msgid_info("[AFE_CONN11]0x70000448 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000448)));
    log_hal_msgid_info("[AFE_CONN12]0x7000044c = 0x%x\r\n",1, *((volatile uint32_t*)(0x7000044c)));
    log_hal_msgid_info("[AFE_CONN13]0x70000450 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000450)));
    log_hal_msgid_info("[AFE_CONN14]0x70000454 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000454)));
    log_hal_msgid_info("[AFE_CONN15]0x70000458 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000458)));
    log_hal_msgid_info("[AFE_CONN16]0x7000045c = 0x%x\r\n",1, *((volatile uint32_t*)(0x7000045c)));
    log_hal_msgid_info("[AFE_CONN17]0x70000460 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000460)));
    log_hal_msgid_info("[AFE_CONN18]0x70000464 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000464)));
    log_hal_msgid_info("[AFE_CONN19]0x70000468 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000468)));
    log_hal_msgid_info("[AFE_CONN20]0x7000046c = 0x%x\r\n",1, *((volatile uint32_t*)(0x7000046c)));
    log_hal_msgid_info("[AFE_CONN21]0x70000470 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000470)));
    log_hal_msgid_info("[AFE_CONN22]0x70000474 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000474)));
    log_hal_msgid_info("[AFE_CONN23]0x70000478 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000478)));

    log_hal_msgid_info("[Output data format]0x7000006c = 0x%x\r\n",1, *((volatile uint32_t*)(0x7000006c)));

    log_hal_msgid_info("[HWSRC]", 0);
    log_hal_msgid_info("0x70000004 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000004)));
    log_hal_msgid_info("0x70001150 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70001150)));
    log_hal_msgid_info("0x70001170 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70001170)));
    log_hal_msgid_info("[HWSRC DL1]0x70001100 = 0x%x, ASRC_BUSY=%x\r\n",2, *((volatile uint32_t*)(0x70001100)), (*((volatile uint32_t*)(0x70001100)))&(1<<9));
    log_hal_msgid_info("[HWSRC DL1]0x70001100 = 0x%x, ASRC_EN=%x\r\n",2, *((volatile uint32_t*)(0x70001100)), (*((volatile uint32_t*)(0x70001100)))&(1<<8));
    log_hal_msgid_info("[HWSRC DL2]0x70001200 = 0x%x, ASRC_BUSY=%x\r\n",2, *((volatile uint32_t*)(0x70001200)), (*((volatile uint32_t*)(0x70001200)))&(1<<9));
    log_hal_msgid_info("[HWSRC DL2]0x70001200 = 0x%x, ASRC_EN=%x\r\n",2, *((volatile uint32_t*)(0x70001200)), (*((volatile uint32_t*)(0x70001200)))&(1<<8));

    log_hal_msgid_info("[I2S Master]", 0);
    log_hal_msgid_info("[I2S Master 0]0x70000860 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000860)));
    log_hal_msgid_info("[I2S Master 1]0x70000864 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000864)));
    log_hal_msgid_info("[I2S Master 2]0x70000868 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000868)));
    log_hal_msgid_info("[I2S Master 3]0x7000086c = 0x%x\r\n",1, *((volatile uint32_t*)(0x7000086c)));
    log_hal_msgid_info("[I2S Master clock gating]0x70000004 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000004)));
    log_hal_msgid_info("[DSP APLL clock gating]", 0);
    log_hal_msgid_info("0x70000000 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000000)));
    log_hal_msgid_info("0x70000dd0 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000dd0)));

    log_hal_msgid_info("[Mic]", 0);
    log_hal_msgid_info("0xA2070108 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070108)));
    log_hal_msgid_info("0xA207010C = 0x%x\r\n",1, *((volatile uint32_t*)(0xA207010C)));
    log_hal_msgid_info("0xA2070224 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070224)));
    log_hal_msgid_info("0xA2070124 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070124)));
    log_hal_msgid_info("0xA2070130 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070130)));
    log_hal_msgid_info("0xA2070134 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070134)));
    log_hal_msgid_info("0x70000f98 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000f98)));
    log_hal_msgid_info("0xA207011C = 0x%x\r\n",1, *((volatile uint32_t*)(0xA207011C)));
    log_hal_msgid_info("0xA2070128 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070128)));
    log_hal_msgid_info("0xA207012C = 0x%x\r\n",1, *((volatile uint32_t*)(0xA207012C)));
    log_hal_msgid_info("[AFE_ADDA_TOP_CON0]0x70000120 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000120)));
    log_hal_msgid_info("[AFE_ADDA_UL_SRC_CON0]0x70000114 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000114)));
    log_hal_msgid_info("[AFE_ADDA2_UL_SRC_CON0]0x70000604 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000604)));
    log_hal_msgid_info("[AFE_ADDA6_UL_SRC_CON0]0x70000a84 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000a84)));
    log_hal_msgid_info("[AFE_ADDA_UL_DL_CON0]0x70000124 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000124)));

    log_hal_msgid_info("[DAC]", 0);
    log_hal_msgid_info("0x70000108 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000108)));
    log_hal_msgid_info("0x70000c50 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000c50)));
    log_hal_msgid_info("[AFE_ADDA_UL_DL_CON0]0x70000124 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000124)));
    log_hal_msgid_info("0xA2070200 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070200)));
    log_hal_msgid_info("0xA2070204 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070204)));
    log_hal_msgid_info("0xA2070208 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070208)));
    log_hal_msgid_info("0xA207020C = 0x%x\r\n",1, *((volatile uint32_t*)(0xA207020C)));
    log_hal_msgid_info("0xA2070210 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070210)));
    log_hal_msgid_info("0xA2070214 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070214)));
    log_hal_msgid_info("0xA207021C = 0x%x\r\n",1, *((volatile uint32_t*)(0xA207021C)));
    log_hal_msgid_info("0xA2070220 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070220)));
    log_hal_msgid_info("0xA2070224 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070224)));
    log_hal_msgid_info("0xA2070228 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070228)));
    log_hal_msgid_info("0xA207022C = 0x%x\r\n",1, *((volatile uint32_t*)(0xA207022C)));
    log_hal_msgid_info("0xA2070230 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070230)));
    log_hal_msgid_info("0x70000f50 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000f50)));
    log_hal_msgid_info("0x70000ed0 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000ed0)));

    log_hal_msgid_info("[INPUT GAIN]", 0);
    log_hal_msgid_info("[Input analog gain][L PGA GAIN]0xA2070100 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070100)));
    log_hal_msgid_info("[Input analog gain][R PGA GAIN]0xA2070104 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070104)));
    log_hal_msgid_info("[Input digital gain][SW GAIN]", 0);
    log_hal_msgid_info("[OUTPUT GAIN]", 0);
    log_hal_msgid_info("[Output analog gain][AMP GAIN]0x70000f58 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000f58)));
    log_hal_msgid_info("[Output digital gain][HW GAIN1 TARGET]0x70000414 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000414)));
    log_hal_msgid_info("[Output digital gain][HW GAIN2 TARGET]0x7000042C = 0x%x\r\n",1, *((volatile uint32_t*)(0x7000042C)));
    log_hal_msgid_info("[Output digital gain][HW GAIN1 CURRENT]0x70000424 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000424)));
    log_hal_msgid_info("[Output digital gain][HW GAIN2 CURRENT]0x7000043C = 0x%x\r\n",1, *((volatile uint32_t*)(0x7000043C)));

    log_hal_msgid_info("[UL/DL and classg]", 0);
    log_hal_msgid_info("0x70000908 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000908)));
    log_hal_msgid_info("0x70000900 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000900)));
    log_hal_msgid_info("0x70000908 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000908)));
    log_hal_msgid_info("0x70000e6c = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000e6c)));
    log_hal_msgid_info("0x70000EE4 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000EE4)));
    log_hal_msgid_info("0x70000EE8 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000EE8)));
    log_hal_msgid_info("0x70000EEC = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000EEC)));
    log_hal_msgid_info("0x70000EE0 = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000EE0)));
    log_hal_msgid_info("0x70000EDC = 0x%x\r\n",1, *((volatile uint32_t*)(0x70000EDC)));
    log_hal_msgid_info("0xA2070224 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2070224)));

    log_hal_msgid_info("[I2S Slave]", 0);
    log_hal_msgid_info("[I2S Slave 0]0xA0070038 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA0070038)));
    log_hal_msgid_info("[I2S Slave 0]0xA0070008 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA0070008)));
    log_hal_msgid_info("[I2S Slave 0]0xA0070030 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA0070030)));
    log_hal_msgid_info("[I2S Slave 1]0xA0080038 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA0080038)));
    log_hal_msgid_info("[I2S Slave 1]0xA0080008 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA0080008)));
    log_hal_msgid_info("[I2S Slave 1]0xA0080030 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA0080030)));
    log_hal_msgid_info("[I2S Slave 2]0xA0090038 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA0090038)));
    log_hal_msgid_info("[I2S Slave 2]0xA0090008 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA0090008)));
    log_hal_msgid_info("[I2S Slave 2]0xA0090030 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA0090030)));
    log_hal_msgid_info("[I2S Slave 3]0xA00A0038 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA00A0038)));
    log_hal_msgid_info("[I2S Slave 3]0xA00A0008 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA00A0008)));
    log_hal_msgid_info("[I2S Slave 3]0xA00A0030 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA00A0030)));

    log_hal_msgid_info("[APLL]", 0);
    log_hal_msgid_info("[APLL 1]0xA2050003 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2050003)));
    log_hal_msgid_info("[APLL 1]0xA2050000 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2050000)));
    log_hal_msgid_info("[APLL 1]0xA2050001 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2050001)));
    log_hal_msgid_info("[APLL 1]0xA2050004 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2050004)));
    log_hal_msgid_info("[APLL 1]0xA205002C = 0x%x\r\n",1, *((volatile uint32_t*)(0xA205002C)));

    log_hal_msgid_info("[APLL 2]0xA2050103 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2050103)));
    log_hal_msgid_info("[APLL 2]0xA2050100 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2050100)));
    log_hal_msgid_info("[APLL 2]0xA2050101 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2050101)));
    log_hal_msgid_info("[APLL 2]0xA2050104 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2050104)));
    log_hal_msgid_info("[APLL 2]0xA205012C = 0x%x\r\n",1, *((volatile uint32_t*)(0xA205012C)));

    log_hal_msgid_info("[APLL TUNER]", 0);
    log_hal_msgid_info("[APLL TUNER 1]0xA2050038 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2050038)));
    log_hal_msgid_info("[APLL TUNER 1]0xA2050034 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2050034)));
    log_hal_msgid_info("[APLL TUNER 2]0xA2050138 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2050138)));
    log_hal_msgid_info("[APLL TUNER 2]0xA2050134 = 0x%x\r\n",1, *((volatile uint32_t*)(0xA2050134)));

    log_hal_msgid_info("[AFE MEMIF]", 0);
    log_hal_msgid_info("[HD_MODE]0x700003F8 = 0x%x\r\n",1, *((volatile uint32_t*)(0x700003F8)));
    log_hal_msgid_info("[HDALIGN]0x700003FC = 0x%x\r\n",1, *((volatile uint32_t*)(0x700003FC)));
    log_hal_msgid_info("[MSB]0x700000CC= 0x%x\r\n",1, *((volatile uint32_t*)(0x700000CC)));
}
#endif

#if defined(HAL_AUDIO_SUPPORT_APLL)

#define AFE_WRITE8(addr, val)   *((volatile uint8_t *)(addr)) = val
#define AFE_READ(addr)          *((volatile uint32_t *)(addr))
#define AFE_WRITE(addr, val)    *((volatile uint32_t *)(addr)) = val
#define AFE_SET_REG(addr, val, msk)  AFE_WRITE((addr), ((AFE_READ(addr) & (~(msk))) | ((val) & (msk))))
#define ReadREG(_addr)          (*(volatile uint32_t *)(_addr))

afe_apll_source_t afe_get_apll_by_samplerate(uint32_t samplerate)
{
    if (samplerate == 176400 || samplerate == 88200 || samplerate == 44100 || samplerate == 22050 || samplerate == 11025) {
        return AFE_APLL1;
    }
    else {
        return AFE_APLL2;
    }
}

void afe_set_aplltuner(bool enable, afe_apll_source_t apll)
{
    log_hal_msgid_info("DSP afe_set_apll_for_i2s_reg APLL:%d, enable:%d\r\n", 2, apll, enable);
    if (true == enable) {
        // Clear upper layer audio CG
        //AFE_SET_REG(0xA2020238, 0x01020000, 0xFFFFFFFF);//CKSYS_CLK_CFG_2 //Selects clk_aud_interface0_sel APLL2_CK, 49.152(MHz) and clk_aud_interface1_sel APLL1_CK, 45.1584(MHz)

        switch (apll) {
            case AFE_APLL1:
                //Open APLL1
                clock_mux_sel(CLK_AUD_INTERFACE1_SEL ,1);//Selects clk_aud_interface1_sel APLL1_CK, 45.1584(MHz)
                //Setting APLL1 Tuner
                AFE_SET_REG(APLL1_CTL14__F_RG_APLL1_LCDDS_TUNER_PCW_NCPO, 0x0DE517A9, 0xFFFFFFFF);//[31:24] is integer  number, [23:0] is fractional number
                //AFE_SET_REG(APLL1_CTL12__F_RG_APLL1_LCDDS_TUNER_PCW_NCPO, 0x00000100, 0xFFFFFFFF);//DDS SSC enable
                AFE_SET_REG(APLL1_CTL13__F_RG_APLL1_LCDDS_TUNER_EN, 0x00000001, 0xFFFFFFFF);//DDS PCW tuner enable
                break;
            case AFE_APLL2:
                //Open APLL2
                clock_mux_sel(CLK_AUD_INTERFACE0_SEL ,2);//Selects clk_aud_interface0_sel APLL2_CK, 49.152(MHz)
                //Setting APLL2 Tuner
                AFE_SET_REG(APLL2_CTL14__F_RG_APLL2_LCDDS_TUNER_PCW_NCPO, 0x0F1FAA4C, 0xFFFFFFFF);//[31:24] is integer  number, [23:0] is fractional number
                //AFE_SET_REG(APLL2_CTL12__F_RG_APLL2_LCDDS_TUNER_PCW_NCPO, 0x00000100, 0xFFFFFFFF);//DDS SSC enable
                AFE_SET_REG(APLL2_CTL13__F_RG_APLL2_LCDDS_TUNER_EN, 0x00000001, 0xFFFFFFFF);//DDS PCW tuner enable
                break;
            default:
                log_hal_msgid_warning("I2S Master not turn on APLL1 and APLL2", 0);
                break;
        }
    } else {
        uint32_t set_value = 0;
        switch (apll) {
            case AFE_APLL1:
                // Disable APLL1 Tuner
                set_value = ReadREG(APLL1_CTL15__F_RG_APLL1_LCDDS_PCW_NCPO);                //[31:24] is integer  number, [23:0] is fractional number
                AFE_SET_REG(APLL1_CTL10__F_RG_APLL1_LCDDS_PCW_NCPO, set_value, 0xFFFFFFFF); //[31:24] is integer  number, [23:0] is fractional number
                //AFE_WRITE8(APLL1_CTL12__F_RG_APLL1_LCDDS_TUNER_PCW_NCPO, 0);
                AFE_SET_REG(APLL1_CTL13__F_RG_APLL1_LCDDS_TUNER_EN, 0x00000000, 0xFFFFFFFF);//DDS PCW tuner disable
                break;
            case AFE_APLL2:
                // Disable APLL2 Tuner
                set_value = ReadREG(APLL2_CTL15__F_RG_APLL2_LCDDS_PCW_NCPO);                //[31:24] is integer  number, [23:0] is fractional number
                AFE_SET_REG(APLL2_CTL10__F_RG_APLL2_LCDDS_PCW_NCPO, set_value, 0xFFFFFFFF); //[31:24] is integer  number, [23:0] is fractional number
                //AFE_WRITE8(APLL2_CTL12__F_RG_APLL2_LCDDS_TUNER_PCW_NCPO, 0);
                AFE_SET_REG(APLL2_CTL13__F_RG_APLL2_LCDDS_TUNER_EN, 0x00000000, 0xFFFFFFFF);//DDS PCW tuner disable
                break;
            default:
                log_hal_msgid_warning("I2S Master not turn off APLL1 and APLL2", 0);
                break;
        }
    }
}

#define AUDIO_TOP_CON0                          0x70000000
#define AFE_APLL1_TUNER_CFG                     0x700003f0
#define AFE_APLL2_TUNER_CFG                     0x700003f4

#define AUDIO_TOP_CON0_PDN_APLL2_TUNER_POS      (18)
#define AUDIO_TOP_CON0_PDN_APLL2_TUNER_MASK     (1<<AUDIO_TOP_CON0_PDN_APLL2_TUNER_POS)
#define AUDIO_TOP_CON0_PDN_APLL_TUNER_POS       (19)
#define AUDIO_TOP_CON0_PDN_APLL_TUNER_MASK      (1<<AUDIO_TOP_CON0_PDN_APLL_TUNER_POS)

#define MAX_TIMES  10000

void afe_aplltuner_clock_on(bool enable, afe_apll_source_t apll)
{
    uint32_t take_times=0;
    uint32_t mask;
    hal_nvic_save_and_set_interrupt_mask(&mask);

    while(++take_times)
    {
        if (HAL_HW_SEMAPHORE_STATUS_OK==hal_hw_semaphore_take(HW_SEMAPHORE_APLL))
        {
            break;
        }
        if (take_times>MAX_TIMES)
        {
             hal_nvic_restore_interrupt_mask(mask);
             //error handling
             log_hal_msgid_info("[SEMAPHORE] CM4 take semaphore %d fail.", 1, HW_SEMAPHORE_APLL);
             platform_assert("[SEMAPHORE] CM4 take semaphore %d fail.",__func__,__LINE__);
             return;
        }
    }

    if (true == enable) {
        switch (apll) {
            case AFE_APLL1:
                AFE_SET_REG(AUDIO_TOP_CON0, 0 << AUDIO_TOP_CON0_PDN_APLL_TUNER_POS , AUDIO_TOP_CON0_PDN_APLL_TUNER_MASK);//PDN control for apll tuner
                break;
            case AFE_APLL2:
                AFE_SET_REG(AUDIO_TOP_CON0, 0 << AUDIO_TOP_CON0_PDN_APLL2_TUNER_POS, AUDIO_TOP_CON0_PDN_APLL2_TUNER_MASK);//PDN control for apll2 tuner
                break;
            default:
                break;
        }
    } else {
        switch (apll) {
            case AFE_APLL1:
                AFE_SET_REG(AUDIO_TOP_CON0, 1 << AUDIO_TOP_CON0_PDN_APLL_TUNER_POS, AUDIO_TOP_CON0_PDN_APLL_TUNER_MASK);
                break;
            case AFE_APLL2:
                AFE_SET_REG(AUDIO_TOP_CON0, 1 << AUDIO_TOP_CON0_PDN_APLL2_TUNER_POS, AUDIO_TOP_CON0_PDN_APLL2_TUNER_MASK);
                break;
            default:
                break;
        }
    }

    if (HAL_HW_SEMAPHORE_STATUS_OK==hal_hw_semaphore_give(HW_SEMAPHORE_APLL))
    {
        hal_nvic_restore_interrupt_mask(mask);
    } else {
        hal_nvic_restore_interrupt_mask(mask);
        //error handling
        log_hal_msgid_info("[SEMAPHORE] CM4 give semaphore %d fail.", 1, HW_SEMAPHORE_APLL);
        platform_assert("[SEMAPHORE] CM4 give semaphore %d fail.",__func__,__LINE__);
    }
}

void afe_enable_apll_tuner(bool enable, afe_apll_source_t apll)
{
    if (true == enable) {
        switch (apll) {
            //Enable tuner
            case AFE_APLL1:
                AFE_SET_REG(AFE_APLL1_TUNER_CFG, 0x00000432, 0x0000FFF7);//AFE TUNER Control Register
                AFE_SET_REG(AFE_APLL1_TUNER_CFG, 0x1, 0x1);
                break;
            case AFE_APLL2:
                AFE_SET_REG(AFE_APLL2_TUNER_CFG, 0x00000434, 0x0000FFF7);//AFE TUNER2 Control Register
                AFE_SET_REG(AFE_APLL2_TUNER_CFG, 0x1, 0x1);
                break;
            default:
                break;
        }
    } else {
        switch (apll) {
            //Disable tuner
            case AFE_APLL1:
                AFE_SET_REG(AFE_APLL1_TUNER_CFG, 0x0, 0x1);
                break;
            case AFE_APLL2:
                AFE_SET_REG(AFE_APLL2_TUNER_CFG, 0x0, 0x1);
                break;
            default:
                break;
        }
    }
}

hal_audio_status_t hal_audio_apll_enable(bool enable, uint32_t samplerate)
{
    uint32_t mask;
    hal_nvic_save_and_set_interrupt_mask(&mask);
    hal_audio_status_t result = HAL_AUDIO_STATUS_OK;

    if (true == enable) {
        switch (afe_get_apll_by_samplerate(samplerate)) {
            case AFE_APLL1:
                aud_apll_1_cntr++;
                if (aud_apll_1_cntr == 1) {
#ifdef HAL_DVFS_MODULE_ENABLED
                    pmu_vcore_lock_control(PMU_NORMAL, PMIC_VCORE_1P1_V, PMU_LOCK);
                    log_hal_msgid_info("frequency is risen to 1.1V for open apll1", 0);
#endif
                    log_hal_msgid_info("[APLL] TurnOnAPLL1, FS:%d, APLL1_CNT:%d", 2, samplerate, aud_apll_1_cntr);
                    clock_set_pll_on(CLK_APLL1);
                    afe_set_aplltuner(true, AFE_APLL1);
                    afe_aplltuner_clock_on(true, AFE_APLL1);
                    afe_enable_apll_tuner(true, AFE_APLL1);
                } else {
                    log_hal_msgid_info("[APLL] TurnOnAPLL1 again, FS:%d, APLL1_CNT:%d", 2, samplerate, aud_apll_1_cntr);
                }
                break;
            case AFE_APLL2:
                aud_apll_2_cntr++;
                if (aud_apll_2_cntr == 1) {
#ifdef HAL_DVFS_MODULE_ENABLED
                    pmu_vcore_lock_control(PMU_NORMAL, PMIC_VCORE_1P1_V, PMU_LOCK);
                    log_hal_msgid_info("frequency is risen to 1.1V for open apll2", 0);
#endif
                    log_hal_msgid_info("[APLL] TurnOnAPLL2, FS:%d, APLL2_CNT:%d", 2, samplerate, aud_apll_2_cntr);
                    clock_set_pll_on(CLK_APLL2);
                    afe_set_aplltuner(true, AFE_APLL2);
                    afe_aplltuner_clock_on(true, AFE_APLL2);
                    afe_enable_apll_tuner(true, AFE_APLL2);
                } else {
                    log_hal_msgid_info("[APLL] TurnOnAPLL2 again, FS:%d, APLL2_CNT:%d", 2, samplerate, aud_apll_2_cntr);
                }
                break;
            default:
                result = HAL_AUDIO_STATUS_INVALID_PARAMETER;
                break;
        }
    } else {
        switch (afe_get_apll_by_samplerate(samplerate)) {
            case AFE_APLL1:
                aud_apll_1_cntr--;
                if (aud_apll_1_cntr == 0) {
                    afe_enable_apll_tuner(false, AFE_APLL1);
                    afe_aplltuner_clock_on(false, AFE_APLL1);
                    afe_set_aplltuner(false, AFE_APLL1);
                    clock_set_pll_off(CLK_APLL1);
                    log_hal_msgid_info("[APLL] TurnOffAPLL1, FS:%d, APLL1_CNT:%d", 2, samplerate, aud_apll_1_cntr);
#ifdef HAL_DVFS_MODULE_ENABLED
                    pmu_vcore_lock_control(PMU_NORMAL, PMIC_VCORE_1P1_V, PMU_UNLOCK);
                    log_hal_msgid_info("frequency is risen to 1.1V for close apll1", 0);
#endif
                } else if (aud_apll_1_cntr < 0) {
                    log_hal_msgid_info("[APLL] Error, Already TurnOffAPLL1, FS:%d, APLL1_CNT:0", 1, samplerate);
                    aud_apll_1_cntr = 0;
                    result = HAL_AUDIO_STATUS_ERROR;
                } else {
                    log_hal_msgid_info("[APLL] TurnOffAPLL1 again, FS:%d, APLL1_CNT:%d", 2, samplerate, aud_apll_1_cntr);
                }
                break;
            case AFE_APLL2:
                aud_apll_2_cntr--;
                if (aud_apll_2_cntr == 0) {
                    afe_enable_apll_tuner(false, AFE_APLL2);
                    afe_aplltuner_clock_on(false, AFE_APLL2);
                    afe_set_aplltuner(false, AFE_APLL2);
                    clock_set_pll_off(CLK_APLL2);
                    log_hal_msgid_info("[APLL] TurnOffAPLL2, FS:%d, APLL2_CNT:%d", 2, samplerate, aud_apll_2_cntr);
#ifdef HAL_DVFS_MODULE_ENABLED
                    pmu_vcore_lock_control(PMU_NORMAL, PMIC_VCORE_1P1_V, PMU_UNLOCK);
                    log_hal_msgid_info("frequency is risen to 1.1V for close apll1", 0);
#endif
                } else if (aud_apll_2_cntr < 0) {
                    log_hal_msgid_info("[APLL] Error, Already TurnOffAPLL2, FS:%d, APLL2_CNT:0", 1, samplerate);
                    aud_apll_2_cntr = 0;
                    result = HAL_AUDIO_STATUS_ERROR;
                } else {
                    log_hal_msgid_info("[APLL] TurnOffAPLL2 again, FS:%d, APLL2_CNT:%d", 2, samplerate, aud_apll_2_cntr);
                }
                break;
            default:
                result = HAL_AUDIO_STATUS_INVALID_PARAMETER;
                break;
        }
    }
    hal_nvic_restore_interrupt_mask(mask);
    return result;
}

hal_audio_status_t hal_audio_query_apll_status(void) {
    log_hal_msgid_info("[APLL] aud_apll_1_cntr=%d", 1, aud_apll_1_cntr);
    log_hal_msgid_info("[APLL] aud_apll_2_cntr=%d", 1, aud_apll_2_cntr);
    return HAL_AUDIO_STATUS_OK;
}

hal_audio_status_t hal_audio_mclk_enable(bool enable, afe_mclk_out_pin_t mclkoutpin, afe_apll_source_t apll, uint8_t divider)
{
    if (mclkoutpin != AFE_MCLK_PIN_FROM_I2S0 && mclkoutpin != AFE_MCLK_PIN_FROM_I2S1 && mclkoutpin != AFE_MCLK_PIN_FROM_I2S2 && mclkoutpin != AFE_MCLK_PIN_FROM_I2S3) {
        log_hal_msgid_info("[MCLK] not support mclkoutpin=%d", 1, mclkoutpin);
        return HAL_AUDIO_STATUS_INVALID_PARAMETER;
    }
    if (apll != AFE_APLL1 && apll != AFE_APLL2) {
        log_hal_msgid_info("[MCLK] not support apll=%d", 1, apll);
        return HAL_AUDIO_STATUS_INVALID_PARAMETER;
    }
    if (divider > 127) {
        log_hal_msgid_info("[MCLK] not support divider=%d", 1, divider);
        return HAL_AUDIO_STATUS_INVALID_PARAMETER;
    }

    if (enable) {
        if (mclk_status[mclkoutpin].status == MCLK_DISABLE) {
            ami_hal_audio_status_set_running_flag(AUDIO_MESSAGE_TYPE_AFE, true);
            uint32_t clock_divider_reg;
            uint32_t clock_divider_shift;
            bool toggled_bit = 0;

            if ((mclkoutpin == AFE_MCLK_PIN_FROM_I2S0) || (mclkoutpin == AFE_MCLK_PIN_FROM_I2S1)) {
                clock_divider_reg = 0xA2020308;
            } else {
                clock_divider_reg = 0xA202030C;
            }
            if ((mclkoutpin == AFE_MCLK_PIN_FROM_I2S0) || (mclkoutpin == AFE_MCLK_PIN_FROM_I2S2)) {
                clock_divider_shift = 0;
            } else {
                clock_divider_shift = 16;
            }

            switch (apll) {
                case AFE_APLL1:
                    hal_audio_apll_enable(true, 44100);
                    AFE_SET_REG (0xA2020304, 0, 0x3<<(8*mclkoutpin));                   // I2S0/1/2/3 clock_source from APLL1
                    mclk_status[mclkoutpin].apll = AFE_APLL1;
                    break;
                case AFE_APLL2:
                    hal_audio_apll_enable(true, 48000);
                    AFE_SET_REG (0xA2020304, 0x1<<(8*mclkoutpin), 0x3<<(8*mclkoutpin)); // I2S0/1/2/3 clock_source from APLL2
                    mclk_status[mclkoutpin].apll = AFE_APLL2;
                    break;
                default:
                    break;
            }

            // Setting audio clock divider   //Toggled to apply apll_ck_div bit-8 or bit-24
            //MCLK = clock_source/(1+n), n = [6:0], clock_source : AFE_I2S_SETTING_MCLK_SOURCE, n : AFE_I2S_SETTING_MCLK_DIVIDER)
            toggled_bit = (ReadREG(clock_divider_reg)&(0x00000100<<clock_divider_shift))>>8;
            if (toggled_bit == true) {
                AFE_SET_REG (clock_divider_reg, divider<<clock_divider_shift, 0x17f<<clock_divider_shift);
            } else {
                AFE_SET_REG (clock_divider_reg, (divider | 0x00000100)<<clock_divider_shift, 0x17f<<clock_divider_shift);
            }

            //Power on apll12_div0/1/2/3 divider
            AFE_SET_REG(0xA2020300, 0<<(8*mclkoutpin), 1<<(8*mclkoutpin));
            mclk_status[mclkoutpin].mclk_cntr++;
            mclk_status[mclkoutpin].divider = divider;
            mclk_status[mclkoutpin].status = MCLK_ENABLE;
            log_hal_msgid_info("[MCLK] TurnOnMCLK[%d], apll%d with divider%d, MCLK_CNT=%d", 4, mclkoutpin, apll, divider, mclk_status[mclkoutpin].mclk_cntr);
            return HAL_AUDIO_STATUS_OK;
        } else {
            if ((mclk_status[mclkoutpin].apll == apll) && (mclk_status[mclkoutpin].divider == divider)) {
                mclk_status[mclkoutpin].mclk_cntr++;
                log_hal_msgid_info("[MCLK] TurnOnMCLK[%d], apll%d with divider%d again, MCLK_CNT=%d", 4, mclkoutpin, apll, divider, mclk_status[mclkoutpin].mclk_cntr);
                return HAL_AUDIO_STATUS_OK;
            } else {
                log_hal_msgid_info("[MCLK] Error, Already TurnOnMCLK[%d] apll%d with divider%d, Request apll%d with divider%d is invalid", 5, mclkoutpin, mclk_status[mclkoutpin].apll, mclk_status[mclkoutpin].divider, apll, divider);
                return HAL_AUDIO_STATUS_ERROR;
            }
        }
    } else {
        if (mclk_status[mclkoutpin].status == MCLK_ENABLE) {
            if ((mclk_status[mclkoutpin].apll == apll) && (mclk_status[mclkoutpin].divider == divider)) {
                mclk_status[mclkoutpin].mclk_cntr--;

                if (mclk_status[mclkoutpin].mclk_cntr == 0) {
                    AFE_SET_REG(0xA2020300, 1<<(8*mclkoutpin), 1<<(8*mclkoutpin));
                    if(mclk_status[mclkoutpin].apll == AFE_APLL1) {
                        hal_audio_apll_enable(false, 44100);
                    } else {
                        hal_audio_apll_enable(false, 48000);
                    }
                    mclk_status[mclkoutpin].status = MCLK_DISABLE;
                    mclk_status[mclkoutpin].mclk_cntr = 0;
                    mclk_status[mclkoutpin].apll = AFE_APLL_NOUSE;
                    mclk_status[mclkoutpin].divider = 0;
                    log_hal_msgid_info("[MCLK] TurnOffMCLK[%d], apll%d with divider%d, MCLK_CNT=%d", 4, mclkoutpin, apll, divider, mclk_status[mclkoutpin].mclk_cntr);
                    if (mclk_status[AFE_MCLK_PIN_FROM_I2S0].mclk_cntr == 0 && mclk_status[AFE_MCLK_PIN_FROM_I2S1].mclk_cntr == 0 && mclk_status[AFE_MCLK_PIN_FROM_I2S2].mclk_cntr == 0 && mclk_status[AFE_MCLK_PIN_FROM_I2S3].mclk_cntr == 0) {
                        ami_hal_audio_status_set_running_flag(AUDIO_MESSAGE_TYPE_AFE, false);
                    }
                    return HAL_AUDIO_STATUS_OK;
                } else if (mclk_status[mclkoutpin].mclk_cntr < 0) {
                    log_hal_msgid_info("[MCLK] Error, Already TurnOffMCLK[%d], apll%d with divider%d, MCLK_CNT=%d", 4, mclkoutpin, apll, divider, mclk_status[mclkoutpin].mclk_cntr);
                    mclk_status[mclkoutpin].mclk_cntr = 0;
                    return HAL_AUDIO_STATUS_ERROR;
                } else {
                    log_hal_msgid_info("[MCLK] TurnOffMCLK[%d], apll%d with divider%d again, MCLK_CNT=%d", 4, mclkoutpin, apll, divider, mclk_status[mclkoutpin].mclk_cntr);
                    return HAL_AUDIO_STATUS_OK;
                }
            } else {
                log_hal_msgid_info("[MCLK] Error, Already TurnOnMCLK[%d] apll%d with divider%d, Request TurnOffMCLK apll%d with divider%d is invalid", 5, mclkoutpin, mclk_status[mclkoutpin].apll, mclk_status[mclkoutpin].divider, apll, divider);
                return HAL_AUDIO_STATUS_ERROR;
            }
        } else {
            log_hal_msgid_info("[MCLK] Already TurnOffMCLK[%d]", 1, mclkoutpin);
            return HAL_AUDIO_STATUS_ERROR;
        }
    }
}

hal_audio_status_t hal_audio_query_mclk_status(void) {
    uint8_t i = 0;
    for (i=0; i<4; i++) {
        log_hal_msgid_info("[MCLK] mclk_status[%d].status=%d", 2, i, mclk_status[i].status);
        log_hal_msgid_info("[MCLK] mclk_status[%d].mclk_cntr=%d", 2, i, mclk_status[i].mclk_cntr);
        log_hal_msgid_info("[MCLK] mclk_status[%d].apll=%d", 2, i, mclk_status[i].apll);
        log_hal_msgid_info("[MCLK] mclk_status[%d].divider=%d", 2, i, mclk_status[i].divider);
    }
    return HAL_AUDIO_STATUS_OK;
}
#endif

#endif /* defined(HAL_AUDIO_MODULE_ENABLED) */
