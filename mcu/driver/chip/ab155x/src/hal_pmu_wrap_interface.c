/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_pmu_wrap_interface.h"

#ifdef HAL_PMU_MODULE_ENABLED
#include "hal.h"
static pmu_mt6388_wrap_api_struct hal_pmu_st = {
    pmu_init_6388,
    pmu_set_register_value_mt6388,
    pmu_get_register_value_mt6388,
    pmu_set_register_value_2byte_mt6388,
    pmu_get_register_value_2byte_mt6388,
    pmu_vcore_voltage_sel_6388,
    pmu_get_vcore_voltage_mt6388,
    pmu_vaud18_voltage_sel_6388,
    pmu_vaud18_sleep_voltage_sel_6388,
};

void pmu_init() {
    hal_pmu_st.init();
}

pmu_operate_status_t pmu_set_register_value(uint32_t address, uint32_t mask, uint32_t shift, uint32_t value) {
    return hal_pmu_st.set_register_value(address,mask,shift,value);
}
uint32_t pmu_get_register_value(uint32_t address, uint32_t mask, uint32_t shift) {
    return hal_pmu_st.get_register_value(address,mask,shift);
}

pmu_operate_status_t pmu_set_register_value_2byte(uint32_t address, uint32_t mask, uint32_t shift, uint32_t value) {
    return hal_pmu_st.set_register_value_2byte(address,mask,shift,value);
}
uint32_t pmu_get_register_value_2byte(uint32_t address, uint32_t mask, uint32_t shift) {
    return hal_pmu_st.get_register_value_2byte(address,mask,shift);
}

pmu_operate_status_t pmu_vcore_voltage_select(pmu_power_stage_t mode ,pmu_power_vcore_voltage_t vol,pmu_lock_parameter_t lock_v) {
    pmu_power_vcore_voltage_t origin_voltage,cur_voltage;
    origin_voltage = pmu_get_vcore_voltage_mt6388();
    cur_voltage = pmu_vcore_lock_control(mode,vol,lock_v);
    if(origin_voltage != cur_voltage) {
        return hal_pmu_st.vcore_voltage_select(mode,cur_voltage);
    } else {
        return PMU_ERROR;
    }
}

pmu_power_vcore_voltage_t pmu_get_vcore_voltage(void) {
    return hal_pmu_st.get_vcore_voltage();
}

pmu_operate_status_t pmu_vaud18_voltage_select(uint8_t vol) {
    return hal_pmu_st.vaud18_voltage_select(vol);
}

pmu_operate_status_t pmu_vaud18_sleep_voltage_select(pmu_power_vaud18_voltage_t vol) {
    return hal_pmu_st.vaud18_sleep_voltage_select(vol);
}

#endif /* HAL_PMU_MODULE_ENABLED */
