/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#include "hal_pdma_internal.h"
#include "hal_sw_dma_internal.h"
#include "hal_sleep_manager.h"
#include "hal_sleep_manager_internal.h"
#include "hal_clock.h"
#include "hal_cache.h"
#include "hal_resource_assignment.h"
// For memcpy
#include <string.h>

#ifndef __UBL__
#include "assert.h"
#else
#define assert(expr) log_hal_error("assert\r\n")
#endif

#include "memory_attribute.h"
/*peripheral dma base address array */
static PDMA_REGISTER_T * const pdma[PDMA_NUMBER] = {PDMA20, PDMA21, PDMA22, PDMA23, PDMA24, PDMA25};

/*peripheral dma global status base address array */
static PDMA_REGISTER_GLOBAL_T * const pdma_global[PDMA_NUMBER] = {PDMA_GLOAL1, PDMA_GLOAL1, PDMA_GLOAL1, PDMA_GLOAL1, PDMA_GLOAL1, PDMA_GLOAL1};

/*peripheral dma global status running bit location array */
static const uint32_t g_pdma_global_running_bit[PDMA_NUMBER] = {PDMA_CHANNEL20_OFFSET, PDMA_CHANNEL21_OFFSET, PDMA_CHANNEL22_OFFSET, PDMA_CHANNEL23_OFFSET, PDMA_CHANNEL24_OFFSET, PDMA_CHANNEL25_OFFSET};

/*peripheral dma init status*/
static volatile uint8_t pdma_init_status[PDMA_NUMBER] = {0};

/*virtual fifo dma base address array */
static VDMA_REGISTER_T * const vdma[VDMA_NUMBER] = {VDMA6, VDMA7, VDMA8, VDMA9, VDMA10, VDMA11, VDMA12, VDMA13, VDMA14, VDMA15, VDMA16, VDMA17, VDMA18, VDMA19};

/*virtual fifo dma global status base address array */
static VDMA_REGISTER_GLOBAL_T * const vdma_global[VDMA_NUMBER] = {VDMA_GLOAL1, VDMA_GLOAL1, VDMA_GLOAL1, VDMA_GLOAL1, VDMA_GLOAL1, VDMA_GLOAL1, VDMA_GLOAL2,
                                                                  VDMA_GLOAL2, VDMA_GLOAL2, VDMA_GLOAL2, VDMA_GLOAL2, VDMA_GLOAL2, VDMA_GLOAL2, VDMA_GLOAL2
                                                                 };

/*virtual fifo dma global status running bit location array */
static const uint32_t g_vdma_global_running_bit[VDMA_NUMBER] = {VDMA_CHANNEL6_OFFSET, VDMA_CHANNEL7_OFFSET, VDMA_CHANNEL8_OFFSET, VDMA_CHANNEL9_OFFSET, VDMA_CHANNEL10_OFFSET,
                                                                VDMA_CHANNEL11_OFFSET, VDMA_CHANNEL12_OFFSET, VDMA_CHANNEL13_OFFSET, VDMA_CHANNEL14_OFFSET,
                                                                VDMA_CHANNEL15_OFFSET, VDMA_CHANNEL16_OFFSET, VDMA_CHANNEL17_OFFSET, VDMA_CHANNEL18_OFFSET, VDMA_CHANNEL19_OFFSET
                                                               };

static const uint8_t dma_clock_offset_table[DMA_NUMBER] = {
    GDMA_CHANNEL1_CLK_OFFSET,
    GDMA_CHANNEL2_CLK_OFFSET,
    GDMA_CHANNEL3_CLK_OFFSET,
    GDMA_CHANNEL4_CLK_OFFSET,
    GDMA_CHANNEL5_CLK_OFFSET,
    VDMA_CHANNEL6_CLK_OFFSET,
    VDMA_CHANNEL7_CLK_OFFSET,
    VDMA_CHANNEL8_CLK_OFFSET,
    VDMA_CHANNEL9_CLK_OFFSET,
    VDMA_CHANNEL10_CLK_OFFSET,
    VDMA_CHANNEL11_CLK_OFFSET,
    VDMA_CHANNEL12_CLK_OFFSET,
    VDMA_CHANNEL13_CLK_OFFSET,
    VDMA_CHANNEL14_CLK_OFFSET,
    VDMA_CHANNEL15_CLK_OFFSET,
    VDMA_CHANNEL16_CLK_OFFSET,
    VDMA_CHANNEL17_CLK_OFFSET,
    VDMA_CHANNEL18_CLK_OFFSET,
    VDMA_CHANNEL19_CLK_OFFSET,
    PDMA_CHANNEL20_CLK_OFFSET,
    PDMA_CHANNEL21_CLK_OFFSET,
    PDMA_CHANNEL22_CLK_OFFSET,
    PDMA_CHANNEL23_CLK_OFFSET,
    PDMA_CHANNEL24_CLK_OFFSET,
    PDMA_CHANNEL25_CLK_OFFSET
};

static const uint8_t dma_interrupt_table[DMA_NUMBER] = {
    DMA_IRQ_DSP_ONLY,           /*DMA channel 0*/
    DMA_IRQ_DSP_ONLY,           /*DMA channel 1*/
    DMA_IRQ_DSP_ONLY,           /*DMA channel 2*/
    DMA_IRQ_DSP_ONLY,           /*DMA channel 3*/
    DMA_IRQ_DSP_CM4,            /*DMA channel 4*/
    DMA_IRQ_DSP_CM4,            /*DMA channel 5*/
    DMA_IRQ_DSP_CM4,            /*DMA channel 6*/
    DMA_IRQ_DSP_CM4,            /*DMA channel 7*/
    DMA_IRQ_DSP_CM4,            /*DMA channel 8*/
    DMA_IRQ_DSP_CM4,            /*DMA channel 9*/
    DMA_IRQ_DSP_CM4,            /*DMA channel 10*/
    DMA_IRQ_I2S,                /*DMA channel 11*/
    DMA_IRQ_I2S,                /*DMA channel 12*/
    DMA_IRQ_I2S,                /*DMA channel 13*/
    DMA_IRQ_I2S,                /*DMA channel 14*/
    DMA_IRQ_I2S,                /*DMA channel 15*/
    DMA_IRQ_I2S,                /*DMA channel 16*/
    DMA_IRQ_I2S,                /*DMA channel 17*/
    DMA_IRQ_I2S,                /*DMA channel 18*/
    DMA_IRQ_SENSOR,             /*DMA channel 19*/
    DMA_IRQ_SENSOR,             /*DMA channel 20*/
    DMA_IRQ_SENSOR,             /*DMA channel 21*/
    DMA_IRQ_SENSOR,             /*DMA channel 22*/
    DMA_IRQ_SENSOR,             /*DMA channel 23*/
    DMA_IRQ_SENSOR              /*DMA channel 24*/
};

static const uint8_t dma_int_offset_table[DMA_NUMBER] = {
    GDMA_CHANNEL1_INT_OFFSET,
    GDMA_CHANNEL2_INT_OFFSET,
    GDMA_CHANNEL3_INT_OFFSET,
    GDMA_CHANNEL4_INT_OFFSET,
    GDMA_CHANNEL5_INT_OFFSET,
    VDMA_CHANNEL6_INT_OFFSET,
    VDMA_CHANNEL7_INT_OFFSET,
    VDMA_CHANNEL8_INT_OFFSET,
    VDMA_CHANNEL9_INT_OFFSET,
    VDMA_CHANNEL10_INT_OFFSET,
    VDMA_CHANNEL11_INT_OFFSET,
    VDMA_CHANNEL12_INT_OFFSET,
    VDMA_CHANNEL13_INT_OFFSET,
    VDMA_CHANNEL14_INT_OFFSET,
    VDMA_CHANNEL15_INT_OFFSET,
    VDMA_CHANNEL16_INT_OFFSET,
    VDMA_CHANNEL17_INT_OFFSET,
    VDMA_CHANNEL18_INT_OFFSET,
    VDMA_CHANNEL19_INT_OFFSET,
    PDMA_CHANNEL20_INT_OFFSET,
    PDMA_CHANNEL21_INT_OFFSET,
    PDMA_CHANNEL22_INT_OFFSET,
    PDMA_CHANNEL23_INT_OFFSET,
    PDMA_CHANNEL24_INT_OFFSET,
    PDMA_CHANNEL25_INT_OFFSET
};

static const uint8_t dma_interrupt_master_cpu_table[DMA_NUMBER] = {
    DMA_FOR_DSP_0,              /*DMA channel 0--GDMA 0*/
    DMA_FOR_DSP_0,              /*DMA channel 1--GDMA 1*/
    DMA_FOR_DSP_1,              /*DMA channel 2--GDMA 2*/
    DMA_FOR_DSP_1,              /*DMA channel 3--GDMA 3*/
    DMA_FOR_CM4,                /*DMA channel 4--GDMA 4*/
    DMA_FOR_CM4,                /*DMA channel 5--UART2_TX*/
    DMA_FOR_CM4,                /*DMA channel 6--UART2_RX*/
    DMA_FOR_CM4,                /*DMA channel 7--UART1_TX*/
    DMA_FOR_CM4,                /*DMA channel 8--UART1_RX*/
    DMA_FOR_CM4,                /*DMA channel 9--UART0_TX*/
    DMA_FOR_CM4,                /*DMA channel 10--UART0_RX*/
    DMA_FOR_CM4,                /*DMA channel 11--I2S0_TX*/
    DMA_FOR_CM4,                /*DMA channel 12--I2S0_RX*/
    DMA_FOR_CM4,                /*DMA channel 13--I2S1_TX*/
    DMA_FOR_CM4,                /*DMA channel 14--I2S1_RX*/
    DMA_FOR_CM4,                /*DMA channel 15--I2S2_TX*/
    DMA_FOR_CM4,                /*DMA channel 16--I2S2_RX*/
    DMA_FOR_CM4,                /*DMA channel 17--I2S3_TX*/
    DMA_FOR_CM4,                /*DMA channel 18--I2S3_RX*/
    DMA_FOR_CM4,                /*DMA channel 19--I2C0_TX*/
    DMA_FOR_CM4,                /*DMA channel 20--I2C0_RX*/
    DMA_FOR_CM4,                /*DMA channel 21--I2C1_TX*/
    DMA_FOR_CM4,                /*DMA channel 22--I2C1_RX*/
    DMA_FOR_CM4,                /*DMA channel 23--I2C2_TX*/
    DMA_FOR_CM4                 /*DMA channel 24--I2C2_RX*/
};
/*virtual fifo dma init status*/
static volatile uint8_t vdma_init_status[VDMA_NUMBER] = {0};

/*define peripheral dma's  callback function structure*/
typedef struct {
    pdma_callback_t func;
    void *argument;
} pdma_user_callback_t;

/*peripheral dma's  callback function array*/
static pdma_user_callback_t g_pdma_callback[PDMA_NUMBER] = {
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL}
};

/*define virtual fifo  dma's  callback function structure*/
typedef struct {
    vdma_callback_t func;
    void *argument;
} vdma_user_callback_t;

/*virtual fifo dma's callback function array*/
static vdma_user_callback_t g_vdma_callback[VDMA_NUMBER] = {
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL},
    {NULL, NULL}
};

void dma_set_interrupt (uint32_t dma_channel)
{
    if(dma_interrupt_table[dma_channel] == DMA_IRQ_DSP_ONLY) {
#if defined(DMA_FOR_DSP)
        hal_nvic_register_isr_handler(DSP_DMA_IRQn, (hal_nvic_isr_t)dma_dsp_only_interrupt_hander);
        hal_nvic_enable_irq(DSP_DMA_IRQn);
#endif
    } else if(dma_interrupt_table[dma_channel] == DMA_IRQ_DSP_CM4) {
        hal_nvic_register_isr_handler(MCU_DMA_IRQn, (hal_nvic_isr_t)dma_dsp_cm4_interrupt_hander);
        hal_nvic_enable_irq(MCU_DMA_IRQn);
    } else if(dma_interrupt_table[dma_channel] == DMA_IRQ_I2S) {
        hal_nvic_register_isr_handler(I2S_DMA_IRQn, (hal_nvic_isr_t)dma_i2s_interrupt_hander);
        hal_nvic_enable_irq(I2S_DMA_IRQn);
    } else if(dma_interrupt_table[dma_channel] == DMA_IRQ_SENSOR) {
        hal_nvic_register_isr_handler(I2C_DMA_IRQn, (hal_nvic_isr_t)dma_sensor_interrupt_hander);
        hal_nvic_enable_irq(I2C_DMA_IRQn);
    }
}

uint32_t dma_enable_clock(uint8_t dma_channel)
{
    if(dma_channel >= DMA_NUMBER) {
        return INVALID_INDEX;
    }

    if(dma_channel < 4) {
        hal_clock_enable(HAL_CLOCK_CG_DSP_DMA);
    } else if (dma_channel >= 4 && dma_channel < 11) {
        hal_clock_enable(HAL_CLOCK_CG_CM4_DMA);
    } else if (dma_channel >= 11 && dma_channel < 19) {
        hal_clock_enable(HAL_CLOCK_CG_I2S_DMA);
    } else if (dma_channel >= 19 && dma_channel < 25) {
        hal_clock_enable(HAL_CLOCK_CG_I2C_DMA);
    }

    if (dma_channel <= 3) {
        *(volatile uint32_t *)(DSP_DMA_GLB_CLK_SET) = (0x1 << dma_channel);
    } else if (dma_channel >= 4 && dma_channel < 11) {
        *(volatile uint32_t *)(CM4_DMA_GLB_CLK_SET) = (0x1 << (dma_channel - 4));
    } else if (dma_channel >= 11 && dma_channel < 19) {
        *(volatile uint32_t *)(I2S_DMA_GLB_CLK_SET) = (0x1 << (dma_channel - 11));
    } else if (dma_channel >= 19 && dma_channel < 25) {
        *(volatile uint32_t *)(I2C_DMA_GLB_CLK_SET) = (0x1 << (dma_channel - 19));
    }
    return 1;
}

uint32_t dma_disable_clock(uint8_t dma_channel)
{
    if(dma_channel >= DMA_NUMBER) {
        return INVALID_INDEX;
    }

    if (dma_channel <= 3) {
        *(volatile uint32_t *)(DSP_DMA_GLB_CLK_CLR) = (0x1 << dma_channel);
    } else if (dma_channel >= 4 && dma_channel < 11) {
        *(volatile uint32_t *)(CM4_DMA_GLB_CLK_CLR) = (0x1 << (dma_channel - 4));
    } else if (dma_channel >= 11 && dma_channel < 19) {
        *(volatile uint32_t *)(I2S_DMA_GLB_CLK_CLR) = (0x1 << (dma_channel - 11));
    } else if (dma_channel >= 19 && dma_channel < 25) {
        *(volatile uint32_t *)(I2C_DMA_GLB_CLK_CLR) = (0x1 << (dma_channel - 19));
    }

    /* Always enable the PDN as the peripherals are used by multi-core */
#if 0
    if(dma_channel < 4) {
        hal_clock_disable(HAL_CLOCK_CG_DSP_DMA);
    } else if (dma_channel >= 4 && dma_channel < 11) {
        hal_clock_disable(HAL_CLOCK_CG_CM4_DMA);
    } else if (dma_channel >= 11 && dma_channel < 19) {
        hal_clock_disable(HAL_CLOCK_CG_I2S_DMA);
    } else if (dma_channel >= 19 && dma_channel < 25) {
        hal_clock_disable(HAL_CLOCK_CG_I2C_DMA);
    }
#endif

    return 1;
}

uint32_t dma_set_int_for_cpu_master(uint8_t dma_channel)
{
    if(dma_channel >= DMA_NUMBER) {
        return INVALID_INDEX;
    }

    if(dma_interrupt_master_cpu_table[dma_channel] == DMA_FOR_DSP_0) {
        if (dma_channel <= 3) {
            *(volatile uint32_t *)(DSP_DMA_BASE+CPU1_INT_SET_OFFSET) = (0x1 << dma_channel);
        } else if (dma_channel >= 4 && dma_channel < 11) {
            *(volatile uint32_t *)(CM4_DMA_BASE+CPU1_INT_SET_OFFSET) = (0x1 << (dma_channel - 4));
        } else if (dma_channel >= 11 && dma_channel < 19) {
            *(volatile uint32_t *)(I2S_DMA_BASE+CPU1_INT_SET_OFFSET) = (0x1 << (dma_channel - 11));
        } else if (dma_channel >= 19 && dma_channel < 25) {
            *(volatile uint32_t *)(I2C_DMA_BASE+CPU1_INT_SET_OFFSET) = (0x1 << (dma_channel - 19));
        }
    }

    if(dma_interrupt_master_cpu_table[dma_channel] == DMA_FOR_DSP_1) {
        if (dma_channel <= 3) {
            *(volatile uint32_t *)(DSP_DMA_BASE+CPU2_INT_SET_OFFSET) = (0x1 << dma_channel);
        } else if (dma_channel >= 4 && dma_channel < 11) {
            *(volatile uint32_t *)(CM4_DMA_BASE+CPU2_INT_SET_OFFSET) = (0x1 << (dma_channel - 4));
        } else if (dma_channel >= 11 && dma_channel < 19) {
            *(volatile uint32_t *)(I2S_DMA_BASE+CPU2_INT_SET_OFFSET) = (0x1 << (dma_channel - 11));
        } else if (dma_channel >= 19 && dma_channel < 25) {
            *(volatile uint32_t *)(I2C_DMA_BASE+CPU2_INT_SET_OFFSET) = (0x1 << (dma_channel - 19));
        }
    }

    if(dma_interrupt_master_cpu_table[dma_channel] == DMA_FOR_CM4) {
        if (dma_channel <= 3) {
            *(volatile uint32_t *)(DSP_DMA_BASE+CPU0_INT_SET_OFFSET) = (0x1 << dma_channel);
        } else if (dma_channel >= 4 && dma_channel < 11) {
            *(volatile uint32_t *)(CM4_DMA_BASE+CPU0_INT_SET_OFFSET) = (0x1 << (dma_channel - 4));
        } else if (dma_channel >= 11 && dma_channel < 19) {
            *(volatile uint32_t *)(I2S_DMA_BASE+CPU0_INT_SET_OFFSET) = (0x1 << (dma_channel - 11));
        } else if (dma_channel >= 19 && dma_channel < 25) {
            *(volatile uint32_t *)(I2C_DMA_BASE+CPU0_INT_SET_OFFSET) = (0x1 << (dma_channel - 19));
        }
    }
    return 1;
}

uint32_t dma_clear_int_for_cpu_master(uint8_t dma_channel)
{
    if(dma_channel >= DMA_NUMBER) {
        return INVALID_INDEX;
    }

    if(dma_interrupt_master_cpu_table[dma_channel] == DMA_FOR_DSP_0) {
        if (dma_channel <= 3) {
            *(volatile uint32_t *)(DSP_DMA_BASE+CPU1_INT_CLR_OFFSET) = (0x1 << dma_channel);
        } else if (dma_channel >= 4 && dma_channel < 11) {
            *(volatile uint32_t *)(CM4_DMA_BASE+CPU1_INT_CLR_OFFSET) = (0x1 << (dma_channel - 4));
        } else if (dma_channel >= 11 && dma_channel < 19) {
            *(volatile uint32_t *)(I2S_DMA_BASE+CPU1_INT_CLR_OFFSET) = (0x1 << (dma_channel - 11));
        } else if (dma_channel >= 19 && dma_channel < 25) {
            *(volatile uint32_t *)(I2C_DMA_BASE+CPU1_INT_CLR_OFFSET) = (0x1 << (dma_channel - 19));
        }
    }

    if(dma_interrupt_master_cpu_table[dma_channel] == DMA_FOR_DSP_1) {
        if (dma_channel <= 3) {
            *(volatile uint32_t *)(DSP_DMA_BASE+CPU2_INT_CLR_OFFSET) = (0x1 << dma_channel);
        } else if (dma_channel >= 4 && dma_channel < 11) {
            *(volatile uint32_t *)(CM4_DMA_BASE+CPU2_INT_CLR_OFFSET) = (0x1 << (dma_channel - 4));
        } else if (dma_channel >= 11 && dma_channel < 19) {
            *(volatile uint32_t *)(I2S_DMA_BASE+CPU2_INT_CLR_OFFSET) = (0x1 << (dma_channel - 11));
        } else if (dma_channel >= 19 && dma_channel < 25) {
            *(volatile uint32_t *)(I2C_DMA_BASE+CPU2_INT_CLR_OFFSET) = (0x1 << (dma_channel - 19));
        }
    }

    if(dma_interrupt_master_cpu_table[dma_channel] == DMA_FOR_CM4) {
        if (dma_channel <= 3) {
            *(volatile uint32_t *)(DSP_DMA_BASE+CPU0_INT_CLR_OFFSET) = (0x1 << dma_channel);
        } else if (dma_channel >= 4 && dma_channel < 11) {
            *(volatile uint32_t *)(CM4_DMA_BASE+CPU0_INT_CLR_OFFSET) = (0x1 << (dma_channel - 4));
        } else if (dma_channel >= 11 && dma_channel < 19) {
            *(volatile uint32_t *)(I2S_DMA_BASE+CPU0_INT_CLR_OFFSET) = (0x1 << (dma_channel - 11));
        } else if (dma_channel >= 19 && dma_channel < 25) {
            *(volatile uint32_t *)(I2C_DMA_BASE+CPU0_INT_CLR_OFFSET) = (0x1 << (dma_channel - 19));
        }
    }
    return 1;
}

/*************************************************************************************************************/
/************************************peripheral  dma function start  line********************************************/
/*************************************************************************************************************/

static uint32_t pdma_check_valid_channel(pdma_channel_t channel)
{
    /*define  peripheral dma base adddress array offset*/
    uint32_t offset = 0;

    /*calucate  current channel 's  offset in peripheral dma base adddress array */
    offset = channel - PDMA_START_CHANNEL;

    if (offset >= PDMA_NUMBER) {
        return INVALID_STATUS;
    } else {
        return offset;
    }
}

/* init peripheral dma pdn enable */
pdma_status_t pdma_init(pdma_channel_t channel)
{
    uint32_t offset = 0;
    uint32_t result;
    offset = pdma_check_valid_channel(channel);

    if (0xff == offset) {
        return PDMA_ERROR_CHANNEL;
    }

    /*check pdma init*/
    PDMA_CHECK_AND_SET_INIT(offset);

    /*un-gating peripheral dma clock*/
    result = dma_enable_clock(channel);

    if (0xff == result) {
        return PDMA_ERROR;
    }

    result= dma_set_int_for_cpu_master(channel);

    if (INVALID_INDEX == result) {
        return PDMA_ERROR;
    }

    /*reset pdma default setting*/
    pdma[offset]->PDMA_CON_UNION.PDMA_CON= 0x0;
    pdma[offset]->PDMA_COUNT = 0x0;
    pdma[offset]->PDMA_START = 0x0;
    pdma[offset]->PDMA_INTSTA = 0x0;
    pdma[offset]->PDMA_PGMADDR = 0x0;

    /*register isr handler*/
    dma_set_interrupt(channel);

    return  PDMA_OK;
}

/*de-init peripheral dma */
pdma_status_t pdma_deinit(pdma_channel_t channel)
{
    uint32_t offset = 0;
    uint32_t result;
    uint32_t saved_mask;
    /*define peripheral dma global status tmp variable*/
    volatile uint32_t  global_status = 0;
    offset = pdma_check_valid_channel(channel);
    if (0xff == offset) {
        return PDMA_ERROR_CHANNEL;
    }

    hal_nvic_save_and_set_interrupt_mask(&saved_mask);
    global_status = pdma_global[offset]->PDMA_GLBSTA;

    if (global_status & (0x1 << (g_pdma_global_running_bit[offset] + 1))) {

        assert(0);
        hal_nvic_restore_interrupt_mask(saved_mask);
        return PDMA_INVALID_PARAMETER;
    }

    /*check if this pdma channel  is running now  */
    if (global_status & (0x1 << g_pdma_global_running_bit[offset])) {

        assert(0);
        hal_nvic_restore_interrupt_mask(saved_mask);
        return PDMA_INVALID_PARAMETER;
    }
    hal_nvic_restore_interrupt_mask(saved_mask);

    /*set pdma idle*/
    PDMA_SET_IDLE(offset);

    result= dma_clear_int_for_cpu_master(channel);

    if (INVALID_INDEX == result) {
        return PDMA_ERROR;
    }

    /*gating peripheral dma clock*/
    result = dma_disable_clock(channel);
    if (0xff == result) {
        return PDMA_ERROR;
    }
    return  PDMA_OK;
}

pdma_status_t pdma_set_ring_buffer(pdma_channel_t channel, uint32_t wppt, uint32_t wpto)
{
    uint32_t offset = 0;

    offset = pdma_check_valid_channel(channel);
    if (0xff == offset) {
        return PDMA_ERROR_CHANNEL;
    }

    /* check wppt value valid or not */
    if (wppt > 0xFFFF) {
        return PDMA_INVALID_PARAMETER;
    }

    /* set wppt value in wrap point address register */
    pdma[offset]->PDMA_WPPT = wppt;

    /* set wpto value in wrap to address register */
    pdma[offset]->PDMA_WPTO = wpto;

    return PDMA_OK;
}

pdma_status_t pdma_configure(pdma_channel_t channel, pdma_config_t *pdma_config)
{
    /* define  peripheral dma base adddress array offset */
    uint32_t offset = 0;

    uint32_t saved_mask;

    /* define  peripheral dma control tmp variable */
    uint32_t dma_control = 0;

    /*define peripheral dma global status tmp variable*/
    volatile uint32_t  global_status = 0;

    offset = pdma_check_valid_channel(channel);
    if (0xff == offset) {
        return PDMA_ERROR_CHANNEL;
    }

    /* burst mode if true*/
    if (pdma_config->burst_mode == true) {
        dma_control |= PDMA_CON_BURST_4BEAT_MASK;
    }
    /*single mode if false*/
    else if (pdma_config->burst_mode == false) {
        dma_control &= ~PDMA_CON_BURST_MASK;
    } else {
        return PDMA_INVALID_PARAMETER;
    }

    hal_nvic_save_and_set_interrupt_mask(&saved_mask);

    /* check whether pdma is in running mode  */
    global_status = pdma_global[offset]->PDMA_GLBSTA;

    if (global_status & (0x1 << g_pdma_global_running_bit[offset])) {
        /*pdma is running now,assert here may be better*/
        hal_nvic_restore_interrupt_mask(saved_mask);
        return PDMA_ERROR;
    }

    /*check wether pdma 's interrrupt is triggered*/
    if (global_status & (0x1 << (g_pdma_global_running_bit[offset] + 1))) {
        /*pdma's interrupt is triggered ,assert here may be better*/
        hal_nvic_restore_interrupt_mask(saved_mask);
        return PDMA_ERROR;
    }
    hal_nvic_restore_interrupt_mask(saved_mask);

    /*make sure that  pdma is stopped before start pdma*/
    pdma[offset]->PDMA_START &= ~PDMA_START_BIT_MASK;

    switch (pdma_config->master_type) {
        case  PDMA_TX:
            dma_control |= PDMA_CON_SINC_MASK;
            break;
        case  PDMA_RX:
            dma_control |= PDMA_CON_DIR_MASK;
            dma_control |= PDMA_CON_DINC_MASK;
            break;
        case  PDMA_TX_RINGBUFF:
            dma_control |= PDMA_CON_SINC_MASK;
            dma_control |= PDMA_CON_WPEN_MASK;
            break;
        case  PDMA_RX_RINGBUFF:
            dma_control |= PDMA_CON_DIR_MASK;
            dma_control |= PDMA_CON_SINC_MASK;
            dma_control |= PDMA_CON_WPSD_MASK;
            dma_control |= PDMA_CON_WPEN_MASK;
            break;
        default:
            return  PDMA_ERROR;
    }

    switch (pdma_config->size) {
        case PDMA_BYTE:
            dma_control |= PDMA_CON_SIZE_BYTE_MASK;
            break;

        case PDMA_HALF_WORD:
            dma_control |= PDMA_CON_SIZE_HALF_WORD_MASK;
            break;
        case PDMA_WORD:
            dma_control |= PDMA_CON_SIZE_WORD_MASK;
            break;
        default:
            return  PDMA_ERROR;
    }

    if ((pdma_config->burst_mode == true) && (pdma_config->size == PDMA_BYTE)) {
        dma_control |= PDMA_CON_B2W_MASK;
    }

    dma_control |= PDMA_CON_DREQ_MASK;

    pdma[offset]->PDMA_CON_UNION.PDMA_CON = dma_control;
    pdma[offset]->PDMA_COUNT = pdma_config->count;

    return  PDMA_OK;
}

pdma_status_t pdma_start_polling(pdma_channel_t channel, uint32_t address)
{
    /* define   peripheral dma base adddress array offset */
    uint32_t offset = 0;
    uint32_t src_addr;

    pdma_running_status_t  running_status = PDMA_IDLE;

    offset = pdma_check_valid_channel(channel);
    if (0xff == offset) {
        return PDMA_ERROR_CHANNEL;
    }

    /* the address for DMA buffer must be 4 bytes aligned */
    if ((address % 4) > 0) {
        assert(0);
        return PDMA_INVALID_PARAMETER;
    }
#ifdef HAL_CACHE_MODULE_ENABLED
    /*the address for DMA buffer must be non-cacheable*/
    if (true == hal_cache_is_cacheable(address)) {
        assert(0);
        return PDMA_INVALID_PARAMETER;
    }
#endif

    src_addr = hal_memview_cm4_to_infrasys(address);

    pdma[offset]->PDMA_PGMADDR = src_addr;

    pdma[offset]->PDMA_CON_UNION.PDMA_CON_CELLS.PDMA_ITEN = 0x0;

    pdma[offset]->PDMA_START =  PDMA_START_BIT_MASK;

    pdma_get_running_status(channel, &running_status);
    while (running_status) {
        pdma_get_running_status(channel, &running_status);
    }

    pdma_stop(channel);
    return  PDMA_OK;
}

pdma_status_t pdma_start_interrupt(pdma_channel_t channel, uint32_t address)
{
    /* define  peripheral dma base adddress array offset */
    uint32_t offset = 0;
    uint32_t src_addr;

    offset = pdma_check_valid_channel(channel);
    if (0xff == offset) {
        return PDMA_ERROR_CHANNEL;
    }

    /* the address for DMA buffer must be 4 bytes aligned */
    if ((address % 4) > 0) {
        assert(0);
        return PDMA_INVALID_PARAMETER;
    }
    /*the address for DMA buffer must be non-cacheable*/
#ifdef HAL_CACHE_MODULE_ENABLED
    if (true == hal_cache_is_cacheable(address)) {
        assert(0);
        return PDMA_INVALID_PARAMETER;
    }
#endif
    src_addr = hal_memview_cm4_to_infrasys(address);
    pdma[offset]->PDMA_PGMADDR = src_addr;
    pdma[offset]->PDMA_CON_UNION.PDMA_CON_CELLS.PDMA_ITEN = 0x1;
    pdma[offset]->PDMA_START =  PDMA_START_BIT_MASK;

    return  PDMA_OK;
}

pdma_status_t pdma_stop(pdma_channel_t channel)
{
    /* define  peripheral dma base adddress array offset */
    uint32_t offset = 0;

    offset = pdma_check_valid_channel(channel);
    if (0xff == offset) {
        return PDMA_ERROR_CHANNEL;
    }

    pdma[offset]->PDMA_START &= ~PDMA_START_BIT_MASK;
    pdma[offset]->PDMA_ACKINT = PDMA_ACKINT_BIT_MASK;

    return  PDMA_OK;
}

pdma_status_t pdma_register_callback(pdma_channel_t channel, pdma_callback_t callback, void *user_data)
{
    /* define  peripheral dma base adddress array offset */
    uint32_t offset = 0;

    offset = pdma_check_valid_channel(channel);
    if (0xff == offset) {
        return PDMA_ERROR_CHANNEL;
    }
    g_pdma_callback[offset].func = callback;
    g_pdma_callback[offset].argument  = user_data;

    return PDMA_OK;
}
pdma_status_t pdma_get_running_status(pdma_channel_t channel, pdma_running_status_t *running_status)
{
    /* define  peripheral dma base adddress array offset */
    uint32_t offset = 0;

    /*define peripheral dma global status tmp variable*/
    volatile uint32_t  global_status = 0;

    offset = pdma_check_valid_channel(channel);
    if (0xff == offset) {
        return PDMA_ERROR_CHANNEL;
    }

    /*  read pdma running  status   */
    global_status = pdma_global[offset]->PDMA_GLBSTA;

    if (global_status & (0x1 << g_pdma_global_running_bit[offset])) {

        *running_status = PDMA_BUSY;
    } else {
        *running_status = PDMA_IDLE;
    }

    return PDMA_OK;
}

pdma_status_t pdma_dump_reg_list(pdma_channel_t channel)
{
    uint32_t offset = 0;

    offset = pdma_check_valid_channel(channel);
    if (0xff == offset) {
        return PDMA_ERROR_CHANNEL;
    }

    log_hal_msgid_info("PDMA channel %d reg list dump:", 1, offset);
    log_hal_msgid_info("PDMA_WPPT = 0x%08x", 1, pdma[offset]->PDMA_WPPT);
    log_hal_msgid_info("PDMA_WPTO = 0x%08x", 1, pdma[offset]->PDMA_WPTO);
    log_hal_msgid_info("PDMA_COUNT = 0x%08x", 1, pdma[offset]->PDMA_COUNT);
    log_hal_msgid_info("PDMA_CON = 0x%08x", 1, pdma[offset]->PDMA_CON_UNION.PDMA_CON);
    log_hal_msgid_info("PDMA_START = 0x%08x", 1, pdma[offset]->PDMA_START);
    log_hal_msgid_info("PDMA_INTSTA = 0x%08x", 1, pdma[offset]->PDMA_INTSTA);
    log_hal_msgid_info("PDMA_RLCT = 0x%08x", 1, pdma[offset]->PDMA_RLCT);
    log_hal_msgid_info("PDMA_LIMITER = 0x%08x", 1, pdma[offset]->PDMA_LIMITER);
    log_hal_msgid_info("PDMA_PGMADDR = 0x%08x", 1, pdma[offset]->PDMA_PGMADDR);

    return PDMA_OK;
}

/*************************************************************************************************************/
/************************************peripheral  dma function end line**********************************************/
/*************************************************************************************************************/



/*************************************************************************************************************/
/************************************virtual fifo dma function start line*********************************************/
/*************************************************************************************************************/

static uint32_t vdma_check_valid_channel(vdma_channel_t channel)
{
    /*define  peripheral dma base adddress array offset*/
    uint32_t offset = 0;

    /*calucate  current channel 's  offset in peripheral dma base adddress array */
    offset = channel - VDMA_START_CHANNEL;

    if (offset > (VDMA_NUMBER - 1)) {
        return INVALID_STATUS;
    } else {
        return offset;
    }
}

vdma_status_t vdma_init(vdma_channel_t channel)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;
    uint32_t result;
    offset = vdma_check_valid_channel(channel);
    if (0xff == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    /*check vdma init*/
    VDMA_CHECK_AND_SET_INIT(offset);

    /*default setting */
    vdma[offset]->VDMA_CON_UNION.VDMA_CON= 0x0;
    vdma[offset]->VDMA_ACKINT = 0x8000;
    vdma[offset]->VDMA_COUNT = 0x0;
    vdma[offset]->VDMA_START = 0x0;
    vdma[offset]->VDMA_INTSTA = 0x0;
    vdma[offset]->VDMA_PGMADDR = 0x0;
    vdma[offset]->VDMA_ALTLEN = 0x0;
    vdma[offset]->VDMA_FFSIZE = 0x0;

    *(volatile uint32_t *)(0xa2270350) = 0x1c00;

    /*un-gating vfifo dma clock*/
    result = dma_enable_clock(channel);

    if (0xff == result) {
        return VDMA_ERROR;
    }

    result= dma_set_int_for_cpu_master(channel);

    if (INVALID_INDEX == result) {
        return VDMA_ERROR;
    }

    switch (channel) {
        case  VDMA_UART2TX:
            vdma[0]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[0]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x0;
            break;

        case  VDMA_UART2RX:
            vdma[1]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_DIRECTION = 0x10;
            vdma[1]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[1]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x0;
            break;

        case  VDMA_UART1TX:
            vdma[2]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[2]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x0;
            break;

        case  VDMA_UART1RX:
            vdma[3]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_DIRECTION = 0x10;
            vdma[3]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[3]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x0;
            break;

        case  VDMA_UART0TX:
            vdma[4]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[4]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x0;
            break;

        case  VDMA_UART0RX:
            vdma[5]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_DIRECTION = 0x10;
            vdma[5]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[5]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x0;
            break;

        case  VDMA_I2S0TX:
            vdma[6]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[6]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x2;
            break;

        case  VDMA_I2S0RX:
            vdma[7]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_DIRECTION = 0x10;
            vdma[7]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[7]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x2;
            break;
        case  VDMA_I2S1TX:
            vdma[8]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[8]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x2;
            break;

        case  VDMA_I2S1RX:
            vdma[9]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_DIRECTION = 0x10;
            vdma[9]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[9]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x2;
            break;

        case  VDMA_I2S2TX:
            vdma[10]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[10]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x2;
            break;

        case  VDMA_I2S2RX:
            vdma[11]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_DIRECTION = 0x10;
            vdma[11]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[11]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x2;
            break;
        case  VDMA_I2S3TX:
            vdma[12]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[12]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x2;
            break;

        case  VDMA_I2S3RX:
            vdma[13]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_DIRECTION = 0x10;
            vdma[13]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SETTING = 0x1;
            vdma[13]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE = 0x2;
            break;

        default:
            return  VDMA_ERROR;
    }

    dma_set_interrupt(channel);

    return VDMA_OK;
}

vdma_status_t vdma_deinit(vdma_channel_t channel)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = INVALID_STATUS;
    uint32_t result;
    uint32_t saved_mask;

    /*define virtual fifo dma global status tmp variable*/
    volatile uint32_t  global_status = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    hal_nvic_save_and_set_interrupt_mask(&saved_mask);

    global_status = vdma_global[offset]->VDMA_GLBSTA;

    if (global_status & (0x1 << (g_vdma_global_running_bit[offset] + 1))) {
        assert(0);
        hal_nvic_restore_interrupt_mask(saved_mask);
        return VDMA_ERROR;

    }
    if (global_status & (0x1 << g_vdma_global_running_bit[offset])) {
        assert(0);
        hal_nvic_restore_interrupt_mask(saved_mask);
        return VDMA_ERROR;

    }
    hal_nvic_restore_interrupt_mask(saved_mask);

    /*set vdma idle*/
    VDMA_SET_IDLE(offset);

    result= dma_clear_int_for_cpu_master(channel);

    if (INVALID_INDEX == result) {
        return VDMA_ERROR;
    }

    /*gating vfifo dma clock*/
    result = dma_disable_clock(channel);
    if (INVALID_STATUS == result) {
        return VDMA_ERROR;
    }

    return VDMA_OK;
}

vdma_status_t vdma_configure(vdma_channel_t channel, vdma_config_t *vdma_config)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;
    uint32_t src_addr;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    /* the address for DMA buffer must be 4 bytes aligned */
    if ((vdma_config->base_address % 4) > 0) {
        assert(0);
        return VDMA_INVALID_PARAMETER;
    }
    /*the address for DMA buffer must be non-cacheable*/
#ifdef HAL_CACHE_MODULE_ENABLED
    if (true == hal_cache_is_cacheable(vdma_config->base_address)) {
        assert(0);
        return VDMA_INVALID_PARAMETER;
    }
#endif

    src_addr = hal_memview_cm4_to_infrasys(vdma_config->base_address);
    vdma[offset]->VDMA_FFSIZE = vdma_config->size;
    vdma[offset]->VDMA_PGMADDR = src_addr;

    return VDMA_OK;
}

vdma_status_t vdma_enable_interrupt(vdma_channel_t channel)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    vdma[offset]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_ITEN = 0x1;
    return VDMA_OK;
}

vdma_status_t vdma_start(vdma_channel_t channel)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;
    uint32_t saved_mask;
    /*define virtual fifol dma global status tmp variable*/
    volatile uint32_t  global_status = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    hal_nvic_save_and_set_interrupt_mask(&saved_mask);
    /* check whether vdma is in running mode  */
    global_status = vdma_global[offset]->VDMA_GLBSTA;

    if (global_status & (0x1 << g_vdma_global_running_bit[offset])) {
        /*vdma is running now,assert here may be better*/
        hal_nvic_restore_interrupt_mask(saved_mask);
        return VDMA_ERROR;
    }

    /*check wether vdma 's interrrupt is triggered*/
    if (global_status & (0x1 << (g_vdma_global_running_bit[offset] + 1))) {
        /*vdma's interrupt is triggered ,assert here may be better*/
        hal_nvic_restore_interrupt_mask(saved_mask);
        return VDMA_ERROR;
    }
    hal_nvic_restore_interrupt_mask(saved_mask);

    vdma[offset]->VDMA_START = VDMA_START_BIT_MASK;
    return VDMA_OK;
}

vdma_status_t vdma_disable_interrupt(vdma_channel_t channel)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    vdma[offset]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_ITEN = 0x0;
    return VDMA_OK;
}

vdma_status_t vdma_get_interrupt_status(vdma_channel_t channel, uint8_t *is_enabled)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;
    offset = channel - VDMA_START_CHANNEL;
    *is_enabled = (vdma[offset]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_ITEN & 0x1);
    return VDMA_OK;
}

vdma_status_t vdma_stop(vdma_channel_t channel)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    vdma[offset]->VDMA_START = VDMA_STOP_BIT_MASK;
    vdma[offset]->VDMA_SW_MV_BYTE = vdma[offset]->VDMA_FFCNT<<vdma[offset]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE;
    return VDMA_OK;
}

vdma_status_t vdma_set_threshold(vdma_channel_t channel, uint32_t threshold)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;
    uint8_t h_size = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    h_size = vdma[offset]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE;
    threshold = threshold >> h_size;

    if (threshold > VDMA_MAX_THRESHOLD) {
        return  VDMA_INVALID_PARAMETER;
    }
    vdma[offset]->VDMA_COUNT = threshold;

    return VDMA_OK;
}

vdma_status_t vdma_set_alert_length(vdma_channel_t channel, uint32_t alert_length)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;
    uint8_t h_size = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    h_size = vdma[offset]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE;
    alert_length = alert_length >> h_size;

    if (alert_length > VDMA_MAX_ALERT_LENGTH) {
        return  VDMA_INVALID_PARAMETER;
    }
    vdma[offset]->VDMA_ALTLEN = alert_length;

    return VDMA_OK;
}

vdma_status_t vdma_push_data(vdma_channel_t channel, uint8_t data)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;
    uint8_t *write_address =0x0;
    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    write_address = (uint8_t *)vdma[offset]->VDMA_WRPTR;

    /*put data into virtual fifo dma */

    *write_address= data;

    vdma_set_sw_move_byte(channel,1);

    return VDMA_OK;
}

vdma_status_t vdma_push_data_4bytes(vdma_channel_t channel, uint32_t data)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;
    uint32_t  *write_address =0x0;
    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    write_address = (uint32_t *)vdma[offset]->VDMA_WRPTR;

    /*put data into virtual fifo dma */

    *write_address= data;
    vdma_set_sw_move_byte(channel,4);

    return VDMA_OK;
}

vdma_status_t vdma_push_data_multi_bytes(vdma_channel_t channel, uint8_t *data, uint32_t size)
{
    uint32_t offset = 0;
    uint8_t  *write_address = 0x0;
    uint32_t byte_to_bndry = 0;
    uint32_t byte_avail = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    byte_avail = vdma[offset]->VDMA_BYTE_AVAIL;
    if (size > byte_avail) {
        return VDMA_INVALID_PARAMETER;
    }

    write_address = (uint8_t *)vdma[offset]->VDMA_WRPTR;
    byte_to_bndry = vdma[offset]->VDMA_BYTE_TO_BNDRY;

    if (size > byte_to_bndry) {
        memcpy(write_address, data, byte_to_bndry);
        write_address = (uint8_t *)vdma[offset]->VDMA_PGMADDR;
        memcpy(write_address, (data + byte_to_bndry), (size - byte_to_bndry));
    } else {
        memcpy(write_address, data, size);
    }
    vdma_set_sw_move_byte(channel, size);

    return VDMA_OK;
}

vdma_status_t vdma_pop_data(vdma_channel_t channel, uint8_t *data)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;
    uint8_t *read_address =0x0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    read_address = (uint8_t *)vdma[offset]->VDMA_RDPTR;

    /*get data from  virtual fifo dma */

    *data = *read_address ;
    vdma_set_sw_move_byte(channel,1);

    return VDMA_OK;
}

vdma_status_t vdma_pop_data_4bytes(vdma_channel_t channel, uint32_t *data)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;
    uint32_t  *read_address =0x0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    read_address = (uint32_t *)vdma[offset]->VDMA_RDPTR;

    /*get data from  virtual fifo dma */

    *data = *read_address ;
    vdma_set_sw_move_byte(channel,4);

    return VDMA_OK;
}

vdma_status_t vdma_pop_data_multi_bytes(vdma_channel_t channel, uint8_t *data, uint32_t size)
{
    uint32_t offset = 0;
    uint8_t  *read_address = 0x0;
    uint32_t ready_for_read = 0;
    uint32_t byte_to_bndry = 0;
    uint8_t h_size = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    h_size = vdma[offset]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE;

    read_address = (uint8_t *)vdma[offset]->VDMA_RDPTR;
    ready_for_read = (vdma[offset]->VDMA_FFCNT << h_size);
    if (size > ready_for_read) {
        return VDMA_INVALID_PARAMETER;
    }

    byte_to_bndry = vdma[offset]->VDMA_BYTE_TO_BNDRY;

    if (size > byte_to_bndry) {
        memcpy(data, read_address, byte_to_bndry);
        read_address = (uint8_t *)vdma[offset]->VDMA_PGMADDR;
        memcpy((data + byte_to_bndry), read_address, (size - byte_to_bndry));
    } else {
        memcpy(data, read_address, size);
    }

    /*pop data from virtual fifo dma */
    vdma_set_sw_move_byte(channel, size);

    return VDMA_OK;
}

vdma_status_t vdma_get_available_receive_bytes(vdma_channel_t channel, uint32_t *receive_bytes)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;
    uint8_t h_size = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    h_size = vdma[offset]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE;

    *receive_bytes = (vdma[offset]->VDMA_FFCNT << h_size);

    return   VDMA_OK;
}

vdma_status_t vdma_get_available_send_space(vdma_channel_t channel, uint32_t *available_space)
{
    /*define virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;
    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    *available_space = vdma[offset]->VDMA_BYTE_AVAIL;

    return VDMA_OK;
}

vdma_status_t vdma_get_hw_read_point(vdma_channel_t channel, uint32_t *read_point)
{
    /*define virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    *read_point = vdma[offset]->VDMA_RDPTR;

    return VDMA_OK;
}

vdma_status_t vdma_set_sw_move_byte(vdma_channel_t channel, uint16_t sw_move_byte)
{
    /*define virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;
    uint8_t h_size = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    h_size = vdma[offset]->VDMA_CON_UNION.VDMA_CON_CELLS.VDMA_SIZE;
    switch (h_size) {
        case 0x2:
            if ((sw_move_byte & 0x3) != 0x0) {
                return VDMA_INVALID_PARAMETER;
            }
            break;
        default:
            break;
    }

    vdma[offset]->VDMA_SW_MV_BYTE = (sw_move_byte >> h_size);

    return VDMA_OK;
}

vdma_status_t vdma_get_hw_write_point(vdma_channel_t channel, uint32_t *write_point)
{
    /*define virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    *write_point = vdma[offset]->VDMA_WRPTR;

    return VDMA_OK;
}

vdma_status_t vdma_register_callback(vdma_channel_t channel, vdma_callback_t callback, void *user_data)
{
    /*define  virtual fifo dma base adddress array offset*/
    uint32_t offset = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    g_vdma_callback[offset].func = callback;
    g_vdma_callback[offset].argument  = user_data;

    return VDMA_OK;
}

vdma_status_t vdma_dump_reg_list(vdma_channel_t channel)
{
    uint32_t offset = 0;

    offset = vdma_check_valid_channel(channel);
    if (INVALID_STATUS == offset) {
        return VDMA_ERROR_CHANNEL;
    }

    log_hal_msgid_info("VDMA channel %d reg list dump:", 1, offset);
    log_hal_msgid_info("VDMA_COUNT = 0x%08x", 1, vdma[offset]->VDMA_COUNT);
    log_hal_msgid_info("VDMA_CON = 0x%08x", 1, vdma[offset]->VDMA_CON_UNION.VDMA_CON);
    log_hal_msgid_info("VDMA_START = 0x%08x", 1, vdma[offset]->VDMA_START);
    log_hal_msgid_info("VDMA_INTSTA = 0x%08x", 1, vdma[offset]->VDMA_INTSTA);
    log_hal_msgid_info("VDMA_LIMITER = 0x%08x", 1, vdma[offset]->VDMA_LIMITER);
    log_hal_msgid_info("VDMA_PGMADDR = 0x%08x", 1, vdma[offset]->VDMA_PGMADDR);
    log_hal_msgid_info("VDMA_WRPTR = 0x%08x", 1, vdma[offset]->VDMA_WRPTR);
    log_hal_msgid_info("VDMA_RDPTR = 0x%08x", 1, vdma[offset]->VDMA_RDPTR);
    log_hal_msgid_info("VDMA_FFCNT = 0x%08x", 1, vdma[offset]->VDMA_FFCNT);
    log_hal_msgid_info("VDMA_FFSTA = 0x%08x", 1, vdma[offset]->VDMA_FFSTA);
    log_hal_msgid_info("VDMA_ALTLEN = 0x%08x", 1, vdma[offset]->VDMA_ALTLEN);
    log_hal_msgid_info("VDMA_FFSIZE = 0x%08x", 1, vdma[offset]->VDMA_FFSIZE);
    log_hal_msgid_info("VDMA_BNDRY_ADDR = 0x%08x", 1, vdma[offset]->VDMA_BNDRY_ADDR);
    log_hal_msgid_info("VDMA_BYTE_TO_BNDRY = 0x%08x", 1, vdma[offset]->VDMA_BYTE_TO_BNDRY);
    log_hal_msgid_info("VDMA_BYTE_AVAIL = 0x%08x", 1, vdma[offset]->VDMA_BYTE_AVAIL);

    return VDMA_OK;
}

static sw_dma_callback_t g_sw_dma_callback;
void sw_dma_register_callback(sw_dma_callback_t callback) {
    g_sw_dma_callback = callback;
}

void dma_dsp_only_interrupt_hander(void)
{
    /*define tmp callback variable */
    sw_dma_callback_t  sw_dma_callback;
    if (sw_dma_channel_has_interrupt(SW_DMA_CHANNEL) == true) {
        SW_DMA_REG_ACKINT_DSP(SW_DMA_CHANNEL) = 0x8000;
        SW_DMA_REG_START_DSP(SW_DMA_CHANNEL)  = 0x0;
#ifdef HAL_SLEEP_MANAGER_ENABLED
        hal_sleep_manager_unlock_sleep(SLEEP_LOCK_DMA);
#endif
        sw_dma_callback = g_sw_dma_callback;
        if (sw_dma_callback != NULL) {
            sw_dma_callback();
        } else {
            assert(0);
        }
    }
}

void dma_dsp_cm4_interrupt_hander(void)
{
    /*define peripheral dma global status tmp variable*/
    volatile uint32_t  global_status = 0;
    void *argument;
    /*define tmp callback variable */
    vdma_callback_t     vdma_callback;
    /*define tmp variable for loop count*/
    static uint32_t count = 0;

    /*define tmp callback variable */
    sw_dma_callback_t  sw_dma_callback;
    if (sw_dma_channel_has_interrupt(SW_DMA_CHANNEL) == true) {
        SW_DMA_REG_ACKINT_CM4(SW_DMA_CHANNEL) = 0x8000;
        SW_DMA_REG_START_CM4(SW_DMA_CHANNEL)  = 0x0;
#ifdef HAL_SLEEP_MANAGER_ENABLED
        hal_sleep_manager_unlock_sleep(SLEEP_LOCK_DMA);
#endif
        sw_dma_callback = g_sw_dma_callback;
        if (sw_dma_callback != NULL) {
            sw_dma_callback();
        } else {
            assert(0);
        }
    }

    /*step 2: handle vdma for DSP and CM4*/
    for (count = 0; count < VDMA_NUMBER_FOR_DSP_CM4; count++) {
        global_status = vdma_global[count]->VDMA_GLBSTA;
        if (global_status & (0x1 << (g_vdma_global_running_bit[count] + 1))) {
            /*clear vdma interrupt*/
            vdma[count]->VDMA_ACKINT = VDMA_ACKINT_BIT_MASK;
            /*call vdma user's callback */
            vdma_callback = g_vdma_callback[count].func;
            argument = g_vdma_callback[count].argument;
            if (vdma_callback != NULL) {
                vdma_callback(VDMA_EVENT_TRANSACTION_SUCCESS, argument);
            } else {
                assert(0);
            }
        }
    }
}

void dma_i2s_interrupt_hander(void)
{
    /*define peripheral dma global status tmp variable*/
    volatile uint32_t  global_status = 0;

    /*define tmp callback variable */
    vdma_callback_t     vdma_callback;
    void *argument;

    /*define tmp variable for loop count*/
    static uint32_t count = 0;

    /* handle vdma for I2S */
    for (count = VDMA_NUMBER_FOR_DSP_CM4; count < VDMA_NUMBER; count++) {
        global_status = vdma_global[count]->VDMA_GLBSTA;
        if (global_status & (0x1 << (g_vdma_global_running_bit[count] + 1))) {
            /*clear vdma interrupt*/
            vdma[count]->VDMA_ACKINT = VDMA_ACKINT_BIT_MASK;
            /*call vdma user's callback */
            vdma_callback = g_vdma_callback[count].func;
            argument = g_vdma_callback[count].argument;
            if (vdma_callback != NULL) {
                vdma_callback(VDMA_EVENT_TRANSACTION_SUCCESS, argument);
            } else {
                assert(0);
            }
        }
    }
}

void dma_sensor_interrupt_hander(void)
{
    /*define peripheral dma global status tmp variable*/
    volatile uint32_t  global_status = 0;

    /*define tmp callback variable */
    pdma_callback_t     pdma_callback;
    void *argument;

    /*define tmp variable for loop count*/
    static uint32_t count = 0;
    /*mask PD DMA interrupt */

    for (count = 0; count < PDMA_NUMBER; count++) {
        global_status = pdma_global[count]->PDMA_GLBSTA;
        if (global_status & (0x1 << (g_pdma_global_running_bit[count] + 1))) {
            /*clear pdma interrupt*/
            pdma[count]->PDMA_ACKINT = PDMA_ACKINT_BIT_MASK;
            pdma[count]->PDMA_START &= ~PDMA_START_BIT_MASK;
            /*call pdma user's callback */
            pdma_callback = g_pdma_callback[count].func;
            argument = g_pdma_callback[count].argument;
            if (pdma_callback != NULL) {
                pdma_callback(PDMA_EVENT_TRANSACTION_SUCCESS, argument);
            } else {
                assert(0);
            }
        }
    }
}


