/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#include "hal_clock.h"
#include "hal_clock_internal.h"

#ifdef HAL_CLOCK_MODULE_ENABLED
#include "hal_nvic.h"

#define lsb(reg)                (reg ^ (reg & (reg-1)))
#define base_sorting(base)      (base << 2)
//#include <stdio.h>
#include <assert.h>
#include "hal_log.h"
#include "hal_gpt.h"
uint8_t clock_set_pll_on(clock_pll_id pll_id);
uint8_t clock_set_pll_off(clock_pll_id pll_id);
ATTR_RWDATA_IN_TCM static volatile bool upll_is_624M = false;
#ifndef __EXT_BOOTLOADER__
ATTR_TEXT_IN_TCM static uint8_t clk_pll_on(clock_pll_id pll_id);
ATTR_TEXT_IN_TCM static uint8_t clk_pll_off(clock_pll_id pll_id);

#define eof_div_tbl(tbl)        &((((clk_div_info*)tbl)->field).div_max)
ATTR_RWDATA_IN_TCM static clock_domain_t clock_table = {
    .top_mux_info_list = {
        {.cur_sel_0 = NULL,
         .cur_sel_1 = NULL,
         .cur_sel_2 = NULL,
         .mux_sels = {{CLK_XO,   CLK_DIV_NONE},     //XO_CK, 26 MHz */
                      {CLK_OSC, CLK_DIV_104M_D2},   //          1 : OSC_104M_D2_D2, 26 MHz
                      {CLK_MPLL,CLK_DIV_D2},        //          2 : MPLL_D2, 312 MHz
                      {CLK_MPLL,CLK_DIV_D3},        //          3 : MPLL_D3, 208 MHz
                      {CLK_MPLL,CLK_DIV_D2},        //          4 : MPLL_D2_D2, 156 MHz
                      {CLK_MPLL,CLK_DIV_D3},        //          5 : MPLL_D3_D2, 104 MHz
                      {CLK_MPLL,CLK_DIV_D2},        //          6 : MPLL_D2_D4, 78 MHz
                      {CLK_OSC,CLK_DIV_NONE},       //          7 : OSC_CLK, 312 MHz
                      {CLK_OSC,CLK_DIV_D2},         //          8 : OSC_D2, 156 MHz
                      {CLK_OSC,CLK_DIV_NONE},       //          9 : OSC_104M_CLK, 104 MHz
                      {CLK_OSC,CLK_DIV_D2},         //          10 : OSC_D2_D2, 78 MHz
                      {CLK_OSC,CLK_DIV_104M_D2}}    //          11 : OSC_104M_D2, 52 MHz
        },
        {.mux_sels = {{CLK_XO,  CLK_DIV_NONE},//CLK_SFC_SEL 0 : XO_CK, 26 MHz
                      {CLK_OSC, CLK_DIV_104M_D2},   //          1 : OSC_104M_D2_D2, 26 MHz
                      {CLK_MPLL,CLK_DIV_D2},        //          2 : MPLL_D2_D4, 78 MHz
                      {CLK_MPLL,CLK_DIV_D5},        //          3 : MPLL_D5_D2, 62.4 MHz
                      {CLK_MPLL,CLK_DIV_D3},        //          4 : MPLL_D3_D4, 52 MHz
                      {CLK_OSC, CLK_DIV_D5},        //          5 : OSC_D5, 62.4 MHz
                      {CLK_OSC,CLK_DIV_104M_D2},    //          6 : OSC_104M_D2, 52 MHz
                      {CLK_XO,   CLK_DIV_NONE},
                      {CLK_XO,   CLK_DIV_NONE},
                      {CLK_XO,   CLK_DIV_NONE},
                      {CLK_XO,   CLK_DIV_NONE},
                      {CLK_XO,   CLK_DIV_NONE}}
        },
        {.mux_sels = {{CLK_XO, CLK_DIV_NONE},    //CLK_SPIMST0_SEL 0 : XO_CK, 26 MHz
                     {CLK_OSC, CLK_DIV_104M_D2},    //          1 : OSC_104M_D2_D2, 26 MHz
                     {CLK_MPLL,CLK_DIV_D2},         //          2 : MPLL_D2_D4, 78 MHz
                     {CLK_MPLL,CLK_DIV_D3},         //          3 : MPLL_D3_D2, 104 MHz
                     {CLK_OSC, CLK_DIV_D2},         //          4 : OSC_D2_D2, 78 MHz
                     {CLK_OSC, CLK_DIV_104M_D2},    //          5 : OSC_104M_CLK, 104 MHz
                     {CLK_XO,  CLK_DIV_NONE},
                     {CLK_XO,  CLK_DIV_NONE},
                     {CLK_XO,  CLK_DIV_NONE},
                     {CLK_XO,  CLK_DIV_NONE},
                     {CLK_XO,  CLK_DIV_NONE},
                     {CLK_XO,  CLK_DIV_NONE}}
        },
       {.mux_sels = {{CLK_XO,   CLK_DIV_NONE},    //CLK_SDIOMST_SEL 0 : XO_CK, 26 MHz
                     {CLK_OSC,   CLK_DIV_104M_D2},   //          1 : OSC_104M_D2_D2, 26 MHz
                     {CLK_MPLL,CLK_DIV_NONE},        //          2 : MPLL_48M, 48 MHz
                     {CLK_MPLL, CLK_DIV_D7},         //          3 : MPLL_D7_D2, 44.57 MHz
                     {CLK_MPLL,CLK_DIV_D3},          //          4 : MPLL_D3_D4, 52 MHz
                     {CLK_OSC,CLK_DIV_D7},           //          5 : OSC_D7, 44.57 MHz
                     {CLK_OSC,CLK_DIV_104M_D2},      //          6 : OSC_104M_D2, 52 MHz
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE}}
        },
        {.mux_sels = {{CLK_XO,CLK_DIV_NONE},      //CLK_SPISLV_SEL 0 : XO_CK, 26 MHz
                     {CLK_OSC,CLK_DIV_104M_D2},     //           1 : OSC_104M_D2_D2, 26 MHz
                     {CLK_MPLL, CLK_DIV_D2},        //           2 : MPLL_D2_D4, 78 MHz
                     {CLK_MPLL, CLK_DIV_D2},        //           3 : MPLL_D3_D2, 104 MHz
                     {CLK_UPLL,CLK_DIV_D2},         //           4 : UPLL_D2_D2(UPLL 312M)/UPLL_D2_D4(UPLL 624M), 78 MHz
                     {CLK_UPLL,CLK_DIV_D3},         //           5 : UPLL_D3(UPLL 312M)/UPLL_D3_D2(UPLL 624M), 104 MHz
                     {CLK_OSC,CLK_DIV_D2},          //           6 : OSC_D2_D2, 78 MHz
                     {CLK_OSC,CLK_DIV_104M_D2},     //           7 : OSC_104M_CLK, 104 MHz
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE}}
        },
        {.mux_sels = {{CLK_XO, CLK_DIV_NONE},      //CLK_USB_SEL 0: XO_CK, 26 MHz
                     {CLK_UPLL,CLK_DIV_D5},        //            1 : UPLL_D5(UPLL 312M)/UPLL_D5_D2(UPLL 624M), 62.4 MHz
                     {CLK_MPLL, CLK_DIV_D5},       //            2 : MPLL_D5_D2, 62.4 MHz
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE}}
        },
        {.mux_sels = {{CLK_XO,CLK_DIV_NONE},  //CLK_AUD_BUS_SEL 0 : XO_CK, 26 MHz
                     {CLK_MPLL,CLK_DIV_D5},        //           1 : MPLL_D5, 124.8 MHz
                     {CLK_XO,   CLK_DIV_NONE},     //           2 : RESERVED
                     {CLK_UPLL, CLK_DIV_D5},       //           3 : UPLL_D5(UPLL 624M), 124.8 MHz
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE}}
        },
        {.mux_sels = {{CLK_XO,CLK_DIV_NONE},     //CLK_AUD_GPSRC_SEL 0 : XO_CK, 26 MHz
                     {CLK_MPLL,CLK_DIV_D3},         //          1 : MPLL_D3_D4, 52 MHz
                     {CLK_MPLL,CLK_DIV_D3},         //          2 : MPLL_D3_D2, 104 MHz
                     {CLK_UPLL,CLK_DIV_D3},         //          3 : UPLL_D3_D2(UPLL 312M)/UPLL_D3_D4(UPLL 624M), 52 MHz
                     {CLK_UPLL,CLK_DIV_D3},         //          4 : UPLL_D3(UPLL 312M)/UPLL_D3_D2(UPLL 624M), 104 MHz
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE}}
        },

        {.mux_sels = {{CLK_XO,CLK_DIV_NONE},      //CLK_AUD_INTERFACE0_SEL 0 : XO_CK, 26 MHz
                     {CLK_APLL1,CLK_DIV_NONE},        //          1 : APLL1_CK, 45.1584 MHz
                     {CLK_APLL2,CLK_DIV_NONE},        //          2 : APLL2_CK, 49.152 MHz
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE}}
        },
        {.mux_sels = {{CLK_XO,CLK_DIV_NONE},      //CLK_EMI_SEL 0 : XO_CK, 26 MHz
                     {CLK_OSC,CLK_DIV_104M_D2},        //          1 : OSC_104M_D2_D2, 26 MHz
                     {CLK_XO,CLK_DIV_NONE},            //          2 : Reserved
                     {CLK_MPLL,CLK_DIV_D3},            //          3 : MPLL_D3, 208 MHz
                     {CLK_MPLL,CLK_DIV_D2},            //          4 : MPLL_D2_D2, 156 MHz
                     {CLK_MPLL,CLK_DIV_D3},            //          5 : MPLL_D3_D2, 104 MHz
                     {CLK_MPLL,CLK_DIV_D2},            //          6 : MPLL_D2_D4, 78 MHz
                     {CLK_XO,CLK_DIV_NONE},            //          7 : Reserved
                     {CLK_OSC,CLK_DIV_D2},             //          8 : OSC_D2, 156 MHz
                     {CLK_OSC,CLK_DIV_NONE},           //          9 : OSC_104M_CLK, 104 MHz
                     {CLK_OSC,CLK_DIV_D2},             //          10 : OSC_D2_D2, 78 MHz
                     {CLK_OSC,CLK_DIV_104M_D2}}        //          11 : OSC_104M_D2, 52 MHz
        },
        {.mux_sels = {{CLK_XO,CLK_DIV_NONE},      //CLK_SPM_SEL    0 : XO_CK, 26 MHz
                     {CLK_OSC,CLK_DIV_104M_D2},     //             1 :OSC_104M_D2_D2, 26 MHz
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE}}
        },
        {.mux_sels = {{CLK_XO,CLK_DIV_NONE},      // CLK_26M_SEL 0 : XO_CK, 26 MHz
                     {CLK_OSC,CLK_DIV_104M_D2},   //             1 : OSC_104M_D2_D2, 26 MHz
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE}}
        },
        {.mux_sels = {{CLK_MPLL,CLK_DIV_D5},      //CLK_AUD_DL_HIRES 0:MPLL_D5, 124.8 MHz
                     {CLK_UPLL,CLK_DIV_D5},       //CLK_AUD_DL_HIRES 1:UPLL_D5(UPLL 624M), 124.8 MHz
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE}}
        },
        {.mux_sels = {{CLK_MPLL,CLK_DIV_D3},      //CLK_AUD_UL_HIRES 0:MPLL_D3, 208 MHz
                     {CLK_UPLL,CLK_DIV_D3},       //CLK_AUD_UL_HIRES 0:UPLL_D3(UPLL 624M), 208 MHz
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE}}
        },
    },
    .peri_mux_info_list = {                         //Periferal
        {.mux_sels = {{CLK_APLL1,CLK_DIV_NONE},  //CLK_MCLK0_SEL 0 : APLL1   45.1584 MHz
                     {CLK_APLL2,CLK_DIV_NONE},   //              1 : APLL2   49.152  MHz
                     {CLK_XO,   CLK_DIV_NONE},   //              2 : XO
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE}}
        },
        {.mux_sels = {{CLK_XO,  CLK_DIV_NONE},   //CLK_DUMMY_SEL 0 : XO,
                     {CLK_UPLL, CLK_DIV_NONE},   //              1 : UPLL
                     {CLK_MPLL, CLK_DIV_NONE},   //              2 : MPLL
                     {CLK_APLL1,CLK_DIV_NONE},   //              3 : APLL1
                     {CLK_APLL2,CLK_DIV_NONE},   //              4 : APLL2
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE},
                     {CLK_XO,   CLK_DIV_NONE}}
        }
    },
    .div_info ={
        .div_tbl = {
            {.field = {   // CLK_UPLL
                .div_low =    CLK_DIV_D2,
                .div_middle = CLK_DIV_D3,
                .div_high =   CLK_DIV_D5,
                .div_max =   CLK_DIV_D7}
            },
            {.field = {   // CLK_MPLL
                .div_low =    CLK_DIV_D2,
                .div_middle = CLK_DIV_D3,
                .div_high =   CLK_DIV_D5,
                .div_max =   CLK_DIV_D7}
            },
            {.field = {   // CLK_OSC
                .div_low =    CLK_DIV_104M_D2,
                .div_middle = CLK_DIV_D2,
                .div_high =   CLK_DIV_D5,
                .div_max =    CLK_DIV_D7
            }}},
    },
    .pll_domain = {.src_cnt = {0}},
    .osc_domain = {.src_cnt = 0}
};
ATTR_RODATA_IN_TCM static const uint32_t clk_linkage_tbl[] = {
    HAL_CLOCK_CG_END,             //CLK_SYS_SEL
    HAL_CLOCK_CG_SFC,             //CLK_SFC_SEL
    HAL_CLOCK_CG_SPIMST0,         //CLK_SPIMST0_SEL
    HAL_CLOCK_CG_SDIOMST,         //CLK_SDIOMST_SEL
    HAL_CLOCK_CG_SPISLV,          //CLK_SPISLV_SEL
    HAL_CLOCK_CG_USB,             //CLK_USB_SEL
    HAL_CLOCK_CG_AUD_BUS,         //CLK_AUD_BUS_SEL
    HAL_CLOCK_CG_AUD_GPSRC,       //CLK_AUD_GPSRC_SEL
    HAL_CLOCK_CG_AUD_INTERFACE0,  //CLK_AUD_INTERFACE0_SEL
    HAL_CLOCK_CG_EMI,             //CLK_EMI_SEL
    HAL_CLOCK_CG_SPM,             //CLK_SPM_SEL
    HAL_CLOCK_CG_END,             //CLK_26M_SEL
    HAL_CLOCK_CG_AUD_DL_HIRES,    //CLK_AUD_DL_HIRES
    HAL_CLOCK_CG_AUD_UL_HIRES,    //CLK_AUD_UL_HIRES
    HAL_CLOCK_CG_END,             //RESERVED
    HAL_CLOCK_CG_SPIMST1,         //CLK_SPIMST1_SEL
    HAL_CLOCK_CG_SPIMST2,         //CLK_SPIMST2_SEL
    HAL_CLOCK_CG_AUD_INTERFACE1,  //CLK_AUD_INTERFACE1_SEL
    HAL_CLOCK_CG_END              //RESERVED
};

ATTR_TEXT_IN_TCM clk_usr_status clock_get_upll_status(){
    return UPLL_DOMAIN_USR_EXIST;
}
ATTR_TEXT_IN_TCM clk_usr_status clock_get_mpll_status(){
    return MPLL_DOMAIN_USR_EXIST;
}

ATTR_TEXT_IN_TCM clk_usr_status clock_get_apll1_status(){
    return APLL1_DOMAIN_USR_EXIST;
}
ATTR_TEXT_IN_TCM clk_usr_status clock_get_apll2_status(){
    return APLL2_DOMAIN_USR_EXIST;
}

ATTR_TEXT_IN_TCM static void mpll_enable(void){
    clk_pll_on(CLK_MPLL);
}
ATTR_TEXT_IN_TCM static void mpll_disable(void){
    clk_pll_off(CLK_MPLL);
}

ATTR_TEXT_IN_TCM static void upll_enable(void){
    clk_pll_on(CLK_UPLL);
}
ATTR_TEXT_IN_TCM static void upll_disable(void){
    clk_pll_off(CLK_UPLL);
}

ATTR_TEXT_IN_TCM static void apll1_enable(void){
    clk_pll_on(CLK_APLL1);
}
ATTR_TEXT_IN_TCM static void apll1_disable(void){
    clk_pll_off(CLK_APLL1);
}

ATTR_TEXT_IN_TCM static void apll2_enable(void){
    clk_pll_on(CLK_APLL2);
}
ATTR_TEXT_IN_TCM static void apll2_disable(void){
    clk_pll_off(CLK_APLL2);
}

ATTR_TEXT_IN_TCM uint8_t *div_memmem(uint8_t *tbl,uint8_t div){
    uint8_t *query = tbl;
    for(;query <= eof_div_tbl(tbl); query++){
        if(*query == div){
            return query;
        }
    }
    return NULL;
}

uint32_t clock_get_pll_state(clock_pll_id pll_id)
{
    switch(pll_id){
    case CLK_APLL1:
        return APLL1_DOMAIN_USR_EXIST;
    case CLK_APLL2:
        return APLL2_DOMAIN_USR_EXIST;
    case CLK_UPLL:
        return UPLL_DOMAIN_USR_EXIST;
    case CLK_MPLL:
        return MPLL_DOMAIN_USR_EXIST;
    default :
        return nonexist;    
    }
}

ATTR_TEXT_IN_TCM hal_clock_status_t clk_topgen_input_directly(clock_pll_id src,mod_type type){
    uint32_t *domain_base = src == CLK_OSC ? (uint32_t *)&clock_table.osc_domain.field.free:
                                             (uint32_t *)&clock_table.pll_domain.field.free;
    uint8_t *p_src_cnt = (uint8_t *)domain_base;
    if(src != CLK_OSC){
        switch(src){
        case CLK_UPLL:
            break;
        case CLK_MPLL:
            p_src_cnt+= 1;
            break;
        case CLK_APLL1:
            p_src_cnt+= 2;
            break;
        case CLK_APLL2:
            p_src_cnt+= 3;
            break;
        case CLK_XO:
            return HAL_CLOCK_STATUS_OK;
        }
    }
    if(type){
        (*p_src_cnt) ++;
    }else if(*p_src_cnt){
        (*p_src_cnt) --;
    }

    return HAL_CLOCK_STATUS_OK;
}



ATTR_TEXT_IN_TCM hal_clock_status_t clock_div_ctrl(pin_mux_t mux,bool on_off){

    hal_clock_status_t result = HAL_CLOCK_STATUS_OK;
    uint8_t shift = 0;
    volatile uint8_t *div_opt_reg_base = NULL;
    clock_div_ctrl_idx div_idx = NONE_DIV;
    clk_usr_status sta = exist;
    clk_src_handler src_hdr = {NULL,NULL};

    switch(mux.src){
    case CLK_UPLL:
        div_opt_reg_base = CKSYS_CLK_DIV_0__F_CLK_PLL1_D2_EN;
        src_hdr = (clk_src_handler){clock_get_upll_status,upll_enable};
        sta = src_hdr.usr_sta();
        div_idx = UPLL_DIV_IDX;
        break;
    case CLK_MPLL:
        div_opt_reg_base = CKSYS_CLK_DIV_1__F_CLK_PLL2_D2_EN;
        src_hdr = (clk_src_handler){clock_get_mpll_status,mpll_enable};
        sta = src_hdr.usr_sta();
        div_idx = MPLL_DIV_IDX;
        break;
    case CLK_OSC:
        div_opt_reg_base = CKSYS_CLK_DIV_2__F_CLK_OSC_104M_D2_EN;
        div_idx = OSC_DIV_IDX;
        break;
    case CLK_APLL1:
        src_hdr = (clk_src_handler){clock_get_apll1_status,apll1_enable};
        sta = src_hdr.usr_sta();
        break;
    case CLK_APLL2:
        src_hdr = (clk_src_handler){clock_get_apll2_status,apll2_enable};
        sta = src_hdr.usr_sta();
        break;
    }

    __IO uint8_t *DIV_PWER = NULL;
    __IO uint8_t *DIV_EN = NULL;
    uint8_t *p_res_mgr = NULL;

    if(div_idx != NONE_DIV){
        uint8_t *div_group_base = (uint8_t *)&clock_table.div_info.div_tbl[div_idx];
        uint8_t *p_div = div_memmem((uint8_t*)div_group_base,mux.div);
        if(p_div){
            shift = p_div - div_group_base;
            DIV_PWER = &CKSYS_CLK_DIV_3__F_CLK_PLL1_DIV_EN[div_idx];
        }else if(mux.div == CLK_DIV_NONE){
            result = clk_topgen_input_directly(mux.src ,on_off);
            goto src_ctrl;
        }
    }else if(mux.div == CLK_DIV_NONE){
        result = clk_topgen_input_directly(mux.src ,on_off);
        goto src_ctrl;
    }else{
        return HAL_CLOCK_STATUS_INVALID_PARAMETER;
    }
    DIV_EN = div_opt_reg_base+ shift ;
    p_res_mgr = mux.src == CLK_OSC ? (uint8_t *)((uint32_t *)&clock_table.osc_domain.field.osc) :
                                     (uint8_t *)(((uint32_t *)&clock_table.pll_domain.src_cnt) + div_idx);
    if(on_off){
        if(DIV_PWER && !*DIV_PWER){
            *DIV_PWER = 0x1;
        }

        if(!*DIV_EN){
            *DIV_EN = 0x1;
        }
        //hal_gpt_delay_us(20);
        p_res_mgr[shift] += 1;

    }else if(p_res_mgr[shift]){
        p_res_mgr[shift] -= 1;

        if(! p_res_mgr[shift]){
            *DIV_EN = 0x0;
            __IO uint32_t *src_div_status = (__IO uint32_t*)p_res_mgr;

            if(DIV_PWER && !(*src_div_status)){
                *DIV_PWER = 0x0;
            }
        }
    }
src_ctrl:

    if(src_hdr.post_hdr && !sta && src_hdr.usr_sta()){
        src_hdr.post_hdr();
    }

    return result;
}

ATTR_TEXT_IN_TCM void static clock_peri_mux_ctrl(clock_mux_sel_id mux_id,uint8_t target_sel){
    uint32_t tmp = 0;
    switch(mux_id){
    case CLK_MCLK0_SEL:
        CKSYS_AUD_CKDIV_REG.APLL_I2S0_MCK_SEL = target_sel; // CKSYS_BASE (0XA2020000)+ 0x0304, select APLL2
        CKSYS_AUD_CKDIV_REG.APLL12_CK_DIV0 = 0x2F; // CKSYS_BASE (0XA2020000)+ 0x0308, 0x2F+1 = 49.152M/1.024M
        tmp = CKSYS_AUD_CKDIV_REG.APLL12_CK_DIV0_CHG; // CKSYS_BASE (0XA2020000)+ 0x0309, toggle to update setting
        CKSYS_AUD_CKDIV_REG.APLL12_CK_DIV0_CHG = ~tmp; // CKSYS_BASE (0XA2020000)+ 0x0309, toggle to update setting
        CKSYS_AUD_CKDIV_REG.APLL12_DIV0_PDN = 0x0; // CKSYS_BASE (0XA2020000)+ 0x0300, enable divider
        break;
    case CLK_MCLK1_SEL:
        CKSYS_AUD_CKDIV_REG.APLL_I2S1_MCK_SEL = target_sel;
        CKSYS_AUD_CKDIV_REG.APLL12_CK_DIV1 = 0x2F;
        tmp = CKSYS_AUD_CKDIV_REG.APLL12_CK_DIV1_CHG;
        CKSYS_AUD_CKDIV_REG.APLL12_CK_DIV1_CHG = ~tmp;
        CKSYS_AUD_CKDIV_REG.APLL12_DIV1_PDN = 0x0;
        break;
    case CLK_MCLK2_SEL:
        CKSYS_AUD_CKDIV_REG.APLL_I2S2_MCK_SEL = target_sel;
        CKSYS_AUD_CKDIV_REG.APLL12_CK_DIV2 = 0x2F;
        tmp = CKSYS_AUD_CKDIV_REG.APLL12_CK_DIV2_CHG;
        CKSYS_AUD_CKDIV_REG.APLL12_CK_DIV2_CHG = ~tmp;
        CKSYS_AUD_CKDIV_REG.APLL12_DIV2_PDN = 0x0;
        break;
    default :
        break;
    }
}

ATTR_TEXT_IN_TCM void static clock_top_mux_ctrl(clock_mux_sel_id mux_id,uint8_t target_sel){

    volatile uint8_t *sel = NULL;
    volatile uint8_t *chg = NULL;
    volatile uint8_t *chg_ok = NULL;
    volatile uint8_t *force_on = NULL;
    switch (mux_id) {
    case CLK_SYS_SEL:
        sel = CKSYS_CLK_CFG_0__F_CLK_SYS_SEL;
        force_on = CKSYS_CLK_FORCE_ON_0__F_CLK_SYS_FORCE_ON;
        chg = CKSYS_CLK_UPDATE_0__F_CHG_SYS;
        chg_ok = CKSYS_CLK_UPDATE_STATUS_0__F_CHG_SYS_OK;
        break;
    case CLK_SFC_SEL:
        sel = CKSYS_CLK_CFG_0__F_CLK_SFC_SEL;
        force_on = CKSYS_CLK_FORCE_ON_0__F_CLK_SFC_FORCE_ON;
        chg = CKSYS_CLK_UPDATE_0__F_CHG_SFC;
        chg_ok = CKSYS_CLK_UPDATE_STATUS_0__F_CHG_SFC_OK;
        break;
    case CLK_SPIMST0_SEL:
        sel = CKSYS_CLK_CFG_0__F_CLK_SPIMST0_SEL;
        force_on = CKSYS_CLK_FORCE_ON_0__F_CLK_SPIMST0_FORCE_ON;
        chg = CKSYS_CLK_UPDATE_0__F_CHG_SPIMST0;
        chg_ok = CKSYS_CLK_UPDATE_STATUS_0__F_CHG_SPIMST0_OK;
        break;
    case CLK_SPIMST1_SEL:
        sel = CKSYS_CLK_CFG_0__F_CLK_SPIMST1_SEL;
        force_on = CKSYS_CLK_FORCE_ON_0__F_CLK_SPIMST1_FORCE_ON;
        chg = CKSYS_CLK_UPDATE_0__F_CHG_SPIMST1;
        chg_ok = CKSYS_CLK_UPDATE_STATUS_0__F_CHG_SPIMST1_OK;
        break;
    case CLK_SPIMST2_SEL:
        sel = CKSYS_CLK_CFG_1__F_CLK_SPIMST2_SEL;
        force_on = CKSYS_CLK_FORCE_ON_1__F_CLK_SPIMST2_FORCE_ON;
        chg = CKSYS_CLK_UPDATE_1__F_CHG_SPIMST2;
        chg_ok = CKSYS_CLK_UPDATE_STATUS_1__F_CHG_SPIMST2_OK;
        break;
    case CLK_SDIOMST_SEL:
        sel = CKSYS_CLK_CFG_1__F_CLK_SDIOMST_SEL;
        force_on = CKSYS_CLK_FORCE_ON_1__F_CLK_SDIOMST_FORCE_ON;
        chg = CKSYS_CLK_UPDATE_1__F_CHG_SDIOMST;
        chg_ok = CKSYS_CLK_UPDATE_STATUS_1__F_CHG_SDIOMST_OK;
        break;
    case CLK_SPISLV_SEL:
        sel = CKSYS_CLK_CFG_1__F_CLK_SPISLV_SEL;
        force_on = CKSYS_CLK_FORCE_ON_1__F_CLK_SPISLV_FORCE_ON;
        chg = CKSYS_CLK_UPDATE_1__F_CHG_SPISLV;
        chg_ok = CKSYS_CLK_UPDATE_STATUS_1__F_CHG_SPISLV_OK;
        break;
    case CLK_USB_SEL:
        sel = CKSYS_CLK_CFG_1__F_CLK_USB_SEL;
        force_on = CKSYS_CLK_FORCE_ON_1__F_CLK_USB_FORCE_ON;
        chg = CKSYS_CLK_UPDATE_1__F_CHG_USB;
        chg_ok = CKSYS_CLK_UPDATE_STATUS_1__F_CHG_USB_OK;
        break;
    case CLK_AUD_BUS_SEL:
        sel = CKSYS_CLK_CFG_2__F_CLK_AUD_BUS_SEL;
        break;
    case CLK_AUD_GPSRC_SEL:
        sel = CKSYS_CLK_CFG_2__F_CLK_AUD_GPSRC_SEL;
        force_on = CKSYS_CLK_FORCE_ON_2__F_CLK_AUD_GPSRC_FORCE_ON;
        chg = CKSYS_CLK_UPDATE_2__F_CHG_AUD_GPSRC;
        chg_ok = CKSYS_CLK_UPDATE_STATUS_2__F_CHG_AUD_GPSRC_OK;
        break;
    case CLK_AUD_INTERFACE0_SEL:
        sel = CKSYS_CLK_CFG_2__F_CLK_AUD_INTERFACE0_SEL;
        break;
    case CLK_AUD_INTERFACE1_SEL:
        sel = CKSYS_CLK_CFG_2__F_CLK_AUD_INTERFACE1_SEL;
        break;
    case CLK_EMI_SEL:
        sel = CKSYS_CLK_CFG_3__F_CLK_EMI_SEL;
        force_on = CKSYS_CLK_FORCE_ON_2__F_CLK_EMI_FORCE_ON;
        chg = CKSYS_CLK_UPDATE_2__F_CHG_EMI;
        chg_ok = CKSYS_CLK_UPDATE_STATUS_2__F_CHG_EMI_OK;
        break;
    case CLK_SPM_SEL:
        sel = CKSYS_CLK_CFG_3__F_CLK_SPM_SEL;
        break;
    case CLK_26M_SEL:
        sel = CKSYS_CLK_CFG_3__F_CLK_26M_SEL;
        break;
    case CLK_AUD_DL_HIRES:
        sel = CKSYS_CLK_CFG_4__F_CLK_AUD_DL_HIRES_SEL;
        break;
    case CLK_AUD_UL_HIRES:
        sel = CKSYS_CLK_CFG_4__F_CLK_AUD_UL_HIRES_SEL;
        break;
    default:
        return;
    }
    if (sel) {
        *sel = target_sel;
    }
    if (force_on) {
        *force_on = 1;
    }
    if (chg) {
        *chg = 1;
        while (*chg == 1);
    }
    if (chg_ok) {
        while (*chg_ok == 1);
    }
    if (force_on) {
        *force_on = 0;
    }
}
ATTR_TEXT_IN_TCM hal_clock_status_t hal_clock_set_upll_freq(uint8_t UPLL_is_624M)
{
    upll_is_624M = UPLL_is_624M;
    return HAL_CLOCK_STATUS_OK;
}

ATTR_TEXT_IN_TCM uint8_t hal_clock_get_upll_freq(void)
{
    return upll_is_624M;
}

uint8_t clock_set_pll_on(clock_pll_id pll_id){
    return 1;
}

/*************************************************************************
 * Clock mux select API definition part
 * 1. Enable individual clock divider
 * 2. Force clock on th prevent clock can't switch to target clock
 * 3. Set CSW to target clock freq. and set change bit
 * 4. After clock change to target freq. Change bit will be cleared to0 and release clock gating
 * 5. Disable forced on clock
 *************************************************************************/

ATTR_TEXT_IN_TCM hal_clock_status_t clock_mux_sel(clock_mux_sel_id mux_id, uint32_t mux_sel){
    uint8_t xo_sel = 0;
    uint32_t irq_mask = 0;
    hal_clock_status_t result = HAL_CLOCK_STATUS_OK;
    pin_mux_t **sta = NULL;
    top_mux_info *top_clk_mux = NULL;
    if(TOPGEN_CLK_DOMAIN(mux_id)){
        top_clk_mux =&clock_table.top_mux_info_list[CLK_SPIMST0_SEL];
        switch(mux_id){
        case CLK_SPIMST0_SEL:
            sta = (pin_mux_t**)&top_clk_mux -> cur_sel_0;
            break;
        case CLK_SPIMST1_SEL:
            sta = (pin_mux_t**)&top_clk_mux -> cur_sel_1;
            break;
        case CLK_SPIMST2_SEL:
            sta = (pin_mux_t**)&top_clk_mux -> cur_sel_2;
            break;
        case CLK_AUD_INTERFACE0_SEL:
            top_clk_mux = &clock_table.top_mux_info_list[CLK_AUD_INTERFACE0_SEL];
            sta = (pin_mux_t**)&top_clk_mux -> cur_sel_0;
            break;
        case CLK_AUD_INTERFACE1_SEL:
            top_clk_mux = &clock_table.top_mux_info_list[CLK_AUD_INTERFACE0_SEL];
            sta = (pin_mux_t**)&top_clk_mux -> cur_sel_1;
            break;
        default :
            if(mux_id >= NR_MUXS)
                return HAL_CLOCK_STATUS_INVALID_PARAMETER;

            if(mux_id == CLK_AUD_DL_HIRES || mux_id == CLK_AUD_UL_HIRES){
                xo_sel = 2;
            }
            top_clk_mux = &clock_table.top_mux_info_list[mux_id];
            sta = (pin_mux_t**)&top_clk_mux->cur_sel_0;
        }
    }else if(PERIPHERAL_CLK_DOMAIN(mux_id)){
        xo_sel = 2;
        top_clk_mux =&clock_table.peri_mux_info_list[CLK_MCLK0_SEL & 0xf];
        switch(mux_id){
        case CLK_MCLK0_SEL:
            sta = (pin_mux_t**)&top_clk_mux -> cur_sel_0;
            break;
        case CLK_MCLK1_SEL:
            sta = (pin_mux_t**)&top_clk_mux -> cur_sel_1;
            break;
        case CLK_MCLK2_SEL:
            sta = (pin_mux_t**)&top_clk_mux -> cur_sel_2;
            break;
        default :
            if(mux_id >= CLK_PERI_NUM)
                return HAL_CLOCK_STATUS_INVALID_PARAMETER;

            xo_sel = 0;
            top_clk_mux =&clock_table.peri_mux_info_list[mux_id & 0x1f];
            sta = (pin_mux_t**)&top_clk_mux->cur_sel_0;
        }
    }

    uint8_t target_sel =0;
    clk_usr_status usr_sta = nonexist ;
    clk_src_handler src_hdr = {NULL,NULL};
    pin_mux_t *next_sta = NULL;
    void (*mux_hw_ctrl)(clock_mux_sel_id,uint8_t) = NULL;

    if(TOPGEN_CLK_DOMAIN(mux_id) && !hal_clock_is_enabled(clk_linkage_tbl[mux_id])){
        next_sta = (top_clk_mux->mux_sels) + mux_sel;
        goto cursor_shift;
    }

    if(mux_sel != CLK_ENABLE){
        // clock disable or normal mux switch
        target_sel = mux_sel == CLK_DISABLE ? xo_sel : mux_sel;
        next_sta = (top_clk_mux->mux_sels) + target_sel;
    }

    if((*sta) != next_sta){
        if(next_sta){
            // clock disable or normal mux switch
            switch((*sta)->src){
            case CLK_UPLL:
                src_hdr = (clk_src_handler){clock_get_upll_status,upll_disable};
                usr_sta = src_hdr.usr_sta();
                break;
            case CLK_MPLL:
                src_hdr = (clk_src_handler){clock_get_mpll_status,mpll_disable};
                usr_sta = src_hdr.usr_sta();
                break;
            case CLK_APLL1:
                src_hdr = (clk_src_handler){clock_get_apll1_status,apll1_disable};
                usr_sta = src_hdr.usr_sta();
                break;
            case CLK_APLL2:
                src_hdr = (clk_src_handler){clock_get_apll2_status,apll2_disable};
                usr_sta = src_hdr.usr_sta();
                break;
            }
        }else{
            //clock enable
            next_sta = *sta;
            target_sel = next_sta - (top_clk_mux->mux_sels);
            *sta = NULL;
        }
        if((result = clock_div_ctrl(*next_sta,true)) != HAL_CLOCK_STATUS_OK){
            hal_nvic_restore_interrupt_mask(irq_mask);
            return result;
        }

        mux_hw_ctrl = PERIPHERAL_CLK_DOMAIN(mux_id) ? clock_peri_mux_ctrl :clock_top_mux_ctrl ;
        if(mux_hw_ctrl)
            mux_hw_ctrl(mux_id,target_sel);

        if(*sta)
            result = clock_div_ctrl(**sta,false);

        if(src_hdr.post_hdr && usr_sta && !(src_hdr.usr_sta())){
            src_hdr.post_hdr();
        }
cursor_shift:
        if(mux_sel != CLK_DISABLE)
            *sta = next_sta;
    }
    return result;
}

uint8_t clock_mux_cur_sel(clock_mux_sel_id mux_id){
    switch (mux_id) {
    case CLK_SYS_SEL:
        return *CKSYS_CLK_CFG_0__F_CLK_SYS_SEL;
    case CLK_SFC_SEL:
        return *CKSYS_CLK_CFG_0__F_CLK_SFC_SEL;
    case CLK_SPIMST0_SEL:
        return *CKSYS_CLK_CFG_0__F_CLK_SPIMST0_SEL;
    case CLK_SPIMST1_SEL:
        return *CKSYS_CLK_CFG_0__F_CLK_SPIMST1_SEL;
    case CLK_SPIMST2_SEL:
        return *CKSYS_CLK_CFG_1__F_CLK_SPIMST2_SEL;
    case CLK_SDIOMST_SEL:
        return *CKSYS_CLK_CFG_1__F_CLK_SDIOMST_SEL;
    case CLK_SPISLV_SEL:
        return *CKSYS_CLK_CFG_1__F_CLK_SPISLV_SEL;
    case CLK_USB_SEL:
        return *CKSYS_CLK_CFG_1__F_CLK_USB_SEL;
    case CLK_AUD_BUS_SEL:
        return *CKSYS_CLK_CFG_2__F_CLK_AUD_BUS_SEL;
    case CLK_AUD_GPSRC_SEL:
        return *CKSYS_CLK_CFG_2__F_CLK_AUD_GPSRC_SEL;
    case CLK_AUD_INTERFACE0_SEL:
        return *CKSYS_CLK_CFG_2__F_CLK_AUD_INTERFACE0_SEL;
    case CLK_AUD_INTERFACE1_SEL:
        return *CKSYS_CLK_CFG_2__F_CLK_AUD_INTERFACE1_SEL;
    case CLK_EMI_SEL:
        return *CKSYS_CLK_CFG_3__F_CLK_EMI_SEL;
    case CLK_SPM_SEL:
        return *CKSYS_CLK_CFG_3__F_CLK_SPM_SEL;
    case CLK_26M_SEL:
        return *CKSYS_CLK_CFG_3__F_CLK_26M_SEL;
    case CLK_AUD_DL_HIRES:
        return *CKSYS_CLK_CFG_4__F_CLK_AUD_DL_HIRES_SEL;
    case CLK_AUD_UL_HIRES:
        return *CKSYS_CLK_CFG_4__F_CLK_AUD_UL_HIRES_SEL;
    default :
        return 0;
    }
}

hal_clock_status_t clk_mux_init(){
    uint8_t xo_sel = 0;
    hal_clock_status_t result = HAL_CLOCK_STATUS_OK;
    clock_mux_sel_id mux_id = CLK_SYS_SEL;
    for(;mux_id < NR_MUXS; mux_id++){
        xo_sel = (mux_id == CLK_AUD_DL_HIRES || mux_id == CLK_AUD_UL_HIRES) ? 2 : 0 ;
        pin_mux_t *init_sel = (pin_mux_t*)&clock_table.top_mux_info_list[mux_id].mux_sels[xo_sel];
        clock_table.top_mux_info_list[mux_id].cur_sel_0 = init_sel;
        clock_table.top_mux_info_list[mux_id].cur_sel_1 = init_sel;
        clock_table.top_mux_info_list[mux_id].cur_sel_2 = init_sel;
    }
    for(mux_id = CLK_MCLK0_SEL;mux_id < CLK_PERI_NUM; mux_id++){
        xo_sel = (mux_id == CLK_MCLK0_SEL) ? 2 : 0 ;
        pin_mux_t *init_sel = (pin_mux_t*)&clock_table.peri_mux_info_list[mux_id & 0x1f].mux_sels[xo_sel];
        clock_table.peri_mux_info_list[mux_id & 0x1f].cur_sel_0 = init_sel;
        clock_table.peri_mux_info_list[mux_id & 0x1f].cur_sel_1 = init_sel;
        clock_table.peri_mux_info_list[mux_id & 0x1f].cur_sel_2 = init_sel;
    }
#ifndef __EXT_BOOTLOADER__
    uint32_t cur_set = clock_mux_cur_sel(CLK_SYS_SEL);
    clock_mux_sel(CLK_SYS_SEL,cur_set);
    cur_set = clock_mux_cur_sel(CLK_SFC_SEL);
    clock_mux_sel(CLK_SFC_SEL,cur_set);
    cur_set = clock_mux_cur_sel(CLK_EMI_SEL);
    clock_mux_sel(CLK_EMI_SEL,cur_set);
    cur_set = clock_mux_cur_sel(CLK_SPIMST0_SEL);
    clock_mux_sel(CLK_SPIMST0_SEL,cur_set);
    cur_set = clock_mux_cur_sel(CLK_SPIMST1_SEL);
    clock_mux_sel(CLK_SPIMST1_SEL,cur_set);
    cur_set = clock_mux_cur_sel(CLK_SPIMST2_SEL);
    clock_mux_sel(CLK_SPIMST2_SEL,cur_set);
    cur_set = clock_mux_cur_sel(CLK_SDIOMST_SEL);
    clock_mux_sel(CLK_SDIOMST_SEL,cur_set);
    cur_set = clock_mux_cur_sel(CLK_SPISLV_SEL);
    clock_mux_sel(CLK_SPISLV_SEL,cur_set);
    cur_set = clock_mux_cur_sel(CLK_AUD_BUS_SEL);
    clock_mux_sel(CLK_AUD_BUS_SEL,cur_set);
    cur_set = clock_mux_cur_sel(CLK_AUD_GPSRC_SEL);
    clock_mux_sel(CLK_AUD_GPSRC_SEL,cur_set);

#endif
    return result;
}

ATTR_TEXT_IN_TCM static uint8_t clk_pll_on(clock_pll_id pll_id){
    if( (pll_id > CLK_APLL2) || (pll_id < CLK_UPLL) ){
        log_hal_msgid_info("invalid parameter\r\n",0);
        return 0;
    }

    uint32_t irq_mask = 0;
    hal_nvic_save_and_set_interrupt_mask(&irq_mask);  /* disable interrupt */

    switch(pll_id){
        case CLK_UPLL:
            if(hal_clock_get_upll_freq() == 1){ /* UPLL = 624M */
                *UPLL_CON1__F_RG_UPLL_POSTDIV = 0x0; //MIXED_BASE (0XA2040000)+ 0x0147, post divider 1
                *CKSYS_CLK_CFG_5__F_UPLL_CK_SEL = 0x1; //CKSYS_BASE (0XA2020000)+ 0x0244, extra div2 for UPLL
                //*CKSYS_CLK_CFG_4__F_CLK_AUD_DL_HIRES_SEL = 0x1; //CKSYS_BASE (0XA2020000)+ 0x0242, UPLL_D5
                //*CKSYS_CLK_CFG_4__F_CLK_AUD_DL_HIRES_SEL = 0x1; //CKSYS_BASE (0XA2020000)+ 0x0243, UPLL_D3
            }else{                         /* UPLL = 312M */
                *UPLL_CON1__F_RG_UPLL_POSTDIV = 0x1; //MIXED_BASE (0XA2040000)+ 0x0147, post divider 2
                *CKSYS_CLK_CFG_5__F_UPLL_CK_SEL = 0x0; //CKSYS_BASE (0XA2020000)+ 0x0244, no extra div2 for UPLL
                //*CKSYS_CLK_CFG_4__F_CLK_AUD_DL_HIRES_SEL = 0x0; //CKSYS_BASE (0XA2020000)+ 0x0242, MPLL_D5
                //*CKSYS_CLK_CFG_4__F_CLK_AUD_DL_HIRES_SEL = 0x0; //CKSYS_BASE (0XA2020000)+ 0x0243, MPLL_D3
            }
            if(!*MPLL_CON0__F_DA_MPLL_EN){
                *CLKSQ_CON0__F_DA_SRCLKENA = 0x1; //MIXED_BASE (0XA2040000)+ 0x0020, bit 0 set to 1’b1 to enable CLKSQ/IV-Gen of PLLGP
            }
            // wait 6us for CLKSQ/IV-Gen stable
            hal_gpt_delay_us(6);

            *DPM_CON1__F_UPLL_SETTLE_TIME = 0x104; //MIXED_BASE (0XA2040000)+ 0x0096, UPLL settle time 20us
            // enable UPLL
            *UPLL_CON0__F_DA_UPLL_EN = 0x1; //MIXED_BASE (0XA2040000)+ 0x0140, bit 0 set to 1 to enable UPLL
            // wait 20us for UPLL stable
            hal_gpt_delay_us(20);
            // enable HW mode TOPSM control and clock CG of PLL control
            *PLL_CON2 = 0x0000; // MIXED_BASE (0XA2040000)+ 0x0048, to enable PLL TOPSM control and clock CG of controller
            *PLL_CON3 = 0x0000; // MIXED_BASE (0XA2040000)+ 0x004C, to enable DCXO 26M TOPSM control and clock CG of controller

            *PLLTD_CON0__F_BP_PLL_DLY= 0x0000; //MIXED_BASE (0XA2040000)+ 0x0700, bit 0 set to 0 to enable delay control
            *UPLL_CON0__F_RG_UPLL_RDY = 0x1; //MIXED_BASE (0XA2040000)+ 0x0143, bit 0 set to 1 to release UPLL clock

            //wait for 1us for TOPSM and delay (HW) control signal stable
            hal_gpt_delay_us(1);
            break;
        case CLK_MPLL:
            if(!*UPLL_CON0__F_DA_UPLL_EN){
                *CLKSQ_CON0__F_DA_SRCLKENA = 0x1; //MIXED_BASE (0XA2040000)+ 0x0020, bit 0 set to 1’b1 to enable CLKSQ/IV-Gen of PLLGP
            }
            *MDDS_CON0__F_RG_MDDS_EN = 0x0; //MIXED_BASE (0XA2040000)+ 0x0640, bit 0 set to 0 to disable MDDS

            //disable PLL_PGDET
            *PLL_CON4__F_RG_PLL_PGDET_EN = 0x0; //MIXED_BASE (0XA2040000)+ 0x0050, bit 2 set to 0 to disable PGDET of MDDS
            // wait 6us for CLKSQ/IV-Gen stable
            hal_gpt_delay_us(6);
            *DPM_CPN2__F_MPLL_SETTLE_TIME = 0x104; //MIXED_BASE (0XA2040000)+ 0x0098, MPLL settling time 20us
            *PLLTD_CON2__F_S1_STB_TIME = 0x4e; //MIXED_BASE (0XA2040000)+ 0x0708, 78*13M = 6us
            //select MPLL frequency
            //*MPLL_CON1__F_RG_MPLL_POSTDIV = 0x0; // MIXED_BASE (0XA2040000)+ 0x0107, set post divider = /1

            //enable and reset MPLL
            *MPLL_CON0__F_DA_MPLL_EN = 0x1; //MIXED_BASE (0XA2040000)+ 0x0100, bit 0 set to 1 to enable MPLL and generate reset of MPLL
            //wait 20us for PLL settle
            hal_gpt_delay_us(20);

            // enable HW mode TOPSM control and clock CG of PLL control
            *PLL_CON2 = 0x0000; // MIXED_BASE (0XA2040000)+ 0x0048, to enable PLL TOPSM control and clock CG of controller
            *PLL_CON3 = 0x0000; // MIXED_BASE (0XA2040000)+ 0x004C, to enable DCXO 26M TOPSM control and clock CG of controller

            // enable delay control
            *PLLTD_CON0__F_BP_PLL_DLY= 0x0000; //MIXED_BASE (0XA2040000)+ 0x0700, bit 0 set to 0 to enable delay control
            *MPLL_CON0__F_RG_MPLL_RDY = 0x1; //MIXED_BASE (0XA2040000)+ 0x0103, bit 0 set to 1 to release MPLL clock

            //wait 20us for for TOPSM and delay (HW) control signal stable
            hal_gpt_delay_us(20);

            //48M sel
            *CKSYS_CLK_CFG_4__F_USB_48M_SEL = 0x1; // CKSYS_BASE (0XA2020000)+ 0x0241, 0:UPLL_48M, 1:MPLL_48M

            hal_clock_enable(HAL_CLOCK_CG_GPLL_26M);
            break;

        case CLK_APLL1:
            *APLL1_CTL0__F_RG_APLL1_V2I_EN = 0x1; //XPLL_CTRL_BASE (0xA2050000) + 0x0003
            *APLL1_CTL0__F_RG_APLL1_DDS_PWR_ON = 0x1; //XPLL_CTRL_BASE (0xA2050000) + 0x0000

            hal_gpt_delay_us(5);

            *APLL1_CTL0__F_RG_APLL1_DDS_ISO_EN = 0x0; //XPLL_CTRL_BASE (0xA2050000) + 0x0001
            *APLL1_CTL1__F_RG_APLL1_EN = 0x1; //XPLL_CTRL_BASE (0xA2050000) + 0x0004
            *APLL1_CTL1__F_RG_APLL1_LCDDS_PWDB = 0x1; //XPLL_CTRL_BASE (0xA2050000) + 0x002C

            hal_gpt_delay_us(30);

            hal_clock_enable(HAL_CLOCK_CG_APLL_26M);
            break;
        case CLK_APLL2:
            *APLL2_CTL0__F_RG_APLL2_V2I_EN = 0x1; //XPLL_CTRL_BASE (0xA2050000) + 0x0103
            *APLL2_CTL0__F_RG_APLL2_DDS_PWR_ON = 0x1; //XPLL_CTRL_BASE (0xA2050000) + 0x0100

            hal_gpt_delay_us(5);

            *APLL2_CTL0__F_RG_APLL2_DDS_ISO_EN = 0x0; //XPLL_CTRL_BASE (0xA2050000) + 0x0101
            *APLL2_CTL1__F_RG_APLL2_EN = 0x1; //XPLL_CTRL_BASE (0xA2050000) + 0x0104
            *APLL2_CTL1__F_RG_APLL2_LCDDS_PWDB = 0x1; //XPLL_CTRL_BASE (0xA2050000) + 0x012C

            hal_gpt_delay_us(30);

            hal_clock_enable(HAL_CLOCK_CG_APLL_26M);

            break;
    }

    hal_nvic_restore_interrupt_mask(irq_mask);  /* restore interrupt */

    return 1;
}
uint8_t clock_set_pll_off(clock_pll_id pll_id){
    return 1;
}

ATTR_TEXT_IN_TCM static uint8_t clk_pll_off(clock_pll_id pll_id)
{
    if( (pll_id >= NR_PLLS) || (pll_id < CLK_UPLL) ){
        log_hal_msgid_info("%s: invalid parameter\r\n", 1, __FUNCTION__);
        return 0;
    }

    uint32_t irq_mask = 0;

    hal_nvic_save_and_set_interrupt_mask(&irq_mask);  /* disable interrupt */

    switch(pll_id){
        case CLK_UPLL:
        //disable MDDS

            if(!*MPLL_CON0__F_DA_MPLL_EN){
                hal_clock_enable(HAL_CLOCK_CG_GPLL_26M);
            }
            //*MDDS_CON0__F_RG_MDDS_EN = 0x0; //MIXED_BASE (0XA2040000)+ 0x0640, bit 0 set to 0 to disable MDDS

            //disable PLL_PGDET
            //*PLL_CON4__F_RG_PLL_PGDET_EN = 0x0; //MIXED_BASE (0XA2040000)+ 0x0050, bit 2 set to 0 to disable PGDET of MDDS
            *UPLL_CON0__F_RG_UPLL_RDY = 0x0; //MIXED_BASE (0XA2040000)+ 0x0143, bit 0 set to 0 to gating UPLL clock
            *UPLL_CON0__F_DA_UPLL_EN = 0x0; //MIXED_BASE (0XA2040000)+ 0x0140, bit 0 set to 0 to disable UPLL
            hal_gpt_delay_us(6);
            if(!*MPLL_CON0__F_DA_MPLL_EN){
                *CLKSQ_CON0__F_DA_SRCLKENA = 0x0; //MIXED_BASE (0XA2040000)+ 0x0020, bit 0 set to 1’b0 to disable CLKSQ/IV-Gen of PLLGP
            }

            break;
        case CLK_MPLL:

            if(!*UPLL_CON0__F_DA_UPLL_EN){
                hal_clock_enable(HAL_CLOCK_CG_GPLL_26M);
            }
            *MPLL_CON0__F_RG_MPLL_RDY = 0x0; //MIXED_BASE (0XA2040000)+ 0x0103, bit 0 set to 0 to gating MPLL clock
            *MPLL_CON0__F_DA_MPLL_EN = 0x0; //MIXED_BASE (0XA2040000)+ 0x0100, bit 0 set to 0 to disable MPLL
            hal_gpt_delay_us(6);
            if(!*UPLL_CON0__F_DA_UPLL_EN){
                *CLKSQ_CON0__F_DA_SRCLKENA = 0x0; //MIXED_BASE (0XA2040000)+ 0x0020, bit 0 set to 1’b0 to disable CLKSQ/IV-Gen of PLLGP
            }
            //hal_clock_disable(HAL_CLOCK_CG_GPLL_26M);
            // wait 6us for CLKSQ/IV-Gen stable
            hal_gpt_delay_us(6);

            break;
        case CLK_APLL1:

            *APLL1_CTL1__F_RG_APLL1_EN = 0x0; //XPLL_CTRL_BASE (0xA2050000) + 0x0004
            *APLL1_CTL1__F_RG_APLL1_LCDDS_PWDB = 0x0; //XPLL_CTRL_BASE (0xA2050000) + 0x002C
            *APLL1_CTL0__F_RG_APLL1_DDS_ISO_EN = 0x1; //XPLL_CTRL_BASE (0xA2050000) + 0x0001
            *APLL1_CTL0__F_RG_APLL1_DDS_PWR_ON = 0x0;//XPLL_CTRL_BASE (0xA2050000) + 0x0000
            *APLL1_CTL0__F_RG_APLL1_V2I_EN = 0x0; //XPLL_CTRL_BASE (0xA2050000) + 0x0003
            *APLL1_CTL12__F_RG_APLL1_LCDDS_PCW_NCPO_CHG = 0x0; //XPLL_CTRL_BASE (0xA2050000) + 0x0030
            hal_gpt_delay_us(5);
            if(!*APLL2_CTL1__F_RG_APLL2_EN){
                hal_clock_disable(HAL_CLOCK_CG_APLL_26M);
            }

            break;
        case CLK_APLL2:

            *APLL2_CTL1__F_RG_APLL2_EN = 0x0; //XPLL_CTRL_BASE (0xA2050000) + 0x0104
            *APLL2_CTL1__F_RG_APLL2_LCDDS_PWDB = 0x0; //XPLL_CTRL_BASE (0xA2050000) + 0x012C
            *APLL2_CTL0__F_RG_APLL2_DDS_ISO_EN = 0x1; //XPLL_CTRL_BASE (0xA2050000) + 0x0101
            *APLL2_CTL0__F_RG_APLL2_DDS_PWR_ON = 0x0;//XPLL_CTRL_BASE (0xA2050000) + 0x0100
            *APLL2_CTL0__F_RG_APLL2_V2I_EN = 0x0; //XPLL_CTRL_BASE (0xA2050000) + 0x0103
            *APLL2_CTL12__F_RG_APLL2_LCDDS_PCW_NCPO_CHG = 0x0; //XPLL_CTRL_BASE (0xA2050000) + 0x0130
            hal_gpt_delay_us(5);
            if(!*APLL1_CTL1__F_RG_APLL1_EN){
                hal_clock_disable(HAL_CLOCK_CG_APLL_26M);
            }
            break;
    }

    hal_nvic_restore_interrupt_mask(irq_mask);  /* restore interrupt */
    return 1;
}

#endif /*__EXT_BOOTLOADER__*/

ATTR_TEXT_IN_TCM uint8_t clock_convert_to_clock_id(uint32_t bit){
    uint8_t hex_idx = 0;
    if(bit <= 0x8)               hex_idx = 0;
    else if(bit <=0x80)          hex_idx = 1;
    else if(bit <= 0x800)        hex_idx = 2;
    else if(bit <= 0x8000)       hex_idx = 3;
    else if(bit <= 0x80000)      hex_idx = 4;
    else if(bit <= 0x800000)     hex_idx = 5;
    else if(bit <= 0x8000000)    hex_idx = 6;
    else                         hex_idx = 7;
    uint8_t clock_id = base_sorting(hex_idx);
    switch(bit >> clock_id){
    case 0x1:
        break;
    case 0x2:
        clock_id += 1;
        break;
    case 0x4:
        clock_id += 2;
        break;
    case 0x8:
        clock_id += 3;
        break;
    default :
        return HAL_CLOCK_CG_END;
    }
    return clock_id + HAL_CLOCK_CG_I2C_DMA;
}


ATTR_TEXT_IN_TCM void hal_clock_set_running_flags(uint32_t clock_cg_opt_set,bool on_off){
    uint32_t op_bit = 0;
    while((op_bit = lsb(clock_cg_opt_set))){
        uint8_t clock_id = clock_convert_to_clock_id(op_bit);
        if((on_off && HAL_CLOCK_STATUS_OK == hal_clock_enable(clock_id))||
           (!on_off && HAL_CLOCK_STATUS_OK == hal_clock_disable(clock_id)))
            clock_cg_opt_set &= (clock_cg_opt_set-1);
    }
}

ATTR_TEXT_IN_TCM int8_t clock_mux_get_state(clock_mux_sel_id mux_id)
{
    return HAL_CLOCK_STATUS_OK;
}

struct _clock_ref_cnt {
    uint32_t cnt;
};

//ATTR_ZIDATA_IN_TCM static struct _clock_ref_cnt clocks[NR_CLOCKS];
//ATTR_ZIDATA_IN_TCM static struct _clock_ref_cnt plls[NR_PLLS];
ATTR_ZIDATA_IN_TCM static struct _clock_ref_cnt dsp_dcm[NR_DSP];

/*************************************************************************
 * CG Clock API definition
 *************************************************************************/

ATTR_TEXT_IN_TCM hal_clock_status_t hal_clock_enable(hal_clock_cg_id clock_id)
{
    hal_clock_status_t ret = HAL_CLOCK_STATUS_OK;
    if(hal_clock_is_enabled(clock_id))
        return ret;
    volatile uint32_t *clr_addr = NULL;
    uint32_t bit_idx = clock_id & (0x1f);

    if (clock_id <= HAL_CLOCK_CG_USB_DMA) {
        clr_addr = PDN_CLRD0_F_PDR_CLRD0;
    } else if ((clock_id >= HAL_CLOCK_CG_PWM0) && (clock_id <= HAL_CLOCK_CG_I2S_DMA)) {
        clr_addr = PDN_CLRD0_F_PDR_CLRD1;
    } else if ((clock_id >= HAL_CLOCK_CG_I2C_DMA) && (clock_id <= HAL_CLOCK_CG_DEBUGSYS)) {
        clr_addr = XO_PDN_CLRD0;
    }else{
        return HAL_CLOCK_STATUS_INVALID_PARAMETER;
    }
    *(clr_addr) |= (0x1 << bit_idx);
#ifndef __EXT_BOOTLOADER__
    if(clock_id != HAL_CLOCK_CG_END){
    const uint32_t *query = clk_linkage_tbl;//(uint32_t*)memmem(clk_linkage_tbl,sizeof(clk_linkage_tbl),&clock_id,4);
        for(;query < &clk_linkage_tbl[NR_MUXS]; query++){
            if(*query == clock_id){
                clock_mux_sel_id linkage_mux = query - clk_linkage_tbl;
                ret = clock_mux_sel(linkage_mux,CLK_ENABLE);
            }
        }
    }
#endif

    return ret;
}

ATTR_TEXT_IN_TCM hal_clock_status_t hal_clock_disable(hal_clock_cg_id clock_id)
{
    hal_clock_status_t ret = HAL_CLOCK_STATUS_OK;
    volatile uint32_t *set_addr = NULL;
    uint32_t bit_idx = clock_id & 0x1f;

    if(!hal_clock_is_enabled(clock_id))
        return ret;
    if (clock_id <= HAL_CLOCK_CG_USB_DMA) {
        set_addr = PDN_SETD0_F_PDR_SETD0;
    } else if ((clock_id >= HAL_CLOCK_CG_PWM0) && (clock_id <= HAL_CLOCK_CG_I2S_DMA)) {
        set_addr = PDN_SETD0_F_PDR_SETD1;
    } else if ((clock_id >= HAL_CLOCK_CG_I2C_DMA) && (clock_id <= HAL_CLOCK_CG_DEBUGSYS)) {
        set_addr = XO_PDN_SETD0;
    }else{
        return HAL_CLOCK_STATUS_INVALID_PARAMETER;
    }
#ifndef __EXT_BOOTLOADER__
if(clock_id != HAL_CLOCK_CG_END){
    const uint32_t *query = clk_linkage_tbl;//(uint32_t*)memmem(clk_linkage_tbl,sizeof(clk_linkage_tbl),&clock_id,4);
    for(;query < &clk_linkage_tbl[NR_MUXS]; query++){
        if(*query == clock_id){
            clock_mux_sel_id linkage_mux = query - clk_linkage_tbl;
            ret = clock_mux_sel(linkage_mux,CLK_DISABLE);
        }
    }
}

#endif
    *(set_addr) |= (0x1 << bit_idx);
    return ret;
}



ATTR_TEXT_IN_TCM bool hal_clock_is_enabled(hal_clock_cg_id clock_id)
{
    uint32_t bit_idx = clock_id & 0x1f; // clock_id %32
    volatile uint32_t * sta_addr = NULL;

    if (clock_id <= HAL_CLOCK_CG_USB_DMA) { /* >=HAL_CLOCK_CG_DMA is true for comparison to unsigned zero */
        sta_addr = PDN_COND0_F_PDR_COND0;
    } else if ((clock_id >= HAL_CLOCK_CG_PWM0) && (clock_id <= HAL_CLOCK_CG_I2S_DMA)) {
        sta_addr = PDN_COND0_F_PDR_COND1;
    } else if ((clock_id >= HAL_CLOCK_CG_I2C_DMA) && (clock_id <= HAL_CLOCK_CG_DEBUGSYS)) {
        sta_addr = XO_PDN_COND0;
    } else {
        return true;
    }

    if (((*sta_addr) & (0x1 << bit_idx)) != 0x0) {
        /* TODO cannot use log_hal_info print log before syslog init */
#ifdef CLK_DEBUG
        log_hal_msgid_info("%d: bit = %d: clock is disabled\n", 2,clock_id, bit_idx);
#endif
        return false;
    } else {
        /* TODO cannot use log_hal_info print log before syslog init */
#ifdef CLK_DEBUG
         log_hal_msgid_info("%d: bit = %d: clock is enabled\n", 2,clock_id, bit_idx);
#endif
        return true;
    }
}

/*************************************************************************
 * Suspend/Resume API definition
 *************************************************************************/
ATTR_TEXT_IN_TCM hal_clock_status_t clock_suspend(void)
{
    return HAL_CLOCK_STATUS_OK;
}

ATTR_TEXT_IN_TCM hal_clock_status_t clock_resume(void)
{
    return HAL_CLOCK_STATUS_OK;
}

/*
 * Funtion: Query frequency meter
 * tcksel: TESTED clock selection. 1: f_fxo_ck, 19: hf_fsys_ck.
 * fcksel: FIXED clock selection. 0: f_fxo_ck, 1: f_frtc_ck, 6: XOSC32K_CK.
 * return frequency unit: KHz
 */

#define FREQ_METER_WINDOW   4096




ATTR_TEXT_IN_TCM uint32_t hal_clock_get_freq_meter_155x(uint8_t tcksel, uint8_t fcksel)
{
    uint32_t target_freq = 0;
    uint64_t target_freq_64 = 0;

    *ABIST_FQMTR_CON0 = 0xC000; // CKSYS_BASE (0XA2020000)+ 0x0400, to reset meter
    hal_gpt_delay_us(1);
    while ((*ABIST_FQMTR_CON1 & 0x8000) != 0); // CKSYS_BASE (0XA2020000)+ 0x0404, wait busy

    //CKSYS_BASE (0XA2020000)+ 0x0224, [10:8] fixed_clock, [4:0] tested_clock
    *CKSYS_TST_SEL_1_F_TCKSEL = tcksel;
    *CKSYS_TST_SEL_1_F_FCKSEL = fcksel;

    *ABIST_FQMTR_CON0 = FREQ_METER_WINDOW-1; // CKSYS_BASE (0XA2020000)+ 0x0400, set window 4096 T
    *ABIST_FQMTR_CON0 = (0x8000|(FREQ_METER_WINDOW-1)); // CKSYS_BASE (0XA2020000)+ 0x0400, to enable meter
    //*ABIST_FQMTR_CON0 = 0x8063; // CKSYS_BASE (0XA2020000)+ 0x0400, to enable meter, window 100T

    hal_gpt_delay_us(5); //wait meter start

    while ((*ABIST_FQMTR_CON1 & 0x8000) != 0); // CKSYS_BASE (0XA2020000)+ 0x0404, wait busy

    /* fqmtr_ck = fixed_ck*fqmtr_data/winset, */
    target_freq = *PLL_ABIST_FQMTR_DATA__F_FQMTR_DATA; // CKSYS_BASE (0XA2020000)+ 0x040C, read meter data

    #if CLOCK_DEBUG
        log_hal_msgid_info("0x%08x\r\n",1, *PLL_ABIST_FQMTR_DATA__F_FQMTR_DATA);
    #endif
    target_freq_64 = (uint64_t)target_freq;
    target_freq_64 = 26 * 1000 * target_freq_64 / FREQ_METER_WINDOW;
    target_freq = (uint32_t)target_freq_64;

    return target_freq;
}
uint32_t hal_clock_get_freq_meter(src_clock clock_src_id, uint32_t winset){
	return hal_clock_get_freq_meter_155x((uint8_t)clock_src_id,0);
}
/*
 * Funtion: Query frequency meter measurement cycles
 * tcksel: TESTED clock selection. 1: f_fxo_ck, 19: hf_fsys_ck.
 * fcksel: FIXED clock selection. 0: f_fxo_ck, 1: f_frtc_ck, 6: XOSC32K_CK.
 * winset: measurement window setting (number of fixed clock cycles)
 * return: meter data
 */
ATTR_TEXT_IN_TCM uint32_t hal_clock_get_freq_meter_cycle(uint8_t tcksel, uint8_t fcksel, uint16_t winset)
{
    uint32_t cycles = 0;

    *ABIST_FQMTR_CON0 = 0xC000; // CKSYS_BASE (0XA2020000)+ 0x0400, to reset meter
    hal_gpt_delay_us(1);
    while ((*ABIST_FQMTR_CON1 & 0x8000) != 0); // CKSYS_BASE (0XA2020000)+ 0x0404, wait busy

    //CKSYS_BASE (0XA2020000)+ 0x0224, [10:8] fixed_clock, [4:0] tested_clock
    *CKSYS_TST_SEL_1_F_TCKSEL = tcksel;
    *CKSYS_TST_SEL_1_F_FCKSEL = fcksel;

    *ABIST_FQMTR_CON0 = winset-1; // CKSYS_BASE (0XA2020000)+ 0x0400, set window 4096 T
    *ABIST_FQMTR_CON0 = (0x8000|(winset-1)); // CKSYS_BASE (0XA2020000)+ 0x0400, to enable meter
    //*ABIST_FQMTR_CON0 = 0x8063; // CKSYS_BASE (0XA2020000)+ 0x0400, to enable meter, window 100T

    hal_gpt_delay_us(5); //wait meter start

    while ((*ABIST_FQMTR_CON1 & 0x8000) != 0); // CKSYS_BASE (0XA2020000)+ 0x0404, wait busy

    /* fqmtr_ck = fixed_ck*fqmtr_data/winset, */
    cycles = *PLL_ABIST_FQMTR_DATA__F_FQMTR_DATA; // CKSYS_BASE (0XA2020000)+ 0x040C, read meter data

    return cycles;
}

const uint32_t OSC_TARGET = 312000;   /* 312MHz */

ATTR_TEXT_IN_TCM static uint8_t osc_search_cali(uint8_t ft) /* worst case = 6 times*/
{
    uint8_t low = 0, high = 0x3F,  mid = 0, cali = 0;
    uint32_t result = 0;
    uint32_t diff = OSC_TARGET;

    *HFOSC_CON0__F_RG_HFOSC_FT = ft;

    while ((low <= high) && (high <= 0X3F))
    {
        mid = (high-low)/2 + low; /* replace (high+low)/2, to avoid overflow */

        *HFOSC_CON0__F_RG_HFOSC_CALI = mid;

        hal_gpt_delay_us(5);

        result = hal_clock_get_freq_meter_155x(0x1e, 0x0);

        //bl_print(0,"low = %d, high = %d, mid = %d, result = %d\r\n", low, high, mid, result);

        if (result == OSC_TARGET){
            return mid;
        }
        else if (result > OSC_TARGET){
            high = mid - 1;
            if (result-OSC_TARGET < diff){
                cali = mid;
                diff = result-OSC_TARGET;
            }
        }
        else if (result < OSC_TARGET){
            low = mid + 1;
            if (OSC_TARGET-result < diff){
                cali = mid;
                diff = OSC_TARGET-result;
            }
        }
    }
    return cali;
}

ATTR_TEXT_IN_TCM static uint8_t osc_search_ft(uint8_t cali) /* worst case = 4 times*/
{
    uint8_t low = 0, high = 0xF,  mid = 0, ft = 0;
    uint32_t result = 0;
    uint32_t diff = OSC_TARGET;

    *HFOSC_CON0__F_RG_HFOSC_CALI = cali;

    while ((low <= high) && (high <= 0XF))
    {
        mid = (high-low)/2 + low; /* replace (high+low)/2, to avoid overflow */

        *HFOSC_CON0__F_RG_HFOSC_FT = mid;

        hal_gpt_delay_us(5);

        result = hal_clock_get_freq_meter_155x(0x1e, 0x0);

        //bl_print(0,"low = %d, high = %d, mid = %d, result = %d\r\n", low, high, mid, result);

        if (result == OSC_TARGET){
            return mid;
        }
        else if (result > OSC_TARGET){
            high = mid - 1;
            if (result-OSC_TARGET < diff){
                ft = mid;
                diff = result-OSC_TARGET;
            }
        }
        else if (result < OSC_TARGET){
            low = mid + 1;
            if (OSC_TARGET-result < diff){
                ft = mid;
                diff = OSC_TARGET-result;
            }
        }
    }
    return ft;
}


ATTR_TEXT_IN_SYSRAM void hal_clock_cm4_clk_156m(){
    hal_clock_enable(HAL_CLOCK_CG_OSC_1P1);
    //hal_clock_enable(HAL_CLOCK_CG_OSC_0P9);
    clock_mux_sel(CLK_SPIMST0_SEL, 5);
    clock_mux_sel(CLK_SPIMST1_SEL, 5);
    clock_mux_sel(CLK_SPIMST2_SEL, 5);
    clock_mux_sel(CLK_SYS_SEL,7);
    clock_mux_sel(CLK_EMI_SEL, 8);
    clock_mux_sel(CLK_SPISLV_SEL,7);
    clock_mux_sel(CLK_SPM_SEL, 1);
}

ATTR_TEXT_IN_SYSRAM void hal_clock_cm4_clk_78m(){
    //hal_clock_enable(HAL_CLOCK_CG_OSC_0P9);
    clock_mux_sel(CLK_SYS_SEL, 8);
    clock_mux_sel(CLK_SPIMST0_SEL, 5);
    clock_mux_sel(CLK_SPIMST1_SEL, 5);
    clock_mux_sel(CLK_SPIMST2_SEL, 5);
    clock_mux_sel(CLK_SPISLV_SEL,7);
    clock_mux_sel(CLK_EMI_SEL, 8);
    clock_mux_sel(CLK_SPM_SEL, 1);
}

ATTR_TEXT_IN_SYSRAM void hal_clock_cm4_clk_39m(){
    //hal_clock_enable(HAL_CLOCK_CG_OSC_0P9);
    clock_mux_sel(CLK_SYS_SEL, 10);
    clock_mux_sel(CLK_SPIMST0_SEL, 1);
    clock_mux_sel(CLK_SPIMST1_SEL, 1);
    clock_mux_sel(CLK_SPIMST2_SEL, 1);
    clock_mux_sel(CLK_SPISLV_SEL,1);
    clock_mux_sel(CLK_EMI_SEL, 11);
    clock_mux_sel(CLK_SPM_SEL, 1);
}

ATTR_TEXT_IN_SYSRAM void hal_clock_cm4_clk_26m(){

    clock_mux_sel(CLK_SYS_SEL, 11);
    clock_mux_sel(CLK_SDIOMST_SEL, 1);
    clock_mux_sel(CLK_AUD_BUS_SEL, 0);
    clock_mux_sel(CLK_AUD_GPSRC_SEL, 0);
    clock_mux_sel(CLK_EMI_SEL, 11);
    clock_mux_sel(CLK_SPM_SEL, 1);
    clock_mux_sel(CLK_SPIMST0_SEL,1);
    clock_mux_sel(CLK_SPIMST1_SEL,1);
    clock_mux_sel(CLK_SPIMST2_SEL,1);
    clock_mux_sel(CLK_SPISLV_SEL,1);
}


ATTR_TEXT_IN_TCM void hal_clock_set_pll_dcm_init(void)
{
#ifndef FPGA_ENV
    uint8_t osc_cali = 0;

    // Sequence to enable HFOSC
    *HFOSC_CON0__F_DA_HFOSC_EN = 0x1; //MIXED_BASE (0XA2040000)+ 0x0920, set to 1’b1 to enable HFOSC 312M
    *HFOSC_CON0__F_RG_HFOSC_104M_EN = 0x1; //MIXED_BASE (0XA2040000)+ 0x0921, set to 1’b1 to enable HFOSC 104M
    *HFOSC_CON2__F_HFOSC_EN_SEL = 0x0; //MIXED_BASE (0XA2040000)+ 0x092a, set to 1’b0 to enable SPM hardware control
    *PLLTD_CON0__F_BP_PLL_DLY = 0x0000; //MIXED_BASE (0XA2040000)+ 0x0700, bit 0 set to 0 to enable delay control

    //Do HFOSC calibration
    // Freq: 312M*(1+(RG_HFOSC_CALI-6'd40)*0.02+RG_HFOSC_FT*0.0012)
    // frequency meter (tcksel = 0x1e, fcksel = 0x0)
    //binary search *HFOSC_CON0__F_RG_HFOSC_CALI //MIXED_BASE (0XA2040000)+ 0x0923
    // *HFOSC_CON0__F_RG_HFOSC_FT //MIXED_BASE (0XA2040000)+ 0x0922

    //wait 5us for HFOSC settle
    hal_gpt_delay_us(5);

    osc_cali = osc_search_cali((0xF-1)/2);
    *HFOSC_CON0__F_RG_HFOSC_FT = osc_search_ft(osc_cali);

    //bl_print(0,"cali = %d, ft = %d\r\n", *HFOSC_CON0__F_RG_HFOSC_CALI, *HFOSC_CON0__F_RG_HFOSC_FT);

    // Sequence to switch to HFOSC clocks as below:
    // enable clock divider
    *CKSYS_CLK_DIV_2__F_CLK_OSC_104M_D2_EN = 0x1;   // CKSYS_BASE (0XA2020000)+ 0x0288, to enable digital frequency divider
    *CKSYS_CLK_DIV_2__F_CLK_OSC_D2_EN = 0x1;   // CKSYS_BASE (0XA2020000)+ 0x0289, to enable digital frequency divider
    *CKSYS_CLK_DIV_2__F_CLK_OSC_D5_EN = 0x1;   // CKSYS_BASE (0XA2020000)+ 0x028A, to enable digital frequency divider
    *CKSYS_CLK_DIV_3__F_CLK_OSC_DIV_EN = 0x1;   // CKSYS_BASE (0XA2020000)+ 0x028e, to enable digital frequency divider

    // set clock mux
    *CKSYS_CLK_FORCE_ON_0 = 0x01010101;   // CKSYS_BASE (0XA2020000)+ 0x0270, to force clock on
    *CKSYS_CLK_FORCE_ON_1 = 0x01010101;   // CKSYS_BASE (0XA2020000)+ 0x0274, to force clock on
    *CKSYS_CLK_FORCE_ON_2 = 0x01010101;   // CKSYS_BASE (0XA2020000)+ 0x0278, to force clock on

    *CKSYS_CLK_CFG_0__F_CLK_SYS_SEL = 0x8; // CKSYS_BASE (0XA2020000)+ 0x0230, SYS clock @ HFOSC 156MHz
    *CKSYS_CLK_CFG_0__F_CLK_SFC_SEL = 0x5; // CKSYS_BASE (0XA2020000)+ 0x0231, SFC clock @ HFOSC 62.4MHz
    *CKSYS_CLK_CFG_0__F_CLK_SPIMST0_SEL = 0x5; // CKSYS_BASE (0XA2020000)+ 0x0232, SPIMST0 clock @ HFOSC 104MHz
    *CKSYS_CLK_CFG_0__F_CLK_SPIMST1_SEL = 0x5; // CKSYS_BASE (0XA2020000)+ 0x0233, SPIMST1 clock @ HFOSC 104MHz
    *CKSYS_CLK_CFG_1__F_CLK_SPIMST2_SEL = 0x5; // CKSYS_BASE (0XA2020000)+ 0x0234, SPIMST2 clock @ HFOSC 104MHz
    *CKSYS_CLK_CFG_1__F_CLK_SDIOMST_SEL = 0x6; // CKSYS_BASE (0XA2020000)+ 0x0235, SDIOMST clock @ HFOSC 52MHz
    *CKSYS_CLK_CFG_1__F_CLK_SPISLV_SEL = 0x7; // CKSYS_BASE (0XA2020000)+ 0x0236, SPISLV clock @ HFOSC 104MHz
    *CKSYS_CLK_CFG_3__F_CLK_EMI_SEL = 0x8; // CKSYS_BASE (0XA2020000)+ 0x023c, EMI clock @ HFOSC 156MHz
    *CKSYS_CLK_CFG_3__F_CLK_SPM_SEL = 0x1; // CKSYS_BASE (0XA2020000)+ 0x023d, SPM clock @ HFOSC 26MHz

    *CKSYS_CLK_UPDATE_0__F_CHG_SYS = 0x1; // CKSYS_BASE (0XA2020000)+ 0x0250, SYS clock switch
    while (*CKSYS_CLK_UPDATE_0__F_CHG_SYS ==1); // CKSYS_BASE (0XA2020000)+ 0x0250, wait SYS clock switch
    while (*CKSYS_CLK_UPDATE_STATUS_0__F_CHG_SYS_OK ==1); // CKSYS_BASE (0XA2020000)+ 0x0260, wait SYS clock switch

    *CKSYS_CLK_UPDATE_0__F_CHG_SFC = 0x1; // CKSYS_BASE (0XA2020000)+ 0x0251, SFC clock switch
    while (*CKSYS_CLK_UPDATE_0__F_CHG_SFC ==1); // CKSYS_BASE (0XA2020000)+ 0x0251, wait SFC clock switch
    while (*CKSYS_CLK_UPDATE_STATUS_0__F_CHG_SFC_OK ==1); // CKSYS_BASE (0XA2020000)+ 0x0261, wait SFC clock switch

    *CKSYS_CLK_UPDATE_0__F_CHG_SPIMST0 = 0x1; // CKSYS_BASE (0XA2020000)+ 0x0252, SPIMST0 clock switch
    while (*CKSYS_CLK_UPDATE_0__F_CHG_SPIMST0 ==1); // CKSYS_BASE (0XA2020000)+ 0x0252, wait SPIMST0 clock switch
    while (*CKSYS_CLK_UPDATE_STATUS_0__F_CHG_SPIMST0_OK ==1); // CKSYS_BASE (0XA2020000)+ 0x0262, wait SPIMST0 clock switch

    *CKSYS_CLK_UPDATE_0__F_CHG_SPIMST1 = 0x1; // CKSYS_BASE (0XA2020000)+ 0x0253, SPIMST1 clock switch
    while (*CKSYS_CLK_UPDATE_0__F_CHG_SPIMST1 ==1); // CKSYS_BASE (0XA2020000)+ 0x0253, wait SPIMST1 clock switch
    while (*CKSYS_CLK_UPDATE_STATUS_0__F_CHG_SPIMST1_OK ==1); // CKSYS_BASE (0XA2020000)+ 0x0263, wait SPIMST1 clock switch

    *CKSYS_CLK_UPDATE_1__F_CHG_SPIMST2 = 0x1; // CKSYS_BASE (0XA2020000)+ 0x0254, SPIMST2 clock switch
    while (*CKSYS_CLK_UPDATE_1__F_CHG_SPIMST2 ==1); // CKSYS_BASE (0XA2020000)+ 0x0254, wait SPIMST2 clock switch
    while (*CKSYS_CLK_UPDATE_STATUS_1__F_CHG_SPIMST2_OK ==1); // CKSYS_BASE (0XA2020000)+ 0x0264, wait SPIMST2 clock switch

    *CKSYS_CLK_UPDATE_1__F_CHG_SDIOMST = 0x1; // CKSYS_BASE (0XA2020000)+ 0x0255, SDIOMST clock switch
    while (*CKSYS_CLK_UPDATE_1__F_CHG_SDIOMST ==1); // CKSYS_BASE (0XA2020000)+ 0x0255, wait SDIOMST clock switch
    while (*CKSYS_CLK_UPDATE_STATUS_1__F_CHG_SDIOMST_OK ==1); // CKSYS_BASE (0XA2020000)+ 0x0265, wait SDIOMST clock switch

    *CKSYS_CLK_UPDATE_1__F_CHG_SPISLV = 0x1; // CKSYS_BASE (0XA2020000)+ 0x0256, SPISLV clock switch
    while (*CKSYS_CLK_UPDATE_1__F_CHG_SPISLV ==1); // CKSYS_BASE (0XA2020000)+ 0x0256, wait SPISLV clock switch
    while (*CKSYS_CLK_UPDATE_STATUS_1__F_CHG_SPISLV_OK ==1); // CKSYS_BASE (0XA2020000)+ 0x0266, wait SPISLV clock switch

    *CKSYS_CLK_UPDATE_2__F_CHG_EMI = 0x1; // CKSYS_BASE (0XA2020000)+ 0x0259, EMI clock switch
    while (*CKSYS_CLK_UPDATE_2__F_CHG_EMI ==1); // CKSYS_BASE (0XA2020000)+ 0x0259, wait EMI clock switch
    while (*CKSYS_CLK_UPDATE_STATUS_2__F_CHG_EMI_OK ==1); // CKSYS_BASE (0XA2020000)+ 0x0269, wait EMI clock switch

    *CKSYS_CLK_FORCE_ON_0 = 0x0; // CKSYS_BASE (0XA2020000)+ 0x0270, to disable force clock on
    *CKSYS_CLK_FORCE_ON_1 = 0x0; // CKSYS_BASE (0XA2020000)+ 0x0274, to disable force clock on
    *CKSYS_CLK_FORCE_ON_2 = 0x0; // CKSYS_BASE (0XA2020000)+ 0x0278, to disable force clock on

    *CKSYS_CLK_CFG_3__F_CLK_26M_SEL = 0x1; // CKSYS_BASE (0XA2020000)+ 0x023e, 26M clock @ HFOSC 26MHz

    //wait 1us for 26M mux settle
    hal_gpt_delay_us(1);

    *XO_PDN_SETD0 = 0x6800000; // CKSYS_XO_CLK_BASE (0XA2030000)+ 0x0b10, disable GPLL/APLL reference 26M, cksys HFOSC 312M

    // set clock DCM
    *SFC_DCM_CON_0__F_RG_SFC_DCM_DBC_NUM = 0xFF; // CKSYS_BASE (0XA2020000)+ 0x0141, to set SFC DCM debounce cycle 255
    *SFC_DCM_CON_0__F_RG_SFC_DCM_DBC_EN = 0x1; // CKSYS_BASE (0XA2020000)+ 0x0142, to enable SFC DCM debounce
    *SFC_DCM_CON_1__F_RG_SFC_CLKOFF_EN = 0x1; // CKSYS_BASE (0XA2020000)+ 0x0144, to enable SFC DCM
    *SFC_DCM_CON_0__F_RG_SFC_DCM_APB_SEL = 0x6; // CKSYS_BASE (0XA2020000)+ 0x0143, to update SFC DCM setting
    *SFC_DCM_CON_1__F_RG_SFC_DCM_APB_TOG = 0x1; // CKSYS_BASE (0XA2020000)+ 0x0147, to sync SFC DCM setting

    *BUS_DCM_CON_0__F_RG_BUS_SFSEL = 0x0; // CKSYS_BUS_CLK_BASE (0XA2270000)+ 0x0100, BUS DCM, idle clock 78M/64

    //add for EMI postsim issue
    *BUS_DCM_CON_0__F_RG_BUS_DCM_DBC_NUM = 0x2; // CKSYS_BUS_CLK_BASE (0XA2270000)+ 0x0101, to set DCM debounce cycle
    *BUS_DCM_CON_0__F_RG_BUS_DCM_DBC_EN = 0x1; // CKSYS_BUS_CLK_BASE (0XA2270000)+ 0x0102, to enable DCM debounce

    // add after DVT pass
    *DSP0_SLOW_CON4__F_RG_DSP0_SFSEL = 0x0; // CKSYS_BUS_CLK_BASE (0XA2270000)+ 0x0410, DSP0 DCM, idle clock 156M/128
    *DSP1_SLOW_CON4__F_RG_DSP1_SFSEL = 0x0; // CKSYS_BUS_CLK_BASE (0XA2270000)+ 0x0510, DSP1 DCM, idle clock 156M/128

    *BUS_DCM_CON_1__F_RG_BUS_CLKSLOW_EN = 0x1; // CKSYS_BUS_CLK_BASE (0XA2270000)+ 0x0105, to enable BUS DCM clock slow
    *BUS_DCM_CON_1__F_RG_BUS_CLKOFF_EN = 0x1; // CKSYS_BUS_CLK_BASE (0XA2270000)+ 0x0104, to enable BUS DCM clock off
    *BUS_DCM_CON_0__F_RG_BUS_DCM_EN = 0x9; // CKSYS_BUS_CLK_BASE (0XA2270000)+ 0x0103, to enable EMI, AUDIO DCM clock off

    // add after DVT pass
    *DSP0_SLOW_CON0__F_RG_DSP0_SLOW_EN = 0x1; // CKSYS_BUS_CLK_BASE (0XA2270000)+ 0x0400, to enable DSP0 DCM clock slow
    *DSP1_SLOW_CON0__F_RG_DSP1_SLOW_EN = 0x1; // CKSYS_BUS_CLK_BASE (0XA2270000)+ 0x0500, to enable DSP1 DCM clock slow

    *CLK_FREQ_SWCH__F_RG_PLLCK_SEL = 0x1; // CKSYS_BUS_CLK_BASE (0XA21D0000)+ 0x0170, to enable DCM control
    //wait 1us for DCM settle
    hal_gpt_delay_us(1);
#endif
}   /* void clock_set_pll_dcm_init(void) */

void dsp0_dcm_div_sel(uint8_t sel)
{
    *DSP0_SLOW_CON4__F_RG_DSP0_SFSEL = sel;
}

void dsp1_dcm_div_sel(uint8_t sel)
{
    *DSP1_SLOW_CON4__F_RG_DSP1_SFSEL = sel;
}

void dsp0_dcm_enable_from_dsp0(uint8_t en)
{
    *DSP0_SLOW_CON0 = en;
}

void dsp0_dcm_enable_from_dsp1(uint8_t en)
{
    *DSP0_SLOW_CON1 = en;
}

void dsp0_dcm_enable_from_cm4(uint8_t en)
{
    *DSP0_SLOW_CON2 = en;
}

void dsp0_dcm_enable_from_n9(uint8_t en)
{
    *DSP0_SLOW_CON3 = en;
}

void dsp1_dcm_enable_from_dsp1(uint8_t en)
{
    *DSP1_SLOW_CON0 = en;
}

void dsp1_dcm_enable_from_dsp0(uint8_t en)
{
    *DSP1_SLOW_CON1 = en;
}

void dsp1_dcm_enable_from_cm4(uint8_t en)
{
    *DSP1_SLOW_CON2 = en;
}

void dsp1_dcm_enable_from_n9(uint8_t en)
{
    *DSP1_SLOW_CON3 = en;
}

void clock_set_chop_div(uint32_t div)
{
    uint8_t tmp = 0;

    *CKSYS_CLK_DIV_4__F_CHOP_DIV_SEL = div; // CKSYS_BASE (0XA2020000)+ 0x0292, if (0x18F*2+1)+1 = 26M/32.5K

    tmp = *CKSYS_CLK_DIV_4__F_CHOP_DIV_CHG; // CKSYS_BASE (0XA2020000)+ 0x0291, toggle to update setting
    *CKSYS_CLK_DIV_4__F_CHOP_DIV_CHG = ~tmp;
}

void clock_set_chop_en(uint8_t en)
{
    *CKSYS_CLK_DIV_4__F_CHOP_DIV_EN = en; // CKSYS_BASE (0XA2020000)+ 0x0290, enable divider
}

ATTR_TEXT_IN_TCM hal_clock_status_t clock_dsp_dcm_enable(clock_dsp_num dsp_num)
{
    uint32_t irq_mask = 0;

    if (dsp_num > CLK_DSP1) {
#ifdef CLK_DEBUG
    // TODO cannot print log before log_hal_info init done */
    log_hal_msgid_info("dsp_num = %d\r\n", 1, dsp_num);
#endif /* ifdef CLK_DEBUG */
        return HAL_CLOCK_STATUS_INVALID_PARAMETER;
    }

    hal_nvic_save_and_set_interrupt_mask(&irq_mask);  /* disable interrupt */

    if (dsp_dcm[dsp_num].cnt > 0) {
        dsp_dcm[dsp_num].cnt--;
    }

    if (dsp_dcm[dsp_num].cnt == 0) {
        if (dsp_num == CLK_DSP0)
            dsp0_dcm_enable_from_cm4(1);
        else
            dsp1_dcm_enable_from_cm4(1);
    }

    hal_nvic_restore_interrupt_mask(irq_mask);  /* restore interrupt */
    return HAL_CLOCK_STATUS_OK;
}

ATTR_TEXT_IN_TCM hal_clock_status_t clock_dsp_dcm_disable(clock_dsp_num dsp_num)
{
    uint32_t irq_mask = 0;

    if (dsp_num > CLK_DSP1) {
#ifdef CLK_DEBUG
    /* TODO cannot print log before log_hal_info init done */
    log_hal_msgid_info("dsp_num = %d\r\n", 1, dsp_num);
#endif /* ifdef CLK_DEBUG */
        return HAL_CLOCK_STATUS_INVALID_PARAMETER;
    }

    hal_nvic_save_and_set_interrupt_mask(&irq_mask);  /* disable interrupt */

    if (dsp_dcm[dsp_num].cnt == 0) {
        if (dsp_num == CLK_DSP0)
            dsp0_dcm_enable_from_cm4(0);
        else
            dsp1_dcm_enable_from_cm4(0);
    }

    if (dsp_dcm[dsp_num].cnt < 32767) {
        dsp_dcm[dsp_num].cnt++;
    }

    hal_nvic_restore_interrupt_mask(irq_mask);  /* restore interrupt */
    return HAL_CLOCK_STATUS_OK;
}



ATTR_TEXT_IN_TCM hal_clock_status_t hal_clock_init(void)
{
    hal_clock_disable(HAL_CLOCK_CG_USB_DMA);
    hal_clock_disable(HAL_CLOCK_CG_USB_BUS);
    hal_clock_disable(HAL_CLOCK_CG_USB);
    hal_clock_disable(HAL_CLOCK_CG_CRYPTO);
    hal_clock_disable(HAL_CLOCK_CG_UART0);
    hal_clock_disable(HAL_CLOCK_CG_TRNG);
    hal_clock_disable(HAL_CLOCK_CG_EFUSE);
#ifndef __EXT_BOOTLOADER__
    return clk_mux_init();
#else
    return HAL_CLOCK_STATUS_OK;
#endif
}
#endif /* HAL_CLOCK_MODULE_ENABLED */


void clock_dump_log(void)
{
#if 0
    /* fix IAR build warning: undefined behavior, the order of volatile accesses is undefined in this statement. */
    uint32_t reg_tmp0, reg_tmp1, reg_tmp2, reg_tmp3, reg_tmp4, reg_tmp5;

    reg_tmp0 = *PDN_COND0_F_PDR_COND0;
    reg_tmp1 = *XO_PDN_COND0;

    /* clock_dump_cg */
    log_hal_info("%s: PDN_COND0=0x%x, XO_PDN_COND0=0x%x\r\n",
                 __FUNCTION__, reg_tmp0, reg_tmp1);

    reg_tmp0 = *CKSYS_CLK_CFG_0_F_CLK_SYS_SEL;
    reg_tmp1 = *CKSYS_CLK_CFG_0_F_CLK_SFC_SEL;
    reg_tmp2 = *CKSYS_CLK_CFG_0_F_CLK_CONN_SEL;
    reg_tmp3 = *CKSYS_CLK_CFG_0_F_CLK_SPIMST_SEL;
    reg_tmp4 = *CKSYS_CLK_CFG_1_F_CLK_XTALCTL_SEL;
    reg_tmp5 = *CKSYS_CLK_CFG_1_F_CLK_SDIOMST_SEL;

    /* clock_dump_mux */
    log_hal_info("%s: (mux)CLK_SYS_SEL=0x%x, (mux)CLK_SFC_SEL=0x%x, (mux)CLK_CONN_SEL=0x%x, (mux)CLK_SPIMST_SEL=0x%x, (mux)CLK_XTALCTL_SEL=0x%x, (mux)CKSYS_CLK_CFG_1_F_CLK_SDIOMST_SEL=0x%x\r\n",
                 __FUNCTION__, reg_tmp0, reg_tmp1, reg_tmp2, reg_tmp3, reg_tmp4, reg_tmp5);

    /* clock_dump_divider */
    reg_tmp0 = *CKSYS_CLK_DIV_0_F_CLK_PLL1_D2_EN;
    reg_tmp1 = *CKSYS_CLK_DIV_0_F_CLK_PLL1_D3_EN;
    reg_tmp2 = *CKSYS_CLK_DIV_0_F_CLK_PLL1_D5_EN;
    reg_tmp3 = *CKSYS_CLK_DIV_0_F_CLK_PLL1_D7_EN;
    reg_tmp4 = *CKSYS_CLK_DIV_2_F_CLK_PLL1_D15_EN;
    reg_tmp5 = *CKSYS_CLK_DIV_2_F_CLK_PLL1_DIV_EN;
    log_hal_info("%s: PLL1 DIV EN(%d), D2(%d), D3(%d), D5(%d), D7(%d), D15(%d)\r\n", __FUNCTION__, reg_tmp5, reg_tmp0, reg_tmp1, reg_tmp2, reg_tmp3, reg_tmp4);

    reg_tmp0 = *CKSYS_CLK_DIV_1_F_CLK_PLL2_D2_EN;
    reg_tmp1 = *CKSYS_CLK_DIV_1_F_CLK_PLL2_D3_EN;
    reg_tmp2 = *CKSYS_CLK_DIV_1_F_CLK_PLL2_D5_EN;
    reg_tmp3 = *CKSYS_CLK_DIV_1_F_CLK_PLL2_D7_EN;
    reg_tmp4 = *CKSYS_CLK_DIV_2_F_CLK_PLL2_D15_EN;
    reg_tmp5 = *CKSYS_CLK_DIV_2_F_CLK_PLL2_DIV_EN;
    log_hal_info("%s: PLL2 DIV EN(%d), D2(%d), D3(%d), D5(%d), D7(%d), D15(%d)\r\n", __FUNCTION__, reg_tmp5, reg_tmp0, reg_tmp1, reg_tmp2, reg_tmp3, reg_tmp4);

    reg_tmp0 = *CKSYS_CLK_DIV_3;
    reg_tmp1 = *CKSYS_CLK_DIV_4;
    reg_tmp2 = *CKSYS_CLK_DIV_5;
    reg_tmp3 = *CKSYS_XTAL_FREQ;
    log_hal_info("%s: CLK_DIV_3=0x%x, CLK_DIV_4=0x%x, CLK_DIV_5=0x%x, CLK_XTAL_FREQ=0x%x\r\n",
                 __FUNCTION__, reg_tmp0, reg_tmp1, reg_tmp2, reg_tmp3);
#else
    return;
#endif
}

void clock_dump_info(void)
{
    clock_dump_log();
}

