/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#include "hal_dvfs_internal.h"

#ifdef HAL_DVFS_MODULE_ENABLED
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "hal.h"
#include "hal_flash_sf.h"
#include "hal_gpt.h"
#include "hal_clock.h"
#include "hal_clock_internal.h"
#include "hal_clock.h"
#include "hal_pmu_wrap_interface.h"
#include "hal_pmu.h"
#include "hal_emi_internal.h"
#include "hal_nvic_internal.h"

#ifdef  HAL_DVFS_DEBUG_ENABLE
#define dvfs_debug(_message,...) log_hal_info("[DVFS] "_message, ##__VA_ARGS__)
#else
#define dvfs_debug(_message,...)
#endif
static dvfs_opp_t domain;
extern uint32_t SysTick_Set(uint32_t ticks);

//------------[DVFS Variable Declaration]
#define EFUSE_ENABLE_156M (*((volatile uint32_t*)(0xA20A0000 + 0x204)) & 0x1)
#define E4_HW_VERSION     ((*(volatile uint32_t *)0xA2000000) == 0xCA02 )
static uint32_t dvfs_pmu_mapping_voltage[DVFS_MAX_SPEED] = {
    PMIC_VCORE_0P9_V,PMIC_VCORE_1P1_V,PMIC_VCORE_1P1_V,PMIC_VCORE_1P3_V
};
static uint32_t dvfs_vcore_voltage[DVFS_MAX_SPEED] = {
    900000,1100000,1100000,1300000
};
static uint32_t dvfs_vcore_frequency[DVFS_MAX_SPEED] = {
    26000,39000,78000, 156000
};

static uint32_t e4_chip_dvfs_pmu_mapping_voltage[DVFS_MAX_SPEED] = {
    PMIC_VCORE_0P9_V,PMIC_VCORE_1P0_V,PMIC_VCORE_1P1_V,PMIC_VCORE_1P3_V
};
static uint32_t e4_chip_dvfs_vcore_voltage[DVFS_MAX_SPEED] = {
    900000,1000000,1100000,1300000
};
static uint32_t e4_chip_dvfs_vcore_frequency[DVFS_MAX_SPEED] = {
    26000,39000,78000,156000
};




int dvfs_check_pmic_voltage(unsigned int index){
    if(domain.pmu_mapping_volt[index] ==pmu_get_vcore_voltage_mt6388()){
        return true;
    }else{
        return false;
    }
}

//------------[DVFS basic setting api]
void dvfs_vcore_switch_voltage(unsigned int c_voltage, unsigned int n_voltage)
{
    const uint32_t *dvfs_pmu_voltage_tbl = domain.pmu_mapping_volt;
    if((pmu_get_lock_index()>0) && (pmu_get_lock_index()>dvfs_pmu_voltage_tbl[n_voltage])){
       // pmu_vcore_voltage_sel_6388(PMU_NORMAL,pmu_get_lock_index());
        pmu_vcore_lock_control(PMU_NORMAL,dvfs_pmu_voltage_tbl[n_voltage],PMU_LOCK);
        pmu_vcore_lock_control(PMU_NORMAL,dvfs_pmu_voltage_tbl[c_voltage],PMU_UNLOCK);
    }else {
        pmu_vcore_lock_control(PMU_NORMAL,dvfs_pmu_voltage_tbl[n_voltage],PMU_LOCK);
        if(pmu_get_lock_index()>0){
            pmu_vcore_lock_control(PMU_NORMAL,dvfs_pmu_voltage_tbl[c_voltage],PMU_UNLOCK);
        }
    }
}


ATTR_TEXT_IN_SYSRAM void dvfs_vcore_switch_frequency(unsigned int cur_opp, unsigned int next_opp){
    if(cur_opp == next_opp)
        return ;

    uint32_t irq_flag = 0;
    register unsigned int c_opp = cur_opp;
    register unsigned int n_opp = next_opp;
#ifndef MTK_NO_PSRAM_ENABLE
    dvfs_enter_privileged_level();
#endif
    /* hal_cache_flush_all_cache_lines();
     * Prevent switch clock have cache miss cause to cache replace and access cache
     */
    hal_nvic_save_and_set_interrupt_mask(&irq_flag);
    hal_cache_flush_all_cache_lines();
    SFI_MaskAhbChannel(1);
    emi_mask_master();

    switch (n_opp) {
            case DVFS_HIGH_SPEED_156M:
                hal_clock_cm4_clk_156m();
                SF_DAL_DEV_SWITCH_TO_HIGH_FQ();
                hal_emi_dynamic_clock_switch(EMI_CLK_LOW_TO_HIGH,EMI_CLK_104M);
                break;
            case DVFS_FULL_SPEED_78M:
                hal_clock_cm4_clk_78m();
                if(c_opp != DVFS_HALF_SPEED_39M){
                    SF_DAL_DEV_SWITCH_TO_62M();
                    hal_emi_dynamic_clock_switch(EMI_CLK_LOW_TO_HIGH,EMI_CLK_78M);
                }
                break;
            case DVFS_HALF_SPEED_39M:
                hal_clock_cm4_clk_39m();
                if(c_opp != DVFS_FULL_SPEED_78M)
                    SF_DAL_DEV_SWITCH_TO_62M();

                if(c_opp != DVFS_LOW_SPEED_26M)
                    hal_emi_dynamic_clock_switch(EMI_CLK_HIGH_TO_LOW,EMI_CLK_26M);
                break;
            case DVFS_LOW_SPEED_26M:

                hal_clock_cm4_clk_26m();
                if(c_opp != DVFS_HALF_SPEED_39M)
                    SF_DAL_DEV_SWITCH_TO_LOW_FQ();
                hal_emi_dynamic_clock_switch(EMI_CLK_HIGH_TO_LOW,EMI_CLK_26M);
                break;
            default:
                assert(0);
    }
    if(cur_opp == DVFS_HIGH_SPEED_156M){
        hal_clock_disable(HAL_CLOCK_CG_OSC_1P1);
    }

    EMI_DynamicClockSwitch((n_opp < c_opp) ? EMI_CLK_LOW_TO_HIGH : EMI_CLK_HIGH_TO_LOW);
    emi_unmask_master();
    SFI_MaskAhbChannel(0);
    hal_nvic_restore_interrupt_mask(irq_flag);

#ifndef MTK_NO_PSRAM_ENABLE
    dvfs_exit_privileged_level();

#endif
    log_hal_msgid_info("Cur: %d \r\n",1,hal_clock_get_freq_meter(17, 0) / 2);
    
    __ISB();
    __DSB();
}

dvfs_opp_t *dvfs_domain_init(){
    memset(&domain, 0, sizeof(domain));
    domain.name = __stringify("DVFS_VCORE");
    domain.opp_num = DVFS_MAX_SPEED; //max number about can choise voltage
    domain.module_num =2;// VCORE/DSP
    domain.cur_opp_index = -1;
    domain.switch_voltage = dvfs_vcore_switch_voltage;//function about switch voltage
    domain.switch_frequency = dvfs_vcore_switch_frequency;// function about switch frequency
    domain.frequency = E4_HW_VERSION ? e4_chip_dvfs_vcore_frequency :dvfs_vcore_frequency;//set all frequency
    domain.voltage = E4_HW_VERSION ? e4_chip_dvfs_vcore_voltage : dvfs_vcore_voltage ;  // set all voltage
    domain.pmu_mapping_volt = E4_HW_VERSION ? e4_chip_dvfs_pmu_mapping_voltage : dvfs_pmu_mapping_voltage;
#ifdef MTK_SYSTEM_CLOCK_26M /* Set system clock to 26M Hz */
    domain.basic_opp_index = DVFS_LOW_SPEED_26M;
#else
#ifdef MTK_SYSTEM_CLOCK_156M /* Set system clock to 156M Hz */
    domain.basic_opp_index = DVFS_HIGH_SPEED_156M;
#else /* Set system clock to 78M Hz */
    domain.basic_opp_index = DVFS_FULL_SPEED_78M;
#endif
#endif
    domain.initialized = true;
    return &domain;
}


#endif /* HAL_DVFS_MODULE_ENABLED */

