/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_pmu_auxadc.h"

#ifdef HAL_PMU_AUXADC_MODULE_ENABLED
#include "hal_pmu_wrap_interface.h"
#include "hal_pmu_mt6388_platform.h"
#include "hal.h"
void pmu_auxadc_init(void) {
    pmu_set_register_value_2byte_mt6388(PMU_AUXADC_CK_AON_ADDR, PMU_AUXADC_CK_AON_MASK, PMU_AUXADC_CK_AON_SHIFT, 0); //setting of dynamic CLK Managerment
    pmu_set_register_value_2byte_mt6388(PMU_RG_STRUP_AUXADC_RSTB_SW_ADDR, PMU_RG_STRUP_AUXADC_RSTB_SW_MASK, PMU_RG_STRUP_AUXADC_RSTB_SW_SHIFT, 1);//auxadc not being reset on sleep mode
    pmu_set_register_value_2byte_mt6388(PMU_RG_STRUP_AUXADC_RSTB_SEL_ADDR, PMU_RG_STRUP_AUXADC_RSTB_SEL_MASK, PMU_RG_STRUP_AUXADC_RSTB_SEL_SHIFT, 1);

    pmu_set_register_value_2byte_mt6388(PMU_AUXADC_LBAT_EN_MAX_ADDR, PMU_AUXADC_LBAT_EN_MAX_MASK, PMU_AUXADC_LBAT_EN_MAX_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_AUXADC_LBAT_EN_MIN_ADDR, PMU_AUXADC_LBAT_EN_MIN_MASK, PMU_AUXADC_LBAT_EN_MIN_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_AUXADC_LBAT2_EN_MAX_ADDR, PMU_AUXADC_LBAT2_EN_MAX_MASK, PMU_AUXADC_LBAT2_EN_MAX_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_AUXADC_LBAT2_EN_MIN_ADDR, PMU_AUXADC_LBAT2_EN_MIN_MASK, PMU_AUXADC_LBAT2_EN_MIN_SHIFT, 1);

    pmu_set_register_value_2byte_mt6388(PMU_AUXADC_THR_EN_MAX_ADDR, PMU_AUXADC_THR_EN_MAX_MASK, PMU_AUXADC_THR_EN_MAX_SHIFT, 1);
    pmu_set_register_value_2byte_mt6388(PMU_AUXADC_THR_EN_MIN_ADDR, PMU_AUXADC_THR_EN_MIN_MASK, PMU_AUXADC_THR_EN_MIN_SHIFT, 1);
}
void pmu_auxadc_delay(unsigned int ms)
{
    hal_gpt_delay_ms(ms);
}
uint32_t pmu_auxadc_get_channel_value(pmu_adc_channel_t Channel)
{
    uint32_t adc_result=-1;
    if (Channel >= PMU_AUX_MAX) {
        log_hal_msgid_error("[PMIC]Error auxadc chnnel\r\n", 0);
        return (0xFFFFFFFF);    //Channel error
    }
    switch(Channel) {
        case PMU_AUX_PN_ZCV:
            adc_result = pmu_get_register_value_2byte_mt6388(PMU_AUXADC_ADC_OUT_PWRON_PCHR_ADDR,PMU_AUXADC_ADC_OUT_PWRON_PCHR_MASK,PMU_AUXADC_ADC_OUT_PWRON_PCHR_SHIFT);
            adc_result =(adc_result*1800*3)/32768;
        break;

        case PMU_AUX_WK_ZCV:
            adc_result = pmu_get_register_value_2byte_mt6388(PMU_AUXADC_ADC_OUT_WAKEUP_PCHR_ADDR,PMU_AUXADC_ADC_OUT_WAKEUP_PCHR_MASK,PMU_AUXADC_ADC_OUT_WAKEUP_PCHR_SHIFT);
            adc_result =(adc_result*1800*3)/32768;
        break;

        case PMU_AUX_BATSNS:
            pmu_set_register_value_2byte_mt6388(PMU_AUXADC_RQST_CH0_ADDR, PMU_AUXADC_RQST_CH0_MASK, PMU_AUXADC_RQST_CH0_SHIFT, 1);
            do {
            }while(pmu_get_register_value_2byte_mt6388(PMU_AUXADC_ADC_RDY_CH0_BY_AP_ADDR,PMU_AUXADC_ADC_RDY_CH0_BY_AP_MASK,PMU_AUXADC_ADC_RDY_CH0_BY_AP_SHIFT)!=1);
            adc_result = pmu_get_register_value_2byte_mt6388(PMU_AUXADC_ADC_OUT_CH0_BY_AP_ADDR,PMU_AUXADC_ADC_OUT_CH0_BY_AP_MASK,PMU_AUXADC_ADC_OUT_CH0_BY_AP_SHIFT);
            adc_result =((float)adc_result*1800*3)/32768;
        break;

        case PMU_AUX_VBUS:
            pmu_set_register_value_2byte_mt6388(PMU_AUXADC_RQST_CH2_ADDR, PMU_AUXADC_RQST_CH2_MASK, PMU_AUXADC_RQST_CH2_SHIFT, 1);
            do {
            }while(pmu_get_register_value_2byte_mt6388(PMU_AUXADC_ADC_RDY_CH2_ADDR,PMU_AUXADC_ADC_RDY_CH2_MASK,PMU_AUXADC_ADC_RDY_CH2_SHIFT)!=1);
            adc_result = pmu_get_register_value_2byte_mt6388(PMU_AUXADC_ADC_OUT_CH2_ADDR,PMU_AUXADC_ADC_OUT_CH2_MASK,PMU_AUXADC_ADC_OUT_CH2_SHIFT);
            adc_result = (adc_result*1800*8)/4096;
        break;

        case PMU_AUX_CHR_THM:
            pmu_set_register_value_2byte_mt6388(PMU_AUXADC_RQST_CH3_ADDR, PMU_AUXADC_RQST_CH3_MASK, PMU_AUXADC_RQST_CH3_SHIFT, 1);
            do {
            }while(pmu_get_register_value_2byte_mt6388(PMU_AUXADC_ADC_RDY_CH3_ADDR,PMU_AUXADC_ADC_RDY_CH3_MASK,PMU_AUXADC_ADC_RDY_CH3_SHIFT)!=1);
            adc_result = pmu_get_register_value_2byte_mt6388(PMU_AUXADC_ADC_OUT_CH3_ADDR,PMU_AUXADC_ADC_OUT_CH3_MASK,PMU_AUXADC_ADC_OUT_CH3_SHIFT);
            adc_result = (adc_result*1800)/4096;
        break;

        case PMU_AUX_HW_JEITA:
            adc_result = pmu_get_register_value_2byte_mt6388(PMU_AUXADC_ADC_OUT_JEITA_ADDR,PMU_AUXADC_ADC_OUT_JEITA_MASK,PMU_AUXADC_ADC_OUT_JEITA_SHIFT);
            adc_result = (adc_result*1800)/4096;
        break;

        case PMU_AUX_PMIC_AP:
            pmu_set_register_value_2byte_mt6388(PMU_AUXADC_RQST_CH4_ADDR, PMU_AUXADC_RQST_CH4_MASK, PMU_AUXADC_RQST_CH4_SHIFT, 1);
            do {
            }while(pmu_get_register_value_2byte_mt6388(PMU_AUXADC_ADC_RDY_CH4_ADDR,PMU_AUXADC_ADC_RDY_CH4_MASK,PMU_AUXADC_ADC_RDY_CH4_SHIFT)!=1);
            adc_result = pmu_get_register_value_2byte_mt6388(PMU_AUXADC_ADC_OUT_CH4_ADDR,PMU_AUXADC_ADC_OUT_CH4_MASK,PMU_AUXADC_ADC_OUT_CH4_SHIFT);
            adc_result = (adc_result*1800)/4096;
        break;
    }
    return adc_result;
}

#endif /* HAL_PMU_AUXADC_MODULE_ENABLED */
