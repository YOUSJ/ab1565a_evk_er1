/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#include "hal_captouch.h" 

#ifdef HAL_CAPTOUCH_MODULE_ENABLED
#include "hal_captouch_internal.h" 
#include "hal_nvic.h"
#include "hal_gpt.h"
#include "hal_clock.h"
#include "hal_rtc.h"
#include "hal_rtc_internal.h"
#include "hal_log.h"

#define CAL_DBG_MIN_L1   (-45)
#define CAL_DBG_MAX_L1   (45)
#define CAL_DBG_MIN_L2   (-60)
#define CAL_DBG_MAX_L2   (60)
#define CAL_DBG_MIN_L3   (-62)
#define CAL_DBG_MAX_L3   (62)
#define VADC_DBG_MIN_L1  (-50)
#define VADC_DBG_MAX_L1  (50)
#define VADC_DBG_MIN_L2  (-70)
#define VADC_DBG_MAX_L2  (70)
#define VADC_DBG_MIN_L3  (-200)
#define VADC_DBG_MAX_L3  (200)

CAPTOUCH_REGISTER_T *captouch = (CAPTOUCH_REGISTER_T *)(CAPTOUCH_BASE);
captouch_buffer_t captouch_buffer;
captouch_context_t captouch_context;

const uint8_t captouch_mapping_keydata[] = {KEYPAD_MAPPING};
const uint32_t captouch_key_time[16] = {
    1,2,3,5,10,20,40,80,160,320,640,1280,2560,5120,10240,20480};

const CAPTOUCH_ANALOG_REG_T CAPTOUCH_ANALOG = {
   {TOUCH_CON0, 
    TOUCH_CON1},
   {TOUCH_THR_H0,
    TOUCH_THR_H1,
    TOUCH_THR_H2,
    TOUCH_THR_H3,
    TOUCH_THR_H4,
    TOUCH_THR_H5,
    TOUCH_THR_H6,
    TOUCH_THR_H7},
   {TOUCH_THR_L0,
    TOUCH_THR_L1,
    TOUCH_THR_L2,
    TOUCH_THR_L3,
    TOUCH_THR_L4,
    TOUCH_THR_L5,
    TOUCH_THR_L6,
    TOUCH_THR_L7},
   {TOUCH_CAL_OUT_MAN0,
    TOUCH_CAL_OUT_MAN1,
    TOUCH_CAL_OUT_MAN2,
    TOUCH_CAL_OUT_MAN3,
    TOUCH_CAL_OUT_MAN4,
    TOUCH_CAL_OUT_MAN5,
    TOUCH_CAL_OUT_MAN6,
    TOUCH_CAL_OUT_MAN7},
    TOUCH_CH_INT_EN,
    TOUCH_CH_WAKEUP_EN,
    TOUCH_CAL_DBG,
    TOUCH_VADC_DBG,
    TOUCH_AVG_DBG,
    TOUCH_LP_CON,
   {TOUCH_ANA_CFG0,
    TOUCH_ANA_CFG1,
    TOUCH_ANA_CFG2},
   {TOUCH_CORSE_CAP_CFG1,
    TOUCH_CORSE_CAP_CFG2},
   {TOUCH_CAL_CFG1,
    TOUCH_CAL_CFG2},
    TOUCH_ATST_CONTROL};


#define XO_PDN_COND0 0xA2030B00     
#define XO_PDN_SETD0 0xA2030B10     
#define XO_PDN_CLRD0 0xA2030B20

void captouch_push_one_event_to_buffer(hal_captouch_key_state_t state, uint32_t data, uint32_t time_stamp)
{
    if (captouch_context.has_initilized != true) {
        return;
    }
        
    captouch_buffer.data[captouch_buffer.write_index].state         = state;
    captouch_buffer.data[captouch_buffer.write_index].key_data      = data;
    captouch_buffer.data[captouch_buffer.write_index].time_stamp    = time_stamp;
    
    captouch_buffer.write_index++;
    captouch_buffer.write_index &= (CAPTOUCH_BUFFER_SIZE - 1);
}

void captouch_pop_one_event_from_buffer(hal_captouch_event_t *key_event)
{
    key_event->state     = captouch_buffer.data[captouch_buffer.read_index].state;
    key_event->key_data  = captouch_buffer.data[captouch_buffer.read_index].key_data;
    key_event->time_stamp= captouch_buffer.data[captouch_buffer.read_index].time_stamp;
    captouch_buffer.read_index++;
    captouch_buffer.read_index &= (CAPTOUCH_BUFFER_SIZE - 1);
}

uint32_t captouch_get_buffer_left_size(void)
{
    if (captouch_buffer.write_index >= captouch_buffer.read_index) {
        return (CAPTOUCH_BUFFER_SIZE - (captouch_buffer.write_index - captouch_buffer.read_index));
    } else {
        return (captouch_buffer.read_index - captouch_buffer.write_index);

    }
}

hal_captouch_status_t captouch_rtc_clk_control(bool is_en)
{
    hal_rtc_status_t rtc_ret;

    if (is_en == true) {
        rtc_ret = hal_rtc_captouch_init();
    }
    else {
        rtc_ret = hal_rtc_captouch_deinit();
    }
    
    if (rtc_ret != HAL_RTC_STATUS_OK) {
        log_hal_msgid_info("[Captouch] RTC init/deinit:%d fail!\r\n",1,is_en);
        return HAL_CAPTOUCH_STATUS_ERROR;
    }
    else {
        return HAL_CAPTOUCH_STATUS_OK;
    }

}


void captouch_analog_init(hal_captouch_config_t *config)
{
    uint16_t wdata;
    uint16_t rdata;
	uint8_t  mavg_r;
	uint8_t  avg_s;
    #if 0
    uint32_t triming;
    #endif
    
    //set TOUCH_HW_DBG_SEL [15:13] to 0x7 to avoid debugout signal toggle  
    //default set the analog clock to 32K, mavg_r to 10, avg_s to 5

	if (config->mavg_r == 0xff) {
		mavg_r = 10;
	}
	else {
		mavg_r = config->mavg_r;
	}

	if (config->avg_s == 0xff) {
		avg_s = 5;
	}
	else {
		avg_s = config->avg_s;
	}
	
    wdata = (0x7<<13)|(CAPTOUCH_ANA_TOUCH_CLK_32K<<7)|(mavg_r<<3)|(avg_s);
    captouch_analog_write_data(TOUCH_CON1, wdata);
    
//adc trimming setting from efuse
#if 0   
    triming = cap_reg32(0xa20a024c);
    if (triming & (1<<20)) {
        //write ADC triming gain
        rdata = ((triming>>9)&0x1ff);  
        captouch_analog_write_data(TOUCH_CORSE_CAP_CFG1,rdata);

        //write ADC triming offset
        rdata = ((triming)&0x1ff);  
        captouch_analog_write_data(TOUCH_CORSE_CAP_CFG2,rdata);

        //enable analog adc triming
        rdata = captouch_analog_read_data(TOUCH_CORSE_CAP_CFG1);
        rdata |= (1<<9);  
        captouch_analog_write_data(TOUCH_CORSE_CAP_CFG1,rdata);
    }
    else {
        //disable analog adc triming
        rdata = captouch_analog_read_data(TOUCH_CORSE_CAP_CFG1);
        rdata &= ~(1<<9);  
        captouch_analog_write_data(TOUCH_CORSE_CAP_CFG1,rdata);
    }
#endif  


    /*analog poweron sequence satart*/
    //enable analog touch circuit, enable adc circuit, enable OP circuit
    rdata = captouch_analog_read_data(TOUCH_ANA_CFG0);
    rdata |= TOUCH_ADC_EN + TOUCH_EN + TOUCH_EN_OP;  
    captouch_analog_write_data(TOUCH_ANA_CFG0,rdata);
    hal_gpt_delay_us(10);

    //Enable per-channel power saving,deactive reset to analg circuit
    //Enale analog output isolation, enable normal mode, enable OP normal mode
    rdata = TOUCH_PER_CH_GATING_EN + TOUCH_DA_RST_N + TOUCH_AD_ISO_ENABLE +TOUCH_EN_LP_NORMAL + TOUCH_EN_OP_LP_NORMAL;  
    captouch_analog_write_data(TOUCH_LP_CON,rdata);

    //Enable per-channel power saving,deactive reset to analg circuit
    //Disaable analog output isolation, enable normal mode, enable OP normal mode
    //rdata = TOUCH_PER_CH_GATING_EN + TOUCH_DA_RST_N +TOUCH_EN_LP_NORMAL + TOUCH_EN_OP_LP_NORMAL;  
    rdata = TOUCH_DA_RST_N +TOUCH_EN_LP_NORMAL + TOUCH_EN_OP_LP_NORMAL; 
    captouch_analog_write_data(TOUCH_LP_CON,rdata);
    hal_gpt_delay_us(40);
    /*analog poweron sequence end*/

    //enable channel sensing,enable timelot,enable auto suspend,enable auto cal
    //note: if TOUCH_BACK2BACK_EN enable,power will increase
    //rdata = (captouch_context.used_channel_map<<8) + TOUCH_CAL_AUTOSUSPEND + TOUCH_CAL_EN;  
    rdata = (captouch_context.used_channel_map<<8) + TOUCH_BACK2BACK_EN + TOUCH_CAL_AUTOSUSPEND + TOUCH_CAL_EN;  
    captouch_analog_write_data(TOUCH_CON0,rdata);
 
    captouch_analog_write_data(TOUCH_ANA_CFG1,0x4000);
    captouch_analog_write_data(TOUCH_ANA_CFG2,0x20);
}


uint32_t captouch_get_buffer_data_size(void)
{
    return (CAPTOUCH_BUFFER_SIZE - captouch_get_buffer_left_size());
}

bool captouch_clk_control(bool is_clock_on)
{
    uint16_t rdata;
    bool backup;

    rdata = captouch_analog_read_data(TOUCH_CON0); 
    
    if (rdata&TOUCH_CLK_EN) {
        backup = true;  
    }
    else {
        backup = false;
    }
    
    if (is_clock_on == true) {
        rdata |= TOUCH_CLK_EN;          //enable touch clk
    }
    else {
        rdata &= ~TOUCH_CLK_EN;         //disable touch clk
    }
    captouch_analog_write_data(TOUCH_CON0,rdata);

    //close captouch clk should wait to effect
    if (is_clock_on == false) {
        rdata = captouch_analog_read_data(TOUCH_LP_CON);
        if ((rdata&TOUCH_EN_LP_LOWPOWER)== TOUCH_EN_LP_LOWPOWER) {
            hal_gpt_delay_ms(2);
        }
        else {
            hal_gpt_delay_us(100);
        }
    }

    return backup;
    
}

void captouch_control_pdn_clock(bool is_clock_on)
{
    if (is_clock_on == true) {
        if (hal_clock_is_enabled(HAL_CLOCK_CG_CAP_TOUCH) == false) {
            hal_clock_enable(HAL_CLOCK_CG_CAP_TOUCH);
        }
    }
    else {
        hal_clock_disable(HAL_CLOCK_CG_CAP_TOUCH);
    }
}

void captouch_analog_write_data(uint8_t waddr, uint16_t wdata)
{
    captouch->WIRE3_CON1.CON = wdata| (waddr<<16) |(CAPTOUCH_WIRE3_WRITE);
    while(captouch->WIRE3_CON0.CELLS.BUSY);
    captouch->WIRE3_CON0.CELLS.START = 1;
    while(captouch->WIRE3_CON0.CELLS.BUSY);
}

uint16_t captouch_analog_read_data(uint8_t raddr)
{
    captouch->WIRE3_CON1.CELLS.ADDR = raddr;
    captouch->WIRE3_CON1.CELLS.RW   = CAPTOUCH_WIRE3_READ;
    while(captouch->WIRE3_CON0.CELLS.BUSY);
    captouch->WIRE3_CON0.CELLS.START = 1;
    while(captouch->WIRE3_CON0.CELLS.BUSY);
    
    return captouch->WIRE3_CON0.CELLS.RDATA;
}

bool captouch_get_event_from_fifo(uint32_t *event,uint32_t *tiemstap)
{
    if (captouch->TOUCH_HIF_CON0.CELLS.FIFO_OVERFLOW) {
        log_hal_msgid_info("event overflow occured\r\n", 0);
    }
            
    if (captouch->TOUCH_HIF_CON1.CELLS.EVENT_PENDING == 1) {

        /*read timestamp*/
        *tiemstap = captouch->TOUCH_HIF_CON2;

        /*read event type*/
        *event    = (uint32_t)captouch->TOUCH_HIF_CON1.CELLS.EVENT_TYPE;
            
        /*pop event*/
        captouch->TOUCH_HIF_CON1.CELLS.EVENT_POP = 1;
        
        return true;
    }
    else {
        return false;
    }
}

void captouch_set_fine_cap(hal_captouch_channel_t channel,int16_t fine_tune)
{
    uint8_t addr;

    //set fine tune
    addr = CAPTOUCH_ANALOG.CAL_OUT_MAN[channel];
    //log_hal_info("captouch_set_fine_cap, addr:0x%x, data:0x%x\r\n",addr,fine_tune);
    captouch_analog_write_data(addr,fine_tune);
}

void captouch_set_coarse_cap(hal_captouch_channel_t channel, uint8_t coarse_tune)
{
    uint8_t addr;
    uint16_t shift;
    uint16_t rdata;
    
    //set coarse tune
    addr = CAPTOUCH_ANALOG.CORSE_CAP_CFG[channel/4];
    rdata = captouch_analog_read_data(addr);
    if (channel<4) {
        shift = (channel*3);
    }
    else {
        shift = ((channel-4)*3);
    }
    rdata &= ~(0x07<<shift);
    rdata |= (coarse_tune<<shift);
    captouch_analog_write_data(addr,rdata);
}

uint8_t captouch_get_coarse_cap(hal_captouch_channel_t channel)
{
    uint8_t addr,ret;
    uint16_t shift;
    uint16_t rdata;
    
    //set coarse tune
    addr = CAPTOUCH_ANALOG.CORSE_CAP_CFG[channel/4];
    rdata = captouch_analog_read_data(addr);
    if (channel<4) {
        shift = (channel*3);
    }
    else {
        shift = ((channel-4)*3);
    }
    
    ret = (uint8_t)((rdata>>shift)&0x7);

    return ret;
}

void captouch_set_threshold(hal_captouch_channel_t channel,int16_t high_thr, int16_t low_thr)
{
    uint8_t addr;

    addr = CAPTOUCH_ANALOG.THR_H[channel];  
    captouch_analog_write_data(addr,(uint16_t)high_thr);


    addr = CAPTOUCH_ANALOG.THR_L[channel];  
    captouch_analog_write_data(addr,(uint16_t)low_thr);
}

void captouch_tune_auto_control(bool is_auto)
{
    uint16_t wdata;

    wdata = captouch_analog_read_data(TOUCH_CON0);

    if (is_auto == true) {  
        //wdata |= TOUCH_CAL_EN; 
        wdata &= (~TOUCH_CTRL_MAN);
    }
    else {
        //wdata &= (~TOUCH_CAL_EN);
        wdata |= TOUCH_CTRL_MAN;
    }
    
    captouch_analog_write_data(TOUCH_CON0,wdata);
}

int16_t captouch_7signed_to_16signed(uint16_t data)
{
    uint16_t abs;
    
    if (data&(1<<6)) {
        abs = data+0xff80;
    }
    else {
        abs = data;
    }

    return (int16_t)abs;
}

int16_t captouch_9signed_to_16signed(uint16_t data)
{
    uint16_t abs;
    
    if (data&(1<<8)) {
        abs = data+0xfe00;
    }
    else {
        abs = data;
    }
    return (int16_t)abs;
}
#if 0
int16_t captouch_sort_and_get_avg_data(int16_t *p, uint32_t data_len)
{
    int16_t max;
    int16_t min;
    int16_t sum;
    int16_t avg;
    uint32_t i;

    max = *(p+0);
    min = *(p+0);
    sum = *(p+0);
    for(i=1;i<data_len;i++) {
        sum += *(p+i);
        
        if(*(p+i)>max) {
            max = *(p+i);
        } 

        if(*(p+i)<min) {
            min = *(p+i);
        } 
    }
    
    avg = (sum - max - min)/(data_len - 2);

    log_hal_msgid_info("max=%d, min=%d\r\n",2,max,min);

    return avg;
}
#endif
uint8_t captouch_tune_find_coare_cap(hal_captouch_channel_t channel) 
{

    int16_t vadc_dbg,cal_dbg,cal_out;
    uint8_t  coare_temp[3],count;
    uint16_t i,temp,rdata;
    uint32_t time,delay_time;
    bool is_tune_ok;
    
    
    count = 0;
    rdata =  captouch_analog_read_data(TOUCH_CON1);
    time  = (rdata>>3) & 0xf;
    delay_time = captouch_key_time[time]/3;
    log_hal_msgid_info("time:%d, key_time=%d\r\n",2,time,delay_time);

    coare_temp[0] = 0;
    coare_temp[1] = 0;
    coare_temp[2] = 0;

    is_tune_ok = false;
    
    //set corse cap ,total 8 level
    for(i=0;i<8;i++) {
        captouch_clk_control(false);
        captouch_set_coarse_cap(channel,i);
        captouch_clk_control(true);
        
        hal_gpt_delay_ms(delay_time);
        
        temp = captouch_analog_read_data(TOUCH_VADC_DBG);
        vadc_dbg = captouch_9signed_to_16signed(temp);
        
        temp = captouch_analog_read_data(TOUCH_CAL_DBG);
        cal_dbg = captouch_7signed_to_16signed(temp);


        temp = captouch_analog_read_data(CAPTOUCH_ANALOG.CAL_OUT_MAN[channel]);
        cal_out = captouch_7signed_to_16signed(temp);

        log_hal_msgid_info("[c_tune ch%d]:crs_cap=%d cnt=%d, cal_dbg=%d, cal_out=%d, vadc_dbg=%d\r\n",6,channel,i,count,cal_dbg,cal_out,(int)vadc_dbg);

        if (i<6) {
            if (((cal_dbg > CAL_DBG_MIN_L1) && (cal_dbg < CAL_DBG_MAX_L1)) && ((vadc_dbg > VADC_DBG_MIN_L1) && (vadc_dbg < VADC_DBG_MAX_L1))) {
                coare_temp[1] = i;
                is_tune_ok = true;
                break;
            }
        }
        else {
           if (((cal_dbg > CAL_DBG_MIN_L2) && (cal_dbg < CAL_DBG_MAX_L2)) && ((vadc_dbg > VADC_DBG_MIN_L2) && (vadc_dbg < VADC_DBG_MAX_L2))) {
                coare_temp[1] = i;
                is_tune_ok = true;
                break;
            }
        }
                
        if (((cal_dbg > CAL_DBG_MIN_L3) && (cal_dbg < CAL_DBG_MAX_L3)) && ((vadc_dbg > VADC_DBG_MIN_L3) && (vadc_dbg < VADC_DBG_MAX_L3))) {
            coare_temp[count] = i;
            count++;
            if (count>= 3) {
                is_tune_ok = true;
                break;
            }   
        }
    }  

    if (is_tune_ok == true) {
        return coare_temp[1];
    }
    else {
        return 0xff;
    }
}

void captouch_switch_debug_sel(hal_captouch_channel_t channel)
{
    uint16_t rdata;

    //captouch_clk_control(captouch_disable);
    rdata = captouch_analog_read_data(TOUCH_CON1);

    //log_hal_info("current ch:%d, last:%d, rdata=0x%x\r\n",channel,mask,rdata);
    rdata &= ~TOUCH_SW_DBG_SEL_MASK;
    rdata |= (channel<<10);
    captouch_analog_write_data(TOUCH_CON1, rdata);
    hal_gpt_delay_ms(1);
    //captouch_clk_control(captouch_enable);
}
void captouch_get_tune_state(hal_captouch_channel_t channel,hal_captouch_tune_data_t *tune_data)
{
    uint16_t temp;

    captouch_switch_debug_sel(channel);
    
    tune_data->coarse_cap = captouch_get_coarse_cap(channel);           //coares cap value
    
    temp =  captouch_analog_read_data(TOUCH_CAL_DBG);                   //fine cap value
    tune_data->fine_cap = captouch_7signed_to_16signed(temp);


    temp = captouch_analog_read_data(TOUCH_VADC_DBG);  //vadc value
    tune_data->vadc = captouch_9signed_to_16signed(temp);

    temp = captouch_analog_read_data(TOUCH_AVG_DBG);  //vadc value
    tune_data->avg_adc  = captouch_9signed_to_16signed(temp);

    temp = captouch_analog_read_data( CAPTOUCH_ANALOG.CAL_OUT_MAN[channel]);  //vadc value
    tune_data->man  = captouch_7signed_to_16signed(temp);
    
}

bool captouch_hw_auto_tune(hal_captouch_channel_t channel, hal_captouch_tune_data_t *tune_data)
{
    uint8_t  coare_index;
    uint16_t rdata;
    uint32_t time;
        
    captouch_tune_auto_control(true);

    coare_index = captouch_tune_find_coare_cap(channel);


    rdata =  captouch_analog_read_data(TOUCH_CON1);
    time  = (rdata>>3) & 0xf;
    
    if(coare_index == 0xff) {
        captouch_set_coarse_cap(channel,7);
    }
    else {
        captouch_set_coarse_cap(channel,coare_index);
    }
    
    hal_gpt_delay_us((captouch_key_time[time]*1000)/3);
    captouch_get_tune_state(channel,tune_data);

    if (coare_index == 0xff){
        return false;
    }
    else {
        return true;
    }
}   

bool captouch_sw_auto_tune(hal_captouch_channel_t channel, hal_captouch_tune_data_t *tune_data)
{
    
    uint16_t temp,cal_out;
    int16_t vadc_dbg;
    uint8_t coare_index;
    int16_t  find_cap[3];
    bool is_tune_ok;

    captouch_tune_auto_control(false);

    coare_index = 0;
    find_cap[0] = -64;
    find_cap[1] = 0;
    find_cap[2] = 63;

    is_tune_ok = false;
    while(1) {

        captouch_clk_control(captouch_disable);
        captouch_set_fine_cap(channel,find_cap[1]);
        captouch_set_coarse_cap(channel,coare_index);
        captouch_clk_control(captouch_enable);
        
        hal_gpt_delay_ms(2);
        temp = captouch_analog_read_data(TOUCH_VADC_DBG);
        vadc_dbg = captouch_9signed_to_16signed(temp);

        temp = captouch_analog_read_data(CAPTOUCH_ANALOG.CAL_OUT_MAN[channel]);
        cal_out = captouch_7signed_to_16signed(temp);

        log_hal_msgid_info("[f_tune ch%d]:crs_cap=%d,f_cap[0]=%d,f_cap[1]=%d,f_cap[2]=%d, vadc_dbg=%d,cal_out=%d\r\n",7,\
            channel,coare_index,find_cap[0],find_cap[1],find_cap[2],vadc_dbg,cal_out);

        // the adc value should be +-32
        if (vadc_dbg>=10) {             //if adc is bigger, the fine_cap should be bigger to decrease the adc value
            find_cap[0] =  find_cap[1]; //the lower limit should move to the current value
            find_cap[1] = (find_cap[2] + find_cap[1])/2;
        }
        else if(vadc_dbg<=(-20)) {      //if adc is smller, the fine_cap should be smller to increase the adc value
            find_cap[2] = find_cap[1];  //the upper limit should move to the current value
            find_cap[1] = (find_cap[1] + find_cap[0])/2;    
        }
        else {
            is_tune_ok = true;
            break;
        }

        if ((find_cap[1]>45) || (find_cap[1]<=(-45))){
            coare_index++;
            if (coare_index>=8) {
                is_tune_ok = false;
                break;
            }   
            find_cap[0] = -64;
            find_cap[1] = 0;
            find_cap[2] = 63;
        }
        
    }

    captouch_get_tune_state(channel,tune_data);
    log_hal_msgid_info("ch:%d, crs_cap=%d, f_cap=%d, man_cap=%d, avg=%d,vadc=%d\r\n",6,\
                channel,tune_data->coarse_cap, tune_data->fine_cap, tune_data->man,tune_data->avg_adc, tune_data->vadc);

    return is_tune_ok;

}   

void captouch_man_tune(hal_captouch_channel_t channel, hal_captouch_tune_data_t *tune_data)
{
    captouch_tune_auto_control(false);
    hal_gpt_delay_ms(1);
    captouch_get_tune_state(channel,tune_data);
}   

void captouch_channel_sense_control(uint8_t sensing_bit_map, bool en)
{
    uint16_t rdata;

    rdata = captouch_analog_read_data(TOUCH_CON0);
    if (en == true) {
        rdata |=  (sensing_bit_map<<8);
    }
    else {
        rdata &= ~((sensing_bit_map<<8));
    }
    captouch_analog_write_data(TOUCH_CON0,rdata);
}

void captouch_int_control(bool en)
{
    uint16_t rdata;

    //captouch irq interrupt
    rdata  = captouch_analog_read_data(TOUCH_CON0);

    if (en==true) {
        rdata |= (TOUCH_INT_EN + TOUCH_WAKE_EN);
    }
    else {
        rdata &= ~(TOUCH_INT_EN + TOUCH_WAKE_EN);
    }
    captouch_analog_write_data(TOUCH_CON0,rdata);
}

void captouch_channel_int_control(uint8_t channel_bit_map, bool en)
{
    uint16_t rdata[2];

    //high and low threhold wakeup&int interrupt
    rdata[0] = captouch_analog_read_data(TOUCH_CH_WAKEUP_EN);
    rdata[1] = captouch_analog_read_data(TOUCH_CH_INT_EN);

    
    if (en == true) {
        rdata[0] |= ((channel_bit_map<<(8)) + channel_bit_map);
        rdata[1] |= ((channel_bit_map<<(8)) + channel_bit_map);
    }
    else {
        rdata[0] &= ~((channel_bit_map<<(8)) + channel_bit_map);
        rdata[1] &= ~((channel_bit_map<<(8)) + channel_bit_map);
    }
      
    captouch_analog_write_data(TOUCH_CH_WAKEUP_EN,rdata[0]);
    captouch_analog_write_data(TOUCH_CH_INT_EN,rdata[1]);
}



void captouch_call_user_callback(void)
{
    hal_captouch_callback_context_t *context;

    context = &captouch_context.captouch_callback;

    if (captouch_context.has_initilized != true) {
        return;
    }

    context->callback(context->user_data);
}


void captouch_interrupt_handler(hal_nvic_irq_t irq_number)
{   
    uint32_t event;
    uint32_t time_stamp;
    uint32_t key_data;
    #ifdef CAPTOUCH_DEBUG_1
    int16_t temp,cal;
    #endif
    hal_captouch_key_state_t state;
    hal_captouch_tune_data_t tune_data;

    //static uint32_t count = 0;
    //count++;
    //log_hal_info("count=%d\r\n",count);
    
    while(1) {
        if (captouch_get_event_from_fifo(&event,&time_stamp)== false) {
            break;
        }

        if (event <= 7) {
            state    = HAL_CAP_TOUCH_KEY_PRESS;
            key_data = event; 
        }
        else {
            state = HAL_CAP_TOUCH_KEY_RELEASE;
            key_data = event - 8; 
        }
        captouch_get_tune_state(key_data,&tune_data);

        #ifdef CAPTOUCH_DEBUG_1
        temp = captouch_analog_read_data(CAPTOUCH_ANALOG.CAL_OUT_MAN[0]);
        cal = captouch_7signed_to_16signed(temp);
        
        log_hal_msgid_info("ch:%d c_cap=%d, df_cap=%d, mf_cap=%d,avg=%d, vadc=%d\r\n",6, \
                key_data,tune_data.coarse_cap,tune_data.fine_cap,cal,tune_data.avg_adc,tune_data.vadc);
        #endif

        if (captouch_context.has_initilized == true) {
            captouch_push_one_event_to_buffer(state,key_data,time_stamp);
        }
    }

    rtc_captouch_ack();

    if (captouch_context.has_initilized == true) {
        captouch_call_user_callback();  
    }

}
void captouch_register_nvic_callback(void)
{
    uint32_t mask;
    static bool register_flag;

    if (register_flag == false) {
        hal_nvic_save_and_set_interrupt_mask(&mask);
        hal_nvic_register_isr_handler(CAP_TOUCH_IRQn, captouch_interrupt_handler);
        hal_nvic_enable_irq(CAP_TOUCH_IRQn);
        hal_nvic_restore_interrupt_mask(mask);
        register_flag = true;
    }

}
    
#endif //HAL_CAPTOUCH_MODULE_ENABLED
 
