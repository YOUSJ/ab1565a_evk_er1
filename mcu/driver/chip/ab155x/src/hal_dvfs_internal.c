/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#include "hal_dvfs.h"
#ifdef HAL_DVFS_MODULE_ENABLED
#include "hal.h"
#include "hal_dvfs_internal.h"
#include "hal_nvic_internal.h"
#include "memory_attribute.h"

#ifdef  HAL_DVFS_DEBUG_ENABLE
#define dvfs_debug(_message,...) log_hal_info("[DVFS] "_message, ##__VA_ARGS__)
#else
#define dvfs_debug(_message,...)
#endif


uint32_t dvfs_handle_array[32];
int frequency_Ctrl[DVFS_MAX_SPEED];

ATTR_RWDATA_IN_CACHED_SYSRAM bool dvfs_switched_to_privileged;

int dvfs_query_frequency_drift(uint32_t freq, const uint32_t *frequency, uint32_t num, hal_dvfs_freq_relation_t relation) {
    unsigned int opp = 0;
    if (freq > frequency[DVFS_HIGH_SPEED_156M]) {
        return DVFS_HIGH_SPEED_156M;
    } else if (freq < frequency[DVFS_LOW_SPEED_26M]) {
        return DVFS_LOW_SPEED_26M;
    } else {
        if (freq >= frequency[DVFS_LOW_SPEED_26M] && freq <= frequency[DVFS_FULL_SPEED_78M]) {
            opp = DVFS_LOW_SPEED_26M;
        } else if (freq >= frequency[DVFS_FULL_SPEED_78M] && freq <= frequency[DVFS_HIGH_SPEED_156M]) {
            opp = DVFS_FULL_SPEED_78M;
        } else {
            opp = DVFS_HIGH_SPEED_156M;
        }
        if (relation == HAL_DVFS_FREQ_RELATION_L) {
            opp += 1;
        }
        return opp;
    }
}
int dvfs_query_frequency(uint32_t freq, const uint32_t *frequency, uint32_t num,hal_dvfs_freq_relation_t relation){
    unsigned int opp;
    unsigned int sta=0;
    for (opp = 0; opp <= num; opp++) {
        if (freq == frequency[opp]) {
            sta=1;
            return opp;
        }
        if((opp==3) && (sta == 0)) {
            log_hal_msgid_error("[%d can't search frequency in dvfs table,use drift check]\r\n",1,freq);
            return dvfs_query_frequency_drift(freq, frequency, num,relation);
        }
    }
    log_hal_msgid_error("[%d can't search frequency in any case ,switch max speend]\r\n",1,freq);
    return DVFS_HIGH_SPEED_156M;
}
//D-die control api
void dvfs_set_register_value(uint32_t address, short int mask, short int shift, short int value) {
    uint32_t mask_buffer,target_value;
    mask_buffer = (~(mask << shift));
    target_value = *((volatile uint32_t *)(address));
    target_value &= mask_buffer;
    target_value |= (value << shift);
    *((volatile uint32_t *)(address)) = target_value;
}

uint32_t dvfs_get_register_value(uint32_t address, short int mask, short int shift) {
    uint32_t change_value, mask_buffer;
    mask_buffer = (mask << shift);
    change_value = *((volatile uint32_t *)(address));
    change_value &=mask_buffer;
    change_value = (change_value>> shift);
    return change_value;
}


#endif /* HAL_DVFS_MODULE_ENABLED */

