/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_uart.h"
#include "hal_spm.h"
#ifdef HAL_UART_MODULE_ENABLED

#include "memory_attribute.h"
#ifdef HAL_SLEEP_MANAGER_ENABLED
#include "hal_gpt.h"
#include "hal_sleep_manager.h"
#include "hal_sleep_manager_internal.h"
#endif
#include "hal_resource_assignment.h"
#include "core_cm4.h"
#include "hal_pdma_internal.h"
#include "hal_nvic.h"
#include "hal_nvic_internal.h"
#include "hal_clock.h"
#include "hal_uart_internal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*uart assert debug status*/
static vdma_status_t g_debug_status = 0;
static uint32_t g_debug_receive_count = 0;
static uint32_t g_debug_avail_count = 0;


static bool g_uart_global_data_initialized = false;
static hal_uart_baudrate_t g_baudrate[HAL_UART_MAX];
static hal_uart_port_t g_uart_port_for_logging = HAL_UART_MAX;
#ifdef HAL_SLEEP_MANAGER_ENABLED
static bool g_uart_frist_send_complete_interrupt[HAL_UART_MAX];
static bool g_uart_send_lock_status[HAL_UART_MAX];
static uart_flowcontrol_t g_uart_flowcontrol_status[HAL_UART_MAX] = {UART_FLOWCONTROL_NONE, UART_FLOWCONTROL_NONE, UART_FLOWCONTROL_NONE};
static uart_sw_flowcontrol_config_t g_uart_sw_flowcontrol_config[HAL_UART_MAX];
static hal_uart_config_t g_uart_config[HAL_UART_MAX];
static sleep_management_lock_request_t uart_sleep_handle[HAL_UART_MAX] = {SLEEP_LOCK_UART0, SLEEP_LOCK_UART1, SLEEP_LOCK_UART2};
//static bool g_uart_irq_pending_status[HAL_UART_MAX];
#endif
static volatile uart_hwstatus_t g_uart_hwstatus[HAL_UART_MAX];
static uart_callback_t g_uart_callback[HAL_UART_MAX];
static uart_dma_callback_data_t g_uart_dma_callback_data[HAL_UART_MAX * 2];
static hal_uart_dma_config_t g_uart_dma_config[HAL_UART_MAX];
static const uint32_t g_uart_baudrate_map[] = {110, 300, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600, 2000000, 3000000, 3200000};

extern UART_REGISTER_T *const g_uart_regbase[];
extern hal_clock_cg_id g_uart_port_to_pdn[];
extern hal_nvic_irq_t g_uart_port_to_irq_num[];

/* for logging with new HW design */
extern void log_sleep_restore_callback(void);
/* for ATCI wakeup system */
#ifdef ATCI_ENABLE
extern hal_uart_port_t g_atci_uart_port;
#endif

static bool uart_port_is_valid(hal_uart_port_t uart_port)
{
    return (uart_port < HAL_UART_MAX);
}

static bool uart_baudrate_is_valid(hal_uart_baudrate_t baudrate)
{
    return (baudrate < HAL_UART_BAUDRATE_MAX);
}

static bool uart_config_is_valid(const hal_uart_config_t *config)
{
    return ((config->baudrate < HAL_UART_BAUDRATE_MAX) &&
            (config->word_length <= HAL_UART_WORD_LENGTH_8) &&
            (config->stop_bit <= HAL_UART_STOP_BIT_2) &&
            (config->parity <= HAL_UART_PARITY_EVEN));
}

/* triggered by vfifo dma rx thershold interrupt or UART receive timeout interrupt.
 * 1. When vfifo dma rx thershold interrupt happen,
 * this function is called with is_timeout=false.
 * then call suer's callback to notice that data can be fetched from receive buffer.
 * 2. When UART receive timeout interrupt happen,
 * this function is called with is_timeout=true.
 * then call suer's callback to notice that data can be fetched from receive buffer.
 */
void uart_receive_handler(hal_uart_port_t uart_port, bool is_timeout)
{
    vdma_channel_t channel;
    uint32_t avail_bytes;
    hal_uart_callback_t callback;
    void *arg;
    UART_REGISTER_T *uartx;
    vdma_status_t status;
    uint8_t vdma_irq_is_enabled;
    static  uint32_t         state_machine_ary[3] = {0,0,0};

    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_DMA_INITIALIZED) {
        UART_ASSERT();
        return;
    }

    uartx   = g_uart_regbase[uart_port];
    channel = uart_port_to_dma_channel(uart_port, 1);
    status  = vdma_get_available_receive_bytes(channel, &avail_bytes);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return;
    }
 
    if(avail_bytes == 0){
        /*Enable dma irq & set dma threshold to '1'*/
        vdma_set_threshold(channel,1);
        vdma_enable_interrupt(channel);
        /*disable uart timeout irq*/
        uart_mask_receive_interrupt(uartx);
        state_machine_ary[uart_port] = 1;
        return;
    }else if(state_machine_ary[uart_port] == 1){
        vdma_set_threshold(channel, g_uart_dma_config[uart_port].receive_vfifo_threshold_size);
        uart_unmask_receive_interrupt(uartx);
        state_machine_ary[uart_port] = 2;
    }

    callback = g_uart_callback[uart_port].func;
    arg      = g_uart_callback[uart_port].arg;
    if (callback == NULL) {
        UART_ASSERT();
        return;
    }

    if (is_timeout == true) {
        uart_clear_timeout_interrupt(uartx);
        uart_mask_receive_interrupt(uartx);
    }
    /* for callback re-entry issue */
    /* Avoid user didn't get data in the callback, then user will receive one dummy irq*/
    vdma_get_interrupt_status(channel, &vdma_irq_is_enabled);
    if (vdma_irq_is_enabled == true) {
        vdma_disable_interrupt(channel);
    }

    if(avail_bytes != 0){
        uart_mask_receive_interrupt(uartx);
    }

    callback(HAL_UART_EVENT_READY_TO_READ, arg);
    if (vdma_irq_is_enabled == true) {
        vdma_enable_interrupt(channel);
    }

    status = vdma_get_available_receive_bytes(channel, &avail_bytes);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return;
    }
    if (avail_bytes >= g_uart_dma_config[uart_port].receive_vfifo_threshold_size) {
        status = vdma_disable_interrupt(channel);
        if (status != VDMA_OK) {
            UART_ASSERT();
            return;
        }
    }

    if(state_machine_ary[uart_port] == 2){
        vdma_disable_interrupt(channel);
        state_machine_ary[uart_port] = 3;
    }
}

/* triggered by vfifo dma tx thershold interrupt or uart transmit complete interrupt.
 * 1. When vfifo dma tx thershold interrupt happen,
 * this function is called with is_send_complete_trigger=false.
 * then call suer's callback to notice that data can be put in send buffer again.
 * 2. When UART transmit complete interrupt happen,
 * this function is called with is_send_complete_trigger=true.
 * Now all user data has been sent out, so we call hal_sleep_manager_unlock_sleep()
 * to unlock sleep.
 */
void uart_send_handler(hal_uart_port_t uart_port, bool is_send_complete_trigger)
{
    vdma_channel_t channel;
    vdma_status_t status;
    uint32_t compare_space, avail_space;
    hal_uart_callback_t callback;
    void *arg;
#ifdef HAL_SLEEP_MANAGER_ENABLED
    uint32_t irq_status;
#endif

    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_DMA_INITIALIZED) {
        UART_ASSERT();
        return;
    }

    channel = uart_port_to_dma_channel(uart_port, 0);
    if (is_send_complete_trigger == true) {
#ifdef HAL_SLEEP_MANAGER_ENABLED
        if (g_uart_frist_send_complete_interrupt[uart_port] == false) {
            hal_nvic_save_and_set_interrupt_mask(&irq_status);
            status = vdma_get_available_send_space(channel, &avail_space);
            if (status != VDMA_OK) {
                hal_nvic_restore_interrupt_mask(irq_status);
                UART_ASSERT();
                return;
            }
            if (g_uart_dma_config[uart_port].send_vfifo_buffer_size - avail_space == 0) {
                if (g_uart_send_lock_status[uart_port] == true) {
                    if( hal_sleep_manager_is_sleep_handle_alive(uart_sleep_handle[uart_port]) == true) {
                        hal_sleep_manager_unlock_sleep(uart_sleep_handle[uart_port]);
                    }
                    g_uart_send_lock_status[uart_port] = false;
                }
            }
            hal_nvic_restore_interrupt_mask(irq_status);
        } else {
            g_uart_frist_send_complete_interrupt[uart_port] = false;
        }
        return;
#endif
    } else {
        callback = g_uart_callback[uart_port].func;
        arg = g_uart_callback[uart_port].arg;
        if (callback == NULL) {
            UART_ASSERT();
            return;
        }
        /* for callback re-entry issue */
        vdma_disable_interrupt(channel);
        callback(HAL_UART_EVENT_READY_TO_WRITE, arg);
        vdma_enable_interrupt(channel);

        status = vdma_get_available_send_space(channel, &avail_space);
        if (status != VDMA_OK) {
            UART_ASSERT();
            return;
        }
        compare_space = g_uart_dma_config[uart_port].send_vfifo_buffer_size
                        - g_uart_dma_config[uart_port].send_vfifo_threshold_size;
        if (avail_space >= compare_space) {
            status = vdma_disable_interrupt(channel);
            if (status != VDMA_OK) {
                UART_ASSERT();
                return;
            }
        }
    }
}

/* Only triggered by UART error interrupt */
void uart_error_handler(hal_uart_port_t uart_port)
{
    UART_REGISTER_T *uartx;
    hal_uart_callback_t callback;
    void *arg;

    uartx = g_uart_regbase[uart_port];

    if (!uart_verify_error(uartx)) {
        uart_purge_fifo(uartx, 1);
        uart_purge_fifo(uartx, 0);
        callback = g_uart_callback[uart_port].func;
        arg = g_uart_callback[uart_port].arg;
        if (callback == NULL) {
            UART_ASSERT();
            return;
        }
        callback(HAL_UART_EVENT_TRANSACTION_ERROR, arg);
    }
}

static void uart_dma_callback_handler(vdma_event_t event, void  *user_data)
{
    /* fix warning */
    vdma_event_t local_event = event;
    local_event = local_event;
    uart_dma_callback_data_t *callback_data = (uart_dma_callback_data_t *)user_data;

    if (callback_data->is_rx == true) {
        uart_receive_handler(callback_data->uart_port, false);
    } else {
        uart_send_handler(callback_data->uart_port, false);
    }
}
static const uint32_t g_uart_dma_base[HAL_UART_MAX] = {0xA0020610, 0xA0020410, 0xA0020210};
static const uint8_t g_uart_dma_tx_bit[HAL_UART_MAX] = {0x5, 0x3, 0x1};
static const uint32_t g_uart_pdn_value[HAL_UART_MAX] = {0x100000, 0x800, 0x1000};


#ifdef HAL_SLEEP_MANAGER_ENABLED
static const hal_eint_number_t g_uart_eint_num[HAL_UART_MAX] = {HAL_EINT_UART_0_RX, HAL_EINT_UART_1_RX, HAL_EINT_UART_2_RX};
extern uint32_t eint_get_status(void);
#ifdef ATCI_ENABLE



static  void    uart_eint_event_process(hal_uart_port_t uart_port)
{
    if ((eint_get_status() >> g_uart_eint_num[uart_port]) & 0x01) {
        hal_uart_callback_t callback;
        void *arg;

        hal_eint_unmask(g_uart_eint_num[uart_port]);
        /* call ATCI callback */
        callback = g_uart_callback[uart_port].func;
        arg = g_uart_callback[uart_port].arg;
        if (callback == NULL) {
            UART_ASSERT();
        }
        callback(HAL_UART_EVENT_WAKEUP_SLEEP, arg);
    }
}

static void eint_uart_handler(void *parameter)
{
    hal_uart_port_t uart_port;
    for (uart_port = HAL_UART_0; uart_port < HAL_UART_MAX; uart_port++) {
        uart_eint_event_process(uart_port);
    }
}
#endif
void uart_backup_all_registers(void)
{
    hal_uart_port_t uart_port;
    UART_REGISTER_T *uartx;
    uint32_t data_length = 0;
    uint32_t *share_buffer;
    uart_hwstatus_t uart_hwstatus;
    uart_flowcontrol_t uart_flowcontrol;
    hal_uart_callback_t uart_callback;
    void *uart_callback_arg;
    int core_state_dsp0,core_state_dsp1;

    share_buffer = (uint32_t *)HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_START;
    g_uart_port_for_logging = (hal_uart_port_t)(*share_buffer++);

    for (uart_port = HAL_UART_0; uart_port < HAL_UART_MAX; uart_port++) {
        if (uart_port == g_uart_port_for_logging) {
            uartx = g_uart_regbase[uart_port];
            while (hal_hw_semaphore_take(HW_SEMAPHORE_SLEEP) != HAL_HW_SEMAPHORE_STATUS_OK);
            core_state_dsp0 = hal_core_status_read(HAL_CORE_DSP0);
            core_state_dsp1 = hal_core_status_read(HAL_CORE_DSP1);
            if(((core_state_dsp0 == HAL_CORE_OFF)||(core_state_dsp0 == HAL_CORE_SLEEP))&&(((core_state_dsp1 == HAL_CORE_OFF)||(core_state_dsp1 == HAL_CORE_SLEEP)))) {
                data_length = *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x28); //VDMA_FFCNT
                if (data_length <= 10) {
                    while (!((uartx->LSR) & UART_LSR_TEMT_MASK));
                    SPM_CLEAR_LOCK_INFRA;
                    //hal_clock_disable(g_uart_port_to_pdn[uart_port]);
                    //*(volatile uint32_t *)(0xA2270310) = g_uart_pdn_value[uart_port];
                    //global dma top interrupt clr
                    //*(volatile uint32_t *)(0xA020010) = (0x1 << g_uart_dma_tx_bit[uart_port]);
                    //ACK INT
                    //*(volatile uint32_t *)(g_uart_dma_base[uart_port] + 0x10) = 0x8000;
                    //*(volatile uint32_t *)(0xA0020078) = (0x1 << g_uart_dma_tx_bit[uart_port]);
                } else {
                    SPM_SET_LOCK_INFRA;
                    // Set related VFIFO TX channel threshold as 0x1
                    *(volatile uint32_t *)(g_uart_dma_base[uart_port]) = 0x1;

                    //global top interrupt clr
                    *(volatile uint32_t *)(0xA0020010) = (0x1 << g_uart_dma_tx_bit[uart_port]);
                    //ACK INT
                    *(volatile uint32_t *)(g_uart_dma_base[uart_port] + 0x10) = 0x8000;
                    //global top interrupt set
                    *(volatile uint32_t *)(0xA002000C) = (0x1 << g_uart_dma_tx_bit[uart_port]);

                    // Enable related VFIFO TX channel interrupt, ITEN = 1
                    *(volatile uint8_t *)(g_uart_dma_base[uart_port] + 0x7) = 0x1;
                }
            }
            while (hal_hw_semaphore_give(HW_SEMAPHORE_SLEEP) != HAL_HW_SEMAPHORE_STATUS_OK);
        } else if(g_uart_hwstatus[uart_port] != UART_HWSTATUS_UNINITIALIZED) {
            uart_hwstatus = g_uart_hwstatus[uart_port];
            uart_flowcontrol = g_uart_flowcontrol_status[uart_port];
            uart_callback = g_uart_callback[uart_port].func;
            uart_callback_arg = g_uart_callback[uart_port].arg;
            hal_uart_deinit(uart_port);
            g_uart_hwstatus[uart_port] = uart_hwstatus;
            g_uart_flowcontrol_status[uart_port] = uart_flowcontrol;
            g_uart_callback[uart_port].func = uart_callback;
            g_uart_callback[uart_port].arg = uart_callback_arg;
#ifdef ATCI_ENABLE
            /* for ATCI wakeup system */
            //if (g_atci_uart_port == uart_port) {
            hal_eint_config_t config_eint;
            config_eint.trigger_mode = HAL_EINT_EDGE_FALLING;
            config_eint.debounce_time = 0;
            hal_eint_init(g_uart_eint_num[uart_port], &config_eint);
            hal_eint_register_callback(g_uart_eint_num[uart_port], eint_uart_handler, NULL);
            hal_eint_unmask(g_uart_eint_num[uart_port]);
            //}
#endif
        }
    }
}

extern void log_sleep_restore_callback(void);
void uart_restore_all_registers(void)
{
    hal_uart_port_t uart_port;
    UART_REGISTER_T *uartx;
    uart_hwstatus_t uart_hwstatus;
    uint32_t *share_buffer;
    uint32_t u32temp = 0;

    share_buffer = (uint32_t *)HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_START;
    g_uart_port_for_logging = (hal_uart_port_t)(*share_buffer++);


    for (uart_port = HAL_UART_0; uart_port < HAL_UART_MAX; uart_port++) {
        if (uart_port == g_uart_port_for_logging) {
            while (hal_hw_semaphore_take(HW_SEMAPHORE_SLEEP) != HAL_HW_SEMAPHORE_STATUS_OK);
            if(SPM_INFRA_OFF_FLAG != 0) {

                SPM_INFRA_OFF_FLAG = 0;
                uartx = g_uart_regbase[uart_port];
                *(volatile uint32_t *)(0xA0020074) = (0x1 << g_uart_dma_tx_bit[uart_port]); // VDMA_CLOCK

                uartx->DLM_DLL = *share_buffer++;
                uartx->FCR_UNION.FCR = *share_buffer++;
                uartx->LCR_UNION.LCR = *share_buffer++;
                uartx->HIGHSPEED = *share_buffer++;
                uartx->SAMPLE_REG_UNION.SAMPLE_REG = *share_buffer++;
                uartx->RATEFIX_UNION.RATEFIX = *share_buffer++;
                uartx->GUARD = *share_buffer++;
                uartx->SLEEP_REG = *share_buffer++;
                uartx->FRACDIV   = *share_buffer++;
                u32temp = *share_buffer++;

                log_sleep_restore_callback();

                // Disable related VFIFO TX channel interrupt, ITEN = 0
                *(volatile uint8_t *)(g_uart_dma_base[uart_port] + 0x7) = 0x0;
                //global top interrupt clr
                *(volatile uint32_t *)(0xA0020010) = (0x1 << g_uart_dma_tx_bit[uart_port]);
                //ACK INT
                *(volatile uint32_t *)(g_uart_dma_base[uart_port] + 0x10) = 0x8000;
                //hal_nvic_clear_pending_irq(MCU_DMA_IRQn);
                *(volatile uint32_t *)(g_uart_dma_base[uart_port]) = *share_buffer++; //VDMA_COUNT
                *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x4) = *share_buffer++; //VDMA_CON
                *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x1C) = *share_buffer++; //VDMA_PGMADDR
                *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x30) = *share_buffer++; //VDMA_ALTLEN
                *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x34) = *share_buffer++; //VDMA_FFSIZE
                *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x8) = 0x8000; //VDMA_START
                uartx->FCR_UNION.FCR |=  UART_FCR_CLRT_MASK | UART_FCR_CLRR_MASK;
                uartx->DMA_CON_UNION.DMA_CON = u32temp;
                /* Because of hardware limitation, we have to send XON manually
                * when software flow control is turn on for that port.
                */
                if (g_uart_flowcontrol_status[uart_port] == UART_FLOWCONTROL_SOFTWARE) {
                    uartx = g_uart_regbase[uart_port];
                    hal_uart_set_software_flowcontrol(uart_port,
                                                  g_uart_sw_flowcontrol_config[uart_port].xon,
                                                  g_uart_sw_flowcontrol_config[uart_port].xoff,
                                                  g_uart_sw_flowcontrol_config[uart_port].escape_character);
                    uart_internal_send_xon_xoff(uartx, true, false);
                } else if (g_uart_flowcontrol_status[uart_port] == UART_FLOWCONTROL_HARDWARE) {
                    uartx->ESCAPE_REG_UNION.ESCAPE_REG = 0;
                    uartx->MCR_UNION.MCR_CELLS.RTS = UART_MCR_RTS_MASK;
                    uartx->EFR_UNION.EFR_CELLS.HW_FLOW_CONT = (UART_EFR_HW_TX_FLOWCTRL_MASK |
                            UART_EFR_HW_RX_FLOWCTRL_MASK);
                }
            } else {
                // Disable related VFIFO TX channel interrupt, ITEN = 0
                *(volatile uint8_t *)(g_uart_dma_base[uart_port] + 0x7) = 0x0;
                //global top interrupt clr
                *(volatile uint32_t *)(0xA0020010) = (0x1 << g_uart_dma_tx_bit[uart_port]);
                //ACK INT
                *(volatile uint32_t *)(g_uart_dma_base[uart_port] + 0x10) = 0x8000;
                hal_nvic_clear_pending_irq(MCU_DMA_IRQn);
            }
            while (hal_hw_semaphore_give(HW_SEMAPHORE_SLEEP) != HAL_HW_SEMAPHORE_STATUS_OK);
        } else {

#ifdef ATCI_ENABLE
            uart_eint_event_process(uart_port);
            hal_eint_deinit(g_uart_eint_num[uart_port]);
#endif
            uart_hwstatus = g_uart_hwstatus[uart_port];
            if (uart_hwstatus != UART_HWSTATUS_UNINITIALIZED) {
                g_uart_hwstatus[uart_port] = UART_HWSTATUS_UNINITIALIZED;
                hal_uart_init(uart_port, &g_uart_config[uart_port]);
            }
            if (uart_hwstatus == UART_HWSTATUS_DMA_INITIALIZED) {
                hal_uart_set_dma(uart_port, &g_uart_dma_config[uart_port]);
                hal_uart_register_callback(uart_port, g_uart_callback[uart_port].func, g_uart_callback[uart_port].arg);
            }
            /* Because of hardware limitation, we have to send XON manually
            * when software flow control is turn on for that port.
            */
            if (g_uart_flowcontrol_status[uart_port] == UART_FLOWCONTROL_SOFTWARE) {
                uartx = g_uart_regbase[uart_port];
                hal_uart_set_software_flowcontrol(uart_port,
                                                  g_uart_sw_flowcontrol_config[uart_port].xon,
                                                  g_uart_sw_flowcontrol_config[uart_port].xoff,
                                                  g_uart_sw_flowcontrol_config[uart_port].escape_character);
                uart_internal_send_xon_xoff(uartx, true, false);
            } else if (g_uart_flowcontrol_status[uart_port] == UART_FLOWCONTROL_HARDWARE) {
                hal_uart_set_hardware_flowcontrol(uart_port);
            }
        }
    }
}

#endif

hal_uart_status_t hal_uart_set_baudrate(hal_uart_port_t uart_port, hal_uart_baudrate_t baudrate)
{
    uint32_t actual_baudrate, irq_status;
    UART_REGISTER_T *uartx;

    if ((!uart_port_is_valid(uart_port)) ||
        (!uart_baudrate_is_valid(baudrate))) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }

    if (g_uart_hwstatus[uart_port] == UART_HWSTATUS_UNINITIALIZED) {
        return HAL_UART_STATUS_ERROR_UNINITIALIZED;
    }

    hal_nvic_save_and_set_interrupt_mask(&irq_status);
#ifdef HAL_SLEEP_MANAGER_ENABLED
    g_uart_config[uart_port].baudrate = baudrate;
#endif
    g_baudrate[uart_port] = baudrate;
    hal_nvic_restore_interrupt_mask(irq_status);

    uartx = g_uart_regbase[uart_port];
    actual_baudrate = g_uart_baudrate_map[baudrate];

    uart_set_baudrate(uartx, actual_baudrate);

    return HAL_UART_STATUS_OK;
}

hal_uart_status_t hal_uart_set_format(hal_uart_port_t uart_port,
                                      const hal_uart_config_t *config)
{
    uint32_t irq_status;
    UART_REGISTER_T *uartx;

    if ((!uart_port_is_valid(uart_port)) ||
        (!uart_config_is_valid(config))) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }

    if (g_uart_hwstatus[uart_port] == UART_HWSTATUS_UNINITIALIZED) {
        return HAL_UART_STATUS_ERROR_UNINITIALIZED;
    }

    hal_nvic_save_and_set_interrupt_mask(&irq_status);
#ifdef HAL_SLEEP_MANAGER_ENABLED
    g_uart_config[uart_port].baudrate = config->baudrate;
    g_uart_config[uart_port].word_length = config->word_length;
    g_uart_config[uart_port].stop_bit = config->stop_bit;
    g_uart_config[uart_port].parity = config->parity;
#endif
    hal_nvic_restore_interrupt_mask(irq_status);

    uartx = g_uart_regbase[uart_port];

    hal_uart_set_baudrate(uart_port, config->baudrate);
    uart_set_format(uartx, config->word_length, config->stop_bit, config->parity);

    return HAL_UART_STATUS_OK;
}

hal_uart_status_t hal_uart_init(hal_uart_port_t uart_port, hal_uart_config_t *uart_config)
{
    UART_REGISTER_T *uartx;
    uint32_t i, actual_baudrate, irq_status;
    hal_clock_status_t status;

    if ((!uart_port_is_valid(uart_port)) ||
        (!uart_config_is_valid(uart_config))) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }

    hal_nvic_save_and_set_interrupt_mask(&irq_status);
    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_UNINITIALIZED) {
        hal_nvic_restore_interrupt_mask(irq_status);
        return HAL_UART_STATUS_ERROR_BUSY;
    }

    if (g_uart_global_data_initialized == false) {
        for (i = 0; i < HAL_UART_MAX; i++) {
            g_uart_hwstatus[i] = UART_HWSTATUS_UNINITIALIZED;
#ifdef HAL_SLEEP_MANAGER_ENABLED
            g_uart_flowcontrol_status[i] = UART_FLOWCONTROL_NONE;
            g_uart_frist_send_complete_interrupt[i] = false;
            g_uart_send_lock_status[i] = false;
#endif
            g_uart_callback[i].arg = NULL;
            g_uart_callback[i].func = NULL;
        }
        g_uart_global_data_initialized = true;
    }
#ifdef HAL_SLEEP_MANAGER_ENABLED
    g_uart_config[uart_port].baudrate = uart_config->baudrate;
    g_uart_config[uart_port].word_length = uart_config->word_length;
    g_uart_config[uart_port].stop_bit = uart_config->stop_bit;
    g_uart_config[uart_port].parity = uart_config->parity;
#endif
    g_uart_hwstatus[uart_port] = UART_HWSTATUS_POLL_INITIALIZED;
    hal_nvic_restore_interrupt_mask(irq_status);

    uint32_t share_buffer_dll_dlm;
    share_buffer_dll_dlm = *(volatile uint32_t *)(HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_START+4);
    if (share_buffer_dll_dlm == 0) {
        //uart_init_for_logging() not be called
        *(volatile uint32_t *)(HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_START) = HAL_UART_MAX;
    }

    status = hal_clock_enable(g_uart_port_to_pdn[uart_port]);
    if (status != HAL_CLOCK_STATUS_OK) {
        UART_ASSERT();
        return HAL_UART_STATUS_ERROR;
    }

    uartx = g_uart_regbase[uart_port];

    uart_reset_default_value(uartx);
    actual_baudrate = g_uart_baudrate_map[uart_config->baudrate];
    g_baudrate[uart_port] = uart_config->baudrate;
    uart_set_baudrate(uartx, actual_baudrate);
    uart_set_format(uartx, uart_config->word_length, uart_config->stop_bit, uart_config->parity);
    uart_set_fifo(uartx);
#ifdef HAL_SLEEP_MANAGER_ENABLED
    uart_set_sleep_mode(uartx);
    sleep_management_register_suspend_callback(SLEEP_BACKUP_RESTORE_UART, (sleep_management_suspend_callback_t)uart_backup_all_registers, NULL);
    sleep_management_register_resume_callback(SLEEP_BACKUP_RESTORE_UART, (sleep_management_resume_callback_t)uart_restore_all_registers, NULL);
#endif

    return HAL_UART_STATUS_OK;
}

hal_uart_status_t hal_uart_deinit(hal_uart_port_t uart_port)
{
    UART_REGISTER_T *uartx;
    VDMA_REGISTER_T *dmax;
    vdma_channel_t tx_dma_channel, rx_dma_channel;
    uint32_t irq_status;
    vdma_status_t status;
    hal_clock_status_t clock_status;

    if (!uart_port_is_valid(uart_port)) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }

    if (g_uart_hwstatus[uart_port] == UART_HWSTATUS_UNINITIALIZED) {
        return HAL_UART_STATUS_ERROR_UNINITIALIZED;
    }

    uartx = g_uart_regbase[uart_port];
    /* wait all left data sent out before deinit. */
    uart_query_empty(uartx);

    /* unregister vdma module */
    if (g_uart_hwstatus[uart_port] == UART_HWSTATUS_DMA_INITIALIZED) {
        tx_dma_channel = uart_port_to_dma_channel(uart_port, 0);
        rx_dma_channel = uart_port_to_dma_channel(uart_port, 1);

        status = vdma_disable_interrupt(tx_dma_channel);
        if (status != VDMA_OK) {
            UART_ASSERT();
            return HAL_UART_STATUS_ERROR;
        }
        status = vdma_disable_interrupt(rx_dma_channel);
        if (status != VDMA_OK) {
            UART_ASSERT();
            return HAL_UART_STATUS_ERROR;
        }

        status = vdma_stop(tx_dma_channel);
        if (status != VDMA_OK) {
            UART_ASSERT();
            return HAL_UART_STATUS_ERROR;
        }
        status = vdma_stop(rx_dma_channel);
        if (status != VDMA_OK) {
            UART_ASSERT();
            return HAL_UART_STATUS_ERROR;
        }

        //ack pending vdma irq
        dmax = (VDMA_REGISTER_T*)g_uart_dma_base[uart_port];
        dmax->VDMA_ACKINT = 0x8000;

        status = vdma_deinit(tx_dma_channel);
        if (status != VDMA_OK) {
            UART_ASSERT();
            return HAL_UART_STATUS_ERROR;
        }
        status = vdma_deinit(rx_dma_channel);
        if (status != VDMA_OK) {
            UART_ASSERT();
            return HAL_UART_STATUS_ERROR;
        }
        hal_nvic_disable_irq(g_uart_port_to_irq_num[uart_port]);
    }
    uart_reset_default_value(uartx);

    //clr pending uart irq
    uart_query_interrupt_type(uartx);

    clock_status = hal_clock_disable(g_uart_port_to_pdn[uart_port]);
    if (clock_status != HAL_CLOCK_STATUS_OK) {
        UART_ASSERT();
        return HAL_UART_STATUS_ERROR;
    }

    //g_uart_callback[uart_port].func = NULL;
    //g_uart_callback[uart_port].arg = NULL;

    hal_nvic_save_and_set_interrupt_mask(&irq_status);

#ifdef HAL_SLEEP_MANAGER_ENABLED
    g_uart_frist_send_complete_interrupt[uart_port] = false;
    g_uart_flowcontrol_status[uart_port] = UART_FLOWCONTROL_NONE;
#endif
    g_uart_hwstatus[uart_port] = UART_HWSTATUS_UNINITIALIZED;

    hal_nvic_restore_interrupt_mask(irq_status);

    return HAL_UART_STATUS_OK;
}

void hal_uart_put_char(hal_uart_port_t uart_port, char byte)
{
    UART_REGISTER_T *uartx;

    if (!uart_port_is_valid(uart_port)) {
        return;
    }

    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_POLL_INITIALIZED) {
        return;
    }

    uartx = g_uart_regbase[uart_port];
    uart_put_char_block(uartx, byte);
}

uint32_t hal_uart_send_polling(hal_uart_port_t uart_port, const uint8_t *data, uint32_t size)
{
    uint32_t i = 0;

    if ((!uart_port_is_valid(uart_port)) || (data == NULL)) {
        return 0;
    }

    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_POLL_INITIALIZED) {
        return 0;
    }

    for (i = 0; i < size; i++) {
        hal_uart_put_char(uart_port, *data);
        data++;
    }

    return size;
}

uint32_t hal_uart_send_dma(hal_uart_port_t uart_port, const uint8_t *data, uint32_t size)
{
    vdma_channel_t channel;
    uint32_t real_count, avail_space;
    vdma_status_t status;
#ifdef HAL_SLEEP_MANAGER_ENABLED
    uint32_t irq_status;
#endif

    if ((!uart_port_is_valid(uart_port)) || (data == NULL) || (size == 0)) {
        return 0;
    }

    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_DMA_INITIALIZED) {
        return 0;
    }

#ifdef HAL_SLEEP_MANAGER_ENABLED
    hal_nvic_save_and_set_interrupt_mask(&irq_status);
    if (g_uart_send_lock_status[uart_port] == false) {
        hal_sleep_manager_lock_sleep(uart_sleep_handle[uart_port]);
        g_uart_send_lock_status[uart_port] = true;
    }
    hal_nvic_restore_interrupt_mask(irq_status);
#endif

    channel = uart_port_to_dma_channel(uart_port, 0);

    status = vdma_get_available_send_space(channel, &avail_space);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return 0;
    }

    if (avail_space >= size) {
        real_count = size;
    } else {
        real_count = avail_space;
    }

    /* Update to multiple byte push operation */
    status = vdma_push_data_multi_bytes(channel, (uint8_t *)data, real_count);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return 0;
    }

    /* If avail space is not enough, turn on TX IRQ
       * so that UART driver can notice user when user's data has been sent out.
       */
    if (real_count == avail_space) {
        status = vdma_enable_interrupt(channel);
        if (status != VDMA_OK) {
            UART_ASSERT();
            return 0;
        }
    }

    return real_count;
}

char hal_uart_get_char(hal_uart_port_t uart_port)
{
    char data;
    UART_REGISTER_T *uartx;

    if (!uart_port_is_valid(uart_port)) {
        return 0;
    }

    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_POLL_INITIALIZED) {
        return 0;
    }

    uartx = g_uart_regbase[uart_port];

    data = uart_get_char_block(uartx);

    return data;
}

uint32_t hal_uart_get_char_unblocking(hal_uart_port_t uart_port)
{
    uint32_t data;
    UART_REGISTER_T *uartx;

    if (!uart_port_is_valid(uart_port)) {
        return 0;
    }

    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_POLL_INITIALIZED) {
        return 0;
    }

    uartx = g_uart_regbase[uart_port];

    data = uart_get_char_unblocking(uartx);

    return data;
}

uint32_t hal_uart_receive_polling(hal_uart_port_t uart_port, uint8_t *buffer, uint32_t size)
{
    uint32_t i;
    uint8_t *pbuf = buffer;

    if ((!uart_port_is_valid(uart_port)) ||
        (buffer == NULL)) {
        return 0;
    }

    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_POLL_INITIALIZED) {
        return 0;
    }

    for (i = 0; i < size; i++) {
        pbuf[i] = hal_uart_get_char(uart_port);
    }

    return size;
}

uint32_t hal_uart_receive_dma(hal_uart_port_t uart_port, uint8_t *buffer, uint32_t size)
{
    vdma_channel_t channel;
    uint32_t receive_count, avail_count;
    vdma_status_t status;
    UART_REGISTER_T *uartx = NULL;

    if ((!uart_port_is_valid(uart_port)) ||
        (buffer == NULL) ||
        (size == 0)) {
        return 0;
    }

    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_DMA_INITIALIZED) {
        return 0;
    }

    channel = uart_port_to_dma_channel(uart_port, 1);

    status = vdma_get_available_receive_bytes(channel, &avail_count);
    if (status != VDMA_OK) {
        g_debug_status = status;
        UART_ASSERT();
        return 0;
    }

    if (avail_count < size) {
        receive_count = avail_count;
    } else {
        receive_count = size;
    }

    status = vdma_pop_data_multi_bytes(channel, buffer, receive_count);
    if (status != VDMA_OK) {
        g_debug_status = status;
        g_debug_receive_count = receive_count;
        g_debug_avail_count = avail_count;

        UART_ASSERT();
        return 0;
    }

    /*enable uart rx irq(include:rx timeout irq)*/
    uartx = g_uart_regbase[uart_port];
    uart_unmask_receive_interrupt(uartx);

    /* If avail bytes is not enough, turn on RX IRQ
       * so that UART driver can notice user when new user's data has been received.
       */
    if (receive_count == avail_count) {
        status = vdma_enable_interrupt(channel);
        if (status != VDMA_OK) {
            UART_ASSERT();
            return 0;
        }
    }

    return receive_count;
}

static void uart_start_dma_transmission(hal_uart_port_t uart_port)
{
    UART_REGISTER_T *uartx;
    vdma_channel_t tx_dma_channel, rx_dma_channel;
    vdma_status_t status;

    uartx = g_uart_regbase[uart_port];
    tx_dma_channel = uart_port_to_dma_channel(uart_port, 0);
    rx_dma_channel = uart_port_to_dma_channel(uart_port, 1);

#ifdef HAL_SLEEP_MANAGER_ENABLED
    if(g_uart_port_for_logging != uart_port) {
        uart_unmask_send_interrupt(uartx);
    }

    g_uart_frist_send_complete_interrupt[uart_port] = true;
#endif
    uart_unmask_receive_interrupt(uartx);
    hal_nvic_enable_irq(g_uart_port_to_irq_num[uart_port]);
    if (uart_port != g_uart_port_for_logging) {
        status = vdma_enable_interrupt(tx_dma_channel);
        if (status != VDMA_OK) {
            UART_ASSERT();
            return;
        }
    }
    status = vdma_enable_interrupt(rx_dma_channel);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return;
    }
    status = vdma_start(tx_dma_channel);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return;
    }
    status = vdma_start(rx_dma_channel);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return;
    }
    uart_enable_dma(uartx);
    uart_set_fifo(uartx);
}

hal_uart_status_t hal_uart_register_callback(hal_uart_port_t uart_port,
        hal_uart_callback_t user_callback,
        void *user_data)
{
    vdma_channel_t tx_dma_channel, rx_dma_channel;
    uint32_t irq_status;
    vdma_status_t status;
    hal_nvic_status_t nvic_status;

    if ((!uart_port_is_valid(uart_port)) ||
        (user_callback == NULL)) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }

    hal_nvic_save_and_set_interrupt_mask(&irq_status);
    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_POLL_INITIALIZED) {
        hal_nvic_restore_interrupt_mask(irq_status);
        return HAL_UART_STATUS_ERROR_UNINITIALIZED;
    }

    tx_dma_channel = uart_port_to_dma_channel(uart_port, 0);
    rx_dma_channel = uart_port_to_dma_channel(uart_port, 1);

    g_uart_callback[uart_port].func = user_callback;
    g_uart_callback[uart_port].arg = user_data;

    uart_dma_channel_to_callback_data(tx_dma_channel, &g_uart_dma_callback_data[uart_port * 2]);
    status = vdma_register_callback(tx_dma_channel, uart_dma_callback_handler, &g_uart_dma_callback_data[uart_port * 2]);
    if (status != VDMA_OK) {
        hal_nvic_restore_interrupt_mask(irq_status);
        UART_ASSERT();
        return HAL_UART_STATUS_ERROR;
    }
    uart_dma_channel_to_callback_data(rx_dma_channel, &g_uart_dma_callback_data[(uart_port * 2) + 1]);
    status = vdma_register_callback(rx_dma_channel, uart_dma_callback_handler, &g_uart_dma_callback_data[(uart_port * 2) + 1]);
    if (status != VDMA_OK) {
        hal_nvic_restore_interrupt_mask(irq_status);
        UART_ASSERT();
        return HAL_UART_STATUS_ERROR;
    }
    nvic_status = hal_nvic_register_isr_handler(g_uart_port_to_irq_num[uart_port], uart_interrupt_handler);
    if (nvic_status != HAL_NVIC_STATUS_OK) {
        hal_nvic_restore_interrupt_mask(irq_status);
        UART_ASSERT();
        return HAL_UART_STATUS_ERROR;
    }

    uart_start_dma_transmission(uart_port);

    g_uart_hwstatus[uart_port] = UART_HWSTATUS_DMA_INITIALIZED;

    hal_nvic_restore_interrupt_mask(irq_status);

    return HAL_UART_STATUS_OK;
}

uint32_t hal_uart_get_available_send_space(hal_uart_port_t uart_port)
{
    vdma_channel_t channel;
    uint32_t roomleft;
    vdma_status_t status;

    if (!uart_port_is_valid(uart_port)) {
        return 0;
    }

    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_DMA_INITIALIZED) {
        return 0;
    }

    channel = uart_port_to_dma_channel(uart_port, 0);
    status = vdma_get_available_send_space(channel, &roomleft);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return 0;
    }

    return roomleft;
}

uint32_t hal_uart_get_available_receive_bytes(hal_uart_port_t uart_port)
{
    vdma_channel_t channel;
    uint32_t avail;
    vdma_status_t status;

    if (!uart_port_is_valid(uart_port)) {
        return 0;
    }

    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_DMA_INITIALIZED) {
        return 0;
    }

    channel = uart_port_to_dma_channel(uart_port, 1);
    status = vdma_get_available_receive_bytes(channel, &avail);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return 0;
    }

    return avail;
}

hal_uart_status_t hal_uart_set_hardware_flowcontrol(hal_uart_port_t uart_port)
{
    UART_REGISTER_T *uartx;

    if (!uart_port_is_valid(uart_port)) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }

    if (g_uart_hwstatus[uart_port] == UART_HWSTATUS_UNINITIALIZED) {
        return HAL_UART_STATUS_ERROR_UNINITIALIZED;
    }

    uartx = g_uart_regbase[uart_port];

    uart_set_hardware_flowcontrol(uartx);

#ifdef HAL_SLEEP_MANAGER_ENABLED
    g_uart_flowcontrol_status[uart_port] = UART_FLOWCONTROL_HARDWARE;
#endif

    return HAL_UART_STATUS_OK;
}

hal_uart_status_t hal_uart_set_software_flowcontrol(hal_uart_port_t uart_port,
        uint8_t xon,
        uint8_t xoff,
        uint8_t escape_character)
{
    UART_REGISTER_T *uartx;

    if (!uart_port_is_valid(uart_port)) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }

    if (g_uart_hwstatus[uart_port] == UART_HWSTATUS_UNINITIALIZED) {
        return HAL_UART_STATUS_ERROR_UNINITIALIZED;
    }

    uartx = g_uart_regbase[uart_port];

    uart_set_software_flowcontrol(uartx, xon, xoff, escape_character);

#ifdef HAL_SLEEP_MANAGER_ENABLED
    g_uart_flowcontrol_status[uart_port] = UART_FLOWCONTROL_SOFTWARE;
    g_uart_sw_flowcontrol_config[uart_port].xon = xon;
    g_uart_sw_flowcontrol_config[uart_port].xoff = xoff;
    g_uart_sw_flowcontrol_config[uart_port].escape_character = escape_character;
#endif

    return HAL_UART_STATUS_OK;
}

hal_uart_status_t hal_uart_disable_flowcontrol(hal_uart_port_t uart_port)
{
    UART_REGISTER_T *uartx;

    if (!uart_port_is_valid(uart_port)) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }

    if (g_uart_hwstatus[uart_port] == UART_HWSTATUS_UNINITIALIZED) {
        return HAL_UART_STATUS_ERROR_UNINITIALIZED;
    }

    uartx = g_uart_regbase[uart_port];

    uart_disable_flowcontrol(uartx);

#ifdef HAL_SLEEP_MANAGER_ENABLED
    g_uart_flowcontrol_status[uart_port] = UART_FLOWCONTROL_NONE;
#endif

    return HAL_UART_STATUS_OK;
}


hal_uart_status_t hal_uart_set_dma(hal_uart_port_t uart_port, const hal_uart_dma_config_t *dma_config)
{
    uint32_t irq_status;
    vdma_config_t internal_dma_config;
    vdma_channel_t tx_dma_channel, rx_dma_channel;
    vdma_status_t status;

    if (!uart_port_is_valid(uart_port)) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }
    if ((dma_config->send_vfifo_buffer == NULL) ||
        (dma_config->receive_vfifo_buffer == NULL)) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }
    if ((dma_config->send_vfifo_buffer_size >= UART_DMA_MAX_SETTING_VALUE) ||
        (dma_config->send_vfifo_threshold_size >= UART_DMA_MAX_SETTING_VALUE) ||
        (dma_config->send_vfifo_threshold_size > dma_config->send_vfifo_buffer_size)) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }
    if ((dma_config->receive_vfifo_buffer_size >= UART_DMA_MAX_SETTING_VALUE) ||
        (dma_config->receive_vfifo_threshold_size >= UART_DMA_MAX_SETTING_VALUE) ||
        (dma_config->receive_vfifo_alert_size >= UART_DMA_MAX_SETTING_VALUE) ||
        (dma_config->receive_vfifo_threshold_size > dma_config->receive_vfifo_buffer_size) ||
        (dma_config->receive_vfifo_alert_size > dma_config->receive_vfifo_buffer_size)) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }

    if (g_uart_hwstatus[uart_port] != UART_HWSTATUS_POLL_INITIALIZED) {
        return HAL_UART_STATUS_ERROR_UNINITIALIZED;
    }

    tx_dma_channel = uart_port_to_dma_channel(uart_port, 0);
    rx_dma_channel = uart_port_to_dma_channel(uart_port, 1);

    status = vdma_init(tx_dma_channel);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return HAL_UART_STATUS_ERROR;
    }
    internal_dma_config.base_address = (uint32_t)dma_config->send_vfifo_buffer;
    internal_dma_config.size = dma_config->send_vfifo_buffer_size;
    status = vdma_configure(tx_dma_channel, &internal_dma_config);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return HAL_UART_STATUS_ERROR;
    }
    status = vdma_set_threshold(tx_dma_channel, dma_config->send_vfifo_threshold_size);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return HAL_UART_STATUS_ERROR;
    }

    status = vdma_init(rx_dma_channel);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return HAL_UART_STATUS_ERROR;
    }
    internal_dma_config.base_address = (uint32_t)dma_config->receive_vfifo_buffer;
    internal_dma_config.size = dma_config->receive_vfifo_buffer_size;
    status = vdma_configure(rx_dma_channel, &internal_dma_config);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return HAL_UART_STATUS_ERROR;
    }
    status = vdma_set_threshold(rx_dma_channel, dma_config->receive_vfifo_threshold_size);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return HAL_UART_STATUS_ERROR;
    }
    status = vdma_set_alert_length(rx_dma_channel, dma_config->receive_vfifo_alert_size);
    if (status != VDMA_OK) {
        UART_ASSERT();
        return HAL_UART_STATUS_ERROR;
    }

    hal_nvic_save_and_set_interrupt_mask(&irq_status);
    g_uart_dma_config[uart_port].send_vfifo_buffer = dma_config->send_vfifo_buffer;
    g_uart_dma_config[uart_port].send_vfifo_buffer_size = dma_config->send_vfifo_buffer_size;
    g_uart_dma_config[uart_port].send_vfifo_threshold_size = dma_config->send_vfifo_threshold_size;
    g_uart_dma_config[uart_port].receive_vfifo_alert_size = dma_config->receive_vfifo_alert_size;
    g_uart_dma_config[uart_port].receive_vfifo_buffer = dma_config->receive_vfifo_buffer;
    g_uart_dma_config[uart_port].receive_vfifo_buffer_size = dma_config->receive_vfifo_buffer_size;
    g_uart_dma_config[uart_port].receive_vfifo_threshold_size = dma_config->receive_vfifo_threshold_size;
    hal_nvic_restore_interrupt_mask(irq_status);

    return HAL_UART_STATUS_OK;
}



#ifdef HAL_UART_FEATURE_VFIFO_DMA_TIMEOUT
hal_uart_status_t hal_uart_set_dma_timeout(hal_uart_port_t uart_port, uint32_t timeout)
{
    UART_REGISTER_T *uartx;

    if (!uart_port_is_valid(uart_port)) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }
    if (timeout > HAL_UART_TIMEOUT_VALUE_MAX) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }

    uartx = g_uart_regbase[uart_port];
    uart_set_timeout_value(uartx, timeout);

    return HAL_UART_STATUS_OK;
}
#endif

hal_uart_status_t hal_uart_set_auto_baudrate(hal_uart_port_t uart_port, bool is_enable)
{
    UART_REGISTER_T *uartx;
    uint32_t current_baudrate;

    if (!uart_port_is_valid(uart_port)) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }

    uartx = g_uart_regbase[uart_port];
    current_baudrate = g_uart_baudrate_map[g_baudrate[uart_port]];

    uart_set_auto_baudrate(uartx, is_enable, current_baudrate);
    return HAL_UART_STATUS_OK;
}

/*for logging*/
hal_uart_status_t uart_init_for_logging(hal_uart_port_t uart_port,
                                        hal_uart_config_t *uart_config,
                                        const hal_uart_dma_config_t *dma_config,
                                        hal_uart_callback_t user_callback,
                                        void *user_data)
{
    UART_REGISTER_T *uartx;
    uint32_t *share_buffer;
    uint32_t *share_buffer_end;
    share_buffer = (uint32_t *)HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_START;
    share_buffer_end = (uint32_t *)(HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_START + HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_LEN - 4);
    uartx = g_uart_regbase[uart_port];
    g_uart_port_for_logging = uart_port;

    hal_uart_init(uart_port, uart_config);
    hal_uart_set_dma(uart_port, dma_config);
    hal_uart_register_callback(uart_port, user_callback, user_data);


    *share_buffer++ = g_uart_port_for_logging;
    *share_buffer++ = uartx->DLM_DLL;
    //g_uart_registers[uart_port].IER_UNION.IER = uartx->IER_UNION.IER;
    *share_buffer++ = uartx->FCR_UNION.FCR;
    //g_uart_registers[uart_port].EFR_UNION.EFR = uartx->EFR_UNION.EFR;
    *share_buffer++ = uartx->LCR_UNION.LCR;
    //g_uart_registers[uart_port].XON_XOFF_UNION.XON_XOFF = uartx->XON_XOFF_UNION.XON_XOFF;
    *share_buffer++ = uartx->HIGHSPEED;
    *share_buffer++ = uartx->SAMPLE_REG_UNION.SAMPLE_REG;
    *share_buffer++ = uartx->RATEFIX_UNION.RATEFIX;
    *share_buffer++ = uartx->GUARD;
    *share_buffer++ = uartx->SLEEP_REG;
    *share_buffer++ = uartx->FRACDIV;
    *share_buffer++ = uartx->DMA_CON_UNION.DMA_CON;
    //g_uart_registers[uart_port].RXTRIG = uartx->RXTRIG;
    //g_uart_registers[uart_port].RX_TO_CON_UNION.RX_TO_CON = uartx->RX_TO_CON_UNION.RX_TO_CON;
    //g_uart_registers[uart_port].RX_TOC_DEST = uartx->RX_TOC_DEST;

    //DMA backup
    *share_buffer++ = *(volatile uint32_t *)(g_uart_dma_base[uart_port]); //VDMA_COUNT
    *share_buffer++ = *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x4); //VDMA_CON
    *share_buffer++ = *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x1C); //VDMA_PGMADDR
    *share_buffer++ = *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x30); //VDMA_ALTLEN
    *share_buffer++ = *(volatile uint32_t *)(g_uart_dma_base[uart_port]+0x34); //VDMA_FFSIZE

    if (share_buffer > share_buffer_end) {
        UART_ASSERT();
    } else {
        *share_buffer_end = g_uart_baudrate_map[uart_config->baudrate];
    }

    return HAL_UART_STATUS_OK;
}

uint32_t uart_get_hw_rptr(hal_uart_port_t uart_port)
{
    vdma_channel_t dma_channel;
    uint32_t read_pointer, *share_buffer;

    share_buffer = (uint32_t *)HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_START;
    dma_channel = uart_port_to_dma_channel(uart_port, 0);
    vdma_get_hw_read_point(dma_channel, &read_pointer);
    return (read_pointer - share_buffer[13]);
}

hal_uart_status_t uart_set_sw_move_byte(hal_uart_port_t uart_port, uint16_t sw_move_byte)
{
    vdma_status_t status;
    vdma_channel_t dma_channel;
    dma_channel = uart_port_to_dma_channel(uart_port, 0);
    status = vdma_set_sw_move_byte(dma_channel, sw_move_byte);
    if (status != VDMA_OK) {
        return HAL_UART_STATUS_ERROR_PARAMETER;
    }
    return HAL_UART_STATUS_OK;
}

uint32_t uart_get_hw_wptr(hal_uart_port_t uart_port)
{
    vdma_channel_t dma_channel;
    uint32_t write_pointer, *share_buffer;

    share_buffer = (uint32_t *)HW_SYSRAM_PRIVATE_MEMORY_SYSLOG_UART_VAR_START;
    dma_channel = uart_port_to_dma_channel(uart_port, 0);
    vdma_get_hw_write_point(dma_channel, &write_pointer);
    return (write_pointer - share_buffer[13]);
}

uint32_t uart_send_polling(hal_uart_port_t uart_port, const uint8_t *data, uint32_t size)
{
    uint32_t i = 0;
    UART_REGISTER_T *uartx;

    uartx = g_uart_regbase[uart_port];

    for (i = 0; i < size; i++) {
        uart_put_char_block(uartx, *data);
        data++;
    }

    return size;
}

/*other module used this api*/
uint32_t uart_exception_send_polling(hal_uart_port_t uart_port, const uint8_t *data, uint32_t size)
{
    uint32_t i, j;
    uint32_t loop_count, remainder, curr_word;
    uint8_t *p_word;
    uint32_t *p_data;
    UART_REGISTER_T *uartx;

    uartx = g_uart_regbase[uart_port];

    loop_count = size / 4;
    remainder = size % 4;

    /* DSP IRAM must be fetched with WORD unit. */
    p_data = (uint32_t *)data;
    for (i=0; i<loop_count; i++) {
        curr_word = p_data[i];
        p_word = (uint8_t *)&curr_word;
        for (j = 0; j < 4; j++) {
            uart_put_char_block(uartx, p_word[j]);
        }
    }
    if (remainder) {
        curr_word = p_data[i];
        p_word = (uint8_t *)&curr_word;
        for (j = 0; j < remainder; j++) {
            uart_put_char_block(uartx, p_word[j]);
        }
    }

    return size;
}

/*Onewire used this api*/
void uart_wait_empty(hal_uart_port_t uart_port)
{
    UART_REGISTER_T *uartx;

    uartx = g_uart_regbase[uart_port];

    /* wait all left data sent out before deinit. */
    uart_query_empty(uartx);
}


/*For Crystal Trim using in at command*/
hal_uart_status_t hal_uart_ext_get_uart_config(hal_uart_port_t uart_port, hal_uart_config_t  *config)
{
    if(uart_port >= HAL_UART_MAX || config == NULL){
        return HAL_UART_STATUS_ERROR;
    }

    if(g_uart_hwstatus[uart_port] == UART_HWSTATUS_UNINITIALIZED ) {
        return HAL_UART_STATUS_ERROR_UNINITIALIZED;
    }
#ifdef HAL_SLEEP_MANAGER_ENABLED
    *config = g_uart_config[uart_port];
#endif
    return HAL_UART_STATUS_OK;
}

bool hal_uart_ext_is_dma_mode(hal_uart_port_t uart_port)
{
    if(uart_port >= HAL_UART_MAX){
        return false;
    }
    if (g_uart_hwstatus[uart_port] & UART_HWSTATUS_DMA_INITIALIZED) {
        return true;
    }
    return false;
}

hal_uart_status_t  hal_uart_ext_set_baudrate(hal_uart_port_t uart_port, uint32_t baudrate)
{
    UART_REGISTER_T *uartx = NULL;

    if(uart_port >= HAL_UART_MAX){
        return HAL_UART_STATUS_ERROR;
    }
    uartx = g_uart_regbase[uart_port];
    /*Set a signal frequency,Baud rate is twice the frequency*/
    uart_set_baudrate(uartx, baudrate * 2);
    return HAL_UART_STATUS_OK;
}


#ifdef __cplusplus
}
#endif

#endif

