/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_pwm.h"

#ifdef HAL_PWM_MODULE_ENABLED
#ifndef HAL_CLOCK_MODULE_ENABLED
#define HAL_CLOCK_MODULE_ENABLED
#endif

#include "hal_pwm_internal.h"
#include "hal_clock.h"
#include "hal_log.h"
#include "hal_clock.h"
#include "hal_clock_platform.h"
#include "hal_sleep_manager.h"
#include "hal_sleep_manager_internal.h"
#include "hal_sleep_manager_platform.h"

extern  uint8_t clock_set_pll_on(clock_pll_id pll_id);
extern  uint8_t clock_set_pll_off(clock_pll_id pll_id);

static  PWM_REGISTER_T  *pwm[PWM_NUMBER]     = {PWM0, PWM1, PWM2, PWM3, PWM4, PWM5, PWM6, PWM7, PWM8, PWM9};
static  hal_clock_cg_id  pwm_pdn[PWM_NUMBER] = {HAL_CLOCK_CG_PWM0, HAL_CLOCK_CG_PWM1, HAL_CLOCK_CG_PWM2, HAL_CLOCK_CG_PWM3, HAL_CLOCK_CG_PWM4,
                                               HAL_CLOCK_CG_PWM5, HAL_CLOCK_CG_PWM6, HAL_CLOCK_CG_PWM7, HAL_CLOCK_CG_PWM8, HAL_CLOCK_CG_PWM9};
static bool              pwm_run_status[PWM_NUMBER];
static volatile  uint8_t pwm_init_status[PWM_NUMBER];

#ifdef HAL_SLEEP_MANAGER_ENABLED
static PWM_REGISTER_T    pwm_backup_register[PWM_NUMBER];

static  void    pwm_enter_suspend()
{
    uint32_t    i=0;

    for (i=0; i<PWM_NUMBER; i++)
    {
        pwm_backup_register[i].PWM_CTRL       = pwm[i]->PWM_CTRL;
        pwm_backup_register[i].PWM_COUNT      = pwm[i]->PWM_COUNT;
        pwm_backup_register[i].PWM_THRESH     = pwm[i]->PWM_THRESH;
        pwm_backup_register[i].PWM_THRESH_DOWN= pwm[i]->PWM_THRESH_DOWN;
    }
}


static void     pwm_enter_resume()
{
    uint32_t    i=0;

    for (i=0; i<PWM_NUMBER; i++)
    {
        pwm[i]->PWM_COUNT        = pwm_backup_register[i].PWM_COUNT;
        pwm[i]->PWM_THRESH       = pwm_backup_register[i].PWM_THRESH;
        pwm[i]->PWM_THRESH_DOWN  = pwm_backup_register[i].PWM_THRESH_DOWN;
        pwm[i]->PWM_CTRL         = pwm_backup_register[i].PWM_CTRL;
    }
}
#endif




hal_pwm_status_t    hal_pwm_init(hal_pwm_channel_t pwm_channel, hal_pwm_source_clock_t source_clock)
{
    hal_pwm_status_t busy_status;

    /*check parameter*/
    if (pwm_channel  >= HAL_PWM_MAX_CHANNEL) {
        log_hal_msgid_error("channel is invalid!\r\n", 0);
        return HAL_PWM_STATUS_ERROR_CHANNEL;
    }

    if (source_clock >= HAL_PWM_CLOCK_MAX) {
        log_hal_msgid_error("source clk is invalid!\r\n", 0);
        return HAL_PWM_STATUS_INVALID_PARAMETER;
    }

    PWM_CHECK_AND_SET_BUSY(pwm_channel,busy_status);
    if (HAL_PWM_STATUS_ERROR == busy_status) {
        log_hal_msgid_error("channel is busy!\r\n", 0);
        return HAL_PWM_STATUS_ERROR;
    }

    /*initial pwm register*/
    pwm[pwm_channel]->PWM_CTRL   = 0;
    pwm[pwm_channel]->PWM_COUNT  = 0;
    pwm[pwm_channel]->PWM_THRESH = 0;
    pwm[pwm_channel]->PWM_THRESH_DOWN = 0;
    pwm[pwm_channel]->PWM_CTRL |= ((source_clock & 0x03)<<PWM_CLK_SEL_OFFSET);
#ifdef HAL_SLEEP_MANAGER_ENABLED
    sleep_management_register_suspend_callback(SLEEP_BACKUP_RESTORE_PWM, pwm_enter_suspend, NULL);
    sleep_management_register_resume_callback(SLEEP_BACKUP_RESTORE_PWM,  pwm_enter_resume,  NULL);
#endif
    return HAL_PWM_STATUS_OK;
}


hal_pwm_status_t    hal_pwm_deinit(hal_pwm_channel_t pwm_channel)
{
    /*check parameter*/
    if (pwm_channel >= HAL_PWM_MAX_CHANNEL) {
        log_hal_msgid_error("channel is invalid!\r\n", 0);
        return HAL_PWM_STATUS_ERROR_CHANNEL;
    }
    if (PWM_CHECK_STATUS(pwm_channel) == PWM_INIT) {
        hal_pwm_stop(pwm_channel);
        PWM_SET_IDLE(pwm_channel);
        return HAL_PWM_STATUS_OK;
    } else {
        log_hal_msgid_error("channel is deinited!\r\n", 0);
        return HAL_PWM_STATUS_ERROR;
    }
}


hal_pwm_status_t    hal_pwm_set_frequency(hal_pwm_channel_t pwm_channel, uint32_t frequency, uint32_t *total_count)
{
    uint32_t    clock_div = 0;
    uint32_t    clock_src = 0;
    uint32_t    clock_pwm = 0;
    uint32_t    control   = 0;
    uint32_t    min_frequency = 0;
    uint32_t    temp      = 0;

    /*Check parameter*/
    if (pwm_channel >= HAL_PWM_MAX_CHANNEL) {
        log_hal_msgid_error("channel is invalid!\r\n", 0);
        return HAL_PWM_STATUS_ERROR_CHANNEL;
    }

    /*Get pwm source clock*/
    control   = pwm[pwm_channel]->PWM_CTRL;
    clock_div = 1<<(control & PWM_CLK_DIV_MASK);
    temp      = (control&PWM_CLK_SEL_MASK)>>PWM_CLK_SEL_OFFSET;
    switch(temp) {
        case HAL_PWM_CLOCK_13MHZ:     clock_src = PWM_CLOCK_SEL2; break;
        case HAL_PWM_CLOCK_13MHZ_OSC: clock_src = PWM_CLOCK_SEL2; break;
        case HAL_PWM_CLOCK_32KHZ:     clock_src = PWM_CLOCK_SEL1; break;
        case HAL_PWM_CLOCK_48MHZ:     clock_src = PWM_CLOCK_SEL3; break;
        default:
            log_hal_msgid_error("clock source invalid(%d)!\r\n",1, temp);
        break;
    }
    clock_pwm = clock_src/clock_div;
    /*Cal counter of current frequency*/
    min_frequency = clock_pwm/PWM_MAX_COUNT;
    if (frequency < min_frequency) {
    log_hal_msgid_error("parameter of frequency %dHz invalid(Min freq %dHz)!\r\n",2, frequency, min_frequency);
        return HAL_PWM_STATUS_INVALID_FREQUENCY;
    } else {
        temp = (clock_pwm/frequency);
    }

    if (temp > 0 ) {
        pwm[pwm_channel]->PWM_COUNT = temp-1;
       *total_count = temp;
        return HAL_PWM_STATUS_OK;
    } else {
        log_hal_msgid_error("parameter of frequency %dHz invalid(Max freq %dHz)!\r\n",2, frequency, clock_pwm);
        return HAL_PWM_STATUS_INVALID_FREQUENCY;
    }

}


hal_pwm_status_t    hal_pwm_set_duty_cycle(hal_pwm_channel_t pwm_channel, uint32_t duty_cycle)
{
    uint32_t    temp  = 0;

    if (pwm_channel >= HAL_PWM_MAX_CHANNEL) {
        log_hal_msgid_error("channel is invalid!\r\n", 0);
        return HAL_PWM_STATUS_ERROR_CHANNEL;
    }

    temp = pwm[pwm_channel]->PWM_COUNT;
    pwm[pwm_channel]->PWM_THRESH_DOWN = 0;

    if (0 == duty_cycle) {
        pwm[pwm_channel]->PWM_THRESH_DOWN = 1;
        pwm[pwm_channel]->PWM_THRESH      = 0;
    } else if (duty_cycle > temp) {

        pwm[pwm_channel]->PWM_THRESH = temp;
    } else {

        pwm[pwm_channel]->PWM_THRESH = duty_cycle-1;
    }

    return HAL_PWM_STATUS_OK;
}




hal_pwm_status_t    hal_pwm_start(hal_pwm_channel_t pwm_channel)
{
    uint32_t            save_mask;
    uint32_t            clock_src;
    uint32_t            control;

    hal_pwm_status_t result = HAL_PWM_STATUS_OK;

    if (pwm_channel >= HAL_PWM_MAX_CHANNEL) {
        log_hal_msgid_error("channel is invalid!\r\n", 0);
        return HAL_PWM_STATUS_ERROR_CHANNEL;
    }

    /*Disable interrupt*/
    hal_nvic_save_and_set_interrupt_mask(&save_mask);

    if (false == pwm_run_status[pwm_channel]) {
        pwm_run_status[pwm_channel] = true;

        if(HAL_CLOCK_STATUS_ERROR == hal_clock_enable(pwm_pdn[pwm_channel])){
            result = HAL_PWM_STATUS_ERROR;
        }

        /*If clock source for 48Mhz, then turn on UPLL/MPLL*/
        control   = pwm[pwm_channel]->PWM_CTRL;
        clock_src = (control&PWM_CLK_SEL_MASK)>>PWM_CLK_SEL_OFFSET;

        if (clock_src == HAL_PWM_CLOCK_48MHZ) {
            clock_set_pll_on(CLK_GPLL);
        }
    }
    /*Enable interrupt*/
    hal_nvic_restore_interrupt_mask(save_mask);

    return result;
}



hal_pwm_status_t    hal_pwm_stop(hal_pwm_channel_t pwm_channel)
{
    uint32_t    save_mask;
    hal_pwm_status_t    result = HAL_PWM_STATUS_OK;
    uint32_t            clock_src;
    uint32_t            control;

    if (pwm_channel >= HAL_PWM_MAX_CHANNEL) {
        log_hal_msgid_error("channel is invalid!\r\n", 0);
        return HAL_PWM_STATUS_ERROR_CHANNEL;
    }

    /*Disable interrupt*/
    hal_nvic_save_and_set_interrupt_mask(&save_mask);

    if (true == pwm_run_status[pwm_channel]) {
        pwm_run_status[pwm_channel] = false;
        if(HAL_CLOCK_STATUS_ERROR == hal_clock_disable(pwm_pdn[pwm_channel])) {
            result = HAL_PWM_STATUS_ERROR;
        }
        /*If clock source for 48Mhz, then turn off UPLL/MPLL*/
        control   = pwm[pwm_channel]->PWM_CTRL;
        clock_src = (control&PWM_CLK_SEL_MASK)>>PWM_CLK_SEL_OFFSET;

        if (clock_src == HAL_PWM_CLOCK_48MHZ) {
            clock_set_pll_off(CLK_GPLL);
        }
    }

    /*Enable interrupt*/
    hal_nvic_restore_interrupt_mask(save_mask);

    return result;

}


hal_pwm_status_t    hal_pwm_get_frequency(hal_pwm_channel_t pwm_channel, uint32_t *frequency)
{
    uint8_t    clock_div = 0;
    uint32_t   clock_src = 0;
    uint32_t   clock_pwm = 0;
    uint32_t   control   = 0;
    uint32_t   temp      = 0;

    /*Check parameter*/
    if (pwm_channel >= HAL_PWM_MAX_CHANNEL) {
        return HAL_PWM_STATUS_ERROR_CHANNEL;
    }

    /*Get clock division*/
    control   = pwm[pwm_channel]->PWM_CTRL;
    temp      = (control & PWM_CLK_DIV_MASK)>>PWM_CLK_DIV_OFFSET;
    switch(temp) {
        case 0: clock_div=1; break;
        case HAL_PWM_CLOCK_DIVISION_2: clock_div=2; break;
        case HAL_PWM_CLOCK_DIVISION_4: clock_div=4; break;
        case HAL_PWM_CLOCK_DIVISION_8: clock_div=8; break;
        default: clock_div=1;break;
    }

    /*Get clock source*/
    temp      = (control&PWM_CLK_SEL_MASK)>>PWM_CLK_SEL_OFFSET;
    switch(temp) {
        case HAL_PWM_CLOCK_13MHZ:     clock_src = PWM_CLOCK_SEL2; break;
        case HAL_PWM_CLOCK_13MHZ_OSC: clock_src = PWM_CLOCK_SEL2; break;
        case HAL_PWM_CLOCK_32KHZ:     clock_src = PWM_CLOCK_SEL1; break;
        case HAL_PWM_CLOCK_48MHZ:     clock_src = PWM_CLOCK_SEL3; break;
    }
    clock_pwm = clock_src/clock_div;

    //Cal frequency
    temp      = pwm[pwm_channel]->PWM_COUNT+1;
    *frequency= clock_pwm/temp;

    return HAL_PWM_STATUS_OK;
}

hal_pwm_status_t    hal_pwm_get_duty_cycle(hal_pwm_channel_t pwm_channel, uint32_t *duty_cycle)
{

    /*Check parameter*/
    if (pwm_channel >= HAL_PWM_MAX_CHANNEL) {
        return HAL_PWM_STATUS_ERROR_CHANNEL;
    }

    *duty_cycle = pwm[pwm_channel]->PWM_THRESH;

    return HAL_PWM_STATUS_OK;
}



hal_pwm_status_t    hal_pwm_get_running_status(hal_pwm_channel_t pwm_channel, hal_pwm_running_status_t *runing_status)
{
    if (pwm_channel >= HAL_PWM_MAX_CHANNEL) {
        return HAL_PWM_STATUS_INVALID_PARAMETER;
    }

    if (true == pwm_run_status[pwm_channel]) {
        *runing_status = HAL_PWM_BUSY;
    } else {
        *runing_status = HAL_PWM_IDLE;
    }
    return HAL_PWM_STATUS_OK;
}

hal_pwm_status_t    hal_pwm_set_advanced_config(hal_pwm_channel_t pwm_channel, hal_pwm_advanced_config_t advanced_config)
{

    if (pwm_channel >= HAL_PWM_MAX_CHANNEL) {
        return HAL_PWM_STATUS_INVALID_PARAMETER;
    }

    if (advanced_config >= HAL_PWM_CLOCK_DIVISION_MAX) {
        return HAL_PWM_STATUS_INVALID_PARAMETER;
    }
    pwm[pwm_channel]->PWM_CTRL &= ~(PWM_CLK_DIV_MASK);
    pwm[pwm_channel]->PWM_CTRL |= advanced_config;

    return HAL_PWM_STATUS_OK;
}

#endif

