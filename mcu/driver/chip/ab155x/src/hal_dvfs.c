/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_dvfs.h"

#ifdef HAL_DVFS_MODULE_ENABLED
#include <string.h>
#include "hal.h"
#include "hal_dvfs_internal.h"
#include "hal_clock.h"
#include "hal_clock_platform.h"
#include "hal_resource_assignment.h"
#include "hal_nvic_internal.h"
#include <assert.h>
#ifdef  HAL_DVFS_DEBUG_ENABLE
#define dvfs_debug(_message,...) log_hal_info("[DVFS] "_message, ##__VA_ARGS__)
#else
#define dvfs_debug(_message,...)
#endif

#define RESET_DVFS_LEVEL(index)      (DVFS_LOCK_STATUS ? domain->frequency[freq_index] : domain->frequency[domain->basic_opp_index])
#define DVFS_LOCK_STATUS             *((uint32_t*)(&frequency_Ctrl[0]))
extern int frequency_Ctrl[DVFS_MAX_SPEED];
static dvfs_opp_t *domain;
const uint32_t *cpu_freq_list;
uint32_t list_num;
unsigned int dvfs_lock_status=0;
unsigned int dvfs_lock_alone_status=0;
extern uint32_t clock_get_pll_state(clock_pll_id pll_id);
const char *lock_handle;
char *lock_name;
uint8_t dvfs_get_current_state(){
    return domain->cur_opp_index;
}

void dvfs_upload_date_global_value(int case_index,int oper) {
    uint8_t status=0;
    switch (case_index) {
        case 0: // freaquecy and voltage
            if(domain->cur_opp_index==DVFS_LOW_SPEED_26M) {
                status = 0x0; //b'0000
            } else if(domain->cur_opp_index==DVFS_FULL_SPEED_78M) {
                status = 0x5; //b'0101
            } else if(domain->cur_opp_index==DVFS_HALF_SPEED_39M) {
                status = 0xa; //b'1010
            } else if(domain->cur_opp_index==DVFS_HIGH_SPEED_156M) {
                status = 0xF; //b'1111
            } else {
                log_hal_msgid_error("[ERROR]Wrong value\r\n", 0);
            }
            dvfs_set_register_value(HW_SYSRAM_PRIVATE_MEMORY_DVFS_STATUS_START,0xf,0,status);
            break;
        case 1 : //B[4]: 0 = dvfs unlock ; 1 = dvfs lock
            dvfs_set_register_value(HW_SYSRAM_PRIVATE_MEMORY_DVFS_STATUS_START,0x1,4,oper);
            break;
        case 2 ://B[5]: 0 = not in dvfs ; 1 = in dvfs
            dvfs_set_register_value(HW_SYSRAM_PRIVATE_MEMORY_DVFS_STATUS_START,0x1,5,oper);
            break;
    }
}

hal_dvfs_status_t hal_dvfs_lock_control(dvfs_frequency_t freq,hal_dvfs_lock_parameter_t lock) {
    int freq_index = DVFS_FULL_SPEED_78M;
    hal_dvfs_status_t result = HAL_DVFS_STATUS_ERROR;
    if (lock == HAL_DVFS_LOCK ) {
        frequency_Ctrl[freq]++;
        hal_dvfs_target_cpu_frequency(domain->frequency[freq],HAL_DVFS_FREQ_RELATION_H);

    }else if( (lock == HAL_DVFS_UNLOCK && frequency_Ctrl[freq])){
        --frequency_Ctrl[freq];
        if(!frequency_Ctrl[freq]){
            for (freq_index = freq; freq_index >= DVFS_LOW_SPEED_26M; freq_index--){
                if (frequency_Ctrl[freq_index]) { break; }
            }
            hal_dvfs_target_cpu_frequency(RESET_DVFS_LEVEL(freq_index),HAL_DVFS_FREQ_RELATION_H);
        }
    }
    result = HAL_DVFS_STATUS_OK;
    return result;
}

hal_dvfs_status_t dvfs_lock_control(char *handle_name, dvfs_frequency_t freq, hal_dvfs_lock_parameter_t lock) {
    int temp_index = 0, next_index = 0;
    hal_dvfs_status_t sta;
    if (freq == DVFS_CURRENT_SPEED) {
        freq = domain->cur_opp_index;
    }
    if (lock == DVFS_LOCK || lock == DVFS_LOCK_ALONE) {
        if (dvfs_lock_alone_status > 0) {
            dvfs_debug("[%s]DVFS fail,because[%s]lock DVFS alone\r\n",handle_name,lock_handle);
            return HAL_DVFS_STATUS_ERROR;
        }
        if (lock == DVFS_LOCK_ALONE) {
            dvfs_lock_alone_status++;
            lock_name = handle_name;
            //log_hal_info("[%s]lock DVFS alone\r\n",handle_name);
        } else {
            log_hal_info("[%s]lock DVFS\r\n",handle_name);
        }
        lock_handle = handle_name;

        hal_dvfs_target_cpu_frequency(domain->frequency[freq], HAL_DVFS_FREQ_RELATION_H);
        frequency_Ctrl[freq]++;
        dvfs_lock_status++;
        dvfs_upload_date_global_value(1, 1);
    } else {

        if (frequency_Ctrl[freq] != 0) {
            if (lock == DVFS_UNLOCK_ALONE && (strncmp(handle_name, lock_handle, strlen(handle_name)) == 0)) {
                dvfs_lock_alone_status--;
                //log_hal_info("[%s]unlock alone\r\n",handle_name);
            } else if (lock == DVFS_UNLOCK_ALONE && (strncmp(handle_name, lock_handle, strlen(handle_name)))) {
                log_hal_info("[%s]not equal[%s]\r\n",handle_name,lock_name);
                return HAL_DVFS_STATUS_ERROR;
            }
            if (dvfs_lock_alone_status == 0) {
                frequency_Ctrl[freq]--;
                dvfs_lock_status--;
            }
        } else {
            log_hal_msgid_error("[%d]unlock too many times\r\n",1,domain->frequency[freq]);
            return HAL_DVFS_STATUS_ERROR;
        }
        for (temp_index = DVFS_HIGH_SPEED_156M; temp_index >= DVFS_LOW_SPEED_26M; temp_index--) {
            if (frequency_Ctrl[temp_index] != 0) {
                next_index = temp_index;
                break;
            }
        }
        if (dvfs_lock_status == 0) {
            dvfs_upload_date_global_value(1, 0);
            if (domain->basic_opp_index >= DVFS_156M_SPEED) {
                domain->basic_opp_index = DVFS_156M_SPEED;
            }
            sta = hal_dvfs_target_cpu_frequency(domain->frequency[domain->basic_opp_index], HAL_DVFS_FREQ_RELATION_H);
            if (sta == HAL_DVFS_STATUS_NOT_PERMITTED) {
                log_hal_msgid_error("No one lock, return to dvfs basic index\r\n", 0);
                return HAL_DVFS_STATUS_ERROR;
            } else if (sta == HAL_DVFS_STATUS_INVALID_PARAM) {
                hal_dvfs_target_cpu_frequency(domain->frequency[(domain->basic_opp_index) + 1], HAL_DVFS_FREQ_RELATION_H);
            } else if (sta == HAL_DVFS_STATUS_ERROR) {
                return HAL_DVFS_STATUS_ERROR;
            }
        } else {
            hal_dvfs_target_cpu_frequency(domain->frequency[next_index], HAL_DVFS_FREQ_RELATION_H);
        }
    }
    return HAL_DVFS_STATUS_OK;
}

uint32_t hal_dvfs_get_cpu_frequency(void)
{
    if (!domain || !domain->initialized) {
        log_hal_error("[%s] non-init\r\n", __FUNCTION__);
        return 0;
    }
    return domain->frequency[domain->cur_opp_index];
}

int dvfs_search_opp(dvfs_opp_t *domain, uint32_t target_freq, hal_dvfs_freq_relation_t relation) {
    int index = 0;
    if (target_freq >= domain->frequency[0]) {
        index = 0;
    } else if ((target_freq <= domain->frequency[0]) && (target_freq >= domain->frequency[1])) {
        index = 1;
    } else if ((target_freq <= domain->frequency[1]) && (target_freq >= domain->frequency[2])) {
        index = 2;
    }

    switch (relation)
    {
        case HAL_DVFS_FREQ_RELATION_L:
            if ((index - 1) <= 0) {
                index = 0;
                index = 0;
            } else {
                index = index - 1;
            }
            return index;
        case HAL_DVFS_FREQ_RELATION_H:
            return index;
        default:
            return -1;
    }
}

hal_dvfs_status_t hal_dvfs_get_cpu_frequency_list(const uint32_t **list, uint32_t *list_num) {
    if (!domain || !domain->initialized) {
        log_hal_error("[%s] non-init\r\n", __FUNCTION__);
        return HAL_DVFS_STATUS_UNINITIALIZED;
    }
    const uint32_t *pfreq = NULL;
    uint8_t num = domain->opp_num;
    for(pfreq = &(domain->frequency)[domain->opp_num - 1];*pfreq == *(pfreq - 1);pfreq--){
        num--;
    }
    *list = domain->frequency;
    *list_num = num;
    return HAL_DVFS_STATUS_OK;
}

static hal_dvfs_status_t dvfs_target_frequency(dvfs_opp_t *domain, uint32_t target_freq,hal_dvfs_freq_relation_t relation)
{
    int freq_index=0,lock_index=0;
    unsigned int next_opp_index, old_opp_index;
    next_opp_index = dvfs_query_frequency(target_freq, domain->frequency, domain->opp_num,relation);
    old_opp_index = domain->cur_opp_index;


     //Find Highest Vcore Voltage
    for (freq_index = DVFS_HIGH_SPEED_156M; freq_index >= DVFS_LOW_SPEED_26M; freq_index--) {
        if (frequency_Ctrl[freq_index] != 0) {
            lock_index = freq_index;
            break;
        }
    }
    //Check lock frequency
    if(lock_index>next_opp_index) {
        log_hal_msgid_info("[Notice]DVFS Lock in higher frequency setting\r\n", 0);
        dvfs_upload_date_global_value(2,0);
        return HAL_DVFS_STATUS_ERROR;
    } else if(dvfs_check_pmic_voltage(next_opp_index) && (lock_index==next_opp_index)&& (old_opp_index==next_opp_index)) {
        log_hal_msgid_info("[Notice]Already in same frequency setting\r\n", 0);
        dvfs_upload_date_global_value(2,0);
        return HAL_DVFS_STATUS_NOT_PERMITTED;
    }else if((dvfs_check_pmic_voltage(next_opp_index)==false) && (lock_index==next_opp_index)&& (old_opp_index==next_opp_index)) {
        log_hal_msgid_info("[Notice]Only Voltage change\r\n", 0);
        domain->switch_voltage(old_opp_index, next_opp_index);
        dvfs_upload_date_global_value(2, 0);
        return HAL_DVFS_STATUS_NOT_PERMITTED;
    }

    //[DVFS can be Implemented]
    if (next_opp_index < old_opp_index) {  //voltgae down
        /* Switch frequencies */
        domain->switch_frequency(old_opp_index, next_opp_index);
        /* Voltage level down */
        domain->switch_voltage(old_opp_index, next_opp_index);
    }
    if (next_opp_index >= old_opp_index) {  //voltgae up
        /* Voltage level up */
        domain->switch_voltage(old_opp_index, next_opp_index);
        /* Switch frequencies */
        domain->switch_frequency(old_opp_index, next_opp_index);
    }

    domain->cur_opp_index = next_opp_index;
    SystemCoreClockUpdate();
    dvfs_upload_date_global_value(0,0);

    dvfs_upload_date_global_value(2,0);
    return HAL_DVFS_STATUS_OK;
}

hal_dvfs_status_t hal_dvfs_target_cpu_frequency(uint32_t target_freq, hal_dvfs_freq_relation_t relation) //keep
{
    dvfs_upload_date_global_value(2,1);
    return dvfs_target_frequency(domain, target_freq, relation);
}
hal_dvfs_status_t hal_dvfs_init(void) //done
{
    if (!domain) {
        domain = dvfs_domain_init();
        if (!domain) {
            log_hal_error("[%s] vcore initialized failed\r\n", __FUNCTION__);
            return HAL_DVFS_STATUS_ERROR;
        }
    }
    return HAL_DVFS_STATUS_OK;
}

void dvfs_cm4_sent_ccni_to_dsp0(uint32_t index) {
    hal_ccni_status_t ret;
    uint32_t status;
    hal_ccni_event_t event = CCNI_CM4_TO_DSP0_EVENT1;
    hal_ccni_message_t msg_array;
    msg_array.ccni_message[0]=2;
    msg_array.ccni_message[1]=index;
    ret = hal_ccni_query_event_status(event, &status);
    if (HAL_CCNI_STATUS_OK != ret) {
        log_hal_msgid_error("CCNI query fail\r\n", 0);
    }
    if (status != HAL_CCNI_EVENT_STATUS_IDLE) {
        log_hal_msgid_error("CCNI busy :fail\r\n", 0);
    }

    if (HAL_CCNI_STATUS_OK != hal_ccni_set_event(event, &msg_array)) {
        log_hal_msgid_error("Error handler\r\n", 0);
    }
}

void dvfs_cm4_receive(hal_ccni_event_t event, void *msg) {
    hal_ccni_status_t status;
    status = hal_ccni_mask_event(event);
    if(status!=HAL_CCNI_STATUS_OK){
        log_hal_error("[%s] hal_ccni_mask_event fail\r\n", __FUNCTION__);
    }
    uint32_t *pMsg = (uint32_t *)msg;
    uint32_t freq_index = pMsg[0];
    hal_dvfs_target_cpu_frequency(domain->frequency[freq_index],HAL_DVFS_FREQ_RELATION_H);
    if(pMsg[1]==0) {
    } else if(pMsg[1]==0x3) {
        hal_dvfs_lock_control(freq_index,HAL_DVFS_LOCK);
    } else if(pMsg[1]==0x4) {
        hal_dvfs_lock_control(freq_index,HAL_DVFS_UNLOCK);
    } else {
        log_hal_msgid_error("[Error] cm4 receive dsp message is wrong\r\n", 0);
    }
    status = hal_ccni_clear_event(event);
    if(status!=HAL_CCNI_STATUS_OK){
        log_hal_error("[%s] hal_ccni_mask_event fail\r\n", __FUNCTION__);
    }
    status = hal_ccni_unmask_event(event);
    if(status!=!HAL_CCNI_STATUS_OK){
        log_hal_error("[%s] hal_ccni_mask_event fail\r\n", __FUNCTION__);
    }
}

hal_dvfs_status_t dvfs_debug_dump(void) {
    const unsigned int *cpufreq_list;
    unsigned int list_num;
    int idx=0;
    hal_dvfs_get_cpu_frequency_list((const uint32_t **) &cpufreq_list, (uint32_t *) &list_num);
    for (idx = 0; idx < list_num; idx++) {
        log_hal_msgid_info("[%d]",1,cpufreq_list[idx]);
    }
    log_hal_msgid_info("domain.cur_opp_index :%d\r\n",1,domain->cur_opp_index);
    log_hal_msgid_info("hal_dvfs_get_cpu_frequency :%d\r\n",1,hal_dvfs_get_cpu_frequency());
    log_hal_msgid_info("[0.7][0.9][1.1][1.2][1.3][1.4][1.5]\r\n", 0);
    log_hal_msgid_info("Vcore index :%d\r\n",1,pmu_get_vcore_voltage_mt6388());

    return HAL_DVFS_STATUS_OK;
}
/*Just for callback when clock pll no one used*/
hal_dvfs_status_t hal_dvfs_reset_basic_setting(void){
    return hal_dvfs_target_cpu_frequency(domain->frequency[(domain->basic_opp_index)],HAL_DVFS_FREQ_RELATION_H);
}
#endif /* HAL_DVFS_MODULE_ENABLED */

