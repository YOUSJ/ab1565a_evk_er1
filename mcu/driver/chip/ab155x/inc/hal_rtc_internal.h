/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#ifndef _HAL_RTC_INTERNAL_H_
#define _HAL_RTC_INTERNAL_H_

#ifdef HAL_RTC_MODULE_ENABLED
//#include "hal_pmu.h"

#define RTC_KEY_BBPU                        (0x4300)
#define RTC_KEY_BBPU_0                      (0xa200)
#define RTC_KEY_BBPU_1                      (0x3900)
#define RTC_KEY_BBPU_2                      (0xda00)
#define RTC_KEY_BBPU_3                      (0xc800)
#define RTC_KEY_BBPU_4                      (0x6400)
#define RTC_KEY_BBPU_5                      (0xee00)
#define RTC_KEY_BBPU_CLEAR_POWERKEY         (0x8700)
#define RTC_KEY_BBPU_RELOAD                 (0x5800)

#define RTC_ALARM_PWREN_OFFSET              (0)
#define RTC_ALARM_PWREN_MASK                (0x1 << RTC_ALARM_PWREN_OFFSET)
/*1: ALARM or EINT occurred, BBPU[1] = 1*/
#define RTC_RTC_PU_OFFSET                   (1)
#define RTC_RTC_PU_MASK                     (0x1 << RTC_RTC_PU_OFFSET)

#define RTC_TICK_PWREN_OFFSET               (2)
#define RTC_TICK_PWREN_MASK                 (0x1 << RTC_TICK_PWREN_OFFSET)

#define RTC_EINT1_PWREN_OFFSET              (3)
#define RTC_EINT1_PWREN_MASK                (0x1 << RTC_EINT1_PWREN_OFFSET)

#define RTC_EINT2_PWREN_OFFSET              (4)
#define RTC_EINT2_PWREN_MASK                (0x1 << RTC_EINT2_PWREN_OFFSET)

#define RTC_CAP_PWREN_OFFSET                (5)
#define RTC_CAP_PWREN_MASK                  (0x1 << RTC_CAP_PWREN_OFFSET)


#define RTC_ALSTA_OFFSET                    (0)
#define RTC_ALSTA_MASK                      (0x1 << RTC_ALSTA_OFFSET)
#define RTC_TCSTA_OFFSET                    (1)
#define RTC_TCSTA_MASK                      (0x1 << RTC_TCSTA_OFFSET)
#define RTC_LPSTA_OFFSET                    (2)
#define RTC_LPSTA_MASK                      (0x1 << RTC_LPSTA_OFFSET)
#define RTC_EINT1STA_OFFSET                 (3)
#define RTC_EINT1STA_MASK                   (0x1 << RTC_EINT1STA_OFFSET)
#define RTC_EINT2STA_OFFSET                 (4)
#define RTC_EINT2STA_MASK                   (0x1 << RTC_EINT2STA_OFFSET)

#define RTC_AL_EN_OFFSET                    (0)
#define RTC_AL_EN_MASK                      (0x1 << RTC_AL_EN_OFFSET)
#define RTC_AL_MASK_DOW_OFFSET              (4)
#define RTC_AL_MASK_DOW_MASK                (0x1 << RTC_AL_MASK_DOW_OFFSET)

#define RTC_TC_EN_OFFSET                    (8)
#define RTC_TC_EN_MASK                      (0x1 << RTC_TC_EN_OFFSET)

#define RTC_TC_SECOND_OFFSET                (0)
#define RTC_TC_SECOND_MASK                  (0x3F << RTC_TC_SECOND_OFFSET)
#define RTC_TC_MINUTE_OFFSET                (8)
#define RTC_TC_MINUTE_MASK                  (0x3F << RTC_TC_MINUTE_OFFSET)

#define RTC_TC_HOUR_OFFSET                  (0)
#define RTC_TC_HOUR_MASK                    (0x1F << RTC_TC_HOUR_OFFSET)
#define RTC_TC_DOM_OFFSET                   (8)
#define RTC_TC_DOM_MASK                     (0x1F << RTC_TC_DOM_OFFSET)

#define RTC_TC_DOW_OFFSET                   (0)
#define RTC_TC_DOW_MASK                     (0x7 << RTC_TC_DOW_OFFSET)
#define RTC_TC_MONTH_OFFSET                 (8)
#define RTC_TC_MONTH_MASK                   (0xF << RTC_TC_MONTH_OFFSET)

#define RTC_TC_YEAR_OFFSET                  (0)
#define RTC_TC_YEAR_MASK                    (0x7F << RTC_TC_YEAR_OFFSET)

#define RTC_AL_SECOND_OFFSET                (0)
#define RTC_AL_SECOND_MASK                  (0x3F << RTC_AL_SECOND_OFFSET)
#define RTC_AL_MINUTE_OFFSET                (8)
#define RTC_AL_MINUTE_MASK                  (0x3F << RTC_AL_MINUTE_OFFSET)

#define RTC_AL_HOUR_OFFSET                  (0)
#define RTC_AL_HOUR_MASK                    (0x1F << RTC_AL_HOUR_OFFSET)
#define RTC_AL_DOM_OFFSET                   (8)
#define RTC_AL_DOM_MASK                     (0x1F << RTC_AL_DOM_OFFSET)

#define RTC_AL_DOW_OFFSET                   (0)
#define RTC_AL_DOW_MASK                     (0x7 << RTC_AL_DOW_OFFSET)
#define RTC_AL_MONTH_OFFSET                 (8)
#define RTC_AL_MONTH_MASK                   (0xF << RTC_AL_MONTH_OFFSET)

#define RTC_AL_YEAR_OFFSET                  (0)
#define RTC_AL_YEAR_MASK                    (0x7F << RTC_AL_YEAR_OFFSET)

#define RTC_RTC_LPD_OPT_OFFSET              (0)
#define RTC_RTC_LPD_OPT_MASK                (0x1 << RTC_RTC_LPD_OPT_OFFSET)
#define RTC_LPSTA_RAW_OFFSET                (0)
#define RTC_LPSTA_RAW_MASK                  (0x1 << RTC_LPSTA_RAW_OFFSET)
#define RTC_EOSC32_LPEN_OFFSET              (1)
#define RTC_EOSC32_LPEN_MASK                (0x1 << RTC_EOSC32_LPEN_OFFSET)
#define RTC_LPRST_OFFSET                    (3)
#define RTC_LPRST_MASK                      (0x1 << RTC_LPRST_OFFSET)

#define RTC_EINT_IRQ_EN_OFFSET              (0)
#define RTC_EINT_IRQ_EN_MASK                (0x1 << RTC_EINT_IRQ_EN_OFFSET)
#define RTC_ENIT_DEB_OFFSET                 (1)
#define RTC_ENIT_DEB_MASK                   (0x1 << RTC_ENIT_DEB_OFFSET)
#define RTC_EINT_SYNC_EN_OFFSET             (2)
#define RTC_EINT_SYNC_EN_MASK               (0x1 << RTC_EINT_SYNC_EN_OFFSET)
#define RTC_EINT_INV_EN_OFFSET              (3)
#define RTC_EINT_INV_EN_MASK                (0x1 << RTC_EINT_INV_EN_OFFSET)
#define RTC_EINT_EN_OFFSET                  (4)
#define RTC_EINT_EN_MASK                    (0x1 << RTC_EINT_EN_OFFSET)
#define RTC_EINT_CLR_OFFSET                 (5)
#define RTC_EINT_CLR_MASK                   (0x1 << RTC_EINT_CLR_OFFSET)

#define RTC_TIMER_CG_EN_OFFSET              (0)
#define RTC_TIMER_CG_EN_MASK                (0x1 << RTC_TIMER_CG_EN_OFFSET)

#define RTC_XOSCCALI_OFFSET                 (0)
#define RTC_XOSCCALI_MASK                   (0x1F << RTC_XOSCCALI_OFFSET)
#define RTC_AMPCTL_EN_OFFSET                (5)
#define RTC_AMPCTL_EN_MASK                  (0x1 << RTC_AMPCTL_EN_OFFSET)
#define RTC_AMP_GSEL_OFFSET                 (6)
#define RTC_AMP_GSEL_MASK                   (0x1 << RTC_AMP_GSEL_OFFSET)
#define RTC_EOSC32_STP_PWD_OFFSET           (7)
#define RTC_EOSC32_STP_PWD_MASK             (0x1 << RTC_EOSC32_STP_PWD_OFFSET)
#define RTC_EOSC32_CHOP_EN_OFFSET           (8)
#define RTC_EOSC32_CHOP_EN_MASK             (0x1 << RTC_EOSC32_CHOP_EN_OFFSET)
#define RTC_EOSC32_VCT_EN_OFFSET            (9)
#define RTC_EOSC32_VCT_EN_MASK              (0x1 << RTC_EOSC32_VCT_EN_OFFSET)
#define RTC_EOSC32_RSV_OFFSET               (10)
#define RTC_EOSC32_RSV_MASK                 (0xF << RTC_EOSC32_RSV_OFFSET)

#define RTC_XOSC_PWDB_OFFSET                (0)
#define RTC_XOSC_PWDB_MASK                  (0x1 << RTC_XOSC_PWDB_OFFSET)
#define RTC_EOSC_PWDB_OFFSET                (1)
#define RTC_EOSC_PWDB_MASK                  (0x1 << RTC_EOSC_PWDB_OFFSET)

#define RTC_RTC_DIFF_OFFSET                 (0)
#define RTC_RTC_DIFF_MASK                   (0x7FF << RTC_RTC_DIFF_OFFSET)

#define RTC_RTC_CALI_OFFSET                 (0)
#define RTC_RTC_CALI_MASK                   (0x1FFF << RTC_RTC_CALI_OFFSET)
#define RTC_CALI_RW_SEL_OFFSET              (13)
#define RTC_CALI_RW_SEL_MASK                (0x1 << RTC_CALI_RW_SEL_OFFSET)

#define RTC_WRTGR_OFFSET                    (0)
#define RTC_WRTGR_MASK                      (0x1 << RTC_WRTGR_OFFSET)
#define RTC_CBUSY_OFFSET                    (0)
#define RTC_CBUSY_MASK                      (0x1 << RTC_CBUSY_OFFSET)
#define RTC_POWER_DETECTED_OFFSET           (1)
#define RTC_POWER_DETECTED_MASK             (0x1 << RTC_POWER_DETECTED_OFFSET)
#define RTC_STA_INITB_OFFSET                (2)
#define RTC_STA_INITB_MASK                  (0x1 << RTC_STA_INITB_OFFSET)

#define RTC_CAP_RST_OFFSET                  (0)
#define RTC_CAP_RST_MASK                    (0x1 << RTC_CAP_RST_OFFSET)
#define RTC_CAP_ISO_OFFSET                  (1)
#define RTC_CAP_ISO_MASK                    (0x1 << RTC_CAP_ISO_OFFSET)
#define RTC_CAP_CLOCK_OFFSET                (2)
#define RTC_CAP_CLOCK_MASK                  (0x1 << RTC_CAP_CLOCK_OFFSET)

#define RTC_LOW_BYTE_OPERATION              (0)
#define RTC_HIGH_BYTE_OPERATION             (1)
#define RTC_WORD_OPERATION                  (2)

#define RTC_STANDARD_1_OFFSET               (0)
#define RTC_STANDARD_1_MASK                 (0x1 << RTC_STANDARD_1_OFFSET)
#define RTC_STANDARD_8_MASK                 (0xff << RTC_STANDARD_1_OFFSET)

#define RTC_POWERKEY0_KEY                   0xa357
#define RTC_POWERKEY1_KEY                   0x67d2

#define RTC_PROTECT1                        0x586a
#define RTC_PROTECT2                        0x9136

#define RTC_OSC32CON0_MAGIC_KEY_1           0x1a57
#define RTC_OSC32CON0_MAGIC_KEY_2           0x2b68

#define RTC_OSC32CON1_MAGIC_KEY_1           0x1a85
#define RTC_OSC32CON1_MAGIC_KEY_2           0xe7cf

#define RTC_OSC32CON2_MAGIC_KEY_1           0x1653
#define RTC_OSC32CON2_MAGIC_KEY_2           0x8918


#define ABIST_FQMTR_BASE                    (0xa2020000 + 0x400)
#define CKSYS_TST_SEL_1_BASE                ((volatile uint32_t *)0xA2020224)
#if 0
#define CKSYS_XTAL_FREQ_BASE                ((volatile uint32_t *)0xA20202A0)
#define RTC_FXO_IS_26M                      ((volatile uint8_t*)(0xA20202A3))
#define RTC_XO_DIV_32K_EN                   ((volatile uint32_t*)(0xA202028C))
#endif
/* frequency meter start */
typedef struct {
    __IO uint32_t ABIST_FQMTR_CON0;     /* Frequency Meter Control Register 0 */
    __IO uint32_t ABIST_FQMTR_CON1;     /* Frequency Meter Control Register 1 */
    __IO uint32_t ABIST_FQMTR_CON2;     /* Frequency Meter Control Register 2 */
    __IO uint32_t ABIST_FQMTR_DATA;     /* Frequency Meter Data */
} ABIST_FQMTR_REGISTER_T;
/* frequency meter end */

/* For internal user request, return frequency meter count with specific test. */
uint32_t f32k_measure_count(uint16_t fixed_clock, uint16_t tested_clock, uint16_t window_setting);
/* Turn off RTC 32k */
void rtc_32k_off(void);
/* For captouch to clear BBPU[1] */
void rtc_captouch_ack(void);
/* For use RTC GPIO as GPIO mode setting */
/* example:
1. RTC_GPIO_1 output high:
    hal_rtc_gpio_setting_t gpio_setting;
    gpio_setting.rtc_gpio = HAL_RTC_GPIO_1;
    gpio_setting.is_input_direction = false;
    gpio_setting.is_output_high = true;
    rtc_gpio_setting(&gpio_setting);
2. RTC_GPIO_1 output low:
    hal_rtc_gpio_setting_t gpio_setting;
    gpio_setting.rtc_gpio = HAL_RTC_GPIO_1;
    gpio_setting.is_input_direction = false;
    gpio_setting.is_output_high = false;
    rtc_gpio_setting(&gpio_setting);
3. Get RTC_GPIO_1 input value:
    hal_rtc_gpio_setting_t gpio_setting;
    hal_rtc_gpio_data_t data;
    gpio_setting.rtc_gpio = HAL_RTC_GPIO_1;
    gpio_setting.is_input_direction = true;
    rtc_gpio_setting(&gpio_setting);
    rtc_gpio_get_input(HAL_RTC_GPIO_1, &data);
*/
void rtc_gpio_setting(hal_rtc_gpio_setting_t *gpio_setting);
/* Get RTC GPIO input value */
void rtc_gpio_get_input(hal_rtc_gpio_t rtc_gpio, hal_rtc_gpio_data_t *data);

#endif /* #ifdef HAL_RTC_MODULE_ENABLED */
#endif /* #ifndef _HAL_RTC_INTERNAL_H_ */
