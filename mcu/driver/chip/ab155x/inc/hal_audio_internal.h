/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __HAL_AUDIO_INTERNAL_H__
#define __HAL_AUDIO_INTERNAL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "hal_audio.h"
#include "hal_audio_message_struct.h"

#ifdef HAL_DVFS_MODULE_ENABLED
#include "hal_dvfs.h"
#include "hal_dvfs_internal.h"
#endif

#ifdef HAL_AUDIO_SUPPORT_MULTIPLE_STREAM_OUT
#define PACKED __attribute__((packed))
#endif

//==== Definition ====
#define UPPER_BOUND(in,up)      ((in) > (up) ? (up) : (in))
#define LOWER_BOUND(in,lo)      ((in) < (lo) ? (lo) : (in))
#define BOUNDED(in,up,lo)       ((in) > (up) ? (up) : (in) < (lo) ? (lo) : (in))
#define MAXIMUM(a,b)            ((a) > (b) ? (a) : (b))
#define MINIMUM(a,b)            ((a) < (b) ? (a) : (b))
#define FOUR_BYTE_ALIGNED(size) (((size) + 3) & ~0x3)

typedef enum {
#ifdef HAL_DVFS_MODULE_ENABLED
    HAL_AUDIO_DVFS_UNLOCK = DVFS_UNLOCK,
    HAL_AUDIO_DVFS_LOCK   = DVFS_LOCK,
#else
    HAL_AUDIO_DVFS_UNLOCK,
    HAL_AUDIO_DVFS_LOCK,
#endif
} hal_audio_dvfs_lock_parameter_t;

typedef enum {
#ifdef HAL_DVFS_MODULE_ENABLED
    HAL_AUDIO_DVFS_DEFAULT_SPEED = DVFS_26M_SPEED,
    HAL_AUDIO_DVFS_MEDIUM_SPEED	 = DVFS_39M_SPEED,
    HAL_AUDIO_DVFS_HIGH_SPEED    = DVFS_78M_SPEED,
    HAL_AUDIO_DVFS_MAX_SPEED     = DVFS_156M_SPEED,
#else
    HAL_AUDIO_DVFS_DEFAULT_SPEED,
    HAL_AUDIO_DVFS_MEDIUM_SPEED,
    HAL_AUDIO_DVFS_HIGH_SPEED,
    HAL_AUDIO_DVFS_MAX_SPEED,
#endif
} hal_audio_dvfs_speed_t;

#if defined(HAL_AUDIO_SUPPORT_MULTIPLE_MICROPHONE)
typedef enum {
    INPUT_DIGITAL_GAIN_FOR_DEVICE_0             = 0,            /**< input digital gain for device path.  */
    INPUT_DIGITAL_GAIN_FOR_DEVICE_1             = 1,            /**< input digital gain for device1 path.  */
    INPUT_DIGITAL_GAIN_FOR_DEVICE_2             = 2,            /**< input digital gain for device2 path.  */
    INPUT_DIGITAL_GAIN_FOR_DEVICE_3             = 3,            /**< input digital gain for device3 path.  */
    INPUT_DIGITAL_GAIN_FOR_ECHO_PATH            = 4,            /**< input digital gain for echo path.  */
    INPUT_DIGITAL_GAIN_NUM                      = 5,            /**< Specifies the number of input digital gain .  */
} afe_input_digital_gain_t;

typedef enum {
    INPUT_ANALOG_GAIN_FOR_MIC_L                 = 0,            /**< input analog gain for analog microphone L.  */
    INPUT_ANALOG_GAIN_FOR_MIC_R                 = 1,            /**< input analog gain for analog microphone R.  */
    INPUT_ANALOG_GAIN_NUM                       = 2,            /**< Specifies the number of input analog gain.  */
} afe_input_analog_gain_t;
//== hal_audio.c related ==
typedef struct {
    hal_audio_sampling_rate_t   stream_sampling_rate;                   /**< Specifies the sampling rate of audio data.*/
    hal_audio_bits_per_sample_t stream_bit_rate;                        /**< Specifies the number of bps of audio data.*/
    hal_audio_channel_number_t  stream_channel;                         /**< Specifies the number of channel.*/
    hal_audio_channel_number_t  stream_channel_mode;                    /**< Specifies the mode of channel.*/
    hal_audio_device_t          audio_device;                           /**< Specifies the device.*/
    bool                        mute;                                   /**< Specifies whether the device is mute or not.*/
    uint32_t                    digital_gain_index[INPUT_DIGITAL_GAIN_NUM];   /**< Digital gain index of the audio stream.*/
    uint32_t                    analog_gain_index[INPUT_ANALOG_GAIN_NUM];     /**< Analog gain index of the audio stream.*/
} hal_audio_stream_info_t;
#else
//== hal_audio.c related ==
typedef struct {
    hal_audio_sampling_rate_t   stream_sampling_rate;  /**< Specifies the sampling rate of audio data.*/
    hal_audio_bits_per_sample_t stream_bit_rate;       /**< Specifies the number of bps of audio data.*/
    hal_audio_channel_number_t  stream_channel;        /**< Specifies the number of channel.*/
    hal_audio_channel_number_t  stream_channel_mode;   /**< Specifies the mode of channel.*/
    hal_audio_device_t          audio_device;          /**< Specifies the device.*/
    bool                        mute;                  /**< Specifies whether the device is mute or not.*/
    uint32_t                    digital_gain_index;    /**< Digital gain index of the audio stream.*/
    uint32_t                    analog_gain_index;     /**< Analog gain index of the audio stream.*/
} hal_audio_stream_info_t;
#endif

typedef struct {
    bool                    init;
    void                    *allocated_memory;

    // stream in/out information
    hal_audio_stream_info_t stream_in;
    hal_audio_stream_info_t stream_out;
    hal_audio_stream_info_t stream_out_DL2;

} audio_common_t;

//== hal_audio_dsp_controller.c related ==
typedef struct {
    uint32_t mState;
    int32_t  mUserCount;
} audiosys_clkmux_control;

typedef enum {
    AUDIO_CLKMUX_BLOCK_INTERNAL_BUS = 0,
    AUDIO_CLKMUX_BLOCK_DOWNLINK_HIRES,
    AUDIO_CLKMUX_BLOCK_UPLINK_HIRES,
    AUDIO_CLKMUX_BLOCK_HW_SRC,
    AUDIO_CLKMUX_BLOCK_NUM_OF_CLKMUX_BLOCK,
} audio_clkmux_block_t;

/** @brief audio structure */
#ifdef HAL_AUDIO_SUPPORT_MULTIPLE_STREAM_OUT
typedef enum {
    AU_DSP_AUDIO  = 0,
    AU_DSP_VOICE  = 1,
    AU_DSP_ANC    = 2,
    AU_DSP_RECORD = 3,
} audio_scenario_sel_t;
typedef enum {
    AU_DSP_CH_LR = 0,       /**< */
    AU_DSP_CH_L,            /**< */
    AU_DSP_CH_R,            /**< */
    AU_DSP_CH_SWAP,         /**< */
    AU_DSP_CH_MIX,
    AU_DSP_CH_MIX_SHIFT,
} audio_channel_sel_t;

typedef enum {
    SDK_NONE   = 0x0000,
    SDK_V1p3   = 0x0001,
    SDK_V1p4   = 0x0002,
} audio_version_t;

typedef struct HAL_AUDIO_CH_SEL_HW_MODE_s
{
    uint8_t                     audioChannelGPIOH;      /**< Channel select when GPIO is high. 0:None, 1:L CH, 2:R CH, 3:Two CH*/
    uint8_t                     audioChannelGPIOL;      /**< Channel select when GPIO is low.  0:None, 1:L CH, 2:R CH, 3:Two CH*/
    uint8_t                     gpioIndex;              /**< GPIO index for audio channel select in HW mode. */
} PACKED HAL_AUDIO_CH_SEL_HW_MODE_t;

typedef struct HAL_AUDIO_CHANNEL_SELECT_s
{
    uint8_t                     modeForAudioChannel;    /**< NVkey_0     Channel select mode. 0:SW_mode, 1: HW_mode */
    uint8_t                     audioChannel;           /**< NVkey_1     Channel select. 0:None, 1:L CH, 2:R CH, 3:Two CH */
    HAL_AUDIO_CH_SEL_HW_MODE_t  hwAudioChannel;         /**< NVkey_2-4  Channel select param in HW_Mode.*/
} PACKED HAL_AUDIO_CHANNEL_SELECT_t;

typedef struct HAL_DSP_PARA_AU_AFE_CTRL_s
{
    uint8_t       Audio_InputDev;               /**< NVkey_0   Audio Stream in device.   [Default BT], [line in playback], [I2S master]
                                                                                                                                            | [I2S master interface]  */
    uint8_t       Audio_OutputDev;              /**< NVkey_1   Audio Stream out device. [DAC (speaker)], [I2S master for external amp.]
                                                                                                                                            | [I2S master interface]  */
    uint8_t       Voice_InputDev;               /**< NVkey_2   Voice Stream in device.   [Analog mic], [Digital mic], [I2S master], [Extended mic configuration]
                                                                                                                                            | [I2S master interface]
                                                                                                                                            | [Analog Mic Select L, R, Dual, none]
                                                                                                                                            | [Digital Mic Select L, R, Dual, none]  */
    uint8_t       Voice_OutputDev;              /**< NVkey_3   Voice Stream out device. [DAC (speaker)], [I2S master for external amp.]
                                                                                                                                            | [I2S master interface]  */
    uint8_t       Voice_Sidetone_EN;            /**< NVkey_4   Voice(HFP) Sidetone enable. 0x0: Disable, 0x1: Enable.*/
    uint8_t       Voice_Sidetone_Gain;          /**< NVkey_5   Voice(HFP) Sidetone gain(dB).*/
    uint8_t       Reserve_Sidetone_Gain;        /**< NVkey_6   Reserve Sidetone gain(dB).*/
    uint8_t       UL_AFE_PARA;                  /**< NVkey_7   UL AFE parameters. [Analog Mic mode, 0x0: DCC, 0x1: ACC]
                                                                                                                                            |[ADC mode, 0x0: Normal, 0x1: High performance].*/
    uint8_t       DL_AFE_PARA;                  /**< NVkey_8   DL AFE parameters. [DAC mode, 0x0: Normal, 0x1: High performance].*/
    uint8_t       MICBIAS_PARA_Audio;           /**< NVkey_9   Audio Stream in(LineIN) MICBIAS setting. [MICBIAS 1, 0x0: Disable, 0x1: Enable.]
                                                                                                                                                            | [MICBIAS 2, 0x0: Disable, 0x1: Enable.]
                                                                                                                                                            | [MICBIAS Level, Lock 0x6: 2.4V] */
    uint8_t       MICBIAS_PARA_Voice;           /**< NVkey_10  Voice Stream in MICBIAS setting. [MICBIAS 1, 0x0: Disable, 0x1: Enable.]
                                                                                                                                                            | [MICBIAS 2, 0x0: Disable, 0x1: Enable.]
                                                                                                                                                            | [MICBIAS Level, 0x0: 1.87V, 0x1: 1.85V, 0x2: 1.9V, 0x3: 2.0V, 0x4: 2.1V, 0x5: 2.2V, 0x6: 2.4V, 0x7: bypass] */
    uint8_t       MICBIAS_PARA_RESERVE;         /**< NVkey_11  Reserve Stream in MICBIAS setting. [MICBIAS 1, 0x0: Disable, 0x1: Enable.]
                                                                                                                                                            | [MICBIAS 2, 0x0: Disable, 0x1: Enable.]
                                                                                                                                                            | [MICBIAS Level, 0x0: 1.87V, 0x1: 1.85V, 0x2: 1.9V, 0x3: 2.0V, 0x4: 2.1V, 0x5: 2.2V, 0x6: 2.4V, 0x7: bypass] */
    uint8_t       I2S_PARA_Audio;               /**< NVkey_12  Audio I2S default parameters. [I2S default CLK source, 0x0: DCXO, 0x1: APLL].*/
    uint8_t       I2S_PARA_Voice;               /**< NVkey_13  Voice I2S default parameters. [I2S default CLK source, 0x0: DCXO, 0x1: APLL].*/
    uint8_t       I2S_PARA_RESERVE;             /**< NVkey_14  Reserve I2S default parameters. [I2S default CLK source, 0x0: DCXO, 0x1: APLL].*/
    uint8_t       AMIC_PARA;                    /**< NVkey_15  Analog mic settings. [VIN0 PU/D, 0x0: MEMS, 0x1: ECM Differential, 0x2: ECM Single].
                                                                                                                                           |[VIN1 PU/D, 0x0: MEMS, 0x1: ECM Differential, 0x2: ECM Single]*/
    uint8_t       DMIC_PARA;                    /**< NVkey_16  Digital mic settings. [Digital Mic GPIO, 0x0: DMIC_0, 0x1: DMIC_1, 0x2: none]*/
    uint8_t       LOOPBACK_RATE;                /**< NVkey_17  Loopback or LineIN sampling rate setting.*/
    uint8_t       LOOPBACK_PARA;                /**< NVkey_18  Loopback or LineIN parameters.*/
    uint8_t       MIC_Select_Main;              /**< NVkey_19  Main MIC device select. 0x00: Analog_Mic_L, 0x01: Analog_Mic_R, 0x02: Digital_Mic0_L, 0x03: Digital_Mic0_R, 0x04: Digital_Mic1_L, 0x05: Digital_Mic1_R.*/
    uint8_t       MIC_Select_Ref;               /**< NVkey_20  Reference MIC device select. 0x00: Analog_Mic_L, 0x01: Analog_Mic_R, 0x02: Digital_Mic0_L, 0x03: Digital_Mic0_R, 0x04: Digital_Mic1_L, 0x05: Digital_Mic1_R.*/
    uint8_t       MIC_Select_Ref_2;             /**< NVkey_21  Reference 2 MIC device select. 0x00: Analog_Mic_L, 0x01: Analog_Mic_R, 0x02: Digital_Mic0_L, 0x03: Digital_Mic0_R, 0x04: Digital_Mic1_L, 0x05: Digital_Mic1_R.*/
    uint8_t       MIC_Select_Ref_3;             /**< NVkey_22  Reference 3 MIC device select. 0x00: Analog_Mic_L, 0x01: Analog_Mic_R, 0x02: Digital_Mic0_L, 0x03: Digital_Mic0_R, 0x04: Digital_Mic1_L, 0x05: Digital_Mic1_R.*/
    uint8_t       MIC_Select_ANC_FF;            /**< NVkey_23  ANC FF MIC device select. 0x00: Analog_Mic_L, 0x01: Analog_Mic_R, 0x02: Digital_Mic0_L, 0x03: Digital_Mic0_R, 0x04: Digital_Mic1_L, 0x05: Digital_Mic1_R.*/
    uint8_t       MIC_Select_ANC_FB;            /**< NVkey_24  ANC FB MIC device select. 0x00: Analog_Mic_L, 0x01: Analog_Mic_R, 0x02: Digital_Mic0_L, 0x03: Digital_Mic0_R, 0x04: Digital_Mic1_L, 0x05: Digital_Mic1_R.*/
    uint8_t       ANC_OutputDev;                /**< NVkey_25  ANC out device. [DAC (speaker)]*/
    uint8_t       MIC_Select_Record_Main;       /**< NVkey_26  Main MIC device select. 0x00: Analog_Mic_L, 0x01: Analog_Mic_R, 0x02: Digital_Mic0_L, 0x03: Digital_Mic0_R, 0x04: Digital_Mic1_L, 0x05: Digital_Mic1_R.*/
    uint8_t       MIC_Select_Record_Ref;        /**< NVkey_27  Reference MIC device select. 0x00: Analog_Mic_L, 0x01: Analog_Mic_R, 0x02: Digital_Mic0_L, 0x03: Digital_Mic0_R, 0x04: Digital_Mic1_L, 0x05: Digital_Mic1_R.*/
    uint8_t       MICBIAS_PARA_Record;          /**< NVkey_28  Record MICBIAS setting. [MICBIAS 1, 0x0: Disable, 0x1: Enable.]
                                                                                                                                                            | [MICBIAS 2, 0x0: Disable, 0x1: Enable.]
                                                                                                                                                            | [MICBIAS Level, Lock 0x6: 2.4V] */
} PACKED HAL_DSP_PARA_AU_AFE_CTRL_t;
#endif


typedef struct HAL_AUDIO_DVFS_CLK_SELECT_s
{
    uint8_t HFP_DVFS_CLK; /* NVkey_0 0x00:26MHz 0x01:39MHz 0x02:78MHz 0x03:156MHz */
    uint8_t RESERVED_1;   /* NVkey_1 0x00 */
    uint8_t RESERVED_2;   /* NVkey_2 0x00 */
    uint8_t RESERVED_3;   /* NVkey_3 0x00 */
    uint8_t RESERVED_4;   /* NVkey_4 0x00 */
    uint8_t RESERVED_5;   /* NVkey_5 0x00 */
    uint8_t RESERVED_6;   /* NVkey_6 0x00 */
    uint8_t RESERVED_7;   /* NVkey_7 0x00 */
    uint8_t RESERVED_8;   /* NVkey_8 0x00 */
    uint8_t RESERVED_9;   /* NVkey_9 0x00 */
} PACKED HAL_AUDIO_DVFS_CLK_SELECT_t;


//== Message related ==
#define MSG_TYPE_BASE_MASK              0x0F00
#define MSG_TYPE_SHIFT_BIT              8


//== Callback related ==
typedef void (*hal_audio_callback_t)(hal_audio_event_t event, void *user_data);
typedef void (*hal_bt_audio_dl_open_callback_t)(void);
typedef void (*hal_audio_notify_task_callback_t)(void);
typedef void (*hal_audio_task_ms_delay_function_t)(uint32_t ms);

//== Ring buffer ==
typedef struct {
    uint32_t write_pointer;
    uint32_t read_pointer;
    uint32_t buffer_byte_count;
    uint8_t *buffer_base_pointer;
} ring_buffer_information_t;

//== AWS related ==
// ToDo: workaround. Not necessary
#define HAL_AUDIO_AWS_NORMAL        0
#define HAL_AUDIO_AWS_NOT_SUPPORT  -1
#define HAL_AUDIO_AWS_ERROR        -2

typedef enum {
    AWS_CODEC_TYPE_AAC_FORMAT,
    AWS_CODEC_TYPE_SBC_FORMAT,
    AWS_CODEC_TYPE_MP3_FORMAT,
    AWS_CODEC_TYPE_PCM_FORMAT
} aws_codec_type_t;

typedef enum {
    AWS_CLOCK_SKEW_STATUS_IDLE,
    AWS_CLOCK_SKEW_STATUS_BUSY
} aws_clock_skew_status_t;

typedef enum {
    CODEC_AWS_CHECK_CLOCK_SKEW,
    CODEC_AWS_CHECK_UNDERFLOW
} aws_event_t;

typedef void (*aws_callback_t)(aws_event_t event, void *user_data);

//==== API ====
void hal_audio_dsp_controller_init(void);
void hal_audio_dsp_controller_deinit(void);

void hal_audio_dsp_controller_send_message(uint16_t message, uint16_t data16, uint32_t data32, bool wait);
void *hal_audio_dsp_controller_put_paramter(const void *p_param_addr, uint32_t param_size, audio_message_type_t msg_type);

void hal_audio_dsp_internalbus_clkmux_control(uint32_t device_rate, bool isEnable);
void hal_audio_dsp_hwsrc_clkmux_control(uint32_t device_rate, bool isEnable);
void hal_audio_dsp_dl_clkmux_control(audio_message_type_t type, hal_audio_device_t out_device, uint32_t device_rate, bool isEnable);
void hal_audio_dsp_ul_clkmux_control(hal_audio_device_t out_device, uint32_t device_rate, bool isEnable);

void hal_audio_set_task_notification_callback(hal_audio_notify_task_callback_t callback);
void hal_audio_dsp_message_process(void);
void hal_audio_set_task_ms_delay_function(hal_audio_task_ms_delay_function_t delay_func);
extern uint32_t hal_audio_dsp2mcu_data_get(void);
uint32_t hal_audio_dsp2mcu_AUDIO_DL_ACL_data_get(void);

#ifdef HAL_AUDIO_SUPPORT_MULTIPLE_STREAM_OUT
uint32_t hal_audio_sampling_rate_enum_to_value(hal_audio_sampling_rate_t hal_audio_sampling_rate_enum);
/**
 * @brief     Get stream in device, MemInterface, i2s_interface from static HW I/O config table.
 * @param[out] Device is a pointer to the audio Device.
 * @param[out] MemInterface is a pointer to the audio MemInterface.
 * @param[out] i2s_interface is a pointer to the audio i2s_interface.
 * @return     #HAL_AUDIO_STATUS_OK, if OK. #HAL_AUDIO_STATUS_ERROR, if wrong.
 */
hal_audio_status_t hal_audio_get_stream_in_setting_config(audio_scenario_sel_t Audio_or_Voice, mcu2dsp_open_stream_in_param_t *stream_in_open_param);

/**
 * @brief     Get stream out device, MemInterface, i2s_interface from static HW I/O config table.
 * @param[out] Device is a pointer to the audio Device.
 * @param[out] MemInterface is a pointer to the audio MemInterface.
 * @param[out] i2s_interface is a pointer to the audio i2s_interface.
 * @return     #HAL_AUDIO_STATUS_OK, if OK. #HAL_AUDIO_STATUS_ERROR, if wrong.
 */
hal_audio_status_t hal_audio_get_stream_out_setting_config(audio_scenario_sel_t Audio_or_Voice, mcu2dsp_open_stream_out_param_t *stream_out_open_param);
#endif

void hal_audio_set_dvfs_control(hal_audio_dvfs_speed_t DVFS_SPEED, hal_audio_dvfs_lock_parameter_t DVFS_lock);

#ifdef HAL_DVFS_MODULE_ENABLED
void hal_audio_set_dvfs_clk(audio_scenario_sel_t Audio_or_Voice, dvfs_frequency_t *dvfs_clk);
#endif

//== Audio Service ==
void hal_audio_service_hook_callback(audio_message_type_t type, hal_audio_callback_t callback, void *user_data);
void hal_audio_service_unhook_callback(audio_message_type_t type);

//== Share buffer ==
n9_dsp_share_info_t *hal_audio_query_bt_audio_dl_share_info(void);
n9_dsp_share_info_t *hal_audio_query_bt_voice_ul_share_info(void);
n9_dsp_share_info_t *hal_audio_query_bt_voice_dl_share_info(void);
n9_dsp_share_info_t *hal_audio_query_ble_audio_ul_share_info(void);
n9_dsp_share_info_t *hal_audio_query_ble_audio_dl_share_info(void);
n9_dsp_share_info_t *hal_audio_query_playback_share_info(void);
n9_dsp_share_info_t *hal_audio_query_record_share_info(void);
#ifdef MTK_BT_SPEAKER_ENABLE
uint8_t *hal_audio_query_fec_share_info(void);
#endif
uint32_t *hal_audio_query_rcdc_share_info(void);
uint32_t *hal_audio_query_hfp_air_dump(void);
n9_dsp_audio_sync_info_t *hal_audio_query_audio_sync_info(void);
n9_dsp_share_info_t *hal_audio_query_prompt_share_info(void);
n9_dsp_share_info_t *hal_audio_query_nvkey_parameter_share_info(void);
void hal_audio_reset_share_info(n9_dsp_share_info_t *p_info);
void hal_audio_a2dp_reset_share_info(n9_dsp_share_info_t *p_info);
void hal_audio_set_sysram(void);
uint32_t *hal_audio_query_ltcs_asi_buf(void);
uint32_t *hal_audio_query_ltcs_min_gap_buf(void);
uint32_t *hal_audio_report_bitrate_buf(void);
uint32_t *hal_audio_report_lostnum_buf(void);
hal_audio_status_t hal_audio_write_audio_drift_val(int32_t val);
hal_audio_status_t hal_audio_write_audio_anchor_clk(uint32_t val);
hal_audio_status_t hal_audio_write_audio_asi_base(uint32_t val);
hal_audio_status_t hal_audio_write_audio_asi_cur(uint32_t val);

//== Buffer management related ==
uint32_t hal_audio_buf_mgm_get_data_byte_count(n9_dsp_share_info_t *p_info);
uint32_t hal_audio_buf_mgm_get_free_byte_count(n9_dsp_share_info_t *p_info);
void hal_audio_buf_mgm_get_free_buffer(
    n9_dsp_share_info_t *p_info,
    uint8_t **pp_buffer,
    uint32_t *p_byte_count);
void hal_audio_buf_mgm_get_data_buffer(
    n9_dsp_share_info_t *p_info,
    uint8_t **pp_buffer,
    uint32_t *p_byte_count);
void hal_audio_buf_mgm_get_write_data_done(n9_dsp_share_info_t *p_info, uint32_t byte_count);
void hal_audio_buf_mgm_get_read_data_done(n9_dsp_share_info_t *p_info, uint32_t byte_count);

//== Status control ==
void hal_audio_status_set_running_flag(audio_message_type_t type, bool is_running);
void hal_audio_status_set_notify_flag(audio_message_type_t type, bool is_notify);
bool hal_audio_status_query_running_flag(audio_message_type_t type);
bool hal_audio_status_query_notify_flag(audio_message_type_t type);
uint16_t hal_audio_status_query_running_flag_value();

//== Data path ==
hal_audio_status_t hal_audio_write_stream_out_by_type(audio_message_type_t type, const void *buffer, uint32_t size);

//== AM A2DP open callback ==
void hal_audio_am_register_a2dp_open_callback(hal_bt_audio_dl_open_callback_t callback);

//== Speech related parameter ==
void speech_update_common(const uint16_t *common);
void speech_update_nb_param(const uint16_t *param);
void speech_update_wb_param(const uint16_t *param);
void speech_update_nb_fir(const int16_t *in_coeff, const int16_t *out_coeff);
void speech_update_wb_fir(const int16_t *in_coeff, const int16_t *out_coeff);

int32_t audio_update_iir_design(const uint32_t *parameter);

//== Ring buffer operation ==
uint32_t ring_buffer_get_data_byte_count(ring_buffer_information_t *p_info);
uint32_t ring_buffer_get_space_byte_count(ring_buffer_information_t *p_info);
void ring_buffer_get_write_information(ring_buffer_information_t *p_info, uint8_t **pp_buffer, uint32_t *p_byte_count);
void ring_buffer_get_read_information(ring_buffer_information_t *p_info, uint8_t **pp_buffer, uint32_t *p_byte_count);
void ring_buffer_write_done(ring_buffer_information_t *p_info, uint32_t write_byte_count);
void ring_buffer_read_done(ring_buffer_information_t *p_info, uint32_t read_byte_count);

uint32_t ring_buffer_get_data_byte_count_non_mirroring(ring_buffer_information_t *p_info);
uint32_t ring_buffer_get_space_byte_count_non_mirroring(ring_buffer_information_t *p_info);
void ring_buffer_get_write_information_non_mirroring(ring_buffer_information_t *p_info, uint8_t **pp_buffer, uint32_t *p_byte_count);
void ring_buffer_get_read_information_non_mirroring(ring_buffer_information_t *p_info, uint8_t **pp_buffer, uint32_t *p_byte_count);
void ring_buffer_write_done_non_mirroring(ring_buffer_information_t *p_info, uint32_t write_byte_count);
void ring_buffer_read_done_non_mirroring(ring_buffer_information_t *p_info, uint32_t read_byte_count);

//== Time report ==
audio_dsp_a2dp_dl_time_param_t *hal_audio_a2dp_dl_get_time_report(void);

#ifdef __cplusplus
}
#endif

#endif /*__HAL_AUDIO_INTERNAL_H__ */
