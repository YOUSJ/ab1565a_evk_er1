/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#ifndef __HAL_PINMUX_DEFINE_H__




#define __HAL_PINMUX_DEFINE_H__

#include "hal_platform.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef HAL_GPIO_MODULE_ENABLED


#define HAL_GPIO_0_GPIO0    0	
#define HAL_GPIO_0_SPI_MST2_SIO2    1	
#define HAL_GPIO_0_I2S_MST2_MCLK    2	
#define HAL_GPIO_0_WIFITOBT    3	
#define HAL_GPIO_0_AUXADC6    4	
#define HAL_GPIO_0_AP_JTMS    5	
#define HAL_GPIO_0_PAD_DFUNC_SOUT    6	
#define HAL_GPIO_0_EINT0    7	
#define HAL_GPIO_0_DEBUGMON17    8

#define HAL_GPIO_1_GPIO1    0	
#define HAL_GPIO_1_UART0_RXD    1	
#define HAL_GPIO_1_I2S_MST2_CK    2	
#define HAL_GPIO_1_I2S_SLV2_CK    3	
#define HAL_GPIO_1_AUXADC5    4	
#define HAL_GPIO_1_AP_JTCK    5		
#define HAL_GPIO_1_EINT1    7	
#define HAL_GPIO_1_DEBUGMON16    8

#define HAL_GPIO_2_GPIO2    0	
#define HAL_GPIO_2_UART0_TXD    1	
#define HAL_GPIO_2_I2S_MST2_TX    2	
#define HAL_GPIO_2_I2S_SLV2_TX    3	
#define HAL_GPIO_2_AUXADC4    4	
#define HAL_GPIO_2_AP_JTDI    5	
#define HAL_GPIO_2_DSP_JTMS    6	
#define HAL_GPIO_2_EINT2    7	
#define HAL_GPIO_2_DEBUGMON15    8

#define HAL_GPIO_3_GPIO3    0	
#define HAL_GPIO_3_SPI_MST2_SCK    1	
#define HAL_GPIO_3_I2S_MST2_WS    2	
#define HAL_GPIO_3_I2S_SLV2_WS    3	
#define HAL_GPIO_3_AUXADC3    4	
#define HAL_GPIO_3_AP_JRSTB    5	
#define HAL_GPIO_3_DSP_JTCK    6	
#define HAL_GPIO_3_EINT3    7	
#define HAL_GPIO_3_DEBUGMON14    8

#define HAL_GPIO_4_GPIO4    0	
#define HAL_GPIO_4_SPI_MST2_MISO    1	
#define HAL_GPIO_4_I2S_MST2_RX    2	
#define HAL_GPIO_4_I2S_SLV2_RX    3	
#define HAL_GPIO_4_AUXADC2    4	
#define HAL_GPIO_4_AP_JTDO    5	
#define HAL_GPIO_4_DSP_JTDI    6	
#define HAL_GPIO_4_EINT4    7	
#define HAL_GPIO_4_DEBUGMON13    8

#define HAL_GPIO_5_GPIO5    0	
#define HAL_GPIO_5_SPI_MST2_CS    1	
#define HAL_GPIO_5_UART0_RXD    2	
#define HAL_GPIO_5_I2C0_SCL    3	
#define HAL_GPIO_5_AUXADC1    4	
#define HAL_GPIO_5_CONN_AICE_TMSC    5	
#define HAL_GPIO_5_DSP_JRSTB    6	
#define HAL_GPIO_5_EINT5    7	
#define HAL_GPIO_5_DEBUGMON19    8

#define HAL_GPIO_6_GPIO6    0	
#define HAL_GPIO_6_SPI_MST2_MOSI    1	
#define HAL_GPIO_6_UART0_TXD    2	
#define HAL_GPIO_6_I2C0_SDA    3	
#define HAL_GPIO_6_AUXADC0    4	
#define HAL_GPIO_6_CONN_AICE_TCKC    5	
#define HAL_GPIO_6_DSP_JTDO    6	
#define HAL_GPIO_6_EINT6    7	
#define HAL_GPIO_6_DEBUGMON18    8

#define HAL_GPIO_7_GPIO7    0	
#define HAL_GPIO_7_I2C0_SCL    1	
#define HAL_GPIO_7_CONN_AICE_TMSC    2	
#define HAL_GPIO_7_PAD_DFUNC_SIN    3	
#define HAL_GPIO_7_PWM0    4	
#define HAL_GPIO_7_I2C_AO_SCL    5	
#define HAL_GPIO_7_DMIC1_CLK    6	
#define HAL_GPIO_7_EINT7    7	
#define HAL_GPIO_7_DEBUGMON12    8

#define HAL_GPIO_8_GPIO8    0	
#define HAL_GPIO_8_I2C0_SDA    1	
#define HAL_GPIO_8_CONN_AICE_TCKC    2	
#define HAL_GPIO_8_PAD_DFUNC_SFCK    3	
#define HAL_GPIO_8_PWM1    4	
#define HAL_GPIO_8_I2C_AO_SDA    5	
#define HAL_GPIO_8_DMIC1_DAT    6	
#define HAL_GPIO_8_EINT8    7	
#define HAL_GPIO_8_DEBUGMON11    8

#define HAL_GPIO_9_GPIO9    0	
#define HAL_GPIO_9_I2C1_SCL    1	
#define HAL_GPIO_9_UART1_RXD    2	
#define HAL_GPIO_9_DMIC1_CLK    3	
#define HAL_GPIO_9_PWM2    4	
#define HAL_GPIO_9_CLKO2    5	
#define HAL_GPIO_9_I2C_AO_SCL    6	
#define HAL_GPIO_9_EINT9    7	
#define HAL_GPIO_9_DEBUGMON10    8

#define HAL_GPIO_10_GPIO10    0	
#define HAL_GPIO_10_I2C1_SDA    1	
#define HAL_GPIO_10_UART1_TXD    2	
#define HAL_GPIO_10_DMIC1_DAT    3	
#define HAL_GPIO_10_RESETB_OUT    4	
#define HAL_GPIO_10_CLKO3    5	
#define HAL_GPIO_10_I2C_AO_SDA    6	
#define HAL_GPIO_10_EINT10    7	
#define HAL_GPIO_10_DEBUGMON9    8

#define HAL_GPIO_11_GPIO11    0	
#define HAL_GPIO_11_I2C2_SCL    1	
#define HAL_GPIO_11_DMIC0_CLK    2	
#define HAL_GPIO_11_UART1_CTS    3	
#define HAL_GPIO_11_I2C_AO_SCL    4	
#define HAL_GPIO_11_UART0_CTS    5	
#define HAL_GPIO_11_BTPRI    6	
#define HAL_GPIO_11_EINT11    7	
#define HAL_GPIO_11_DEBUGMON8    8

#define HAL_GPIO_12_GPIO12    0	
#define HAL_GPIO_12_I2C2_SDA    1	
#define HAL_GPIO_12_DMIC0_DAT    2	
#define HAL_GPIO_12_UART1_RTS    3	
#define HAL_GPIO_12_I2C_AO_SDA    4	
#define HAL_GPIO_12_UART0_RTS    5	
#define HAL_GPIO_12_DSP_JTMS    6	
#define HAL_GPIO_12_EINT12    7	
#define HAL_GPIO_12_DEBUGMON7    8

#define HAL_GPIO_13_GPIO13    0		
#define HAL_GPIO_13_DSP_JTDO    2		
#define HAL_GPIO_13_AP_JTDO    4		
#define HAL_GPIO_13_BTPRI    6	
#define HAL_GPIO_13_EINT13    7	
#define HAL_GPIO_13_DEBUGMON0    8

#define HAL_GPIO_14_GPIO14    0	
#define HAL_GPIO_14_I2S_MST0_MCLK    1		
#define HAL_GPIO_14_SPI_MST1_SIO2    3	
#define HAL_GPIO_14_CONN_TDI    4	
#define HAL_GPIO_14_XRST    5		
#define HAL_GPIO_14_EINT14    7	
#define HAL_GPIO_14_DEBUGMON13    8

#define HAL_GPIO_15_GPIO15    0	
#define HAL_GPIO_15_I2S_MST0_TX    1	
#define HAL_GPIO_15_I2S_SLV0_TX    2	
#define HAL_GPIO_15_CLKO0    3	
#define HAL_GPIO_15_CONN_DBGACK_N    4	
#define HAL_GPIO_15_ENB    5		
#define HAL_GPIO_15_EINT15    7	
#define HAL_GPIO_15_DEBUGMON14    8

#define HAL_GPIO_16_GPIO16    0	
#define HAL_GPIO_16_I2S_MST0_CK    1	
#define HAL_GPIO_16_I2S_SLV0_CK    2	
#define HAL_GPIO_16_SPI_MST1_SIO3    3	
#define HAL_GPIO_16_CONN_DBGI_N    4	
#define HAL_GPIO_16_B0    5		
#define HAL_GPIO_16_EINT16    7	
#define HAL_GPIO_16_DEBUGMON15    8

#define HAL_GPIO_17_GPIO17    0	
#define HAL_GPIO_17_I2S_MST0_WS    1	
#define HAL_GPIO_17_I2S_SLV0_WS    2	
#define HAL_GPIO_17_MSDC0_RST    3	
#define HAL_GPIO_17_CONN_TCK    4	
#define HAL_GPIO_17_R1    5		
#define HAL_GPIO_17_EINT17    7	
#define HAL_GPIO_17_DEBUGMON16    8

#define HAL_GPIO_18_GPIO18    0	
#define HAL_GPIO_18_I2S_MST1_MCLK    1		
#define HAL_GPIO_18_SPI_MST1_SCK    3	
#define HAL_GPIO_18_CONN_TDO    4	
#define HAL_GPIO_18_B1    5		
#define HAL_GPIO_18_EINT18    7	
#define HAL_GPIO_18_DEBUGMON17    8

#define HAL_GPIO_19_GPIO19    0	
#define HAL_GPIO_19_I2S_MST0_RX    1	
#define HAL_GPIO_19_I2S_SLV0_RX    2	
#define HAL_GPIO_19_CLKO1    3	
#define HAL_GPIO_19_CONN_TRST_B    4	
#define HAL_GPIO_19_G0    5		
#define HAL_GPIO_19_EINT19    7	
#define HAL_GPIO_19_DEBUGMON18    8

#define HAL_GPIO_20_GPIO20    0	
#define HAL_GPIO_20_I2S_MST1_RX    1	
#define HAL_GPIO_20_I2S_SLV1_RX    2	
#define HAL_GPIO_20_CLKO2    3	
#define HAL_GPIO_20_CONN_TMS    4	
#define HAL_GPIO_20_G1    5		
#define HAL_GPIO_20_EINT20    7	
#define HAL_GPIO_20_DEBUGMON19    8

#define HAL_GPIO_21_GPIO21    0	
#define HAL_GPIO_21_I2S_MST1_WS    1	
#define HAL_GPIO_21_I2S_SLV1_WS    2	
#define HAL_GPIO_21_SPI_MST1_CS    3	
#define HAL_GPIO_21_UART2_RXD    4	
#define HAL_GPIO_21_R0    5		
#define HAL_GPIO_21_EINT21    7	
#define HAL_GPIO_21_DEBUGMON1    8

#define HAL_GPIO_22_GPIO22    0	
#define HAL_GPIO_22_I2S_MST1_CK    1	
#define HAL_GPIO_22_I2S_SLV1_CK    2	
#define HAL_GPIO_22_SPI_MST1_MOSI    3	
#define HAL_GPIO_22_UART2_TXD    4			
#define HAL_GPIO_22_EINT22    7	
#define HAL_GPIO_22_DEBUGMON2    8

#define HAL_GPIO_23_GPIO23    0	
#define HAL_GPIO_23_I2S_MST1_TX    1	
#define HAL_GPIO_23_I2S_SLV1_TX    2	
#define HAL_GPIO_23_SPI_MST1_MISO    3	
#define HAL_GPIO_23_SRCLKENAI_FREF    4			
#define HAL_GPIO_23_EINT23    7	
#define HAL_GPIO_23_DEBUGMON3    8

#define HAL_GPIO_24_GPIO24    0	
#define HAL_GPIO_24_SPI_MST0_SCK    1	
#define HAL_GPIO_24_SPI_SLV0_SCK    2	
#define HAL_GPIO_24_MSDC0_CMD    3	
#define HAL_GPIO_24_I2C0_SCL    4	
#define HAL_GPIO_24_AUDIO_EXT_SYNC_EN    5		
#define HAL_GPIO_24_EINT24    7	
#define HAL_GPIO_24_DEBUGMON4    8

#define HAL_GPIO_25_GPIO25    0	
#define HAL_GPIO_25_SPI_MST0_CS    1	
#define HAL_GPIO_25_SPI_SLV0_CS    2	
#define HAL_GPIO_25_MSDC0_DAT3    3	
#define HAL_GPIO_25_I2C0_SDA    4	
#define HAL_GPIO_25_BTPRI    5		
#define HAL_GPIO_25_EINT25    7	
#define HAL_GPIO_25_DEBUGMON5    8

#define HAL_GPIO_26_GPIO26    0	
#define HAL_GPIO_26_SPI_MST0_MISO    1	
#define HAL_GPIO_26_SPI_SLV0_MISO    2	
#define HAL_GPIO_26_MSDC0_DAT2    3	
#define HAL_GPIO_26_I2C1_SCL    4	
#define HAL_GPIO_26_WIFITOBT    5		
#define HAL_GPIO_26_EINT26    7	
#define HAL_GPIO_26_DEBUGMON6    8

#define HAL_GPIO_27_GPIO27    0	
#define HAL_GPIO_27_SPI_MST0_MOSI    1	
#define HAL_GPIO_27_SPI_SLV0_MOSI    2	
#define HAL_GPIO_27_MSDC0_DAT1    3	
#define HAL_GPIO_27_I2C1_SDA    4			
#define HAL_GPIO_27_EINT27    7	
#define HAL_GPIO_27_DEBUGMON7    8

#define HAL_GPIO_28_GPIO28    0	
#define HAL_GPIO_28_SPI_MST0_SIO2    1	
#define HAL_GPIO_28_SPI_SLV0_SIO2    2	
#define HAL_GPIO_28_MSDC0_DAT0    3	
#define HAL_GPIO_28_UART0_TXD    4			
#define HAL_GPIO_28_EINT28    7	
#define HAL_GPIO_28_DEBUGMON8    8

#define HAL_GPIO_29_GPIO29    0	
#define HAL_GPIO_29_SPI_MST0_SIO3    1	
#define HAL_GPIO_29_SPI_SLV0_SIO3    2	
#define HAL_GPIO_29_MSDC0_CLK    3	
#define HAL_GPIO_29_UART0_RXD    4			
#define HAL_GPIO_29_EINT29    7	
#define HAL_GPIO_29_DEBUGMON9    8

#define HAL_GPIO_30_GPIO30    0	
#define HAL_GPIO_30_CLKO0    1	
#define HAL_GPIO_30_CTP1    2	
#define HAL_GPIO_30_MSDC0_RST    3	
#define HAL_GPIO_30_DMIC1_CLK    4	
#define HAL_GPIO_30_DMIC0_CLK    5	
#define HAL_GPIO_30_BTPRI    6	
#define HAL_GPIO_30_EINT30    7	
#define HAL_GPIO_30_DEBUGMON10    8

#define HAL_GPIO_31_GPIO31    0	
#define HAL_GPIO_31_SPI_MST0_SIO2    1	
#define HAL_GPIO_31_CTP2    2	
#define HAL_GPIO_31_WIFITOBT    3	
#define HAL_GPIO_31_DMIC1_DAT    4	
#define HAL_GPIO_31_DMIC0_DAT    5		
#define HAL_GPIO_31_EINT31    7	
#define HAL_GPIO_31_DEBUGMON11    8

#define HAL_GPIO_32_GPIO32    0	
#define HAL_GPIO_32_SPI_MST0_MOSI    1	
#define HAL_GPIO_32_CTP3    2	
#define HAL_GPIO_32_AP_JTDI    3	
#define HAL_GPIO_32_PWM0    4	
#define HAL_GPIO_32_DSP_JTMS    5		
#define HAL_GPIO_32_EINT32    7	
#define HAL_GPIO_32_DEBUGMON12    8

#define HAL_GPIO_33_GPIO33    0	
#define HAL_GPIO_33_SPI_MST0_MISO    1	
#define HAL_GPIO_33_CTP4    2	
#define HAL_GPIO_33_AP_JRSTB    3	
#define HAL_GPIO_33_PWM1    4	
#define HAL_GPIO_33_DSP_JTCK    5		
#define HAL_GPIO_33_EINT33    7	
#define HAL_GPIO_33_DEBUGMON13    8

#define HAL_GPIO_34_GPIO34    0	
#define HAL_GPIO_34_SPI_MST0_CS    1	
#define HAL_GPIO_34_CTP5    2	
#define HAL_GPIO_34_AP_JTDO    3	
#define HAL_GPIO_34_PWM2    4	
#define HAL_GPIO_34_DSP_JTDI    5		
#define HAL_GPIO_34_EINT34    7	
#define HAL_GPIO_34_DEBUGMON14    8

#define HAL_GPIO_35_GPIO35    0	
#define HAL_GPIO_35_SPI_MST0_SCK    1	
#define HAL_GPIO_35_CTP6    2	
#define HAL_GPIO_35_AP_JTMS    3	
#define HAL_GPIO_35_PWM3    4	
#define HAL_GPIO_35_DSP_JRSTB    5		
#define HAL_GPIO_35_EINT35    7	
#define HAL_GPIO_35_DEBUGMON15    8

#define HAL_GPIO_36_GPIO36    0	
#define HAL_GPIO_36_SPI_MST0_SIO3    1	
#define HAL_GPIO_36_CTP7    2	
#define HAL_GPIO_36_AP_JTCK    3	
#define HAL_GPIO_36_PWM4    4	
#define HAL_GPIO_36_DSP_JTDO    5		
#define HAL_GPIO_36_EINT36    7	
#define HAL_GPIO_36_DEBUGMON16    8

#define HAL_GPIO_37_GPIO37    0	
#define HAL_GPIO_37_I2S_MST2_MCLK    1		
#define HAL_GPIO_37_SRCLKENAI_FREF    3				
#define HAL_GPIO_37_EINT37    7	
#define HAL_GPIO_37_DEBUGMON17    8

#define HAL_GPIO_38_GPIO38    0	
#define HAL_GPIO_38_I2S_MST2_TX    1	
#define HAL_GPIO_38_I2S_SLV2_TX    2					
#define HAL_GPIO_38_EINT38    7	
#define HAL_GPIO_38_DEBUGMON18    8

#define HAL_GPIO_39_GPIO39    0	
#define HAL_GPIO_39_I2S_MST2_CK    1	
#define HAL_GPIO_39_I2S_SLV2_CK    2					
#define HAL_GPIO_39_EINT39    7	
#define HAL_GPIO_39_DEBUGMON19    8

#define HAL_GPIO_40_GPIO40    0	
#define HAL_GPIO_40_I2S_MST2_RX    1	
#define HAL_GPIO_40_I2S_SLV2_RX    2					
#define HAL_GPIO_40_EINT40    7	
#define HAL_GPIO_40_DEBUGMON0    8

#define HAL_GPIO_41_GPIO41    0	
#define HAL_GPIO_41_I2S_MST2_WS    1	
#define HAL_GPIO_41_I2S_SLV2_WS    2					
#define HAL_GPIO_41_EINT41    7	
#define HAL_GPIO_41_DEBUGMON1    8

#define HAL_GPIO_42_GPIO42    0	
#define HAL_GPIO_42_I2C0_SCL    1	
#define HAL_GPIO_42_DMIC1_CLK    2					
#define HAL_GPIO_42_EINT42    7	
#define HAL_GPIO_42_DEBUGMON2    8

#define HAL_GPIO_43_GPIO43    0	
#define HAL_GPIO_43_I2C0_SDA    1	
#define HAL_GPIO_43_DMIC1_DAT    2					
#define HAL_GPIO_43_EINT43    7	
#define HAL_GPIO_43_DEBUGMON3    8

#define HAL_GPIO_44_GPIO44    0	
#define HAL_GPIO_44_PWM5    1	
#define HAL_GPIO_44_BTPRI    2			
#define HAL_GPIO_44_CLKO1    5		
#define HAL_GPIO_44_EINT44    7	
#define HAL_GPIO_44_DEBUGMON4    8

#define HAL_GPIO_45_GPIO45    0	
#define HAL_GPIO_45_PWM6    1	
#define HAL_GPIO_45_WIFITOBT    2			
#define HAL_GPIO_45_CLKO2    5		
#define HAL_GPIO_45_EINT45    7	
#define HAL_GPIO_45_DEBUGMON5    8

#define HAL_GPIO_46_GPIO46    0	
#define HAL_GPIO_46_PWM7    1	
#define HAL_GPIO_46_AP_JTDI    2	
#define HAL_GPIO_46_DSP_JTDI    3		
#define HAL_GPIO_46_CLKO3    5		
#define HAL_GPIO_46_EINT46    7	
#define HAL_GPIO_46_DEBUGMON6    8

#define HAL_GPIO_47_GPIO47    0	
#define HAL_GPIO_47_PWM8    1	
#define HAL_GPIO_47_AP_JRSTB    2	
#define HAL_GPIO_47_DSP_JRSTB    3				
#define HAL_GPIO_47_EINT47    7	
#define HAL_GPIO_47_DEBUGMON7    8

#define HAL_GPIO_48_GPIO48    0	
#define HAL_GPIO_48_PWM9    1	
#define HAL_GPIO_48_AP_JTDO    2	
#define HAL_GPIO_48_DSP_JTDO    3				
#define HAL_GPIO_48_EINT48    7	
#define HAL_GPIO_48_DEBUGMON8    8

#define HAL_GPIO_49_GPIO49    0	
#define HAL_GPIO_49_UART2_RXD    1	
#define HAL_GPIO_49_I2C2_SCL    2					
#define HAL_GPIO_49_EINT49    7	
#define HAL_GPIO_49_DEBUGMON9    8

#define HAL_GPIO_50_GPIO50    0	
#define HAL_GPIO_50_UART2_TXD    1	
#define HAL_GPIO_50_I2C2_SDA    2					
#define HAL_GPIO_50_EINT50    7	
#define HAL_GPIO_50_DEBUGMON10    8

#define HAL_GPIO_51_GPIO51    0		
#define HAL_GPIO_51_DSP_JTDI    2		
#define HAL_GPIO_51_AP_JTDI    4	
#define HAL_GPIO_51_WIFITOBT    5		
#define HAL_GPIO_51_EINT51    7	
#define HAL_GPIO_51_DEBUGMON11    8

#define HAL_GPIO_52_GPIO52    0	
#define HAL_GPIO_52_I2C2_SCL    1						
#define HAL_GPIO_52_EINT52    7	
#define HAL_GPIO_52_DEBUGMON12    8

#define HAL_GPIO_53_GPIO53    0	
#define HAL_GPIO_53_I2C2_SDA    1						
#define HAL_GPIO_53_EINT53    7	
#define HAL_GPIO_53_DEBUGMON13    8

#define HAL_GPIO_54_GPIO54    0		
#define HAL_GPIO_54_DSP_JRSTB    2		
#define HAL_GPIO_54_AP_JRSTB    4	
#define HAL_GPIO_54_BTPRI    5		
#define HAL_GPIO_54_EINT54    7	
#define HAL_GPIO_54_DEBUGMON14    8

#define HAL_GPIO_55_GPIO55    0	
#define HAL_GPIO_55_RESETB_OUT    1						
#define HAL_GPIO_55_EINT55    7	
#define HAL_GPIO_55_DEBUGMON15    8

#define HAL_GPIO_56_GPIO56    0	
#define HAL_GPIO_56_I2S_MST3_CK    1	
#define HAL_GPIO_56_I2S_SLV3_CK    2			
#define HAL_GPIO_56_HCK    5		
#define HAL_GPIO_56_EINT56    7	
#define HAL_GPIO_56_DEBUGMON16    8

#define HAL_GPIO_57_GPIO57    0	
#define HAL_GPIO_57_I2S_MST3_RX    1	
#define HAL_GPIO_57_I2S_SLV3_RX    2			
#define HAL_GPIO_57_HST    5		
#define HAL_GPIO_57_EINT57    7	
#define HAL_GPIO_57_DEBUGMON17    8

#define HAL_GPIO_58_GPIO58    0	
#define HAL_GPIO_58_I2S_MST3_WS    1	
#define HAL_GPIO_58_I2S_SLV3_WS    2			
#define HAL_GPIO_58_VCK    5		
#define HAL_GPIO_58_EINT58    7	
#define HAL_GPIO_58_DEBUGMON18    8

#define HAL_GPIO_59_GPIO59    0	
#define HAL_GPIO_59_I2S_MST3_TX    1	
#define HAL_GPIO_59_I2S_SLV3_TX    2			
#define HAL_GPIO_59_VST    5		
#define HAL_GPIO_59_EINT59    7	
#define HAL_GPIO_59_DEBUGMON19    8

#define HAL_GPIO_60_GPIO60    0	
#define HAL_GPIO_60_I2S_MST3_MCLK    1						
#define HAL_GPIO_60_EINT60    7	
#define HAL_GPIO_60_DEBUGMON0    8

#define HAL_GPIO_61_GPIO61    0		
#define HAL_GPIO_61_DSP_JTMS    2		
#define HAL_GPIO_61_AP_JTMS    4			
#define HAL_GPIO_61_EINT61    7	
#define HAL_GPIO_61_DEBUGMON1    8

#define HAL_GPIO_62_GPIO62    0	
#define HAL_GPIO_62_UART1_RXD    1						
#define HAL_GPIO_62_EINT62    7	
#define HAL_GPIO_62_DEBUGMON2    8

#define HAL_GPIO_63_GPIO63    0	
#define HAL_GPIO_63_UART1_TXD    1						
#define HAL_GPIO_63_EINT63    7	
#define HAL_GPIO_63_DEBUGMON3    8

#define HAL_GPIO_64_GPIO64    0	
#define HAL_GPIO_64_UART0_RTS    1	
#define HAL_GPIO_64_AP_JTMS    2	
#define HAL_GPIO_64_DSP_JTMS    3	
#define HAL_GPIO_64_UART1_CTS    4			
#define HAL_GPIO_64_EINT64    7	
#define HAL_GPIO_64_DEBUGMON4    8

#define HAL_GPIO_65_GPIO65    0	
#define HAL_GPIO_65_UART0_CTS    1	
#define HAL_GPIO_65_AP_JTCK    2	
#define HAL_GPIO_65_DSP_JTCK    3	
#define HAL_GPIO_65_UART1_RTS    4			
#define HAL_GPIO_65_EINT65    7	
#define HAL_GPIO_65_DEBUGMON5    8

#define HAL_GPIO_66_GPIO66    0	
#define HAL_GPIO_66_SPI_MST1_SIO2    1	
#define HAL_GPIO_66_DSP_JTCK    2		
#define HAL_GPIO_66_AP_JTCK    4			
#define HAL_GPIO_66_EINT66    7	
#define HAL_GPIO_66_DEBUGMON6    8

#define HAL_GPIO_67_GPIO67    0	
#define HAL_GPIO_67_SPI_MST1_CS    1						
#define HAL_GPIO_67_EINT67    7	
#define HAL_GPIO_67_DEBUGMON7    8

#define HAL_GPIO_68_GPIO68    0	
#define HAL_GPIO_68_SPI_MST1_MISO    1						
#define HAL_GPIO_68_EINT68    7	
#define HAL_GPIO_68_DEBUGMON8    8

#define HAL_GPIO_69_GPIO69    0	
#define HAL_GPIO_69_SPI_MST1_MOSI    1						
#define HAL_GPIO_69_EINT69    7	
#define HAL_GPIO_69_DEBUGMON9    8

#define HAL_GPIO_70_GPIO70    0	
#define HAL_GPIO_70_SPI_MST1_SCK    1						
#define HAL_GPIO_70_EINT70    7	
#define HAL_GPIO_70_DEBUGMON10    8

#define HAL_GPIO_71_GPIO71    0	
#define HAL_GPIO_71_SPI_MST1_SIO3    1	
#define HAL_GPIO_71_IRRX    2					
#define HAL_GPIO_71_EINT71    7	
#define HAL_GPIO_71_DEBUGMON11    8

#define HAL_GPIO_72_GPIO72    0	
#define HAL_GPIO_72_SPI_MST2_SIO3    1			
#define HAL_GPIO_72_AUXADC7    4			
#define HAL_GPIO_72_EINT72    7	
#define HAL_GPIO_72_DEBUGMON12    8

#define HAL_GPIO_73_GPIO73    0	
#define HAL_GPIO_73_MSDC0_DAT0    1	
#define HAL_GPIO_73_SPI_MST1_SIO3    2	
#define HAL_GPIO_73_UART2_RXD    3	
#define HAL_GPIO_73_DMIC0_CLK    4	
#define HAL_GPIO_73_AP_JTMS    5	
#define HAL_GPIO_73_PAD_DFUNC_SFCS0    6	
#define HAL_GPIO_73_EINT73    7	
#define HAL_GPIO_73_DEBUGMON6    8

#define HAL_GPIO_74_GPIO74    0	
#define HAL_GPIO_74_MSDC0_DAT1    1	
#define HAL_GPIO_74_SPI_MST1_MISO    2	
#define HAL_GPIO_74_I2S_MST1_RX    3	
#define HAL_GPIO_74_I2S_SLV1_RX    4	
#define HAL_GPIO_74_AP_JTCK    5		
#define HAL_GPIO_74_EINT74    7	
#define HAL_GPIO_74_DEBUGMON5    8

#define HAL_GPIO_75_GPIO75    0	
#define HAL_GPIO_75_MSDC0_RST    1	
#define HAL_GPIO_75_SPI_MST1_MOSI    2	
#define HAL_GPIO_75_I2S_MST1_TX    3	
#define HAL_GPIO_75_I2S_SLV1_TX    4	
#define HAL_GPIO_75_CLKO2    5		
#define HAL_GPIO_75_EINT75    7	
#define HAL_GPIO_75_DEBUGMON4    8

#define HAL_GPIO_76_GPIO76    0	
#define HAL_GPIO_76_MSDC0_CLK    1	
#define HAL_GPIO_76_CLKO0    2	
#define HAL_GPIO_76_I2S_MST1_MCLK    3		
#define HAL_GPIO_76_AP_JTDO    5	
#define HAL_GPIO_76_DSP_JTDO    6	
#define HAL_GPIO_76_EINT76    7	
#define HAL_GPIO_76_DEBUGMON3    8

#define HAL_GPIO_77_GPIO77    0	
#define HAL_GPIO_77_MSDC0_CMD    1	
#define HAL_GPIO_77_SPI_MST1_CS    2	
#define HAL_GPIO_77_I2S_MST1_WS    3	
#define HAL_GPIO_77_I2S_SLV1_WS    4	
#define HAL_GPIO_77_AP_JRSTB    5	
#define HAL_GPIO_77_DSP_JTDI    6	
#define HAL_GPIO_77_EINT77    7	
#define HAL_GPIO_77_DEBUGMON2    8

#define HAL_GPIO_78_GPIO78    0	
#define HAL_GPIO_78_MSDC0_DAT3    1	
#define HAL_GPIO_78_SPI_MST1_SCK    2	
#define HAL_GPIO_78_UART2_TXD    3	
#define HAL_GPIO_78_DMIC0_DAT    4	
#define HAL_GPIO_78_AP_JTDI    5	
#define HAL_GPIO_78_DSP_JRSTB    6	
#define HAL_GPIO_78_EINT78    7	
#define HAL_GPIO_78_DEBUGMON1    8

#define HAL_GPIO_79_GPIO79    0	
#define HAL_GPIO_79_MSDC0_DAT2    1	
#define HAL_GPIO_79_SPI_MST1_SIO2    2	
#define HAL_GPIO_79_I2S_MST1_CK    3	
#define HAL_GPIO_79_I2S_SLV1_CK    4	
#define HAL_GPIO_79_CLKO3    5	
#define HAL_GPIO_79_DSP_JTCK    6	
#define HAL_GPIO_79_EINT79    7	
#define HAL_GPIO_79_DEBUGMON0    8

#define HAL_GPIO_80_GPIO80    0	
#define HAL_GPIO_80_SPI_MST2_MOSI    1						
#define HAL_GPIO_80_EINT80    7	
#define HAL_GPIO_80_DEBUGMON13    8

#define HAL_GPIO_81_GPIO81    0	
#define HAL_GPIO_81_SPI_MST2_SIO2    1						
#define HAL_GPIO_81_EINT81    7	
#define HAL_GPIO_81_DEBUGMON14    8

#define HAL_GPIO_82_GPIO82    0	
#define HAL_GPIO_82_SPI_MST2_SCK    1						
#define HAL_GPIO_82_EINT82    7	
#define HAL_GPIO_82_DEBUGMON15    8

#define HAL_GPIO_83_GPIO83    0	
#define HAL_GPIO_83_SPI_MST2_MISO    1						
#define HAL_GPIO_83_EINT83    7	
#define HAL_GPIO_83_DEBUGMON16    8

#define HAL_GPIO_84_GPIO84    0	
#define HAL_GPIO_84_SPI_MST2_SIO3    1						
#define HAL_GPIO_84_EINT84    7	
#define HAL_GPIO_84_DEBUGMON17    8

#define HAL_GPIO_85_GPIO85    0	
#define HAL_GPIO_85_SPI_MST2_CS    1						
#define HAL_GPIO_85_EINT85    7	
#define HAL_GPIO_85_DEBUGMON18    8

#ifdef __cplusplus
}
#endif

#endif /*HAL_GPIO_MODULE_ENABLED*/

#endif /*__HAL_PINMUX_DEFINE_H__*/

