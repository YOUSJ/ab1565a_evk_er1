/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#include "hal_platform.h"
#ifndef __HAL_PMU_H__
#define __HAL_PMU_H__
#ifdef HAL_PMU_MODULE_ENABLED

#define PMIC_SLAVE_ADDR            0x6B
#define INVALID_INTERRUPT_VALUE    0xFF
#define PMU_PRESS_PK_TIME 300000  //300ms
#define PMU_VCORE_CP     0xA20A0204
#define PMU_ELR_0 0xA2070508
#define APB_PMU 0xA20A0220
//#define HAL_PMU_DEBUG_ENABLE

typedef enum{
    PMU_LOCK=0,
    PMU_UNLOCK,
    PMU_TEMP
}pmu_lock_parameter_t;

typedef enum {
    PMU_NONE          = 0,                 /**< NONE Trigger */
    PMU_EDGE_RISING   = 1,                 /**< edge and rising trigger */
    PMU_EDGE_FALLING  = 2,                 /**< edge and falling trigger */
    PMU_EDGE_FALLING_AND_RISING = 3        /**< edge and falling or rising trigger */
} pmu_int_trigger_mode_t;

typedef enum {
    PMU_STATUS_INVALID_PARAMETER  = -1,     /**< pmu error invalid parameter */
    PMU_STATUS_ERROR              = 0,     /**< pmu undefined error */
    PMU_STATUS_SUCCESS            = 1       /**< pmu function ok */
}pmu_status_t;

typedef enum {
    PMU_ERROR   = 0,
    PMU_OK    = 1
} pmu_operate_status_t;

typedef enum {
    PMU_OFF    = 0,
    PMU_ON   = 1,
} pmu_power_operate_t;

typedef enum {
    PMU_PK_PRESS    = 0,
    PMU_PK_RELEASE   = 1,
} pmu_pk_operate_t;

typedef enum {
    PMU_SW_MODE,
    PMU_HW_MODE,
} pmu_control_mode_t;

typedef enum {
    PMU_DURATION_5S,
    PMU_DURATION_8S,
    PMU_DURATION_11S,
    PMU_DURATION_25S,
} pmu_pwrkey_time_t;

typedef enum {
    PMU_DEBOUNCE_PWRKEY,
    PMU_RELEASE_PWRKEY,
    PMU_REPRESS_PWRKEY,
    PMU_RESET_DEFAULT,
} pmu_pwrkey_scenario_t;

typedef enum {
    PMU_PWROFF,
    PMU_RTC,
    PMU_SLEEP,
    PMU_NORMAL,
    PMU_DVS,
} pmu_power_stage_t;

typedef enum {
    PMIC_VAUD18_0P8_V,
    PMIC_VAUD18_0P9_V,
    PMIC_VAUD18_0P85_V,
} pmu_power_vaud18_voltage_t;

typedef enum {
       PMIC_VCORE_0P7_V,
       PMIC_VCORE_0P9_V,
       PMIC_VCORE_0P97_V,
       PMIC_VCORE_1P0_V,
       PMIC_VCORE_1P1_V,
       PMIC_VCORE_1P2_V,
       PMIC_VCORE_1P3_V,
       PMIC_VCORE_1P4_V,
       PMIC_VCORE_FAIL_V,
} pmu_power_vcore_voltage_t;

typedef enum {
    PMU_BUCK_VCORE,     //0.7~13V
    PMU_BUCK_VIO18,     //1.8V
    PMU_BUCK_VRF,       //1.45V/2.2V
    PMU_BUCK_VAUD18,    //0.9V/1.8V
    PMU_LDO_VA18,       //1.8V
    PMU_LDO_VLDO33,     //3.3V
} pmu_power_domain_t;

typedef enum {
RG_INT_PWRKEY,      //INT_CON0 index :0
RG_INT_PWRKEY_R,
RG_INT_AD_LBAT_LV,
RG_INT_THM_OVER40,
RG_INT_THM_OVER55,
RG_INT_THM_OVER110,
RG_INT_THM_OVER125,
RG_INT_THM_UNDER40,
RG_INT_THM_UNDER55,
RG_INT_THM_UNDER110,
RG_INT_THM_UNDER125,
RG_INT_bat2_h_r,         //INT_CON1 index :11
RG_INT_VLDO33_LBAT_DET,
RG_INT_VBAT_RECHG,
RG_INT_bat_h_lv,
RG_INT_bat_l_lv,
RG_INT_thr_h_r,
RG_INT_thr_h_f,
RG_INT_thr_l_r,
RG_INT_thr_l_f,
RG_INT_JEITA_HOT,
RG_INT_JEITA_WARM,
RG_INT_JEITA_COOL,
RG_INT_JEITA_COLD,
RG_INT_VCORE_OC,            //INT_CON2 index : 24
RG_INT_VIO18_OC,
RG_INT_VRF_OC,
RG_INT_VAUD18_OC,
RG_INT_VLDO33_OC,
RG_INT_VA18_OC,
RG_INT_BATOV,            //INT_CON3 index : 30
RG_INT_CHRDET,
RG_INT_ChgStatInt,
RG_INT_ILimInt,
RG_INT_ThermRegInt,
RG_INT_VBUS_OVP,
RG_INT_VBUS_UVLO,
RG_INT_ICHR_ITERM,
RG_INT_SAFETY_TIMEOUT,
RG_INT_VSYS_DPM,
PMU_INT_MAX,
} pmu_interrupt_index_t;

enum
{
    PMU_NOT_INIT,
    PMU_INIT,
};

typedef void (*pmu_callback_t)(void);

typedef struct 
{
    void (*pmu_callback)(void);
    void *user_data;
    bool init_status;
	bool isMask;
} pmu_function_t;

typedef struct
{
    pmu_callback_t callback1; //press callback
    void *user_data1;
    pmu_callback_t callback2; //release callback
    void *user_data2;
} pmu_pwrkey_config_t;

void pmu_init_6388(void);
/*
 * [4]   STS_RBOOT  :Power on for cold reset
 * [3]   STS_SPAR   :reserved
 * [2]   STS_CHRIN  :Power on for charger insertion
 * [1]   STS_RTCA   :Power on for RTC alarm
 * [0]   STS_PWRKEY :Power on for PWREKY press
 *
 */
void pmu_get_all_int_status(void);
pmu_power_vcore_voltage_t pmu_vcore_lock_control(pmu_power_stage_t mode,pmu_power_vcore_voltage_t vol,pmu_lock_parameter_t lock);
pmu_operate_status_t pmu_set_register_value_mt6388(uint32_t address, uint32_t mask, uint32_t shift, uint32_t value);
uint32_t pmu_get_register_value_mt6388(uint32_t address, uint32_t mask, uint32_t shift);
void pmu_control_power_6388(pmu_power_domain_t pmu_pdm, pmu_power_operate_t operate);
pmu_status_t pmu_control_enable_interrupt(pmu_interrupt_index_t int_channel, int isEnable);
pmu_status_t pmu_control_mask_interrupt(pmu_interrupt_index_t int_channel, int isEnable);
pmu_status_t pmu_control_clear_interrupt(pmu_interrupt_index_t int_channel);
pmu_status_t  pmu_control_clear_interrupt(pmu_interrupt_index_t int_channel);
uint32_t pmu_get_raw_status_interrupt(pmu_interrupt_index_t int_channel);
int pmu_get_status_interrupt(pmu_interrupt_index_t int_channel);
void pmu_charger_interrupt_handler(void);
pmu_status_t pmu_deregister_callback(pmu_interrupt_index_t pmu_int_ch);
pmu_status_t pmu_register_callback(pmu_interrupt_index_t pmu_int_ch, pmu_callback_t callback, void *user_data);
bool pmu_is_charger_exist_init(void);
pmu_status_t pmu_control_mask(pmu_interrupt_index_t pmu_int_ch, bool isMaskInterrupt);
void pmu_control_clear_all_charger_interrupt(void);
pmu_operate_status_t pmu_set_register_value_2byte_mt6388(uint32_t address, uint32_t mask, uint32_t shift, uint32_t value);
uint32_t pmu_get_register_value_2byte_mt6388(uint32_t address, uint32_t mask, uint32_t shift);
pmu_operate_status_t pmu_vcore_voltage_sel_6388(pmu_power_stage_t mode ,pmu_power_vcore_voltage_t vol);
pmu_power_vcore_voltage_t pmu_get_vcore_voltage_mt6388(void);
pmu_operate_status_t pmu_vaud18_voltage_sel_6388(uint8_t vol);
pmu_operate_status_t pmu_vaud18_sleep_voltage_sel_6388(pmu_power_vaud18_voltage_t vol) ;

void pmic_i2c_deinit(void);
void pmic_i2c_init(void);
void pmu_on_mode_switch_6388(pmu_power_domain_t domain, pmu_control_mode_t mode);
void pmu_lp_mode_6388(pmu_power_domain_t domain, pmu_control_mode_t mode);

void pmu_vcroe_voltage_turing(int symbol,int num );
void pmu_vio18_voltage_turing(int symbol,int num );
void pmu_vaud18_voltage_turing(int symbol,int num );

uint32_t pmu_get_register_value_internal(uint32_t address, short int mask, short int shift);
void pmu_set_register_value_internal(uint32_t address, short int mask, short int shift, short int value);
uint8_t pmu_get_lock_status(void);
int pmu_get_lock_index(void);
void pmu_set_usb_input_status(void);
uint8_t pmu_get_usb_input_status(void);
void pmu_power_off_sequence(pmu_power_stage_t stage);
void pmu_srclken_control_mode_6388(pmu_power_operate_t mode);
void pmu_power_enable_6388(pmu_power_domain_t pmu_pdm, pmu_power_operate_t operate);
void pmu_voltage_selet_6388(pmu_power_stage_t mode,pmu_power_domain_t domain,uint32_t vol);
void pmu_audio_low_power_setting(int oper);
void pmu_sw_enter_sleep(pmu_power_domain_t domain);
pmu_operate_status_t pmu_pk_filter(uint8_t pk_sta);
void pmic_i2c_hold(int oper);
/*PWRKEY long pressed reset enable & disable*/
pmu_operate_status_t pmu_pwrkey_enable(pmu_power_operate_t oper);
uint8_t pmu_get_power_status_6388(pmu_power_domain_t pmu_pdm);
void pmu_scan_interrupt_status(void);
void pmu_lock_va18(int oper);
void pmu_irq_init(void);
void pmu_irq_count(int int_channel);
void hal_pmu_sleep_backup(void);
/*Long pressed time to issue reset
 * 2'b00: 5 sec
 * 2'b01: 8 sec
 * 2'b10: 11 sec
 * 2'b11: 25 sec
 * */
pmu_operate_status_t pmu_pwrkey_duration_time(pmu_pwrkey_time_t tmr);

/*Re power-on scenario slection
 * 2'b00: debounce pwrkey
 * 2'b01: after release pwrkey
 * 2'b10: after release pwrkey and press pwrkey again
*/
pmu_operate_status_t pmu_long_press_shutdown_function_sel(pmu_pwrkey_scenario_t oper);
/*
 * Power OFF status output
 * 0  No Power Off Event (first power on)
 * 1  PWRHOLD=0 (RTC mode)
 * 2  UVLO
 * 3  THRDN
 * 5  SW CRST
 * 8  WDT CRST & reboot
 *10  Long press shutdown
 *11  PUPSRC
 *12  KEYPWR  : VIO18/VCORE PG of OC
 *13  SYSRSTB CRST
 *14  VCORE OC
 *15  VIO18 OC
 *16  VAUD18 OC
 *17  VRF OC
 *18  VCORE PG
 *19  VIO18 PG
 *20  VAUD18 PG
 *21  VRF PG
 *22  VA18 PG
 * */
uint8_t pmu_get_power_off_reason(void);
#endif /* HAL_PMU_MODULE_ENABLED */
#endif //__HAL_PMU_H__
