/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#ifndef __HAL_PMU_PLATFORM_H__
#define __HAL_PMU_PLATFORM_H__

#define PMU_D_REG_BASE (0xA2070000)

// PMU Group
#define PMU2_ANA_CON0	(PMU_D_REG_BASE+0x0500)
#define PMU2_ANA_CON1	(PMU_D_REG_BASE+0x0504)
#define PMU2_ELR_0		(PMU_D_REG_BASE+0x0508)
#define PMU2_ANA_RO		(PMU_D_REG_BASE+0x0510)

//Control RG

#define PMU_BC12_IBIAS_EN_V12_ADDR                  PMU2_ANA_CON0
#define PMU_BC12_IBIAS_EN_V12_MASK                  0x1
#define PMU_BC12_IBIAS_EN_V12_SHIFT                 0

#define PMU_BC12_CMP_EN_V12_ADDR                    PMU2_ANA_CON0
#define PMU_BC12_CMP_EN_V12_MASK                    0x3
#define PMU_BC12_CMP_EN_V12_SHIFT                   1

#define PMU_BC12_DCD_EN_V12_ADDR                          PMU2_ANA_CON0
#define PMU_BC12_DCD_EN_V12_MASK                          0x1
#define PMU_BC12_DCD_EN_V12_SHIFT                         3

#define PMU_BC12_IPDC_EN_V12_ADDR                          PMU2_ANA_CON0
#define PMU_BC12_IPDC_EN_V12_MASK                          0x3
#define PMU_BC12_IPDC_EN_V12_SHIFT                         4

#define PMU_BC12_IPD_EN_V12_ADDR                          PMU2_ANA_CON0
#define PMU_BC12_IPD_EN_V12_MASK                          0x3
#define PMU_BC12_IPD_EN_V12_SHIFT                         6

#define PMU_BC12_IPD_HALF_EN_V12_ADDR                       PMU2_ANA_CON0
#define PMU_BC12_IPD_HALF_EN_V12_MASK                       0x1
#define PMU_BC12_IPD_HALF_EN_V12_SHIFT                      8

#define PMU_BC12_IPU_EN_V12_ADDR                          PMU2_ANA_CON0
#define PMU_BC12_IPU_EN_V12_MASK                          0x3
#define PMU_BC12_IPU_EN_V12_SHIFT                         9

#define PMU_BC12_VREF_VTH_EN_V12_ADDR                       PMU2_ANA_CON0
#define PMU_BC12_VREF_VTH_EN_V12_MASK                       0x3
#define PMU_BC12_VREF_VTH_EN_V12_SHIFT                      11

#define PMU_BC12_SRC_EN_V12_ADDR                          PMU2_ANA_CON0
#define PMU_BC12_SRC_EN_V12_MASK                          0x3
#define PMU_BC12_SRC_EN_V12_SHIFT                         13

#define PMU_BC12_IPU_TEST_EN_V12_ADDR                       PMU2_ANA_CON1
#define PMU_BC12_IPU_TEST_EN_V12_MASK                       0x1
#define PMU_BC12_IPU_TEST_EN_V12_SHIFT                      0

#define PMU_BC12_CS_TRIM_V12_ADDR                          PMU2_ELR_0
#define PMU_BC12_CS_TRIM_V12_MASK                          0x3F
#define PMU_BC12_CS_TRIM_V12_SHIFT                         0

#define PMU_AQ_QI_BC12_CMP_OUT_V12_ADDR                       PMU2_ANA_RO
#define PMU_AQ_QI_BC12_CMP_OUT_V12_MASK                       0x1
#define PMU_AQ_QI_BC12_CMP_OUT_V12_SHIFT                      0

#endif
