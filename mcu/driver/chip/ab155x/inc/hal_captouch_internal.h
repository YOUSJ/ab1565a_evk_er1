/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */
 
#ifndef __HAL_CAPTOUCH_INTERNAL_H__
#define __HAL_CAPTOUCH_INTERNAL_H__

#include "hal_captouch.h"
#ifdef HAL_CAPTOUCH_MODULE_ENABLED
#include "ept_keypad_drv.h"

#ifdef __cplusplus
extern "C"
{
#endif

//#define CAPTOUCH_DEBUG_1 


#define cap_reg32(x)    (*(volatile uint32_t*)(x))

#ifdef DRV_KBD_CAPTOUCH_SEL
#define CAPTOUCH_USED_CHANNEL_MASK      DRV_KBD_CAPTOUCH_SEL     /*indicate the used channel by ept*/
#else 
#define CAPTOUCH_USED_CHANNEL_MASK      0xff                     /*indicate the used channel by default*/
#endif

#define CAPTOUCH_BUFFER_SIZE            (64)    /* key position buffer size */

#define CAPTOUCH_FINE_CAP_MAX           (63)   /* the fine cap max value */
#define CAPTOUCH_COARSE_CAP_MAX         (0x7)   /* the coarse cap max value */
#define CAPTOUCH_ADC_MAX                (255)   /* the adc max value */

#define CAPTOUCH_WIRE3_READ             1
#define CAPTOUCH_WIRE3_WRITE            0

#define CAPTOUCH_WIRE3_CLOCK_1_2        0x3     /*1x: The 3-wire clock is 1/2 of host bus clock*/
#define CAPTOUCH_WIRE3_CLOCK_1_4        0x1     /*01: The 3-wire clock is 1/4 of host bus clock*/
#define CAPTOUCH_WIRE3_CLOCK_1_8        0x0     /*00: The 3-wire clock is 1/8 of host bus clock*/

#define CAPTOUCH_ANA_TOUCH_CLK_32K      0x0     /*Touch Analog clock selection:4'b0000: 32K*/
#define CAPTOUCH_ANA_TOUCH_CLK_16K      0x1     /*Touch Analog clock selection:4'b0001: 16K*/
#define CAPTOUCH_ANA_TOUCH_CLK_8K       0x2     /*Touch Analog clock selection:4'b0010: 8K*/
#define CAPTOUCH_ANA_TOUCH_CLK_4K       0x3     /*Touch Analog clock selection:4'b0011: 4K*/
#define CAPTOUCH_ANA_TOUCH_CLK_2K       0x4     /*Touch Analog clock selection:4'b0100: 2K*/
#define CAPTOUCH_ANA_TOUCH_CLK_1K       0x5     /*Touch Analog clock selection:4'b0101: 1K*/

#define TOUCH_CTRL_MAN                  (1<<7)
#define TOUCH_BACK2BACK_EN              (1<<6)
#define TOUCH_CLK_EN                    (1<<5)
#define TOUCH_CAL_AUTOSUSPEND           (1<<3)
#define TOUCH_CAL_EN                    (1<<2)
#define TOUCH_INT_EN                    (1<<1)
#define TOUCH_WAKE_EN                   (1<<0)

#define TOUCH_ADC_EN                    (1<<2)
#define TOUCH_EN                        (1<<1)
#define TOUCH_EN_OP                     (1<<0)

#define TOUCH_PER_CH_GATING_EN          (1<<8)      //Enable per-channel power saving.
#define TOUCH_DA_RST_N                  (1<<7)      //Deactivate reset to analog circuits
#define TOUCH_AD_ISO_ENABLE             (1<<6)
#define TOUCH_EN_LP_NORMAL              (0x2<<4)
#define TOUCH_EN_LP_LOWPOWER            (0x3<<4)
#define TOUCH_EN_OP_LP_NORMAL           (0)
#define TOUCH_EN_OP_LP_LOWPOWER         (0xf)

#define TOUCH_SW_DBG_SEL_MASK           (0x7<<10)

#define captouch_enable                 (true)
#define captouch_disable                (false)

typedef struct {
    hal_captouch_event_t data[CAPTOUCH_BUFFER_SIZE];
    uint32_t write_index;
    uint32_t read_index;
    uint32_t press_count;
} captouch_buffer_t;

typedef struct {
    bool        has_initilized;
    bool        is_running;
    uint32_t    used_channel_map;               
    hal_captouch_callback_context_t captouch_callback;
} captouch_context_t;

typedef struct {
	bool     is_key[8];
	bool     is_rs;
	bool     is_thr[8];
	uint8_t  mavg_r;
	uint8_t  avg_s;
	uint8_t  en_ch_map;
	int16_t  thr_h[8];
	int16_t  thr_l[8];
    uint8_t  coarse_cap[8];                                  
    int16_t  fine_cap[8];
	uint8_t  hw_ch_map;
} hal_captouch_nvdm_data;


typedef struct {
    uint8_t CON[2];
    uint8_t THR_H[8];
    uint8_t THR_L[8];
    uint8_t CAL_OUT_MAN[8];
    uint8_t CH_INT_MASK;
    uint8_t CH_WAKEUP_MASK;
    uint8_t CAL_DBG;
    uint8_t VADC_DBG;
    uint8_t AVG_DBG;
    uint8_t LP_CON;
    uint8_t ANA_CFG[3];
    uint8_t CORSE_CAP_CFG[2];
    uint8_t CAL_CFG[2];
    uint8_t ATST_CONTROL;
} CAPTOUCH_ANALOG_REG_T;


/********* varible extern *************/
extern CAPTOUCH_REGISTER_T *captouch ;
extern captouch_context_t captouch_context;
extern captouch_buffer_t captouch_buffer;

extern const CAPTOUCH_ANALOG_REG_T CAPTOUCH_ANALOG;
extern const uint8_t captouch_mapping_keydata[];

/******** funtion extern **************/
void captouch_control_pdn_clock(bool is_clock_on);
void captouch_analog_write_data(uint8_t waddr, uint16_t wdata);
uint16_t captouch_analog_read_data(uint8_t raddr);
void captouch_register_nvic_callback(void);
void captouch_pop_one_event_from_buffer(hal_captouch_event_t *key_event);
uint32_t captouch_get_buffer_data_size(void);

bool captouch_hw_auto_tune(hal_captouch_channel_t channel, hal_captouch_tune_data_t *tune_data);
bool captouch_sw_auto_tune(hal_captouch_channel_t channel, hal_captouch_tune_data_t *tune_data);
void captouch_man_tune(hal_captouch_channel_t channel, hal_captouch_tune_data_t *tune_data);

void captouch_set_fine_cap(hal_captouch_channel_t channel,int16_t fine_tune);
void captouch_set_coarse_cap(hal_captouch_channel_t channel, uint8_t coarse_tune);
void captouch_set_threshold(hal_captouch_channel_t channel,int16_t high_thr, int16_t low_thr);

bool captouch_clk_control(bool is_clock_on);
void captouch_get_tune_state(hal_captouch_channel_t channel,hal_captouch_tune_data_t *tune_data);

int16_t captouch_7signed_to_16signed(uint16_t data);
int16_t captouch_9signed_to_16signed(uint16_t data);

int16_t captouch_sort_and_get_avg_data(int16_t *p, uint32_t data_len);
void captouch_switch_debug_sel(hal_captouch_channel_t channel);

void captouch_tune_auto_control(bool is_auto);
void captouch_analog_init(hal_captouch_config_t *config);
void captouch_channel_int_control(uint8_t channel_bit_map, bool en);
void captouch_int_control(bool en);
void captouch_channel_sense_control(uint8_t sensing_bit_map, bool en);
hal_captouch_status_t captouch_rtc_clk_control(bool is_en);

uint8_t captouch_get_coarse_cap(hal_captouch_channel_t channel);
#ifdef __cplusplus
}
#endif

#endif /* HAL_CAPTOUCH_MODULE_ENABLED */

#endif /* __HAL_CAPTOUCH_INTERNAL_H__ */

