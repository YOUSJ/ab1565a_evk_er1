/* Copyright Statement:
 *
 * (C) 2017  Airoha Technology Corp. All rights reserved.
 *
 * This software/firmware and related documentation ("Airoha Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.
 * Without the prior written permission of Airoha and/or its licensors,
 * any reproduction, modification, use or disclosure of Airoha Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 * You may only use, reproduce, modify, or distribute (as applicable) Airoha Software
 * if you have agreed to and been bound by the applicable license agreement with
 * Airoha ("License Agreement") and been granted explicit permission to do so within
 * the License Agreement ("Permitted User").  If you are not a Permitted User,
 * please cease any access or use of Airoha Software immediately.
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES
 * ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.
 */

#ifndef __HAL_PLATFORM_H__
#define __HAL_PLATFORM_H__


#include "hal_define.h"
#include "hal_feature_config.h"
#include "ab155x.h"
#include "memory_map.h"
#include "hal_core_status.h"

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************
* Defines for module subfeatures.
* All the subfeatures described below are mandatory for the driver operation. No change is recommended.
*****************************************************************************/
#ifdef HAL_DVFS_MODULE_ENABLED
#define HAL_CLOCK_METER_ENABLE          /*Enable clock software meter */
#define HAL_DVFS_LOCK_CTRL_ENABLED      /*Enable dvfs lock control relative api*/
#define HAL_DVFS_312M_SOURCE            /*Enable 312Mhz / 192Mhz clock */
#endif

#ifdef HAL_CACHE_MODULE_ENABLED
#define HAL_CACHE_WITH_REMAP_FEATURE     /* Enable CACHE setting with remap feature. */
#define HAL_CACHE_REGION_CONVERT         /* Enable mutual conversion between cacheable and non-cacheable addresses*/
#endif

#ifdef HAL_AUDIO_MODULE_ENABLED
#define HAL_AUDIO_SUPPORT_MULTIPLE_STREAM_OUT   /*Enable support multiple audio stream out feature.*/
#define HAL_AUDIO_SUPPORT_DEBUG_DUMP            /*Enable support dump audio register for debug.*/
#define HAL_AUDIO_SUPPORT_APLL                  /*Enable support apll feature.*/
//#define HAL_AUDIO_SUPPORT_MULTIPLE_MICROPHONE   /*Enable support multiple microphone.*/
#define HAL_AUDIO_LEAKAGE_COMPENSATION_FEATURE   /*Enable support leakage compensation.*/
#endif

#ifdef HAL_I2C_MASTER_MODULE_ENABLED
#define HAL_I2C_MASTER_FEATURE_HIGH_SPEED       /* Enable I2C high speed 2M&3.25M. */
#define HAL_I2C_MASTER_FEATURE_SEND_TO_RECEIVE  /* Enable I2C master send to receive feature. */
#define HAL_I2C_MASTER_FEATURE_EXTENDED_DMA     /* Enable I2C master extend DMA feature.*/
#define HAL_I2C_MASTER_FEATURE_CONFIG_IO        /* Enable I2C master config IO mode feature.*/
#endif

#ifdef HAL_SPI_MASTER_MODULE_ENABLED
#define HAL_SPI_MASTER_FEATURE_ADVANCED_CONFIG       /* Enable SPI master advanced configuration feature. For more details, please refer to hal_spi_master.h.*/
#define HAL_SPI_MASTER_FEATURE_DEASSERT_CONFIG       /* Enable SPI master deassert configuration feature to deassert the chip select signal after each byte data transfer is complete. For more details, please refer to hal_spi_master.h.*/
#define HAL_SPI_MASTER_FEATURE_CHIP_SELECT_TIMING    /* Enable SPI master chip select timing configuration feature to set timing value for chip select signal. For more details, please refer to hal_spi_master.h.*/
#define HAL_SPI_MASTER_FEATURE_DMA_MODE              /* Enable SPI master DMA mode feature to do data transfer. For more details, please refer to hal_spi_master.h.*/
#define HAL_SPI_MASTER_FEATURE_DUAL_QUAD_MODE        /* Enable SPI master to use dual mode and quad mode. For more details, please refer to hal_spi_master.h. */
#endif

#ifdef HAL_GPIO_MODULE_ENABLED
#define HAL_GPIO_FEATURE_PUPD               /* Pull state of the pin can be configured with different resistors through different combinations of GPIO_PUPD_x,GPIO_RESEN0_x and GPIO_RESEN1_x. For more details, please refer to hal_gpio.h. */
#define HAL_GPIO_FEATURE_CLOCKOUT           /* The pin can be configured as an output clock. For more details, please refer to hal_gpio.h. */
#define HAL_GPIO_FEATURE_HIGH_Z             /* The pin can be configured to provide high impedance state to prevent possible electric leakage. For more details, please refer to hal_gpio.h. */
#define HAL_GPIO_FEATURE_SET_DRIVING        /* The pin can be configured to enhance driving. For more details, please refer to hal_gpio.h. */
#define HAL_GPIO_FEATURE_SET_SCHMITT        /* The pin can be configured to enhance schmitt trigger hysteresis. */
#define HAL_GPIO_FEATURE_SET_SLEW_RATE      /* The pin can be configured to enhance slew rate. */
#endif

#ifdef HAL_EINT_MODULE_ENABLED
#define HAL_EINT_FEATURE_MASK                /* Supports EINT mask interrupt. */
#define HAL_EINT_FEATURE_SW_TRIGGER_EINT     /* Supports software triggered EINT interrupt. */
#define HAL_EINT_FEATURE_MUX_MAPPING         /* Supports EINT number mux to different EINT GPIO pin. */
#endif

#ifdef HAL_GPT_MODULE_ENABLED
#define HAL_GPT_FEATURE_US_TIMER               /* Supports a microsecond timer. */
#define HAL_GPT_SW_GPT_FEATURE                 /* Supports software GPT timer. */
#define HAL_GPT_PORT_ALLOCATE                  /* Allocates GPT communication port. */
#define HAL_GPT_SW_GPT_US_FEATURE              /* Supports software GPT us timer. */
#endif

#ifdef HAL_PWM_MODULE_ENABLED
#define HAL_PWM_FEATURE_ADVANCED_CONFIG        /* Supports PWM advanced configuration. */
#endif

#ifdef HAL_RTC_MODULE_ENABLED
#define HAL_RTC_FEATURE_TIME_CALLBACK           /* Supports time change notification callback. */
#define HAL_RTC_FEATURE_RTC_MODE                /* Supports enter RTC mode. */
#define HAL_RTC_FEATURE_GPIO_EINT               /* Supports RTC GPIO and EINT configuration. */
#define HAL_RTC_FEATURE_CAPTOUCH                /* Supports CAPTOUCH configuration. */
#endif

#ifdef HAL_SPI_SLAVE_MODULE_ENABLED
#define HAL_SPI_SLAVE_FEATURE_SW_CONTROL        /* Supports SPI slave to communicate with SPI master using software control. */
#endif

#ifdef HAL_UART_MODULE_ENABLED
#define HAL_UART_FEATURE_VFIFO_DMA_TIMEOUT        /* Supports configurable timeout value setting */
#define HAL_UART_FEATURE_3M_BAUDRATE              /* Supports UART 3M baudrate setting */
#endif

#ifdef HAL_AES_MODULE_ENABLED
#define HAL_AES_USE_PHYSICAL_MEMORY_ADDRESS        /* Notify caller must use physical memory */
#endif

#ifdef HAL_SHA_MODULE_ENABLED
#define HAL_SHA_USE_PHYSICAL_MEMORY_ADDRESS        /* Notify caller must use physical memory */
#endif

#ifdef HAL_SLEEP_MANAGER_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup SLEEP_MANAGER
 * @{
 * @addtogroup hal_sleep_manager_enum Enum
 * @{
 */
/*****************************************************************************
 * Enum
 *****************************************************************************/
/** @brief Sleep modes */
typedef enum {
    HAL_SLEEP_MODE_NONE = 0,        /**< No sleep. */
    HAL_SLEEP_MODE_IDLE,            /**< Idle state. */
    HAL_SLEEP_MODE_SLEEP,           /**< Sleep state. */
    HAL_SLEEP_MODE_NUMBER           /**< To support range detection. */
} hal_sleep_mode_t;
/** @brief sleep_manager wake up source */
typedef enum {
    HAL_SLEEP_MANAGER_WAKEUP_SOURCE_GPT = 0,            /**< General purpose timer. */
    HAL_SLEEP_MANAGER_WAKEUP_SOURCE_EINT = 1,           /**< External interrupt. */
    HAL_SLEEP_MANAGER_WAKEUP_SOURCE_IRQGEN = 2,         /**< IRQGEN. */
    HAL_SLEEP_MANAGER_WAKEUP_SOURCE_OST = 3,            /**< OST. */
    HAL_SLEEP_MANAGER_WAKEUP_SOURCE_AUDIO = 4,          /**< AUDIO.  */
    HAL_SLEEP_MANAGER_WAKEUP_SOURCE_I2S = 5,            /**< I2S. */
    HAL_SLEEP_MANAGER_WAKEUP_SOURCE_I2S_DMA = 6,        /**< I2S_DMA. */
    HAL_SLEEP_MANAGER_WAKEUP_SOURCE_CAP_TOUCH = 7,      /**< CAP_TOUCH. */
    HAL_SLEEP_MANAGER_WAKEUP_SOURCE_ANC = 8,            /**< ANC. */
    HAL_SLEEP_MANAGER_WAKEUP_SOURCE_SPI_SLAVE = 9,      /**< SPI_SLAVE. */
    HAL_SLEEP_MANAGER_WAKEUP_SOURCE_RGU = 10,           /**< RGU. */
    HAL_SLEEP_MANAGER_WAKEUP_SOURCE_DSP_DMA = 12,       /**< DSP_DMA. */
    HAL_SLEEP_MANAGER_WAKEUP_SOURCE_ALL = 13            /**< All wakeup source. */
} hal_sleep_manager_wakeup_source_t;
/**
 * @}
 * @}
 * @}
 */
#endif

#ifdef HAL_UART_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup UART
 * @{
 * @addtogroup hal_uart_enum Enum
 * @{
 */
/*****************************************************************************
* UART
*****************************************************************************/
/** @brief UART port index
 * There are total of four UART ports. Only UART0 and UART1 support hardware flow control.
 * | UART port | Hardware Flow Control |
 * |-----------|-----------------------|
 * |  UART0    |           V           |
 * |  UART1    |           V           |
 * |  UART2    |           X           |
 * |  UART3    |           X           |
 */
typedef enum {
    HAL_UART_0 = 0,                            /**< UART port 0. */
    HAL_UART_1 = 1,                            /**< UART port 1. */
    HAL_UART_2 = 2,                            /**< UART port 2. */
    HAL_UART_MAX                               /**< The total number of UART ports (invalid UART port number). */
} hal_uart_port_t;

/**
  * @}
  */

/**@addtogroup hal_uart_define Define
 * @{
  */

/** @brief  The maximum timeout value for UART timeout interrupt, unit is ms.
  */
#define HAL_UART_TIMEOUT_VALUE_MAX  (2500)

/**
  * @}
  */

/**
 * @}
 * @}
 */
#endif


#ifdef HAL_I2C_MASTER_MODULE_ENABLED
#ifdef HAL_I2C_MASTER_FEATURE_EXTENDED_DMA
/**
 * @addtogroup HAL
 * @{
 * @addtogroup I2C_MASTER
 * @{
 * @section HAL_I2C_Transaction_Pattern_Chapter API transaction length and packets
 *  The Transaction packet is a packet sent by the I2C master using SCL and SDA.
 *  Different APIs have different transaction packets, as shown below.
 * - \b Transaction \b length \b supported \b by \b the \b APIs \n
 *  The total transaction length is determined by 4 parameters:\n
 *  send_packet_length(Ns), which indicates the number of sent packet.\n
 *  send_bytes_in_one_packet(Ms).\n
 *  receive_packet_length(Nr).\n
 *  receive_bytes_in_one_packet(Mr).\n
 *  Next, the relationship between transaction packet and these 4 parameters is introduced.
 *  - Total transaction length = Ns * Ms + Nr * Mr.
 *   - Ns is the packet length to be sent by the I2C master.
 *   - Ms is the total number of bytes in a sent packet.
 *   - Nr is the packet length to be received by the I2C master.
 *   - Mr is the total number of bytes in a received packet.
 *  - NA means the related parameter should be ignored.
 *  - 1~8 specifies the parameter range is from 1 to 8. 1~15 specifies the parameter range is from 1 to 15. 1~255 specifies the parameter range from 1 to 255.
 *  - 1 means the parameter value can only be 1.
 *  - Note1: functions with the suffix "_ex" have these 4 parameters. Other functions only have the "size" parameter and the driver splits the "size" into these 4 parameters.
 *  - Note2: The maximum of total transaction length is 256K.\n
 *    #hal_i2c_master_send_polling() for example, the "size" will be divided like this: send_packet_length = 1, send_bytes_in_one_packet = size.
 *          As a result, the total size should be: send_packet_length * send_bytes_in_one_packet = 1 * size = size. The range of "size" should be from 1 to 8.
 * |API                                         |send_packet_length(Ns) | send_bytes_in_one_packet(Ms) | receive_packet_length(Nr) | receive_bytes_in_one_packet(Mr) |
 * |--------------------------------------------|-----------------------|------------------------------|---------------------------|---------------------------------|
 * |hal_i2c_master_send_polling                 |          1            |            1~8               |            NA             |                NA               |
 * |hal_i2c_master_receive_polling              |          NA           |            NA                |            1              |                1~8              |
 * |hal_i2c_master_send_to_receive_polling      |          1            |            1~8               |            1              |                1~8              |
 * |hal_i2c_master_send_dma                     |          1            |            1~65535           |            NA             |                NA               |
 * |hal_i2c_master_receive_dma                  |          NA           |            NA                |            1~65535        |                1                |
 * |hal_i2c_master_send_to_receive_dma          |          1            |            1~65535           |            1~65534        |                1                |
 * |hal_i2c_master_send_dma_ex                  |          1~65535      |            1~65535           |            NA             |                NA               |
 * |hal_i2c_master_receive_dma_ex               |          NA           |            NA                |            1~65535        |                1~65535          |
 * |hal_i2c_master_send_to_receive_dma_ex       |          1            |            1~65535           |            1~65534        |                1~65535          |
 *
 * - \b Waveform \b pattern \b supported \b by \b the \b APIs \n
 *  The 4 parameters (send_packet_length(Ns), send_bytes_in_one_packet(Ms), receive_packet_length(Nr), receive_bytes_in_one_packet(Mr) will also affect the transaction packet.
 *  The relationship between transaction packet and these 4 parameters is shown below.
 *  - Ns is the send_packet_length.
 *  - Ms is the send_bytes_in_one_packet.
 *  - Nr is the receive_packet_length.
 *  - Mr is the receive_bytes_in_one_packet.
 * |API                                          |transaction packet format                                 |
 * |---------------------------------------------|----------------------------------------------------------|
 * | hal_i2c_master_send_polling                 |  @image html hal_i2c_send_poling_waveform.png            |
 * | hal_i2c_master_receive_polling              |  @image html hal_i2c_receive_poling_waveform.png         |
 * | hal_i2c_master_send_to_receive_polling      |  @image html hal_i2c_send_to_receive_poling_waveform.png |
 * | hal_i2c_master_send_dma                     |  @image html hal_i2c_send_dma_waveform.png            |
 * | hal_i2c_master_receive_dma                  |  @image html hal_i2c_receive_dma_waveform.png         |
 * | hal_i2c_master_send_to_receive_dma          |  @image html hal_i2c_send_to_receive_dma_waveform.png |
 * | hal_i2c_master_send_dma_ex                  |  @image html hal_i2c_send_dma_ex_waveform.png            |
 * | hal_i2c_master_receive_dma_ex               |  @image html hal_i2c_receive_dma_ex_waveform.png         |
 * | hal_i2c_master_send_to_receive_dma_ex       |  @image html hal_i2c_send_to_receive_dma_ex_waveform.png |
 *
 *
 *
 *
 */
#endif

/** @defgroup hal_i2c_master_define Define
 * @{
  */

/** @brief  The maximum polling mode transaction size.
  */
#define HAL_I2C_MAXIMUM_POLLING_TRANSACTION_SIZE  8

/** @brief  The maximum DMA mode transaction size.
  */
#define HAL_I2C_MAXIMUM_DMA_TRANSACTION_SIZE   65535

/**
  * @}
  */

/** @addtogroup hal_i2c_master_enum Enum
  * @{
  */

/*****************************************************************************
* I2C master
*****************************************************************************/
/** @brief This enum defines the I2C port.
 *  The platform supports 4 I2C masters. Three of them support polling mode and DMA mode,
 *  while the other only supports polling mode. For more information about the polling mode,
 *  DMA mode, queue mode, please refer to @ref HAL_I2C_Features_Chapter. The details
 *  are shown below:
 *  - Supported features of I2C masters \n
 *    V : supported;  X : not supported.
 * |I2C Master   | Polling mode | DMA mode | Extended DMA mode |
 * |-------------|--------------|----------|-------------------|
 * |I2C0         |      V       |    V     |         V         |
 * |I2C1         |      V       |    V     |         V         |
 * |I2C2         |      V       |    V     |         V         |
 * |I2CAO        |      X       |    X     |         X         |
 *
 *
*/
typedef enum {
    HAL_I2C_MASTER_0 = 0,                /**< I2C master 0. User defined. */
    HAL_I2C_MASTER_1 = 1,                /**< I2C master 1. User defined. */
    HAL_I2C_MASTER_2 = 2,                /**< I2C master 2. User defined. */
    HAL_I2C_MASTER_AO = 3,               /**< I2C master AO. Used for Pmic module,not recommended for customers.*/
    HAL_I2C_MASTER_MAX                   /**< The total number of I2C masters (invalid I2C master number). */
} hal_i2c_port_t;

/**
  * @}
  */

/**
 * @}
 * @}
 */
#endif


#ifdef HAL_GPIO_MODULE_ENABLED
/**
* @addtogroup HAL
* @{
* @addtogroup GPIO
* @{
*
* @addtogroup hal_gpio_enum Enum
* @{
*/

/*****************************************************************************
* GPIO
*****************************************************************************/
/** @brief This enum defines the GPIO port.
 *  The platform supports a total of 49 GPIO pins with various functionality.
 *
*/

typedef enum {
    HAL_GPIO_0  = 0,    /**< GPIO pin0. */
    HAL_GPIO_1  = 1,    /**< GPIO pin1. */
    HAL_GPIO_2  = 2,    /**< GPIO pin2. */
    HAL_GPIO_3  = 3,    /**< GPIO pin3. */
    HAL_GPIO_4  = 4,    /**< GPIO pin4. */
    HAL_GPIO_5  = 5,    /**< GPIO pin5. */
    HAL_GPIO_6  = 6,    /**< GPIO pin6. */
    HAL_GPIO_7  = 7,    /**< GPIO pin7. */
    HAL_GPIO_8  = 8,    /**< GPIO pin8. */
    HAL_GPIO_9  = 9,    /**< GPIO pin9. */
    HAL_GPIO_10 = 10,   /**< GPIO pin10. */
    HAL_GPIO_11 = 11,   /**< GPIO pin11. */
    HAL_GPIO_12 = 12,   /**< GPIO pin12. */
    HAL_GPIO_13 = 13,   /**< GPIO pin13. */
    HAL_GPIO_14 = 14,   /**< GPIO pin14. */
    HAL_GPIO_15 = 15,   /**< GPIO pin15. */
    HAL_GPIO_16 = 16,   /**< GPIO pin16. */
    HAL_GPIO_17 = 17,   /**< GPIO pin17. */
    HAL_GPIO_18 = 18,   /**< GPIO pin18. */
    HAL_GPIO_19 = 19,   /**< GPIO pin19. */
    HAL_GPIO_20 = 20,   /**< GPIO pin20. */
    HAL_GPIO_21 = 21,   /**< GPIO pin21. */
    HAL_GPIO_22 = 22,   /**< GPIO pin22. */
    HAL_GPIO_23 = 23,   /**< GPIO pin23. */
    HAL_GPIO_24 = 24,   /**< GPIO pin24. */
    HAL_GPIO_25 = 25,   /**< GPIO pin25. */
    HAL_GPIO_26 = 26,   /**< GPIO pin26. */
    HAL_GPIO_27 = 27,   /**< GPIO pin27. */
    HAL_GPIO_28 = 28,   /**< GPIO pin28. */
    HAL_GPIO_29 = 29,   /**< GPIO pin29. */
    HAL_GPIO_30 = 30,   /**< GPIO pin30. */
    HAL_GPIO_31 = 31,   /**< GPIO pin31. */
    HAL_GPIO_32 = 32,   /**< GPIO pin32. */
    HAL_GPIO_33 = 33,   /**< GPIO pin33. */
    HAL_GPIO_34 = 34,   /**< GPIO pin34. */
    HAL_GPIO_35 = 35,   /**< GPIO pin35. */
    HAL_GPIO_36 = 36,   /**< GPIO pin36. */
    HAL_GPIO_37 = 37,   /**< GPIO pin37. */
    HAL_GPIO_38 = 38,   /**< GPIO pin38. */
    HAL_GPIO_39 = 39,   /**< GPIO pin39. */
    HAL_GPIO_40 = 40,   /**< GPIO pin40. */
    HAL_GPIO_41 = 41,   /**< GPIO pin41. */
    HAL_GPIO_42 = 42,   /**< GPIO pin42. */
    HAL_GPIO_43 = 43,   /**< GPIO pin43. */
    HAL_GPIO_44 = 44,   /**< GPIO pin44. */
    HAL_GPIO_45 = 45,   /**< GPIO pin45. */
    HAL_GPIO_46 = 46,   /**< GPIO pin46. */
    HAL_GPIO_47 = 47,   /**< GPIO pin47. */
    HAL_GPIO_48 = 48,   /**< GPIO pin48. */
    HAL_GPIO_49 = 49,   /**< GPIO pin49. */
    HAL_GPIO_50 = 50,   /**< GPIO pin50. */
    HAL_GPIO_51 = 51,   /**< GPIO pin51. */
    HAL_GPIO_52 = 52,   /**< GPIO pin52. */
    HAL_GPIO_53 = 53,   /**< GPIO pin53. */
    HAL_GPIO_54 = 54,   /**< GPIO pin54. */
    HAL_GPIO_55 = 55,   /**< GPIO pin55. */
    HAL_GPIO_56 = 56,   /**< GPIO pin56. */
    HAL_GPIO_57 = 57,   /**< GPIO pin57. */
    HAL_GPIO_58 = 58,   /**< GPIO pin58. */
    HAL_GPIO_59 = 59,   /**< GPIO pin59. */
    HAL_GPIO_60 = 60,   /**< GPIO pin60. */
    HAL_GPIO_61 = 61,   /**< GPIO pin61. */
    HAL_GPIO_62 = 62,   /**< GPIO pin62. */
    HAL_GPIO_63 = 63,   /**< GPIO pin63. */
    HAL_GPIO_64 = 64,   /**< GPIO pin64. */
    HAL_GPIO_65 = 65,   /**< GPIO pin65. */
    HAL_GPIO_66 = 66,   /**< GPIO pin66. */
    HAL_GPIO_67 = 67,   /**< GPIO pin67. */
    HAL_GPIO_68 = 68,   /**< GPIO pin68. */
    HAL_GPIO_69 = 69,   /**< GPIO pin69. */
    HAL_GPIO_70 = 70,   /**< GPIO pin70. */
    HAL_GPIO_71 = 71,   /**< GPIO pin71. */
    HAL_GPIO_72 = 72,   /**< GPIO pin72. */
    HAL_GPIO_73 = 73,   /**< GPIO pin73. */
    HAL_GPIO_74 = 74,   /**< GPIO pin74. */
    HAL_GPIO_75 = 75,   /**< GPIO pin75. */
    HAL_GPIO_76 = 76,   /**< GPIO pin76. */
    HAL_GPIO_77 = 77,   /**< GPIO pin77. */
    HAL_GPIO_78 = 78,   /**< GPIO pin78. */
    HAL_GPIO_79 = 79,   /**< GPIO pin79. */
    HAL_GPIO_80 = 80,   /**< GPIO pin80. */
    HAL_GPIO_81 = 81,   /**< GPIO pin81. */
    HAL_GPIO_82 = 82,   /**< GPIO pin82. */
    HAL_GPIO_83 = 83,   /**< GPIO pin83. */
    HAL_GPIO_84 = 84,   /**< GPIO pin84. */
    HAL_GPIO_85 = 85,   /**< GPIO pin85. */
    HAL_GPIO_MAX                               /**< The total number of GPIO pins (invalid GPIO pin number). */
} hal_gpio_pin_t;

/**
  * @}
  */

/**
 * @}
 * @}
 */
#endif

#ifdef HAL_GPIO_FEATURE_CLOCKOUT
/**
 * @addtogroup HAL
 * @{
 * @addtogroup GPIO
 * @{
 * @addtogroup hal_gpio_enum Enum
 * @{
 */
/*****************************************************************************
* CLKOUT
*****************************************************************************/
/** @brief  This enum defines output clock number of GPIO */
typedef enum {
    HAL_GPIO_CLOCK_0   = 0,              /**< define GPIO output clock 0 */
    HAL_GPIO_CLOCK_1   = 1,              /**< define GPIO output clock 1 */
    HAL_GPIO_CLOCK_2   = 2,              /**< define GPIO output clock 2 */
    HAL_GPIO_CLOCK_3   = 3,              /**< define GPIO output clock 3 */
    HAL_GPIO_CLOCK_4   = 4,              /**< define GPIO output clock 4 */
    HAL_GPIO_CLOCK_MAX                   /**< define GPIO output clock max number(invalid) */
} hal_gpio_clock_t;


/** @brief This enum defines output clock mode of GPIO */
typedef enum {
    HAL_GPIO_CLOCK_MODE_32K = 0,        /**< define GPIO output clock mode as 32KHz */
    HAL_GPIO_CLOCK_MODE_24M = 2,        /**< define GPIO output clock mode as 24MHz */
    HAL_GPIO_CLOCK_MODE_48M = 3,        /**< define GPIO output clock mode as 48MHz */
    HAL_GPIO_CLOCK_MODE_40K = 5,        /**< define GPIO output clock mode as 40MHz */
    HAL_GPIO_CLOCK_MODE_MAX             /**< define GPIO output clock mode of max number(invalid) */
} hal_gpio_clock_mode_t;
/**
  * @}
  */

/**
 * @}
 * @}
 */
#endif

#ifdef HAL_ADC_MODULE_ENABLED
/**
* @addtogroup HAL
* @{
* @addtogroup ADC
* @{
*
* @addtogroup hal_adc_enum Enum
* @{
*/

/*****************************************************************************
* ADC
*****************************************************************************/
/** @brief adc channel */
typedef enum {
    HAL_ADC_CHANNEL_0 = 0,                        /**< ADC channel 0. */
    HAL_ADC_CHANNEL_1 = 1,                        /**< ADC channel 1. */
    HAL_ADC_CHANNEL_2 = 2,                        /**< ADC channel 2. */
    HAL_ADC_CHANNEL_3 = 3,                        /**< ADC channel 3. */
    HAL_ADC_CHANNEL_4 = 4,                        /**< ADC channel 4. */
    HAL_ADC_CHANNEL_5 = 5,                        /**< ADC channel 5. */
    HAL_ADC_CHANNEL_6 = 6,                        /**< ADC channel 6. */
    HAL_ADC_CHANNEL_7 = 7,                        /**< ADC channel 7. */
    HAL_ADC_CHANNEL_MAX                           /**< The total number of ADC channels (invalid ADC channel).*/
} hal_adc_channel_t;

/**
  * @}
  */


/**
 * @}
 * @}
 */
#endif



#ifdef HAL_I2S_MODULE_ENABLED
/**
* @addtogroup HAL
* @{
* @addtogroup I2S
* @{
*
* @addtogroup hal_i2s_enum Enum
* @{
*/


/*****************************************************************************
* I2S
*****************************************************************************/
#ifdef HAL_I2S_FEATURE_MULTI_I2S
/** @brief This enum defines the I2S port.
 *
 *  The platform supports 2 I2S HW interface. Two of them support master mode and slave mode.
 *  User can drive 2 I2S HW simultaneously by the extended APIs. The basic APIs are only for I2S0.
 *  User should not use basic APIs and extension APIs simultaneously. The details are shown below:
 *
 *  - I2S supported feature table \n
 *    V : means support;  X : means not support.
 *
 * |I2S PORT   |SAMPLE WIDTH    | FS | I2S TDM  |
 * |--------- |--------------|-------------|-------------------|
 * |I2S0         | 16 bits                 |11.025 / 16 / 22.05 / 24 / 44.1 /48 /96 / 192 KHZ     | V        |
 * |I2S1         | 16 / 24 bits         |11.025 / 16 / 22.05 / 24 / 44.1 /48 /96 / 192 KHZ     | X        |
 *
*/
typedef enum {
    HAL_I2S_0  = 0,   /**< I2S interfeace 0. */
    HAL_I2S_1  = 1,    /**< I2S interfeace 1. */
    HAL_I2S_MAX
} hal_i2s_port_t;
#endif


#ifdef HAL_I2S_FEATURE_TDM
/** @brief Channels per frame sync. Number of channels in each frame sync.*/
typedef enum {
    HAL_I2S_TDM_2_CHANNEL  = 0,   /**< 2 channels. */
    HAL_I2S_TDM_4_CHANNEL  = 1    /**< 4 channels. */
} hal_i2s_tdm_channel_t;


/** @brief Polarity of BCLK.*/
typedef enum {
    HAL_I2S_BCLK_INVERSE_DISABLE  = 0, /**< Normal mode. (Invalid)*/
    HAL_I2S_BCLK_INVERSE_EABLE  = 1    /**< Invert BCLK. (Invalid)*/
} hal_i2s_bclk_inverse_t;
#endif

#ifdef HAL_I2S_EXTENDED
/** @brief I2S sample widths.  */
typedef enum {
    HAL_I2S_SAMPLE_WIDTH_8BIT  = 0,   /**< I2S sample width is 8bit. (Invalid)*/
    HAL_I2S_SAMPLE_WIDTH_16BIT = 1,   /**< I2S sample width is 16bit. (HAL_I2S_0)*/
    HAL_I2S_SAMPLE_WIDTH_24BIT = 2    /**< I2S sample width is 24bit. (HAL_I2S_0/HAL_I2S_1)*/
} hal_i2s_sample_width_t;


/** @brief Number of bits per frame sync(FS). This parameter determines the bits of a complete sample of both left and right channels.*/
typedef enum {
    HAL_I2S_FRAME_SYNC_WIDTH_32  = 0,   /**< 32 bits per frame. */
    HAL_I2S_FRAME_SYNC_WIDTH_64  = 1,   /**< 64 bits per frame. */
    HAL_I2S_FRAME_SYNC_WIDTH_128  = 2   /**< 128 bits per frame. */
} hal_i2s_frame_sync_width_t;


/** @brief Enable or disable right channel of the I2S TX to send the same data as on the left channel of the I2S TX.\n
        This function only works when the sample width of the I2S is 16 bits.*/
typedef enum {
    HAL_I2S_TX_MONO_DUPLICATE_DISABLE = 0,  /**< Keep data to its channel. */
    HAL_I2S_TX_MONO_DUPLICATE_ENABLE  = 1   /**< Assume input is mono data for left channel, the data is duplicated to right channel.*/
} hal_i2s_tx_mode_t;


/** @brief Enable or disable twice the downsampling rate mode in the I2S RX.
                 In this mode the sampling rate of the I2S TX is 48kHz while the sampling rate of the I2S RX is 24kHz. The I2S RX automatically drops 1 sample in each 2 input samples received. */
typedef enum {
    HAL_I2S_RX_DOWN_RATE_DISABLE = 0,  /**< Actual sampling rate of the I2S RX = sampling rate. (Default)*/
    HAL_I2S_RX_DOWN_RATE_ENABLE  = 1   /**< Actual sampling rate of the I2S RX is half of the original sampling rate. (Invalid)*/
} hal_i2s_rx_down_rate_t;
#endif //  #ifdef HAL_I2S_EXTENDED


/** @brief Enable or disable data swapping between right and left channels of the I2S link.\n
        This function only works when the sample width of the I2S is 16 bits.*/
typedef enum {
    HAL_I2S_LR_SWAP_DISABLE = 0,  /**< Disable the data swapping. */
    HAL_I2S_LR_SWAP_ENABLE  = 1   /**< Enable the data swapping.  */
} hal_i2s_lr_swap_t;


/** @brief Enable or disable word select clock inverting of the I2S link. */
typedef enum {
    HAL_I2S_WORD_SELECT_INVERSE_DISABLE = 0,  /**< Disable the word select clock inverting. */
    HAL_I2S_WORD_SELECT_INVERSE_ENABLE  = 1   /**< Enable the word select clock inverting.  */
} hal_i2s_word_select_inverse_t;

/** @brief This enum defines initial type of the I2S.
 */

typedef enum {
    HAL_I2S_TYPE_EXTERNAL_MODE          = 0,        /**< External mode. I2S mode with external codec.*/
    HAL_I2S_TYPE_EXTERNAL_TDM_MODE      = 1,        /**< External TDM mode. I2S TDM mode with external codec*/
    HAL_I2S_TYPE_INTERNAL_MODE          = 2,        /**< Internal mode. I2S mode with internal codec. (Invalid)*/
    HAL_I2S_TYPE_INTERNAL_LOOPBACK_MODE = 3,        /**< I2S internal loopback mode. Data loopback mode.*/
    HAL_I2S_TYPE_INTERNAL_TDM_LOOPBACK_MODE = 4,    /**< TDM internal loopback mode. Data loopback mode.*/
    HAL_I2S_TYPE_MAX = 5
} hal_i2s_initial_type_t;


/** @brief I2S event */
typedef enum {
    HAL_I2S_EVENT_ERROR               = -1, /**<  An error occurred during the function call. */
    HAL_I2S_EVENT_NONE                =  0, /**<  No error occurred during the function call. */
    HAL_I2S_EVENT_OVERFLOW            =  1, /**<  RX data overflow. */
    HAL_I2S_EVENT_UNDERFLOW           =  2, /**<  TX data underflow. */
    HAL_I2S_EVENT_DATA_REQUEST        =  3, /**<  Request for user-defined data. */
    HAL_I2S_EVENT_DATA_NOTIFICATION   =  4  /**<  Notify user the RX data is ready. */
} hal_i2s_event_t;


/** @brief I2S sampling rates */
typedef enum {
    HAL_I2S_SAMPLE_RATE_8K        = 0,  /**<  8000Hz  */
    HAL_I2S_SAMPLE_RATE_11_025K   = 1,  /**<  11025Hz */
    HAL_I2S_SAMPLE_RATE_12K       = 2,  /**<  12000Hz */
    HAL_I2S_SAMPLE_RATE_16K       = 3,  /**<  16000Hz */
    HAL_I2S_SAMPLE_RATE_22_05K    = 4,  /**<  22050Hz */
    HAL_I2S_SAMPLE_RATE_24K       = 5,  /**<  24000Hz */
    HAL_I2S_SAMPLE_RATE_32K       = 6,  /**<  32000Hz */
    HAL_I2S_SAMPLE_RATE_44_1K     = 7,  /**<  44100Hz */
    HAL_I2S_SAMPLE_RATE_48K       = 8,  /**<  48000Hz */
    HAL_I2S_SAMPLE_RATE_88_2K     = 9,  /**<  88200Hz */
    HAL_I2S_SAMPLE_RATE_96K       = 10, /**<  96000Hz */
    HAL_I2S_SAMPLE_RATE_176_4K    = 11, /**<  176400Hz */
    HAL_I2S_SAMPLE_RATE_192K      = 12  /**<  192000Hz */
} hal_i2s_sample_rate_t;

/**
  * @}
  */

/**
 * @}
 * @}
 */
#endif


#ifdef HAL_SPI_MASTER_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup SPI_MASTER
 * @{
 * @defgroup hal_spi_master_define Define
 * @{
 */

/** @brief  The maximum polling mode transaction size in bytes.
 */
#define HAL_SPI_MAXIMUM_POLLING_TRANSACTION_SIZE  32

/** @brief  The maximum transaction size in bytes when configuration is not single mode.
 */
#define HAL_SPI_MAXIMUM_NON_SINGLE_MODE_TRANSACTION_SIZE  15

/** @brief  The minimum clock frequency.
 */
#define  HAL_SPI_MASTER_CLOCK_MIN_FREQUENCY  30000

/** @brief  The maximum clock frequency.
 */
#define  HAL_SPI_MASTER_CLOCK_MAX_FREQUENCY  52000000

/**
 * @}
 */

/**
 * @addtogroup hal_spi_master_enum Enum
 * @{
 */

/*****************************************************************************
* SPI master
*****************************************************************************/
/** @brief This enum defines the SPI master port.
 *  The chip supports total of 4 SPI master ports, each of them supports polling mode
 *  and DMA mode. For more details about polling mode and DMA mode, please refer to @ref
 *  HAL_SPI_MASTER_Features_Chapter.
 */
typedef enum {
    HAL_SPI_MASTER_0 = 0,                              /**< SPI master port 0. */
    HAL_SPI_MASTER_1 = 1,                              /**< SPI master port 1. */
    HAL_SPI_MASTER_2 = 2,                              /**< SPI master port 2. */
    HAL_SPI_MASTER_MAX                                 /**< The total number of SPI master ports (invalid SPI master port). */
} hal_spi_master_port_t;

/** @brief This enum defines the options to connect the SPI slave device to the SPI master's CS pins. */
typedef enum {
    HAL_SPI_MASTER_SLAVE_0 = 0,                       /**< The SPI slave device is connected to the SPI master's CS0 pin. */
    HAL_SPI_MASTER_SLAVE_MAX                          /**< The total number of SPI master CS pins (invalid SPI master CS pin). */
} hal_spi_master_slave_port_t;

/** @brief SPI master transaction bit order definition. */
typedef enum {
    HAL_SPI_MASTER_LSB_FIRST = 0,                       /**< Both send and receive data transfer LSB first. */
    HAL_SPI_MASTER_MSB_FIRST = 1                        /**< Both send and receive data transfer MSB first. */
} hal_spi_master_bit_order_t;

/** @brief SPI master clock polarity definition. */
typedef enum {
    HAL_SPI_MASTER_CLOCK_POLARITY0 = 0,                     /**< Clock polarity is 0. */
    HAL_SPI_MASTER_CLOCK_POLARITY1 = 1                      /**< Clock polarity is 1. */
} hal_spi_master_clock_polarity_t;

/** @brief SPI master clock format definition. */
typedef enum {
    HAL_SPI_MASTER_CLOCK_PHASE0 = 0,                         /**< Clock format is 0. */
    HAL_SPI_MASTER_CLOCK_PHASE1 = 1                          /**< Clock format is 1. */
} hal_spi_master_clock_phase_t;

/** @brief This enum defines the mode of the SPI master. */
typedef enum {
    HAL_SPI_MASTER_SINGLE_MODE = 0,                      /**< Single mode. */
    HAL_SPI_MASTER_3_WIRE_MODE = 1,                      /**< Normal mode. */
    HAL_SPI_MASTER_DUAL_MODE = 2,                        /**< Dual mode. */
    HAL_SPI_MASTER_QUAD_MODE = 3,                        /**< Quad mode. */
} hal_spi_master_mode_t;

/**
 * @}
 */

/**
 * @}
 * @}
 */
#endif

#ifdef HAL_SPI_SLAVE_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup SPI_SLAVE
 * @{
 * @addtogroup hal_spi_slave_enum Enum
 * @{
 */

/*****************************************************************************
* SPI slave
*****************************************************************************/
/** @brief This enum defines the SPI slave port. This chip supports only one
 *  SPI slave port.
 */
typedef enum {
    HAL_SPI_SLAVE_0 = 0,                             /**< SPI slave port 0. */
    HAL_SPI_SLAVE_MAX                                /**< The total number of SPI slave ports (invalid SPI slave port number). */
} hal_spi_slave_port_t;

/** @brief SPI slave transaction bit order definition. */
typedef enum {
    HAL_SPI_SLAVE_LSB_FIRST = 0,                       /**< Both send and receive data transfer is the LSB first. */
    HAL_SPI_SLAVE_MSB_FIRST = 1                        /**< Both send and receive data transfer is the MSB first. */
} hal_spi_slave_bit_order_t;

/** @brief SPI slave clock polarity definition. */
typedef enum {
    HAL_SPI_SLAVE_CLOCK_POLARITY0 = 0,                 /**< Clock polarity is 0. */
    HAL_SPI_SLAVE_CLOCK_POLARITY1 = 1                  /**< Clock polarity is 1. */
} hal_spi_slave_clock_polarity_t;

/** @brief SPI slave clock format definition. */
typedef enum {
    HAL_SPI_SLAVE_CLOCK_PHASE0 = 0,                    /**< Clock format is 0. */
    HAL_SPI_SLAVE_CLOCK_PHASE1 = 1                     /**< Clock format is 1. */
} hal_spi_slave_clock_phase_t;

/** @brief This enum defines the SPI slave event when an interrupt occurs. */
typedef enum {
    HAL_SPI_SLAVE_EVENT_POWER_ON = SPIS_INT_POWER_ON_MASK,         /**< Power on command is received. */
    HAL_SPI_SLAVE_EVENT_POWER_OFF = SPIS_INT_POWER_OFF_MASK,       /**< Power off command is received. */
    HAL_SPI_SLAVE_EVENT_CRD_FINISH = SPIS_INT_RD_CFG_FINISH_MASK,  /**< Configure read command is received. */
    HAL_SPI_SLAVE_EVENT_RD_FINISH = SPIS_INT_RD_TRANS_FINISH_MASK, /**< Read command is received. */
    HAL_SPI_SLAVE_EVENT_CWR_FINISH = SPIS_INT_WR_CFG_FINISH_MASK,  /**< Configure write command is received. */
    HAL_SPI_SLAVE_EVENT_WR_FINISH = SPIS_INT_WR_TRANS_FINISH_MASK, /**< Write command is received. */
    HAL_SPI_SLAVE_EVENT_RD_ERR = SPIS_INT_RD_DATA_ERR_MASK,        /**< An error occurred during a read command. */
    HAL_SPI_SLAVE_EVENT_WR_ERR = SPIS_INT_WR_DATA_ERR_MASK,        /**< An error occurred during a write command. */
    HAL_SPI_SLAVE_EVENT_TIMEOUT_ERR = SPIS_INT_TMOUT_ERR_MASK      /**< A timeout is detected between configure read command and read command or configure write command and write command. */
} hal_spi_slave_callback_event_t;

/** @brief This enum defines the SPI slave commands. */
typedef enum {
    HAL_SPI_SLAVE_CMD_WS        = 0,       /**< Write Status command. */
    HAL_SPI_SLAVE_CMD_RS        = 1,       /**< Read Status command. */
    HAL_SPI_SLAVE_CMD_WR        = 2,       /**< Write Data command. */
    HAL_SPI_SLAVE_CMD_RD        = 3,       /**< Read Data command. */
    HAL_SPI_SLAVE_CMD_POWEROFF  = 4,       /**< POWER OFF command. */
    HAL_SPI_SLAVE_CMD_POWERON   = 5,       /**< POWER ON command. */
    HAL_SPI_SLAVE_CMD_CW        = 6,       /**< Configure Write command. */
    HAL_SPI_SLAVE_CMD_CR        = 7,        /**< Configure Read command. */
    HAL_SPI_SLAVE_CMD_CT        = 8        /**< Configure Type command. */
} hal_spi_slave_command_type_t;

/**
 * @}
 */


/**
 * @}
 * @}
 */
#endif


#ifdef HAL_RTC_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup RTC
 * @{
 * @addtogroup hal_rtc_define Define
 * @{
 */

/** @brief  This macro defines a maximum number for backup data that used in #hal_rtc_set_data(),
  * #hal_rtc_get_data(), #hal_rtc_clear_data functions.
  */
#define HAL_RTC_BACKUP_BYTE_NUM_MAX     (10)

/**
 * @}
 */

/**
 * @defgroup hal_rtc_enum Enum
 * @{
 */

/** @brief RTC current time change notification period selections. */
typedef enum {
    HAL_RTC_TIME_NOTIFICATION_NONE = 0,                     /**< No need for a time notification. */
    HAL_RTC_TIME_NOTIFICATION_EVERY_SECOND = 1,             /**< Execute a callback function set by #hal_rtc_set_time_notification_period() for every second. */
    HAL_RTC_TIME_NOTIFICATION_EVERY_MINUTE = 2,             /**< Execute a callback function set by #hal_rtc_set_time_notification_period() for every minute. */
    HAL_RTC_TIME_NOTIFICATION_EVERY_HOUR = 3,               /**< Execute a callback function set by #hal_rtc_set_time_notification_period() for every hour. */
    HAL_RTC_TIME_NOTIFICATION_EVERY_DAY = 4,                /**< Execute a callback function set by #hal_rtc_set_time_notification_period() for every day. */
    HAL_RTC_TIME_NOTIFICATION_EVERY_MONTH = 5,              /**< Execute a callback function set by #hal_rtc_set_time_notification_period() for every month. */
    HAL_RTC_TIME_NOTIFICATION_EVERY_YEAR = 6,               /**< Execute a callback function set by #hal_rtc_set_time_notification_period() for every year. */
    HAL_RTC_TIME_NOTIFICATION_EVERY_SECOND_1_2 = 7,         /**< Execute a callback function set by #hal_rtc_set_time_notification_period() for every one-half of a second. */
    HAL_RTC_TIME_NOTIFICATION_EVERY_SECOND_1_4 = 8,         /**< Execute a callback function set by #hal_rtc_set_time_notification_period() for every one-fourth of a second. */
    HAL_RTC_TIME_NOTIFICATION_EVERY_SECOND_1_8 = 9          /**< Execute a callback function set by #hal_rtc_set_time_notification_period() for every one-eighth of a second. */
} hal_rtc_time_notification_period_t;
/** @brief This enum defines the type of RTC GPIO. */
typedef enum {
    HAL_RTC_GPIO_0 = 0,     /**< RTC GPIO 0. */
    HAL_RTC_GPIO_1 = 1,     /**< RTC GPIO 1. */
    HAL_RTC_GPIO_2 = 2,     /**< RTC GPIO 2. */
    HAL_RTC_GPIO_MAX
} hal_rtc_gpio_t;
/** @brief This enum defines the data type of RTC GPIO. */
typedef enum {
    HAL_RTC_GPIO_DATA_LOW  = 0,                     /**< RTC GPIO data low. */
    HAL_RTC_GPIO_DATA_HIGH = 1                      /**< RTC GPIO data high. */
} hal_rtc_gpio_data_t;
/**
 * @}
 */

/** @defgroup hal_rtc_struct Struct
  * @{
  */

/** @brief RTC time structure definition. */
typedef struct {
    uint8_t rtc_sec;                                  /**< Seconds after minutes     - [0,59]  */
    uint8_t rtc_min;                                  /**< Minutes after the hour    - [0,59]  */
    uint8_t rtc_hour;                                 /**< Hours after midnight      - [0,23]  */
    uint8_t rtc_day;                                  /**< Day of the month          - [1,31]  */
    uint8_t rtc_mon;                                  /**< Months                    - [1,12]  */
    uint8_t rtc_week;                                 /**< Days in a week            - [0,6]   */
    uint8_t rtc_year;                                 /**< Years                     - [0,127] */
    uint16_t rtc_milli_sec;                           /**< Millisecond value, when in time API, this represents the read only register rtc_int_cnt - [0,32767]; when in alarm API, this parameter has no meaning. */
} hal_rtc_time_t;
/** @brief RTC GPIO control structure definition. */
typedef struct {
    hal_rtc_gpio_t rtc_gpio;        /**< Configure which GPIO will apply this setting. */
    bool is_enable_rtc_eint;        /**< Enable RTC EINT or not. */
    bool is_falling_edge_active;    /**< Configure RTC EINT as falling edge active or not. */
    bool is_enable_debounce;        /**< Enable RTC EINT debounce or not, if enabled, EINT debounce time is 4T*32k. */
} hal_rtc_eint_config_t;
/** @brief This structure defines the settings of RTC GPIO. */
typedef struct {
    hal_rtc_gpio_t rtc_gpio;        /**< Configure which GPIO will apply this setting. */
    bool is_input_direction;        /**< Cinfugure RTC GPIO as input direction or not. */
    bool is_output_high;            /**< If RTC GPIO is output direction, configure RTC GPIO output high or not. */
} hal_rtc_gpio_setting_t;
/**
 * @}
 */

/**
 * @}
 * @}
 */
#endif


#ifdef HAL_EINT_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup EINT
 * @{
 * @addtogroup hal_eint_enum Enum
 * @{
 */

/*****************************************************************************
* EINT
*****************************************************************************/
/** @brief EINT pins. */
typedef enum {
    HAL_EINT_NUMBER_0 = 0,
    HAL_EINT_NUMBER_1 = 1,
    HAL_EINT_NUMBER_2 = 2,
    HAL_EINT_NUMBER_3 = 3,
    HAL_EINT_NUMBER_4 = 4,
    HAL_EINT_NUMBER_5 = 5,
    HAL_EINT_NUMBER_6 = 6,
    HAL_EINT_NUMBER_7 = 7,
    HAL_EINT_NUMBER_8 = 8,
    HAL_EINT_NUMBER_9 = 9,
    HAL_EINT_NUMBER_10 = 10,
    HAL_EINT_NUMBER_11 = 11,
    HAL_EINT_NUMBER_12 = 12,
    HAL_EINT_NUMBER_13 = 13,
    HAL_EINT_NUMBER_14 = 14,
    HAL_EINT_NUMBER_15 = 15,
    HAL_EINT_NUMBER_16 = 16,
    HAL_EINT_NUMBER_17 = 17,
    HAL_EINT_NUMBER_18 = 18,
    HAL_EINT_NUMBER_19 = 19,
    HAL_EINT_NUMBER_20 = 20,
    HAL_EINT_NUMBER_21 = 21,
    HAL_EINT_NUMBER_22 = 22,
    HAL_EINT_UART_0_RX = 23,    /**< EINT number 23:  UART0 RX. */
    HAL_EINT_UART_1_RX = 24,    /**< EINT number 24:  UART1 RX. */
    HAL_EINT_UART_2_RX = 25,    /**< EINT number 25:  UART2 RX. */
    HAL_EINT_RTC       = 26,    /**< EINT number 26:  RTC. */
    HAL_EINT_USB       = 27,    /**< EINT number 27:  USB. */
    HAL_EINT_PMU       = 28,    /**< EINT number 28:  PMIC. */
    HAL_EINT_WAKE_AP   = 29,    /**< EINT number 29:  WAK_AP. */
    HAL_EINT_WDT       = 30,    /**< EINT number 30:  CONN2AP_WDT. */
    HAL_EINT_WAKEUP    = 31,    /**< EINT number 31:  CONN2AP_WAKEUP. */
    HAL_EINT_NUMBER_MAX         /**< The total number of EINT channels (invalid EINT channel). */
} hal_eint_number_t;
/**
  * @}
  */
/**
 * @}
 * @}
 */
#endif

#ifdef HAL_GPT_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup GPT
 * @{
 * @addtogroup hal_gpt_enum Enum
 * @{
 */

/*****************************************************************************
* GPT
*****************************************************************************/
/** @brief GPT port */
typedef enum {
    HAL_GPT_0 = 0,                          /**< GPT port 0. User defined. */
    HAL_GPT_1 = 1,                          /**< GPT port 1. Usee for CM4/DSP0/DSP1 to set a millisecond delay and get 1/32Khz free count. The clock source is 32Khz*/
    HAL_GPT_2 = 2,                          /**< GPT port 2. Usee for CM4/DSP0/DSP1 to set a microsecond delay and get microsecond free count. The clock source is 1Mhz*/
    HAL_GPT_3 = 3,                          /**< GPT port 3. Used for CM4 as software GPT. The clock source is 32Khz.*/
    HAL_GPT_4 = 4,                          /**< GPT port 4. Used for CM4 as software GPT. The clock source is 1Mhz.*/
    HAL_GPT_5 = 5,                          /**< GPT port 5. Used for DSP0 as software GPT. The clock source is 32Khz.*/
    HAL_GPT_6 = 6,                          /**< GPT port 6. Used for DSP0 as software GPT. The clock source is 1Mhz.*/
    HAL_GPT_7 = 7,                          /**< GPT port 7. Used for DSP0 as OS timer.*/
    HAL_GPT_8 = 8,                          /**< GPT port 8. Used for DSP1 as software GPT. The clock source is 32Khz.*/
    HAL_GPT_9 = 9,                          /**< GPT port 9. Used for DSP1 as software GPT. The clock source is 1Mhz.*/
    HAL_GPT_MAX_PORT = 10,                  /**< The total number of GPT ports (invalid GPT port). */
    HAL_GPT_MAX = 10
} hal_gpt_port_t;

/** @brief GPT clock source  */
typedef enum {
    HAL_GPT_CLOCK_SOURCE_32K = 0,            /**< Set the GPT clock source to 32kHz, 1 tick = 1/32768 seconds. */
    HAL_GPT_CLOCK_SOURCE_1M  = 1             /**< Set the GPT clock source to 1MHz, 1 tick = 1 microsecond.*/
} hal_gpt_clock_source_t;

/** @brief  The maximum time of millisecond timer.
  */
#define HAL_GPT_MAXIMUM_MS_TIMER_TIME   (130150523)

/** @brief  The SW GPT support maximal task number, include ms and us sw timer.
  */
#define HAL_GPT_SW_NUMBER    (16)

/**
  * @}
  */
/**
 * @}
 * @}
 */
#endif

#ifdef HAL_FLASH_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup FLASH
 * @{
 */

/*****************************************************************************
* Flash
*****************************************************************************/

/** @defgroup hal_flash_define Define
 * @{
  */

/** @brief  This macro defines the Flash base address.
  */
#define HAL_FLASH_BASE_ADDRESS    (0x08000000)
/**
  * @}
  */
/**
 * @}
 * @}
 */
#endif


#ifdef HAL_PWM_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup PWM
 * @{
 * @addtogroup hal_pwm_enum Enum
 * @{
 */
/*****************************************************************************
* PWM
*****************************************************************************/
/** @brief The PWM channels */
typedef enum {
    HAL_PWM_0 = 0,                            /**< PWM channel 0. */
    HAL_PWM_1 = 1,                            /**< PWM channel 1. */
    HAL_PWM_2 = 2,                            /**< PWM channel 2. */
    HAL_PWM_3 = 3,                            /**< PWM channel 3. */
    HAL_PWM_4 = 4,                            /**< PWM channel 4. */
    HAL_PWM_5 = 5,                            /**< PWM channel 5. */
    HAL_PWM_6 = 6,                            /**< PWM channel 6. */
    HAL_PWM_7 = 7,                            /**< PWM channel 7. */
    HAL_PWM_8 = 8,                            /**< PWM channel 8. */
    HAL_PWM_9 = 9,                            /**< PWM channel 9. */
    HAL_PWM_MAX_CHANNEL                     /**< The total number of PWM channels (invalid PWM channel).*/
} hal_pwm_channel_t;


/** @brief PWM clock source options */
typedef enum {
    HAL_PWM_CLOCK_13MHZ = 0,                /**< PWM clock source 13MHz(FXO). */
    HAL_PWM_CLOCK_13MHZ_OSC = 1,            /**< PWM clock source 13MHz(OSC). */
    HAL_PWM_CLOCK_32KHZ = 2,                /**< PWM clock srouce 32kHz. */
    HAL_PWM_CLOCK_48MHZ = 3,                /**< PWM clock srouce 48MHz. */
    HAL_PWM_CLOCK_MAX,
} hal_pwm_source_clock_t ;

/**
  * @}
  */
/**
 * @}
 * @}
 */
#endif

#ifdef HAL_WDT_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup WDT
 * @{
 * @addtogroup hal_wdt_define Define
 * @{
 */

/*****************************************************************************
* WDT
*****************************************************************************/
/** @brief  Define the platform related WDT restart address.
  */
#define WDT_RESTART_ADDRESS    (0xA209000C)

/** @brief  Define the platform related WDT restart address magic number.
  */
#define WDT_RESTART_KEY        (0x1456789A)

/** @brief  Define the platform related WDT interrupt register macro for fix
            wakeup source active issue when WDT reset happened.
  */
#define WDT_INT_REG    ((volatile uint32_t*)(0xA209001C))

/** @brief  This enum define the max timeout value of WDT.  */
#define HAL_WDT_MAX_TIMEOUT_VALUE (1000)

/**
 * @}
 */

/**
 * @}
 * @}
 */
#endif

#ifdef HAL_CACHE_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup CACHE
 * @{
 */

/*****************************************************************************
* Cache
*****************************************************************************/
/* NULL */

/**
 * @}
 * @}
 */
#endif

#define audio_message_type_t hal_audio_message_type_t
#ifdef HAL_AUDIO_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup AUDIO
 * @{
 * @addtogroup hal_audio_enum Enum
 * @{
 */
/** @brief AUDIO port */
typedef enum {
    HAL_AUDIO_STREAM_OUT1    = 0, /**<  stream out HWGAIN1 only. */
    HAL_AUDIO_STREAM_OUT2    = 1, /**<  stream out HWGAIN2 only. */
    HAL_AUDIO_STREAM_OUT_ALL = 2, /**<  stream out HWGAIN1 and HWGAIN2. */
} hal_audio_hw_stream_out_index_t;

/** @brief Audio message type */
typedef enum {
    AUDIO_MESSAGE_TYPE_COMMON,            /**< Audio basic scenario. */
    AUDIO_MESSAGE_TYPE_BT_AUDIO_UL  = 1,  /**< BT audio UL scenario. */
    AUDIO_MESSAGE_TYPE_BT_AUDIO_DL  = 2,  /**< BT audio DL scenario. */
    AUDIO_MESSAGE_TYPE_BT_VOICE_UL  = 3,  /**< BT aoice UL scenario. */
    AUDIO_MESSAGE_TYPE_BT_VOICE_DL  = 4,  /**< BT aoice DL scenario. */
    AUDIO_MESSAGE_TYPE_PLAYBACK     = 5,  /**< Local playback scenario. */
    AUDIO_MESSAGE_TYPE_RECORD       = 6,  /**< Mic record scenario. */
    AUDIO_MESSAGE_TYPE_PROMPT       = 7,  /**< Voice prompt scenario. */
    AUDIO_MESSAGE_TYPE_LINEIN       = 8,  /**< LineIN & loopback scenario. */
    AUDIO_MESSAGE_TYPE_BLE_AUDIO_UL = 9,  /**< BLE audio UL scenario. */
    AUDIO_MESSAGE_TYPE_BLE_AUDIO_DL = 10, /**< BLE audio DL scenario. */
    AUDIO_MESSAGE_TYPE_SIDETONE,          /**< Sidetone scenario. */
    AUDIO_MESSAGE_TYPE_ANC,               /**< ANC scenario. */
    AUDIO_MESSAGE_TYPE_AFE,               /**< DSP AFE dummy type. */
    AUDIO_MESSAGE_TYPE_MAX,               /**< Audio scenario type MAX. */

    AUDIO_RESERVE_TYPE_QUERY_RCDC,        /**< Query Message: RCDC. Different from above audio main scenario messages. Only for query purpose.*/
} audio_message_type_t;

/*****************************************************************************
* Audio setting
*****************************************************************************/

#ifdef HAL_AUDIO_SUPPORT_MULTIPLE_STREAM_OUT
/** @brief Audio device. */
typedef enum {
    HAL_AUDIO_DEVICE_NONE               = 0x0000,  /**<  No audio device is on. */
    HAL_AUDIO_DEVICE_MAIN_MIC_L         = 0x0001,  /**<  Stream in: main mic L. */
    HAL_AUDIO_DEVICE_MAIN_MIC_R         = 0x0002,  /**<  Stream in: main mic R. */
    HAL_AUDIO_DEVICE_MAIN_MIC_DUAL      = 0x0003,  /**<  Stream in: main mic L+R. */
    HAL_AUDIO_DEVICE_LINEINPLAYBACK_L   = 0x0004,  /**<  Stream in: line in playback L. */
    HAL_AUDIO_DEVICE_LINEINPLAYBACK_R   = 0x0008,  /**<  Stream in: line in playback R. */
    HAL_AUDIO_DEVICE_LINEINPLAYBACK_DUAL= 0x000c,  /**<  Stream in: line in playback L+R. */
    HAL_AUDIO_DEVICE_USBAUDIOPLAYBACK_L   = 0x0014,  /**<  Stream in: usb audio playback L. */
    HAL_AUDIO_DEVICE_USBAUDIOPLAYBACK_R   = 0x0018,  /**<  Stream in: usb audio playback R. */
    HAL_AUDIO_DEVICE_USBAUDIOPLAYBACK_DUAL= 0x001c,  /**<  Stream in: usb audio playback L+R. */
    HAL_AUDIO_DEVICE_DIGITAL_MIC_L      = 0x0010,  /**<  Stream in: digital mic L. */
    HAL_AUDIO_DEVICE_DIGITAL_MIC_R      = 0x0020,  /**<  Stream in: digital mic R. */
    HAL_AUDIO_DEVICE_DIGITAL_MIC_DUAL   = 0x0030,  /**<  Stream in: digital mic L+R. */

    HAL_AUDIO_DEVICE_DAC_L              = 0x0100,  /**<  Stream out:speaker L. */
    HAL_AUDIO_DEVICE_DAC_R              = 0x0200,  /**<  Stream out:speaker R. */
    HAL_AUDIO_DEVICE_DAC_DUAL           = 0x0300,  /**<  Stream out:speaker L+R. */

    HAL_AUDIO_DEVICE_I2S_MASTER         = 0x1000,  /**<  Stream in/out: I2S master role */
    HAL_AUDIO_DEVICE_I2S_SLAVE          = 0x2000,  /**<  Stream in/out: I2S slave role */
    HAL_AUDIO_DEVICE_EXT_CODEC          = 0x3000,   /**<  Stream out: external amp.&codec, stereo/mono */

    HAL_AUDIO_DEVICE_MAIN_MIC           = 0x0001,       /**<  OLD: Stream in: main mic. */
    HAL_AUDIO_DEVICE_HEADSET_MIC        = 0x0002,       /**<  OLD: Stream in: earphone mic. */
    HAL_AUDIO_DEVICE_HANDSET            = 0x0004,       /**<  OLD: Stream out:receiver. */
    HAL_AUDIO_DEVICE_HANDS_FREE_MONO    = 0x0008,       /**<  OLD: Stream out:loudspeaker, mono. */
    HAL_AUDIO_DEVICE_HANDS_FREE_STEREO  = 0x0010,       /**<  OLD: Stream out:loudspeaker, stereo to mono L=R=(R+L)/2. */
    HAL_AUDIO_DEVICE_HEADSET            = 0x0020,       /**<  OLD: Stream out:earphone, stereo */
    HAL_AUDIO_DEVICE_HEADSET_MONO       = 0x0040,       /**<  OLD: Stream out:earphone, mono to stereo. L=R. */
    HAL_AUDIO_DEVICE_LINE_IN            = 0x0080,       /**<  OLD: Stream in/out: line in. */
    HAL_AUDIO_DEVICE_DUAL_DIGITAL_MIC   = 0x0100,       /**<  OLD: Stream in: dual digital mic. */
    HAL_AUDIO_DEVICE_SINGLE_DIGITAL_MIC = 0x0200,       /**<  OLD: Stream in: single digital mic. */

    HAL_AUDIO_DEVICE_DUMMY              = 0xFFFFFFFF,   /**<  for DSP structrue alignment */
} hal_audio_device_t;
#endif

/** @brief audio channel selection define */
typedef enum {
    HAL_AUDIO_DIRECT                     = 0, /**< A single interconnection, output equal to input. */
    HAL_AUDIO_SWAP_L_R                   = 2, /**< L and R channels are swapped. That is (L, R) -> (R, L). */
    HAL_AUDIO_BOTH_L                     = 3, /**< only output L channel. That is (L, R) -> (L, L). */
    HAL_AUDIO_BOTH_R                     = 4, /**< only output R channel. That is (L, R) -> (R, R). */
    HAL_AUDIO_MIX_L_R                    = 5, /**< L and R channels are mixed. That is (L, R) -> (L+R, L+R). */
    HAL_AUDIO_MIX_SHIFT_L_R              = 6, /**< L and R channels are mixed and shift. That is (L, R) -> (L/2+R/2, L/2+R/2). */
    HAL_AUDIO_CHANNEL_SELECTION_DUMMY    = 0xFFFFFFFF,   /**<  for DSP structrue alignment */
} hal_audio_channel_selection_t;

/** @brief i2s clk source define */
typedef enum {
    I2S_CLK_SOURCE_APLL                         = 0, /**< Low jitter mode. */
    I2S_CLK_SOURCE_DCXO                         = 1, /**< Normal mode. */
    I2S_CLK_SOURCE_TYPE_DUMMY                   = 0xFFFFFFFF,   /**<  for DSP structrue alignment */
} I2S_CLK_SOURCE_TYPE;

/** @brief micbias source define */
typedef enum {
    MICBIAS_SOURCE_0                            = 1, /**< Open micbias0. */
    MICBIAS_SOURCE_1                            = 2, /**< Open micbias1. */
    MICBIAS_SOURCE_ALL                          = 3, /**< Open micbias0 and micbias1. */
    MICBIAS_SOURCE_TYPE_DUMMY                   = 0xFFFFFFFF,   /**<  for DSP structrue alignment */
} MICBIAS_SOURCE_TYPE;

/** @brief micbias out voltage define */
typedef enum {
    MICBIAS3V_OUTVOLTAGE_1p8v                   = 1 << 2,   /**< 1.8V */
    MICBIAS3V_OUTVOLTAGE_1p85v                  = 1 << 3,   /**< 1.85V (Default) */
    MICBIAS3V_OUTVOLTAGE_1p9v                   = 1 << 4,   /**< 1.9V */
    MICBIAS3V_OUTVOLTAGE_2p0v                   = 1 << 5,   /**< 2.0V */
    MICBIAS3V_OUTVOLTAGE_2p1v                   = 1 << 6,   /**< 2.1V */
    MICBIAS3V_OUTVOLTAGE_2p2v                   = 1 << 7,   /**< 2.2V (Not support in 2V) */
    MICBIAS3V_OUTVOLTAGE_2p4v                   = 1 << 8,   /**< 2.4V (Not support in 2V) */
    MICBIAS3V_OUTVOLTAGE_VCC                    = 0x7f<< 2, /**< BYPASSEN  */
    MICBIAS3V_OUTVOLTAGE_TYPE_DUMMY             = 0xFFFFFFFF,   /**<  for DSP structrue alignment */
} MICBIAS3V_OUTVOLTAGE_TYPE;

/** @brief micbias0 amic type define */
typedef enum {
    MICBIAS0_AMIC_MEMS                          = 0 << 9,  /**< MEMS (Default)*/
    MICBIAS0_AMIC_ECM_DIFFERENTIAL              = 1 << 9,  /**< ECM Differential*/
    MICBIAS0_AMIC_ECM_SINGLE                    = 3 << 9,  /**< ECM Single*/
    MICBIAS0_AMIC_TYPE_DUMMY                    = 0xFFFFFFFF,   /**<  for DSP structrue alignment */
} MICBIAS0_AMIC_TYPE;

/** @brief micbias1 amic type define */
typedef enum {
    MICBIAS1_AMIC_MEMS                          = 0 << 11,  /**< MEMS (Default)*/
    MICBIAS1_AMIC_ECM_DIFFERENTIAL              = 1 << 11,  /**< ECM Differential*/
    MICBIAS1_AMIC_ECM_SINGLE                    = 3 << 11,  /**< ECM Single*/
    MICBIAS1_AMIC_TYPE_DUMMY                    = 0xFFFFFFFF,   /**<  for DSP structrue alignment */
} MICBIAS1_AMIC_TYPE;

/** @brief uplink performance type define */
typedef enum {
    UPLINK_PERFORMANCE_NORMAL                   = 0 << 13, /**< Normal mode (Default)*/
    UPLINK_PERFORMANCE_HIGH                     = 1 << 13, /**< High performance mode*/
    UPLINK_PERFORMANCE_TYPE_DUMMY               = 0xFFFFFFFF,   /**<  for DSP structrue alignment */
} UPLINK_PERFORMANCE_TYPE;

/** @brief amic mic type define */
typedef enum {
    AMIC_DCC                                    = 0 << 14, /**< AMIC DCC mode.*/
    AMIC_ACC                                    = 1 << 14, /**< AMIC ACC mode.*/
    AMIC_TYPE_DUMMY                             = 0xFFFFFFFF,   /**<  for DSP structrue alignment */
} AMIC_TYPE;

/** @brief downlink performance type define */
typedef enum {
    DOWNLINK_PERFORMANCE_NORMAL                 = 0, /**< Normal mode (Default)*/
    DOWNLINK_PERFORMANCE_HIGH                   = 1, /**< High performance mode*/
    DOWNLINK_PERFORMANCE_TYPE_DUMMY             = 0xFFFFFFFF,   /**<  for DSP structrue alignment */
} DOWNLINK_PERFORMANCE_TYPE;

/** @brief audio MCLK pin select define */
typedef enum {
    AFE_MCLK_PIN_FROM_I2S0 = 0,     /**< MCLK from I2S0's mclk pin */
    AFE_MCLK_PIN_FROM_I2S1,         /**< MCLK from I2S1's mclk pin */
    AFE_MCLK_PIN_FROM_I2S2,         /**< MCLK from I2S2's mclk pin */
    AFE_MCLK_PIN_FROM_I2S3,         /**< MCLK from I2S3's mclk pin */
} afe_mclk_out_pin_t;

/** @brief audio APLL define */
typedef enum {
    AFE_APLL_NOUSE = 0,
    AFE_APLL1 = 1,                  /**< APLL1:45.1584M, 44.1K base */
    AFE_APLL2 = 2,                  /**< APLL2:49.152M, 48K base */
} afe_apll_source_t;

/** @brief audio MCLK status define */
typedef enum {
    MCLK_DISABLE = 0,               /**< Turn off MCLK */
    MCLK_ENABLE  = 1,               /**< Turn on MCLK */
} afe_mclk_status_t;

/** @brief amp performance define */
typedef enum {
    AUDIO_AMP_PERFORMANCE_NORMAL                = 0, /**< Normal mode. */
    AUDIO_AMP_PERFORMANCE_HIGH                  = 1, /**< High performance mode. */
    AUDIO_AMP_PERFORMANCE_TYPE_DUMMY            = 0xFFFFFFFF,   /**<  for DSP structrue alignment */
} AUDIO_AMP_PERFORMANCE_TYPE;

/** @brief audio ul1 dmic data selection define */
typedef enum {
    HAL_AUDIO_UL1_DMIC_DATA_SHIFT_NUM           = 15,
    HAL_AUDIO_UL1_DMIC_DATA_GPIO_DMIC0          = 0 << HAL_AUDIO_UL1_DMIC_DATA_SHIFT_NUM,    /**< Select UL1 data from GPIO DMIC0 */
    HAL_AUDIO_UL1_DMIC_DATA_GPIO_DMIC1          = 1 << HAL_AUDIO_UL1_DMIC_DATA_SHIFT_NUM,    /**< Select UL1 data from GPIO DMIC1 */
    HAL_AUDIO_UL1_DMIC_DATA_ANA_DMIC0           = 2 << HAL_AUDIO_UL1_DMIC_DATA_SHIFT_NUM,    /**< Select UL1 data from ANA_DMIC0 */
    HAL_AUDIO_UL1_DMIC_DATA_ANA_DMIC1           = 3 << HAL_AUDIO_UL1_DMIC_DATA_SHIFT_NUM,    /**< Select UL1 data from ANA_DMIC1 */
    HAL_AUDIO_UL1_DMIC_DATA_MASK                = HAL_AUDIO_UL1_DMIC_DATA_ANA_DMIC1,
} hal_audio_ul1_dmic_data_selection_t;

/** @brief audio ul2 dmic data selection define */
typedef enum {
    HAL_AUDIO_UL2_DMIC_DATA_SHIFT_NUM           = 17,
    HAL_AUDIO_UL2_DMIC_DATA_GPIO_DMIC0          = 0 << HAL_AUDIO_UL2_DMIC_DATA_SHIFT_NUM,    /**< Select UL2 data from GPIO DMIC0 */
    HAL_AUDIO_UL2_DMIC_DATA_GPIO_DMIC1          = 1 << HAL_AUDIO_UL2_DMIC_DATA_SHIFT_NUM,    /**< Select UL2 data from GPIO DMIC1 */
    HAL_AUDIO_UL2_DMIC_DATA_ANA_DMIC0           = 2 << HAL_AUDIO_UL2_DMIC_DATA_SHIFT_NUM,    /**< Select UL2 data from ANA_DMIC0 */
    HAL_AUDIO_UL2_DMIC_DATA_ANA_DMIC1           = 3 << HAL_AUDIO_UL2_DMIC_DATA_SHIFT_NUM,    /**< Select UL2 data from ANA_DMIC1 */
    HAL_AUDIO_UL2_DMIC_DATA_MASK                = HAL_AUDIO_UL2_DMIC_DATA_ANA_DMIC1,
} hal_audio_ul2_dmic_data_selection_t;

/** @brief audio ul3 dmic data selection define */
typedef enum {
    HAL_AUDIO_UL3_DMIC_DATA_SHIFT_NUM           = 19,
    HAL_AUDIO_UL3_DMIC_DATA_GPIO_DMIC0          = 0 << HAL_AUDIO_UL3_DMIC_DATA_SHIFT_NUM,    /**< Select UL3 data from GPIO DMIC0 */
    HAL_AUDIO_UL3_DMIC_DATA_GPIO_DMIC1          = 1 << HAL_AUDIO_UL3_DMIC_DATA_SHIFT_NUM,    /**< Select UL3 data from GPIO DMIC1 */
    HAL_AUDIO_UL3_DMIC_DATA_ANA_DMIC0           = 2 << HAL_AUDIO_UL3_DMIC_DATA_SHIFT_NUM,    /**< Select UL3 data from ANA_DMIC0 */
    HAL_AUDIO_UL3_DMIC_DATA_ANA_DMIC1           = 3 << HAL_AUDIO_UL3_DMIC_DATA_SHIFT_NUM,    /**< Select UL3 data from ANA_DMIC1 */
    HAL_AUDIO_UL3_DMIC_DATA_MASK                = HAL_AUDIO_UL3_DMIC_DATA_ANA_DMIC1,
} hal_audio_ul3_dmic_data_selection_t;

/** @brief DSP streaming source channel define */
typedef enum {
    AUDIO_DSP_CHANNEL_SELECTION_STEREO          = 0, /**< DSP streaming output L and R will be it own. */
    AUDIO_DSP_CHANNEL_SELECTION_MONO            = 1, /**< DSP streaming output L and R will be (L+R)/2. */
    AUDIO_DSP_CHANNEL_SELECTION_BOTH_L          = 2, /**< DSP streaming output both L. */
    AUDIO_DSP_CHANNEL_SELECTION_BOTH_R          = 3, /**< DSP streaming output both R. */
    AUDIO_DSP_CHANNEL_SELECTION_NUM,
} AUDIO_DSP_CHANNEL_SELECTION;

/** @brief audio MCLK status structure */
typedef struct {
    bool                        status;                 /**< Audio mclk on/off status*/
    int16_t                     mclk_cntr;              /**< Audio mclk user count*/
    afe_apll_source_t           apll;                   /**< Specifies the apll of mclk source.*/
    uint8_t                     divider;                /**< Specifies the divider of mclk source, MCLK = clock_source/(1+Divider), Divider = [6:0].*/
} hal_audio_mclk_status_t;

#if defined(HAL_AUDIO_SUPPORT_MULTIPLE_MICROPHONE)
/** @brief DSP input gain selection define */
typedef enum {
    HAL_AUDIO_INPUT_GAIN_SELECTION_D0_A0          = 0, /**< Setting input digital gain0 and analog gain0 . */
    HAL_AUDIO_INPUT_GAIN_SELECTION_D0_D1          = 1, /**< Setting input digital gain0 and digital gain1 . */
    HAL_AUDIO_INPUT_GAIN_SELECTION_D2_D3          = 2, /**< Setting input digital gain2 and digital gain3 . */
    HAL_AUDIO_INPUT_GAIN_SELECTION_D4             = 3, /**< Setting input digital gain4 (echo path). */
    HAL_AUDIO_INPUT_GAIN_SELECTION_A0_A1          = 4, /**< Setting input analog gain0 and analog gain1 . */
} hal_audio_input_gain_select_t;
#endif

/**
  * @}
  */
/**
 * @}
 * @}
 */
#endif

#ifdef HAL_GPC_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup GPC
 * @{
 * @addtogroup hal_gpc_enum Enum
 * @{
 */
/** @brief GPC port */
typedef enum {
    HAL_GPC_0 = 0,                          /**< GPC port 0. */
    HAL_GPC_MAX_PORT                        /**< The total number of GPC ports (invalid GPC port). */
} hal_gpc_port_t;


/**
  * @}
  */
/**
 * @}
 * @}
 */
#endif


#ifdef HAL_SD_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup SD
 * @{
 * @addtogroup hal_sd_enum Enum
 * @{
 */
/*****************************************************************************
* SD
*****************************************************************************/
/** @brief  This enum defines the SD/eMMC port. */
typedef enum {
    HAL_SD_PORT_0 = 0,                                             /**<  SD/eMMC port 0. */
    HAL_SD_PORT_1 = 1                                              /**<  SD/eMMC port 1. */
} hal_sd_port_t;

/**
  * @}
  */
/**
 * @}
 * @}
 */
#endif


#ifdef HAL_SDIO_SLAVE_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup SDIO
 * @{
 * @addtogroup hal_sdio_enum Enum
 * @{
 */
/*****************************************************************************
* SDIO
*****************************************************************************/
/** @brief  This enum defines the SDIO port.  */
typedef enum {
    HAL_SDIO_SLAVE_PORT_0 = 0,                                             /**< SDIO slave port 0. */
} hal_sdio_slave_port_t;


/**
  * @}
  */
/**
 * @}
 * @}
 */
#endif


#ifdef HAL_SDIO_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup SDIO
 * @{
 * @addtogroup hal_sdio_enum Enum
 * @{
 */
/*****************************************************************************
* SDIO
*****************************************************************************/
/** @brief  This enum defines the SDIO port.  */
typedef enum {
    HAL_SDIO_PORT_0 = 0,                                             /**< SDIO port 0. */
    HAL_SDIO_PORT_1 = 1                                              /**< SDIO port 1. */
} hal_sdio_port_t;


/**
  * @}
  */
/**
 * @}
 * @}
 */
#endif

#ifdef HAL_CLOCK_MODULE_ENABLED
/*****************************************************************************
* Clock
*****************************************************************************/

/**
 * @addtogroup HAL
 * @{
 * @addtogroup CLOCK
 * @{
 * @addtogroup hal_clock_enum Enum
 * @{
 *
 * @section CLOCK_CG_ID_Usage_Chapter HAL_CLOCK_CG_ID descriptions
 *
 * Each #hal_clock_cg_id is related to one CG. Please check the following parameters before controlling the clock.
 *
 * The description of API parameters for HAL_CLOCK_CG_ID is listed below:
 * | HAL_CLOCK_CG_ID            |Details                                                                            |
 * |----------------------------|-----------------------------------------------------------------------------------|
 * |\b HAL_CLOCK_CG_DMA         | The CG for DMA. It is controlled in DMA driver.|
 * |\b HAL_CLOCK_CG_SDIOMST_BUS | The CG for SDIO master bus. It is controlled in SDIO driver.|
 * |\b HAL_CLOCK_CG_SW_ASYS     | The CG for I2S1. It is controlled in I2S driver.|
 * |\b HAL_CLOCK_CG_SPISLV      | The CG for SPI slave. This CG should be enabled when it is connected to the master device if choosing a custom driver.|
 * |\b HAL_CLOCK_CG_SPIMST      | The CG for SPI master. It is controlled in SPI driver.|
 * |\b HAL_CLOCK_CG_SW_AUDIO    | The CG for I2S0. It is controlled in I2S driver.|
 * |\b HAL_CLOCK_CG_SDIOMST     | The CG for SDIO master. It is controlled in SDIO driver.|
 * |\b HAL_CLOCK_CG_UART1       | The CG for UART1. It is controlled in UART driver.|
 * |\b HAL_CLOCK_CG_UART2       | The CG for UART2. It is controlled in UART driver.|
 * |\b HAL_CLOCK_CG_I2C0        | The CG for I2C0. It is controlled in I2C driver.|
 * |\b HAL_CLOCK_CG_I2C1        | The CG for I2C1. It is controlled in I2C driver.|
 * |\b HAL_CLOCK_CG_CM_SYSROM   | The CG for system ROM. It cannot be disabled, otherwise the system will fail.|
 * |\b HAL_CLOCK_CG_SFC_SW      | The CG for serial flash controller. It cannot be disabled, otherwise the system will fail.|
 * |\b HAL_CLOCK_CG_SW_TRNG     | The CG for TRNG. It is controlled in TRNG driver.|
 * |\b HAL_CLOCK_CG_SW_XTALCTL  | The CG for crystal oscillator. It cannot be disabled, otherwise the system will fail.|
 * |\b HAL_CLOCK_CG_UART0       | The CG for UART0. It cannot be disabled, otherwise the system will fail.|
 * |\b HAL_CLOCK_CG_CRYPTO      | The CG for crypto engine. It is controlled in crypto engine driver.|
 * |\b HAL_CLOCK_CG_SDIOSLV     | The CG for SDIO slave. This CG should be enabled when it is connected to the master device if choosing a custom driver.|
 * |\b HAL_CLOCK_CG_PWM0        | The CG for PWM0. It is controlled in PWM driver.|
 * |\b HAL_CLOCK_CG_PWM1        | The CG for PWM1. It is controlled in PWM driver.|
 * |\b HAL_CLOCK_CG_PWM2        | The CG for PWM2. It is controlled in PWM driver.|
 * |\b HAL_CLOCK_CG_PWM3        | The CG for PWM3. It is controlled in PWM driver.|
 * |\b HAL_CLOCK_CG_PWM4        | The CG for PWM4. It is controlled in PWM driver.|
 * |\b HAL_CLOCK_CG_PWM5        | The CG for PWM5. It is controlled in PWM driver.|
 * |\b HAL_CLOCK_CG_SW_GPTIMER  | The CG for general purpose timer. It cannot be disabled, otherwise the system will fail.|
 * |\b HAL_CLOCK_CG_SW_AUXADC   | The CG for ADC. It is controlled in ADC driver.|
 */
/** @brief Use hal_clock_cg_id in Clock API. */
/*************************************************************************
 * Define clock gating registers and bit structure.
 * Note: Mandatory, modify clk_cg_mask in hal_clock.c source file, if hal_clock_cg_id has changed.
 *************************************************************************/
typedef enum {
    /* NR_PDN_COND0 = 25 */
    HAL_CLOCK_CG_SPISLV                 =  1,        /* bit 1, PDN_COND0_FROM */
    HAL_CLOCK_CG_SDIOMST                =  2,        /* bit 2, */
    HAL_CLOCK_CG_SDIOMST_BUS            =  3,        /* bit 3, */
    HAL_CLOCK_CG_I2S0                   =  4,        /* bit 4, */
    HAL_CLOCK_CG_I2S1                   =  5,        /* bit 5, */
    HAL_CLOCK_CG_I2S2                   =  6,        /* bit 6, */
    HAL_CLOCK_CG_I2S3                   =  7,        /* bit 7, */
    HAL_CLOCK_CG_SPIMST0                =  8,        /* bit 8, */
    HAL_CLOCK_CG_SPIMST1                =  9,        /* bit 9, */
    HAL_CLOCK_CG_SPIMST2                = 10,        /* bit 10, */
    HAL_CLOCK_CG_UART1                  = 11,        /* bit 11, */
    HAL_CLOCK_CG_UART2                  = 12,        /* bit 12, */
    HAL_CLOCK_CG_BSI                    = 13,        /* bit 13, */
    HAL_CLOCK_CG_IRRX_26M               = 14,        /* bit 14, */
    HAL_CLOCK_CG_CM_SYSROM              = 16,        /* bit 16, */
    HAL_CLOCK_CG_SFC                    = 17,        /* bit 17, */
    HAL_CLOCK_CG_TRNG                   = 18,        /* bit 18, */
    HAL_CLOCK_CG_EMI                    = 19,        /* bit 19, */
    HAL_CLOCK_CG_UART0                  = 20,        /* bit 20, */
    HAL_CLOCK_CG_CRYPTO                 = 21,        /* bit 21, */
    HAL_CLOCK_CG_GPTIMER                = 23,        /* bit 23, */
    HAL_CLOCK_CG_OSTIMER                = 24,        /* bit 24, */
    HAL_CLOCK_CG_USB                    = 25,        /* bit 25, */
    HAL_CLOCK_CG_USB_BUS                = 26,        /* bit 26, */
    HAL_CLOCK_CG_USB_DMA                = 27,        /* bit 27, PDN_COND0_TO */

    /* NR_PDN_COND1 = 13 */
    HAL_CLOCK_CG_PWM0                   = (0 + 32),    /* bit 0, PDN_COND1_FROM */
    HAL_CLOCK_CG_PWM1                   = (1 + 32),    /* bit 1, */
    HAL_CLOCK_CG_PWM2                   = (2 + 32),    /* bit 2, */
    HAL_CLOCK_CG_PWM3                   = (3 + 32),    /* bit 3, */
    HAL_CLOCK_CG_PWM4                   = (4 + 32),    /* bit 4, */
    HAL_CLOCK_CG_PWM5                   = (5 + 32),    /* bit 5, */
    HAL_CLOCK_CG_PWM6                   = (6 + 32),    /* bit 6, */
    HAL_CLOCK_CG_PWM7                   = (7 + 32),    /* bit 7, */
    HAL_CLOCK_CG_PWM8                   = (8 + 32),    /* bit 8, */
    HAL_CLOCK_CG_PWM9                   = (9 + 32),    /* bit 9, */
    HAL_CLOCK_CG_CM4_DMA                = (10 + 32),   /* bit 10, */
    HAL_CLOCK_CG_DSP_DMA                = (11 + 32),   /* bit 11, */
    HAL_CLOCK_CG_I2S_DMA                = (12 + 32),   /* bit 12, PDN_COND1_TO */

    /* NR_XO_PDN_COND0 = 28 */
    HAL_CLOCK_CG_I2C_DMA                = (0 + 64),    /* bit 0, XO_PDN_COND0_FROM */
    HAL_CLOCK_CG_CAP_TOUCH              = (1 + 64),    /* bit 1, */
    HAL_CLOCK_CG_AUD_ENGINE             = (2 + 64),    /* bit 2, */
    HAL_CLOCK_CG_AUD_UL_HIRES           = (3 + 64),    /* bit 3, */
    HAL_CLOCK_CG_AUD_DL_HIRES           = (4 + 64),    /* bit 4, */
    HAL_CLOCK_CG_AUD_INTERFACE0         = (5 + 64),    /* bit 5, */
    HAL_CLOCK_CG_AUD_INTERFACE1         = (6 + 64),    /* bit 6, */
    HAL_CLOCK_CG_AUD_GPSRC              = (7 + 64),    /* bit 7, */
    HAL_CLOCK_CG_AUD_BUS                = (8 + 64),    /* bit 8, */
    HAL_CLOCK_CG_BT_26M                 = (9 + 64),    /* bit 9, */
    HAL_CLOCK_CG_IRRX                   = (10 + 64),   /* bit 10, */
    HAL_CLOCK_CG_I2C0                   = (11 + 64),   /* bit 11, */
    HAL_CLOCK_CG_I2C1                   = (12 + 64),   /* bit 12, */
    HAL_CLOCK_CG_I2C2                   = (13 + 64),   /* bit 13, */
    HAL_CLOCK_CG_ANC                    = (14 + 64),   /* bit 13, */
    HAL_CLOCK_CG_BT_104M                = (15 + 64),   /* bit 15, */
    HAL_CLOCK_CG_SPM                    = (16 + 64),   /* bit 16, */
    HAL_CLOCK_CG_MIXEDSYS               = (17 + 64),   /* bit 17, */
    HAL_CLOCK_CG_EFUSE                  = (18 + 64),   /* bit 18, */
    HAL_CLOCK_CG_SEJ                    = (19 + 64),   /* bit 19, */
    HAL_CLOCK_CG_I2C_AO                 = (20 + 64),   /* bit 20, */
    HAL_CLOCK_CG_AUXADC                 = (21 + 64),   /* bit 21, */
    HAL_CLOCK_CG_ABB_26M                = (22 + 64),   /* bit 22, */
    HAL_CLOCK_CG_OSC_1P1                = (23 + 64),   /* bit 23, */
    HAL_CLOCK_CG_OSC_0P9                = (24 + 64),   /* bit 24, */
    HAL_CLOCK_CG_GPLL_26M               = (25 + 64),   /* bit 25, */
    HAL_CLOCK_CG_APLL_26M               = (26 + 64),   /* bit 26, */
    HAL_CLOCK_CG_DEBUGSYS               = (27 + 64),   /* bit 27, XO_PDN_COND0_TO*/
    HAL_CLOCK_CG_END                    = (28 + 64)
} hal_clock_cg_id;

typedef enum {
    hf_fsys_ck = 0x11,
    hf_fsfc_ck,
    hf_fspimst0_ck,
    hf_fspimst1_ck,
    hf_fspimst2_ck,
    hf_fdiomst_ck,
    hf_fspislv_ck,
    hf_fusb_ck,
    hf_faud_bus_ck,
    hf_faud_gpsrc_ck,
    hf_femi_ck,
    f_fspm_ck,
    f_f26m_ck,
    f_faud_interface0_ck=0x23,
    f_faud_interface1_ck,
    hf_faud_i2s0_m_ck=0x2D,
    hf_faud_i2s1_m_ck,
    hf_faud_i2s2_m_ck=0x30,
    hf_faud_i2s3_m_ck,
    hf_faud_uplink_hires_ck,
    hf_faud_downlink_hires_ck
}src_clock;
/**
  * @}
  */

/**
 * @}
 * @}
 */
#endif


#ifdef HAL_DVFS_MODULE_ENABLED
typedef enum {
    DVFS_LOW_SPEED_26M = 0,
    DVFS_HALF_SPEED_39M,
    DVFS_FULL_SPEED_78M,
    DVFS_HIGH_SPEED_156M,
    DVFS_MAX_SPEED,
} dvfs_voltage_mode_t;
#define     HAL_DVFS_MAX_SPEED  DVFS_MAX_SPEED
typedef enum {
    DVFS_26M_SPEED = 0,
    DVFS_39M_SPEED,
    DVFS_78M_SPEED,
    DVFS_156M_SPEED,
    DVFS_CURRENT_SPEED,
    DVFS_ERROR_SPEED,
} dvfs_frequency_t;


#endif


#ifdef HAL_SW_DMA_MODULE_ENABLED
/**
 * @addtogroup HAL
 * @{
 * @addtogroup SW_DMA
 * @{
 * @defgroup hal_sw_dma_define Define
 * @{
 */

/** @brief  The maximum number of SW DMA users.
 */
#define    SW_DMA_USER_NUM  (8)

/** @brief  The GDMA channel for SW DMA users in CM4.
 */
#define    SW_DMA_CHANNEL   (4)

/**
 * @}
 */


/**
 * @}
 * @}
 */
#endif

#ifdef __cplusplus
}
#endif

#endif /* __HAL_PLATFORM_H__ */

