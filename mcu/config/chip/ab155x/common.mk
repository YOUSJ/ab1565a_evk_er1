# Copyright Statement:                                                                                               
#                                                                                                                    
# (C) 2017  Airoha Technology Corp. All rights reserved.                                                             
#                                                                                                                    
# This software/firmware and related documentation ("Airoha Software") are                                           
# protected under relevant copyright laws. The information contained herein                                          
# is confidential and proprietary to Airoha Technology Corp. ("Airoha") and/or its licensors.                        
# Without the prior written permission of Airoha and/or its licensors,                                               
# any reproduction, modification, use or disclosure of Airoha Software,                                              
# and information contained herein, in whole or in part, shall be strictly prohibited.                               
# You may only use, reproduce, modify, or distribute (as applicable) Airoha Software                                 
# if you have agreed to and been bound by the applicable license agreement with                                      
# Airoha ("License Agreement") and been granted explicit permission to do so within                                  
# the License Agreement ("Permitted User").  If you are not a Permitted User,                                        
# please cease any access or use of Airoha Software immediately.                                                     
# BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES                                        
# THAT AIROHA SOFTWARE RECEIVED FROM AIROHA AND/OR ITS REPRESENTATIVES                                               
# ARE PROVIDED TO RECEIVER ON AN "AS-IS" BASIS ONLY. AIROHA EXPRESSLY DISCLAIMS ANY AND ALL                          
# WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF                             
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.                                              
# NEITHER DOES AIROHA PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE                                            
# SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR                                              
# SUPPLIED WITH AIROHA SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH                                            
# THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES                               
# THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES                       
# CONTAINED IN AIROHA SOFTWARE. AIROHA SHALL ALSO NOT BE RESPONSIBLE FOR ANY AIROHA                                  
# SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR                                   
# STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND AIROHA'S ENTIRE AND                               
# CUMULATIVE LIABILITY WITH RESPECT TO AIROHA SOFTWARE RELEASED HEREUNDER WILL BE,                                   
# AT AIROHA'S OPTION, TO REVISE OR REPLACE AIROHA SOFTWARE AT ISSUE,                                                 
# OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO                                          
# AIROHA FOR SUCH AIROHA SOFTWARE AT ISSUE.                                                                          
#                                                                                                                    

IC_CONFIG                             = ab155x
BOARD_CONFIG                          = ab155x_evk

# debug level: none, error, warning, info and debug
MTK_DEBUG_LEVEL                       = info

# let syslog dump to flash
MTK_SAVE_LOG_TO_FLASH_ENABLE          = n

MTK_USB_DEMO_ENABLED                  = y
MTK_USB_AUDIO_V1_ENABLE               = n
MTK_USB_AUDIO_V2_ENABLE               = n
MTK_USB_AUDIO_MICROPHONE              = n
#SWLA
MTK_SWLA_ENABLE                       = y
MTK_NO_PSRAM_ENABLE = y
# heap dump
MTK_SUPPORT_HEAP_DEBUG                = y
MTK_SUPPORT_HEAP_DEBUG_ADVANCED       = n
# heap peak profiling
MTK_HEAP_SIZE_GUARD_ENABLE            = n
###################################################
# bt at command
MTK_BT_AT_COMMAND_ENABLE = y

# CAPID store to NVDM
MTK_CAPID_IN_NVDM_AT_COMMAND_ENABLE = y

# port service
MTK_PORT_SERVICE_ENABLE = y

#NVDM feature
MTK_NVDM_ENABLE = y

# bt codec enable
MTK_BT_CODEC_ENABLED = y

# BT A2DP codec AAC support
MTK_BT_A2DP_AAC_ENABLE = y

# BT A2DP codec vendor support
MTK_BT_A2DP_VENDOR_ENABLE = n

# avm direct feature
MTK_AVM_DIRECT                       = y

# BT Dual mode
MTK_BT_DUO_ENABLE = y

# bt module enable
MTK_BT_ENABLE                       = y
MTK_BLE_ONLY_ENABLE                 = n
MTK_BT_HFP_ENABLE                   = y
MTK_BT_HSP_ENABLE                   = y
MTK_BT_AVRCP_ENABLE                 = y
MTK_BT_AVRCP_ENH_ENABLE             = y
MTK_BT_A2DP_ENABLE                  = y
MTK_BT_PBAP_ENABLE                  = n
MTK_BT_SPP_ENABLE                   = y
# aws earbuds feature
MTK_AWS_MCE_ENABLE                  = y

# HCI log output mixed with system log.
MTK_HCI_LOG_MIX_WITH_SYSLOG         = y

#BT external timer
MTK_BT_TIMER_EXTERNAL_ENABLE = y
MTK_PORT_SERVICE_ENABLE               = y

MTK_ATCI_VIA_PORT_SERVICE           = y

MTK_HAL_EXT_32K_ENABLE = n

#MTK BATTERY CONTROL
MTK_BATTERY_MANAGEMENT_ENABLE		  = y

#AT Command
MTK_AT_COMMAND                        = y
MTK_SMT_AUDIO_TEST                    = y
#FOTA Related
MTK_RACE_CMD_ENABLE                 = y
MTK_RACE_DUAL_CMD_ENABLE            = y
MTK_FOTA_ENABLE                     = y
MTK_FOTA_VIA_RACE_CMD               = y
MTK_FOTA_VIA_RACE_CMD_DUAL_DEVICES  = y
MTK_MBEDTLS_CONFIG_FILE             = config-mtk-fota-race-cmd.h
MTK_PORT_SERVICE_BT_ENABLE          = y
MTK_AIRUPDATE_ENABLE                = y
MTK_SWITCH_TO_RACE_COMMAND_ENABLE   = y
MTK_RACE_FIND_ME_ENABLE             = y
# prompt sound
MTK_PROMPT_SOUND_ENABLE	            = y
MTK_PROMPT_SOUND_SYNC_ENABLE        = n
MTK_AUDIO_AT_CMD_PROMPT_SOUND_ENABLE          = n
# mp3
MTK_AUDIO_MP3_ENABLED               = n
MTK_MP3_DECODER_ENABLED             = y
MTK_MP3_CODEC_TASK_DEDICATE         = y
MTK_MP3_STEREO_SUPPORT              = y
# wave decoder by charlie
MTK_WAV_DECODER_ENABLE              = y
# record middleware
MTK_RECORD_ENABLE                   = y
# audio dump
MTK_AUDIO_DUMP_ENABLE               = y
# PEQ
MTK_PEQ_ENABLE                      = y

# linein playback
MTK_LINEIN_PLAYBACK_ENABLE          = y
# LINEIN PEQ 
MTK_LINEIN_PEQ_ENABLE               = n
# LINEIN INS
MTK_LINEIN_INS_ENABLE               = n

# AirDump module
MTK_AIRDUMP_EN                      = y

# amp dc compensation
MTK_AMP_DC_COMPENSATION_ENABLE      = n

MTK_SYSTEM_AT_COMMAND_ENABLE        = y

# system hang debug: none, y, o1, o2 and mp
MTK_SYSTEM_HANG_TRACER_ENABLE       = y

MTK_MEMORY_MONITOR_ENABLE           = n

# NVDM gain setting table
MTK_AUDIO_GAIN_TABLE_ENABLE         = y
EINT_KEY_ENABLED                    = y
ifeq ($(MTK_AWS_MCE_ENABLE),y)
SUPPORT_ROLE_HANDOVER_SERVICE       = y
endif

ifeq ($(MTK_AWS_MCE_ENABLE),y)
MTK_AWS_MCE_ROLE_RECOVERY_ENABLE = y
endif

# boot reason check
MTK_BOOTREASON_CHECK_ENABLE         = y
MTK_BT_FAST_PAIR_ENABLE             = y
MTK_MINIDUMP_ENABLE                 = n
MTK_FULLDUMP_ENABLE                 = y


# ANC module
ANC_LIB = $(strip $(SOURCE_DIR))/../mcu/middleware/MTK/audio/anc_control/anc.flag
ifeq ($(ANC_LIB), $(wildcard $(ANC_LIB)))
$(warning If ANC control doesn't exist, user can't enable MTK_ANC_ENABLE)
MTK_ANC_ENABLE                      = y
else
MTK_ANC_ENABLE                      = n
endif
ifeq ($(MTK_ANC_ENABLE),y)
MTK_ANC_BACKUP_STATUS_ENABLE        = y
ifeq ($(MTK_AWS_MCE_ENABLE),y)
MTK_HYBRID_ANC_ENABLE               = y
ifeq ($(MTK_HYBRID_ANC_ENABLE),y)
MTK_POST_PEQ_DEFAULT_ON             = y
MTK_VOICE_ANC_EQ                    = y
MTK_DEQ_ENABLE                      = n
endif
endif
endif

# Race relay cmd
ifeq ($(MTK_AWS_MCE_ENABLE),y)
MTK_RACE_RELAY_CMD_ENABLE = y
endif

# APPs features
ifneq ($(CCASE_ENABLE),y)
APPS_DISABLE_BT_WHEN_CHARGING 		= y
#Enable BT when charger out, Disable BT when Charger in
endif

APPS_SLEEP_AFTER_NO_CONNECTION		= y

#define atci tx buffer slim
MTK_ATCI_BUFFER_SLIM         = y

# GSOUND LIBRARY ENABLE
GSOUND_LIB = $(strip $(SOURCE_DIR))/middleware/third_party/gsound/module.mk
ifeq ($(GSOUND_LIB), $(wildcard $(GSOUND_LIB)))
GSOUND_LIBRARY_ENABLE               = n
endif

ifeq ($(GSOUND_LIBRARY_ENABLE), y)
# BISTO_NOT_ENABLE_AM_RECORD
MTK_AM_NOT_SUPPORT_STREAM_IN        = y
MTK_SBC_ENCODER_ENABLE              = y
endif

ifeq ($(SUPPORT_ROLE_HANDOVER_SERVICE),y)
# Do RHO when agent low battery, power off or disable BT
APPS_AUTO_TRIGGER_RHO       		= y
# Do RHO press key on partner
APPS_TRIGGER_RHO_BY_KEY     		= y
endif

# Record opus encoder
MTK_RECORD_OPUS_ENABLE = n

# Enable DSP1 DRAM for DSP0 memory pool
MTK_DSP1_DRAM_FOR_DSP0_POOL_ENABLE = n

# Audio PLC
MTK_AUDIO_PLC_ENABLE = n

MTK_IN_EAR_FEATURE_ENABLE = n

MTK_CONN_VP_SYNC_ENABLE = n
