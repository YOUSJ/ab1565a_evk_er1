include $(SOURCE_DIR)/driver/chip/ab155x/src/GCC/module.mk

include $(SOURCE_DIR)/driver/board/$(BOARD_CONFIG)/hw_resource_assignment/module.mk

ifneq ($(wildcard $(strip $(SOURCE_DIR))/driver/chip/ab155x/src_core/),)
include $(SOURCE_DIR)/driver/chip/ab155x/src_core/GCC/module.mk
else
LIBS += $(SOURCE_DIR)/prebuilt/driver/chip/ab155x/lib/libhal_core_CM4_GCC.a
endif

ifneq ($(wildcard $(strip $(SOURCE_DIR))/driver/chip/ab155x/src_protected/),)
include $(SOURCE_DIR)/driver/chip/ab155x/src_protected/GCC/module.mk
else
LIBS += $(SOURCE_DIR)/prebuilt/driver/chip/ab155x/lib/libhal_protected_CM4_GCC.a
endif

ifeq ($(MTK_SECURE_BOOT_ENABLE),y)
ifneq ($(wildcard $(strip $(SOURCE_DIR))/milddleware/MTK/secure_boot/),)
include $(SOURCE_DIR)/middleware/MTK/secure_boot/module.mk
else
LIBS += $(SOURCE_DIR)/prebuilt/driver/chip/ab155x/lib/libsboot_CM4_GCC.a
endif
endif

